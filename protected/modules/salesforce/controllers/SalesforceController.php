<?php

class SalesforceController extends Controller
{
	public function actionIndex()
	{
		$sf_config = Yii::app()->params['salesforce'];
		
		$client_id =  $sf_config['client_id'];
		$client_secret =  $sf_config['client_secret'];
		$redirect_uri =  $sf_config['redirect_uri'];
		$login_uri =  $sf_config['login_uri'];

		$token_url = $login_uri . "/services/oauth2/token";

		$code = $_GET['code'];

		if (!isset($code) || $code == "") {
			die("Error - code parameter missing from request!");
		}

		$params = "code=" . $code
			. "&grant_type=authorization_code"
			. "&client_id=" . $client_id
			. "&client_secret=" . $client_secret
			. "&redirect_uri=" . urlencode($redirect_uri);

		$curl = curl_init($token_url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

		$json_response = curl_exec($curl);

		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		if ( $status != 200 ) {
			$error = "Error: call to token URL $token_url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
			
			$this->render('index',array("error"=>$error));
			exit;
			
//			die("Error: call to token URL $token_url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
		}

		curl_close($curl);

		$response = json_decode($json_response, true);

		$access_token = $response['access_token'];
		$instance_url = $response['instance_url'];
		$refresh_token = $response['refresh_token'];

		if (!isset($access_token) || $access_token == "") {
			$error = "Error - access token missing from response!";
//			die("Error - access token missing from response!");
			$this->render('index',array("error"=>$error));
			exit;
		}

		if (!isset($instance_url) || $instance_url == "") {
			$error = "Error - instance URL missing from response!";
//			die("Error - instance URL missing from response!");
			$this->render('index',array("error"=>$error));
			exit;
		}

//echo "here"; exit;
		$crm_salesforce = new Salesforce(); // model obj
		$crm_salesforce->user_id = Yii::app()->user->id;
		$crm_salesforce->access_token = $access_token;
		$crm_salesforce->refresh_token = $refresh_token;
		$crm_salesforce->instance_url = $instance_url;
				
		$crm_salesforce->save();
		$last_id = $crm_salesforce->id; 
		
		$this->render('index',array("last_id"=>$last_id));
	}

}
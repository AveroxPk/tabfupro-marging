<?php

class UserModule extends CWebModule
{
    
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
                Yii::app()->theme = 'classic'; 
		$this->setImport(array(
			'user.models.*',
			'user.components.*',
			'user.forms.*',
                        'facebook.models.*',
                        'autoresponders.models.*',
			'autoresponders.components.*',
                        'autoresponders.components.aweber.*',
                        'autoresponders.components.getResponse.*',
                        'autoresponders.components.iContact.*',
                        'autoresponders.components.infusionSoft.*',
                        'autoresponders.components.mailChimp.*',
                        'autoresponders.components.sendReach.*',
                        'message.models.*',
                        'message.components.*',
                        'membership.models.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
                    
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}

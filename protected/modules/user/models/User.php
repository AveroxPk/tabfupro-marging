<?php

/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'User':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $lastname
 * @property string $resetkey
 * @property integer $status
 * @property AutoresponderAweber $autoresponderAwerb
 * @property AutoresponderGetresponse $autoresponderGetresponse
 * @property AutoresponderIcontact $autoresponderIcontact
 * @property AutoresponderInfusionsoft $autoresponderInfusionsoft
 * @property AutoresponderMailchimp $autoresponderMailchimp
 * @property AutoresponderSendreach $autoresponderSendreach
 */
    //include __DIR__.'/../../../../PHPMailer/PHPMailerAutoload.php';
class User extends CActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	
	const PACKAGE = 0; // package will be 0 if old tabfu.online user upgrade to tabfu.online, so all feature will available to them
	const PACKAGE_NULL = NULL;  // by default all old tabfu.online user has null package so they cannot login to tabfu.online with null package status
	
    const STATUS_PASSWD_RESET = 3;
    
    const USER_NORMAL = 1;
    const USER_ADMIN  = 2;
    const TIME_FOR_RESET_PASS = 86400;

    const DAYS_FREE_MEMBERSHIP = 15;
	
    public $repeatPassword;
    public $resetContent = 'Reset link: {%reset%}';
    public $access_token = ''; // Facebook access token
    public $new_password;
    public $verify_password;
    public $current_password;
    public $current_password2;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email', 'required'),
            array('email,name,new_password,verify_password,current_password,current_password2', 'required','on'=>'my_profile'),
            array('new_password', 'compare', 'compareAttribute'=>'verify_password', 'on'=>'my_profile'),
            array('current_password', 'compare', 'compareAttribute'=>'current_password2', 'on'=>'my_profile'),
            array('current_password', 'verifypassword','on'=>'my_profile'),
            array('email, password, repeatPassword, name', 'length', 'max'=>255),
            array('email, password, name, lastname', 'safe'),
            

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, email, password, name, lastname, status, package', 'safe', 'on'=>'search'),
            
            
            array('email, password, repeatPassword', 'required', 'on'=>'register'),
            array('email', 'unique', 'on'=>'register,create'),
            array('email', 'email'),
            array('password', 'compare', 'compareAttribute'=>'repeatPassword', 'on'=>'register,profile'),
        );
    }
    
    
    

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'userFacebook'=>array(self::HAS_MANY, 'UserFacebook', 'user_id'),
            'autoresponderAweber'=>array(self::HAS_ONE, 'AutoresponderAweber', 'user_user_id'),
            'autoresponderGetresponse'=>array(self::HAS_ONE, 'AutoresponderGetresponse', 'user_user_id'),
            'autoresponderIcontact'=>array(self::HAS_ONE, 'AutoresponderIcontact', 'user_user_id'),
            'autoresponderInfusionsoft'=>array(self::HAS_ONE, 'AutoresponderInfusionsoft', 'user_user_id'),
            'autoresponderMailchimp'=>array(self::HAS_ONE, 'AutoresponderMailchimp', 'user_user_id'),
            'autoresponderSendreach'=>array(self::HAS_ONE, 'AutoresponderSendreach', 'user_user_id'),
            'userMessages' => array(self::HAS_MANY, 'UserMessage', 'user_user_id    '),
            'favouriteTemplates' => array(self::MANY_MANY, 'ARTemplate', 'template_favourites(user_id, template_id)'),
            'membership'=>array(self::HAS_ONE, 'Membership', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'name' => 'First Name',
            'lastname' => 'Last Name',
            'status' => 'Status',
            'package' => 'Package',
            'registerdate' => 'Register Date',
            'new_password' => 'New Password',
            'verify_password' => 'Verify Password',
            'current_password' => 'Current Password',
            'current_password2' => 'Current Password',
        );
    }

        public function verifypassword($attributes,$params){
        $password = $this->$attributes;;
        if(!password_verify($password, $this->password )){
            $this->addError($attributes,'In not your password');
        }else{
            
        }
       
    }

        /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria(array(
            'select' => 't.*',
            'alias' => 't',
            'group' => 't.user_id'
        ));
        $sort = new CSort();

        $sort->defaultOrder =array(
            'lastname'=>'desc'
        );
        
        $criteria->compare('email',$this->email,true);
        $criteria->compare('password',$this->password,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('type',$this->type);
        $criteria->compare('status',$this->status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort
        ));
    }

    public function getDisplayName()
    {
        return strlen($this->name) > 0 ? $this->name : $this->email;
    }

    public function isUniqueUser()
    {
        if( self::model()->exists('email = :email', array(':email' => $this->email) ) ) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function isExists($uid)
    {
        if( $uid = self::model()->find('email = :email', array(':email' => $uid)) ) {
            return $uid->user_id;
        } else {
            return false;
        }
    }
    
    public function resetUserPassword()
    {
        //$this->status = self::STATUS_PASSWD_RESET;
        $this->resetkey = self::generateHash( $this->email );
        $this->resetdate = strtotime("now");
        return $this->save() ? true : false;
    }

    public function sendMessageReset()
    {
        //Send reset linkemail = null, $resetkey = null
        $resetLink = '<a href="'.Yii::app()->request->getBaseUrl(true).Yii::app()->createUrl( '/user/home/ForgetPassword/step/2/mail/'.$this->email.'/hash/'.self::generateHash( $this->email )).'">Click</a>';


        $mailto = $this->email;
        $mailsubject = 'Reset password';
        $mailcontent = 'To reset password please click link below:<br/>'.$resetLink;
        return MailHelper::sendMail($mailto, $mailsubject, $mailcontent, null, 'text/html');
    }
    
    public function sendMessageActivate()
    {
        $hash = self::generateHash( $this->email );
        $this->resetkey = $hash;
		
        if($this->save(false)){
			
            $activateLink = '<a href="'.Yii::app()->request->getBaseUrl(true).Yii::app()->createUrl( '/user/home/activate/mail/'.$this->email.'/hash/'.$hash).'">Click</a>';

            $mailto = $this->email;
            $mailsubject = 'Activate account';
            $mailcontent = 'To activate account please click link below:<br/>'.$activateLink;
			
			if(!MailHelper::sendMail($mailto, $mailsubject, $mailcontent, null, 'text/html')) {
				Yii::log('sendMessageActivate', 'error');
			}
			return true;
        }
        else{
            return false;
        }
    }
    
    public function sendMessageRegistrationFacebook($password){
        return $this->sendMessageWithPassword($password);
    }
    
    public function sendMessageWithPassword($password){
        $mailto = $this->email;
        $mailsubject = 'TabFu registration';
        
        $mailcontent  = '<p>Welcome to Tabfu - thanks for becoming a member! Please use the information below to access you account.</p>';
        $mailcontent .= "<p>your login: <strong>{$this->email}</strong></p>";
        $mailcontent .= "<p>your password: <strong>{$password}</strong></p>";
        $mailcontent .= '<p>You can login here: <a href="https://tabfu.online/login">tabfu.online/login</a></p>';
        $mailcontent .= '<p>If you’re having any problems please contact support: <a href="mailto:support@tabfu.zendesk.com">support@tabfu.zendesk.com</a></p>';
        $mailcontent .= '<p>Thanks again!</p>';
        if(!MailHelper::sendMail($mailto, $mailsubject, $mailcontent, null, 'text/html')) {
            Yii::log('sendMessageWithPassword', 'error');
        }
        return true;
    }
    
    public function sendMessageMembershipDeactivated(){
        $mailto = $this->email;
        $mailsubject = 'TabFu Membership deactivated';
        $mailcontent  = 'Your membership end date has expired and now Your membership is inactive.';

        return MailHelper::sendMail($mailto, $mailsubject, $mailcontent, null, 'text/html');
    }

    public function sendMessageAgencyDeactivated(){
        $mailto = $this->email;
        $mailsubject = 'Yout TabFu Agency Membership deactivated';
        $mailcontent  = 'Your membership end date has expired and now Your membership is inactive.';

        return MailHelper::sendMail($mailto, $mailsubject, $mailcontent, null, 'text/html');
    }
    
    public function resetPassword()
    {
        if ( $this instanceof User ) {

            if ( $this->resetUserPassword() ) {
                if ( $this->sendMessageReset() ) {
                    var_dump('message send');
                    exit();

                    return true;
                } else {

                    var_dump('message not send');
                    exit();
                    Yii::log('sendMessageReset', 'error');
                    return false;
                }
            } else {

                var_dump('reset error');
                exit();


                Yii::log('resetUserPassword', 'error');
                return false;
            }
        }
    }
    
    
    
    public static function generateHash($string)
    {
        return md5($string.time());
    }
    
    public static function encode($passwd){
        return password_hash($passwd.Yii::app()->params['salt'], PASSWORD_BCRYPT,  array('cost'=>8));
    
        
    }
    
    public static function getUserRoles()
    {
        return array(
            self::USER_NORMAL => 'User',
            self::USER_ADMIN  => 'Administrator'
        );
    }
    
    public static function getUserStatuses($model = null)
    {
        $result = array(
            self::STATUS_ACTIVE  => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_DELETED => 'Deleted'
        );
        if (!$model || $model->type == self::USER_ADMIN) {
            $result[self::STATUS_PASSWD_RESET] = 'Password reset';
        }
        return $result;
    }

    public function getUserMessageTargets() {
        
        $target = array(Message::TARGET_ALL);
        
        if($this->getRemainingFreeMembership()) {
            $target[] = Message::TARGET_TRIAL_MEMBER;
            
        } else if($this->isMembershipActive()) {
            $target[] = Message::TARGET_PAYING_MEMBER;
        }
        
        return $target;
    }
    
    public function getAllToArray(){
        $criteria = new CDbCriteria;
        $criteria->select = '*';
        $models = User::model()->findAll($criteria);
        $return = array();
        foreach ($models as $model){
            $return[$model->id] = $model->email;
        }
        return $return;
    }

    /**
     * Check if user is admin 
     * @return boolean
     */
    public function isAdmin(){
        if($this->type == USER::USER_ADMIN){
                return true;
        }
        return false;
    }
    
    public function isActive()
    {
        return $this->status == User::STATUS_ACTIVE || $this->status == User::STATUS_PASSWD_RESET ? true : false;
    }
    
    /**
     * Check if user membership is active
     * @return boolean
     */
    public function isMembershipActive() {

        if(!$this->membership) { return false; }
        
        return $this->membership->isActive();
    }
    
   
    public function getEndDate() {    	
    	return $this->membership->membership_end_date;
    }
    
    public function getMessagesUnreaded(){
        $target = $this->getUserMessageTargets();
        $target_sql = '';
        if($target) {
            $target_sql = " AND target IN (".implode(',',$target).") ";
        }
        
        $messages = Yii::app()->db->createCommand()
            ->select('id, title, content, date')
            ->from('message')
            ->where("(`date`>='{$this->registerdate}' OR special=1) AND id NOT IN (select message_id from user_message where user_user_id = '{$this->user_id}') $target_sql")
            ->order('id DESC')
            ->queryAll();
        
        return $messages;
    }
    
    
    public function getMessagesReaded(){
        $target = $this->getUserMessageTargets();
        $target_sql = '';
        if($target) {
            $target_sql = " AND target IN (".implode(',',$target).") ";
        }
        
        $messages = Yii::app()->db->createCommand()
            ->select('id, title, content, date')
            ->from('message')
            ->where("(`date`>='{$this->registerdate}' OR special=1) AND id IN (select message_id from user_message where user_user_id = '{$this->user_id}' AND status = '0') $target_sql")
            ->order('id DESC')
            ->queryAll();
        
        return $messages;
    }
    
    public function getMessagesArchived(){
        $target = $this->getUserMessageTargets();
        $target_sql = '';
        if($target) {
            $target_sql = " AND target IN (".implode(',',$target).") ";
        }
        
        $messages = Yii::app()->db->createCommand()
            ->select('id, title, content, date')
            ->from('message')
            ->where("(`date`>='{$this->registerdate}' OR special=1) AND id IN (select message_id from user_message where user_user_id = '{$this->user_id}' AND status = '1') $target_sql")
            ->order('id DESC')
            ->queryAll();
        
        return $messages;
    }
    
    public function generateRandomPassword($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
    }




    public function getRemainingFreeMembership(){
        if($this->membership) { return false; }
        
        if(time()-strtotime($this->registerdate)<=(self::DAYS_FREE_MEMBERSHIP*24*60*60)){
            return self::DAYS_FREE_MEMBERSHIP-floor((time()-strtotime($this->registerdate))/(24*60*60));
        }else{
            return false;
        }
    }
    
    public function unpublishFacebookTabs() {
        
        foreach($this->userFacebook as $facebookUser) {
            
            //Init facebook
            $facebook = Yii::app()->getModule('facebook');
            $facebook->setAccessToken($facebookUser->long_token);
            
            $tabs = UserTabs::model()->findByUserPublished($facebookUser->id);
            foreach($tabs as $tab) {
                $tab->unpublishOnFB();
            }
        }
        
        return true;
    }
    
    
    public function renderMembershipStatus($data) {
 
        if(!$data->membership) { 
            return Membership::getMembershipStatusLabel(Membership::STATUS_INACTIVE);
        }
        
        return Membership::getMembershipStatusLabel($data->membership->membership_status);
    }
    
	public function findByEmail($email)
	{
		return self::model()->findByAttributes(array('email' => $email));
	}
    
}

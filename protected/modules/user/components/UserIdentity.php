<?php

class UserIdentity extends CUserIdentity
{
    const DURATION = 2592000;//30 days
    const ERROR_USERNAME_INACTIVE=67;
	
    private $_id;
    private $_package;  //usama addition
	
    public function authenticate()
    {
        $record=User::model()->findByAttributes(array('email'=>$this->username));
        if($record===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if(!password_verify($this->password, $record->password ))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else if($record->status == User::STATUS_INACTIVE)
            $this->errorCode = self::ERROR_USERNAME_INACTIVE;

		// usama addition
		
        else if($record->package == User::PACKAGE_NULL)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
		
        else if($record->status == User::STATUS_DELETED)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else
        {
			$user_id = $record->user_id;
			
            $this->_id = $user_id;
			
			$this->_package = $record->package;

			if($record->package == 1)
			{
				$package_features = Package::model()->findAll('user_id = :userid AND status = :status', array(':userid' =>$user_id, ':status' => 1));
				$features = array();
				foreach($package_features as $key => $value)
				{
					$features[] = $value->feature;
				}

				if(empty($features))
				{
					$this->errorCode = self::ERROR_USERNAME_INVALID;
					return !$this->errorCode;
				}
				
				$this->_package = $features;						
			}
			
            $this->setState('status', $record->status);
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
 
    public function setId($id){
        $this->_id = $id;
    }
    
    public function getId()
    {
        return $this->_id;
    }
	
     //usama addition
    public function setPackage($value){
        $this->_package = $value;
    }
    
    public function getPackage()
    {
        return $this->_package;
    }
    
}
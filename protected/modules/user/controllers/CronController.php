<?php

class CronController extends Controller
{
    
    public function filters() {
        return array(
            'accessControl', 
        );
    }
    
    public function accessRules() {
        return array(
            array(
                'allow',
                'users' => array('*'),
                ),
            );
    }
    
    public function actionRegisterFromAweber() {
        
//            $access_code = 'AzutsUPoZg4F1pSSH72N1DbE|ls1R8EIEcnX3Fdb0Rsl7v8QgnLTSupjB9GIP74jP|AqiGmReKRF4tHAZ6AZ0UFbO0|ApziAVZRyZb9xrGkieaUnLjizyiTeK7RXUYl6oCu|7nrn9s|';
//            $auth = AWeberAPI::getDataFromAweberID(trim($access_code));
//            $consumer_key = $auth[0];
//            $consumer_secret = $auth[1];
//            $access_key = $auth[2];
//            $access_secret = $auth[3];
      
        $aweberConfig = isset(Yii::app()->params['Aweber']) ? Yii::app()->params['Aweber'] : array();
        try {
            $this->checkAweberConfig($aweberConfig);

        } catch(Exception $e) {
            echo $e->getMessage();
            exit;
        }
            
        try {

            $consumer_key   = $aweberConfig['consumer_key'];
            $consumer_secret = $aweberConfig['consumer_secret'];
            $access_key     = $aweberConfig['access_key'];
            $access_secret  = $aweberConfig['access_secret'];
            $list_name      = $aweberConfig['trial_list_name'];
            
            $api = new AWeberAPI($consumer_key, $consumer_secret);
            $account = $api->getAccount($access_key, $access_secret);
            
            $list = $account->lists->find(array('name' => $list_name));
            $list = isset($list[0]) ? $list[0] : null;
            
            if(!$list) {
                throw new Exception('List "'.$list_name.'" not found.');
            }
            
            $params = array('status' => 'subscribed');
            $found_subscribers = $list->subscribers->find($params);
            $emails = array();
            foreach($found_subscribers as $subscriber) {
                $emails[] = $subscriber->email;
            }
            
            Yii::log(CVarDumper::dumpAsString($emails), CLogger::LEVEL_INFO, 'cron.registerFromAweber.toRegister');
            
            $emailsAdded = array();
            // foreach email list
            foreach($emails as $email) {

                $newUser = new User();
                $newUser->email = $email;
                if(!$newUser->isUniqueUser()) {
                    continue;
                }
                $password = $newUser->generateRandomPassword();
                $passwordHashed = User::encode($password);
                $newUser->password = $passwordHashed;
                $newUser->repeatPassword = $passwordHashed;
                $newUser->status = User::STATUS_ACTIVE;
                $newUser->type = User::USER_NORMAL;
                if($newUser->save()){
                    // send mail to user with password
                    $newUser->sendMessageWithPassword($password);
                    $emailsAdded[] = $email;
                }
            }
            
            Yii::log(CVarDumper::dumpAsString($emailsAdded), CLogger::LEVEL_INFO, 'cron.registerFromAweber.registered');
        
        }catch(Exception $e){
            echo $e->getMessage();
            Yii::log(CVarDumper::dumpAsString($e->getMessage()), CLogger::LEVEL_ERROR, 'cron.registerFromAweber');
            exit;
        }
        
        echo 'OK';
        return true;
    }
    
    public function checkAweberConfig($config)  
    {
        if(!is_array($config)) {
            throw Exception('Config is not array');
        }
        $requiredParams = array('consumer_key','consumer_secret','access_key','access_secret','trial_list_name');
        foreach($requiredParams as $required) {
            if(!isset($config[$required])) {
                throw new Exception('Config parameter "'.$required.'" is required.');
            }
        }
        return true;
    }
    
}
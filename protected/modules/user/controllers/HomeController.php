<?php

// Paul Inglis
// CAAVQsGZA6FSwBAJ6T2wcCeBMZC3QaBF5eal9Nqn5VEbhCZBQT2NbTfFrDx5DMVrvss8pnv3O8XOMYnWbWpBsn75JTD9bwO9B7xuIvAY686Ur2y3hApGIfQFWwSj0ggJ0iqbE7ebWXuljFb1TSkeSuOajUZBsbYpSPBTv3MzoYlTXZB94QiHc83ETn595SPubUXqZAQ80ZA9IV1ZAdD5Y1tQV
// CAAVQsGZA6FSwBANVPLimWBVVEWZAh6db7N4h8yu7JKqlXbGwVIIlhNi18oummLeAHvWqGk3XyJ9Lbg742mkImjZBQ38kWjHh8KtOHHVo6n6aaYXxKTV6qKitSAyqirsWLwloGnG8djvyVcXaTKqO8wpdh6vKIpK4ffVqqJpcyKLxK6vta9IZC0PPmjBoaiEZD

// Bartosz Wójcik
// CAAVQsGZA6FSwBADyc8Vll8oimZC0El4DbhwIS0IWwGthL9A2kDcQkvwhlId8a7i1WEZCMydFSBYIwWaEf1ZAz78OurbjWGPclFsP1NZC9EsKBS1RpEMDdUe9GxuQzPDkjj1KkbhsdAh7Gx7Jxd4cMdGgZBwrUhWbGaEzbOsHdy1UBbmP36l7IkNwOGZAQ5bo4vtZAMiITtCgHwWGiAejnCSZC
// CAAVQsGZA6FSwBADorhYZCozNAbzD3L7Upgpg4ygl1QQvi13GwqZAKfsbC4aKXXKMtVCHO4LbZBDDB00g7AlV1sVWWqotTKnV2chZCMRfEHjlg4Ke0SmLe8S5RriFcdOMu5jbbaaqXytT5ZCJGnWa179r3F0ZA35ZCdlYvuvRnmsvCSPoL00iZAnOUFQCUTR5mOrUZD


class HomeController extends Controller
{
    
    
    
        /**
      * Declares class-based actions.
      */
     public function actions()
     {
         return array(
       'oauth' => array(
         // the list of additional properties of this action is below
         'class'=>'ext.hoauth.HOAuthAction',
         // Yii alias for your user's model, or simply class name, when it already on yii's import path
         // default value of this property is: User
         'model' => 'User', 
         // map model attributes to attributes of user's social profile
         // model attribute => profile attribute
         // the list of avaible attributes is below
         'attributes' => array(
           'email' => 'email',
           //'fname' => 'firstName',
           //'lname' => 'lastName',
           //'gender' => 'genderShort',
           //'birthday' => 'birthDate',
           // you can also specify additional values, 
           // that will be applied to your model (eg. account activation status)
           //'acc_status' => 1,
         ),
       ),
       // this is an admin action that will help you to configure HybridAuth 
       // (you must delete this action, when you'll be ready with configuration, or 
       // specify rules for admin role. User shouldn't have access to this action!)
       'oauthadmin' => array(
         'class'=>'ext.hoauth.HOAuthAdminAction',
       ),
         );
     }
    
    
	public function actionIndex()
	{
            if (!Yii::app()->user->isGuest ){
                $this->render('index');
            } else {
                $this->redirect(array('login'));
            }
	}
        
    public function actionLogin(){
        error_reporting(E_ALL);
        if(Yii::app()->user->isGuest){
            
            // display facebook login button on login page?
            $displayFacebookLogin = isset(Yii::app()->request->cookies['logged']) ? 
                    (string)Yii::app()->request->cookies['logged'] : false;

            $model = new UserLoginForm();
            if(isset($_POST['UserLoginForm'])){
                $model->attributes=$_POST['UserLoginForm'];
                if($model->validate() && $model->login()){
                    // if logged set cookie that shows facebook login button on login page
                    Yii::app()->request->cookies['logged'] = new CHttpCookie('logged', true);
                  $user = User::model()->findByPk(Yii::app()->user->id);
                  if($user->liked == 0){
                      $this->redirect(array('/facebook/like'));
                  }else{
                      $this->redirect(array('/facebook'));
                  }
                  
                }
            }
            $this->render('login', array(
                'model'=>$model,
                'displayFacebookLogin' => $displayFacebookLogin,
            ));
        }else{
            $this->redirect(array('/facebook'));
        }

    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(array('/user/home/login'));
    }

    public function actionForgetPassword($step=null, $hash = null, $mail= null)
    {
        if($step === null){
            $model = new UserResetRequestForm();
            if(isset($_POST['UserResetRequestForm'])) {
                $model->attributes=$_POST['UserResetRequestForm'];
                if ($model->validate()) {
                    if ($model->request()) {
                        Yii::app()->user->setFlash('notice', 'You password will be reset. Please check your email.');
                    }
                }
            }
            $this->render('forgetpassword', array(
                'model'=>$model
            )); 
        }
        else if ($step == 2 && !empty($hash) && !empty($mail)){
            $model = new UserResetPasswordForm();
            if (isset($_POST['UserResetPasswordForm'])){
                $model->attributes=$_POST['UserResetPasswordForm'];
                if ($model->validate()){
                    if ($model->reset($mail, $hash)){
                        Yii::app()->user->setFlash('notice', 'Your password has been changed');
                        $form = new UserLoginForm;
                        $form->loginWoPassword($mail);
                        $this->redirect(array('/user/home/login'));
                    }
                    else{
                        Yii::app()->user->setFlash('notice', 'Something went wrong');
                    }
                }
                else{
                    Yii::app()->user->setFlash('notice', 'Something went wrong');
                }
            }
            $this->render('forgetpassword-step-2', array(
                'model'=>$model,
            ));
        }
        else{
            $this->redirect(array('/user/home/forgetpassword'));
        }

    }

    public function actionActivate($mail = null, $hash = NULL){
        if($mail != null && $hash != null){
            $record = User::model()->findByAttributes(array(
                'email'=>$mail,
                'resetkey'=>$hash
            ));
            if($record != null){
                if($record->status == User::STATUS_INACTIVE){
                    $record->status = User::STATUS_ACTIVE;
                    $record->save(false);
                    Yii::app()->user->setFlash('notice', 'Your account has been activated');
                    $form = new UserLoginForm;
                    $form->loginWoPassword($mail);
                }else if($record->status == User::STATUS_DELETED){
                    Yii::app()->user->setFlash('notice', 'Your account has been deleted');
                }else if($record->status == User::STATUS_ACTIVE){
                    Yii::app()->user->setFlash('notice', 'You already activated your account');
                }else{
                    Yii::app()->user->setFlash('notice', 'Wrong url');
                }
            }else{
                Yii::app()->user->setFlash('notice', 'Wrong url');
            }
        }else{
            Yii::app()->user->setFlash('notice', 'Wrong url');
        }
        $this->redirect(array('/user/home/login'));
    }

    public function actionRegister()
    {
        // if registration is disabled redirect to membership page
        if(!$this->isRegistrationEnabled()) {
            $this->redirect('/login');
        }
		
        
        $model = new UserRegisterForm();
        if(isset($_POST['UserRegisterForm'])){
          
            $model->attributes=$_POST['UserRegisterForm'];
            $register = new User('register');
            $register->attributes=$_POST['UserRegisterForm'];
            if($model->validate() && $model->register($register)){
                $this->redirect(array('/user/home/check'));
            }
        }
        elseif (isset($_POST['source']) && $_POST['source'] == 'landing-page') {
            $model->email = $_POST['email'];
            $model->password = $_POST['password'];
            $model->repeatPassword = $_POST['password'];
            $register = new User('register');
            $register->attributes = $model->attributes;
            if($model->validate() && $model->register($register)){
                $this->redirect(array('/user/home/check'));
            }
        }


        $this->render('register',array(
            'model'=>$model,
        ));
    }
    
    public function actionResend($email)
    {
        $user = User::model()->findByAttributes(array(
            'email'=>$email
        ));
        if ($user && $user->status == User::STATUS_INACTIVE) {
            if ($user->sendMessageActivate()) {
                Yii::app()->user->setFlash('notice', 'The activation email has been sent to your inbox');
            }
        }
        
        $this->redirect(array('/user/home/login'));
    }
    
    public function actionCheck()
    {
      
        $this->render('check');
    }

    public function actionLoginFacebook($token){
        $module = Yii::app()->getModule('facebook');
        $module->setAccessToken($token);
        $facebookUserId = $module->getUser();
//       var_dump($facebookUserId);
//       exit;
        //Check if user is logged to facebook
        if($facebookUserId > 0){
            $userFacebook = UserFacebook::model()->findByAttributes(array('facebook_user_id'=> $facebookUserId));
          
            //Check if user is already in database
            if($userFacebook !== null ){ 
               
                //Check if facebook user wasn't deleted. If was then "undelete" him.
                if($userFacebook->deleted == UserFacebook::DELETED){
                    $userFacebook->deleted = UserFacebook::ACTIVE;
                    $userFacebook->save();
                }
                $modelLogin = new UserIdentity($userFacebook->user->email, $userFacebook->user->password);
                $modelLogin->setId($userFacebook->user->user_id);
                Yii::app()->user->login($modelLogin,  UserIdentity::DURATION);
                Yii::app()->user->setState('access_token', $token);

                $this->redirect(array('/facebook/facebook/addAccount', 'token'=>$token, 'message'=>false));

            }
            //We need to create new user
            else{
               
                // if registration is disabled redirect to membership page
                if(!$this->isRegistrationEnabled()) {
                    $this->redirect('/membership');
                }
        
                $userInfo = $module->api('/me', null, true);
                $userModel = new User();
                $password = $userModel->generateRandomPassword();
                $passwordHashed = User::encode($password);
                $userModel->password = $passwordHashed;
                $userModel->repeatPassword = $passwordHashed;
                
                if(!isset($userInfo["email"])){
                    Yii::app()->user->setFlash('notice', 'You need to provide email access.');
                }
                $userModel->email = $userInfo["email"];
                $userModel->name = $userInfo["first_name"];
                $userModel->lastname = $userInfo["last_name"];
                $userModel->status = User::STATUS_ACTIVE;
                $userModel->type = User::USER_NORMAL;
                //We need add new facebook user
                if($userModel->save()){
                    
                    $userModel->sendMessageRegistrationFacebook($password);
                    $modelLogin = new UserIdentity($userModel->email, $userModel->password);
                    $modelLogin->setId($userModel->user_id);
                    Yii::app()->user->login($modelLogin,  UserIdentity::DURATION);
                    $this->redirect(array('/facebook/facebook/addAccount', 'token'=>$token));
                }

            }
        }else{
            Yii::app()->user->setFlash('notice', 'Something went wrong. Please login to facebook again.');
        }
        
        $this->redirect(array('/user/home/login'));
    }

    public function actionProfile(){
        if(!Yii::app()->user->isGuest){
            //$model = new UserProfileForm();
            $user = User::model()->findByPk(Yii::app()->user->id);
            $user->scenario = 'profile';
            
            /*$autoresponderGetresponse = $user->getRelated('autoresponderGetresponse');
            $autoresponderAweber = $user->getRelated('autoresponderAweber');
            $autoresponderIcontanct = $user->getRelated('autoresponderIcontact');
            $autoresponderInfusionsoft = $user->getRelated('autoresponderInfusionsoft');
            $autoresponderMailchimp = $user->getRelated('autoresponderMailchimp');
            $autoresponderSendreach = $user->getRelated('autoresponderSendreach');


            //Get instance for GetResponse
            if($autoresponderGetresponse == null){ 
                $autoresponderGetresponse = new AutoresponderGetresponse();
                $autoresponderGetresponse->user_user_id = $user->user_id;
            }

            //Get instance  for Aweber
            if($autoresponderAweber == null){ 
                $autoresponderAweber = new AutoresponderAweber();
                $autoresponderAweber->user_user_id = $user->user_id;
            }

            //Get instance  for Autoresponder Icontact
            if($autoresponderIcontanct == null){ 
                $autoresponderIcontanct = new AutoresponderIcontact();
                $autoresponderIcontanct->user_user_id = $user->user_id;
            }


            //Get instance  for Infusionsoft
            if($autoresponderInfusionsoft == null){ 
                $autoresponderInfusionsoft = new AutoresponderInfusionsoft();
                $autoresponderInfusionsoft->user_user_id = $user->user_id;
            }

            //Get instance  for Mailchimp
            if($autoresponderMailchimp == null){ 
                $autoresponderMailchimp = new AutoresponderMailchimp();
                $autoresponderMailchimp->user_user_id = $user->user_id;
            }

            //Get instance  for Aweber
            if($autoresponderSendreach == null){ 
                $autoresponderSendreach = new AutoresponderSendreach();
                $autoresponderSendreach->user_user_id = $user->user_id;
            }*/
            
            $user->password = '';
            $user->repeatPassword = '';

            if(isset($_POST['User'])){
                
                $user->attributes=$_POST['User'];
                if ($user->validate()) {
                    if (strlen($user->password) <= 0) {
                        $oldUser = User::model()->findByPk(Yii::app()->user->id);
                        $user->password = $oldUser->password;
                        $user->repeatPassword = $oldUser->password;
                    }
                    else {
                        $user->password = User::encode($user->password);
                        $user->repeatPassword = $user->password;
                    }
                    if ($user->update()) {
                        Yii::app()->user->setFlash('notice', 'Your profile has been updated');
                    }
                }

                /*//Getresponse
                $autoresponderGetresponse->attributes=$_POST['AutoresponderGetresponse'];
                $autoresponderGetresponse->user_user_id = $user->user_id;
                if($autoresponderGetresponse->validate() ){
                    $autoresponderGetresponse->isNewRecord?$autoresponderGetresponse->save():$autoresponderGetresponse->update();

                }

                //Aweber
                $oldAccessCode = $autoresponderAweber->access_code ;
                $autoresponderAweber->attributes=$_POST['AutoresponderAweber'];
                $autoresponderAweber->user_user_id = $user->user_id;
                if($autoresponderAweber->validate() ){
                    if($autoresponderAweber->isNewRecord){
                        $autoresponderAweber->save();
                    }else{
                        $autoresponderAweber->update();
                    }
                    if($oldAccessCode != $autoresponderAweber->access_code){
                        $autoresponderAweber->connect();
                    }

                }

                //Icontact
                $autoresponderIcontanct->attributes=$_POST['AutoresponderIcontact'];
                $autoresponderIcontanct->user_user_id = $user->user_id;
                if($autoresponderIcontanct->validate() ){

                    $autoresponderIcontanct->save(false);

                }

                //Aweber
                $autoresponderInfusionsoft->attributes=$_POST['AutoresponderInfusionsoft'];
                $autoresponderInfusionsoft->user_user_id = $user->user_id;
                if($autoresponderInfusionsoft->validate() ){
                    $autoresponderInfusionsoft->isNewRecord?$autoresponderInfusionsoft->save():$autoresponderInfusionsoft->update();

                }

                //MailChimp
                $autoresponderMailchimp->attributes=$_POST['AutoresponderMailchimp'];
                $autoresponderMailchimp->user_user_id = $user->user_id;
                if($autoresponderMailchimp->validate() ){
                    $autoresponderMailchimp->isNewRecord?$autoresponderMailchimp->save():$autoresponderMailchimp->update();

                }

                //SendReach
                $autoresponderSendreach->attributes=$_POST['AutoresponderSendreach'];
                $autoresponderSendreach->user_user_id = $user->user_id;
                if($autoresponderSendreach->validate() ){
                    $autoresponderSendreach->isNewRecord?$autoresponderSendreach->save():$autoresponderSendreach->update();

                }*/
            }
            
            $user->password = '';
            $user->repeatPassword = '';
            
            $this->render('profile', array(
                //'model'=>$model,
                'user'=>$user,
                /*'getResponse'=>$autoresponderGetresponse,
                'aweber'=>$autoresponderAweber,
                'icontact'=>$autoresponderIcontanct,
                'infusionsoft'=>$autoresponderInfusionsoft,
                'mailchimp'=>$autoresponderMailchimp,
                'aweber'=>$autoresponderAweber,
                'sendreach'=>$autoresponderSendreach,*/
            ));
        }
        else{
            $this->redirect(array('/user/home/login'));
        }

    }

    public function actionChangePassword(){
        if(!Yii::app()->user->isGuest){
            $model = new UserResetPasswordForm();
            $user = User::model()->findByPk(Yii::app()->user->id);
            if(isset($_POST['UserResetPasswordForm'])){
                $model->attributes=$_POST['UserResetPasswordForm'];
                if($model->validate() && $model->update($user)){
                    Yii::app()->user->setFlash('notice', 'You password have been updated');
                }
            }
            $this->render('changepassword', array(
                'model'=>$model
            ));
        }else{
            $this->redirect(array('/user/home/login'));
        }

    }        
}
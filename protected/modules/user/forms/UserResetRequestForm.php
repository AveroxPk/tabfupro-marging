<?php

class UserResetRequestForm extends CFormModel
{
    
	public $email;

	/**
	 * Declares the validation rules.
	 * The rules state that email and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('email', 'required'),
                        array('email', 'email')
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'email'=>"Email",
		);
	}

    /**
     * Requests password reset link for specific user (basing on provided e-mail address)
     * @return boolean true if form input validation and reset request was successfull
     */
    public function request()
    {
        if ($this->validate()) {
            if (($model = User::model()->findByAttributes(array('email' => $this->email))) === null) {
                $this->addError('email', "Provided email doesn't exist.");
                return false;
            }
            
            if($model->status == User::STATUS_INACTIVE){
                $this->addError('email', 'Your account is inactive<br/><a href="'.CHtml::normalizeUrl(array('/user/home/resend', 'email'=>$this->email)).'">Resend the activation email</a>');
                return false;
            }
            if($model->status == User::STATUS_DELETED){
                $this->addError('email', "Your account have been deleted");
                return false;
            }
            if((strtotime("now") - $model->resetdate) < User::TIME_FOR_RESET_PASS && $model->resetkey !== null){
                $this->addError('email', "You already requested reset password");
                return false;
            }
            if (!$model->resetPassword()) {
                $this->addError('email', "Internal server error. Please try again later.");
                return false;
            }
            
            return true;
        }
        return false;
    }

}

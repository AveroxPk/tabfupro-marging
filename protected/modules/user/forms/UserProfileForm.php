<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UserProfileForm extends CFormModel
{

    
	public $name;
        public $lastname;
        public $autoresponderGetresponse = array();
        
        

	/**
	 * Declares the validation rules.
	 * The rules state that email and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// email and password are required
			array('name, lastname', 'required'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			
                        'name'=>"Name",
                        'lastname'=>"Lastname",
                        
		);
	}
    public function update($model = null){
        if($model instanceof User){
            $model->attributes = $this->attributes;
            if($model->save()){
                return true;

            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
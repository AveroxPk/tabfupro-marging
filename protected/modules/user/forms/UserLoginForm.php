<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UserLoginForm extends CFormModel
{

    
	public $email;
	public $password;
	public $rememberMe;
        
        private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that email and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// email and password are required
			array('email, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>"Keep me logged in",
			'email'=>"Email",
			'password'=>"Password",
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->email,$this->password);
			if(!$this->_identity->authenticate()){
                if($this->_identity->errorCode == UserIdentity::ERROR_USERNAME_INACTIVE){
                    $this->addError('password','Account inactive. Please check your email.<br/><a href="'.CHtml::normalizeUrl(array('/user/home/resend', 'email'=>$this->email)).'">Resend the activation email</a>');
                }else{
                    $this->addError('password','Incorrect Email or password.');
                }
            }
                        			
		}
	}

	/**
	 * Logs in the user using the given email and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null) {
			$this->_identity=new UserIdentity($this->email, $this->password);
			$this->_identity->authenticate();
		}
		
        if($this->_identity->errorCode===UserIdentity::ERROR_NONE) {
			$duration=$this->rememberMe ? UserIdentity::DURATION:0;
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
        else {
            return false;
        }
	}
    
    public function loginWoPassword($email)
    {
        $this->_identity = new UserIdentity($email, '');
        $record = User::model()->findByAttributes(array('email'=>$email));
        $this->_identity->id = $record->user_id;
        $this->_identity->setState('status', $record->status);
        $this->_identity->errorCode = UserIdentity::ERROR_NONE;
        Yii::app()->user->login($this->_identity, 0);
    }
}

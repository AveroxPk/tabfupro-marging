<?php
class UserResetPasswordForm extends CFormModel
{
    public $newPassword;
    public $passwordVerify;
    public $userId;
    
    public function rules()
    {
        return array(
			array('newPassword, passwordVerify', 'required'),
			array('newPassword, passwordVerify', 'length', 'max'=>128, 'min' => 4,'message' => "Incorrect password (minimal length 4 symbols)."),
			array('passwordVerify', 'compare', 'compareAttribute'=>'newPassword', 'message' => "Retype Password is incorrect."),
		);
    }
    
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'newPassword' => 'New password',
			'passwordVerify' => 'Retype Password',
		);
	}
    
        public function reset($email=null, $hash=null)
        {

            if($email !== null && $hash !== null){
                $user = User::model()->findByAttributes(array(
                    'email'=>$email,
                    'resetkey'=>$hash,
                ));
                if($user !== null){
                    if(strtotime("now")- $user->resetdate < User::TIME_FOR_RESET_PASS){
                        $user->password = User::encode($this->newPassword) ;
                        $user->resetkey = null;
                        $user->status = User::STATUS_ACTIVE;
                        return $user->save() ? TRUE : FALSE;
                    }else{
                        $this->addError('newPassword', 'You reset token have expired. Please try again.');
                        return false;
                    }

                }else{
                    //Yii::app()->request->redirect(array('/user/home/login'));
                    return false;
                }

            }else{
                return false;
            }
        }
        
        public function update($user = null)
        {

            if($user != null && $user instanceof User && $this->validate()){
                    $user->password = User::encode($this->newPassword) ;
                    return $user->save() ? TRUE : FALSE;
                    

                }else{
                    return false;
                }
        }
}
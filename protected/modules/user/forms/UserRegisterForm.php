<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UserRegisterForm extends CFormModel
{

    
	public $email;
	public $password;
        public $repeatPassword;
	public $name;
        public $lastname;
        
        

	/**
	 * Declares the validation rules.
	 * The rules state that email and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// email and password are required
			array('email, password, repeatPassword', 'required'),
                        array('password, repeatPassword', 'length', 'max'=>128, 'min' => 4, 'message' => "Incorrect password (minimal length 4 symbols)."),
                        array('repeatPassword', 'compare', 'compareAttribute'=>'password', 'message' => "Passwords do not match"),
			
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			
			'email'=>"Email",
			'password'=>"Password",
                        'repeatPassword'=>"Repeat Password",
                        'name'=>"Name",
                        'lastname'=>"Lastname",
                        
		);
	}
        
        
        
        public function register($model = null){
            if($model instanceof User){
                if($model->isUniqueUser()){
                    $model->password = $model->repeatPassword = User::encode($model->password);
                    $model->status = User::STATUS_INACTIVE;
                    $model->package = User::PACKAGE;
                    if($model->save()){
                        if($model->sendMessageActivate()){
                            return true;
                        }else{
                            return true;
                           // $model->delete();
                        }
                        
                    }else{
                        $this->addError('email',  'Please try again');
                    }
                }else{
                    $this->addError('email','User already exists');
                }
            }else{
                $this->addError('email','Not instance');
            }
            
            
        }
}
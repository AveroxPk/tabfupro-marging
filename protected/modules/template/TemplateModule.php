<?php

class TemplateModule extends CWebModule
{
    private $_repositoryAdapter;
    public $repositoryAdapterConfig = array(
        'class' => 'LocalRepositoryAdapter'
    );
    
	public function init()
	{ 
        
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
        
        // Zend Loader setup
        include Yii::app()->basePath. '/vendor/ZendFramework-2.3.1/library/Zend/Loader/StandardAutoloader.php';
        $autoloader = new Zend\Loader\StandardAutoloader(array('autoregister_zf' => TRUE));
        $autoloader->registerNamespace('Procreative_Validate_', Yii::app()->basePath . '/vendor/Procreative/Validate');
        spl_autoload_register(array($autoloader, 'autoload'));


        
		// import the module-level models and components
		$this->setImport(array(
			'facebook.models.*',
			'facebook.components.*',
			'template.models.*',
			'template.models.exception.*',
			'template.models.activeRecord.*',
			'template.models.page.*',
			'template.models.page.exception.*',
			'template.models.page.asset.*',
			'template.models.page.asset.repositoryAdapter.*',
			'template.models.page.asset.repositoryAdapter.exception.*',
			'template.models.field.*',
			'template.models.field.exception.*',
			'template.models.field.attribute.*',
			'template.models.field.attribute.exception.*',
			'template.models.field.attribute.dataType.*',
			'template.models.field.attribute.dataType.exception.*',
			'template.components.*',
                        'analytics.components.*',
                        'analytics.models.*'
			
		));
        
	}
    
    /**
     * 
     * @return AbstractRepositoryAdapter
     */
    public function getRepositoryAdapter()
    {
        if ($this->_repositoryAdapter === null) {
            $repositoryAdapterClass = $this->repositoryAdapterConfig['class'];
            unset($this->repositoryAdapterConfig['class']);
            $this->_repositoryAdapter = new $repositoryAdapterClass($this->repositoryAdapterConfig);
        }
		
        return $this->_repositoryAdapter;
    }

	public function beforeControllerAction($controller, $action)
	{



		if (parent::beforeControllerAction($controller, $action)) {
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		
        return false;
	}
}

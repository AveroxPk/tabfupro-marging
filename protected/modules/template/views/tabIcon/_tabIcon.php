<?php 
    if (($imgUrl = $data->getImageUrl())):
?>
    <?php if($data->user_id !== null): ?>
        <div class="tab-icon user-icon" data-id="<?=$data->id?>" data-filename="<?= $data->original_filename ?>">
            <div class="remove-button">x</div>
        <?=CHtml::image($imgUrl, '', array('class'=>'icon-image'));?>
    <?php else: ?>
        <div class="tab-icon system-icon" data-id="<?=$data->id?>" data-filename="<?= $data->original_filename ?>">
        <?=CHtml::image($imgUrl, '', array('class'=>'icon-image'));?>
    <?php endif; ?>
        
    </div>
<?php 
    endif;
<?php

    foreach ($this->page->getFields() as $field) {
        echo CHtml::tag('div', array(
            'class' => 'field-attributes',
            'data-field-id' => $field->getId(),
            'style' => 'display:none'
        ),'', false);

        if (!empty($field->getAttributes())) {

            foreach ($field->getAttributes() as $attributeName => $attribute) {
                if ($attribute->getHidden()) {
                    continue;
                }
                
                echo '<div>';
                    echo '<label>';
                        echo CHtml::encode($attribute->getLabel());
                    echo '</label><br/>';

                    if ($attribute->getCustomEditWidgetClass() == 'default') {
                        $widgetClassPath = 'application.modules.template.widgets.dataTypeEditors.standard.' . $attribute->getDataType()->getName() . 'Editor';
                    } else {
                        $widgetClassPath = 'application.modules.template.widgets.dataTypeEditors.custom.' . $attribute->getCustomEditWidgetClass();
                    }
                    $this->widget($widgetClassPath, array(
                        'name' => $attributeName,
                        'value' => $attribute->getValue(),
                    ));
                echo '</div>';
            }

        }
        echo '</div>';
    }

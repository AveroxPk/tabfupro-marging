<!doctype html>
<html>
    <head>

        <title>Facebook preview</title>

        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="/css/facebook-preview.css">

        <script type="text/javascript">
            function iframeLoaded() {
                var iFrameID = document.getElementById('fb_iframe');
                if(iFrameID) {
                      // here you can make the height, I delete it first, then I make it again
                      iFrameID.height = "";
                      iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
                }   
            }
        </script> 
    </head>
    <body>
        <div class="main">
            <div class="top">
                <div class="page-image">
                    <!--<img src="/images/facebook-preview/test.jpg" alt="" />-->
                </div>
                <div class="page-names-holder">
                    <div class="page-name">
                        <div class="left"></div>
                        <div class="middle">Facebook</div>
                        <div class="right"></div>
                    </div>
                    <div class="tab-name">
                        <div class="left">Preview</div>
                        <div class="right"></div>
                    </div>
                </div>
            </div>

            <div class="content">
                <?php /*<div class="tab-preview"></div>*/ ?>
                <iframe id="fb_iframe" border="0" src="<?= $pagePreviewUrl ?>" style="border:none" width="815px" height="800px" onload="iframeLoaded()"></iframe>                
            </div>

            <div class="bottom">

            </div>
        </div>
    </body>
</html>
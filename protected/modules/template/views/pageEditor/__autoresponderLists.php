<div class="select-a-list" style="display:none;">
    <div class="content">
       <ul>
           <?php 
               if (($lists = $model->getLists())) {
                   foreach ($lists as $listId => $listName) {
                       echo '<li>';
                           echo CHtml::link(
                               CHtml::encode($listName), 
                               '#', 
                               array(
                                   'class' => 'list-selection-position',
                                   'data-list-id' => $listId,
                                   'data-autoresponder-name' => get_class($model)
                               )
                           );
                       echo '</li>';
                   }
               }
           ?>
       </ul>
    </div>
</div>
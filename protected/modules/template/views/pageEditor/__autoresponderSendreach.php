
<div class="left"><span class="step-list">Step 1&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
<div class="right">
    <a href="http://portal.sendreach.com/" class="button-api api-credentials" title="Get your api credentials" target="_blank">
		<i class="fa fa-compress" aria-hidden="true"></i> Get your api credentials
	</a>
</div>

<div class="bottom">
    <div class="top">

        <div class="left"><span class="step-list">Step 2&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
        <div class="right">

            <!-- Normal inputes -->
            <form>
                <div class="input-long">
                    <div class="input-label"><i class="fa fa-id-card" aria-hidden="true"></i> User ID</div>
                    <div class="input-hold">
                        <?php echo CHtml::activeTextField($model, 'api_user_id', array(
                            'class' => 'input',
                            'placeholder' => 'SendReach User ID here...'
                        )) ?>
                    </div>
                </div>
                <div class="input-long">
                    <div class="input-label"><i class="icofont icofont-key"></i> App Key</div>
                    <div class="input-hold">
                        <?php echo CHtml::activeTextField($model, 'api_key', array(
                            'class' => 'input',
                            'placeholder' => 'SendReach API Key here...'
                        )) ?>
                    </div>
                </div>
                <div class="input-long">
                    <div class="input-label"><i class="fa fa-user-secret" aria-hidden="true"></i> App Secret</div>
                    <div class="input-hold">
                        <?php echo CHtml::activeTextField($model, 'api_secret', array(
                            'class' => 'input',
                            'placeholder' => 'SendReach App Secret here...'
                        )) ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
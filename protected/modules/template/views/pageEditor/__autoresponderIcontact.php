
<div class="left"><span class="step-list">Step 1&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
<div class="right">
    <a href="https://app.icontact.com/icp/core/registerapp" class="button-api api-credentials" title="Get your api credentials" target="_blank">
		<i class="fa fa-compress" aria-hidden="true"></i> Get your api credentials
	</a>
</div>

<div class="bottom">
    <div class="top">

        <div class="left"><span class="step-list">Step 2&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
        <div class="right">
            <form>
                <!-- Normal inputes -->
                <div class="input-long">
                    <div class="input-label"><i class="fa fa-user" aria-hidden="true"></i> Username</div>
                    <div class="input-hold">
                        <?php echo CHtml::activeTextField($model, 'app_login', array(
                            'class' => 'input',
                            'placeholder' => 'iContact Username here...'
                        )) ?>
                    </div>
                </div>
                <div class="input-long">
                    <div class="input-label"><i class="fa fa-ticket" aria-hidden="true"></i> App Pass</div>
                    <div class="input-hold">
                        <?php echo CHtml::activeTextField($model, 'app_pass', array(
                            'class' => 'input',
                            'placeholder' => 'iContact API Password here...'
                        )) ?>
                    </div>
                </div>
                <div class="input-long">
                    <div class="input-label"><i class="fa fa-id-card" aria-hidden="true"></i> App ID</div>
                    <div class="input-hold">
                        <?php echo CHtml::activeTextField($model, 'app_id', array(
                            'class' => 'input',
                            'placeholder' => 'iContact App ID here...'
                        )) ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="left"><span class="step-list">Step 1&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
<div class="right">
    <a href="https://signin.infusionsoft.com/app/central/home" class="button-api api-credentials" title="Get your api credentials">
		<i class="fa fa-compress" aria-hidden="true"></i> Get your api credentials
	</a>
</div>

<div class="bottom">
    <div class="top">

        <div class="left"><span class="step-list">Step 2&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
        <div class="right">
            <form>
                <!-- Normal inputes -->
                <div class="input-long">
                    <div class="input-label"><i class="fa fa-rocket" aria-hidden="true"></i> App Name</div>
                    <div class="input-hold">
                        <?php echo CHtml::activeTextField($model, 'app_name', array(
                            'class' => 'input',
                            'placeholder' => 'Infusionsoft App Name here...'
                        )) ?>
                    </div>
                </div>
                <div class="input-long">
                    <div class="input-label"><i class="icofont icofont-key"></i> App Key</div>
                    <div class="input-hold">
                        <?php echo CHtml::activeTextField($model, 'app_key', array(
                            'class' => 'input',
                            'placeholder' => 'Infustionsoft App Key here...'
                        )) ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
 <div class="crm-section-header">
     <div class="top">
         <a href="#" class="closs-button" title="Close"><i class="icofont icofont-power"></i></a>
		 
		<h3> CRM </h3>
		<span> Select and connect your Customer relation managment service </span>
     </div>
 </div>
  <div class="crm-header col-md-3">
     <div class="crm-options">
         <?php foreach ($crms as $crmKey => $crm): ?>
         <div class="crm-element" id="<?=$crmKey?>-btn">
             <div class="button">
                 <a href="#" class="button-crm <?=strtolower($crmKey) ?>" title="<?=$crm;?>" data-crm="<?=$crmKey?>"><?=$crm;?></a>
             </div>
         </div>
         <?php endforeach; ?>
     </div>
  </div>
  
  <div class="col-md-9">
 <?php 
 
    foreach ($crms as $crmName => $crm) {
        
        $configured = false;
        $crmViewData = [
            'index' => $crmName,
            'model' => $crm,
			'sf_id' => $sf_id,
            'connected' => false
        ];
		
        if (isset($connectedCrms[$crm])) {
			$crmViewData = [
				'index' => $crmName,
				'model' => $crm,
				'sf_id' => $sf_id,
				'connected' => true
			];
        }
        $this->renderPartial('_crm', $crmViewData);
    }
  ?>
  </div>
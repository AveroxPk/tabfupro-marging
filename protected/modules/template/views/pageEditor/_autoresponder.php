 <div class="ar-content" style="display:none;" id="<?=$arClass=get_class($model)?>">
    <div class="autoresponder-settings"<?php if ($connected) {echo ' style="display:none"';}?>>
        <div class="top">
            <div class="header">
               <span class="title">Connect to <?=$model->getDisplayName()?></span>
            </div>
            <div class="content">
                <?php
                   $this->renderPartial('__' . lcfirst($arClass), compact('model'));
                ?>
            </div>
        </div>
        <div class="bottom">
            
            
            <div class="footer">
                <div class="left">
                    <a href="#" class="close-window-button" title="Close window"><i class="fa fa-compress" aria-hidden="true"></i> Close window</a>
                </div>
                <div class="right">
                    <a href="#" class="button connect" data-autoresponder-name="<?=$arClass?>"><i class="icofont icofont-connection"></i> Connect</a>
                    <!-- <a href="#" class="button connect">Connect</a> -->
                </div>
            </div>
        </div> 
         
         
     </div>
     <div class="autoresponder-list-choice"<?php if (!$connected) {echo ' style="display:none"';}?>>
        <div class="top">
            <div class="header">
               <span class="title">Connect to <?=$model->getDisplayName()?></span>
            </div>
            <div class="content">
                <div class="middle">
                     <a href="#" class="choose-your-list-button">Choose your list </a>

                     <?php $this->renderPartial('__autoresponderLists', compact('model')); ?>

                </div>
            </div>


        </div>
        <div class="bottom">
            <div class="top">

                <div class="middle" style="/*display:none*/">
                    <div class="big-title">
                        <input type="hidden" class="ar-list-name-value" id="<?=$arClass?>-list-name-value" value="<?=$selectedListId?>" />
                        <span id="<?=$arClass?>-list-name" class="ar-list-name"><?=CHtml::encode($selectedListName)?></span>
						<span class="big-ticked" id="<?=$arClass?>-list-tick"<?=$selectedListName ? '' : ' style="display:none"'?>><i class="fa fa-check" aria-hidden="true"></i>
</span>
                    </div>
                </div>

            </div>
            <div class="footer">
                <div class="left">
                    <a href="#" class="close-window-button" title="Close window"><i class="fa fa-compress" aria-hidden="true"></i> Close window</a>
                </div>
                <div class="right">
                    <a href="#" class="button disconnect" data-autoresponder-name="<?=$arClass?>"><i class="fa fa-chain-broken" aria-hidden="true"></i> Disconnect</a>
                </div>
            </div>
        </div>
     </div>  
     
     <div class="autoresponder-modal warning-modal">
        <div class="autoresponder-modal-inner warning-modal-inner">
            <p>
                Disconnecting this Autoresponder will also stop any existing posts from sending new contacts to it's lists...
                <br/><br/>Are you sure?
            </p>
            <a class="button-tf-yes" href="#">Yes</a>
            <a class="button-tf-cancel" href="#">Cancel</a>
        </div>
    </div>

 </div>
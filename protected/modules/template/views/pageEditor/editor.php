<?php

/**
 * @var Page $page
 */
Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerLinkTag(
        'stylesheet', 
        'text/css', 
        Yii::app()->getBaseUrl(true) . '/css/template.editor.iframe.css', 
        null, 
        array(
            'id' => 'iframe-style'
        )
    )
    ->registerScriptFile(Yii::app()->getBaseUrl() . '/js/template.editor.js')
;
?>



<div id="page-editor" data-page-id="<?php echo $page->getId(); ?>">
    <div id="page-preview" style="float:left;">
        <?php 
            echo CHtml::tag(
                'iframe',
                array(
                    'id' => 'page-preview-frame',
                    'src' => CHtml::normalizeUrl(array('pagePreview', 'id' => $page->getId())),
                    'onload' => 'initIframe()',
                    'style' => 'display: none; width: 750px; height: 800px;'
                ),
                '&nbsp;'
            ); 
        ?>
    </div>
    <div id="editor-sidebar">
        <?php
        
            foreach ($page->getFields() as $field) {
                echo CHtml::tag('div', array(
                    'class' => 'field-attributes',
                    'data-field-id' => $field->getId(),
                    'style' => 'display:none'
                ));

                if (!empty($field->getAttributes())) {
                    
                    foreach ($field->getAttributes() as $attributeName => $attribute) {
                        echo '<div>';
                            echo '<label>';
                                echo CHtml::encode($attribute->getLabel());
                            echo '</label>';

                            if ($attribute->getCustomEditWidgetClass() == 'default') {
                                $widgetClassPath = 'application.modules.template.widgets.dataTypeEditors.standard.' . $attribute->getDataType()->getName() . 'Editor';
                            } else {
                                $widgetClassPath = 'application.modules.template.widgets.dataTypeEditors.custom.' . $attribute->getCustomEditWidgetClass();
                            }
                            $this->widget($widgetClassPath, array(
                                'name' => $attributeName,
                                'value' => $attribute->getValue(),
                            ));
                        echo '</div>';
                    }
                    
                }
                echo '</div>';
            }
            echo CHtml::button('Save changes', array('id' => 'button-save-changes'));
            
        ?>
    </div>
</div>

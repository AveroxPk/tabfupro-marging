
<div class="left"><span class="step-list">Step 1&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
<div class="right">
    <a href="https://us8.admin.mailchimp.com/account/api/" class="button-api api-key" title="Get your api key" target="_blank">
		<i class="fa fa-compress" aria-hidden="true"></i> Get your api key
	</a>
</div>

<div class="bottom">
    <div class="top">

        <div class="left"><span class="step-list">Step 2&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
        <div class="right">
            <form>
                <div class="input-textarea">
                    <div class="input-label">MailChimp API</div>
                    <div class="input-hold">      
                        <?php echo CHtml::activeTextArea($model, 'api_key', array(
                            'class' => 'input-textarea-edit',
                            'placeholder' => 'Paste in your MailChimp API Key here...'
                        )); ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
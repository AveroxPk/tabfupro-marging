<?php
// Configuration 
		$sf_config = Yii::app()->params['salesforce'];
		
		$client_id =  $sf_config['client_id'];
		$redirect_uri =  $sf_config['redirect_uri'];
		$login_uri =  $sf_config['login_uri'];


// oauth URL

	$auth_url = $login_uri
			. "/services/oauth2/authorize?response_type=code&client_id="
			. $client_id . "&redirect_uri=" . urlencode($redirect_uri);

?>
<script>
		
	$(document).ready(function(){
		$('#login').click(function(e) {
			e.preventDefault();
			var ref = window.open('<?=$auth_url;?>', 
								  '_blank', 'location=no,toolbar=no');
								  
			window.CallParent = function(id) {
				ref.close();
				$('#login').hide();
				$("#notice").hide();
				$(".big-title.middle").show();
				$("#message").show();
				$("#sf-account-id").val(id);				
				$('.button.connect').addClass('active');				
			}
		});
	});
		
		
</script>
<div class="left"><span class="step-list">Step 1&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
<div class="right">

	<div class="big-title middle connect-tick" style="padding-top:10px;<?=($sf_id && $sf_id > 0) ? 'display:block;' : 'display:none;';?>">
		<span class="small-ticked" ><i class="fa fa-check" aria-hidden="true"></i></span>
	</div>
	
	<a href="javascript:void(0);" id="login" style="<?=($sf_id && $sf_id > 0) ? 'display:none;':'display:block;';?>" class="button-api auth-code" title="Connect SalesForce Account With Tabfu" >
		<i class="fa fa-plug" aria-hidden="true"></i> Connect SalesForce With Tabfu
	</a>
<?php 
		
?>
</div>

<div class="bottom">
    <div class="top">

        <div class="left"><span class="step-list">Step 2&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
        <div class="right" style="border:1px solid #f5f5f5; border-radius:10px;">
			<?php 
				if($sf_id && $sf_id > 0){
				echo '<p id="message" style="color:#000; margin:10px;">
					Click Connect Button to attach your salesforce account with this Tab.	
				</p>';
					
				}else{
			?>
				<p id="notice" style="color:#000; margin:10px">
					Once Your SalesForce Account connected Successfully with Tabfu then you can attach it with current Tab.	
				</p>
				<p id="message" style="color:green; margin:10px; display:none;">
					Congradulations:: Your SalesForce Account Successfully Connected with Tabfu, Now click on Connect Button to attach this account with current Tab.	
				</p>
			<?php 
				}
			?>
			<form>
			<input type="hidden" name="<?=$model;?>" id="sf-account-id" value="<?=$sf_id;?>">
			</form>
        </div>
    </div>
</div>
 <div class="crm-content" style="display:none;" id="<?=$crmClass=$index?>">
    <div class="crm-settings"<?php if ($connected) {echo ' style="display:none"';}?>>
        <div class="top">
            <div class="header">
               <span class="title">Connect to <?=$model;?></span>
            </div>
            <div class="content">
                <?php
                   $this->renderPartial('__crm' . ucfirst($crmClass), compact('model','sf_id'));
                ?>
            </div>
        </div>
		
		<div class="bottom">
			<div class="footer">
				<div class="left">
					<a href="#" class="close-window-button" title="Close window"><i class="fa fa-compress" aria-hidden="true"></i> Close window</a>
				</div>
				<div class="right">
					<a href="#" class="button connect" id="<?=$model?>" data-crm-name="<?=$model?>"><i class="icofont icofont-connection"></i> Connect</a>
				</div>
			</div>
		</div> 
     </div>
	 

	 
     <div class="crm-list-choice"<?php if (!$connected) {echo ' style="display:none"';}?>>
        <div class="top">
            <div class="header">
               <span class="title">Connect to <?=$model?></span>
            </div>
            <div class="content">
                <div class="middle">
                     <!--<a href="#" class="choose-your-list-button">Choose your list </a>-->

                     <?php// $this->renderPartial('__autoresponderLists', compact('model')); ?>

                </div>
            </div>


        </div>
        <div class="bottom">
            <div class="top">
                <div class="middle" style="/*display:none*/">
                    <div class="big-title">
                        <span id="<?=$crmClass?>-list-name" class="crm-list-name"><? //CHtml::encode($selectedListName)?></span>
						<span class="big-ticked" id="<?=$crmClass?>-list-tick"<?//$selectedListName ? '' : ' style="display:none"'?>>
							<i class="fa fa-check" aria-hidden="true"></i>
						</span>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="left">
                    <a href="#" class="close-window-button" title="Close window"><i class="fa fa-compress" aria-hidden="true"></i> Close window</a>
                </div>
                <div class="right">
                    <a href="#" class="button disconnect" data-crm-name="<?=$model?>"><i class="fa fa-chain-broken" aria-hidden="true"></i> Disconnect</a>
                </div>
            </div>
        </div>
     </div>  
     
     <div class="crm-modal warning-modal">
        <div class="crm-modal-inner warning-modal-inner">
            <p>
                Disconnecting this CRM will also stop any existing posts from sending new contacts to it's lists...
                <br/><br/>Are you sure?
            </p>
            <a class="button-tf-yes" href="#">Yes</a>
            <a class="button-tf-cancel" href="#">Cancel</a>
        </div>
    </div>

 </div>
 
 
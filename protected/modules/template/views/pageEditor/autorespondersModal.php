<div class="ar-section-header">
     <div class="top">
         <a href="#" class="closs-button" title="Close"><i class="icofont icofont-power"></i></a>
		 
		<h3> Autoresponders </h3>
		<span> Select and connect your Autoresponder service </span>
	
     </div>
</div>
 
 <div class="ar-header col-md-3">
	  <div class="ar-options">
		 <?php foreach ($autoresponders as $autoresponderClassName => $autoresponder): ?>
		 <div class="ar-element" id="<?=$autoresponderClassName?>-btn">
			 <div class="button">
				 <a href="#" class="button-ar <?=substr(strtolower($autoresponderClassName), 13) ?>" title="<?=$autoresponder->getDisplayName()?>" data-autoresponder="<?=$autoresponderClassName?>"><?=$autoresponder->getDisplayName()?></a>
			 </div>
<!--			 <div class="eye"></div> -->
		 </div>
		 <?php endforeach; ?>
	 </div>
</div>
<div class="col-md-9">
 <?php 
    foreach ($autoresponders as $autoresponderName => $autoresponder) {
        
        $configured = false;
        if (!$autoresponder->isNewRecord) {
            //Autoresponder must have at least 1 config attribute not empty to 
            //be treated as configured
            foreach ($autoresponder->attributes as $attributeName => $value) {
                if ($attributeName != 'user_user_id' && !empty($value)) {
                    $configured = true;
                    break;
                }
            }
        }
        
        $autoresponderViewData = [
            'model' => $autoresponder,
            'selectedListId' => '',
            'selectedListName' => '',
            'connected' => $configured
        ];
        if (isset($connectedAutoresponders[$autoresponderName])) {
            $autoresponderViewData['selectedListId'] = $connectedAutoresponders[$autoresponderName][0];
            $autoresponderViewData['selectedListName'] = $connectedAutoresponders[$autoresponderName][1];
        }
        
        $this->renderPartial('_autoresponder', $autoresponderViewData);
    }
  ?>
  </div>
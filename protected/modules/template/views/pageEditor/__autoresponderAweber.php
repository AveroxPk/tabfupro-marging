
<div class="left"><span class="step-list">Step 1&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
<div class="right">
    <a href="https://auth.aweber.com/1.0/oauth/authorize_app/25442493" class="button-api auth-code" title="Get your access code" target="_blank">
		<i class="fa fa-compress" aria-hidden="true"></i> Get your access code
	</a>
</div>

<div class="bottom">
    <div class="top">

        <div class="left"><span class="step-list">Step 2&nbsp;&nbsp;<i class="icofont icofont-hand-right"></i></span></div>
        <div class="right">
            <!-- Text area input -->
            <form>
                <div class="input-textarea">
                    <div class="input-label">AWeber authorization code</div>
                    <div class="input-hold">
                        <?php echo CHtml::activeTextArea($model, 'access_code', array(
                            'class' => 'input-textarea-edit',
                            'placeholder' => 'Paste in your AWeber authorization code here...'
                        )) ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
foreach (array(
    'temp-01' => '00ba99',
    'temp-02' => 'fff',
    'temp-03' => 'e96c3e',
    'temp-04' => 'fff',
    'temp-05' => '00ba99'
) as $id => $color) {
    echo FieldPlaceholder::createImportField(
        'styleColor',
        array(
            'background' => 1,
            'selector' => 'section#' . $id,
            'adminTag' => "{$id}-background",
            'color' => $color
        )
    );
    echo '<br/>';
}

echo FieldPlaceholder::createImportField(
    'styleColor',
    array(
        'background' => 1,
        'selector' => 'footer#footer',
        'adminTag' => "footer-background",
        'color' => '424242'
    )
);
/*die(
);

Yii::app()->getModule('facebook');
$fm = new FieldManager($tab = UserTabs::model()->findByAttributes(array('id' => 152)));
$pq = new PageQuery();
$page = $pq->getPageFromRepositoryByManagingTab($tab);

try {
    $fm->updateFieldInRepository($page->getField(780), array('color' => '#ff0000f'));
}catch (AttributeValidationException $e) {
    var_dump($e->getValidationErrors());
}*/
//We'll test new tab creation functionality
/*$this->widget('template.widgets.tabWizard.TabWizard');
Yii::app()->clientScript->registerScript('tests', <<<JS
    tabWizard.newTab();
JS
);*/
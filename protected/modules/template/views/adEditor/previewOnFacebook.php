<!doctype html>
<html>
    <head>

        <title>Facebook preview</title>

        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="/css/facebook-preview.css">
    </head>
    <body>
        <div class="main ad-preview">
            <div class="top">
                <div class="page-image">
                    <!--<img src="/images/facebook-preview/test.jpg" alt="" />-->
                </div>
                <div class="page-names-holder">
                    <div class="page-name">
                        <div class="left"></div>
                        <div class="middle">Facebook</div>
                        <div class="right"></div>
                    </div>
                    <div class="tab-name">
                        <div class="left">Preview</div>
                        <div class="right"></div>
                    </div>
                </div>
            </div>
            
            <div class="fake-before"></div>

            <div class="content">
                <div class="tab-preview">
                    <img src="<?= $pagePreviewUrl ?>" alt="Loading...">
                </div>
            </div>
            <div class="content-end"></div>
            
            <div class="fake-after"></div>

            <div class="bottom">

            </div>
        </div>
    </body>
</html>
<?php
    $form = $this->beginWidget('CActiveForm', array(
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
        )
    )); 
?>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <?php echo $form->labelEx($model, 'templateName'); ?>
    <?php echo $form->textField($model, 'templateName'); ?>
    <?php echo $form->error($model, 'templateName'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model, 'zipFile'); ?>
    <?php echo $form->fileField($model, 'zipFile'); ?>
    <?php echo $form->error($model, 'zipFile'); ?>
</div>

<?php echo CHtml::submitButton('Import'); ?>

<?php $this->endWidget();
<?php

class AdEditorController extends Controller
{
    /**
     *
     * @var Page
     */
    public $page;
    
    /**
     *
     * @var UserAds
     */
    public $tab;
    
    public function init()
    { 
        Yii::import('application.modules.facebook.models.*');
        
        if (!isset($_GET['tabId'])) {
            throw new CHttpException(500, 'No tab specified');
        }
        
        //try {
            $this->tab = UserAds::model()->findByAttributes(array('id' => $_GET['tabId']));
            if ($this->tab === null) {
                throw new CException("Tab {$_GET['tabId']} doesn't exist.");
            }
            
            $pageModel = AdPage::model()->findByAttributes(array(
                'user_ads_id' => $_GET['tabId']
            ));
            if ($pageModel === null) {
                throw new CException("Page for this tab {$_GET['tabId']} doesn't exist.");
            }
            $pageId = $pageModel->id;
            $query = new AdPageQuery();
            $this->page = $query->getPageFromRepositoryByManagingTab($this->tab);
            
        /*} catch (Exception $e) {
            throw new CHttpException(404, 'Page not found.');
        }*/

    }
    
    public function actionPagePreview($adminMode = true, $download = false)
    {
        $renderer = new PageRenderer($this->tab);
        $renderer->adminMode = $adminMode;
        $renderer->params = array(
            'gatesConfig' => $this->page->getGatesConfig(),
            'tabId' => $this->tab->id
        );
        $pageContent = $renderer->renderPage($this->page);
        
        if ($download) {
			
            Yii::app()->request->sendFile('index.html', $pageContent, 'text/html');
            exit;
        }
        
        echo $pageContent;
        
    }
    
    public function actionPreviewOnFacebook($tabId)
    {
        $this->renderPartial('previewOnFacebook', array(
            'pagePreviewUrl' => CHtml::normalizeUrl(array('/template/adEditor/renderAd/tabId/' . $this->tab->id))
        ));
    }
    
    public function actionPageInfo()
    {
        $dynamicContent = false;
        $fields = $this->page->getFields();
        foreach ($fields as $field) {
            if (!$field->isCacheable()) {
                $dynamicContent = true;
                break;
            }
        }
        
        //Determine connected autoresponders and their lists
        Yii::app()->getModule('autoresponders');
        $autoresponderNamesList = [];
        $autoresponderListsList = [];
        
        //Determine enabled gates
        $enabledGatesList = [];
        
            
        echo json_encode(array(
            'changesUnpublished' => $this->tab->published && $this->page->isMofified(),
            'tabPublished' => (bool)$this->tab->published,
            'facebookPageId' => $this->tab->userFanpage->facebook_page_id,
            'tabIconUrl' => $this->tab->tabIconUrl,
            'tabName' => $this->tab->name,
            'dynamicContent' => $dynamicContent,
            'gatesConfig' => $this->page->getGatesConfig(),
            
            //Autoresponder info in publish tab
            'autoresponders' => implode(', ', $autoresponderNamesList),
            'autorespondersLists' => implode(', ', $autoresponderListsList),
            'enabledGates' => implode(', ', $enabledGatesList)
            
        ));
    }
    
    public function actionUpdateField($tabId, $fieldId)
    {
        
        $fieldManager = new AdFieldManager($this->tab);
        $pageField = $this->page->getField($fieldId);
        
        $renderer = new PageRenderer;
        $renderer->adminMode = true;
        
        //TODO: move below function into some helper class
        /**
         * Converts Zend Validation errors list into nicely formatted string 
         * like as follows:
         * * error message 1
         * * error message 2
         * @return string
         */
        function formatErrorList(array $errorList)
        {
            $errorListString = '';
            
            //Flatten array to get "leaf" values which are error messages
            $errorMessagesIter = new RecursiveIteratorIterator(new RecursiveArrayIterator($errorList));
            foreach ($errorMessagesIter as $errorMessage) {
                $errorListString .= "* $errorMessage\n"; //Decorate error message
            }
            
            return $errorListString;
        }
        
        try {
            $updatedField = $fieldManager->updateFieldInRepository($pageField, $_POST);
            echo CJSON::encode(array(
                'error' => 0,
                'content' => $renderer->renderField($updatedField)
            ));
        } catch (AttributeValidationException $ex) {
            echo CJSON::encode(array(
                'error' => 1,
                'errorMessage' => formatErrorList($ex->getValidationErrors())
            ));
        }
        
        
    }

    public function actionUploadImage()
    {
        $file = CUploadedFile::getInstanceByName('file');
        
        $model = new AdPageFile;
        $model->original_filename = $file->name;
        $model->server_filename = md5($this->tab->id . $file->name . time()) . '.' . $file->getExtensionName();
        $model->page_id = $this->page->getId();
        
        //Enable only wellknown image file extensions
        if (!in_array(strtolower($file->getExtensionName()), array(
            'jpg',
            'jpeg',
            'gif',
            'png'
        ))) {
            exit('error');
        }
        
        if ($file->saveAs(Yii::app()->getModule('template')->repositoryAdapterConfig['pattern']['path'] . $model->server_filename)) {
            $model->save(false);
            
            $assetQuery = new AdAssetQuery(Yii::app()->getModule('template')->getRepositoryAdapter());
            exit(json_encode($assetQuery->getAssetByServerFilename($model->server_filename)));
        }
        
        exit('error');
        
    }
    
    public function actionPublish()
    {
        $pageManager = new PageManager($this->tab);
        
        $renderer = new PageRenderer();
        $renderer->adminMode = false;

        //Unfortunately we can't get things like Facebook tab URL at this stage
        //so the widgets which uses renderer params should be NOT CACHEABLE
        $renderer->params = array(); 
        
        try {
            //$this->tab->publishOnFB();
            $pageManager->publishLastPageVersion($this->page, $renderer);   
            
            echo 'OK';
        } catch (Exception $e) {
            echo 'ERROR';
        }
    }
    
    public function actionPageSetFanpage($tabId, $fanpageId)
    {
        //Security checks
        $fanpage = UserFanpage::model()->findByAttributes(array(
            'facebook_page_id' => $fanpageId,
            'user_facebook_id' => Yii::app()->user->u_f_id
        ));
        if ($fanpage === null) {
            throw new CHttpException(404, 'Page not found.');
        }
        
        //Update Tab's fanpage
        $this->tab->user_fanpage_id = $fanpage->id;
        exit($this->tab->save() ? 'OK' : 'ERROR');
        
    }
    
    public function actionRenderHtml()
    {
        $renderer = new PageRenderer($this->tab);
        $renderer->adminMode = false;
        $renderer->params = array(
            'gatesConfig' => $this->page->getGatesConfig(),
            'tabId' => $this->tab->id
        );
        echo $renderer->renderPage($this->page);
    }
    
    public function actionRenderAd($download = false)
    { 
        $dir = '/facead_resources/' . $this->tab->id;
        $fileUrl = $dir . '/rendered.jpg';
        $filePath = Yii::getPathOfAlias('webroot') . $fileUrl;
        if (!file_exists(Yii::getPathOfAlias('webroot') . $dir)) {
            mkdir(Yii::getPathOfAlias('webroot') . $dir);
        }
        system('wkhtmltoimage ' . Yii::app()->createAbsoluteUrl('/template/adEditor/renderHtml?tabId=' . $this->tab->id) . ' ' . $filePath);
        
        if (file_exists($filePath)) {
            header("Pragma-directive: no-cache");
            header("Cache-directive: no-cache");
            header("Cache-control: no-cache");
            header("Pragma: no-cache");
            header("Expires: 0");
            header('Content-Type: image/jpeg');
            if ($download) {
                header('Content-Disposition: attachment; filename="' . $this->tab->name . '.jpg"');
            }
            readfile($filePath);
        }else{
			echo "file not exsist?";
		}
    }
    
}
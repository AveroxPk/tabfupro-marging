<?php

class TabIconController extends Controller
{
    const FACEBOOK_TAB_ICON_SIZE_WIDTH = 111;
    const FACEBOOK_TAB_ICON_SIZE_HEIGHT = 74;
    
    private function _handleIconUpload($tab)
    {
        $file = CUploadedFile::getInstanceByName('file');
        
        //Enable only wellknown image file extensions
        if (!in_array(strtolower($file->getExtensionName()), array(
            'jpg',
            'jpeg',
            'gif',
            'png'
        ))) {
            return null;
        }
        
        
        
        $model = new TabIcon;
        $model->original_filename = $file->name;
        $model->server_filename = md5($tab->id . $file->name . time()) . '.' . $file->getExtensionName();
        $model->user_id = Yii::app()->user->id;
        
        if (
            ImageHelper::resizeImage(
                $file->tempName,
                self::FACEBOOK_TAB_ICON_SIZE_WIDTH, 
                self::FACEBOOK_TAB_ICON_SIZE_HEIGHT, 
                $model->getImagePath(),
                $file->getExtensionName()
            )
        ) {
            if ($model->save(false)) {
                return $model;
            }
        }
        
        return null;
    }
    
    public function actionLibrary()
    {
        TabIconHelper::generateTabLibraryWidget()->run();
    }
    
    public function actionSelectForTab($tabId, $iconId = null)
    {   
        Yii::app()->getModule('facebook');
        $tab = UserTabs::model()->findByAttributes(array(
            'user_facebook_id' => Yii::app()->user->u_f_id,
            'id' => $tabId
        ));
        if ($tab === null) {
            throw new CHttpException(404, 'Specified tab has not been found for this user.');
        }
        
        if ($iconId === null) {
            if (($icon = $this->_handleIconUpload($tab)) === null) {
                exit('ERROR');
            }
        } else {
            $icon = TabIcon::model()->forCurrentUser()->findByPk($iconId);
            if ($icon === null) {
                throw new CHttpException(500, 'Invalid icon.');
            }
        }
        
        $tab->tab_icon_id = $icon->id;
        if ($tab->update(array('tab_icon_id'))) {
            if ($tab->published) {
                $tab->publishOnFB();
            }
            exit($icon->getImageUrl());
        }
        
        exit('ERROR');
        
    }
    
    public function actionDeleteTabIcon($tabId=null, $iconId = null){
        if($iconId !== null OR $tabId !== null){
            
            Yii::app()->getModule('facebook');
            $tab = UserTabs::model()->findByAttributes(array(
                'user_facebook_id' => Yii::app()->user->u_f_id,
                'id' => $tabId
            ));
            
            if ($tab === null) {
                throw new CHttpException(404, 'Specified tab has not been found for this user.');
            }
            
            $icon = TabIcon::model()->forCurrentUser()->findByPk($iconId);
            if ($iconId !== null) {
                if($icon->delete()){
                    exit('ok');
                }
            }
        }
        
        exit('ERROR');
    }
    
}
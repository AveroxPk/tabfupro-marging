<?php

class AdController extends Controller
{
    public function actionImport($id, $name = null)
    {
        $importer = new AdImporter;
        $dirPath = Yii::getPathOfAlias('webroot.html_mockups.fb-ad-templates') . DIRECTORY_SEPARATOR . $id;
        $zipName = "$id.zip";
        $zipPath = "$dirPath/$zipName";
        $this->_createTemplateZip($dirPath, $zipName);

        $templateModel = $importer->import($zipPath, $name);
        
        print_r($templateModel);
        
        /*$model = new TemplateImportForm;
        
        if (isset($_POST['TemplateImportForm'])) {
            $model->attributes = $_POST['TemplateImportForm'];
            $model->zipFile = CUploadedFile::getInstance($model, 'zipFile');
            
            if ($model->validate()) {
                $this->_performImport($model);
            }
        }
        
        $this->render('import', compact('model'));*/
        
    }
    
    private function _performImport(TemplateImportForm &$model)
    {
        $importer = new TemplateImporter;
        try {
            $importer->import($model->zipFile->tempName, $model->templateName);
            Yii::app()->user->setFlash('success', "'{$model->zipFile->name}' file has been successfully imported to the database as '{$model->templateName}' template.");
        } catch (Exception $e) { //Rewrite exceptions as form model's errors
            $model->addError('zipFile', $e->getMessage());
        }
    }
    
    private function _createTemplateZip($templateDirectoryPath, $zipName)
    {
        // Initialize archive object
        $zip = new ZipArchive;
        $zip->open("$templateDirectoryPath/$zipName", ZipArchive::OVERWRITE);

        $resourcesPath = "$templateDirectoryPath/resources";
        $resourcesPathLen = strlen($resourcesPath);
        
        // Create recursive directory iterator
        $resourceFiles = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($resourcesPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($resourceFiles as $file) {
            // Get real path for current file
            $filePath = $file->getRealPath();
            
            if (is_dir($filePath)) {
                continue;
            }
            
            // Add current file to archive
            $zip->addFile($filePath, 'resources' . substr($filePath, $resourcesPathLen));
        }
        
        $zip->addFile("$templateDirectoryPath/index.html", 'index.html');
        
        if (file_exists($previewPath = "$templateDirectoryPath/preview.jpg")) {
            $zip->addFile($previewPath, 'preview.jpg');
        }
        if (file_exists($previewPath = "$templateDirectoryPath/preview-big.png")) {
            $zip->addFile($previewPath, 'preview-big.png');
        }
        
        if (file_exists($gateConfigPath = "$templateDirectoryPath/title.txt")) {
            $zip->addFile($gateConfigPath, 'title.txt');
        }
        if (file_exists($gateConfigPath = "$templateDirectoryPath/description.txt")) {
            $zip->addFile($gateConfigPath, 'description.txt');
        }

        // Zip archive will be created only after closing object
        $zip->close();
    }
    
    public function actionReimportAll()
    {
        //Hide all existing ads
        Ad::model()->updateAll(array('is_visible' => 0));
        
        //Remove all tabs
        //Yii::app()->getModule('facebook');
        //UserTabs::model()->deleteAll();
        
        $importer = new AdImporter;
        foreach (array(
            'desktop-app'   => ['name' => 'Desktop Ap Ad 1', 'categories' => [1, 4, 3]],
            'link'          => ['name' => 'Link Ad 1', 'categories' => [5, 3]],
            'mobile-app'    => ['name' => 'Mobile Ap Ad 1', 'categories' => [1, 2, 3]],
            'offer'         => ['name' => 'Offer Ad 1', 'categories' => [6, 3]],
            'page-like'     => ['name' => 'Page Like Ad 1', 'categories' => [10]],
            'photo'         => ['name' => 'Photo Ad 1', 'categories' => [7, 8]]
        ) as $templateDirectory => $templateData) {
            $dirPath = Yii::getPathOfAlias('webroot.html_mockups.fb-ad-templates') . DIRECTORY_SEPARATOR . $templateDirectory;
            $zipName = "$templateDirectory.zip";
            $zipPath = "$dirPath/$zipName";
            $this->_createTemplateZip($dirPath, $zipName);
            
            $templateModel = $importer->import($zipPath, $templateData['name']);
            
            foreach ($templateData['categories'] as $categoryId) {
                Yii::app()->db->createCommand()
                    ->insert('ad_category_to_ad', [
                        'ad_id' => $templateModel->id,
                        'category_id' => $categoryId
                    ])
                ;
            }
            
        }
        
        $this->redirect('/facebook/ads#createAd');
        
    }

    public function actionPreview($id)
    {
        $renderer = new PageRenderer();
        $renderer->adminMode = false;

        $template = Ad::model()
            ->with('adToAdFields')
            ->findByPk($id);

        // Once I've got problem to solve, so I used java. I ended with ProblemFactory.
        Yii::import('application.modules.facebook.models.*');

        echo $renderer->renderTemplate(PageManager::getPreviewPage($template));
    }

    public function actionGetTemplatesView()
    {
        $model = new Ad;
        $templateProvider = $model->search();

        $templateProvider->pagination = false;
        $this->widget('zii.widgets.CListView', array(
            'id' => 'editor-templates-list-content',
            'htmlOptions' => array(
                'class' => 'editor-templates-list-content'
            ),
            'dataProvider' => $templateProvider,
            'itemView' => 'template.widgets.adWizard.views.__template',
            'summaryText' => ''
        ));
        exit;
    }

    public function actionToggleFavourite($id)
    {
        $template       = Ad::model()->findByPk($id);
        $currentUserId  = Yii::app()->user->id;

        if (!$template && Yii::app()->user->isGuest) {
            die(json_encode(array(
                'status' => false
            )));
        }

        $favorurited = TemplateFavourites::model()->findByAttributes(array(
            'template_id' => $id,
            'user_id' => $currentUserId
        ));

        if (!$favorurited) {
            $fav = new TemplateFavourites();
            $fav->user_id     = $currentUserId;
            $fav->template_id = $id;
            $fav->save();
            die(json_encode(array(
                'status' => true
            )));
        }

        $favorurited->delete();
        die(json_encode(array(
            'status' => true
        )));
    }
}
<?php

class GatesController extends CController
{
    public function actionEmail($tabId)
    {
        
        Yii::app()->getModule('facebook');
        
        $tab = UserTabs::model()->findByAttributes(array('id' => $tabId));
        if ($tab === null) {
            throw new CHttpException(404, 'Not found.');
        }
		
		
        $email = isset($_POST['email']) ? $_POST['email'] : '';
		
        
        if (!isset($_POST['lastName']) && isset($_POST['name'])) {
            list($name, $lastName) = explode(' ', $_POST['name'], 2);
        } else {
            $name = isset($_POST['name']) ? $_POST['name'] : '';
            $lastName = isset($_POST['lastName']) ? $_POST['lastName'] : '';
        }
        
		
        $emailValidator = new \Zend\Validator\EmailAddress();
		
        if (empty($email) || !$emailValidator->isValid($email)) {
            exit(json_encode(array(
                'error' => 1,
                'message' => 'Invalid e-mail address.'
            )));
        }
		
        $userTabEmail = new UserTabsEmails();
        $userTabEmail->user_tabs_id = $tab->id;
        $userTabEmail->email = $email;
        $userTabEmail->name = $name;
        $userTabEmail->lastname = $lastName;
        $userTabEmail->save();
		

 
        $failedAutoresponders = Yii::app()->getModule('autoresponders')->addSubscriberToTabAutoresponders($tab, $email, $name, $lastName);
        if (!empty($failedAutoresponders)) {
            $failedAutorespondersList = join(
                "\n", 
                array_map(
                    function($autoresponder){
                        return "* $autoresponder";
                    }, 
                    $failedAutoresponders
                )
            );
            
            exit(json_encode(array(
                'error' => 1,
                'message' => "Error while subscribing to the following autoresponders: \n$failedAutorespondersList\n\nIf you are an administrator of this page, please recheck your autoresponders configuration."
            )));
        }
        
        if (isset($_POST['message'])) {
            
            //Determine e-mail of the Tab owner
            $tabOwnerEmail = $tab->userFacebook->user->email;

            //Forward e-mail message sent from facebook tab's gate to Tab owner
            if (!MailHelper::sendMail($tabOwnerEmail, 'Message from TabFu e-mail gate', $_POST['message'], $email)) {
                exit(json_encode(array(
                    'error' => 1,
                    'message' => 'Some error occured. Please try again later.'
                )));
            }
            
        }

        // Track action
        Yii::import('application.modules.analytics.*');
        Analytics::track(Analytics::ANALYTICS_PAGE, PageAnalytics::ACTION_EMAIL_GATE, $tabId);
        
        //Success
        //setcookie($tabId . 'eml', '1', 10 * 365 * 24 * 3600);
        exit(json_encode(array(
            'error' => 0,
        )));
    }
	
	//------------- Created for Custom tabs------------
   public function actionCustom($tabId)
    {
        
        Yii::app()->getModule('facebook');
        
        $tab = UserTabs::model()->findByAttributes(array('id' => $tabId));
        if ($tab === null) {
            throw new CHttpException(404, 'Not found.');
        }
		
		
        $email = isset($_POST['email']) ? $_POST['email'] : '';
		
		$inputFields = array_keys($_POST);
 //       var_dump($_POST);
//		exit;

        if (!isset($_POST['lastName']) && isset($_POST['name'])) {
            list($name, $lastName) = explode(' ', $_POST['name'], 2);
        } else {
            $name = isset($_POST['name']) ? $_POST['name'] : '';
            $lastName = isset($_POST['lastName']) ? $_POST['lastName'] : '';
        }
        
        if(isset($_POST['nameRequired']) && strlen($_POST['name']<1))
		{
            exit(json_encode(array(
                'error' => 1,
                'message' => 'Name field required.'
            )));
			
		}
		
        $emailValidator = new \Zend\Validator\EmailAddress();
		
        if (empty($email) || !$emailValidator->isValid($email)) {
            exit(json_encode(array(
                'error' => 1,
                'message' => 'Invalid e-mail address.'
            )));
        }
		
		// Dynamically check the required field values and generating error on invalid value
		foreach($inputFields as $field)
		{
			if(($field !='email') && ($field != 'name'))
			{
				
				if(isset($_POST[$field.'Required']))
				{
					exit(json_encode(array(
						'error' => 1,
						'message' => 'Invalid '.$field.'.'
					)));
					
				}
				
			}
		}
	
		
		

	/**************** Zoho Start  ****************************/
		
		
		
		$zoho_xml = '';	    // empty string for zoho lead data
		$sf_lead = [];		// empty array for sf lead data
		
		
		$zoho_xml .= '<FL val="Lead Source">Facebook Tabs</FL>';
		$zoho_xml .= '<FL val="Lead Status">Not Contacted</FL>';
		
		if (!isset($_POST['lastName'])) {
			if(trim($lastName)=='')
				$lastName = 'n/a';

			$zoho_xml .= '<FL val="Last Name">'.$lastName.'</FL>';
//			$sf_lead['LastName'] = $lastName;
		}
		if(!isset($_POST['company']) || trim($_POST['company'])=='' )
		{
//			$sf_lead['Company'] = 'n/a';
		}
		
		// Creating CSV file headers here, if they already not exists.
		if(null == (UserTabsInputFields::model()->findByAttributes(array('user_tabs_id' => $tab->id))))
		{
			foreach($inputFields as $field)
			{
				$UserTabsInputFields = new UserTabsInputFields();
				$UserTabsInputFields->user_tabs_id = $tab->id;
				$UserTabsInputFields->field_name = $field;
				$UserTabsInputFields->save();
			}
			
		}
		
		// Creating CSV value
		foreach($_POST as $k => $data)
		{
			if($data=='')
				$data = NULL;
			
			$UserTabsInputFieldsData = new UserTabsInputFieldsData();
			$UserTabsInputFieldsData->user_tabs_id = $tab->id;
			$UserTabsInputFieldsData->field_value = $data;
			$UserTabsInputFieldsData->save();
			
			$zoho_Input_fields_arr = $this->getZohoInputFields();
			
			if(isset($zoho_Input_fields_arr[$k]))
			{
				if($k == 'name')
					$zoho_xml .='<FL val="'.$zoho_Input_fields_arr[$k].'">'.$name.'</FL>';				
				else
					$zoho_xml .='<FL val="'.$zoho_Input_fields_arr[$k].'">'.$data.'</FL>';
				
			}
		}
		
		
		if(null != ($crm = TabCrms::model()->findByAttributes(array('tab_id' => $tab->id,'crm_name' => 'Zoho'))))
		{
			$leads = '
				<Leads>
					<row no="1">'.$zoho_xml.'</row>
				</Leads>		
				';
			$fields = array(
					'newFormat'=>1,
					'scope'=>'crmapi',
					'authtoken'=>$crm->api_key,		
					'xmlData'=>$leads
				);

			$ch = curl_init('https://crm.zoho.com/crm/private/xml/Leads/insertRecords');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			$result = @curl_exec($ch);
		}
		
	/**************** Zoho End  ****************************/
	
	/**************** SalesForce Start  ****************************/
		if(null != ($crm = TabCrms::model()->findByAttributes(array('tab_id' => $tab->id,'crm_name' => 'SalesForce'))))
		{
			Yii::import('application.modules.salesforce.models.*');

			
			
			$sf_id  = $crm->api_key;
			
			$salesforceCrm = Salesforce::model()->findByAttributes(array(
				'id' => $sf_id
			));
			
			$access_token = $salesforceCrm->access_token;
			$refresh_token = $salesforceCrm->refresh_token;
			$instance_url = $salesforceCrm->instance_url;
			
			if(false != ($new_access_token = $this->actionGetAccessToken($access_token, $refresh_token)))
			{
				$access_token = $new_access_token;
				
				$salesforceCrm->access_token = $access_token;
				$salesforceCrm->update();				
			}
			
			$sf_Input_field_Arr = $this->getSfInputFields($instance_url, $access_token);
			
			$sf_lead['LeadSource'] = 'Facebook Tabs'; // Default value
				
			foreach($sf_Input_field_Arr as $k => $value)
			{
				
				if(!$value['nillable'] && (!isset($_POST[$k]) || trim($_POST[$k])==''))
				{
					$sf_lead[$value['fieldName']] = 'n/a';
					
				}
				
				if($value['fieldName']=="LastName" && trim($lastName)!='')
					$sf_lead['LastName'] = $lastName;
				
				elseif(isset($_POST[$k])){

					if($k == 'name')
						$sf_lead[$value['fieldName']] = $name; 
					else
						$sf_lead[$value['fieldName']] = $_POST[$k]; 
				}
				
				
			}
			
			$this->actionCreateLead($sf_lead, $instance_url, $access_token);
			
		}	
	/**************** SalesForce End  ****************************/
 
        $failedAutoresponders = Yii::app()->getModule('autoresponders')->addSubscriberToTabAutoresponders($tab, $email, $name, $lastName);
        if (!empty($failedAutoresponders)) {
            $failedAutorespondersList = join(
                "\n", 
                array_map(
                    function($autoresponder){
                        return "* $autoresponder";
                    }, 
                    $failedAutoresponders
                )
            );
            
            exit(json_encode(array(
                'error' => 1,
                'message' => "Error while subscribing to the following autoresponders: \n$failedAutorespondersList\n\nIf you are an administrator of this page, please recheck your autoresponders configuration."
            )));
        }
        
        if (isset($_POST['message'])) {
            
            //Determine e-mail of the Tab owner
            $tabOwnerEmail = $tab->userFacebook->user->email;

            //Forward e-mail message sent from facebook tab's gate to Tab owner
            if (!MailHelper::sendMail($tabOwnerEmail, 'Message from TabFu e-mail gate', $_POST['message'], $email)) {
                exit(json_encode(array(
                    'error' => 1,
                    'message' => 'Some error occured. Please try again later.'
                )));
            }
            
        }

        // Track action
        Yii::import('application.modules.analytics.*');
        Analytics::track(Analytics::ANALYTICS_PAGE, PageAnalytics::ACTION_EMAIL_GATE, $tabId);
        
        //Success
        //setcookie($tabId . 'eml', '1', 10 * 365 * 24 * 3600);
        exit(json_encode(array(
            'error' => 0,
        )));
    }	
	
    public function actionRenderCustomCode($subdomain)
    {
        // get subdomain 
//        $hostArray = explode(".", $_SERVER['HTTP_HOST']);
//        $subdomain = array_shift($hostArray);

        // get allowed subdomain prefix
        $customCodeConfig = Yii::app()->params['customCodeGate'];
        $allowedSubdomainPrefix = isset($customCodeConfig['allowedSubdomainPrefix']) ? 
                        $customCodeConfig['allowedSubdomainPrefix'] : 'tabs';

        //check subdomain
        if(strlen($subdomain)<strlen($allowedSubdomainPrefix) || substr($subdomain, 0, strlen($allowedSubdomainPrefix)) != $allowedSubdomainPrefix) {
            throw new CHttpException(405, 'Not allowed subdomain');
        }
        
        
        // get Tab ID from subdomain
        $tabId = substr($subdomain, strlen($allowedSubdomainPrefix));
        if (!$tabId) {
            throw new CHttpException(500, 'No tab specified');
        }

        // get user tab
        $tab = UserTabs::model()->findByAttributes(array('id' => $tabId));
        if ($tab === null) {
            throw new CException("Tab doesn't exist.");
        }
        
        // get newest page version
        $query = new PageQuery();
        $page = $query->getPageFromRepositoryByManagingTab($tab);

        // get gates config from page
        $gatesConfig = $page->getGatesConfig();

        // get customCode modalContent attribute value
        $customCodeContent = isset($gatesConfig['customCode']) ? 
                    isset($gatesConfig['customCode']['modalContent']) ? $gatesConfig['customCode']['modalContent'] : '' 
                : '';
        
        $this->renderPartial('customCode', array('content'=>$customCodeContent));
    }

	public function actionCreateLead($lead, $instance_url, $access_token) {
		$url = "$instance_url/services/data/v20.0/sobjects/Lead/";
		
		$content = json_encode($lead);

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
				array("Authorization: OAuth $access_token",
					"Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

		$json_response = curl_exec($curl);

		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		if ( $status != 201 ) {
			die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
		}
		
		//echo "HTTP status $status creating account<br/><br/>";

		curl_close($curl);

//		$response = json_decode($json_response, true);

//		$id = $response["id"];

//		echo "New record id $id<br/><br/>";

//		return $id;
		return true;
	}
/*
	function is_valid_token($instance_url, $access_token) {
		$url = "$instance_url/services/data/v39.0/sobjects/Lead";

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
				array("Authorization: OAuth $access_token"));

		$json_response = curl_exec($curl);
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		if($status == 401)
		{
			return false;
		}

		return true;

	}
*/	
	public function actionGetAccessToken($access_token, $refresh_token)
	{
		$url = 'https://login.salesforce.com/services/oauth2/token';
		
		$sf_config = Yii::app()->params['salesforce'];
		
		$client_id =  $sf_config['client_id'];
		$client_secret =  $sf_config['client_secret'];

		$data = [
			'grant_type'=>'refresh_token',
			'client_id'=> $client_id,
			'client_secret'=> $client_secret,
			'refresh_token'=> $refresh_token
		];
		$http_params = http_build_query($data);

		$curl = curl_init($url);

		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $http_params);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
			array("Content-type: application/x-www-form-urlencoded"));

		$json_response = curl_exec($curl);
		
		$decoded_data = json_decode($json_response);
		
		if($access_token == $decoded_data->access_token)
			return false;  // no db update required, we have a velid access_token
		
		return $decoded_data->access_token;
		
	}

    public function getZohoInputFields(){
		// Map tabfu custom template input fields name on the Zoho xml val
		// e.g Tabfu  'leadName'  will be  'Lead Name'  in zoho
		$zoho_Input_fields_arr = [
			'name'=>'First Name',
			'lastName'=>'Last Name',
			'activities'=>'Activities',
			'description'=>'Description',
			'leadName'=>'Lead Name',
			'company'=>'Company',
			'email'=>'Email',
			'phone'=>'Phone',
			'leadSource'=>'Lead Source',
			'leadOwner'=>'Lead Owner',
			'skypeId'=>'Skype ID',
			'title'=>'Title',
			'fax'=>'Fax',
			'mobile'=>'Mobile',
			'website'=>'Website',
			'leadStatus'=>'Lead Status',
			'industry'=>'Industry',
			'noOfEmployees'=>'No. of Employees',
			'annualRevenue'=>'Annual Revenue',
			'rating'=>'Rating',
			'createdBy'=>'Created By',
			'modifiedBy'=>'Modified By',
			'createdDate'=>'Created Date',
			'modifiedDate'=>'Modified Date',
			'street'=>'Street',
			'city'=>'City',
			'state'=>'State',
			'zip'=>'Zip Code',
			'country'=>'Country',
			'emailOptOut'=>'Email Opt Out',
			'salutation'=>'Salutation',
			'secondaryEmail'=>'Secondary Email',
			'lastActivityDate'=>'Last Activity Date',
			'twitter'=>'Twitter',
		];
		
		return $zoho_Input_fields_arr;
		
	}
	
	public function getSfInputFields($instance_url, $access_token)
	{
		
		$url = "$instance_url/services/data/v39.0/sobjects/Lead/describe";
		
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
				array("Authorization: OAuth $access_token"));

		$json_response = curl_exec($curl);
		curl_close($curl);

		$response = json_decode($json_response, true);
		
		$fieldsArr = [];
		$ignore_fields = [
			'Id',
			'Name',
			'IsDeleted',
			'Address',
			'Status',
			'OwnerId',
			'IsConverted',
			'IsUnreadByOwner',
			'CreatedDate',
			'CreatedById',
			'LastModifiedDate',
			'LastModifiedById',
			'SystemModstamp'
			
			];
		
		foreach($response['fields'] as $field)
		{
			if(in_array($field['name'], $ignore_fields))
				continue;
			
			$tabfu_map = lcfirst($field['name']);
			
			if($field['name']=='FirstName')
				$tabfu_map = 'name';	
			
			if($field['name']=='PostalCode')
				$tabfu_map = 'zip';	
			
			if($field['name']=='MobilePhone')
				$tabfu_map = 'mobile';	
			
			if($field['name']=='Street')
				$tabfu_map = 'address';	
			
			$fieldsArr[$tabfu_map] =array("fieldName" => $field['name'], "nillable" => $field['nillable']);
		}		
		
		return $fieldsArr;
/*		
		$sf_Input_field_Arr = [
				"name" => "FirstName",
				"lastName"=> "LastName",
				"company" =>"Company",
				"phone"=>"Phone",
				"email"=>"Email",
				"title"=>"Title",
				"mobile"=>"MobilePhone",
				"fax" => "Fax",
				"website" => "Website",
				"street" => "Street",
				"city" => "City",
				"state" => "State",
				"zip" => "PostalCode",
				"country" => "Country",
				"description" => "Description",
				"salutation" => "Salutation",
				"leadSource" => "LeadSource",				
		];
		
		return $sf_Input_field_Arr;
		*/
	}
}
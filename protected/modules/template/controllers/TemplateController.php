<?php

class TemplateController extends Controller
{
    public function actionImport($id, $name = null)
    {
        $importer = new TemplateImporter;
        $dirPath = Yii::getPathOfAlias('webroot.html_mockups.templates') . DIRECTORY_SEPARATOR . $id;
        $zipName = "$id.zip";
        $zipPath = "$dirPath/$zipName";
        $this->_createTemplateZip($dirPath, $zipName);

        $templateModel = $importer->import($zipPath, $name);
        
        print_r($templateModel);
        
        /*$model = new TemplateImportForm;
        
        if (isset($_POST['TemplateImportForm'])) {
            $model->attributes = $_POST['TemplateImportForm'];
            $model->zipFile = CUploadedFile::getInstance($model, 'zipFile');
            
            if ($model->validate()) {
                $this->_performImport($model);
            }
        }
        
        $this->render('import', compact('model'));*/
        
    }
    
    private function _performImport(TemplateImportForm &$model)
    {
        $importer = new TemplateImporter;
        try {
            $importer->import($model->zipFile->tempName, $model->templateName);
            Yii::app()->user->setFlash('success', "'{$model->zipFile->name}' file has been successfully imported to the database as '{$model->templateName}' template.");
        } catch (Exception $e) { //Rewrite exceptions as form model's errors
            $model->addError('zipFile', $e->getMessage());
        }
    }
    
    private function _createTemplateZip($templateDirectoryPath, $zipName)
    {
        // Initialize archive object
        $zip = new ZipArchive;
        $zip->open("$templateDirectoryPath/$zipName", ZipArchive::OVERWRITE);

        $resourcesPath = "$templateDirectoryPath/resources";
        $resourcesPathLen = strlen($resourcesPath);
        
        // Create recursive directory iterator
        $resourceFiles = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($resourcesPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($resourceFiles as $file) {
            // Get real path for current file
            $filePath = $file->getRealPath();
            
            if (is_dir($filePath)) {
                continue;
            }
            
            // Add current file to archive
            $zip->addFile($filePath, 'resources' . substr($filePath, $resourcesPathLen));
        }
        
        $zip->addFile("$templateDirectoryPath/index.html", 'index.html');
        
        if (file_exists($previewPath = "$templateDirectoryPath/preview.jpg")) {
            $zip->addFile($previewPath, 'preview.jpg');
        }
        if (file_exists($previewPath = "$templateDirectoryPath/preview-big.png")) {
            $zip->addFile($previewPath, 'preview-big.png');
        }
        
        if (file_exists($gateConfigPath = "$templateDirectoryPath/gates.txt")) {
            $zip->addFile($gateConfigPath, 'gates.txt');
        }
        
        if (file_exists($gateConfigPath = "$templateDirectoryPath/title.txt")) {
            $zip->addFile($gateConfigPath, 'title.txt');
        }
        if (file_exists($gateConfigPath = "$templateDirectoryPath/description.txt")) {
            $zip->addFile($gateConfigPath, 'description.txt');
        }

        // Zip archive will be created only after closing object
        $zip->close();
    }
    
    public function actionReimportAll()
    {
        //Hide all existing templates
        ARTemplate::model()->updateAll(array('is_visible' => 0));
        
        //Remove all tabs
        //Yii::app()->getModule('facebook');
        //UserTabs::model()->deleteAll();
        
        $importer = new TemplateImporter;
        foreach (array(
            'template_01' => ['name' => 'Template 1', 'categories' => [4, 9]],
            'template_02' => ['name' => 'Template 2', 'categories' => [6, 9, 8]],
            'template_03' => ['name' => 'Template 3', 'categories' => [9, 2]],
            'template_04' => ['name' => 'Template 4', 'categories' => [10, 6, 9]],
            'template_05' => ['name' => 'Template 5', 'categories' => [6, 2, 8]],
            'template_06' => ['name' => 'Template 6', 'categories' => [12, 5, 6]],
            'template_07' => ['name' => 'Template 7', 'categories' => [7, 6, 5]],
            'template_08' => ['name' => 'Template 8', 'categories' => [3]],
            'template_09' => ['name' => 'Template 9', 'categories' => [6, 2, 8, 9]],
            'template_10' => ['name' => 'Template 10', 'categories' => [1, 3]],
            'template_11' => ['name' => 'Template 11', 'categories' => [6, 2, 8]],
            'template_12' => ['name' => 'Template 12', 'categories' => [4, 6]],
            'template_13' => ['name' => 'Template 13', 'categories' => [6, 2, 8]],
            'template_14' => ['name' => 'Template 14', 'categories' => [5, 6, 8]],
            'template_15' => ['name' => 'Template 15', 'categories' => [6]],
            'template_16' => ['name' => 'Template 16', 'categories' => [9, 6, 8, 5]],
            'template_17' => ['name' => 'Template 17', 'categories' => [3]],
        ) as $templateDirectory => $templateData) {
            $dirPath = Yii::getPathOfAlias('webroot.html_mockups.templates') . DIRECTORY_SEPARATOR . $templateDirectory;
            $zipName = "$templateDirectory.zip";
            $zipPath = "$dirPath/$zipName";
            $this->_createTemplateZip($dirPath, $zipName);
            
            $templateModel = $importer->import($zipPath, $templateData['name']);
            
            foreach ($templateData['categories'] as $categoryId) {
                Yii::app()->db->createCommand()
                    ->insert('template_category_to_template', [
                        'template_id' => $templateModel->id,
                        'category_id' => $categoryId
                    ])
                ;
            }
            
        }
        
        $this->redirect('/fanpages#createTab');
        
    }

    public function actionPreview($id)
    {
        $renderer = new PageRenderer();
        $renderer->adminMode = false;

        $template = ARTemplate::model()
            ->with('templateToTemplateFields')
            ->findByPk($id);

        // Once I've got problem to solve, so I used java. I ended with ProblemFactory.
        Yii::import('application.modules.facebook.models.*');

        echo $renderer->renderTemplate(PageManager::getPreviewPage($template));
    }

    public function actionGetTemplatesView()
    {
        $model = new ARTemplate;
        $templateProvider = $model->search();

        $templateProvider->pagination = false;
        $this->widget('zii.widgets.CListView', array(
            'id' => 'editor-templates-list-content',
            'htmlOptions' => array(
                'class' => 'editor-templates-list-content'
            ),
            'dataProvider' => $templateProvider,
            'itemView' => 'template.widgets.tabWizard.views.__template',
            'summaryText' => ''
        ));
        exit;
    }

    public function actionToggleFavourite($id)
    {
        $template       = ARTemplate::model()->findByPk($id);
        $currentUserId  = Yii::app()->user->id;

        if (!$template && Yii::app()->user->isGuest) {
            die(json_encode(array(
                'status' => false
            )));
        }

        $favorurited = TemplateFavourites::model()->findByAttributes(array(
            'template_id' => $id,
            'user_id' => $currentUserId
        ));

        if (!$favorurited) {
            $fav = new TemplateFavourites();
            $fav->user_id     = $currentUserId;
            $fav->template_id = $id;
            $fav->save();
            die(json_encode(array(
                'status' => true
            )));
        }

        $favorurited->delete();
        die(json_encode(array(
            'status' => true
        )));
    }
}
<?php

class PageEditorController extends Controller
{
    /**
     *
     * @var Page
     */
    public $page;
    
    /**
     *
     * @var UserTabs
     */
    public $tab;
    
    public function init()
    {
        Yii::import('application.modules.facebook.models.*');
        Yii::import('application.modules.salesforce.models.*');
        Yii::import('application.modules.template.widgets.fields.TemplateFieldWidget');
        Yii::import('application.modules.template.widgets.fields.gateField.GateField');
        
        if (!isset($_GET['tabId'])) {
            throw new CHttpException(500, 'No tab specified');
        }
        
        //try {
            $this->tab = UserTabs::model()->findByAttributes(array('id' => $_GET['tabId']));
            if ($this->tab === null) {
                throw new CException("Tab doesn't exist.");
            }
            
            $pageModel = ARPage::model()->findByAttributes(array(
                'user_tabs_id' => $_GET['tabId']
            ));
            if ($pageModel === null) {
                throw new CException("Page for this tab doesn't exist.");
            }
            $pageId = $pageModel->id;
            $query = new PageQuery();
            $this->page = $query->getPageFromRepositoryByManagingTab($this->tab);
            
        /*} catch (Exception $e) {
            throw new CHttpException(404, 'Page not found.');
        }*/

    }
    
    public function actionPagePreview($adminMode = true, $download = false)
    {
        $renderer = new PageRenderer($this->tab);
        $renderer->adminMode = $adminMode;
        $renderer->params = array(
            'gatesConfig' => $this->page->getGatesConfig(),
            'tabId' => $this->tab->id
        );
        $pageContent = $renderer->renderPage($this->page);
        
        if ($download) {
            Yii::app()->request->sendFile('index.html', $pageContent, 'text/html');
            exit;
        }
        
        echo $pageContent;
        
    }
    
    public function actionPreviewOnFacebook($tabId)
    {
        $this->renderPartial('previewOnFacebook', array(
            'pagePreviewUrl' => CHtml::normalizeUrl(array('/template/pageEditor/pagePreview/tabId/' . $this->tab->id . '/adminMode/0'))
        ));
    }
    
    public function actionPageInfo()
    {
        $dynamicContent = false;
        $fields = $this->page->getFields();
        foreach ($fields as $field) {
            if (!$field->isCacheable()) {
                $dynamicContent = true;
                break;
            }
        }
        
        //Determine connected autoresponders and their lists
        Yii::app()->getModule('autoresponders');
        $autoresponderNamesList = [];
        $autoresponderListsList = [];
        $autoresponderConnections = TabAutoresponders::model()->findAllByAttributes(array('tab_id' => $this->tab->id));
        foreach ($autoresponderConnections as $autoresponderConnection) {
            //AutoresponderName::model()->getDisplayName();
            $autoresponderNamesList[] = call_user_func(array($autoresponderConnection->autoresponder_name, 'model'))->getDisplayName();
            $autoresponderListsList[] = $autoresponderConnection->list_name;
        }
        
        //Determine enabled gates
        $gatesConfig = $this->page->getGatesConfig();
        $enabledGatesList = [];
        foreach ([
            //'like' => 'Like gate', 
            'share' => 'Share gate', 
            'email' => 'Email gate',
            'customCode' => 'Custom Code gate'
        ] as $gateName => $gateDisplayName) {
            if (isset($gatesConfig[$gateName]['enabled']) && $gatesConfig[$gateName]['enabled'] == '1') {
                $enabledGatesList[] = $gateDisplayName;
            }
        }
        
            
        echo json_encode(array(
            'changesUnpublished' => $this->tab->published && $this->page->isMofified(),
            'tabPublished' => (bool)$this->tab->published,
            'facebookPageId' => $this->tab->userFanpage->facebook_page_id,
            'tabIconUrl' => $this->tab->tabIconUrl,
            'tabName' => $this->tab->name,
            'dynamicContent' => $dynamicContent,
            'gatesConfig' => $this->page->getGatesConfig(),
            
            //Autoresponder info in publish tab
            'autoresponders' => implode(', ', $autoresponderNamesList),
            'autorespondersLists' => implode(', ', $autoresponderListsList),
            'enabledGates' => implode(', ', $enabledGatesList)
            
        ));
    }
    
    public function actionUpdateField($tabId, $fieldId)
    {
        
        $fieldManager = new FieldManager($this->tab);
        $pageField = $this->page->getField($fieldId);
        
        $renderer = new PageRenderer;
        $renderer->adminMode = true;
        
        //TODO: move below function into some helper class
        /**
         * Converts Zend Validation errors list into nicely formatted string 
         * like as follows:
         * * error message 1
         * * error message 2
         * @return string
         */
        function formatErrorList(array $errorList)
        {
            $errorListString = '';
            
            //Flatten array to get "leaf" values which are error messages
            $errorMessagesIter = new RecursiveIteratorIterator(new RecursiveArrayIterator($errorList));
            foreach ($errorMessagesIter as $errorMessage) {
                $errorListString .= "* $errorMessage\n"; //Decorate error message
            }
            
            return $errorListString;
        }
        
        try {
            $updatedField = $fieldManager->updateFieldInRepository($pageField, $_POST);
            echo CJSON::encode(array(
                'error' => 0,
                'content' => $renderer->renderField($updatedField)
            ));
        } catch (AttributeValidationException $ex) {
            echo CJSON::encode(array(
                'error' => 1,
                'errorMessage' => formatErrorList($ex->getValidationErrors())
            ));
        }
        
        
    }

    public function actionUploadImage()
    {
		
        $file = CUploadedFile::getInstanceByName('file');
               
 
        $model = new ARPageFile;
        $model->original_filename = $file->name;
        $model->server_filename = md5($this->tab->id . $file->name . time()) . '.' . $file->getExtensionName();
        $model->page_id = $this->page->getId();
        
        //Enable only wellknown image file extensions
        if (!in_array(strtolower($file->getExtensionName()), array(
            'jpg',
            'jpeg',
            'gif',
            'png'
        ))) {
            exit(json_encode('error:: invalid image formate.'));
        }
		
		
        if ($file->saveAs(Yii::app()->getModule('template')->repositoryAdapterConfig['pattern']['path'] . $model->server_filename)) {
            $model->save(false);
			
            $assetQuery = new AssetQuery(Yii::app()->getModule('template')->getRepositoryAdapter());
            exit(json_encode($assetQuery->getAssetByServerFilename($model->server_filename)));
        }
        
        exit(json_encode('error:: unable to save file'));
        
    }
    
    public function actionPublish()
    {
        $pageManager = new PageManager($this->tab);
        
        $renderer = new PageRenderer();
        $renderer->adminMode = false;

        //Unfortunately we can't get things like Facebook tab URL at this stage
        //so the widgets which uses renderer params should be NOT CACHEABLE
        $renderer->params = array(); 
        
        try {
            //$this->tab->publishOnFB();
            $pageManager->publishLastPageVersion($this->page, $renderer);   
            
            echo 'OK';
        } catch (Exception $e) {
            echo 'ERROR';
        }
    }
    
    public function actionPageSetFanpage($tabId, $fanpageId)
    {
        //Security checks
        $fanpage = UserFanpage::model()->findByAttributes(array(
            'facebook_page_id' => $fanpageId,
            'user_facebook_id' => Yii::app()->user->u_f_id
        ));
        if ($fanpage === null) {
            throw new CHttpException(404, 'Page not found.');
        }
        
        //Update Tab's fanpage
        $this->tab->user_fanpage_id = $fanpage->id;
        exit($this->tab->save() ? 'OK' : 'ERROR');
        
    }
    
    public function actionUpdateGatesConfig()
    {
        if (!isset($_POST['gatesConfig']) || !is_array($_POST['gatesConfig'])) {
            die();
        }
        
        //Updating gates configuration...
        //I know this is dirty way, we should create some method in PageManager
        //but we didn't have time for it...
        $this->page->getVersion();
        
        $version = ARPageVersion::model()->findByAttributes(array(
            'page_id' => $this->page->getId(),
            'version' => $this->page->getVersion()
        ));
        
        $assetQuery = new AssetQuery(Yii::app()->getModule('template')->getRepositoryAdapter());
        
        //NOT SAFE
        //TODO: PLEASE VERIFY POST DATA
        $gatesConfig = $_POST['gatesConfig'];
        
        if (!empty($gatesConfig['like']['downloadImg'])) {
            $gatesConfig['like']['downloadImgUrl'] = $assetQuery->getAssetByServerFilename($gatesConfig['like']['downloadImg'])->getUrl();
        }
        if (!empty($gatesConfig['share']['downloadImg'])) {
            $gatesConfig['share']['downloadImgUrl'] = $assetQuery->getAssetByServerFilename($gatesConfig['share']['downloadImg'])->getUrl();
        }
        if (!empty($gatesConfig['email']['promptButtonImg'])) {
            $gatesConfig['email']['promptButtonImgUrl'] = $assetQuery->getAssetByServerFilename($gatesConfig['email']['promptButtonImg'])->getUrl();
        }
        
        //Invalid number in height? Set 150 by default
        if (!empty($gatesConfig['customCode']['contentModalHeight']) && !ctype_digit($gatesConfig['customCode']['contentModalHeight'])) {
            $gatesConfig['customCode']['contentModalHeight'] = GateField::CUSTOM_CODE_GATE_DEFAULT_CONTENT_HEIGHT;
        }
        
        $version->gates_config = json_encode($gatesConfig);
        
        if (!$version->update(array('gates_config'))) {
            die(0);
        }
        
        die($version->gates_config);
        
    }
    
    public function actionAutorespondersConfig()
    {
		if(!Yii::app()->user->Autoresponder){
			echo 'error';			
			return;
		}

        //Get "autoresponder name -> [connected list id, connected list name]" array
        $connectedAutoresponders = [];
        $tabAutoresponders = TabAutoresponders::model()->findAllByAttributes(array(
            'tab_id' => $this->tab->id
        ));
        foreach ($tabAutoresponders as $tabAutoresponder) {
            $connectedAutoresponders[$tabAutoresponder->autoresponder_name] = [
                $tabAutoresponder->list_id,
                $tabAutoresponder->list_name
            ];
        }
        
        $this->renderPartial('autorespondersModal', array(
            'autoresponders' => Yii::app()->getModule('autoresponders')->getAutorespondersForUser(),
            'connectedAutoresponders' => $connectedAutoresponders
        ));
    }
    
    //Lists connecting
    public function actionConnectAutoresponder()
    {
        if (!isset($_POST['autoresponder'])) {
            throw new CHttpException(500, 'Invalid request');
        }
        
        $autoresponder = $_POST['autoresponder'];
        
        if (!isset($_POST['listName']) || !isset($_POST['listId'])) {
            $listName = '';
            $listId = '';
        } else {
            $listName = $_POST['listName'];
            $listId = $_POST['listId'];
        }
        
        echo Yii::app()->getModule('autoresponders')->connectAutoresponderToTab($this->tab, $_POST['autoresponder'], $listId, $listName) ? 1 : 0;
        
    }
    
    public function actionDisconnectAutoresponder()
    {
        if (!isset($_POST['autoresponder'])) {
            throw new CHttpException(500, 'Invalid request');
        }
        
        echo Yii::app()->getModule('autoresponders')->disconnectAutoresponderFromTab($this->tab, $_POST['autoresponder']) ? 1 : 0;
    }
	
    public function actionCrmConfig()
    {
		if(!Yii::app()->user->Crm){
			echo 'error';			
			return;
		}
		
        $connectedCrms = [];
		$sf_id = false;
		
		$salesforceCrm = Salesforce::model()->findByAttributes(array(
			'user_id' => Yii::app()->user->id
		));
		
		$sf_id = $salesforceCrm->id;
		
        $tabCrms = TabCrms::model()->findAllByAttributes(array(
            'tab_id' => $this->tab->id
        ));
		
        foreach ($tabCrms as $tabCrm) {
            $connectedCrms[$tabCrm->crm_name] = [
                $tabCrm->api_key
            ];
        }
		
		$this->renderPartial('crmsModal', array(
			'crms' => array('zoho'=>'Zoho','salesforce'=>'SalesForce'),
			'connectedCrms' => $connectedCrms,
			'sf_id' => $sf_id
		));
		
/*        
        $this->renderPartial('autorespondersModal', array(
            'autoresponders' => Yii::app()->getModule('autoresponders')->getAutorespondersForUser(),
            'connectedAutoresponders' => $connectedAutoresponders
        ));
*/		
    }
	
	public function actionConnectCrm()
	{
		$crm_name = '';
		$api_key = '';
		
        $crm_name = array_keys($_POST)[0];
		$api_key = $_POST["$crm_name"];
		
        if (trim($crm_name) == '' || trim($api_key) == '') {
            throw new CHttpException(500, 'Invalid request');
        }
		
		$tabCrm = new TabCrms();  //tabCrm model obj
		$tabCrm->tab_id = $this->tab->id;
		$tabCrm->crm_name = $crm_name;
		$tabCrm->api_key = $api_key;
		
		$tabCrm->save();
		
		return true;
	}

    public function actionDisconnectCrm()
    {
        if (!isset($_POST['crm'])) {
            throw new CHttpException(500, 'Invalid request');
        }
		/*
        $tabCrm = TabCrms::model()->findAllByAttributes(array(
            'tab_id' => $this->tab->id,
            'crm_name' => $_POST['crm']
        ));
		*/
        $tabCrm = TabCrms::model()->find('tab_id = :tid AND crm_name = :crmname', array(
            ':tid' => $this->tab->id,
            ':crmname' => $_POST['crm']
        ));
		
		if(null !== $tabCrm)
		{
			echo $tabCrm->delete()? 1 : 0;
		}
    }
	
	
}
<?php

class ExtraController extends Controller
{
    public function actionGenerateField()
    {
        FieldPlaceholder::createImportField('downloadButton', array(
            ''
        ));
    }
    
    public function actionTest()
    {
        $model = new ARTemplate;
        $templateProvider = $model->search();
        
        $availableTemplateFields = ARTemplateField::model()->with('templateFieldAttributes')->findAll();

        $this->render('test', compact('templateProvider', 'availableTemplateFields'));
    }
}
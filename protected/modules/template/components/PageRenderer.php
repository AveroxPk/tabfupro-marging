<?php

Yii::import('application.modules.template.widgets.fields.TemplateFieldWidget');
Yii::import('application.modules.template.widgets.fields.gateField.GateField');
class PageRenderer
{
    /**
     * If true, DOM will contain extra div containers which contain information
     * essential for page editing.
     * @var bool
     */
    public $adminMode = false;
    
    /**
     * Extra params which will be passed to Fields like Facebook tab URL 
     * (for Share buttons)
     * @var array
     */
    public $params = array();
    
    /**
     * Renders specific Page's content (replaces placeholders with Field's 
     * widgets)
     * 
     * @param Page $page Page model which content should be rendered
     * @param bool $omitNotCacheableFields If true, all fields that are not 
     * cacheable will be ignored and their placeholders will be left in the 
     * way as they are before
     * @return string Rendered content
     */
    public function renderPage(Page $page, $omitNotCacheableFields = false, $tab = null)
    {
        $content = $page->getContent();
        if(!$page->IsStatic()) {
            foreach ($page->getFields() as $fieldId => $field) {
                if ($omitNotCacheableFields && !$field->isCacheable()) {
                    continue;
                }
                $content = $this->_strReplaceOnce(FieldPlaceholder::createPagePlaceholder($fieldId), $this->renderField($field), $content);
            }
        }
        // replace the title
        if ($tab) {
            $content = preg_replace('/<title>(.*)<\/title>/i', '<title>'.htmlspecialchars($tab->name).'</title>', $content);
        }
        // adding mobile script reference
        $content = str_replace('</head>', '</head><script src="/js/detectmobilebrowser.js"></script>', $content);
        // adding meta tags
        if ($tab) {
            $content = str_replace('</head>', '<meta property="og:url" content="' . Yii::app()->request->getBaseUrl(true) . '/facebook/showtab/mobile/tabid/' . $tab->id . '?noredir=1" />'.
                '<meta property="og:title" content="' . htmlspecialchars($tab->name) . '" />' .
                '<meta property="og:description" content="' . htmlspecialchars($tab->name) . '" />' .
                '<meta property="og:image" content="' . ($tab->getTabIconUrl() != null ? $tab->getTabIconUrl() : Yii::app()->request->getBaseUrl(true) . '/template_resources/'.$tab->getTemplateId().'/preview.jpg') . '" /></head>', $content);
        }
        // updating URLs to custom scripts and styles
        $content = str_replace('href="/assets', 'href="' . Yii::app()->request->getBaseUrl(true) . '/assets', $content);
        $content = str_replace('src="/assets', 'src="' . Yii::app()->request->getBaseUrl(true) . '/assets', $content);
        $content = str_replace('href="/css', 'href="' . Yii::app()->request->getBaseUrl(true) . '/css', $content);
        $content = str_replace('src="/images', 'src="' . Yii::app()->request->getBaseUrl(true) . '/images', $content);
        $content = str_replace('src="/js', 'src="' . Yii::app()->request->getBaseUrl(true) . '/js', $content);
        
        if ($tab) {
            Analytics::track(Analytics::ANALYTICS_PAGE, PageAnalytics::ACTION_IMPRESSION, $tab->id);
        }
        
        return $content;
    }

    public function renderTemplate(Page $page, $omitNotCacheableFields = false)
    {
        $content = $page->getContent();
        if(!$page->IsStatic()) {
            foreach ($page->getFields() as $fieldId => $field) {
                if ($omitNotCacheableFields && !$field->isCacheable()) {
                    continue;
                }
                $content = $this->_strReplaceOnce(FieldPlaceholder::createTemplatePlaceholder($fieldId), $this->renderField($field), $content);
            }
        }
        return $content;
    }
    
    private function _strReplaceOnce($search, $replace, $subject)
    {
        $pos = strpos($subject, $search);
        if ($pos !== false) {
            return substr_replace($subject, $replace, $pos, strlen($search));
        }
        
        return $subject;
    }
    
    /**
     * Renders specific Field
     * @param Field $field Field model to be rendered
     * @return string Field's widget's output content
     */
    public function renderField(Field $field)
    {
        //Set up field's attributes
        $attributes = array(
            'adminMode' => $this->adminMode
        );
        
        $editableAttributes = array();
        if ($field->getAttributes()) {
            foreach ($field->getAttributes() as $attributeName => $attributeModel) {
                $attributes[$attributeName] = $attributeModel->getValue();
            }
        }
        
        //Pass extra params from renderer to field
        $attributes['rendererParams'] = $this->params;
        $attributes['rendererParams']['fieldId'] = $field->getId();
        
        //Resolve field's widget class path
        $fieldName = lcfirst($field->getName());
        $fieldClass = ucfirst($fieldName) . 'Field';
        $fieldClassPath = "application.modules.template.widgets.fields.$fieldName.$fieldClass";
        
        //Create and render field's widget
        ob_start();
        $widget = Yii::app()->controller->widget(
            $fieldClassPath,
            //Pass only not empty attributes to the widget. 
            //It'll enable widget to use it's own default values on such case
            array_filter($attributes, function($value) {return $value !== '';})
        );
        $content = ob_get_clean();
        
        //If in admin mode, decorate Fields with special divs with extra metadata
        if ($this->adminMode) {
            
            //Collect attribute values for the editor but from widget
            $editableAttributesNames = array_diff(
                array_keys($attributes), 
                array(
                    'rendererParams',
                    'adminMode'
                )
            );
            foreach ($editableAttributesNames as $attributeName) {
                $editableAttributes[$attributeName] = $widget->$attributeName;
            }
            
            $tagAttributes = array(
                'class' => 'field-element',
                'id' => 'page-field-' . $field->getId(),
                'data-field-id' => $field->getId(),
                'data-attributes' => json_encode($editableAttributes),
                'data-field-type' => $field->getName()
            );
            
            if ($widget instanceof GateField) {
                $tagAttributes['data-gate-field'] = 1;
            }
            
            if (!empty($attributes['adminTag'])) {
                $tagAttributes['data-tag'] = $attributes['adminTag'];
            }
                        
            return CHtml::tag('div', $tagAttributes, $content);
        }
        
        return $content;
        
    }
    

    
    
}
<?php

class SubdomainHelper
{
    public static function generateDomainForCustomCodeGate($tabId)
    {
        $protocol = Yii::app()->request->isSecureConnection ? 'https' : 'http';
        $domain = str_replace('www.', '', Yii::app()->request->serverName);
        $subdomainPrefix = 
            isset(Yii::app()->params['customCodeGate']['allowedSubdomainPrefix']) ? 
                Yii::app()->params['customCodeGate']['allowedSubdomainPrefix'] : 
                'tabs'
        ;
        
        $subdomain = $subdomainPrefix . $tabId;
        
        
        return "$protocol://$subdomain.$domain";
    }
}
<?php
//TODO: refactor, especially move model initalization code from _htmlImport function
class TemplateImporter
{
    /**
     * @var CDbTransaction
     */
    protected $_dbTransaction;
    
    /**
     * @var ARTemplate
     */
    protected $_template;
    
    /**
     *
     * @var array
     */
    protected $_placeHoldersToIds;
    
    public function import($filePath, $templateName)
    {
        $zip = new ZipArchive;
        if (!$zip->open($filePath)) {
            throw new CException('Invalid ZIP file');
        }
        
        $this->_dbTransaction = Yii::app()->db->beginTransaction();
        
        $this->_importHtml($zip->getFromName('index.html'), $templateName);
        $resourcesPath = Yii::app()->basePath . '/../template_resources/' . $this->_template->id . '/';
        $extractedResources = $this->_extractResources($zip, $resourcesPath);
        $this->_extractPreview($zip, $resourcesPath);
        $this->_extractPreviewBig($zip, $resourcesPath);
        $this->_template->gates_config = $this->_getGatesConfig($zip);
        $this->_template->name = $this->_getTitle($zip);
        $this->_template->description = $this->_getDescription($zip);
        
        $zip->close();
        
        if ($extractedResources === false) {
            $this->_dbTransaction->rollback();
            throw new CException("Couldn't extract template's resources.");
        }
        
        //We must plug in gates CSSes and JS logic to the template if any gate 
        //is available (we assume that any configuration of the gates other than 
        //empty is identical to the fact that one of the gates is available)
        if ($this->_template->gates_config != '{}') {
            $this->_template->content = $this->_appendGatesResources($this->_template->content);
        }
        
        $this->_template->content = $this->_correctResourcePathsInHtml(
            $this->_template->content
        );
        
        if (!$this->_template->save(false)) {
            $this->_dbTransaction->rollback();
            throw new CException("Couldn't correct resource pathes.");
        }
        
        $this->_dbTransaction->commit();
        
        return $this->_template;
        
    }
    
    private function _correctResourcePath($resourcePath)
    {
        if (strpos($resourcePath, 'resources/') !== 0) {
            return $resourcePath;
        }
       
        return str_replace(
            //replace:
            'resources/',
            //to:
            Yii::app()->getBaseUrl(true) . '/template_resources/' . $this->_template->id . '/resources/',
            //in:
            $resourcePath
        );
       
    }
    
    private function _extractPreview(ZipArchive &$zip, $destinationPath)
    {
        if ($zip->getFromName('preview.jpg') === false) {
            return false;
        }
        
        return $zip->extractTo($destinationPath, array('preview.jpg'));
    }
    
    private function _extractPreviewBig(ZipArchive &$zip, $destinationPath)
    {
        if ($zip->getFromName('preview-big.png') === false) {
            return false;
        }
        
        return $zip->extractTo($destinationPath, array('preview-big.png'));
    }
    
    private function _getGatesConfig(ZipArchive &$zip)
    {
        if (($fh = $zip->getStream('gates.txt')) === false) {
            return '{}';
        }
        
        $json = fread($fh, 1024 * 5);
        if (json_decode($json) === null) {
            throw new CException('Invalid gates configuration in template archive ' . $zip->filename);
        }
        
        return $json;
    }
    
    private function _extractResources(ZipArchive &$zip, $path)
    {
        $filesToExtract = array();
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $entry = $zip->getNameIndex($i);
            if (strpos($entry, 'resources/') !== 0) {
                continue;
            }
            
            $filesToExtract[] = $entry;
        }
        
        if (count($filesToExtract) === 0) {
            return array();
        }
        
        if (!$zip->extractTo($path, $filesToExtract)) {
            return false;
        }
        
        return $filesToExtract;
        
    }
    
    private function _correctResourcePathsInHtml($html)
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        libxml_use_internal_errors(false);
        $xpath = new DOMXPath($dom);
       
        //Correct scripts and static images
        if ($elements = $xpath->query('//script[@src] | //img[@src]')) {
            foreach ($elements as $element) {
                $element->setAttribute('src', $this->_correctResourcePath($element->getAttribute('src')));
            }
        }
       
        //Correct CSS links
        if ($links = $xpath->query('/html/head/link[@href]')) {
            foreach ($links as $script) {
                $script->setAttribute('href', $this->_correctResourcePath($script->getAttribute('href')));
            }
        }
        
        $headEl = $dom->getElementsByTagName('head')->item(0);
        
        //Open all links in topmost (parent) frame
        $baseEl = $dom->createElement('base');
        $baseEl->appendChild($dom->createAttribute('target'));
        $baseEl->setAttribute('target', '_top');
        $headEl->appendChild($baseEl);
        
        return $dom->saveHTML();
    }
    
    private function _appendGatesResources($html)
    {
        $jQueryUrl = Yii::app()->clientScript->coreScriptUrl . '/jquery.js';
        $appendHeadCode = <<<HTML
    <link rel="stylesheet" type="text/css" href="/css/gates.css" /> 
    <script type="text/javascript" src="$jQueryUrl"></script> 
    <script type="text/javascript" src="/js/gates.js"></script>
HTML;
        
        $count = 0;
        $html = str_replace('</head>', $appendHeadCode . '</head>', $html, $count);
        
        if ($count != 1) {
            throw new CException("Couldn't attach Gates resources to the template HTML code.");
        }
        return $html;
        
    }
    
    private function _getTitle(ZipArchive &$zip)
    {
        if (($fh = $zip->getStream('title.txt')) === false) {
            return '';
        }
        
        try {
            return fread($fh, 1024 * 5);
        } finally {
            fclose($fh);
        }
        
    }
    
    private function _getDescription(ZipArchive &$zip)
    {
        if (($fh = $zip->getStream('description.txt')) === false) {
            return '';
        }
        
        try {
            return fread($fh, 1024 * 5);
        } finally {
            fclose($fh);
        }
        
    }
    
    /**
     * 
     * @param string $filePath
     * @param string $templateName
     * @throws CException
     */
    private function _importHtml($html, $templateName)
    {
        //Init all available placeholders together with id's of their TemplateFields in DB
        $this->_placeHoldersToIds = array();
        foreach (ARTemplateField::model()->findAll() as $fieldModel) {
            $this->_placeHoldersToIds[$fieldModel->name] = $fieldModel->id;
        }
        
        //RegEx pattern which will find all placeholders
        $regExPattern = FieldPlaceholder::createImportFieldsRegEx(array_keys($this->_placeHoldersToIds));
        
        //Create provisional Template model
        $this->_template = new ARTemplate;
        $this->_template->name = $templateName;
        $this->_template->is_visible = 1;
        $this->_template->creation_time = date('y-m-d H:i:s');
        if (!$this->_template->save()) { //We save that model because we'll need parent's id for it's future submodels
            $errors = array();
            foreach ($this->_template->errors as $errorField) {
                foreach ($errorField as $errorMsg) {
                    $errors[] = $errorMsg;
                }
            }
            
            throw new CException("Template creation error(s): " . join(', ', $errors));
        }
        
        //Create all needed TemplateToTemplateField models and compile them into
        //Template's HTML content.
        $compiledContent = preg_replace_callback($regExPattern, array($this, '_insertField'), $html);
        $this->_template->content = $compiledContent;
        
        //Save Template compiled content
        if (!$this->_template->save(false)) {
            $this->_dbTransaction->rollback();
            throw new CException("Database error");
        }
        
        
    }
    
    /**
     * This function is called from preg_replace_callback.
     * The goal is to create TemplateToTemplateField model for each placeholder
     * like "{field_type}" and replace that placeholder to "{model_id}".
     * @param array $regExMatch
     * @return string
     * @throws CException
     */
    private function _insertField($regExMatch)
    {   
        
        $placeholderData = FieldPlaceholder::extractFromImportPlaceholder($regExMatch[0]);
        
        $model = new ARTemplateToTemplateField;
        $model->template_id = $this->_template->id;
        $model->template_field_id = $this->_placeHoldersToIds[$placeholderData['fieldName']];
        
        //Correct resource paths in field's default values
        if ($placeholderData['defaultAttributesValues']) {
            foreach ($placeholderData['defaultAttributesValues'] as &$attribute) {
                $attribute = $this->_correctResourcePath($attribute);
            }
        }
        
        $model->default_values = json_encode($placeholderData['defaultAttributesValues'] ? : null);
        
        if (!$model->save()) {
            $this->_dbTransaction->rollback();
            throw new CException("Database error");
        }
        
        return FieldPlaceholder::createTemplatePlaceholder($model->id); //"{{$model->id}}";
    }

    
    
}
<?php

class TemplateHelper
{
    public static function getTemplatePreview(ARTemplate $template)
    {
        $resourcesPath = '/template_resources/' . $template->id . '/';

        if (file_exists(Yii::app()->basePath . '/../' . $resourcesPath . 'preview.jpg')) {
            $imgUrl = $resourcesPath . 'preview.jpg';
        } else {
            $imgUrl = '/images/tabwizard/image-placeholder.jpg';
        }
        
        return CHtml::image(Yii::app()->getBaseUrl(true) . $imgUrl, $template->name);
    }
}
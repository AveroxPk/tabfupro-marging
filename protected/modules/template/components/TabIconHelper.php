<?php

class TabIconHelper
{
    /**
     * Generates widget with list of tab icons for currently logged in user.
     * @see TabWizard
     * @see TabIconController
     * @return CListView
     */
    public static function generateTabLibraryWidget()
    {
        $dataProvider = TabIcon::model()->forCurrentUser()->search();
        $dataProvider->setPagination(false);
        
        return Yii::app()->controller->createWidget('zii.widgets.CListView', array(
            'id' => 'TabIconLibraryListView',
            'dataProvider' => $dataProvider,
            'itemView' => 'application.modules.template.views.tabIcon._tabIcon',
            'ajaxUrl' => array('/template/tabIcon/library'),
            'ajaxType'=>'GET',
            'itemsCssClass' => 'tab-icon-library',
            'enablePagination' => false,
            'summaryText' => '',
            'pager' => array(
                'header' => ''
            )
        ));
    }
    
}
<?php

class ImageHelper
{
    public static function resizeImage($filePath, $newWidth, $newHeight, $newFilePath = null, $type = null)
    {
        //Check if image already meets size options
        list($curWidth, $curHeight) = getimagesize($filePath);
        if ($curWidth == $newWidth && $curHeight == $newHeight) {
            if ($newFilePath !== null) {
                return copy($filePath, $newFilePath);
            }
            return true;
        }
        
        if ($newFilePath === null) {
            $newFilePath = $filePath;
        }
        
        
        //Source image resource
        if ($type === null) {
            $type = strtolower(substr(strrchr($filePath, "."), 1));
        }
        switch($type){
            case 'bmp': 
                $srcImage = imagecreatefromwbmp($filePath); 
                break;

            case 'gif': 
                $srcImage = imagecreatefromgif($filePath); 
                break;

            case 'jpg':
            case 'jpeg': 
                $srcImage = imagecreatefromjpeg($filePath); 
                break;

            case 'png': 
                $srcImage = imagecreatefrompng($filePath); 
                break;

            default: 
                return false;
        }
        
        //New image resource
        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        if ($newImage === false) {
            return false;
        }
        
        try {
            
            //Try to resize image
            if (!imagecopyresampled($newImage, $srcImage, 0, 0, 0, 0, $newWidth, $newHeight, $curWidth, $curHeight)) {
                return false;
            }
            
            //Save resized image
            switch ($type) {
                case 'bmp': 
                    return imagewbmp($newImage, $newFilePath); 
                
                case 'gif': 
                    return imagegif($newImage, $newFilePath);
                    
                case 'jpeg':
                case 'jpg': 
                    return imagejpeg($newImage, $newFilePath);
                    
                case 'png': 
                    return imagepng($newImage, $newFilePath);
            }
            
        } finally {
            //Free image resources before function return
            imagedestroy($srcImage);
            imagedestroy($newImage);
        }
        
        
    }
}
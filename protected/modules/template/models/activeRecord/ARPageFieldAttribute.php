<?php

/**
 * This is the model class for table "page_field_attribute".
 *
 * The followings are the available columns in table 'page_field_attribute':
 * @property string $id
 * @property string $page_field_id
 * @property string $template_field_attribute_id
 * @property string $value
 *
 * The followings are the available model relations:
 * @property PageField $pageField
 * @property TemplateFieldAttribute $templateFieldAttribute
 */
class ARPageFieldAttribute extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'page_field_attribute';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_field_id, template_field_attribute_id', 'required'),
			array('page_field_id', 'length', 'max'=>19),
			array('template_field_attribute_id', 'length', 'max'=>10),
            array('value', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, page_field_id, template_field_attribute_id, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pageField' => array(self::BELONGS_TO, 'ARPageField', 'page_field_id'),
			'templateFieldAttribute' => array(self::BELONGS_TO, 'ARTemplateFieldAttribute', 'template_field_attribute_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page_field_id' => 'Page Field',
			'template_field_attribute_id' => 'Template Field Attribute',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('page_field_id',$this->page_field_id,true);
		$criteria->compare('template_field_attribute_id',$this->template_field_attribute_id,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ARPageFieldAttribute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

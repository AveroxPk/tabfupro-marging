<?php

/**
 * This is the model class for table "template_field".
 *
 * The followings are the available columns in table 'template_field':
 * @property string $id
 * @property string $name
 * @property string $label
 * @property string $description
 * @property integer $is_visible
 * @property integer $is_cacheable
 *
 * The followings are the available model relations:
 * @property PageField[] $pageFields
 * @property TemplateFieldAttribute[] $templateFieldAttributes
 * @property TemplateToTemplateField[] $templateToTemplateFields
 */
class ARTemplateField extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'template_field';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, name, label, description', 'required'),
			array('is_visible, is_cacheable', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>10),
			array('name, label, editor_title, editor_description', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, label, description, is_visible, is_cacheable', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pageFields' => array(self::HAS_MANY, 'ARPageField', 'template_field_id'),
			'templateFieldAttributes' => array(self::MANY_MANY, 'ARTemplateFieldAttribute', 'template_field_to_template_field_attribute(template_field_id, template_field_attribute_id)'),
			'templateToTemplateFields' => array(self::HAS_MANY, 'ARTemplateToTemplateField', 'template_field_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'label' => 'Label',
			'description' => 'Description',
			'is_visible' => 'Is Visible',
			'is_cacheable' => 'Is Cacheable',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('is_visible',$this->is_visible);
		$criteria->compare('is_cacheable',$this->is_cacheable);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ARTemplateField the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

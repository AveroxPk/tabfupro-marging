<?php

/**
 * This is the model class for table "page_version".
 *
 * The followings are the available columns in table 'page_version':
 * @property string $id
 * @property string $page_id
 * @property string $version
 * @property string $content
 * @property integer $is_published
 * @property integer $is_scheduled
 * @property string $publish_time
 * @property string $creation_time
 * @property integer $is_fully_cached
 *
 * The followings are the available model relations:
 * @property PageField[] $pageFields
 * @property Page $page
 */
class ARPageVersion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'page_version';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_id, version, content', 'required'),
			array('is_published, is_scheduled, is_fully_cached', 'numerical', 'integerOnly'=>true),
			array('page_id, version', 'length', 'max'=>10),
			array('publish_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, page_id, version, content, is_published, is_scheduled, publish_time, creation_time, is_fully_cached', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pageFields' => array(self::HAS_MANY, 'ARPageField', 'page_version_id'),
			'page' => array(self::BELONGS_TO, 'ARPage', 'page_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page_id' => 'Page',
			'version' => 'Version',
			'content' => 'Content',
			'is_published' => 'Is Published',
			'is_scheduled' => 'Is Scheduled',
			'publish_time' => 'Publish Time',
			'creation_time' => 'Creation Time',
			'is_fully_cached' => 'Is Fully Cached',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('page_id',$this->page_id,true);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('is_published',$this->is_published);
		$criteria->compare('is_scheduled',$this->is_scheduled);
		$criteria->compare('publish_time',$this->publish_time,true);
		$criteria->compare('creation_time',$this->creation_time,true);
		$criteria->compare('is_fully_cached',$this->is_fully_cached);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ARPageVersion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

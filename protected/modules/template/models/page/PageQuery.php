<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PageQuery
 *
 * @author Adam
 */
class PageQuery
{

    public function getPageFromRepositoryByManagingTab(UserTabs $tab)
    {
        return $this->getPageFromRepositoryById($tab->getRelated('pages')->id);
    }

    public function getPageFromRepositoryByManagingTabAndVersion(UserTabs $tab, $version)
    {
        return $this->getPageFromRepositoryByIdAndVersion($tab->getRelated('pages')->id, $version);
    }

    public function getPageFromRepositoryByManagingTabInPublishedVersion(UserTabs $tab)
    {
        return $this->getPageInPublishedVersionFromRepository($tab->getRelated('pages')->id);
    }


    /**
     *
     * @param int $id
     * @return Page
     */
    public function getPageFromRepositoryById($id)
    {
        return $this->_getPageVersionFromRepositoryById($id);

    }

    /**
     *
     * @param int $id
     * @param int $version
     * @return Page
     * @throws PageException
     */
    public function getPageFromRepositoryByIdAndVersion($id, $version)
    {
        try
        {
            $page = $this->_getPageVersionFromRepositoryById($id, FALSE, $version);

        } catch (PageException $exc)
        {
            throw new PageException('Requested version not found');
        }

        return $page;
    }

    /**
     *
     * @param in $id
     * @return Page
     * @throws PageException
     */
    public function getPageInPublishedVersionFromRepository($id)
    {
        try
        {
            $page = $this->_getPageVersionFromRepositoryById($id, TRUE);
        } catch (PageException $exc)
        {
            throw new PageException('No published version has beed found for page');
        }

        return $page;

    }

    /**
     *
     * @param int $id
     * @param bool $mustBePublished
     * @param int $version
     * @return \Page
     * @throws PageException
     */
    private function _getPageVersionFromRepositoryById($id, $mustBePublished = FALSE, $version = NULL)
    {
        $page = ARPage::model()->find("id = {$id}");

        if(!$page) {
            throw new PageException('Page not found in repository');
        }

        $versionCriteria = new CDbCriteria(array(
            'condition' => 'page_id = :pageId',
            'order' => 'version DESC',
            'params' => array(
                ':pageId' => $id
            )
        ));

        if($version) {
            $versionCriteria->addCondition("version = {$version}");
        }

        if($mustBePublished) {
            $versionCriteria->addCondition('is_published = 1');
        }
        else {
            $versionCriteria->addCondition('is_published = 0');
        }

        $lastVersion = ARPageVersion::model()->find($versionCriteria);

        if(!$lastVersion) {
            throw new PageException('No version of page found in repository');
        }

        $isReadOnly = (bool) $lastVersion->is_published;

        $fields = $this->_generateFieldObjects($lastVersion, $isReadOnly);
        $pageObject = new Page( //TODO: refactor longish constructor into 1 associative array parameter
            $page->id, 
            $fields, 
            $lastVersion->content, 
            $lastVersion->version, 
            $lastVersion->is_published, 
            $lastVersion->is_fully_cached, 
            $lastVersion->is_modified, 
            $lastVersion->gates_config
        );

        return $pageObject;
    }

    /**
     * 
     * @param ARPageVersion $version
     * @param bool $isReadOnly
     * @return \Field
     */
    private function _generateFieldObjects(ARPageVersion $version, $isReadOnly)
    {
        $fields = $version->pageFields;

        $pageFields = [];
        foreach ($fields as $field) {

            $attributes = $this->_generateAttributeObjects($field);
            $pageFields[] = new Field(
                $field->id,
                $field->templateField->name,
                $attributes,
                $field->templateField->is_cacheable,
                $field->templateField->label,
                $isReadOnly,
                $field->is_used
                );

            
        }

        return $pageFields;
    }

    /**
     * 
     * @param ARPageField $field
     * @return \Attribute
     */
    private function _generateAttributeObjects(ARPageField $field)
    {
        $attributes = $field->pageFieldAttributes;

        $pageFieldAttributes = [];

        /** @var ARPageFieldAttribute $attribute */
        foreach ($attributes as $attribute) {

            $pageFieldAttributes[] = new Attribute(
                $attribute->id, 
                $attribute->templateFieldAttribute->name, 
                AbstractDataType::createDataTypeModel($attribute->templateFieldAttribute->data_type), 
                $attribute->value, 
                $attribute->templateFieldAttribute->label,
                null,
                $attribute->templateFieldAttribute->custom_edit_widget_class,
                $attribute->templateFieldAttribute->hidden
            );
            
        }

        return $pageFieldAttributes;
    }
}

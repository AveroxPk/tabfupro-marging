<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * PageManager is used to create new pages and manage them
 *
 * @author Adam
 */
class AdManager extends AbstractManager
{
    private $_transaction;
    private $_transactionCreator;
    
    
    /**
     * Allows to create new Page model object that is linked to 
     * records in repository. Initial version, all fields with attributes 
     * containing default values are created as well.
     *
     * @param ARTemplate $template ActiveRecord object of Template
     * @param string $name Name that will be ussed for Page
     * @return Page
     * @throws PageCreationException
     */
    public function createNewPageFromTemplate(Ad $template, $name)
    {
        /*
         * Begin transaction and set transactionCreator to
         * current method. Transaction creator is required to
         * determine if other methods are allowed to start new transactions
         * or use already created one 
         */
        $this->_transaction = Yii::app()->db->beginTransaction();
        $this->_transactionCreator = __METHOD__;

        /**
         * Validate if $name is not empty string
         */
        if($name === '' || ctype_space($name)) {
            throw new PageCreationException('Name cannot be empty');
        }

        /**
         * Setup Page attributes
         */
        $page = new AdPage();
        $page->ad_id = $template->id;
        $page->name = $name;
        $page->user_ads_id = $this->_managingTab->id;

        if(!$page->save()) {
            $this->_transaction->rollback();
            throw new PageCreationException('New Page cannot be created');
        }

        /**
         * Create Page model from Active Record
         */
        $newPageObject = new Page($page->id, [], $template->content);

        /**
         * Create and get page in Version 1
         */
        $version = $this->addVersionToPage($newPageObject);
        

        $this->_transaction->commit();
        $this->_transaction = NULL;

        


        return $version;


    }
    
    public function createPageFromPage(Page $page, $tabId)
    {
        $this->_transaction = Yii::app()->db->beginTransaction();
        $this->_transactionCreator = __METHOD__;
        
        $oldPageAr = AdPage::model()->find("id = {$page->getId()}");
        
        
        $newPageAr = new AdPage();
        $newPageAr->ad_id = $oldPageAr->ad_id;
        $newPageAr->name = $oldPageAr->name;
        $newPageAr->user_ads_id = $tabId;
        
        if(!$newPageAr->save()) {
            $this->_transaction->rollback();
            throw new PageCreationException('Cannot create copy of the page');
        }
        
        $copiedPageObject = new Page($newPageAr->id, [], $oldPageAr->ad->content);

        $this->_transaction->commit();
        $this->_transaction = NULL;

        return $this->addVersionToPage($page, $copiedPageObject);
        
        
    }



    /**
     * Adds new version to existing Page $page.
     * Passed Page object must be linked to repository.
     * All fields with attributes are created as well. 
     * For initial version default values of attributes are used.
     *
     * @param Page $page
     * @return \Page
     * @throws VersionCreationException
     * @throws ManagerPermissionsException
     */
    public function addVersionToPage(Page $page, Page $overridePageForCopy = NULL)
    {
        if(!$this->_transaction) {
            $this->_transaction = Yii::app()->db->beginTransaction();
            $this->_transactionCreator = __METHOD__;
        }
        
        $arPage = AdPage::model()->find("id = {$page->getId()}");
        if($overridePageForCopy) {
            $arOldPage = AdPage::model()->find("id = {$overridePageForCopy->getId()}");
        }
        
        if(!$arPage) {
            throw new VersionCreationException('Vesrion cannot be added to page that is not present in repository');
        } elseif (!$this->assertManagingPermissions($overridePageForCopy ? $arOldPage : $arPage)) {
            throw new ManagerPermissionsException('Tab specified as managing Tab is not permited to manage this Page');
        }

        $pageId = $overridePageForCopy ? $overridePageForCopy->getId() : $page->getId();
        $newVersionNumber = $this->_getNewVersionNumberForPage($overridePageForCopy ? $overridePageForCopy : $page);
        
        $previousVersion = null;
        if ($overridePageForCopy) {
            $previousVersion = AdPageVersion::model()->with('pageFields')->findByAttributes(array(
                'page_id' => $page->getId(),
                'version' => 1 // @fix Get last version of copied page not first
            ));
        } else if ($newVersionNumber > 1) {
            $previousVersion = AdPageVersion::model()->with('pageFields')->findByAttributes(array(
                'page_id' => $pageId,
                'version' => $newVersionNumber - 1
            ));
        }
        
        $version = new AdPageVersion();
        $version->page_id         = $pageId;
        $version->version         = $newVersionNumber;
        $version->content         = $previousVersion ? $previousVersion->content : $arPage->ad->content;
        $version->is_published    = 0;
        $version->is_scheduled    = 0;
        $version->is_fully_cached = 0;

        if(!$version->save()) {
            $this->_transaction->rollback();
            Throw new VersionCreationException('Cannot create version of the page');
        }

        if ($previousVersion) {
            $versionFields = $this->_rewriteVersionFields($previousVersion, $version);
        } else {
            $versionFields = $this->_initFirstVersionFields($arPage->ad, $version);
        }

        $version->content = str_replace($versionFields['fieldsMap']['old'], $versionFields['fieldsMap']['new'], $version->content);

        if(!$version->save()) {
            $this->_transaction->rollback();
            throw new VersionCreationException('Cannot create version from fields');
        }

        $newVersionOfPage = new Page($page->getId(), $versionFields['fieldList'], $version->content, $version->version);
        

        if($this->_transactionCreator === __METHOD__) {
            $this->_transaction->commit();
            $this->_transaction = NULL;
        }

        return $newVersionOfPage;
    }
    
    public function publishLastPageVersion(Page $page, PageRenderer $renderer = NULL)
    {
        //Find current page version
        $currentId = $page->getId();
        $versionCriteria = new CDbCriteria(array(
            'condition' => 'page_id = :pageId',
            'order' => 'version DESC',
            'params' => array(
                ':pageId' => $currentId
            )
        ));
        $currentVersionAr = AdPageVersion::model()->find($versionCriteria);
        
        //Create new version before modifing old one due to caching logic which
        //destroys essential information about fields, namely their placement 
        //in the template's content
        $this->addVersionToPage($page);
        
        //Now we can begin publishing proccess
        $transaction = Yii::app()->db->beginTransaction();

        try {
            AdPageVersion::model()->updateAll(['is_published' => 0], "page_id = {$currentId}");
        } catch (Exception $exc)
        {
            $transaction->rollback();
            throw new PageException("Cannot publish version - {$exc->getMessage()}");
        }


        if($renderer) {
            $currentVersionAr->content = $renderer->renderPage($page, TRUE);
        }
        
        $currentVersionAr->is_published = 1;
        $currentVersionAr->publish_time = date('y-m-d H:i:s');

        


        //Get fields (from current version) that are cacheable
        $fieldUpdateCriteria = new CDbCriteria(array(
            'alias' => 'pf',
            'with' => array(
                'adField' => array(
                    'alias' => 'tf'
                )
            ),
            'condition' => "pf.page_version_id = :pageId AND tf.is_cacheable = 1",
            'params' => array(
                ':pageId' => $currentVersionAr->id,

            )
        ));

        $fields = AdPageField::model()->findAll($fieldUpdateCriteria);

        foreach($fields as $field) {
            $field->is_used = 0;
            if(!$field->save()) {
                $transaction->rollback();
                throw new PageException('Cannot publish version');
            }
        }

        if(count($fields) === (int) AdPageField::model()->count("page_version_id = {$currentVersionAr->id}")) {
            $currentVersionAr->is_fully_cached = 1;
        }

        

        if(!$currentVersionAr->save()) {
            $transaction->rollback();
            throw new PageException('Cannot publish version');
        }

        $transaction->commit();



    }

    /**
     * Get new version number for next page version.
     * If no version exists inital value of "1" is
     * returned.
     *
     * @param Page $page
     * @return int
     */
    private function _getNewVersionNumberForPage(Page $page)
    {
        $lastVersionCriteria = new CDbCriteria(array(
            'select' => 'version',
            'condition' => 'page_id = :pageId',
            'order' => 'version DESC',
            'params' => array(
                ':pageId' => $page->getId()
            )
        ));

        $lastVersion = AdPageVersion::model()->find($lastVersionCriteria);

        $newVersionNumber = ($lastVersion) ? ++$lastVersion->version : 1;

        return $newVersionNumber;
    }

    
    /**
     * Copies fields with their attributes from one version to another.
     * @param ARPageVersion $oldVersion
     * @param ARPageVersion $newVersion
     */
    private function _rewriteVersionFields(AdPageVersion $oldVersion, AdPageVersion $newVersion)
    {
        $fieldList = array();
        $fieldsMap = array(
            'old' => array(),
            'new' => array()
        );
        
        foreach ($oldVersion->pageFields as $oldPageField) {
            
            $attributeObjects = array();
            
            //Clone field to new version
            $newPageField = new AdPageField;
            $newPageField->ad_field_id = $oldPageField->ad_field_id;
            $newPageField->page_version_id = $newVersion->id;
            if (!$newPageField->save()) {
                $this->_transaction->rollback();
                throw new VersionCreationException('Cannot create fields for page version');
            }
            
            //Rewrite attributes values from an old version to the new version
            foreach ($oldPageField->pageFieldAttributes as $oldAttribute) {
                $newAttribute = new AdPageFieldAttribute;
                $newAttribute->page_field_id = $newPageField->id;
                $newAttribute->ad_field_attribute_id = $oldAttribute->ad_field_attribute_id;
                $newAttribute->value = $oldAttribute->value;
                if (!$newAttribute->save()) {
                    $this->_transaction->rollback();
                    throw new VersionCreationException('Cannot create field attributes for page version');
                }
                
                $attributeObject = new Attribute(
                    $newAttribute->id, 
                    $oldAttribute->adFieldAttribute->name, 
                    AbstractDataType::createDataTypeModel($oldAttribute->adFieldAttribute->data_type), 
                    $newAttribute->value, 
                    $oldAttribute->adFieldAttribute->label, 
                    $oldAttribute->adFieldAttribute->adFieldAttributeValidators,
                    $oldAttribute->adFieldAttribute->custom_edit_widget_class, 
                    $oldAttribute->adFieldAttribute->hidden
                );
                $attributeObjects[] = $attributeObject;
                
            }
            
            $fieldObject = new Field(
                $newPageField->id, 
                $newPageField->adField->name, 
                $attributeObjects, 
                $oldPageField->adField->is_cacheable, 
                $oldPageField->adField->label, 
                false,
                $oldPageField->is_used
            );
            
            $fieldList[] = $fieldObject;
            $fieldsMap['old'][] = FieldPlaceholder::createPagePlaceholder($oldPageField->id);
            $fieldsMap['new'][] = FieldPlaceholder::createPagePlaceholder($newPageField->id);
            
        }
        
        return compact('fieldList', 'fieldsMap');
        
    }
    
    /**
     * Initializes first version fields with their attributes.
     * @param ARTemplate $template
     * @param ARPageVersion $version
     */
    private function _initFirstVersionFields(Ad $template, AdPageVersion $version)
    {
        $fieldList = array();
        $fieldsMap = array(
            'old' => array(),
            'new' => array()
        );
        
        foreach ($template->adToAdFields as $templateField) {
            $pageField = new AdPageField;
            $pageField->ad_field_id = $templateField->ad_field_id;
            $pageField->page_version_id = $version->id;
            
            if (!$pageField->save()) {
                $this->_transaction->rollback();
                throw new VersionCreationException('Cannot create fields for page version');
            }
            
            
            //Create PageFieldAttributes and resolve attributes default values 
            //according to this priority:
            //1. default_values from template_to_template_field table
            //2. template_field_attribute->default_value
            
            $fieldType = $templateField->adField;
            $templateDefaultValues = json_decode($templateField->default_values, true) ?: array();
            
            $attributeObjects = array();
            foreach ($fieldType->adFieldAttributes as $templateFieldAttribute) {
                $attribute = new AdPageFieldAttribute;
                $attribute->page_field_id = $pageField->id;
                $attribute->ad_field_attribute_id = $templateFieldAttribute->id;
                $attribute->value = 
                    array_key_exists($templateFieldAttribute->name, $templateDefaultValues) ?
                        $templateDefaultValues[$templateFieldAttribute->name] : //Default value priority 1
                        $templateFieldAttribute->default_value                  //Default value priority 2
                ;
                
                if (!$attribute->save()) {
                    $this->_transaction->rollback();
                    throw new VersionCreationException('Cannot create fields for page version');
                }
                
                //Init Attribute model (not active record entity)
                try {
                    $attributeObject = new Attribute(
                        $attribute->id,
                        $templateFieldAttribute->name,
                        AbstractDataType::createDataTypeModel($templateFieldAttribute->data_type),
                        $attribute->value,
                        null,
                        null,
                        $templateFieldAttribute->custom_edit_widget_class
                    );
                } catch (AttributeValidationException $exc) {
                
                    $this->_transaction->rollback();
                    throw new VersionCreationException('Cannot setup attributes - validation error');

                } catch (AttributeCreationException $aexc) {

                    $this->_transaction->rollback();
                    throw new VersionCreationException('Cannot setup attributes');

                }
                
                $attributeObjects[] = $attributeObject;
                
            }
            
            $fieldList[] = new Field(
                $pageField->id, 
                $fieldType->name, 
                $attributeObjects, 
                $fieldType->is_cacheable
            );
            
            $fieldsMap['old'][] = FieldPlaceholder::createTemplatePlaceholder($templateField->id);
            $fieldsMap['new'][] = FieldPlaceholder::createPagePlaceholder($pageField->id);
            
        }
        
        return compact('fieldList', 'fieldsMap');
        
    }
    
    
    

    /**
     *
     * @param ARTemplate $template
     * @param ARPageVersion $version
     * @return type
     * @throws VersionCreationException
     */
    private function _oldCreateVersionFields(AdTemplate $template, AdPageVersion $version)
    {
        $templateFields = $template->adToAdFields;

        $fieldsMap = [['old'],['new']];
        $fields    = [];

        foreach($templateFields as $templateField) {


            $fieldData = $templateField->adField;

            $pageField = new AdPageField();
            $pageField->ad_field_id = $fieldData->id;
            $pageField->page_version_id = $version->id;

            if(!$pageField->save()) {

                $this->_transaction->rollback();
                throw new VersionCreationException('Cannot create fields for page version');
            }

            if($version->version == 1) {
                $fieldAttributes = $this->_createInitialAttributesForField($pageField, json_decode($templateField->default_values, true));
            } else {
                $fieldAttributes = $this->_createAttributesForField($pageField);
            }
            

            $fields[] = new Field($pageField->id, $fieldData->name, $fieldAttributes, $fieldData->is_cacheable);

            $fieldsMap['old'][] = FieldPlaceholder::createTemplatePlaceholder($templateField->id); 
            $fieldsMap['new'][] = FieldPlaceholder::createPagePlaceholder($pageField->id); 



        }

        return [
            'fieldList' => $fields,
            'fieldsMap' => $fieldsMap
            ];
    }

    /**
     * 
     * @param ARPageField $field
     * @param array $defaultValues
     * @return \Attribute
     * @throws VersionCreationException
     */
    private function _createInitialAttributesForField(AdPageField $field, array $defaultValues = NULL)
    {
        $fieldAttributesData = $field->adField->adFieldAttributes;

        

        $pageFieldAttributes = [];
        foreach($fieldAttributesData as $singleFieldAttributeData) {
            $attribute = new AdPageFieldAttribute();
            $attribute->page_field_id = $field->id;
            $attribute->ad_field_attribute_id = $singleFieldAttributeData->id;



            if(isset($defaultValues[$singleFieldAttributeData->name])) {
                $attribute->value = $defaultValues[$singleFieldAttributeData->name];
            } else {
                $attribute->value = $singleFieldAttributeData->default_value;
            }

            

            If(!$attribute->save()) {
                $this->_transaction->rollback();
                throw new VersionCreationException('Initial attribute cannot be created');
            }

            try
            {
                $attributeObject = new Attribute(
                    $attribute->id,
                    $singleFieldAttributeData->name,
                    AbstractDataType::createDataTypeModel($singleFieldAttributeData->data_type),
                    $attribute->value,
                    null,
                    null,
                    $singleFieldAttributeData->custom_edit_widget_class
                );

            } catch (AttributeValidationException $exc) {
                
                $this->_transaction->rollback();
                throw new VersionCreationException('Cannot setup attributes - validation error');

            } catch (AttributeCreationException $aexc) {
                
                $this->_transaction->rollback();
                throw new VersionCreationException('Cannot setup attributes');
                
            }



            
            $pageFieldAttributes[] = $attributeObject;
        }

        return $pageFieldAttributes;
    }

    private function _oldCreateAttributesForField(AdPageField $field)
    {
        $fieldAttributesData = $field->adField->adFieldAttributes;
        $pageVersion = $field->pageVersion;
        $previousVersion = $pageVersion->version - 1;


        $pageFieldAttributes = [];

        foreach($fieldAttributesData as $singleFieldAttributeData) {

            $oldValueCriteria = new CDbCriteria(array(
                'alias' => 'a',
                'select' => 'a.value',
                'with' => array(
                    'pageField' => array(
                        'alias' => 'pf',
                        'select' => 'pf.id',
                        'with' => array(
                            'pageVersion' => array(
                                'alias' => 'pv',
                                'select' => 'pv.version'
                        ))
                    )
                ),
                'condition' => 'pv.version = :version AND a.ad_field_attribute_id = :adFieldId AND pv.page_id = :pageId',

                'params' => array(
                    ':version' => $previousVersion,
                    ':adFieldId' => $singleFieldAttributeData->id,
                    ':pageId' => $field->pageVersion->page_id
                )

            ));

            $oldAttribute = AdPageFieldAttribute::model()->find($oldValueCriteria);
            

            $attributre = new AdPageFieldAttribute();
            $attributre->page_field_id = $field->id;
            $attributre->ad_field_attribute_id = $singleFieldAttributeData->id;
            $attributre->value = $oldAttribute->value;

            if(!$attributre->save())
            {
                $this->_transaction->rollback();
                throw new VersionCreationException('Cannot rewrite attributes from previous version');
            }

            $attributeObject = new Attribute(
                $attributre->id, 
                $singleFieldAttributeData->name, 
                AbstractDataType::createDataTypeModel($singleFieldAttributeData->data_type), 
                $attributre->value,
                null,
                null,
                $singleFieldAttributeData->custom_edit_widget_class
            );
            $pageFieldAttributes[] = $attributeObject;

        }

        return $pageFieldAttributes;

    }

    public static function getPreviewPage(Ad $template)
    {
        $fields = array();
        foreach ($template->adToAdFields as $arfield) {
            if (strpos($arfield->adField->name, 'Gate') !== false) continue;
            $attributes = array();
            foreach ($arfield->adField->adFieldAttributes as $attr) {
                if (in_array($attr->name, array('imageAsset', 'iconImage', 'Template02GateField'))) continue;
                $defaultValue = json_decode($arfield->default_values, true);
                if (is_array($defaultValue)) {
                    $defaultValue = current($defaultValue);
                }
                $attributes[] = new Attribute(
                    $attr->id,
                    $attr->name,
                    AbstractDataType::createDataTypeModel($attr->data_type),
                    $defaultValue,
                    null,
                    null,
                    $attr->custom_edit_widget_class,
                    $attr->hidden);
            }
            $fields[$arfield->id] = new Field($arfield->id, $arfield->adField->name, $attributes, false, null, true, true);
        }

        $newPageObject = new Page(0, $fields, $template->content, 1, false, false, false);
        return $newPageObject;
    }



}

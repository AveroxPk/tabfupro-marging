<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AbstractRepository
 *
 * @author Adam
 */
abstract class AbstractRepositoryAdapter
{
    /**
     *
     * @var array
     */
    protected $_config;

    /**
     *
     * @param array $config
     */
    public function __construct(array $config = NULL)
    {
        $this->_config = $config;
        $this->init();
    }

    /**
     * Repository initialization - called by constructor
     */
    public function init()
    {
        
    }

    public function getName()
    {
        return get_class($this);
    }

    
    abstract public function getFilePath($file);
    abstract public function getFileNameFromPath($path);
    abstract public function getFileUrl($file);
    abstract public function getFileNameFromUrl($url);

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LocalRepository
 *
 * @author Adam
 */
class LocalRepositoryAdapter extends AbstractRepositoryAdapter
{

    private $_path;
    private $_url;

    public function init()
    {
        if(!isset($this->_config['pattern'])) {
            throw new RepositoryConfigException('Pattern is missing in Repository config');

        } elseif (!isset ($this->_config['pattern']['path'])) {
            throw new RepositoryConfigException('Pattern must contain path info');

        } elseif (!isset ($this->_config['pattern']['url'])) {
            throw new RepositoryConfigException('Pattern must contain path url');

        }

        $path = realpath($this->_config['pattern']['path']);

//echo $path; exit;
        if(!$path) {
            throw new RepositoryException('Provided pattern is not valid or existing path in locale filesystem', $this);

        } elseif (!is_dir($path)) {
            throw new RepositoryException('Provided pattern is not valid path for direcotry', $this);

        } elseif (!is_writable($path)) {
            throw new RepositoryException('Provided pattern is path to writeable directory', $this);
            
        }

        $this->_path = $path;
        $this->_url  = $this->_config['pattern']['url'];

    }

    public function getFileNameFromPath($path)
    {
        return str_replace("{$this->_path}/", '', realpath($path));
    }

    public function getFilePath($file)
    {


        $filePath = "{$this->_path}/{$file}";

        if(!file_exists($filePath)) {
            throw new RepositoryException("File: '{$file}' does not exist in configured path", $this);

        } elseif (!is_readable($filePath)) {
            throw new RepositoryException("File: '{$file}' is not readable", $this);
            
        }

        return $filePath;

       
    }

    public function getFileNameFromUrl($url)
    {

    }

    public function getFileUrl($file)
    {
        return "{$this->_url}/$file";
    }




}

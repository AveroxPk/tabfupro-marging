<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RepositoryException
 *
 * @author Adam
 */
class RepositoryException extends Exception
{
    public function __construct($message, AbstractRepositoryAdapter $adapter)
    {
        
        parent::__construct("{$adapter->getName()}: {$message}");
    }

    

}

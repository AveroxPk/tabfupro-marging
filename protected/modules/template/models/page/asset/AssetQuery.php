<?php

class AssetQuery
{
    private $_repositoryAdapter;
    private $_pageId;
    
    public function __construct($repositoryAdapter, $pageId = null) 
    {
        $this->_repositoryAdapter = $repositoryAdapter;
        $this->_pageId = $pageId;
    }
    
    private function _convertARToAsset(ARPageFile $ar)
    {
        $assetFile = new AssetFile($ar->server_filename, CFileHelper::getExtension($ar->server_filename));
        $asset = new Asset($ar->id, $ar->original_filename, 0, $assetFile, $this->_repositoryAdapter);
        
        return $asset;
    }
    
    public function getAssetById($id)
    {
        if ($this->pageId === null) {
            throw new CException('Page id must be defined when quering for assets by their ids');
        }
        
        return $this->_convertARToAsset(
            ARPageFile::model()->findByAttributes(array(
                'id' => $id,
                'page_id' => $this->pageId
            ))
        );
    }
    
    public function getAssetByServerFilename($serverFilename)
    {
        return $this->_convertARToAsset(
            ARPageFile::model()->findByAttributes(array(
                'server_filename' => $serverFilename
            ))
        );
    }

}
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssetFile
 *
 * @author Adam
 */
class AssetFile
{
    private $_fileName;
    private $_extension;

    function __construct($fileName, $extension)
    {
        $this->_fileName = $fileName;
        $this->_extension = $extension;

    }


    public function getFileName()
    {
        return $this->_fileName;
    }

    public function getExtension()
    {
        return $this->_extension;
    }

}

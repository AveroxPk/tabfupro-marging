<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Asset
 *
 * @author Adam
 */
class Asset implements JsonSerializable
{
    /**
     *
     * @var int
     */
    private $_id;

    /**
     *
     * @var string
     */
    private $_name;

    /**
     *
     * @var int
     */
    private $_creationTime;

    /**
     *
     * @var type
     */
    private $_file;
    private $_repositoryAdapter;

    function __construct($id, $name, $creationTime, AssetFile $file, AbstractRepositoryAdapter $repositoryAdapter)
    {
        $this->_id = $id;
        $this->_name = $name;
        $this->_creationTime = $creationTime;
        $this->_file = $file;
        $this->_repositoryAdapter = $repositoryAdapter;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getCreationTime()
    {
        return $this->_creationTime;
    }

    public function getPath()
    {
        return $this->_repositoryAdapter->getFilePath($this->_file->getFileName());
    }
    
    public function getUrl()
    {
        return $this->_repositoryAdapter->getFileUrl($this->_file->getFileName());
    }
    
    public function getServerFilename()
    {
        return $this->_file->getFileName();
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->_id,
            'name' => $this->_name,
            'creationTime' => $this->_creationTime,
            //'path' => $this->getPath(),
            'serverFilename' => $this->getServerFilename(),
            'url' => $this->getUrl()
        );
    }

}

<?php
Yii::import('application.modules.template.widgets.fields.TemplateFieldWidget');
Yii::import('application.modules.template.widgets.fields.gateField.GateField');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author Adam
 */
class Page
{
    /**
     *
     * @var int
     */
    private $_id;

    /**
     *
     * @var Field[]
     */
    private $_fields;

    /**
     *
     * @var string
     */
    private $_content;

    /**
     *
     * @var int
     */
    private $_version;

    /**
     *
     * @var bool
     */
    private $_isPublished;
    
    /**
     *
     * @var bool
     */
    private $_isStatic;

    /**
     *
     * @var bool
     */
    private $_isModified;
    
    private $_gatesConfig;

    /**
     * 
     * @param int $id
     * @param array $fields
     * @param string $content
     * @param int $version
     * @param bool $isPublished
     * @throws PageCreationException
     */
    public function __construct($id, array $fields, $content, $version = 1, $isPublished = FALSE, $isStatic = FALSE, $isModified = FALSE, $gatesConfig = '')
    {
        if(!is_numeric($id)) {
            throw new PageCreationException('Id must be numeric');
        }

        if(!is_numeric($version) || $version < 1) {
            throw new PageCreationException('Version must be numeric');
        }
        
        $this->_id          = (int) $id;
        $this->_version     = (int) $version;
        $this->_isPublished = boolval($isPublished);
        $this->_isModified  = boolval($isModified);
        $this->_gatesConfig = $gatesConfig;

        $unusedFieldsCount = 0;
        if(!$isStatic)
        {
            foreach($fields as $field) {
                if($field instanceof Field && $field->isUsed()) {

                    $this->_fields[$field->getId()] = $field;

                } elseif($field instanceof Field && !$field->isUsed()) {

                    $unusedFieldsCount++;

                } else {

                    throw new PageCreationException('One of the passed fields is not proper field object');

                }
            }

            if(count($fields) !== count($this->_fields)+$unusedFieldsCount) {
                throw new PageCreationException('Some of the fields has not beed added');
            }
        }
        if($isStatic || $unusedFieldsCount === count($fields)) {
            $this->_isStatic = TRUE;
        } else {
            $this->_isStatic = FALSE;
        }

        
        $this->_content = $content;
    }

    /**
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     *
     * @return array
     */
    public function getFields()
    {
        if($this->_isStatic) {
            throw new PageException('Access for fields is forbidden for static pages');
        }
        return $this->_fields;
    }

    /**
     *
     * @param int $id
     * @return Field
     * @throws PageException
     */
    public function getField($id)
    {
        if($this->_isStatic) {
            throw new PageException('Access for fields is forbidden for static pages');
        } elseif (isset($this->_fields[$id])) {
            return $this->_fields[$id];
        } else {
            throw new PageException('Requested field don\'t exist');
        }
    }

    /**
     *
     * @return string
     */
    public function getContent()
    {
        return $this->_content;
    }

    /**
     *
     * @return int
     */
    public function getVersion()
    {
        return $this->_version;
    }

    /**
     * 
     * @return bool
     */
    public function IsPublished()
    {
        return $this->_isPublished;

    }
    
    /**
     * 
     * @return bool
     */
    public function IsStatic()
    {
        return $this->_isStatic;

    }

    public function isMofified()
    {
        return $this->_isModified;
    }

    
    public function getGatesConfigJSON()
    {
        return $this->_gatesConfig;
    }
    
    public function getGatesConfig()
    {
        $config = json_decode($this->_gatesConfig, true);
        
        //Default custom code gate modal content height
        if (isset($config['customCode']) && !isset($config['customCode']['modalContentHeight'])) {
            $config['customCode']['modalContentHeight'] = GateField::CUSTOM_CODE_GATE_DEFAULT_CONTENT_HEIGHT;
        }
        if (isset($config['customCode']) && !isset($config['customCode']['modalContentWidth'])) {
            $config['customCode']['modalContentWidth'] = GateField::CUSTOM_CODE_GATE_DEFAULT_CONTENT_WIDTH;
        }
        
        return $config;
    }



}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AbstractManager
 *
 * @author agmak_000
 */
abstract class AbstractManager
{
    /**
     *
     * @var UserTabs
     */
    protected $_managingTab;
    
    /**
     * 
     * @param UserTabs $managingTab ActiveRecord object of User Tab on behave 
     * of which Manager will manage Page
     */
    final public function __construct($managingTab)
    {
        
        $this->_managingTab = $managingTab;
    }
    
    /**
     * Check if tab set as Page manager is allowed to manage $page
     * 
     * @param ARPage $page
     * @return boolean
     */
    protected function assertManagingPermissions($page)
    {
        if ($page instanceof ARPage) {
            return $page->user_tabs_id == $this->_managingTab->id;
        }
        elseif ($page instanceof AdPage) {
            return $page->user_ads_id == $this->_managingTab->id;
        }

    }

}

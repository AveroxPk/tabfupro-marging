<?php

class TemplateImportForm extends CFormModel
{
    public $templateName;
    public $zipFile;
    
    public function rules()
    {
        return array(
            array('templateName, zipFile', 'required'),
            array('zipFile', 'file', 'mimeTypes' => 'application/zip')
        );
    }
    
    public function attributeLabels() 
    {
        return array(
            'templateName' => 'Template name',
            'zipFile' => 'Template ZIP file'
        );
    }
    
}
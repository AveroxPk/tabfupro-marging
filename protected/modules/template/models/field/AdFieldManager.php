<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FieldManager
 *
 * @author Adam
 */
class AdFieldManager extends AbstractManager
{
    
    /**
     * Updates attributes records in repository and returns new Field object.
     * 
     * @param Field $field
     * @param array $updatedAttributes
     * @return Field
     * @throws FieldException
     * @throws ManagerPermissionsException
     */
    public function updateFieldInRepository(Field $field, array $updatedAttributes)
    {
        /**
         * Restrict update to fields that belong to unpublished versions of the page.
         */
        if($field->isReadOnly()) {
            throw new FieldException('Cannot update field that belongs to published page version');
        }
        
        $fieldAr = AdPageField::model()->find("id = {$field->getId()}");
        $pageManagingTab = $fieldAr->pageVersion->page;
        $pageVersion = $fieldAr->pageVersion;


        
        if (!$this->assertManagingPermissions($pageManagingTab)) {
            throw new ManagerPermissionsException('Tab specified as managing Tab is not permited to update fields in this Page');
        }

        $pageVersion->is_modified = 1;
        $pageVersion->modified_time = date('y-m-d H:i:s');

        if(!$pageVersion->save()) {
            throw new FieldException('Cannot update version values in repository');
        }

        $oldAttributes = $field->getAttributes();
        $newAttributes = [];


        foreach ($updatedAttributes as $attributeName => $value) {

            $oldAttributeObject = $oldAttributes[$attributeName];

            /**
             * Assignment for improved readability
             */
            $newId       = $oldAttributeObject->getId();
            $newDataType = $oldAttributeObject->getDataType();
            $newLabel    = $oldAttributeObject->getLabel();
            $newName     = $attributeName;
            $newValue    = $value;
            $customEditWidgetClass = $oldAttributeObject->getCustomEditWidgetClass();
            
            /**
             * Create ActiveRecord to get required validators and prepare for 
             * update in repository.
             */
            $attributeRecord = AdPageFieldAttribute::model()->find("id = {$newId}");
            $validatorList = $attributeRecord
                             ->adFieldAttribute
                             ->adFieldAttributeValidators;
            
            $validators = [];

            foreach($validatorList as $validatorData) {
                
                $validatorOptions = json_decode($validatorData->validator_data, TRUE);
                $validators[] = new $validatorData->validator($validatorOptions);
                
            }
            
            
            $newAttributeObject = new Attribute(
                    $newId, 
                    $newName, 
                    $newDataType, 
                    $newValue, 
                    $newLabel, 
                    $validators,
                    $customEditWidgetClass
                    );
            
            $newAttributes[$attributeName] = $newAttributeObject;
            
            $attributeRecord->value = $newAttributeObject->getValueAsString();
            if(!$attributeRecord->save()) {
                throw new FieldException('Cannot update attribute value in repository');
            }

        }

        $mergedAttributes = array_merge($oldAttributes, $newAttributes);

        return new Field(
                $field->getId(), 
                $field->getName(), 
                $mergedAttributes, 
                $field->isCacheable()
                );


    }
}

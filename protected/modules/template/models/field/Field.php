<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Field
 *
 * @author Adam
 */
class Field
{
    /**
     *
     * @var int
     */
    private $_id;

    /**
     *
     * @var string
     */
    private $_description;

    /**
     *
     * @var string
     */
    private $_name;

    /**
     *
     * @var string
     */
    private $_label;

    /**
     *
     * @var bool
     */
    private $_isCacheable;

    /**
     *
     * @var AbstractAttribute[]
     */
    private $_attributes;

    /**
     *
     * @var bool
     */
    private $_isReadOnly;
    
    /**
     *
     * @var bool
     */
    private $_isUsed;

    /**
     *
     * @param int $id
     * @param string $name
     * @param array $attributes
     * @param bool $isCacheable
     * @param string $label
     * @param bool $isReadOnly
     * @param bool $isUsed
     * @throws FieldCreationException
     */
    public function __construct($id, $name, array $attributes, $isCacheable = FALSE, $label = NULL, $isReadOnly = FALSE, $isUsed = TRUE)
    {
        if(!is_numeric($id)) {
            throw new FieldCreationException('Id must be numeric');
        }
        
        if($name === '' || ctype_space($name)) {
            throw new FieldCreationException('Name cannot be empty');
        }
        
        $this->_id = $id;
        $this->_name = $name;
        $this->_isCacheable = (bool) $isCacheable;
        $this->_label = ($label === NULL) ? $name : $label;
        $this->_isReadOnly = (bool) $isReadOnly;
        $this->_isUsed = (bool) $isUsed;

        /** @var AbstractAttribute $attribute*/
        foreach ($attributes as $attribute) {
            if($attribute instanceof Attribute) {
                $this->_attributes[$attribute->getName()] = $attribute;
            } else {
                throw new FieldCreationException('One of the attributes is not proper Attribute object');
            }
        }

        if(count($attributes) !== count($this->_attributes)) {
            throw new FieldCreationException('Some attributes has not been added');
        }
    }

    /**
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }

    public function getAttribute($name)
    {
        if(isset($this->_attributes[$name])) {
            return $this->_attributes[$name];
        } else {
            throw new FieldException('Requested attribute don\'t exist');
        }
    }
    
    /**
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->_label;
    }

    /**
     *
     * @return bool
     */
    public function isCacheable()
    {
        return $this->_isCacheable;
    }

    public function isReadOnly()
    {
        return $this->_isReadOnly;
    }
    
    public function isUsed()
    {
        return $this->_isUsed;
    }


        

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FieldPlaceholder
 *
 * @author agmak_000
 */
class FieldPlaceholder
{

    const FIELD_PLACEHOLDER_PATTERN_OUT   = '{{_%s:%s:%d}}';
    const FIELD_PLACEHOLDER_PATTERN_IN    = '{{_%[^:]%[^:]%d}}';
    const IMPORT_PLACEHOLDER_PATTERN_OUT  = '{{_%s:%s:%s:%s}}';
    const IMPORT_PLACEHOLDER_PATTERN_IN   = '{{_%[^:]:%[^:]:%[^:]:%[^}}]';
    const PLACEHOLDER_NAMESPACE           = 'tabfu';
    const PLACEHOLDER_TYPE_TEMPLATE_FIELD = 'TemplateField';
    const PLACEHOLDER_TYPE_PAGE_FIELD     = 'PageField';
    const PLACEHOLDER_TYPE_IMPORT_FIELD   = 'ImportField';

    /**
     *
     * @param int $data
     * @return string
     */
    public static function createTemplatePlaceholder($data)
    {
        return sprintf(
                self::FIELD_PLACEHOLDER_PATTERN_OUT,
                self::PLACEHOLDER_NAMESPACE,
                self::PLACEHOLDER_TYPE_TEMPLATE_FIELD,
                $data
                );
    }

    /**
     *
     * @param int $data
     * @return string
     */
    public static function createPagePlaceholder($data)
    {
        return sprintf(
                self::FIELD_PLACEHOLDER_PATTERN_OUT,
                self::PLACEHOLDER_NAMESPACE,
                self::PLACEHOLDER_TYPE_PAGE_FIELD,
                $data
                );
    }

    /**
     *
     * @param string $placeholder
     * @return int
     */
    public static function extractFromPlaceholder($placeholder)
    {
        $data = sscanf($placeholder, self::FIELD_PLACEHOLDER_PATTERN_IN);
        return end($data);
    }

    /**
     *
     * @param string $placeholder
     * @return array
     */
    public static function extractFromImportPlaceholder($placeholder)
    {
        $data = sscanf($placeholder, self::IMPORT_PLACEHOLDER_PATTERN_IN);
        
        $fieldData['namespace'] = $data[0];
        $fieldData['fieldName'] = $data[2];

        
        if(!isset($data[3]) || $data[3] === '' || ctype_space($data[3])) {
            $fieldData['defaultAttributesValues'] = NULL;
        } else {
            
            $json = base64_decode($data[3], TRUE);
            $attributesData = json_decode($json, TRUE);
            
            if(!$json || !$attributesData) {
                throw new FieldException('Incorrect base64 value');
            }



            $fieldData['defaultAttributesValues'] = $attributesData;
            
            
        }
        
        
        

        return $fieldData;

    }


    public static function createImportField($name, array $defaultAttributes)
    {
        $json = json_encode($defaultAttributes);
        $base64 = base64_encode($json);

        return sprintf(
            self::IMPORT_PLACEHOLDER_PATTERN_OUT,
            self::PLACEHOLDER_NAMESPACE,
            self::PLACEHOLDER_TYPE_IMPORT_FIELD,
            $name,
            $base64
            );
    }

    /**
     * Creates RegEx which will find all import field placeholders for specific
     * field types.
     * @param array $fieldNames Available field types
     * @return string RegEx
     */
    public static function createImportFieldsRegEx(array $fieldNames)
    {
        return sprintf(
            '(' . self::IMPORT_PLACEHOLDER_PATTERN_OUT . ')',
            self::PLACEHOLDER_NAMESPACE,
            self::PLACEHOLDER_TYPE_IMPORT_FIELD,
            '(' . join('|', array_map('preg_quote', $fieldNames)) . ')',
            '([A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?' //Base64 RegEx rule
        );
    }
    
    
    
}

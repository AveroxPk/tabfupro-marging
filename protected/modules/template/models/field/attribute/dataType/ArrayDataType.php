<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ArrayDataType
 *
 * @author Adam
 */
class ArrayDataType extends AbstractDataType
{
    public function convertDataTypeValueToString($value)
    {
        if(!is_array($value)) {
            $value = [$value];
        }
        return json_encode($value);
    }

    public function convertStringToDataTypeValue($value)
    {
        
        return json_decode($value, TRUE);
    }

}

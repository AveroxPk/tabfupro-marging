<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BoolDataType
 *
 * @author Adam
 */
class BoolDataType extends AbstractDataType
{
    public function convertDataTypeValueToString($value)
    {
        return (string) boolval($value);
    }

    public function convertStringToDataTypeValue($value)
    {
        return boolval($value);
    }

}

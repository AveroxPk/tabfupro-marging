<?php

class ImageadDataType extends AbstractDataType
{
    public function convertDataTypeValueToString($value)
    {
        if (empty($value)) {
            return '';
        }
        
        return $value->getServerFilename();
    }

    public function convertStringToDataTypeValue($value)
    {
        if (empty($value)) {
            return null;
        }
        
        $assetQuery = new AdAssetQuery(Yii::app()->getModule('template')->getRepositoryAdapter());
        return $assetQuery->getAssetByServerFilename($value);
    }

}

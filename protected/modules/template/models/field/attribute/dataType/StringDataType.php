<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StringDataType
 *
 * @author Adam
 */
class StringDataType extends AbstractDataType
{
    public function convertDataTypeValueToString($value)
    {
        return (string) $value;
    }

    public function convertStringToDataTypeValue($value)
    {
        return (string) $value;
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AbstractDataType
 *
 * @author Adam
 */
abstract class AbstractDataType
{
    private $_name;

    public function __construct()
    {

        $this->_name = get_class($this);

    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Converts value from repository to value required by attribute
     */
    abstract public function convertStringToDataTypeValue($value);

    abstract public function convertDataTypeValueToString($value);
    

    public static function createDataTypeModel($dataType)
    {
        $dataTypeClass = ucfirst($dataType) . 'DataType';
        if(!class_exists($dataTypeClass)) {
            throw new DataTypeNotExists("Data type {$dataType} not exists");
        }
        
        return new $dataTypeClass;
    }
    
}

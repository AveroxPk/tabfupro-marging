<?php

class HtmlDataType extends StringDataType
{
    public function convertStringToDataTypeValue($value)
    {
        $p = new CHtmlPurifier();
        $p->options = array(
            'Attr.AllowedFrameTargets' => array(
                '_blank', '_self', '_parent', '_top'
            )
        );
        return (string) $p->purify($value);
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IntDataType
 *
 * @author Adam
 */
class IntDataType extends AbstractDataType
{
    public function convertDataTypeValueToString($value)
    {
        return (int) $value;
    }

    public function convertStringToDataTypeValue($value)
    {
        return (int) $value;
    }

}

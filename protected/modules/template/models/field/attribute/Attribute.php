<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Attribute
 *
 * @author Adam
 */
class Attribute
{
    /**
     *
     * @var int
     */
    private $_id;

    /**
     *
     * @var string
     */
    private $_name;

    /**
     *
     * @var string
     */
    private $_label;

    /**
     *
     * @var AbstractDataType
     */
    private $_dataType;

    /**
     *
     * @var Zend\Validator\ValidatorInterface[]
     */
    private $_validators = [];

    /**
     *
     * @var mixed
     */
    private $_value;
    
    /**
     *
     * @var bool
     */
    private $_hidden;
    
    /**
     *
     * @var string
     */
    private $_customEditWidgetClass;

    /**
     * @param int $id
     * @param string $name
     * @param string $dataType
     * @param mixed $value
     * @param string $label
     * @param array $validators
     * @throws \AttributeValidationException
     */
    function __construct(
        $id,
        $name,
        AbstractDataType $dataType,
        $value,
        $label = NULL,
        array $validators = NULL, 
        $customEditWidgetClass = 'default',
        $hidden = false
    )
    {
        
        if(!is_numeric($id)) {
            throw new \AttributeCreationException('Attribute Id must be numeric');
        }

        /**
         * Check if name parameter is valid
         */
        if($name === '' || ctype_space($name)) {
            throw new \AttributeCreationException('Attribute name cannot be empty');
        }

        /**
         * If validaror/s are provided setyp them in object
         */
        if($validators !== NULL && !empty($validators)) {
            $this->_setupValidators($validators);
        }

        /*
         * Setup properties
         */
        $this->_name = $name;
        $this->_dataType = $dataType;
        $this->_id = $id;
        $this->_label = ($label === NULL) ? $name : $label;
        $this->_customEditWidgetClass = $customEditWidgetClass;
        $this->_hidden = $hidden;

        $convertedValue = $this->_dataType->convertStringToDataTypeValue($value);

        if(!empty($this->_validators)) {
            /*
             * Validate value against all validators
             */
            

            $errors = $this->_getValueValidationErrors($convertedValue);
            if(!empty($errors)) {
                
                throw new AttributeValidationException("Incorrect Value", $errors);
            } else {
                $this->_value = $convertedValue;
            }

        } else {
            /**
             * Set unvalidated value
             */
            $this->_value = $convertedValue;
        }
    }

    /**
     *
     * @param array $validators
     * @return boolean
     * @throws \AttributeCreationException
     */
    private function _setupValidators(array $validators)
    {

        foreach($validators as $index => $validator) {
            if($validator instanceof Zend\Validator\ValidatorInterface) {
                $this->_validators[] = $validator;
            } elseif ($validator instanceof ARTemplateFieldAttributeValidator) {
                $validatorOptions = json_decode($validator->validator_data, true);
                $validatorClass = $validator->validator;

                $this->_validators[] = new $validatorClass($validatorOptions);

            } else {
                throw new \AttributeCreationException("Validator at index {$index} is not CValidator neither ARTemplateFieldAttributeValidator");
            }
        }

        return TRUE;

    }

    /**
     *
     * @param mixed $value
     */
    private function _getValueValidationErrors($value)
    {

        $errors = [];
        /**
         * @var \Zend\Validator\ValidatorInterface $validator
         */
        foreach($this->_validators as $validator) {
            if(!$validator->isValid($value)) {
                $errors[] = $validator->getMessages();
            }
        }

        return $errors;
        
    }
    
    public function getId()
    {
        return $this->_id;
    }

        /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }
    
    /**
     * 
     * @return string
     */
    public function getLabel()
    {
        return $this->_label;
    }


    /**
     * 
     * @return mixed
     */
    public function getValue()
    {
        return $this->_value;
    }
    
    /**
     * 
     * @return string
     */
    public function getValueAsString()
    {
        return $this->_dataType->convertDataTypeValueToString($this->_value);
    }

    public function getDataType()
    {
        return $this->_dataType;
    }

    /**
     * 
     * @return string
     */
    public function getCustomEditWidgetClass()
    {
        return $this->_customEditWidgetClass;
    }

    public function getHidden()
    {
        return $this->_hidden;
    }



}

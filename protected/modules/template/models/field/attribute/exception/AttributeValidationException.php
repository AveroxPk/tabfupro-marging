<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AttributeValidationException
 *
 * @author Adam
 */
class AttributeValidationException extends Exception
{
    private $_errors;

    public function __construct($message, array $errors)
    {
        $this->_errors = $errors;
        parent::__construct($message);
    }

    public function getValidationErrors()
    {
        return $this->_errors;
    }

}

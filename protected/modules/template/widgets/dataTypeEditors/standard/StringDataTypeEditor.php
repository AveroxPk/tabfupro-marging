<?php

class StringDataTypeEditor extends DataTypeEditor
{
    public function run()
    { ?>
        <div class="url-link-holder" style="display:none">
            <div class="left">
                <a href="#" title="<?=CHtml::encode($this->label)?>">
                    <span class="plus-element">+</span>
                    <span class="description"><?=CHtml::encode($this->label)?></span>
                </a>
            </div>
            <div class="right">
                <?php 
                    echo CHtml::textField(
                        $this->name, 
                        $this->value
                    ); 
                ?>
            </div>
        </div><?php
    }
    
    public static function updateLogicJS() 
    {
        return 
            'var textBox = $(this).find("input[type=\'text\']");'
            . 'textBox.val(value); textBox.parents(".url-link-holder").show();'
        ;
    }
}

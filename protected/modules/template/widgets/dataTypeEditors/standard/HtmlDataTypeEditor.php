<?php

class HtmlDataTypeEditor extends DataTypeEditor
{
    public function run()
    {
        echo CHtml::textArea($this->name, $this->value, array(
            'id' => $this->getId()
        ));
        Yii::app()->clientScript
            ->registerScript($this->getId(), "CKEDITOR.replace('{$this->id}', { height: 150, allowedContent: true });");
    }
    
    public static function updateLogicJS() 
    {
        return 'CKEDITOR.instances[$(this).find("textarea").attr("id")].setData(value);';
    }
}

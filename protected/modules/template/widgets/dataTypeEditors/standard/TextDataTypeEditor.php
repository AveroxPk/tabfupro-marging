<?php

class TextDataTypeEditor extends DataTypeEditor
{
    public function run()
    {
        echo CHtml::textArea($this->name, $this->value);
    }
    
    public static function updateLogicJS() 
    {
        return '$(this).find("textarea").val(value);';
    }
}

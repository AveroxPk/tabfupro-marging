<?php

class IntDataTypeEditor extends DataTypeEditor
{
    public function run()
    {
        echo CHtml::numberField($this->name, $this->value);
    }
}
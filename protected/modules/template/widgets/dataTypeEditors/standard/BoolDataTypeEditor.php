<?php

class BoolDataTypeEditor extends DataTypeEditor
{
    public function run()
    {
        echo '<label for="' . $this->name . '" style="display:none">';
        echo CHtml::checkBox($this->name, $this->value, array('uncheckValue' => 0));
        echo ' ';
        echo CHtml::encode($this->label);
        echo '</label>';
    }
    
    public static function updateLogicJS() 
    {
        return 
            'var checkBox = $(this).find("input[type=\'checkbox\']");'
            . 'if (value) {'
                . 'checkBox.attr("checked", "checked");'
            . '} else {'
                . 'checkBox.removeAttr("checked");'
            . '} checkBox.parent().show();'
        ;
    }
}
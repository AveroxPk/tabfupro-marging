<?php

class ImageDataTypeEditor extends DataTypeEditor
{
    public function run()
    {
        $fileInputName = $this->name . uniqid() . '_file';
        
        echo CHtml::hiddenField($this->name, $this->value ? $this->value->getServerFilename() : '', array('class' => $fileInputName));
        echo CHtml::fileField($fileInputName, '', array(
            'class' => 'image-upload',
            'style' => 'display:none',
        )); ?>
        <div class="preview-element">   
            <div class="head">Use a <span class="prefered-image-size"></span> pixel image for best results</div>
            <div class="content" id="<?= $this->getId() ?>" style="text-align:center;position:relative">
                <span class="image-element-hider">
                    <img class="image-preview" src="<?= $this->value ? '/uploads/'.trim($this->value->getServerFilename(), '/') : '/images/tabwizard/drag-file.png' ?>" />
                </span>
                <a href="#" class="delete-image-button"></a>
                <a class="upload-button">Choose</a>
            </div>
        </div>
        
        <?php //Clicking will force file selection dialog
        Yii::app()->clientScript->registerScript($this->id, <<<JS
    (function() {
        var \$input = \$('#$fileInputName'),
            input = \$input[0]
        ;
                
        \$input.change(function() {
            var formData;
                
            $.blockUI({message: "Uploading file... Please wait..."});

            if (input.files.length === 0) {
                return;
            }

            formData = new FormData();
            formData.append('file', input.files[0]);

            $.ajax({
                'url': '/template/pageEditor/uploadImage/tabId/' + tabWizard.getCurrentTabId(),
                'cache': false,
                'contentType': false,
                'processData': false,
                'dataType': 'json',
                'type': 'POST',
                'data': formData,
            })
                .done(function(data) {
                    var \$parent;
                
                    if (data === 'error') {
                        alert("There was an error while image upload.");
                        return;
                    }

                    //Reset file input
                    \$input.val('');
                        
                    \$parent = \$input.parent();
                
                    //Server filename is sent through hidden field
                    //.siblings('.' + input.getAttribute('name'))
                    $("input[type=hidden]", \$parent).val(data.serverFilename);
                
                
                    //$(".no-image-selected", \$parent).hide();
                    //$(".image-selected", \$parent).show();
                    $(".image-preview", \$parent).attr("src", data.url);
                    $(".delete-image-button").show();
                })
                .complete(function() {
                    $.unblockUI();
                });
        });

        $('#{$this->id}').click(function() {
            \$input.click();
        });
        
        $('.delete-image-button').click(function() {
            $(this).parents('.attribute-editor').find('img.image-preview').attr('src', '/images/tabwizard/drag-file.png');
            $(this).parents('.attribute-editor').find('input[type=hidden]').val('');
            $(".delete-image-button").hide();
            return false;
        });
    
    })();
JS
        );
            
    }
    
    public static function updateLogicJS() 
    {
        
        return 
            '$("input[type=\'hidden\']", this).val(value ? value.serverFilename : "");'
            . 'value ? $(".delete-image-button").show() : $(".delete-image-button").hide();'
            . '$(".image-preview", this).attr("src", value ? value.url : "/images/tabwizard/drag-file.png");'
            . 'if (typeof attributes.preferedWidth !== "undefined") {'    
                . '$(this).find(".prefered-image-size").text(attributes.preferedWidth + "x" + attributes.preferedHeight);'
            . '} else {'
                . '$(this).find(".prefered-image-size").parents(".head").hide();'
            . '}'
                
        ;
    }
}
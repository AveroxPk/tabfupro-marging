<?php

class UrlEditor extends DataTypeEditor
{
    public function run()
    {
        ?>
        <div class="url-link-holder">
            <div class="left">
                <a href="#">
                    <span class="plus-element">+</span>
                    <span class="description"><?=CHtml::encode($this->label)?></span>
                </a>
            </div>
            <div class="right">
                <?php 
                    echo CHtml::textField(
                        $this->name, 
                        $this->value, 
                        array(
                            'placeholder' => 'Paste your URL here...',
                        )
                    ); 
                ?>
            </div>
        </div>
        <?php
    }
    
    public static function updateLogicJS() 
    {
        return 
            'var textBox = $(this).find("input[type=\'text\']");'
            . 'textBox.val(value); textBox.parents(".url-link-holder").show();'
        ;
    }
}
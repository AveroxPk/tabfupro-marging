<?php

class DatePicker extends DataTypeEditor
{
    public function run()
    {
        $uniqueId = 'datepicker_' .  uniqid();
        ?>
        <div class="url-link-holder">
            <div class="left">
                <a href="#">
                    <span class="plus-element">+</span>
                    <span class="description"><?=CHtml::encode($this->label)?></span>
                </a>
            </div>
            <div class="right datetime-picker" id="<?= $uniqueId ?>">
                <?php 
                    echo CHtml::textField(
                        $this->name, 
                        $this->value, 
                        array(
                            'placeholder' => 'Please select the date and time...',
                            'data-format' => 'yyyy-MM-dd hh:mm:ss'
                        )
                    ); 
                ?>
                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
            </div>
        </div>
        <?php
        
        Yii::app()->clientScript
            ->registerScript(__CLASS__, "$('#{$uniqueId}').datetimepicker({
                language: 'en',
                pick12HourFormat: true,
                pickTime: true
            });", CClientScript::POS_READY)
            ->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-datetimepicker.min.js');
            
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/bootstrap-datetimepicker.min.css');
    }
    
    /*public static function updateLogicJS() 
    {
        return 
            //Get rid of '#' character as it is already visualised in UI
            'if (typeof value != "undefined" && value != null && value.length > 1 && value.substring(0, 1) === "#") {'
            . '  value = value.substring(1);'
            . '}'
                
            //Update text input + init color preview by triggering change event
            . '$(this).find("input").val(value).trigger("change")'
        ;
    }*/
}
<?php

class VideoUrlEditor extends DataTypeEditor
{
    public function run()
    {
        ?>
        <div class="preview-element">
            <div class="head">Use a 1280x720 video for best results</div>
            <div class="content"><p class="no-video-placeholder">No video. Add one below.</p><img style="display:none"/></div>
        </div>
        <div class="url-link-holder video">
            <div class="left">
                <a>
                    <span class="plus-element">+</span>
                    <span class="description">Add Video</span>
                </a>
            </div>
            <div class="right">
                <?php 
                    echo CHtml::textField(
                        $this->name, 
                        $this->value, 
                        array(
                            'placeholder' => 'Paste your URL here...',
                            'class' => 'video-url-holder'
                        )
                    ); 
                ?>
            </div>
        </div>
        <?php
        Yii::app()->clientScript
            ->registerScript(__CLASS__, 
<<<'JS'
    $('.video-url-holder').on('change', function() {
        var $content = $(this).parents('.attribute-editor').find('.preview-element .content'),
            value = $(this).val()
        ;
        function getYtId(url) 
        {
            var video_id = null;
            if (url.indexOf('v=') != -1) {
                video_id = url.split('v=')[1];
            }
            else if (url.indexOf('.be/') != -1) {
                video_id = url.split('.be/')[1];
            }
            if (video_id) {
                var ampersandPosition = video_id.indexOf('&');
                if(ampersandPosition != -1) {
                    video_id = video_id.substring(0, ampersandPosition);
                }
            }
            
            return video_id;
        }

        if (value) {
            $content.find('.no-video-placeholder').hide();
            $content.find('img')
                .attr('src', 'https://img.youtube.com/vi/' + getYtId($(this).val()) + '/0.jpg')
                .show()
            ;
        } else {
            $content.find('.no-video-placeholder').show();
            $content.find('img').hide();
        }

    });
JS
            )
        ;
    }
    
    public static function updateLogicJS() 
    {
        return '$(this).find("input").val(value).trigger("change"); $(this).find("input").parents(".url-link-holder").show();';
    }
}
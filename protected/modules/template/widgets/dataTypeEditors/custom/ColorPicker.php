<?php

class ColorPicker extends DataTypeEditor
{
    public function run()
    {
        ?>
        <div class="preview-element colorpicker">
            <div class="head-color">
                <div class="color-picker-preview" style="background: red;"></div>
                <div class="color-picker-pickup">
                    <div class="left">#</div>
                    <div class="right">
                        <?php echo CHtml::textField($this->name, $this->value, array(
                            'class' => 'color-picker-value',
                            'maxlength' => 6
                        )); ?>
                    </div>
                </div>
            </div>
            <div class="content"></div>
        </div>
        <?php
        
        Yii::app()->clientScript
            ->registerScript(__CLASS__, 
<<<'JS'
    $('.color-picker-value').each(function() {            
        var $pickerValue = $(this),
            $content = $(this).parents('.colorpicker').find('.content')
        ;
        
        function updateColorPreview() {
            $pickerValue
                .parents('.head-color')
                .find('.color-picker-preview')
                    .css(
                        'background-color', 
                        '#' + $pickerValue.val()
                    )
            ;           
        }
        
        $pickerValue.on('change', function() {
            $.farbtastic($content).setColor("#" + $(this).val());
            updateColorPreview();
        });
                    
        $content.farbtastic({
            callback: function(color) {
                $pickerValue.val(color.substring(1));
                updateColorPreview();
            },
            width: 170
        });
    });          

JS
            )
            ->registerScriptFile(Yii::app()->baseUrl . '/js/farbtastic.js')
        ;
    }
    
    public static function updateLogicJS() 
    {
        return 
            //Get rid of '#' character as it is already visualised in UI
            'if (typeof value != "undefined" && value != null && value.length > 1 && value.substring(0, 1) === "#") {'
            . '  value = value.substring(1);'
            . '}'
                
            //Update text input + init color preview by triggering change event
            . '$(this).find("input").val(value).trigger("change")'
        ;
    }
}
<?php

class TimezonePicker extends DataTypeEditor
{
    public function run()
    {
        $timezones = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        $tzlist = array_combine($timezones, $timezones);
        ?>
        <div class="url-link-holder">
            <div class="left">
                <a href="#">
                    <span class="plus-element">+</span>
                    <span class="description"><?=CHtml::encode($this->label)?></span>
                </a>
            </div>
            <div class="right">
                <?php
                    echo CHtml::dropDownList(
                        $this->name, 
                        $this->value, 
                        $tzlist
                    ); 
                ?>
            </div>
        </div>
        <?php
    }
    
    public static function updateLogicJS() 
    {
        //Just rewrite raw value into input element by default
        return '$(this).find("select").val(value);';
    }
}
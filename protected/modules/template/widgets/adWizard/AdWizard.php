<?php

class AdWizard extends CWidget
{
    public $fanpages;
    
    public function run()
    {
        $model = new Ad;
        $templateProvider = $model->search();

        //$criteria = new CDbCriteria();
        
        $availableAdFields = AdField::model()
            ->with('adFieldAttributes')
            ->findAll();
        $categories = AdCategory::model()->findAll();

        //$fanpages = $this->fanpages instanceof CActiveRecord ? $this->fanpages : $this->fanpages->getData();
        //$fanpagesJSData = CHtml::listData($fanpages, , $textField);
        
        //Yii::app()->clientScript->registerScript(__CLASS__ . 'fanpages', );
        
        $this->render('adWizard', compact('templateProvider', 'availableAdFields', 'categories'));
        
    }
}
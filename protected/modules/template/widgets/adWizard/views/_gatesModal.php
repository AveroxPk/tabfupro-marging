<?php 
    Yii::app()->clientScript
            ->registerCssFile(Yii::app()->baseUrl . '/css/tabwizard.gates.css')
            ->registerScriptFile(Yii::app()->baseUrl . '/js/tabwizard.gates.js')
    ; 
?>
<div class="gs-modal">
  <div class="gs-header">
      <h1 class="gates-header general-gates-header">Add Gates</h1>
      <h1 class="gates-header email-gates-header" style="display:none">Add Gate</h1>
      <p class="gates-info general-gates-info">Choose which Gates you want applied to your Tab and edit the messages to your liking</p>
      <p class="gates-info email-gates-info" style="display:none">Edit the messages to your liking below</p>
      <a href="#" class="close-button"><i class="icofont icofont-power"></i></a>
  </div>
  <div class="gs-nav">
    <div class="like-gate gate active" data-settings-id="like-gate-settings">
      <a href="#" class="button-nav like">Like gate</a>
      <span class="tick_container">
        <span class="tick" id="like-gate-tick"></span>
      </span>
    </div>
    <div class="share-gate gate" data-settings-id="share-gate-settings">
      <a href="#" class="button-nav share">Share gate</a>
      <span class="tick_container">
        <span class="tick" id="share-gate-tick"></span>
      </span>
    </div>
    <div class="email-gate gate" data-settings-id="email-gate-settings">
      <a href="#" class="button-nav email">Email gate</a>
      <span class="tick_container">
        <span class="tick" id="email-gate-tick"></span>
      </span>
    </div>
  </div>
    
    <div id="like-gate-settings" class="gate-settings">
        <?php $this->render('__likeGateSettings'); ?>
    </div>
    
    <div id="share-gate-settings" class="gate-settings" style="display:none">
        <?php $this->render('__shareGateSettings'); ?>
    </div>
    
    <div id="email-gate-settings" class="gate-settings" style="display:none">
        <?php $this->render('__emailGateSettings'); ?>
    </div>
    
    <div class="gate-footer">
      <div class="left">
        <a href="#" class="close-window-169">Close window</a>
      </div>
      <div class="middle">
        <span id="gates-settings-saved-info" style="display:none">your settings have been saved...</span>
      </div>
      <div class="right">
        <a href="#" class="save-button">Save</a>
      </div>
    </div>
</div>
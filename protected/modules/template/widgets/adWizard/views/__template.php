<div class="template-box" data-id="<?=$data->id;?>" data-categories="<?= implode(',', $data->getCategoriesIds()) ?>" data-catnames="<?= implode(', ', $data->getCategoriesNames()) ?>">
    <div class="title"><?= $data->name ?></div>
    <div class="thumb"><?=AdHelper::getAdPreview($data)?><img src="<?= Yii::app()->baseUrl . '/images/tabwizard/magnify.png' ?>" class="thumb-overlay" alt="" /></div>
    <div class="description"><span><?= $data->description ?></span></div>
    <div class="buttons">
        <a href="#" class="edit-button">Edit</a>
        <a href="#" class="favorite-button <?= $data->isFavourite ? ' active' : '' ?> ">
			<span class="icofont icofont-star"></span>
		</a>
    </div>
</div>
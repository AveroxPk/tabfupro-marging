<div class="preview-header" style="width:70%; float:left;">
	<div class="left">
		<span class="icofont icofont-file-text"></span>
		<span class="title">Ad title</span>
	</div>
    <div class="editor-template-title right">
      <input type="text" name="template-title" value="" placeholder="Enter your title here..." />
    </div>
</div>
<div class="right">
	<a href="#" id="editor-preview-on-facebook" class="custom-btn selected editor-preview-on-facebook" title="Preview on facebook"><i class="icofont icofont-open-eye"></i> Preview on facebook</a>
</div>

<div class="clearfix"></div>
<div id="editor-template-preview" class="editor-template-preview">
  <!-- It's temporary -->
  <!--<div style="width: 810px; height: 800px; background: #ff0000; margin: 0 auto;"></div>-->
  <iframe id="page-preview" src="about:blank" style="width: 1200px; height: 1184px; background: transparent; margin: 0 auto; border: none; transform: scale(0.675); transform-origin: 0 0; -ms-zoom: 0.75; -moz-transform: scale(0.675); -moz-transform-origin: 0 0; -o-transform: scale(0.675); -o-transform-origin: 0 0; -webkit-transform: scale(0.675); -webkit-transform-origin: 0 0;"></iframe>
</div> 

<div id="editor-modals" style="/*display:none*/">
    <?php 
        $clientScript = Yii::app()->clientScript;

        //Render editor modals for each TemplateField type
        foreach ($availableAdFields as $templateField) {
            
            echo CHtml::tag('div', array(
                'id' => $templateField->name . '-editor-modal',
                'class' => 'modal-editor',
                'style' => 'display:none',
            ));
            ?>
            <div class="modal-editor-header">
                <span class="draggable"><i class="fa fa-arrows" aria-hidden="true"></i></span>
                <div class="head video">
                    <?=$templateField->editor_title ? "<span class=\"title\">{$templateField->editor_title}</span>" : ''?>
                    <?=$templateField->editor_description ? "<span class=\"description\">{$templateField->editor_description}</span>" : ''?>
                </div>
                <a href="#" class="close-modal-button"><i class="icofont icofont-power"></i></a>
            </div>
            <div class="modal-editor-content">
            <?php
            $visibleAttributesCount = 0;
            foreach ($templateField->adFieldAttributes as $attribute) {
                if ($attribute->hidden) {
                    continue;
                }

                $editorDivId = $templateField->name . '-' . $attribute->name . '-editor';

                $attributeWrapperClass = 'attribute-editor';
                if ($attribute->custom_edit_widget_class == 'default') {
                    $widgetClassPath = 'application.modules.template.widgets.dataTypeEditors.standard.' . ucfirst($attribute->data_type) . 'DataTypeEditor';
                } else {
                    $widgetClassPath = 'application.modules.template.widgets.dataTypeEditors.custom.' . $attribute->custom_edit_widget_class;
                    $attributeWrapperClass .= ' ' . $attribute->custom_edit_widget_class;
                }
                echo CHtml::tag('div', array(
                    'id' => $editorDivId,
                    'class' => $attributeWrapperClass
                ));
                
                
                $widget = $this->widget($widgetClassPath, array(
                    'name' => $attribute->name,
                    'value' => '',
                    'label' => $attribute->label
                ));

                $widgetClass = get_class($widget);
                $updateLogicFunctionName = $widgetClass . 'UpdateLogic';

                //Register update logic script for particular data editor / custom widget
                //It may be reused many times so Yii will handle duplicates removal
                $clientScript->registerScript($updateLogicFunctionName, "function $updateLogicFunctionName(e, value, attributes) {{$widgetClass::updateLogicJS()}}");

                //Bind update logic to particular attribute editor
                //Many attributes may use one update logic (n->1 relation)
                //so script ids should be unique across attributes
                $clientScript->registerScript("$editorDivId#updateBinding", "$('#$editorDivId').bind('update', $updateLogicFunctionName);");

                echo '</div>';
                ++$visibleAttributesCount;
            }
            
            if ($visibleAttributesCount == 0) {
                echo 'This element has no configurable options.';
            }
            
            echo '</div>';
            
            echo '<div class="clearfix"></div>';
            echo '<div class="modal-editor-footer">';
                echo '<a href="#" class="modal-editor-ok-buttom">Ok</a>';
            echo '</div>';
            
            
            echo '</div>';
        } 
    ?>
</div>

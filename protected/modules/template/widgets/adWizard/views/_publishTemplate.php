<div id="editor-content-header" class="tabbed-modal-content-header">
 <h1>Export your completed Ad Image</h1>
 <h2>Click export to download the completed ad image as a Jpeg file, ready to use in your Facebook campaign</h2>
</div>
<div id="editor-content-main" class="tabbed-modal-content-main">
    <div class="editor-publish-container">
       <div class="publishing-tab-to publish-box">
         <div class="content">
             <img src="" alt="Please wait..." class="ad-preview-img" />
            <a href="#" class="button preview-on-fb-big"><i class="icofont icofont-eye-alt"></i>&nbsp;&nbsp;&nbsp;Preview on facebook</a>
            <a href="#" class="button publish-now-big"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;Export</a>
         </div>
       </div>
    </div>
</div>
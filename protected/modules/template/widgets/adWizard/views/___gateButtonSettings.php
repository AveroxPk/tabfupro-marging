<div id="<?=$modalId?>" class="modal-editor ui-draggable" style="display: none;">
    <div class="modal-editor-header">
        <span class="draggable"><i class="fa fa-arrows" aria-hidden="true"></i></span>
        <div class="head">
            <span><?=$modalTitle?></span>
        </div>
        <a href="#" class="close-modal-button"><i class="icofont icofont-power"></i></a>
    </div>
    <div class="modal-editor-content download-button-modal">
        <div class="preview-element">
            <div class="head">Use a <span class="prefered-image-size">275x59</span> pixel image for best results</div>
            <input type="file" id="<?=$modalId?>-file" style="display:none" data-value-bind="<?=$modalId?>-img" data-image-bind="<?=$buttonId?>" class="gate-button-image-file-input" />
            <div class="content">
                <input type="hidden" id="<?=$modalId?>-img" />
                <span class="image-element-hider">
                    <img class="image-preview" src="/images/tabwizard/drag-file.png">
                </span>
                <a href="#" class="delete-image-button"></a>
                <a class="upload-button">Upload</a>
            </div>
        </div>
        
        <?php if ($customizableUrl): ?>
        <div class="url-link-holder">
            <div class="left">
                <a href="#">
                    <span class="plus-element">+</span>
                    <span class="description">Add link</span>
                </a>
            </div>
            <div class="right">
                <input type="text" name="holder" value="" placeholder="Paste your URL here..." id="<?=$modalId?>-url">
            </div>
        </div>
        <?php endif; ?>
        
    </div>
    <div class="clearfix"></div>
    
    <div class="modal-editor-footer">
        <a class="modal-gate-ok-button" href="#">Ok</a>
    </div>
</div>
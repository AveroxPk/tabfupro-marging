<div id="modal-congrats" class="modal-congrats ad-congrats">
    <div class="header">
        <span>Congrats your Ad Image was...</span>
        <a href="#" class="close-modal-button"><i class="icofont icofont-power"></i></a>
    </div>
    <div class="modal-congrats-content">
        <div class="content published">
			<h2>Exported <span class="checked-action"><i class="fa fa-check-circle" aria-hidden="true"></i></span></h2>
            <div class="what-next">What next?</div>
            <a href="#" class="button duplicatead">Duplicate</a>
            <a href="#" class="button create-new">Create new</a>
        </div>
    </div>
    <div class="modal-congrats-footer">
        <a href="#" class="modal-congrats-close-buttom">Close window</a>
        <!--<a href="#" class="modal-congrats-exit-buttom">Exit</a>-->
    </div>
</div>
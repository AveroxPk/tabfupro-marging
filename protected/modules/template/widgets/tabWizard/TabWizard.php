<?php

class TabWizard extends CWidget
{
    public $fanpages;
    
    public function run()
    {
        $model = new ARTemplate;
        $templateProvider = $model->search();

        $criteria = new CDbCriteria();
        
        $availableTemplateFields = ARTemplateField::model()
            ->with('templateFieldAttributes')
            ->findAll();
        $categories = TemplateCategory::model()->findAll();

        $fanpages = $this->fanpages instanceof CActiveRecord ? $this->fanpages : $this->fanpages->getData();
        //$fanpagesJSData = CHtml::listData($fanpages, , $textField);
        
        //Yii::app()->clientScript->registerScript(__CLASS__ . 'fanpages', );
        $compact = compact('templateProvider', 'availableTemplateFields', 'categories');
		// CVarDumper::dump($templateProvider->criteria);
		$this->render('tabWizard', $compact);
        
    }
}
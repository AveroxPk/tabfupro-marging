<div class="button-area">
	<a href="#" id="editor-preview-on-facebook" class="custom-btn" title="Preview on facebook"><i class="icofont icofont-open-eye"></i> Preview on facebook</a>

    <a href="#" id="editor-add-autoresponder" class="custom-btn" title="Add autoresponder"><i class="icofont icofont-robot-face"></i> Add autoresponder</a>
	
	<a href="#" id="editor-add-crm" class="custom-btn" title="Add Crm"><i class="icofont icofont-people"></i> Add CRM</a>
	
</div>
<div class="clearfix"></div>
<div class="preview-header">
	<div class="left">
		<span class="icofont icofont-file-text"></span>
		<span class="title">Template title</span>
	</div>
    <div class="editor-template-title right">
      <input type="text" name="template-title" value="" placeholder="Enter your title here..." />
    </div>
</div>

<div id="editor-template-preview" class="editor-template-preview">
  <!-- It's temporary -->
  <!--<div style="width: 810px; height: 800px; background: #ff0000; margin: 0 auto;"></div>-->
  <iframe id="page-preview" class="kkj" src="about:blank" style="width: 810px; height: 610px; background: transparent; margin: 0 auto; border: none;"></iframe>
</div> 

<div id="editor-modals" style="/*display:none*/">
    <?php 
        $clientScript = Yii::app()->clientScript;

        //Render editor modals for each TemplateField type
        foreach ($availableTemplateFields as $templateField) {
            
            echo CHtml::tag('div', array(
                'id' => $templateField->name . '-editor-modal',
                'class' => 'modal-editor',
                'style' => 'display:none',
            ));
            ?>
            <div class="modal-editor-header">
                <span class="draggable"><i class="fa fa-arrows" aria-hidden="true"></i></span>
                <div class="head video">
                    <?=$templateField->editor_title ? "<span class=\"title\">{$templateField->editor_title}</span>" : ''?>
                    <?=$templateField->editor_description ? "<span class=\"description\">{$templateField->editor_description}</span>" : ''?>
                </div>
                <a href="#" class="close-modal-button"><i class="icofont icofont-power"></i></a>
            </div>
            <div class="modal-editor-content">
            <?php
				$orderedTemplateFieldAttributesArr = array();
				$attributeOrder = ['placeholder','visible','required'];
				
				foreach($attributeOrder as $attribute)
				{
					foreach($templateField->templateFieldAttributes as $val)
					{
						$subject = $val['name'];
						$result = strTolower(preg_replace('/\B([A-Z])/', '_$1', $subject));
						$attributeValue = explode('_',$result)[1];
						if($attributeValue == $attribute)
						{
							$orderedTemplateFieldAttributesArr[] = $val;
						}
					}
					
				}
				
				if(!empty($orderedTemplateFieldAttributesArr))
					$orderedTemplateFieldAttributes = $orderedTemplateFieldAttributesArr;
				else
					$orderedTemplateFieldAttributes = $templateField->templateFieldAttributes;
				
            $visibleAttributesCount = 0;
            foreach ($orderedTemplateFieldAttributes as $attribute) {
                if ($attribute->hidden) {
                    continue;
                }

                $editorDivId = $templateField->name . '-' . $attribute->name . '-editor';

                $attributeWrapperClass = 'attribute-editor';
                if ($attribute->custom_edit_widget_class == 'default') {
                    $widgetClassPath = 'application.modules.template.widgets.dataTypeEditors.standard.' . ucfirst($attribute->data_type) . 'DataTypeEditor';
                } else {
                    $widgetClassPath = 'application.modules.template.widgets.dataTypeEditors.custom.' . $attribute->custom_edit_widget_class;
                    $attributeWrapperClass .= ' ' . $attribute->custom_edit_widget_class;
                }
                echo CHtml::tag('div', array(
                    'id' => $editorDivId,
                    'class' => $attributeWrapperClass
                ));
                
                
                $widget = $this->widget($widgetClassPath, array(
                    'name' => $attribute->name,
                    'value' => '',
                    'label' => $attribute->label
                ));

                $widgetClass = get_class($widget);
                $updateLogicFunctionName = $widgetClass . 'UpdateLogic';

                //Register update logic script for particular data editor / custom widget
                //It may be reused many times so Yii will handle duplicates removal
                $clientScript->registerScript($updateLogicFunctionName, "function $updateLogicFunctionName(e, value, attributes) {{$widgetClass::updateLogicJS()}}");

                //Bind update logic to particular attribute editor
                //Many attributes may use one update logic (n->1 relation)
                //so script ids should be unique across attributes
                $clientScript->registerScript("$editorDivId#updateBinding", "$('#$editorDivId').bind('update', $updateLogicFunctionName);");

                echo '</div>';
                ++$visibleAttributesCount;
            }
            
            if ($visibleAttributesCount == 0) {
                echo 'This element has no configurable options.';
            }
            
            echo '</div>';
            
            echo '<div class="clearfix"></div>';
            echo '<div class="modal-editor-footer">';
                echo '<a href="#" class="modal-editor-ok-buttom">Ok</a>';
            echo '</div>';
            
            
            echo '</div>';
        } 
    ?>
</div>

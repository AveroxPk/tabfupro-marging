<?php
    Yii::app()->clientScript
        ->registerCoreScript('jquery')
        ->registerCoreScript('jquery.ui') //Needed for draggable modals
        ->registerCssFile(Yii::app()->baseUrl . '/css/tabwizard.css')
        ->registerCssFile(Yii::app()->baseUrl . '/css/gates.css')
        ->registerCssFile(Yii::app()->baseUrl . '/css/tabwizard.autoresponders.css')
        ->registerCssFile(Yii::app()->baseUrl . '/css/tabwizard.crms.css')
        ->registerCssFile(Yii::app()->baseUrl . '/css/jquery.mCustomScrollbar.css')
        ->registerLinkTag(
            'stylesheet', 
            'text/css', 
            Yii::app()->getBaseUrl(true) . '/css/template.editor.iframe.css', 
            null, 
            array(
                'id' => 'iframe-style'
            )
        )
        ->registerScriptFile(Yii::app()->baseUrl . '/js/tabwizard.js')
        ->registerScriptFile(Yii::app()->baseUrl . '/js/tabwizard.autoresponders.js')
        ->registerScriptFile(Yii::app()->baseUrl . '/js/tabwizard.crm.js')
        ->registerScriptFile(Yii::app()->baseUrl . '/js/tabwizard.congrats.js')
        ->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.mCustomScrollbar.concat.min.js')
        ->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.easyModal.js')
        ->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.blockUi.js')
    ;
?>

<div id="editor" class="tabbed-modal" style="display:none">

	<a href="#" id="editor-close" title="Close">
		<i class="icofont icofont-power"></i>
	</a>
	<div id="editor-header-choose-title" class="editor-save-details">
		<span class="error-information">
			Give your Tab Template a title first: 
		</span>		
		<div class="error-input">
			<div class="left">
				<span class="icofont icofont-file-text"></span>
				<span class="title">Template title</span>
			</div>
			<div class="right">
			  <input type="text" name="template-title" value="" placeholder="Enter your title here..." />
			</div>
		</div>
		
		<button id="editor-header-choose-title-save"><i class="fa fa-check" aria-hidden="true"></i></button>		
	</div>
	
	<div id="editor-header-saving" class="editor-save-details">
		<h1>Saving...</h1>
	</div>
	
	<div id="editor-header-saved" class="editor-save-details">
		<h1>Saved <img src="/images/tabwizard/save-template-title-tick.png"/></h1>
	</div>
	
		
    <div id="editor-header" class="col-md-1 tabbed-modal-header">

        
	

        <div id="editor-nav" class="tabbed-modal-nav">
			<div class="steps active">
				<a href="#ChooseTemplateTab" id="editor-choose-template" class="editor-nav-link icofont icofont-file-jpg" title="Choose template"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>

			<div class="steps">
				<a href="#EditTemplateTab" id="editor-edit-template" class="editor-nav-link icofont icofont-edit" title="Edit template"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>

			<div class="steps">
				<a href="#PublishTab" id="editor-publish" class="editor-nav-link fa fa-check" title="Publish"></a>
			</div>
		</div>
	</div>
    <div id="editor-content" class="col-md-11 tabbed-modal-content">
        <!-- Choose template -->
        <div class="tab active" id="ChooseTemplateTab">
           <div id="editor-content-header" class="tabbed-modal-content-header">
             <h1>Choose a Template</h1>
             <h2>Select a Template to edit. You can search for templates via the search bar or choose a category below.</h2>
           </div>
           <div id="editor-content-main" class="tabbed-modal-content-main">
                <?php $this->render('_chooseTemplate', array(
                    'dataProvider' => $templateProvider,
                    'categories'   => $categories
                )); ?>
           </div>
        </div>

        <!-- Edit template -->
        <div class="tab" id="EditTemplateTab">
            <div id="editor-content-header" class="tabbed-modal-content-header second-tab">
             <h1>Edit your Template</h1>
             <h2>Give your template a title and double click on any element to edit it.</h2>
            </div>
            <div id="editor-content-main" class="tabbed-modal-content-main">
               <?php $this->render('_editTemplate', compact('availableTemplateFields')); ?>
            </div>
        </div>

        <!-- Publish -->
        <div class="tab" id="PublishTab">
            <?php $this->render('_publishTemplate'); ?>
        </div>
		
		
		<div id="editor-footer" class="tabbed-modal-footer">
			<div id="editor-footer-left-side">
				<a href="#" id="editor-back-button" class="custom-btn-third active"><i class="icofont icofont-arrow-left"></i> Previous</a>
				<!-- <a href="#" id="editor-back-button" class="button cancel">Cancel</a> -->
			</div>

			<div id="editor-footer-middle">
				<span id="editor-changes-not-published">Template has been changed, please publish it again to populate Facebook tab with the updated content.</span>
				<span id="editor-changes-saved">Changes have been saved a few second ago...</span>
			</div>

			<div id="editor-footer-right-side">
				<a href="#" id="editor-next-button" class="button disabled">Next <i class="icofont icofont-arrow-right"></i></a>
				<!-- <a href="#" id="editor-close-button" class="button">Close window</a> -->
			</div>
		</div>

    </div>

</div>

<div class="ar-modal"></div>
<div class="crms-modal"></div>
<?php 
$this->render('_copyHtmlModal');
$this->render('_gatesModal');
$this->render('_congratsModal');
$this->render('_templatePreview');

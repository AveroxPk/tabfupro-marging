<div class="preview-header">
	<div class="left">
		<span class="icofont icofont-search"></span>
		<span class="title">Search Tab Template</span>
	</div>
    <div class="right">
      <input type="text" id="search-templates" value="" placeholder="Search Tab Templates" />
    </div>
</div>

<div id="editor-templates-list" class="editor-templates-list">
    <div class="editor-header-templates-list">
      <span class="one">Displaying Templates for:</span>
      <span class="two">
          <strong>All Categories</strong>
      </span>
        <select name="aa" class="categories-select-box">
            <?php foreach ($categories as $category): ?>
                <option value="<?= $category->id ?>">
                    <?= $category->name ?>
                </option>
            <?php endforeach; ?>
      </select>
      <span class="three">or</span>
      <a href="#" class="show-all-button"><i class="icofont icofont-eye-alt"></i> Show All</a>
      <i class="ticked active"></i>
    </div>

    <?php
        $dataProvider->pagination = false;
        $this->widget('zii.widgets.CListView', array(
            'id' => 'editor-templates-list-content',
            'htmlOptions' => array(
                'class' => 'editor-templates-list-content'
            ),
            'dataProvider' => $dataProvider,
            'itemView' => '__template',
            'summaryText' => ''
        ));
    ?>
</div>
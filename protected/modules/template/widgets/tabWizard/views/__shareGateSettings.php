<div class="gs-info-active">
   <div class="left">
     <div class="about-info">The user is required to share your page before they gain access to your offer.</div>
   </div>
   <div class="right">
		<span class="on">ON</span>
		 <div class="on-off-switch off" id="share-gate-switch">
		   <a href="#" class="on-click-switch">On/Off</a>
		 </div>
		<span class="off">OFF</span>
   </div>
 </div>
 <div class="gate-main">
   <div class="gate-navigate">
     <div class="left">
       <a href="#" class="link first active" id="share-navigate-prompt">
         <span class="content">Prompt Message
</span>
         <span class="divied"></span>
       </a>
     </div>
     <div class="right">
       <!-- <a href="#" class="link second disabled"> -->
       <a href="#" class="link second normal" id="share-navigate-confirm">
         <span class="content">Confirm Message
</span>
         <span class="divied"></span>
       </a>
     </div>
   </div>
   <div class="gate-content" id="share-prompt-content">
     <div class="information">Share Prompt: This will display when the user clicks your call to action. Edit it to your liking below</div>
     <!--<div class="information-bottom">*This Gate is automatically turned on for this template</div>-->
     
    <div class="gate-popup gate-popup-short">
        <div class="gate-popup-close"></div>
        <div class="gate-popup-short-prompt-message-wrapper">
            <div id="share-prompt-text-edit" class="gate-popup-short-prompt-message-body" contenteditable>

            </div>
        </div>
        <img src="/images/fb-share-button.png" class="fb-share-button"/>
    </div>

   </div>
   <div class="gate-content" id="share-confirm-content" style="display:none;">
     <div class="information">This will display after the user has successfully<br /> 
shared your tab link</div>
     <!--<div class="information-bottom">*This Gate is automatically turned on for this template</div>-->
     

    <div class="gate-popup gate-popup-short">
        <div class="gate-popup-close"></div>
        <div class="gate-popup-short-prompt-message-wrapper">
            <div id="share-confirm-text-edit" class="gate-popup-short-prompt-message-body" contenteditable>
                
            </div>
        </div>
        <?php
            echo CHtml::image(
                '/images/download-button.png', 
                'Download',
                array(
                    'class' => 'gate-download-button',
                    'id' => 'share-download-button',
                    'data-default-src' => '/images/download-button.png'
                )
            );
        ?>
    </div>
    <?php 
        $this->render(
            '___gateButtonSettings', 
            array(
                'modalTitle' => 'Download button settings',
                'modalId' => 'share-download-button-modal', 
                'buttonId' => 'share-download-button',
                'customizableUrl' => true
            )
        );
    ?>
   </div>
     

 </div>

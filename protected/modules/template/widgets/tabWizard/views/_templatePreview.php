<?php
Yii::app()->clientScript
    ->registerCssFile(Yii::app()->baseUrl . '/css/tabwizard.preview.css')
    ->registerScriptFile(Yii::app()->baseUrl . '/js/tabwizard.preview.js');
?>
<div class="tp-modal">
    <div class="tp-modal-header">
        <h2>Template Name</h2>
        <div class="categories">
            <span>none</span>
        </div>
        <a href="#" class="close-button"><i class="icofont icofont-power"></i></a>
    </div>
    <div id="page-preview">
        <img src="" alt="Template preview" />
    </div>
    <div class="description">
        No description
    </div>
    <div class="footer">
        <div class="left">
            <a href="#" class="close-window-button custom-btn active text-center" style="float: left; margin-top: 0px; margin-left: 12px;">Close window</a>
        </div>
        <div class="middle">
            <span id="gates-settings-saved-info" style="display:none">your settings have been saved...</span>
        </div>
        <div class="right">
            <a href="#" class="edit-button">Edit</a>
        </div>
    </div>
</div>
<div class="gs-info-active">
   <div class="left">
		
     <div class="about-info">
		<span class="icon"><i class="icofont icofont-info-circle"></i></span>
		<span class="message">The user is required to input their email before they gain access to your offer.</span>
	 </div>
   </div>
   <div class="right">
		<span class="on">ON</span>
		<div class="on-off-switch off" id="email-gate-switch">
			<a href="#" class="on-click-switch">On/Off</a>
		</div>
		<span class="on">OFF</span>	
   </div>
 </div>
 <div class="gate-main">
   <div class="gate-navigate">
     <div class="left">
       <a href="#" class="link first active" id="email-navigate-prompt">
         <span class="content">Prompt Message</span>
         <span class="divied"></span>
       </a>
     </div>
     <div class="right">
       <!-- <a href="#" class="link second disabled"> -->
       <a href="#" class="link second normal" id="email-navigate-confirm">
         <span class="content">Confirm Message</span>
         <span class="divied"></span>
       </a>
     </div>
   </div> 
    <div class="gate-content" id="email-prompt-content">
        <div class="information">
			<span class="icon"><i class="icofont icofont-info-circle"></i></span>
			<span class="message">		
				This will display when the user clicks your call to action. <br /> Edit it to your liking below
			</span>
		</div>
        <!--<div class="information-bottom">*This Gate is automatically turned on for this template</div>-->
     
        <div class="content-scrollable">
            <div class="gate-popup email-gate">
                <div id="email-prompt-text-edit" class="gate-popup-header-bar" contenteditable>
                    Enter your Email below to gain instant access!
                </div>
                <div class="gate-popup-close"></div>

                <input type="text" value="your email here..." class="gate-main-input" /><br/>
                <a href="#" class="cta"><img src="/images/gates/popups/get-access-now-button.png" data-default-src="/images/gates/popups/get-access-now-button.png" id="email-get-access-now-button" /></a>
                <p class="privacy-info"><input type="text" value="Your Privacy is protected" /></p>
            </div>
        </div>
        
        <?php 
            $this->render(
                '___gateButtonSettings', 
                array(
                    'modalTitle' => 'Email prompt button appearance',
                    'modalId' => 'email-get-access-now-button-modal', 
                    'buttonId' => 'email-get-access-now-button', 
                    'customizableUrl' => false
                )
            );
        ?>
        
   </div>
   <div class="gate-content" id="email-confirm-content" style="display:none;">
     <div class="information">
		<span class="icon"><i class="icofont icofont-info-circle"></i></span>
		<span class="message">This will display after the user has successfully<br />submitted their email</span>
	 </div>
     <!--<div class="information-bottom">*This Gate is automatically turned on for this template</div>-->
     
     
    <div class="gate-popup email-gate">
        <div id="email-confirm-title-edit" class="gate-popup-header-bar" contenteditable>
            ALL SET!
        </div>
        <div class="gate-popup-close"></div>

        <div class="email-gate-confirm-message-wrapper">
            <div id="email-confirm-text-edit" class="email-gate-confirm-message-body" contenteditable>

            </div>
        </div>

    </div>
   </div>
     

 </div>

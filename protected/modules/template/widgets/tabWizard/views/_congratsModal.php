<div id="modal-congrats" class="modal-congrats">
    <div class="header">
        <span>Congrats your Tab was...</span>
        <a href="#" class="close-modal-button"><i class="icofont icofont-power"></i></a>
    </div>
    <div class="modal-congrats-content">
        <div class="content published">
            <h2>Published <span class="checked-action"><i class="fa fa-check-circle" aria-hidden="true"></i></span></h2>

            <div class="what-next">What next?</div>
            <a href="#" class="custom-button text-center selected create-new"><i class="fa fa-plus" aria-hidden="true"></i> Create new</a>
            <a href="facebook/Showtab/RedirectToLastPublished" target="_blank" class="custom-button text-center selected view-on-fb"><i class="icofont icofont-eye"></i> View on Facebook</a>
            <a href="#" class="custom-button text-center selected duplicate"><i class="icofont icofont-copy"></i> Duplicate</a>
            <a href="#" class="custom-button text-center selected analytics"><i class="fa fa-bar-chart" aria-hidden="true"></i> Analytics</a>
        </div>
    </div>
    <div class="modal-congrats-footer">
        <a href="#" class="modal-congrats-close-buttom"><i class="fa fa-compress" aria-hidden="true"></i> Close window</a>
        <!--<a href="#" class="modal-congrats-exit-buttom">Exit</a>-->
    </div>
</div>
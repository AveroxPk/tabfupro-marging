<div class="gs-info-active">
   <div class="left">
     <div class="about-info">The user is required to like your page before they gain access to your offer.</div>
   </div>
   <div class="right">
		<span class="on">ON</span>
		 <div class="on-off-switch off" id="like-gate-switch">
		   <a href="#" class="on-click-switch">On/Off</a>
		 </div>
		<span class="off">OFF</span>
		 
   </div>
 </div>
 <div class="gate-main">
   <div class="gate-navigate">
     <div class="left">
       <a href="#" class="link first active" id="like-navigate-prompt">
         <span class="content">Prompt Message
</span>
         <span class="divied"></span>
       </a>
     </div>
     <div class="right">
       <!-- <a href="#" class="link second disabled"> -->
       <a href="#" class="link second normal" id="like-navigate-confirm">
         <span class="content">Confirm Message
</span>
         <span class="divied"></span>
       </a>
     </div>
   </div>
   <div class="gate-content" id="like-prompt-content">
     <div class="information">Like Prompt: This will display when the user click your call to action. Edit it to your liking below</div>
     <!--<div class="information-bottom">*This Gate is automatically turned on for this template</div>-->
     
     
    <div class="gate-popup gate-popup-short">
        <div class="gate-popup-close"></div>
        <div class="gate-popup-short-prompt-message-wrapper">
            <div id="like-prompt-text-edit" class="gate-popup-short-prompt-message-body" contenteditable>

            </div>
        </div>
        <img src="/images/tabwizard/fb_like_button.png" class="admin-like-img"/>
    </div>
     

   </div>
   <div class="gate-content" id="like-confirm-content" style="display:none;">
     <div class="information">This will display after the user has successfully<br /> 
liked your fan page</div>
     <!--<div class="information-bottom">*This Gate is automatically turned on for this template</div>-->
     
    <div class="gate-popup gate-popup-short">
        <div class="gate-popup-close"></div>
        <div class="gate-popup-short-prompt-message-wrapper">
            <div id="like-confirm-text-edit" class="gate-popup-short-prompt-message-body" contenteditable>

            </div>
        </div>
        <?php
            echo CHtml::image(
                '/images/download-button.png', 
                'Download',
                array(
                    'class' => 'gate-download-button',
                    'id' => 'like-download-button',
                    'data-default-src' => '/images/download-button.png'
                )
            );
        ?>
    </div>
    <?php 
        $this->render(
            '___gateButtonSettings', 
            array(
                'modalTitle' => 'Download button settings',
                'modalId' => 'like-download-button-modal', 
                'buttonId' => 'like-download-button', 
                'customizableUrl' => true
            )
        );
    ?>
   </div>
    

 </div>

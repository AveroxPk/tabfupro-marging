<div id="editor-content-header" class="tabbed-modal-content-header">
 <h1>Publish your Template as Facebook Tab</h1>
 <h2>Add a Image for your Tab icon, check your settings and publish to Facebook. You can also view and copy the template HTML.</h2>
</div>
<div id="editor-content-main" class="tabbed-modal-content-main">
    <div class="editor-publish-container">
     <div class="left-column">
       <div class="your-tab-icon publish-box">
         <div class="header">
           <h3>Your Tab icon</h3>
           <span>Select an image from the library or upload your own</span>
         </div>
         <div style="display:none"><input type="file" id="tab-icon-file" /></div>
         <div class="content">
           <a href="#" class="select-button" title="Select"><i class="icofont icofont-ui-add"></i></a>
           <a href="#" class="upload-tab-icon custom-btn-second right" title="Upload"><i class="icofont icofont-cloud-upload"></i> Upload</a>
           <p>111x74 px</p>
         </div>
       </div>
	   <!--
       <div class="search-library publish-box">
         <div class="background-search">
           <input type="search" id="search-library-publish" placeholder="Search Library" />
         </div>
       </div>
	   -->
       <div class="icon-library publish-box" id="icon-library-scroll">
         <div class="header">
           <h3>Icon Library</h3>
           <span>Choose a stock or previously uploaded image</span>
         </div>
         <div class="content special">
			<div class="preview-header">
				<div class="left">
					<span class="icofont icofont-search"></span>
					<span class="title">Search Library</span>
				</div>
				<div class="right">
				  <input type="text" id="search-library-publish" value="" placeholder="Search Library" />
				</div>
			</div>		 
             <?php TabIconHelper::generateTabLibraryWidget()->run(); ?>
         </div>
       </div>
     </div>
     <div class="right-column">
       <div class="responders publish-box">
         <div class="header">
             <span class="title">Attached Services</span>
         </div>
		 
			<div class="preview-header">
				<div class="left">
					<span class="title"><i class="icofont icofont-robot-face"></i> Autoresponder</span>
				</div>
				<div class="right">
				   <span class="contents">
					 <span id="autoresponders-sum-up"></span>
				   </span>
				</div>
			</div>		 
		 
			<div class="preview-header">
				<div class="left">
					<span class="title"><i class="icofont icofont-list"></i> List</span>
				</div>
				<div class="right">
				   <span class="contents">
					 <span id="autoresponders-lists-sum-up"></span>
				   </span>
				</div>
			</div>		 

			<div class="preview-header">
				<div class="left">
					<span class="title"><i class="icofont icofont-investigator"></i> Gate</span>
				</div>
				<div class="right">
				   <span class="contents">
					 <span id="gates-sum-up"></span>
				   </span>
				</div>
			</div>		 
			
       </div>
       <div class="publishing-tab-to publish-box">
         <div class="header">
             <span class="title">Publishing Tab to: <span id="editor-publish-fanpage-name"></span></span>
           <a href="#" class="choose-other-page-button"><i class="fa fa-expand" aria-hidden="true"></i> Choose other page</a>
         </div>
         <div class="content">
			<div class="row">
			   <a href="#" class="custom-btn preview-on-fb-big"><i class="icofont icofont-eye"></i> Preview on facebook</a>
			   <a href="#" class="custom-btn right publish-now-big"><i class="icofont icofont-delivery-time"></i> Publish now</a>
			   <a href="#" class="custom-btn publish-changes-big">Publish changes</a>
		   </div>
		   <div class="row">
			   <a href="#" class="custom-btn schedule-big"><i class="icofont icofont-calendar"></i> Schedule</a>
			   <a href="#" class="custom-btn right copy-download-big"><i class="icofont icofont-tag"></i> Copy or Download HTML</a>
		   </div>
         </div>
       </div>
     </div>
    </div>
</div>
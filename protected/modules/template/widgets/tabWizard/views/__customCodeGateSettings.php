<div class="gs-info-active">
   <div class="left">
     <div class="about-info">
		<span class="icon"><i class="icofont icofont-info-circle"></i></span>
		<span class="message">Add your code below. For example paste in a custom web form code, paypal or other buy button.</span>
	 </div>
   </div>
   <div class="right">
		<span class="on">ON</span>
		 <div class="on-off-switch off" id="custom-code-gate-switch">
		   <a href="#" class="on-click-switch">On/Off</a>
		 </div>
		<span class="off">OFF</span>
   </div>
 </div>

 <div class="gate-main">
     
    <div class="gate-navigate">
     <div class="left">
       <a href="#" class="link first active" id="custom-code-navigate-prompt">
         <span class="content">HTML code</span>
         <span class="divied"></span>
       </a>
     </div>
     <div class="right">
     </div>
    </div>    
     
    <div class="gate-content" id="custom-code-prompt-content">
        <div class="information">
			<span class="icon"><i class="icofont icofont-info-circle"></i></span>
			<span class="message">		
				Paste in your HTML code below. This will be rendered and displayed when the user clicks your Call to Action.
			</span>
		</div>
        <!--<div class="information-bottom">*This Gate is automatically turned on for this template</div>-->
     
        <div class="content-scrollable">
            <div class="gate-popup custom-code-gate">
                
                <div id="custom-code-prompt-text-edit" class="gate-popup-header-bar" contenteditable>
                    YOUR CUSTOM HEADING HERE
                </div>
                
                <div class="gate-popup-close"></div>

                <div class="custom-code-gate-text-wrapper">
                    <label for="custom-code-prompt-modal-content-height">Popup content height:</label> <input style="width:55px" type="number" id="custom-code-prompt-modal-content-height" min="100" max="1000" /> px, 
                    <label for="custom-code-prompt-modal-content-width">width:</label> <input style="width:55px" type="number" id="custom-code-prompt-modal-content-width" min="100" max="1000" /> px<br/>
                    <textarea id="custom-code-prompt-content-modal-content" class="custom-code-gate-text" placeholder="paste your html here..."></textarea>
                </div>

            </div>
        </div>
        
    </div>
        
        
 </div>
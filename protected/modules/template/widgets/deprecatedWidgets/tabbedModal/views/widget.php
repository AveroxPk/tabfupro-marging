<!-- Editor - modal -->
<div id="<?=$this->id; ?>" class="tabbed-modal">
    <div id="<?=$this->id; ?>-header" class="tabbed-modal-header">

        <?php 
            foreach ($this->extraHeaders as $headerId => $headerContent) {
                echo CHtml::tag(
                    'div', 
                    array(
                        'id' => $headerId,
                        'style' => 'display:none'
                    ),
                    $headerContent
                );
            }
        ?>

        <div id="<?=$this->id ?>-nav" class="tabbed-modal-nav">
            <?php 
                //Generate tab links basing on widget's tabLabels property
                $tabHrefs = array_map(
                    function ($label, $tabId) {
                        return CHtml::link(
                            CHtml::encode($label), 
                            '#' . $tabId, 
                            array(
                                'id' => $tabId,
                                'class' => 'tabbed-modal-nav-link' . (($this->activeTab == $tabId) ? ' active' : ''),
                                'title' => $label
                            )
                        );
                    }, 
                    $this->tabLabels, 
                    array_keys($this->tabLabels)
                );
                
                //Put arrows between tab links and echo all out
                echo join('<div class="editor-nav-next"></div>', $tabHrefs);
            ?>
        </div>

        <a href="#" id="<?=$this->id; ?>-close" class="tabbed-modal-close" title="Close"><i class="icofont icofont-power"></i></a>

    </div>

    <div id="<?=$this->id; ?>-content" class="tabbed-modal-content">
        <?=$tabs ?>        
    </div>

    <div id="<?=$this->id; ?>-footer" class="tabbed-modal-footer">
        <?=$this->footerContent ?>
    </div>

</div>
<?php

class TabbedModalWidget extends CWidget
{
    public $id = 'editor';
    
    /**
     * List of labels that will be shown in header
     * @var array
     */
    public $tabLabels;
    
    /**
     * Id of tab to be activated on init. By default, it's the first tab.
     * @var string
     */
    public $activeTab;
    
    /**
     * Alternative headers that will replace tab header ocasionally
     * @var array
     */
    public $extraHeaders = array();
    
    /**
     * JS function that will be called every time user tries to switch the tab
     * it can cancel that change by returning false.
     * @var string
     */
    public $beforeTabChange;
    
    /**
     * JS function that will be called before closing the modal. It can cancel
     * closing by returning false.
     * @var string
     */
    public $beforeDialogClose;
    
    /**
     * Raw HTML which will be inserted in footer div
     * @var string
     */
    public $footerContent;
    
    private $_tabContents = array();
    private $_unclosedTabId;
    
    public function beginTab($tabId = null)
    {
        $this->endTab(); //End previous tab if any
        
        if ($tabId === null) {
            $tabId = key($this->tabLabels);
            next($this->tabLabels);
        } elseif (!key_exists($tabId, $this->tabLabels) ) {
            throw CException("Tab with id '$tabId' hasn't been defined in tabLabels array.");
        }
        
        ob_start();
        
        if ($this->activeTab === null) { //First tab activated by default
            $this->activeTab = array_keys($this->tabLabels)[0];    
        }
        $isActiveTab = $this->activeTab == $tabId;
        
        echo CHtml::tag('div', array(
            'class' => 'tab' . ($isActiveTab ? ' active' : ''),
            'id' => $tabId . '-content'
        ));
        
        $this->_unclosedTabId = $tabId;
        
    }
    
    public function endTab()
    {
        if ($this->_unclosedTabId !== null) {
            echo CHtml::closeTag('div');
            $this->_tabContents[$this->_unclosedTabId] = ob_get_clean();
            $this->_unclosedTabId = null;
        }
    }
    
    public function init()
    {
        $this->_registerAssets(array(
            'tabbed.modal.css',
            'tabbed.modal.js',
            'jquery.easyModal.js'
        ));
        Yii::app()->clientScript->registerScript(__CLASS__ . '#' . $this->id, "jQuery('#{$this->id}').tabbedModal();");
    }
    
    protected function _registerAssets(array $assets)
    {
        foreach ($assets as $asset) {
            $x = explode('.', $asset);
            $assetType = end($x);
            $registerFunction = $assetType == 'js' ? 'registerScriptFile' : 'registerCssFile';
            
            Yii::app()->clientScript->$registerFunction(Yii::app()->getBaseUrl() . '/' . $assetType . '/' . $asset);
        }
    }
    
    public function run()
    {
        $this->endTab();
        $this->render('widget', array(
            'tabs' => join('', $this->_tabContents)
        ));
    }
    
}
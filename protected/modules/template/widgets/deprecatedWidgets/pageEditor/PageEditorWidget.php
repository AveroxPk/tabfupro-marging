<?php

class PageEditorWidget extends CWidget
{
    public function run()
    {

        $this->render('widget');
        $this->registerClientScript();

    }
    
    
    public function registerClientScript()
    {
        
        $options = [];
        $options = CJavaScript::encode($options);
        
        Yii::app()->getClientScript()
            ->registerScriptFile(
                Yii::app()->baseUrl . '/js/page.editor.js', 
                CClientScript::POS_END
            )
            ->registerScript(
                __CLASS__ . '#' . $this->id, 
                "jQuery('#{$this->id}').pageEditor($options);"
            )
            ->registerLinkTag(
                'stylesheet', 
                'text/css', 
                Yii::app()->getBaseUrl(true) . '/css/template.editor.iframe.css', 
                null, 
                array(
                    'id' => 'iframe-style'
                )
            )
            ->registerCssFile(
                Yii::app()->baseUrl . '/css/template.editor.css'
            )
        ;
    }
    

}
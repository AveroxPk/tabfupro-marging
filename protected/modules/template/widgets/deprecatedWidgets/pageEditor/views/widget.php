<?php
echo CHtml::tag('div', array('id' => $this->id));
    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id' => $this->id . '_modal',
        // additional javascript options for the dialog plugin
        'options' => array(
            'title' => 'Edit tab',
            'autoOpen' => false,
            'modal' => true,
            'width' => 1300,
            'height' => 900
        )
    ));
    ?>
    <div class="editor-loading">
        Loading editor... Please wait...
    </div>

    <div class="page-editor">
        <div class="editor-toolbar">
            <?php 
                echo CHtml::button('Save tab modifications', array('id' => $this->id . '-publish-tab'));
            ?>
        </div>
        <div class="page-preview" style="float:left;">
            <?php 
                echo CHtml::tag(
                    'iframe',
                    array(
                        'id' => $this->id . '-page-preview-frame',
                        'src' => 'about:blank',
                        'style' => 'width: 810px; height: 800px;'
                    ),
                    '&nbsp;'
                );
            ?>
        </div>
        <?php
            echo CHtml::tag('div', array(
                'id' => $this->id . '-sidebar',
            ));
            echo '</div>';
            echo CHtml::button('Apply', array('class' => 'button-save-changes'));
        ?>
        
    

    </div>

    <?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
</div>
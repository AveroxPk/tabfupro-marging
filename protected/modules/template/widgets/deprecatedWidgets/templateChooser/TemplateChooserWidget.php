<?php
Yii::import('application.modules.template.components.TemplateHelper');

/**
 * Widget which shows up modal in which user can select one of available 
 * templates.
 */
class TemplateChooserWidget extends CWidget
{
    public $templateSelectedJSCallback;
    
    public function init()
    {
        if (empty($this->templateSelectedJSCallback)) {
            throw new CException('Please define templateSelectedJSCallback JavaScript function.');
        }
    }
    
    public function run()
    {
        $dataProvider = new CActiveDataProvider('ARTemplate', array(
            'pagination' => false
        ));
        $this->render('widget', compact('dataProvider'));
        $this->registerClientScript();
    }
    
    public function registerClientScript()
    {
        
        $options = [];
        $options['templateSelectedJSCallback'] = $this->templateSelectedJSCallback;
        $options = CJavaScript::encode($options);
        
        Yii::app()->getClientScript()
            ->registerScriptFile(
                Yii::app()->baseUrl . '/js/template.chooser.js', 
                CClientScript::POS_END
            )
            ->registerScript(
                __CLASS__ . '#' . $this->id, 
                "jQuery('#{$this->id}').templateChooser($options);"
            )
            ->registerCssFile(
                Yii::app()->baseUrl . '/css/template.chooser.css'
            )
        ;
    }
    
    
}
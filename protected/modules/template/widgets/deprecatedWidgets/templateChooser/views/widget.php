<?php

echo CHtml::tag('div', array(
    'id' => $this->id,
    'class' => 'template-chooser'
));
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id' => $this->id . '_modal',
    // additional javascript options for the dialog plugin
    'options' => array(
        'title' => 'Select template',
        'autoOpen' => false,
        'modal' => true,
        'width' => 1000
    ),
));

    $this->widget('zii.widgets.CListView', array(
        'id' => $this->id . '_listView',
        'dataProvider' => $dataProvider,
        'itemView' => '_template',
        /*'sortableAttributes'=>array(
            'title',
            'create_time'=>'Post Time',
        ),*/
    ));

$this->endWidget('zii.widgets.jui.CJuiDialog');

echo '</div>';
<div class="template-item">
    <?php echo TemplateHelper::getTemplatePreview($data); ?>
    <p>
        <?php echo CHtml::encode($data->name); ?>
        <br/>
        <?php echo CHtml::link('Select', '#', array(
            'data-template-id' => $data->id,
            'class' => $this->id . '-select-template select-template'
        )); ?>
    </p>
    
</div>
<?php

class Template02GateField extends GateField
{
    public $submitButtonLabel;
    public $orientationHorizontal;
    public $nameVisible;
    public $namePlaceholder;
    public $emailPlaceholder;
    
    
    public function renderMe()
    {
        $this->renderGates();
        
        $input1Html = CHtml::textField('name', '', array(
            'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
            'class' => 'your-name-field form-fields'
        ));
        
        $input2Html = CHtml::emailField('email', '', array(
            'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
            'class' => 'your-email-field form-fields'
        ));
        
        /*$waitingForGatesOtherThanEmail = (
            ($this->likeGateEnabled() && !$this->pageLiked()) || 
            ($this->shareGateEnabled() && !$this->pageShared())
        );*/
        
        $submitButtonHtml = CHtml::submitButton($this->submitButtonLabel, array(
            'class' => 'submit-get-informed',
            'style' => "font-family: 'Helvetica Neue'; font-size: 22px; color: #fff; font-weight: bold;",
            'onclick' => !$this->emailSubscribed() ? 'return subscribeEmail(this)' : 'return showGatePopup()'
        ));
        
        echo '<form>';
        if ($this->orientationHorizontal) {
            echo "<div class=\"form-final-left\">" . ($this->nameVisible ? "$input1Html<br/>" : '') . "$input2Html</div><div class=\"form-final-right\">$submitButtonHtml</div>";
        } else {
            if ($this->nameVisible) {
                echo $input1Html;
            }
            echo $input2Html;
            echo $submitButtonHtml;
        }
        echo '</form>';
        
    }

}
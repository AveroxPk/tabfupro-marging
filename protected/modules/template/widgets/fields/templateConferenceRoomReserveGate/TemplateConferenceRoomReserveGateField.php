<?php

class TemplateConferenceRoomReserveGateField extends GateField
{
    public $submitButtonLabel;
    public $orientationHorizontal;
    public $nameVisible;
	public $nameRequired;
    public $namePlaceholder;
	public $emailRequired;
	public $emailVisible;
    public $emailPlaceholder;
	
	public $phoneRequired;
	public $phoneVisible;
    public $phonePlaceholder;
	
	public $dateoptoneVisible;
	public $dateoptoneRequired;
	public $dateoptonePlaceholder;
	public $dateopttwoVisible;
	public $dateopttwoRequired;
	public $dateopttwoPlaceholder;
	
	public $participentVisible;
	public $participentRequired;
	public $participentPlaceholder;
	
	public $positionVisible;
	public $positionRequired;
	public $positionPlaceholder;
	
	public $departmentVisible;
	public $departmentRequired;
	public $departmentPlaceholder;
	
	public $specialinstructionVisible;
	public $specialinstructionPlaceholder;
    
    
    public function renderMe()
    {
        $this->renderGates();
			
		if($this->nameRequired == 1 && $this->nameVisible == 1)
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
				'required' => 'required'
			));
			
		else 
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
			));
			
		if($this->emailRequired == 1 && $this->emailVisible == 1)
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields'
			));
						
		if($this->phoneRequired == 1 && $this->phoneVisible == 1)
			$input3Html = CHtml::textField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input3Html = CHtml::textField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
			));
			
			
		if($this->participentRequired == 1 && $this->participentVisible == 1)
			$input4Html = CHtml::textField('participent', '', array(
				'placeholder' => $this->participentPlaceholder !== null ? $this->participentPlaceholder : 'Number of Participent',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input4Html = CHtml::textField('participent', '', array(
				'placeholder' => $this->participentPlaceholder !== null ? $this->participentPlaceholder : 'Number of Participent',
				'class' => 'form-fields',
			));
			
		if($this->positionRequired == 1 && $this->positionVisible == 1)
			$input5Html = CHtml::textField('position', '', array(
				'placeholder' => $this->positionPlaceholder !== null ? $this->positionPlaceholder : 'position',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input5Html = CHtml::textField('position', '', array(
				'placeholder' => $this->positionPlaceholder !== null ? $this->positionPlaceholder : 'position',
				'class' => 'form-fields',
			));
			
		if($this->departmentRequired == 1 && $this->departmentVisible == 1)
			$input6Html = CHtml::textField('department', '', array(
				'placeholder' => $this->departmentPlaceholder !== null ? $this->departmentPlaceholder : 'department',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input6Html = CHtml::textField('department', '', array(
				'placeholder' => $this->departmentPlaceholder !== null ? $this->departmentPlaceholder : 'department',
				'class' => 'form-fields',
			));
			
		if($this->dateoptoneRequired == 1 && $this->dateoptoneVisible == 1)
			$input7Html = CHtml::textField('checkin', '', array(
				'placeholder' => 'Conference starts at',
				'class' => 'form-fields form_datetime',
				'required' => 'required'
			));			
		else
			$input7Html = CHtml::textField('checkin', '', array(
				'placeholder' => 'Conference starts at',
				'class' => 'form-fields form_datetime'
			));
			
		if($this->dateopttwoRequired == 1 && $this->dateopttwoVisible == 1)
			$input8Html = CHtml::textField('checkout', '', array(
				'placeholder' => 'Conference ends at',
				'class' => 'form-fields form_datetime',
				'required' => 'required'
			));			
		else
			$input8Html = CHtml::textField('checkout', '', array(
				'placeholder' => 'Conference ends at',
				'class' => 'form-fields form_datetime',
			));
			
		if($this->specialinstructionVisible == 1)	
			$input9Html = CHtml::textArea('description', '', array(
				'placeholder' => $this->specialinstructionPlaceholder !== null ? $this->specialinstructionPlaceholder : 'Special Instructions',
				'class' => 'form-fields text-area',
			));			
			
        
        $submitButtonHtml = CHtml::submitButton($this->submitButtonLabel, array(
            'class' => 'submit-btn-submit',
            'style' => "font-family: 'Helvetica Neue';",
            'onclick' => !$this->emailSubscribed() ? 'return customForm(this)' : 'return showGatePopup()'
        ));
        
        echo '<form>';
        if ($this->orientationHorizontal) {
            echo "<div class=\"form-final-left\">" . ($this->nameVisible ? "$input1Html<br/>" : '') . "$input2Html</div><div class=\"form-final-right\">$submitButtonHtml</div>";
        } else {
			
			echo '<div class="left">';
			
				if ($this->nameVisible) {
					echo $input1Html;
				}
				if($this->emailVisible)
					echo $input2Html;
				if($this->phoneVisible)
					echo $input3Html;
				
				if($this->participentVisible)
					echo $input4Html;
				if($this->positionVisible)
					echo $input5Html;
				if($this->departmentVisible)
					echo $input6Html;
			
			echo '</div><div class="right">';
			
				if($this->dateoptoneVisible)
					echo $input7Html;
				if($this->dateopttwoVisible)
					echo $input8Html;			
				if($this->specialinstructionVisible)
					echo $input9Html;
				
			echo '</div>';
			
			echo '<div class="custom-btn-area">';
				echo $submitButtonHtml;
			echo '</div>';
        }
        echo '</form>';
        
    }

}
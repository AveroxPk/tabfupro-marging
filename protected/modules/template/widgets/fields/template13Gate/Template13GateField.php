<?php

class Template13GateField extends GateField
{
    public $label;
    
    public function renderMe()         
    {
        $this->renderGates();
        ?>

        <div class="download">
            <div class="download-button" onclick="<?=$this->emailSubscribed() ? 'return showGatePopup()' : 'return subscribeEmail(this)' ?>">
                <?= $this->label ?>
            </div>
        </div>
        <?php
        
    }

}

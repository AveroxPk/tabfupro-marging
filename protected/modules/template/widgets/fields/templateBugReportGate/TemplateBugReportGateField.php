<?php

class TemplateBugReportGateField extends GateField
{
    public $submitButtonLabel;
    public $orientationHorizontal;
    public $nameVisible;
	public $nameRequired;
    public $namePlaceholder;
	public $emailRequired;
	public $emailVisible;
    public $emailPlaceholder;
	public $phoneRequired;
	public $phoneVisible;
    public $phonePlaceholder;
	
	public $urlRequired;
	public $urlVisible;
    public $urlPlaceholder;
	
	public $dateoptoneVisible;
	public $dateoptoneRequired;
	public $dateoptonePlaceholder;
	
	public $dropdownoptiononeVisible;
	public $specialinstructionVisible;
	public $specialinstructionPlaceholder;

	
    
    public function renderMe()
    {
        $this->renderGates();
			
		if($this->nameRequired == 1 && $this->nameVisible == 1)
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
				'required' => 'required'
			));
			
		else 
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
			));
			
		if($this->emailRequired == 1 && $this->emailVisible == 1)
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields'
			));
			
			
		if($this->phoneRequired == 1 && $this->phoneVisible == 1)
			$input3Html = CHtml::telField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input3Html = CHtml::telField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
			));
			
		if($this->urlRequired == 1 && $this->urlVisible == 1)
			$input4Html = CHtml::textField('url', '', array(
				'placeholder' => $this->urlPlaceholder !== null ? $this->urlPlaceholder : 'Uploaded image url',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input4Html = CHtml::textField('url', '', array(
				'placeholder' => $this->urlPlaceholder !== null ? $this->urlPlaceholder : 'Uploaded image url',
				'class' => 'form-fields',
			));
			
        /*$waitingForGatesOtherThanEmail = (
            ($this->likeGateEnabled() && !$this->pageLiked()) || 
            ($this->shareGateEnabled() && !$this->pageShared())
        );*/
        if($this->dateoptoneRequired == 1  && $this->dateoptoneVisible == 1)
			$input5Html = CHtml::textField('bugNoticedAt', '', array(
				'placeholder' => $this->dateoptonePlaceholder !== null ? $this->dateoptonePlaceholder : 'Date & Time you first noticed bug',
				'class' => 'form-fields form_datetime',
				'required' => 'required'
			));			
		else
			$input5Html = CHtml::textField('bugNoticedAt', '', array(
				'placeholder' => $this->dateoptonePlaceholder !== null ? $this->dateoptonePlaceholder : 'Date & Time you first noticed bug',
				'class' => 'form-fields form_datetime'
			));
	
			
		if($this->dropdownoptiononeVisible == 1)
			$input6Html = CHtml::dropDownList('impactLevel','4', 
					array( 'low'=>'Low', 
						   'medium'=>'Medium',
						   'heigh' => 'Heigh',
						   'critical' => 'Critical'),
						   
				   array(  
							'options' => array('low'=>array('selected'=>true))
					
			));
			
		if($this->specialinstructionVisible == 1)	
			$input7Html = CHtml::textArea('description', '', array(
				'placeholder' => $this->specialinstructionPlaceholder !== null ? $this->specialinstructionPlaceholder : 'Detail explanation of bug',
				'class' => 'form-fields text-area',
			));			
	
		
        $submitButtonHtml = CHtml::submitButton($this->submitButtonLabel, array(
            'class' => 'submit-btn-submit',
            'style' => "font-family: 'Helvetica Neue'; font-size: 12px; color: #fff; font-weight: normal;",
            'onclick' => !$this->emailSubscribed() ? 'return customForm(this)' : 'return showGatePopup()'
        ));
        
        echo '<form>';
        if ($this->orientationHorizontal) {
            echo "<div class=\"form-final-left\">" . ($this->nameVisible ? "$input1Html<br/>" : '') . "$input2Html</div><div class=\"form-final-right\">$submitButtonHtml</div>";
        } else {
            if ($this->nameVisible) {
                echo $input1Html;
            }
			if($this->emailVisible)
				echo $input2Html;
			if($this->phoneVisible)
				echo $input3Html;
			if($this->urlVisible)
				echo $input4Html;			
			if($this->dateoptoneVisible)
				echo $input5Html;
			
			if($this->dropdownoptiononeVisible){
				echo '<br><lable>Impact: </lable><br>';
				echo $input6Html;
				echo '<br>';
			}
			
			if($this->specialinstructionVisible)
				echo $input7Html;
			
			echo '<div class="custom-btn-area">';
				echo $submitButtonHtml;
			echo '</div>';
        }
        echo '</form>';
        
    }

}
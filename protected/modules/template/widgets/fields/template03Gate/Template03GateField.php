<?php

class Template03GateField extends GateField
{
    public $imageAsset;
    
    private $_preferedWidth;
    private $_preferedHeight;
    
    public function renderMe() 
    {
        $this->renderGates();
        
        $btnAttributes = array(
            'class' => 'get-instant-access'
        );
        
        if (!$this->adminMode) {
            $btnAttributes['onclick'] = 'return showGatePopup()';
        }
        
        if ($this->imageAsset) {
            $btnAttributes['style'] = 'background-image: url('.$this->imageAsset->getUrl().')';
        }
        
        echo CHtml::link('GET INSTANT ACCESS', '#', $btnAttributes);
    }
    
    public function setPreferedWidth($value)
    {
        $this->_preferedWidth = $value;
    }
    
    public function setPreferedHeight($value)
    {
        $this->_preferedHeight = $value;
    }
    
    public function getPreferedWidth()
    {
        return $this->_preferedWidth;
    }
    
    public function getPreferedHeight()
    {
        return $this->_preferedHeight;
    }
}
<?php

class Template12GateField extends GateField
{
    public $label;
    
    public function renderMe()         
    {
        $this->renderGates();
        ?>
        <div class="request">
            <a href="#" class="request-a-demo" onclick="return showGatePopup()"><?=$this->label?></a>
        </div>
        <?php
        
    }

}
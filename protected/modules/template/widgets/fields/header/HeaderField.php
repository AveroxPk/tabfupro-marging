<?php

class HeaderField extends TemplateFieldWidget
{
    public $level = 1;
    public $title;
    
    public function renderMe()
    {
        echo CHtml::tag('h' . $this->level, array(), nl2br(CHtml::encode($this->title)));
    }
}
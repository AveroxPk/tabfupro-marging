<?php

class SocialShareIconsField extends TemplateFieldWidget
{
    public $urlToShare;
    
    private $_socialNetworks = array(
        //Twitter
        array(
            'name' => 'Twitter',
            'icon' => 'twitter.png',
            'shareUrl' => 'https://twitter.com/home?status='
        ),
        
        //Facebook
        array(
            'name' => 'Facebook',
            'icon' => 'facebook.png',
            'shareUrl' => 'https://www.facebook.com/sharer/sharer.php?u='
        ),
        
        //Google+
        array(
            'name' => 'Google+',
            'icon' => 'google.png',
            'shareUrl' => 'https://plus.google.com/share?url='
        ),
        
        //LinkedIn
        array(
            'name' => 'LinkedIn',
            'icon' => 'linkedin.png',
            'shareUrl' => 'https://www.linkedin.com/shareArticle?mini=true&url='
        ),
        
    );
    

    public function renderMe()
    {
        $sUrl = urlencode($this->urlToShare);
        echo '<div class="share-images">';
            foreach ($this->_socialNetworks as $socialNetwork) {
                $img = CHtml::image(Yii::app()->getBaseUrl(true) . '/images/fields/social/' . $socialNetwork['icon'], $socialNetwork['name']);
                if (!$this->adminMode) {
                    $img = CHtml::link($img, $socialNetwork['shareUrl'] . $sUrl, array(
                        'title' => $socialNetwork['name'],
                        'target' => '_blank'
                    ));
                }
                echo $img;
            }
        echo '</div>';
        
        /*<div class="share-images">
            <!-- Share icons -->
            <a href="#" title="Twitter"><img src="resources/img/twitter.png" alt="Twitter" /></a> <!-- Twitter -->
            <a href="#" title="Facebook"><img src="resources/img/facebook.png" alt="Facebook" /></a> <!-- Facebook -->
            <a href="#" title="Google+"><img src="resources/img/google.png" alt="Google+" /></a> <!-- Google+ -->
            <a href="#" title="LinkedIn"><img src="resources/img/linkedin.png" alt="LinkedIn" /></a> <!-- LinkedIn -->
        </div>
        <p class="share-element" style="color: #225aa6; font-size: 18px; font-weight: bold; font-style: italic; font-style: 'Helvetica Neue';">Share</p>*/
    }

}
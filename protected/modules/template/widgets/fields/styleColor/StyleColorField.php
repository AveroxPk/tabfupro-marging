<?php

class StyleColorField extends TemplateFieldWidget
{
    public $selector;
    public $background = true;
    public $color;
    
    public function renderMe() 
    {
        echo '<style>';
            $this->_outputColorStyle();
            $this->_outputDisplayStyle();
        echo '</style>';
    }
    
    public function renderMeForAdmin() 
    {
        echo '<style>';
            $this->_outputColorStyle();
        echo '</style>';
        /*echo '<script type="text/javascript">';
            $encodedSelector = json_encode($this->selector);
            
            if ($this->visible) {
                echo "$($encodedSelector, 'iframe').remove('.hidden-field-cover');";
            } else {
                echo "var container = $($encodedSelector, 'iframe');";
                echo 'if (container.find(".hidden-field-cover").length === 0) {';
                    echo "container.append($('<div class=\"hidden-field-cover\"></div>'));";
                echo '}';
            }
            
        echo '</script>';*/
        
    }
    
    private function _outputColorStyle()
    {
		echo $this->selector;
        echo ' {';
            echo $this->background ? 'background-color' : 'color';
        echo ': ';
            echo $this->color[0] == '#' ? $this->color : "#{$this->color}";
        echo ' !important ;';
		
		if($this->background==2)
			
			echo 'color : #' . $this->color . '; }';
			
		else
			echo '}';
    }
    
    private function _outputDisplayStyle()
    {
        if (!$this->visible) {
            echo $this->selector;
            echo ' {display: none;}';
        }
    }
    
    
    public function run()
    {
        if ($this->adminMode) {
            $this->renderMeForAdmin();
        } else {
            $this->renderMe();
        }
    }

}
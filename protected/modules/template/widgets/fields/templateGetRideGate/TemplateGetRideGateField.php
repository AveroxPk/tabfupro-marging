<?php

class TemplateGetRideGateField extends GateField
{
    public $submitButtonLabel;
    public $orientationHorizontal;
    public $nameVisible;
	public $nameRequired;
    public $namePlaceholder;
	public $emailRequired;
	public $emailVisible;
    public $emailPlaceholder;
	public $phoneRequired;
	public $phoneVisible;
    public $phonePlaceholder;
		
	public $radiooptoneVisible;
	public $dropdownoptiononeVisible;
	public $dropdownoptiontwoVisible;

	public $specialinstructionVisible;
	public $specialinstructionPlaceholder;
	
	//Options Data
	public $radioOptOne = 'new';
	public $radioOptTwo = 'used';

    
    public function renderMe()
    {
        $this->renderGates();
			
		if($this->nameRequired == 1 && $this->nameVisible == 1)
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
				'required' => 'required'
			));
			
		else 
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
			));
			
		if($this->emailRequired == 1 && $this->emailVisible == 1)
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields'
			));
			
			
		if($this->phoneRequired == 1 && $this->phoneVisible == 1)
			$input3Html = CHtml::telField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input3Html = CHtml::telField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
			));
			
		if($this->radiooptoneVisible == 1)
			$input4Html = CHtml::radioButtonList('type', 'field',
                    array(  $this->radioOptOne => ucfirst($this->radioOptOne),
                            $this->radioOptTwo => ucfirst($this->radioOptTwo)),
 					array(
						'labelOptions'=>array('style'=>'display:inline; color:#fff'), // add this code
						//'options' => array($this->radioOptOne=>array('selected'=>true)),
						'separator'=>'  ',
			));
			
		if($this->dropdownoptiononeVisible == 1)
			$input5Html = CHtml::dropDownList('year','11', 
					array( '2010'=>'2010', 
						   '2011'=>'2011',
						   '2012' => '2012',
						   '2013' => '2013',
						   '2014' => '2014',
						   '2015' => '2015',
						   '2016' => '2016',
						   '2017' => '2017',
						   '2018' => '2018',
						   '2019' => '2019',
						   '2020' => '2020'						   
						   ),
						   
				   array(  
							'options' => array('2017'=>array('selected'=>true))
					
			));
			
		if($this->dropdownoptiontwoVisible == 1)
			$input6Html = CHtml::dropDownList('make','8', 
					array( 'bmw'=>'BMW', 
						   'ferrari' => 'Ferrari',
						   'ford' => 'Ford',
						   'honda'=>'Honda',
						   'mercedes' => 'Mercedes',
						   'nissan' => 'Nissan',
						   'suzuki' => 'Suzuki',
						   'toyota' => 'Toyota'
						   ),
						   
				   array(  
							'options' => array('honda'=>array('selected'=>true))
					
			));
			
		if($this->specialinstructionVisible == 1)	
			$input7Html = CHtml::textArea('description', '', array(
				'placeholder' => $this->specialinstructionPlaceholder !== null ? $this->specialinstructionPlaceholder : 'Detail explanation',
				'class' => 'form-fields text-area',
			));			
	
		
        $submitButtonHtml = CHtml::submitButton($this->submitButtonLabel, array(
            'class' => 'submit-btn-submit',
            'onclick' => !$this->emailSubscribed() ? 'return customForm(this)' : 'return showGatePopup()'
        ));
        
        echo '<form>';
        if ($this->orientationHorizontal) {
            echo "<div class=\"form-final-left\">" . ($this->nameVisible ? "$input1Html<br/>" : '') . "$input2Html</div><div class=\"form-final-right\">$submitButtonHtml</div>";
        } else {
			
            if ($this->radiooptoneVisible) {
				echo '<lable>Type of Vehicle</lable><br>';
                echo $input4Html;
				echo '<br>';
            }
			if($this->dropdownoptiononeVisible){
				echo $input5Html;
			}
			if($this->dropdownoptiontwoVisible){
				echo $input6Html;
				echo '<br>';
			}
			
            if ($this->nameVisible) {
                echo $input1Html;
            }
			if($this->emailVisible)
				echo $input2Html;
			if($this->phoneVisible)
				echo $input3Html;
			
			if($this->specialinstructionVisible)
				echo $input7Html;
			
			echo '<div class="custom-btn-area">';
				echo $submitButtonHtml;
			echo '</div>';
        }
        echo '</form>';
        
    }

}
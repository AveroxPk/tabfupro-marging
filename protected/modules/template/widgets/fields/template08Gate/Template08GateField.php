<?php

class Template08GateField extends GateField
{
    public $label;
    
    
    public function renderMe() 
    {
        $this->renderGates();
        
        echo CHtml::submitButton(strip_tags($this->label), array('onclick' => $this->adminMode ? 'return false' : 'return subscribeEmail(this)'));
        
    }
}
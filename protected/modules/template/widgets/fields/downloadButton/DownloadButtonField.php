<?php

class DownloadButtonField extends TemplateFieldWidget
{
    public $title;
    public $subtitle;
    
    public $url;
    
    /**
     * @var string
     */
    public $iconPlaceholderSrc;
    
    /**
     * @var Asset
     */
    public $iconImage;
    
    public $color;
    
    
    private function _renderButton($return = false)
    {
        if ($return) {
            ob_start();
        }
        
        if (strlen($this->color) > 0 && substr($this->color, 0, 1) !== '#') {
            $this->color = "#{$this->color}";
        }
        
        echo '<div class="download-button"' . (strlen($this->color) > 0 ? ' style="background-color: ' . $this->color . '"' : '') . '>';
            echo '<div class="image">';
                echo CHtml::image($this->iconImage ? $this->iconImage->getUrl() : $this->iconPlaceholderSrc);
            echo '</div>';
            if (strlen(trim($this->title)) > 0 || strlen(trim($this->subtitle)) > 0) {
                echo '<div class="text-content">';
                    echo strlen(trim($this->title)) > 0 ? CHtml::tag('h1', array(), $this->title) : '';
                    echo strlen(trim($this->subtitle)) > 0 ? CHtml::tag('h2', array(), $this->subtitle) : '';
                echo '</div>';
            }
        echo '</div>';
        
        if ($return) {
            return ob_get_clean();
        }
        
    }
    
    public function renderMe() 
    {
        echo CHtml::link($this->_renderButton(true), $this->url, strlen($this->color) > 0 ? 
			array('target' => '_blank', 'style' => "background-color: {$this->color}") :
			array('target' => '_blank'));
    }
    
    public function renderMeForAdmin() 
    {
        echo CHtml::link($this->_renderButton(true), '#', strlen($this->color) > 0 ? 
			array('style' => "background-color: {$this->color}") :
			array());
    }
    
}
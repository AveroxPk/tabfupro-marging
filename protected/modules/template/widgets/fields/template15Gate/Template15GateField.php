<?php

class Template15GateField extends GateField
{
    public $label;
    public $imageAsset;
    public $which;
    
    private $_preferedWidth;
    private $_preferedHeight;
    
    public function renderMe()         
    {
        $this->renderGates();
        
        if ($this->which == 'top') {
            ?>
            <input type="submit" class="button-blue" name="submit" value="<?=CHtml::encode($this->label)?>" onclick="return showGatePopup()"<?php if ($this->imageAsset) { ?> style="background: url(<?= $this->imageAsset->getUrl() ?>) no-repeat top left"<?php } ?> />
            <?php
        } elseif ($this->which == 'middle') {
            ?>
            <a href="#" class="green-button" onclick="return showGatePopup()"<?php if ($this->imageAsset) { ?> style="background: url(<?= $this->imageAsset->getUrl() ?>) no-repeat top left"<?php } ?>><?=CHtml::encode($this->label)?></a>
            <?php
        } elseif ($this->which == 'yellow') {
            ?>
            <a href="#" class="yellow-button" onclick="return showGatePopup()"<?php if ($this->imageAsset) { ?> style="background: url(<?= $this->imageAsset->getUrl() ?>) no-repeat top left"<?php } ?>><?=CHtml::encode($this->label)?></a>
            <?php
        } elseif ($this->which == 'footer') {
            ?>
            <div class="email-footer">
                <input type="email" name="footer-email" class="footer-email-input" placeholder="<?=CHtml::encode($this->label)?>">
                <input type="submit" name="submit" value="Submit" class="footer-submit" onclick="<?=!$this->emailSubscribed() ? 'return subscribeEmail(this)' : 'return showGatePopup()' ?>">
            </div>
            <?php
        }
    }
    
    public function setPreferedWidth($value)
    {
        $this->_preferedWidth = $value;
    }
    
    public function setPreferedHeight($value)
    {
        $this->_preferedHeight = $value;
    }
    
    public function getPreferedWidth()
    {
        return $this->_preferedWidth;
    }
    
    public function getPreferedHeight()
    {
        return $this->_preferedHeight;
    }

}

<?php

class Template5k10kRacingGateField extends GateField
{
    public $submitButtonLabel;
    public $orientationHorizontal;
    public $nameVisible;
	public $nameRequired;
    public $namePlaceholder;
	public $emailRequired;
	public $emailVisible;
    public $emailPlaceholder;
	public $addressRequired;
	public $addressVisible;
    public $addressPlaceholder;
	public $cityRequired;
	public $cityVisible;
    public $cityPlaceholder;
	public $stateRequired;
	public $stateVisible;
    public $statePlaceholder;
	public $zipRequired;
	public $zipVisible;
    public $zipPlaceholder;
	public $phoneRequired;
	public $phoneVisible;
    public $phonePlaceholder;
	
	public $dateoptoneVisible;
	public $dateoptoneRequired;
	public $dateoptonePlaceholder;
	
	public $radiooptoneVisible;
	public $radioopttwoVisible;
	public $dropdownoptiononeVisible;
	
	//Options Data
	public $radioOptOne = 'male';
	public $radioOptTwo = 'female';
	public $radioOptThree = '5k';
	public $radioOptFour = '10k';
    
    public function renderMe()
    {
        $this->renderGates();
			
		if($this->nameRequired == 1 && $this->nameVisible == 1)
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
				'required' => 'required'
			));
			
		else 
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
			));
			
		if($this->emailRequired == 1 && $this->emailVisible == 1)
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields'
			));
			
		if($this->addressRequired == 1 && $this->addressVisible == 1)
			$input3Html = CHtml::textField('address', '', array(
				'placeholder' => $this->addressPlaceholder !== null ? $this->addressPlaceholder : 'your address',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input3Html = CHtml::textField('address', '', array(
				'placeholder' => $this->addressPlaceholder !== null ? $this->addressPlaceholder : 'your address',
				'class' => 'form-fields',
			));
			
		if($this->cityRequired == 1 && $this->cityVisible == 1)
			$input4Html = CHtml::textField('city', '', array(
				'placeholder' => $this->cityPlaceholder !== null ? $this->cityPlaceholder : 'your city',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input4Html = CHtml::textField('city', '', array(
				'placeholder' => $this->cityPlaceholder !== null ? $this->cityPlaceholder : 'your city',
				'class' => 'form-fields',
			));
        
			
		if($this->stateRequired == 1 && $this->stateVisible == 1)
			$input5Html = CHtml::textField('state', '', array(
				'placeholder' => $this->statePlaceholder !== null ? $this->statePlaceholder : 'your state',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input5Html = CHtml::textField('state', '', array(
				'placeholder' => $this->statePlaceholder !== null ? $this->statePlaceholder : 'your state',
				'class' => 'form-fields',
			));
			
		if($this->zipRequired == 1 && $this->zipVisible == 1)
			$input6Html = CHtml::textField('zip', '', array(
				'placeholder' => $this->zipPlaceholder !== null ? $this->zipPlaceholder : 'Zip code',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input6Html = CHtml::textField('zip', '', array(
				'placeholder' => $this->zipPlaceholder !== null ? $this->zipPlaceholder : 'Zip code',
				'class' => 'form-fields',
			));
			
		if($this->phoneRequired == 1 && $this->phoneVisible == 1)
			$input7Html = CHtml::telField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input7Html = CHtml::telField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
			));
			
			
        /*$waitingForGatesOtherThanEmail = (
            ($this->likeGateEnabled() && !$this->pageLiked()) || 
            ($this->shareGateEnabled() && !$this->pageShared())
        );*/
	
        if($this->dateoptoneRequired == 1  && $this->dateoptoneVisible == 1)
			$input8Html = CHtml::textField('dateOfBirth', '', array(
				'placeholder' => $this->dateoptonePlaceholder !== null ? $this->dateoptonePlaceholder : 'Date of Birth',
				'class' => 'form-fields form_datetime',
				'required' => 'required'
			));			
		else
			$input8Html = CHtml::textField('dateOfBirth', '', array(
				'placeholder' => $this->dateoptonePlaceholder !== null ? $this->dateoptonePlaceholder : 'Date of Birth',
				'class' => 'form-fields form_datetime'
			));

		if($this->radiooptoneVisible == 1)
			$input9Html = CHtml::radioButtonList('gender', 'field',
                    array(  $this->radioOptOne => ucfirst($this->radioOptOne),
                            $this->radioOptTwo => ucfirst($this->radioOptTwo)),
 					array(
						'labelOptions'=>array('style'=>'display:inline; color:#fff'), // add this code
						'separator'=>'  ',
			));
		
			
		if($this->dropdownoptiononeVisible == 1)
			$input10Html = CHtml::dropDownList('shirtSize','12', 
					array( 'small'=>'Small', 
						   'medium'=>'Medium',
						   'large' => 'Large'),
						   
				   array(  
							'options' => array('large'=>array('selected'=>true))
					
			));
			
		if($this->radioopttwoVisible == 1)
			$input11Html = CHtml::radioButtonList('racelength', 'field',
                    array(  $this->radioOptThree => ucfirst($this->radioOptThree),
                            $this->radioOptFour => ucfirst($this->radioOptFour)),
 					array(
						'labelOptions'=>array('style'=>'display:inline; color:#fff'), // add this code
						'separator'=>'  ',
			));
		
        $submitButtonHtml = CHtml::submitButton($this->submitButtonLabel, array(
            'class' => 'submit-btn-submit',
            'style' => "font-family: 'Helvetica Neue'; font-size: 12px; color: #fff; font-weight: normal;",
            'onclick' => !$this->emailSubscribed() ? 'return customForm(this)' : 'return showGatePopup()'
        ));
        
        echo '<form>';
        if ($this->orientationHorizontal) {
            echo "<div class=\"form-final-left\">" . ($this->nameVisible ? "$input1Html<br/>" : '') . "$input2Html</div><div class=\"form-final-right\">$submitButtonHtml</div>";
        } else {
            if ($this->nameVisible) {
                echo $input1Html;
            }
			if($this->emailVisible)
				echo $input2Html;
			if($this->addressVisible)
				echo $input3Html;
			if($this->cityVisible)
				echo $input4Html;
			if($this->stateVisible)
				echo $input5Html;
			if($this->zipVisible)
				echo $input6Html;
			if($this->phoneVisible)
				echo $input7Html;
			
			if($this->dateoptoneVisible)
				echo $input8Html;
			if($this->radiooptoneVisible){
				echo '<br><lable for="gender">Gender: </lable>';
				echo $input9Html;
				echo '<br>';
			}
			if($this->dropdownoptiononeVisible){
				echo '<br><lable for="shirtSize">T-Shirt Size: </lable>';
				echo $input10Html;
				echo '<br>';
			}
			if($this->radioopttwoVisible){
				echo '<br><lable for="racelength">Race Length: </lable>';
				echo $input11Html;
				echo '<br>';
			}

			echo '<div class="custom-btn-area">';
				echo '<span><i class="icofont icofont-paper-plane"></i></span>';
				echo $submitButtonHtml;
			echo '</div>';
        }
        echo '</form>';
        
    }

}
<?php

class Template06GateField extends GateField
{
    public $imageAsset;
    
    private $_preferedWidth;
    private $_preferedHeight;
    
    
    public function renderMe() 
    {
        $this->renderGates();
        $htmlOptions = array(
            'onclick' => $this->adminMode ? 'return false' : 'return subscribeEmail(this)'
        );
        if ($this->imageAsset) {
            $htmlOptions['style'] = 'background-image: url('.$this->imageAsset->getUrl().')';
        }
        
        echo '<input type="text" name="email" placeholder="sign up for news">';
        echo CHtml::submitButton('GO', $htmlOptions);
    }
    
    public function setPreferedWidth($value)
    {
        $this->_preferedWidth = $value;
    }
    
    public function setPreferedHeight($value)
    {
        $this->_preferedHeight = $value;
    }
    
    public function getPreferedWidth()
    {
        return $this->_preferedWidth;
    }
    
    public function getPreferedHeight()
    {
        return $this->_preferedHeight;
    }
}
<?php

class Template18GateField extends GateField
{
    public $submitButtonLabel;
    public $orientationHorizontal;
    public $nameVisible;
	public $nameRequired;
    public $namePlaceholder;
	public $emailRequired;
	public $emailVisible;
    public $emailPlaceholder;
	public $addressRequired;
	public $addressVisible;
    public $addressPlaceholder;
	public $cityRequired;
	public $cityVisible;
    public $cityPlaceholder;
	public $stateRequired;
	public $stateVisible;
    public $statePlaceholder;
	public $zipRequired;
	public $zipVisible;
    public $zipPlaceholder;
	public $phoneRequired;
	public $phoneVisible;
    public $phonePlaceholder;
	
	public $dateOfBirthVisible;
	public $dateOfBirthRequired;
	public $dateOfBirthPlaceholder;
	public $genderVisible;
	public $chooseRaceVisible;
	public $dropdownOptionVisible;
    
    
    public function renderMe()
    {
        $this->renderGates();
			
		if($this->nameRequired == 1 && $this->nameVisible == 1)
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
				'required' => 'required'
			));
			
		else 
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
			));
			
		if($this->emailRequired == 1 && $this->emailVisible == 1)
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields'
			));
			
		if($this->addressRequired == 1 && $this->addressVisible == 1)
			$input3Html = CHtml::textField('address', '', array(
				'placeholder' => $this->addressPlaceholder !== null ? $this->addressPlaceholder : 'your address',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input3Html = CHtml::textField('address', '', array(
				'placeholder' => $this->addressPlaceholder !== null ? $this->addressPlaceholder : 'your address',
				'class' => 'form-fields',
			));
			
		if($this->cityRequired == 1 && $this->cityVisible == 1)
			$input4Html = CHtml::textField('city', '', array(
				'placeholder' => $this->cityPlaceholder !== null ? $this->cityPlaceholder : 'your city',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input4Html = CHtml::textField('city', '', array(
				'placeholder' => $this->cityPlaceholder !== null ? $this->cityPlaceholder : 'your city',
				'class' => 'form-fields',
			));
        
			
		if($this->stateRequired == 1 && $this->stateVisible == 1)
			$input5Html = CHtml::textField('state', '', array(
				'placeholder' => $this->statePlaceholder !== null ? $this->statePlaceholder : 'your state',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input5Html = CHtml::textField('state', '', array(
				'placeholder' => $this->statePlaceholder !== null ? $this->statePlaceholder : 'your state',
				'class' => 'form-fields',
			));
			
		if($this->zipRequired == 1 && $this->zipVisible == 1)
			$input6Html = CHtml::textField('zip', '', array(
				'placeholder' => $this->zipPlaceholder !== null ? $this->zipPlaceholder : 'Zip code',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input6Html = CHtml::textField('zip', '', array(
				'placeholder' => $this->zipPlaceholder !== null ? $this->zipPlaceholder : 'Zip code',
				'class' => 'form-fields',
			));
			
		if($this->phoneRequired == 1 && $this->phoneVisible == 1)
			$input7Html = CHtml::telField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input7Html = CHtml::telField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
			));
			
			
        /*$waitingForGatesOtherThanEmail = (
            ($this->likeGateEnabled() && !$this->pageLiked()) || 
            ($this->shareGateEnabled() && !$this->pageShared())
        );*/
        if($this->dateOfBirthRequired == 1  && $this->dateOfBirthVisible == 1)
			$input8Html = CHtml::textField('dateOfBirth', '', array(
				'placeholder' => $this->dateOfBirthPlaceholder !== null ? $this->dateOfBirthPlaceholder : 'Date of Birth',
				'class' => 'form-fields form_datetime',
				'required' => 'required'
			));			
		else
			$input8Html = CHtml::textField('dateOfBirth', '', array(
				'placeholder' => $this->dateOfBirthPlaceholder !== null ? $this->dateOfBirthPlaceholder : 'Date of Birth',
				'class' => 'form-fields form_datetime'
			));

		if($this->genderVisible == 1)
			$input9Html = CHtml::radioButtonList('gender', 'field',
                    array(  'male' => 'Male',
                            'female' => 'Female'),
 					array(
						'labelOptions'=>array('style'=>'display:inline; color:#fff'), // add this code
						'separator'=>'  ',
			));
			
        $submitButtonHtml = CHtml::submitButton($this->submitButtonLabel, array(
            'class' => 'submit-btn-submit',
            'style' => "font-family: 'Helvetica Neue'; font-size: 12px; color: #fff; font-weight: normal;",
            'onclick' => !$this->emailSubscribed() ? 'return hotelReserve(this)' : 'return showGatePopup()'
        ));
        
        echo '<form>';
        if ($this->orientationHorizontal) {
            echo "<div class=\"form-final-left\">" . ($this->nameVisible ? "$input1Html<br/>" : '') . "$input2Html</div><div class=\"form-final-right\">$submitButtonHtml</div>";
        } else {
            if ($this->nameVisible) {
                echo $input1Html;
            }
			if($this->emailVisible)
				echo $input2Html;
			if($this->addressVisible)
				echo $input3Html;
			if($this->cityVisible)
				echo $input4Html;
			if($this->stateVisible)
				echo $input5Html;
			if($this->zipVisible)
				echo $input6Html;
			if($this->phoneVisible)
				echo $input7Html;
			
			if($this->dateOfBirthVisible)
				echo $input8Html;
			if($this->genderVisible)
				echo $input9Html;
			
			echo '<div class="custom-btn-area">';
				echo '<span><i class="icofont icofont-paper-plane"></i></span>';
				echo $submitButtonHtml;
			echo '</div>';
        }
        echo '</form>';
        
    }

}
<?php

class Template08SocialButtonField extends TemplateFieldWidget
{
    public $url;
    public $iconSrc;
    public $buttonTitle;
    
    public function renderMe()
    {
        if (empty(trim($this->url))) {
            $this->url = '';
        }
        ?>
        <div class="social">
            <div class="left">
                <a href="<?=CHtml::encode($this->url)?>" target="_blank">
                    <img src="<?=CHtml::encode($this->iconSrc)?>" alt="">
                </a>
            </div>
            <div class="right">
                <a href="<?=CHtml::encode($this->url)?>" class="button" target="_blank">
                   <span><?=CHtml::encode($this->buttonTitle)?></span>
                </a>
            </div>
        </div>
        <?php
    }
}
<?php

class Template09GateField extends GateField
{
    public $url;
    
    public function renderMe()
    {
        $this->renderGates();
        
        if ($this->emailSubscribed()) {
            $this->renderVideo();
        } else {
            $this->renderThumbnail();
        }
    }
    
    public function renderThumbnail()
    {
        $urlVars = [];
        parse_str( parse_url( $this->url, PHP_URL_QUERY ), $urlVars );
        $ytId = $urlVars['v'];

        echo CHtml::image("https://img.youtube.com/vi/$ytId/0.jpg");
    }
    
    
    public function renderVideo() 
    {
        if (empty($this->url)) {
            return;
        }
        
        //Youtube?
        if (strstr($this->url, 'youtube') !== false) {
            //<iframe width="560" height="315" src="//www.youtube.com/embed/EQ1HKCYJM5U?rel=0" frameborder="0" allowfullscreen></iframe>
            
            $ytRegEx = <<<'RegEx'
                ~
                ^(?:https?://)?              # Optional protocol
                 (?:www\.)?                  # Optional subdomain
                 (?:youtube\.com|youtu\.be)  # Mandatory domain name
                 /watch\?v=([^&]+)           # URI with video id as capture group 1
                ~x
RegEx;

            $matches = array();
            if (!preg_match($ytRegEx, $this->url, $matches)) {
                return;
            }
            $embedVideoUrl = 'https://www.youtube.com/embed/' . $matches[1] . '?rel=0';
            
            echo CHtml::tag('iframe', array(
                'width' => 320, //560,
                'height' => 180, //315,
                'src' => $embedVideoUrl,
                'frameborder' => 0,
                'allowfullscreen' => null
            ), '&nbsp;');
            return;
        }
        
        //Vimeo?
        if (strstr($this->url, 'vimeo') !== false) {
            echo 'Vimeo not supported yet.';
            return;
        }
        
    }
    
    public function renderMeForAdmin() 
    {
        if (empty($this->url)) {
            echo CHtml::image($this->getAssetsUrl() . '/placeholder.png');
            return;
        }
        
        if (strstr($this->url, 'youtube') !== false) {
            echo CHtml::image($this->getAssetsUrl() . '/yt_placeholder.png');
            return;
        }
        
        
    }
    
    private $_assetsUrl;
    public function getAssetsUrl()
    {
        if (empty($this->_assetsUrl)) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.template.widgets.fields.video.assets'));
        }
        return $this->_assetsUrl;
    }

}

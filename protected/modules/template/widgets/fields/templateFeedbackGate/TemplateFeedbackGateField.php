<?php

class TemplateFeedbackGateField extends GateField
{
    public $submitButtonLabel;
    public $orientationHorizontal;
    public $nameVisible;
	public $nameRequired;
    public $namePlaceholder;
	public $emailRequired;
	public $emailVisible;
    public $emailPlaceholder;
	
	public $phoneRequired;
	public $phoneVisible;
    public $phonePlaceholder;
	
	
	public $dropdownoptiononeVisible;	
	public $specialinstructionVisible;
	public $specialinstructionPlaceholder;
    
	public $informationVisible;
	
    public function renderMe()
    {
        $this->renderGates();
		
	
		if($this->nameRequired == 1 && $this->nameVisible == 1)
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
				'required' => 'required'
			));
			
		else 
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
			));
			
		if($this->emailRequired == 1 && $this->emailVisible == 1)
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields'
			));
						
		if($this->phoneRequired == 1 && $this->phoneVisible == 1)
			$input3Html = CHtml::textField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input3Html = CHtml::textField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
			));
			
			
		if($this->dropdownoptiononeVisible == 1)
			$input4Html = CHtml::dropDownList('type','4', 
					array( 'suggession'=>'Suggession', 
						   'compliment'=>'Compliment',
						   'complaint' => 'Complaint',
						   'other' => 'Other'),
						   
				   array(  
							'options' => array('suggession'=>array('selected'=>true)),
							'class' => 'form-fields'
					
			));
			
		if($this->specialinstructionVisible == 1)	
			$input5Html = CHtml::textArea('description', '', array(
				'placeholder' => 'Suggestions/Comments ',
				'class' => 'form-fields text-area',
			));			
			
        
        $submitButtonHtml = CHtml::submitButton($this->submitButtonLabel, array(
            'class' => 'submit-btn-submit',
            'style' => "font-family: 'Helvetica Neue';",
            'onclick' => !$this->emailSubscribed() ? 'return customForm(this)' : 'return showGatePopup()'
        ));
        
        echo '<form>';
        if ($this->orientationHorizontal) {
            echo "<div class=\"form-final-left\">" . ($this->nameVisible ? "$input1Html<br/>" : '') . "$input2Html</div><div class=\"form-final-right\">$submitButtonHtml</div>";
        } else {
			
			if($this->dropdownoptiononeVisible)
				echo $input4Html;			
			if($this->specialinstructionVisible)
				echo $input5Html;
			
			if($this->informationVisible)
				echo '<div class="information"><span><b>Please complete the following details</b></span></div>';

			
			if ($this->nameVisible) 
				echo $input1Html;
			
			if($this->emailVisible)
				echo $input2Html;
			
			if($this->phoneVisible)
				echo $input3Html;
				
			
			echo '<div class="custom-btn-area">';
				echo $submitButtonHtml;
			echo '</div>';
        }
        echo '</form>';
        
    }

}
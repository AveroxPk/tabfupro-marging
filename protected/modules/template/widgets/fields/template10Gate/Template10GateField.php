<?php

class Template10GateField extends GateField
{
    public $imageAsset;
    
    private $_preferedWidth;
    private $_preferedHeight;
    
    public function renderMe() 
    {
        $this->renderGates();
        
        $btnAttributes = array(
            'class' => 'get-instant-access'
        );
        
        if ($this->emailGateEnabled() && !$this->adminMode) {
            $btnAttributes['onclick'] = 'return subscribeEmail(this)';
        }
        
        if ($this->imageAsset) {
            $btnAttributes['style'] = 'background-image: url('.$this->imageAsset->getUrl().')';
        }
        
        echo CHtml::link('GET INSTANT ACCESS', '#', $btnAttributes);
    }
    
    public function setPreferedWidth($value)
    {
        $this->_preferedWidth = $value;
    }
    
    public function setPreferedHeight($value)
    {
        $this->_preferedHeight = $value;
    }
    
    public function getPreferedWidth()
    {
        return $this->_preferedWidth;
    }
    
    public function getPreferedHeight()
    {
        return $this->_preferedHeight;
    }
}
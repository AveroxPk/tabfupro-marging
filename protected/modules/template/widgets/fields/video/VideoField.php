<?php

class VideoField extends TemplateFieldWidget
{
    public $url;
    public $placeholderSrc = 'placeholder.png';
    public $autoplay;
    public $forceHd;
    
    public function renderMe() 
    {

        if (empty($this->url)) {
            return;
        }
        //Youtube?
        if (strstr($this->url, 'youtube') !== false || strstr($this->url, 'youtu.be') !== false) {
            //<iframe width="560" height="315" src="//www.youtube.com/embed/EQ1HKCYJM5U?rel=0" frameborder="0" allowfullscreen></iframe>
            
            $ytRegEx = <<<'RegEx'
                ~
                ^https?://(?:www\.)?youtu(?:be\.com/watch\?v=|\.be/)([-_\w]*)(&(amp;)?[\w\?=]*)?
                ~x
RegEx;
 
            $matches = array();
            if (!preg_match($ytRegEx, $this->url, $matches)) {
                return;
            }
            // https://developers.google.com/youtube/player_parameters
            $embedVideoUrl = 'https://www.youtube.com/embed/' . $matches[1] . '?rel=0&showinfo=0&controls=2';
            
            if($this->autoplay) {
                $embedVideoUrl .= '&autoplay=1';
            } else {
                $embedVideoUrl .= '&autoplay=0';
            }
            
            if($this->forceHd) {
                $embedVideoUrl .= '&vq=hd720';
            }
     
            echo CHtml::tag('iframe', array(
                'width' => $this->forceHd ? 1280 : 721, //560,
                'height' => $this->forceHd ? 720 : 423, //315,
                'src' => $embedVideoUrl,
                'frameborder' => 0,
                'allowfullscreen' => null
            ), '&nbsp;');
            return;
        }
        
        //Vimeo?
        if (strstr($this->url, 'vimeo') !== false) {
            echo 'Vimeo not supported yet.';
            return;
        }
        
    }
    
    public function renderMeForAdmin() 
    {
        if (!empty($this->url) && (strstr($this->url, 'youtube') !== false || strstr($this->url, 'youtu.be') !== false)) {
            echo CHtml::image($this->getAssetsUrl() . '/yt_placeholder.png');
            return;
        }
        
        echo CHtml::image($this->getAssetsUrl() . '/' . $this->placeholderSrc);
        return;
    }
    
    private $_assetsUrl;
    public function getAssetsUrl()
    {
        if (empty($this->_assetsUrl)) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.template.widgets.fields.video.assets'));
        }
        return $this->_assetsUrl;
    }

}
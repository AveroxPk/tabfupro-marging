<?php

class SocialFollowIconsField extends TemplateFieldWidget
{
    public $twitterUrl;
    public $facebookUrl;
    public $googleUrl;
    public $linkedInUrl;
    
    public $overrideTwitterIconSrc;
    public $overrideFacebookIconSrc;
    public $overrideGoogleIconSrc;
    public $overrideLinkedInIconSrc;
    
    private $_socialNetworks = array(
        //Twitter
        array(
            'name'          => 'Twitter',
            'icon'          => 'twitter.png',
            'urlAttribute'  => 'twitterUrl',
            'overrideIcon'  => 'overrideTwitterIconSrc'
        ),
        
        //Facebook
        array(
            'name'          => 'Facebook',
            'icon'          => 'facebook.png',
            'urlAttribute'  => 'facebookUrl',
            'overrideIcon'  => 'overrideFacebookIconSrc'
        ),
        
        //Google+
        array(
            'name'          => 'Google+',
            'icon'          => 'google.png',
            'urlAttribute'  => 'googleUrl',
            'overrideIcon'  => 'overrideGoogleIconSrc'
        ),
        
        //LinkedIn
        array(
            'name'          => 'LinkedIn',
            'icon'          => 'linkedin.png',
            'urlAttribute'  => 'linkedInUrl',
            'overrideIcon'  => 'overrideLinkedInIconSrc'
        ),
		
        
    );
    

    public function renderMe()
    {
        echo '<div class="share-images">';
            foreach ($this->_socialNetworks as $socialNetwork) {
                
                $url = $this->{$socialNetwork['urlAttribute']};
                $iconUrl = $this->{$socialNetwork['overrideIcon']} ? : (Yii::app()->getBaseUrl(true) . '/images/fields/social/' . $socialNetwork['icon']);
                
                //Skip not defined social networks, although show their icons
                //in editor (admin mode)
                if (empty($url) && !$this->adminMode) {
                    continue;
                }
                
                $img = CHtml::image($iconUrl, $socialNetwork['name']);
                echo CHtml::link($img, $this->adminMode ? '#' : $url, array(
                    'title' => $socialNetwork['name'],
                    'target' => '_blank'
                ));
                
            }
        echo '</div>';
    }

}
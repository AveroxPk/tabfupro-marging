<?php

abstract class TemplateFieldWidget extends CWidget
{    
    public $adminMode = false;
    public $visible = true;
    public $duplicateTag = '';
    
    public $adminTag;
    
    /**
     *
     * @var array
     */
    public $rendererParams;
    
    public function run()
    {
        
        if ($this->adminMode) {
            $this->renderMeForAdmin();
            if (!$this->visible) {
                echo '<div class="hidden-field-cover">';
                echo '</div>';
            }
        } else {
            if (!$this->visible) {
                return;
            }
            $this->renderMe();
        }
    }

    abstract public function renderMe();
    
    /**
     * Please override this method in case widget content should be different
     * for admin's editor preview. By default it's the same as for a fan/visitor.
     */
    public function renderMeForAdmin()
    {
        $this->renderMe();
    }
    
}

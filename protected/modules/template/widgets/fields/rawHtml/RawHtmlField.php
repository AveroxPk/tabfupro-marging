<?php

class RawHtmlField extends TemplateFieldWidget
{
    public $content;
    
    public function renderMe()
    {
        echo $this->content;
    }
}
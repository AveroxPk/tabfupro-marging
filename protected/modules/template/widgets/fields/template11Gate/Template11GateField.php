<?php

class Template11GateField extends GateField
{
    public $label;
    
    public function renderMe()         
    {
        $this->renderGates();
        ?>
        <div class="text-on-bg">
            <a href="#" class="download-now" onclick="<?=$this->emailSubscribed() ? 'return showGatePopup()' : 'return subscribeEmail(this)' ?>"><span><?= $this->label ?></span></a>
        </div>
        <?php
        
    }

}
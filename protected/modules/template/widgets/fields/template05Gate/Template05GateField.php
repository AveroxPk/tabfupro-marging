<?php

class Template05GateField extends GateField
{
    public $label = '';
    public $url;
    public $imageAsset;
    
    private $_preferedWidth;
    private $_preferedHeight;
    
    public function renderMe() 
    {
        $this->renderGates();
        
        $btnAttribute = '';
        $label = '';
        if ($this->imageAsset) {
            $btnAttribute = ' style="background-image: url('.$this->imageAsset->getUrl().')"';
        }
        else {
            $label = $this->label;
        }
        
        if ($this->atLeastOneGateEnabled()) {
            $this->url = '#';
            echo '<div class="download-button" onclick="return showGatePopup()"'.$btnAttribute.'>';
        } else {
            echo '<div class="download-button"'.$btnAttribute.'>';
        }
        echo $label;
        echo '</div>';
    }
    
    public function setPreferedWidth($value)
    {
        $this->_preferedWidth = $value;
    }
    
    public function setPreferedHeight($value)
    {
        $this->_preferedHeight = $value;
    }
    
    public function getPreferedWidth()
    {
        return $this->_preferedWidth;
    }
    
    public function getPreferedHeight()
    {
        return $this->_preferedHeight;
    }
}
<?php

abstract class GateField extends TemplateFieldWidget
{ 
    const CUSTOM_CODE_GATE_DEFAULT_CONTENT_HEIGHT = 150;
    const CUSTOM_CODE_GATE_DEFAULT_CONTENT_WIDTH = 500;
    
    
    public function getViewPath($checkTheme = false) 
    {
        return __DIR__ . '/views';
    }
    
    /* LIKE GATE */
    protected function renderLikeGate()
    {
        $this->render('likeGatePrompt');
        
    }
    
    protected function renderLikeConfirmed()
    {
        // Track action
        Yii::import('application.modules.analytics.*');
        Analytics::track(Analytics::ANALYTICS_PAGE, PageAnalytics::ACTION_EMAIL_GATE, $this->rendererParams['tabId']);
        $this->renderDownloadButton('like');
    }
    /* END LIKE GATE */
    
    /* SHARE GATE */
    protected function renderShareGate()
    {
        $this->render('shareGatePrompt');
        return;
        $shareButtonAttrs = array(
            'onclick' => 'shareButtonClick()',
            'style' => 'position: absolute; top: 50%; margin-top: -30px; right: 10px; cursor: pointer;'
        );
        
        //Disable "Share" button clicking in admin mode
        if ($this->adminMode) {
            unset($shareButtonAttrs['onclick']);
        }
        
        //echo '<div style="position: relative; width: 100%; height: 100%;">';
        ?>
        <div class="share-gate-text">
            <?=$this->rendererParams['gatesConfig']['share']['promptText']?>
        </div>
        <?php
        
        echo CHtml::image(
            '/images/fb-share-button.png', 
            'Share on Facebook',
            $shareButtonAttrs
        );
        //echo '</div>';
    }
        
    protected function renderShareConfirmed()
    {
        // Track action
        Yii::import('application.modules.analytics.*');
        Analytics::track(Analytics::ANALYTICS_PAGE, PageAnalytics::ACTION_SHARE_GATE, $this->rendererParams['tabId']);
        $this->renderDownloadButton('share');
    }
    /* END SHARE GATE */
    
    
    /* EMAIL GATE */
    protected function renderEmailGate()
    {
        $this->render('emailGatePrompt');
    }
    
    protected function renderEmailConfirmed()
    {
        /*echo '<div class="gate-email-confirmed">';
            echo '<div class="gate-email-confirmed-bar">ALL SET!</div>';
            echo '<div class="gate-email-confirmed-text">';
            echo $this->rendererParams['gatesConfig']['email']['confirmText'];
            echo '</div>';
        echo '</div>';*/
        $this->render('emailGateConfirm');
        
        
    }
    /* END EMAIL GATE */
    
    /* CUSTOM CODE GATE */
    protected function renderCustomCodeGate()
    {
        $this->render('customCodeGatePrompt', array(
            'iframeSrc' => SubdomainHelper::generateDomainForCustomCodeGate($this->rendererParams['tabId']) . '/template/gates/rendercustomcode'
        ));
    }
    /* END CUSTOM CODE GATE */
    
    protected function renderDownloadButton($gate)
    {
        $gateConfig = $this->rendererParams['gatesConfig'][$gate];
        $this->render('downloadConfirm', array(
            'buttonImg' => isset($gateConfig['downloadImgUrl']) ? $gateConfig['downloadImgUrl'] : '/images/download-button.png',
            'buttonUrl' => isset($gateConfig['downloadUrl']) ? $gateConfig['downloadUrl'] : '#',
            'confirmedBeforeCookieName' => $this->rendererParams['tabId'] . 'cnfrmd'
        ));
    }
    
    public function renderConfirmed()
    {
        //Render appropiate gate confirmed view with priority as follows:
        //email, share, like, customCode
        
        if ($this->emailGateEnabled()) {
            $this->renderEmailConfirmed();
            return;
        }
        
        if ($this->shareGateEnabled()) {
            $this->renderShareConfirmed();
            return;
        }
        
        /* LIKE GATES ARE DISABLED NOW
         * if ($this->likeGateEnabled()) {
            $this->renderLikeConfirmed();
            return;
        }*/
    }
    
    private static $_gatesRendered = false;
    protected function renderGates()
    {
        if ($this->adminMode) {
            return;
        }
        
        if (self::$_gatesRendered) {
            return;
        }
        self::$_gatesRendered = true;
        
        $this->_initCssAndScriptsForGates();
        
        //Render appropiate gate where priority is as follows: like, share, email 
        /* LIKE GATES ARE DISABLED NOW
         * if ($this->likeGateEnabled() && !$this->pageLiked()) {
            $this->renderLikeGate();
            return;
        }*/
        
        if ($this->shareGateEnabled() && !$this->pageShared()) {
            $this->renderShareGate();
            return;
        }
        
        if ($this->emailGateEnabled() && !$this->emailSubscribed()) {
            $this->renderEmailGate();
            return;
        }
        
        if ($this->customCodeGateEnabled()) {
            setcookie($this->_getCustomCodeCookieName(), null, -1); //Unset cookie immediately after first popup display
            //There might be also e-mail gate cookie as it could be previous gate in gate chain
            if (isset($_COOKIE[$this->_getEmailCookieName()])) {
                //so we hide it because we don't want to show email confirmation dialog
                setcookie($this->_getEmailCookieName(), null, -1);
            }
            
            $this->renderCustomCodeGate();
            return;
        }
        
        //Render confirmed view
        $this->renderConfirmed();
        
        
    }
        
    /* LIKE GATES ARE DISABLED NOW
     * protected function likeGateEnabled()
    {
        return $this->_gateEnabled('like');
    }*/
    
    protected function shareGateEnabled()
    {
        return $this->_gateEnabled('share');
    }
    
    protected function emailGateEnabled()
    {
        return $this->_gateEnabled('email');
    }
    
    protected function customCodeGateEnabled()
    {
        return $this->_gateEnabled('customCode');
    }
    
    private static $_scriptsInitializedAlready = false;
    private function _initCssAndScriptsForGates()
    {
        if (!self::$_scriptsInitializedAlready) {
            $this->render('generatedJS', array(
                'shareCookieName' => $this->_getShareCookieName(),
                'emailCookieName' => $this->_getEmailCookieName(),
                'customCodeCookieName' => $this->_getCustomCodeCookieName()
            ));
            self::$_scriptsInitializedAlready = true;
        }
    }
    
    
    /*protected function pageLiked()
    {
        if ($this->adminMode) {
            return false;
        }
        
        if (!isset($this->rendererParams['facebook']['page']['liked'])) {
            return false;
        }
        
        return $this->rendererParams['facebook']['page']['liked'];
        
    }*/
    
    protected function pageShared()
    {
        if ($this->adminMode) {
            return false;
        }
        
        return isset($_COOKIE[$this->_getShareCookieName()]);
        
    }
    
    protected function emailSubscribed()
    {
        if ($this->adminMode) {
            return true;
        }
        
        return isset($_COOKIE[$this->_getEmailCookieName()]);
    }
    
    protected function customCodeVisible()
    {
        if ($this->adminMode) {
            return false;
        }
        
        return isset($_COOKIE[$this->_getCustomCodeCookieName()]);
    }
    
    private function _getShareCookieName()
    {
        return md5("{$this->rendererParams['tabId']}shr");
    }
    
    private function _getEmailCookieName()
    {
        return md5("{$this->rendererParams['tabId']}eml");
    }
    
    private function _getCustomCodeCookieName()
    {
        return md5("{$this->rendererParams['tabId']}cod");
    }
    
    private function _gateEnabled($gateName)
    {
        if (!key_exists('gatesConfig', $this->rendererParams)) {
            return false;
        }
        
        return isset($this->rendererParams['gatesConfig'][$gateName]['enabled']) && $this->rendererParams['gatesConfig'][$gateName]['enabled'];
    }
    
    protected function getMostImportantConfirmText()
    {
        $gatesPriority = array('email', 'share'/*, 'like'*/);
        
        foreach ($gatesPriority as $gate) {
            if (
                isset($this->rendererParams['gatesConfig'][$gate]) &&
                $this->rendererParams['gatesConfig'][$gate]['enabled'] &&
                isset($this->rendererParams['gatesConfig'][$gate]['confirmText'])
            ) {
                return $this->rendererParams['gatesConfig'][$gate]['confirmText'];
            }
        }
        
        return '';
    }
    
    protected function atLeastOneGateEnabled()
    {
        foreach (array(/*'like', */'share', 'email', 'customCode') as $gateName) {
            if ($this->_gateEnabled($gateName)) {
                return true;
            }
        }
        return false;
    }
}
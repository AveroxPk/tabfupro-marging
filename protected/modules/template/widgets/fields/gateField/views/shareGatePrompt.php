<div class="gate-popup-overlay"<?php echo (/*($this->likeGateEnabled() && $this->pageLiked()) || */
    ($this->emailGateEnabled() && $this->emailSubscribed())) && 
    !$this->pageShared() ? '' : ' style="display:none"' ?>>
    <div class="gate-popup gate-popup-short">
        <div class="gate-popup-close"></div>
        <div class="gate-popup-short-prompt-message-wrapper">
            <div class="gate-popup-short-prompt-message-body">
                <?=$this->rendererParams['gatesConfig']['share']['promptText']?>
            </div>
        </div>
        <?php
            $shareButtonAttrs = array(
                'onclick' => 'shareButtonClick()',
                'class' => 'fb-share-button'
            );

            //Disable "Share" button clicking in admin mode
            if ($this->adminMode) {
                unset($shareButtonAttrs['onclick']);
            }

            echo CHtml::image(
                '/images/fb-share-button.png', 
                'Share on Facebook',
                $shareButtonAttrs
            );
        ?>
    </div>
</div>
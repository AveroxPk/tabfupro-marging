<div class="gate-popup-overlay"<?php echo (/*($this->likeGateEnabled() && $this->pageLiked()) || */
    ($this->shareGateEnabled() && $this->pageShared())) && 
    !$this->emailSubscribed() ? '' : ' style="display:none"' ?>>
    <div class="gate-popup email-gate">
        <div class="gate-popup-header-bar">
            <?=$this->rendererParams['gatesConfig']['email']['promptText']?>
        </div>
        <div class="gate-popup-close"></div>
        
        <input id="gates-popup-email" type="text" placeholder="<?= isset($this->rendererParams['gatesConfig']['email']['inputPlaceholder']) ? $this->rendererParams['gatesConfig']['email']['inputPlaceholder'] : 'your email here...' ?>" /><br/>
        <a href="#" class="cta" onclick="return subscribeEmail($('#gates-popup-email').val())"><img src="<?php echo isset($this->rendererParams['gatesConfig']['email']['promptButtonImgUrl']) && strlen($this->rendererParams['gatesConfig']['email']['promptButtonImgUrl']) ? $this->rendererParams['gatesConfig']['email']['promptButtonImgUrl'] : '/images/gates/popups/get-access-now-button.png' ?>" /></a>
        <p class="privacy-info"><?= isset($this->rendererParams['gatesConfig']['email']['privacyText']) ? $this->rendererParams['gatesConfig']['email']['privacyText'] : 'Your Privacy is protected' ?></p>
    </div>
</div>
<div class="gate-popup-overlay">
    <div class="gate-popup email-gate">
        <div class="gate-popup-header-bar">
            <?=$this->rendererParams['gatesConfig']['email']['confirmTitle']?>
        </div>
        <div class="gate-popup-close"></div>

        <div class="email-gate-confirm-message-wrapper">
            <div class="email-gate-confirm-message-body">
                <?=$this->rendererParams['gatesConfig']['email']['confirmText']?>
            </div>
        </div>
            
    </div>
</div>
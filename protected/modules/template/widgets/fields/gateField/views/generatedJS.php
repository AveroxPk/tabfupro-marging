<?php if (!$this->adminMode && isset($this->rendererParams['appId']) && isset($this->rendererParams['facebook'])): ?>
    <script src="https://connect.facebook.net/en_US/all.js#appId=<?=$this->rendererParams['appId']?>&amp;xfbml=1"></script>
    <script>
    FB.init({
        appId:"<?=$this->rendererParams['appId']?>",
        xfbml:true,
        status:true,
        cookie:true
    });
    FB.Event.subscribe('edge.create', function(response) {
        refreshPage();
    });
    </script>
<?php endif; ?>

<script>
    var thisPageUrl = "<?= Yii::app()->request->getBaseUrl(true) ?>/facebook/showtab/mobile/tabid/<?= $this->rendererParams['tabId'] ?>";
    var fbPageUrl = "https://www.facebook.com/pages/-/<?= $this->rendererParams['facebook']['page']['id'] ?>?sk=app_<?= $this->rendererParams['appId'] ?>";
    var thisPageName = <?= json_encode(isset($this->rendererParams['tabName']) ? $this->rendererParams['tabName'] : '') ?>;
    function refreshPage()
    {
        top.location.href = thisPageUrl;
    }

    function shareButtonClick() 
    {
        if (jQuery.browser.mobile) {
            document.cookie = <?=json_encode($shareCookieName . '=1')?>;
            <?php if ($this->customCodeGateEnabled()): ?>
            document.cookie = <?=json_encode($customCodeCookieName . '=1')?>;
            <?php endif; ?>
            top.location.href = 'https://www.facebook.com/dialog/share?' +
                'app_id=<?= $this->rendererParams['appId'] ?>' +
                '&display=touch' +
                '&href=' + encodeURIComponent(thisPageUrl + '?noredir=1') +
                '&redirect_uri=' + encodeURIComponent(thisPageUrl);
        }
        else {
            FB.ui(
                {
                    method: 'share',
                    href: thisPageUrl
                },
                function(response) {
                    if (response && !response.error_code) {
                        document.cookie = <?=json_encode($shareCookieName . '=1')?>; //response.object_id;
                        <?php if ($this->customCodeGateEnabled()): ?>
                        document.cookie = <?=json_encode($customCodeCookieName . '=1')?>;
                        <?php endif; ?>
                        refreshPage();
                    }
                }
            );
        }
    }

    function subscribeEmail(email, name, lastName)
    {
        var postData,
            fromForm = (typeof email === 'object' && typeof email.nodeName !== 'undefined')
        ;

        if (fromForm) {
            var $form = $(email).parents('form'),
                $name = $form.find("[name='name']"),
                $lastName = $form.find("[name='lastName']"),
                $email = $form.find("[name='email']")
            ;
			

			
            if ($name.length > 0) {
                name = $name.val();
            }

            if ($lastName.length > 0) {
                lastName = $lastName.val();
            }

            email = $email.val();
        }

        postData = {
            "email": email
        };

        if (typeof name !== 'undefined') {
            postData.name = name;
        }

        if (typeof lastName !== 'undefined') {
            postData.lastName = lastName;
        }
        $.ajax({
            "url": '<?= Yii::app()->request->getBaseUrl(true) ?>/template/gates/email/tabId/<?=$this->rendererParams['tabId']?>',
            "data": postData,
            "type": 'post',
            "dataType": 'json'
        }).done(function(data) {
            if (data.error) {
                alert(data.message);
                return;
            }
            document.cookie = <?=json_encode($emailCookieName . '=1')?>;
            <?php if ($this->customCodeGateEnabled()): ?>
            document.cookie = <?=json_encode($customCodeCookieName . '=1')?>;
            <?php endif; ?>
            refreshPage();
            
            
        }).fail(function() {
            alert('Some error occured. Please try again.');
        });

        return false;
    }
	
	
    function customForm(email, name, lastName)
    {
        var postData,
            fromForm = (typeof email === 'object' && typeof email.nodeName !== 'undefined')
        ;

        if (fromForm) {
            var $form = $(email).parents('form'),
                $name = $form.find("[name='name']"),
                $lastName = $form.find("[name='lastName']"),
                $email = $form.find("[name='email']"),
                $address = $form.find("[name='address']"),
                $city = $form.find("[name='city']"),
                $state = $form.find("[name='state']"),
                $zip = $form.find("[name='zip']"),
                $phone = $form.find("[name='phone']"),
				
                $checkin = $form.find("[name='checkin']"),
                $checkout = $form.find("[name='checkout']"),
                $adults = $form.find("[name='adults']"),
                $child = $form.find("[name='child']"),
				
                $gender = $form.find("[name='gender']:checked"),
                $racelength = $form.find("[name='racelength']:checked"),
                $shirtSize = $form.find("[name='shirtSize']"),
                $dateOfBirth = $form.find("[name='dateOfBirth']"),
				
                $impactLevel = $form.find("[name='impactLevel']"),
                $bugNoticedAt = $form.find("[name='bugNoticedAt']"),
                $externalUrl = $form.find("[name='url']"),
				
                $type = $form.find("[name='type']"),
                $year = $form.find("[name='year']"),
                $make = $form.find("[name='make']"),
				
                $participent = $form.find("[name='participent']"),
                $position = $form.find("[name='position']"),
                $department = $form.find("[name='department']"),
				
                $contactMethod = $form.find("[name='contactMethod']"),
                $datetime = $form.find("[name='datetime']"),
                $dropdownOption = $form.find("[name='dropdownOption']"),
				
                $description = $form.find("[name='description']")
            ;
			

			
            if ($name.length > 0) {
                name = $name.val();
            }

            if ($lastName.length > 0) {
                lastName = $lastName.val();
            }

            if ($address.length > 0) {
                address = $address.val();
            }
			
            if ($city.length > 0) {
                city = $city.val();
            }
			
            if ($state.length > 0) {
                state = $state.val();
            }
			
            if ($zip.length > 0) {
                zip = $zip.val();
            }
			
            if ($phone.length > 0) {
                phone = $phone.val();
            }
			
            if ($checkin.length > 0) {
                checkin = $checkin.val();
            }
			
            if ($checkout.length > 0) {
                checkout = $checkout.val();
            }
			
            if ($adults.length > 0) {
                adults = $adults.val();
            }
			
            if ($child.length > 0) {
                child = $child.val();
            }
			
            if ($gender.length > 0) {
                gender = $gender.val();
            }
			
            if($dateOfBirth.length > 0) {
				dateOfBirth = $dateOfBirth.val();
			}
            if($racelength.length > 0) {
				racelength = $racelength.val();
			}
            if($shirtSize.length > 0) {
				shirtSize = $shirtSize.val();
			}
									
            if($type.length > 0) {
				type = $type.val();
			}
            if($year.length > 0) {
				year = $year.val();
			}
            if($make.length > 0) {
				make = $make.val();
			}								
									
            if($participent.length > 0) {
				participent = $participent.val();
			}
            if($position.length > 0) {
				position = $position.val();
			}
            if($department.length > 0) {
				department = $department.val();
			}								
									
            if ($datetime.length > 0) {
                datetime = $datetime.val();
            }
            if ($contactMethod.length > 0) {
                contactMethod = $contactMethod.val();
            }
            if ($dropdownOption.length > 0) {
                dropdownOption = $dropdownOption.val();
            }
			
            if ($description.length > 0) {
                description = $description.val();
            }

            email = $email.val();
        }
        postData = {
        };

        if (typeof name !== 'undefined') {
            postData.name = name;
			if(typeof $name.attr('required') !== 'undefined' && name.length < 1)
			{
				postData.nameRequired = 1;
			}
        }

        if (typeof lastName !== 'undefined') {
            postData.lastName = lastName;
        }
		
        if (typeof email !== 'undefined') {
            postData.email = email;
        }
		
        if (typeof address !== 'undefined') {
            postData.address = address;
			
			if(typeof $address.attr('required') !== 'undefined' && address.length < 1)
			{
				postData.addressRequired = 1;
			}
        }
		
        if (typeof city !== 'undefined') {
            postData.city = city;
			
			if(typeof $city.attr('required') !== 'undefined' && city.length < 1)
			{
				postData.cityRequired = 1;
			}
        }
		
        if (typeof state !== 'undefined') {
            postData.state = state;
			
			if(typeof $state.attr('required') !== 'undefined' && state.length < 1)
			{
				postData.stateRequired = 1;
			}
        }
		
        if (typeof zip !== 'undefined') {
            postData.zip = zip;
			
			if(typeof $zip.attr('required') !== 'undefined' && zip.length < 1)
			{
				postData.zipRequired = 1;
			}
        }
		
        if (typeof phone !== 'undefined') {
            postData.phone = phone;
			
			if(typeof $phone.attr('required') !== 'undefined' && phone.length < 1)
			{
				postData.phoneRequired = 1;
			}
			
        }
		
        if (typeof checkin !== 'undefined') {
            postData.checkin = checkin;
			
			if(typeof $checkin.attr('required') !== 'undefined' && checkin.length < 1)
			{
				postData.checkinRequired = 1;
			}
        }
		
        if (typeof checkout !== 'undefined') {
            postData.checkout = checkout;

			if(typeof $checkout.attr('required') !== 'undefined' && checkout.length < 1)
			{
				postData.checkoutRequired = 1;
			}
        }

        if (typeof adults !== 'undefined') {
            postData.adults = adults;
			if(typeof $adults.attr('required') !== 'undefined' && adults.length < 1)
			{			
				postData.adultsRequired = 1;
			}

        }

        if (typeof child !== 'undefined') {
            postData.child = child;
			
			if(typeof $child.attr('required') !== 'undefined' && child.length < 1)
			{
				postData.childRequired = 1;
			}
        }
		
        if (typeof gender !== 'undefined') {
            postData.gender = gender;			
        }
		
		if (typeof dateOfBirth !== 'undefined'){
			postData.dateOfBirth = dateOfBirth;
		}
		if (typeof racelength !== 'undefined'){
			postData.racelength = racelength;
		}
		if (typeof shirtSize !== 'undefined'){
			postData.shirtSize = shirtSize;
		}

		if (typeof impactLevel !== 'undefined'){
			postData.impactLevel = impactLevel;
		}
		if (typeof bugNoticedAt !== 'undefined'){
			postData.bugNoticedAt = bugNoticedAt;
		}
		if (typeof externalUrl !== 'undefined'){
			postData.externalUrl = externalUrl;
		}
		
		if (typeof type !== 'undefined'){
			postData.type = type;
		}
		if (typeof year !== 'undefined'){
			postData.year = year;
		}
		if (typeof make !== 'undefined'){
			postData.make = make;
		}
		

		if (typeof participent !== 'undefined'){
			postData.participent = participent;
		}
		if (typeof position !== 'undefined'){
			postData.position = position;
		}
		if (typeof department !== 'undefined'){
			postData.department = department;
		}
		
		if (typeof contactMethod !== 'undefined'){
			postData.contactMethod = contactMethod;
		}
		if (typeof datetime !== 'undefined'){
			postData.datetime = datetime;
		}
		if (typeof dropdownOption !== 'undefined'){
			postData.dropdownOption = dropdownOption;
		}
		
        if (typeof description !== 'undefined') {
            postData.description = description;
        }
	
        $.ajax({
            "url": '<?= Yii::app()->request->getBaseUrl(true) ?>/template/gates/custom/tabId/<?=$this->rendererParams['tabId']?>',
            "data": postData,
            "type": 'post',
            "dataType": 'json'
        }).done(function(data) {
            if (data.error) {
                alert(data.message);
                return;
            }
            document.cookie = <?=json_encode($emailCookieName . '=1')?>;
            <?php if ($this->customCodeGateEnabled()): ?>
            document.cookie = <?=json_encode($customCodeCookieName . '=1')?>;
            <?php endif; ?>
            refreshPage();
            
            
        }).fail(function() {
            alert('Some error occured. Please try again.');
        });

        return false;
    }	

/*
    function racingTemplate(email, name, lastName)
    {
        var postData,
            fromForm = (typeof email === 'object' && typeof email.nodeName !== 'undefined')
        ;

        if (fromForm) {
            var $form = $(email).parents('form'),
                $name = $form.find("[name='name']"),
                $lastName = $form.find("[name='lastName']"),
                $email = $form.find("[name='email']"),
                $address = $form.find("[name='address']"),
                $city = $form.find("[name='city']"),
                $state = $form.find("[name='state']"),
                $zip = $form.find("[name='zip']"),
                $phone = $form.find("[name='phone']"),
                $gender = $form.find("[name='gender']:checked"),
                $racelength = $form.find("[name='racelength']:checked"),
                $shirtSize = $form.find("[name='shirtSize']"),
                $dateOfBirth = $form.find("[name='dateOfBirth']")
 //               $description = $form.find("[name='description']")
            ;
			

			
            if ($name.length > 0) {
                name = $name.val();
            }

            if ($lastName.length > 0) {
                lastName = $lastName.val();
            }

            if ($address.length > 0) {
                address = $address.val();
            }
			
            if ($city.length > 0) {
                city = $city.val();
            }
			
            if ($state.length > 0) {
                state = $state.val();
            }
			
            if ($zip.length > 0) {
                zip = $zip.val();
            }
			
            if ($phone.length > 0) {
                phone = $phone.val();
            }
			
            if ($gender.length > 0) {
                gender = $gender.val();
            }
			
            if($dateOfBirth.length > 0) {
				dateOfBirth = $dateOfBirth.val();
			}
            if($racelength.length > 0) {
				racelength = $racelength.val();
			}
            if($shirtSize.length > 0) {
				shirtSize = $shirtSize.val();
			}
			
            email = $email.val();
        }
        postData = {
        };

        if (typeof name !== 'undefined') {
            postData.name = name;
			if(typeof $name.attr('required') !== 'undefined' && name.length < 1)
			{
				postData.nameRequired = 1;
			}
        }

        if (typeof lastName !== 'undefined') {
            postData.lastName = lastName;
        }
		
        if (typeof email !== 'undefined') {
            postData.email = email;
        }
		
        if (typeof address !== 'undefined') {
            postData.address = address;
			
			if(typeof $address.attr('required') !== 'undefined' && address.length < 1)
			{
				postData.addressRequired = 1;
			}
        }
		
        if (typeof city !== 'undefined') {
            postData.city = city;
			
			if(typeof $city.attr('required') !== 'undefined' && city.length < 1)
			{
				postData.cityRequired = 1;
			}
        }
		
        if (typeof state !== 'undefined') {
            postData.state = state;
			
			if(typeof $state.attr('required') !== 'undefined' && state.length < 1)
			{
				postData.stateRequired = 1;
			}
        }
		
        if (typeof zip !== 'undefined') {
            postData.zip = zip;
			
			if(typeof $zip.attr('required') !== 'undefined' && zip.length < 1)
			{
				postData.zipRequired = 1;
			}
        }
		
        if (typeof phone !== 'undefined') {
            postData.phone = phone;
			
			if(typeof $phone.attr('required') !== 'undefined' && phone.length < 1)
			{
				postData.phoneRequired = 1;
			}
			
        }
		
        if (typeof gender !== 'undefined') {
            postData.gender = gender;			
        }
		
		if (typeof dateOfBirth !== 'undefined'){
			postData.dateOfBirth = dateOfBirth;
		}
		if (typeof racelength !== 'undefined'){
			postData.racelength = racelength;
		}
		if (typeof shirtSize !== 'undefined'){
			postData.shirtSize = shirtSize;
		}
        
	
        $.ajax({
            "url": '<?= Yii::app()->request->getBaseUrl(true) ?>/template/gates/custom/tabId/<?=$this->rendererParams['tabId']?>',
            "data": postData,
            "type": 'post',
            "dataType": 'json'
        }).done(function(data) {
            if (data.error) {
                alert(data.message);
                return;
            }
            document.cookie = <?=json_encode($emailCookieName . '=1')?>;
            <?php if ($this->customCodeGateEnabled()): ?>
            document.cookie = <?=json_encode($customCodeCookieName . '=1')?>;
            <?php endif; ?>
            refreshPage();
            
            
        }).fail(function() {
            alert('Some error occured. Please try again.');
        });

        return false;
    }	
	*/
</script>
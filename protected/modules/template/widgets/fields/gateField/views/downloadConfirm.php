<script type="text/javascript">
<?php if (!isset($_COOKIE[$confirmedBeforeCookieName])): //Show popup automatically only once ?>
    document.cookie = <?=json_encode("$confirmedBeforeCookieName=1")?>;
    <?php if (!empty($buttonUrl) && $buttonUrl != '#'): //Redirect to download URL automatically after 3 secs ?>
    setTimeout(function() {
        document.location = <?=json_encode($buttonUrl)?>;
    }, 3000);
    <?php endif; ?>
<?php endif; ?>
</script>
<div class="gate-popup-overlay"<?php echo // show confirm immediately if all gates are done
    (/*$this->likeGateEnabled() && $this->pageLiked() && */!$this->shareGateEnabled() && !$this->emailGateEnabled()) ||
    ($this->shareGateEnabled() && $this->pageShared() && /*!$this->likeGateEnabled() && */!$this->emailGateEnabled()) /*||
    ($this->likeGateEnabled() && $this->pageLiked() && $this->shareGateEnabled() && $this->pageShared() && !$this->emailGateEnabled())*/
        ? '' : ' style="display:none"' ?>>
    <div class="gate-popup gate-popup-short">
        <div class="gate-popup-close"></div>
        <div class="gate-popup-short-prompt-message-wrapper">
            <div class="gate-popup-short-prompt-message-body">
                <?=$this->getMostImportantConfirmText()?>
            </div>
        </div>
        <?php
            echo CHtml::link(
                CHtml::image(
                    $buttonImg, 
                    'Download',
                    array(
                        'class' => 'gate-download-button'
                    )
                ),
                $buttonUrl
            );
        ?>
    </div>
</div>
<div class="gate-popup-overlay"<?php echo $this->emailGateEnabled() && $this->emailSubscribed()/* && !$this->pageLiked()*/ ? '' : ' style="display:none"' ?>>
    <div class="gate-popup gate-popup-short">
        <div class="gate-popup-close"></div>
        <div class="gate-popup-short-prompt-message-wrapper">
            <div class="gate-popup-short-prompt-message-body">
                <?=$this->rendererParams['gatesConfig']['like']['promptText']?>
            </div>
        </div>
        <?php
            if ($this->adminMode || !isset($this->rendererParams['appId']) || !isset($this->rendererParams['facebook'])) {
                //echo '<div style="float:right; margin-top: -33px;">';
                    echo '<img src="/images/tabwizard/fb_like_button.png" class="admin-like-img"/>';
                //echo '</div>';
            } else {
                ?>
                    <div style="float:right; margin-top: -40px;">
                        <fb:like href="<?=$this->rendererParams['fanpageUrl']?>" send="false" layout="button" width="90" show_faces="false" font="arial"></fb:like>
                    </div>
                <?php
            }
        ?>
    </div>
</div>
<?php
$width = isset($this->rendererParams['gatesConfig']['customCode']['modalContentWidth']) ? $this->rendererParams['gatesConfig']['customCode']['modalContentWidth'] : GateField::CUSTOM_CODE_GATE_DEFAULT_CONTENT_WIDTH;
$height = $this->rendererParams['gatesConfig']['customCode']['modalContentHeight'];
?><div id="gate-popup-overlay-custom-code" class="gate-popup-overlay"<?= 
    ($this->shareGateEnabled() && $this->pageShared() || !$this->shareGateEnabled()) &&
    ($this->emailGateEnabled() && $this->emailSubscribed() || !$this->emailGateEnabled()) &&
    $this->customCodeVisible() ? '' : ' style="display:none"' ?>>
    <div class="gate-popup email-gate" style="width: <?= $width ?>px;margin-left: -<?= (int)($width / 2) ?>px">
        <div class="gate-popup-header-bar" style="margin-bottom:5px">
            <?=$this->rendererParams['gatesConfig']['customCode']['promptTitle']?>
        </div>
        <div class="gate-popup-close"></div>
        
        <iframe src="<?=$iframeSrc?>" style="margin:0 auto" width="<?= $width ?>" height="<?= $height ?>"></iframe>
    </div>
</div>
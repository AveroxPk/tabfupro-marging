<?php

class CountdownTimerField extends TemplateFieldWidget
{
    public $endTime;
    public $timezone;
    public $selector = '.information .counter';
    public $daysTitle;
    public $hoursTitle;
    public $minutesTitle;
    public $secondsTitle;
    
    
    public function renderMe() 
    {
        //Yii::app()->clientScript->registerScriptFile('resources/js/jquery.countdown.js');
                
        if (!$this->endTime) {
            $this->endTime = date('Y-m-d H:i:s', strtotime('+1 day'));
        }
        if (!$this->timezone) {
            $this->timezone = 'America/New_York';
        }
        // convert to name if needed
        if (strpos($this->timezone, '/') === false) {
            $tzParts = explode(':', $this->timezone);
            $offset = ((int)$tzParts[0]) * 3600;
            if ((int)$tzParts[1] > 0) {
                $offset += $offset < 0 ? -(int)$tzParts[1] : (int)$tzParts[1];
            }
            $this->timezone = timezone_name_from_abbr('', $offset, 0);
        }
        
        $dateTime = new DateTime('now', new DateTimeZone($this->timezone));
        $theTimezone = $dateTime->format('P');
        
        $theDate = date('Y-m-d\TH:i:s', strtotime($this->endTime));
        echo '<script type="text/javascript">';
        echo '$(function () {
            var endTime = "'.$theDate.$theTimezone.'",
                // when updating field in the admin panel we need to dig into the iframe
                $counterElement = $("'.$this->selector.'").length > 0 ? $("'.$this->selector.'") : $("#page-preview").contents().find("'.$this->selector.'");

            if (window.globalCountdown != null) {
                window.globalCountdown.stop();
            }

            $counterElement.countdown({
                date: endTime,
                render: function(date) {
                    return $(this.el).html(
                        "<div class=\"element\">" +
                            "<div class=\"number\">" + date.days + "</div><div class=\"desc\">'.($this->daysTitle !== null ? $this->daysTitle : 'days').'</div>" +
                        "</div>" +
                        "<div class=\"element\">" +
                            "<div class=\"number\">" + this.leadingZeros(date.hours) + "</div><div class=\"desc\">'.($this->hoursTitle !== null ? $this->hoursTitle : 'hours').'</div>" +
                        "</div>" +
                        "<div class=\"element\">" +
                            "<div class=\"number\">" + this.leadingZeros(date.min) + "</div><div class=\"desc\">'.($this->minutesTitle !== null ? $this->minutesTitle : 'minutes').'</div>" +
                        "</div>" +
                        "<div class=\"element\">" +
                            "<div class=\"number\">" + this.leadingZeros(date.sec) + "</div><div class=\"desc\">'.($this->secondsTitle !== null ? $this->secondsTitle : 'seconds').'</div>" +
                        "</div>"
                    );
                }
            });
            '.($this->adminMode ? '// stop the timer when in edit mode
            $counterElement.data("countdown").stop();' : '').'
            
            window.globalCountdown = $counterElement.data("countdown");
        });';
        echo '</script>';
    }    
}
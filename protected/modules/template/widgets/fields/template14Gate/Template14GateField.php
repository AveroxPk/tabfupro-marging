<?php

class Template14GateField extends GateField
{
    public $label;
    
    
    public function renderMe() 
    {
        $this->renderGates();
        
        echo '<div class="input"><form>';
        echo '<input type="email" name="email" class="email-input" placeholder="Enter your email address">';
        echo CHtml::submitButton(strip_tags($this->label), array('onclick' => $this->adminMode ? 'return false' : 'return subscribeEmail(this)'));
        echo '</form></div>';
        
    }
}
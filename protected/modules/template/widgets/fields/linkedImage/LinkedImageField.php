<?php
Yii::import('application.modules.template.widgets.fields.image.ImageField');
class LinkedImageField extends ImageField
{
    /**
     * @var string
     */
    public $url;

    public function renderMe() 
    {
        if ($this->url) {
            echo CHtml::link($this->_renderImage(true), $this->url);
            return;
        }
        
        $this->_renderImage();
        
    }
    
    public function renderMeForAdmin() 
    {
        $this->_renderImage();
    }
    
    private function _renderImage($return = false)
    {
        if ($return) {
            ob_start();
        }
        
        parent::renderMe();
            
        if ($return) {
            return ob_get_clean();
        }
    }

}
<?php

class Template17GateField extends GateField
{
    public $submitButtonLabel;
    public $orientationHorizontal;
    public $nameVisible;
	public $nameRequired;
    public $namePlaceholder;
	public $emailRequired;
	public $emailVisible;
    public $emailPlaceholder;
	public $addressRequired;
	public $addressVisible;
    public $addressPlaceholder;
	public $cityRequired;
	public $cityVisible;
    public $cityPlaceholder;
	public $stateRequired;
	public $stateVisible;
    public $statePlaceholder;
	public $zipRequired;
	public $zipVisible;
    public $zipPlaceholder;
	public $phoneRequired;
	public $phoneVisible;
    public $phonePlaceholder;
	
	public $checkinVisible;
	public $checkinRequired;
	public $checkinPlaceholder;
	public $checkoutVisible;
	public $checkoutRequired;
	public $checkoutPlaceholder;
	public $adultsVisible;
	public $adultsRequired;
	public $adultsPlaceholder;
	public $childVisible;
	public $childRequired;
	public $childPlaceholder;
	public $specialInstructionVisible;
	public $specialInstructionPlaceholder;
    
    
    public function renderMe()
    {
        $this->renderGates();
			
		if($this->nameRequired == 1 && $this->nameVisible == 1)
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
				'required' => 'required'
			));
			
		else 
			$input1Html = CHtml::textField('name', '', array(
				'placeholder' => $this->namePlaceholder !== null ? $this->namePlaceholder : 'your name',
				'class' => 'form-fields',
			));
			
		if($this->emailRequired == 1 && $this->emailVisible == 1)
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input2Html = CHtml::emailField('email', '', array(
				'placeholder' => $this->emailPlaceholder !== null ? $this->emailPlaceholder : 'your email',
				'class' => 'form-fields'
			));
			
		if($this->addressRequired == 1 && $this->addressVisible == 1)
			$input3Html = CHtml::textField('address', '', array(
				'placeholder' => $this->addressPlaceholder !== null ? $this->addressPlaceholder : 'your address',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input3Html = CHtml::textField('address', '', array(
				'placeholder' => $this->addressPlaceholder !== null ? $this->addressPlaceholder : 'your address',
				'class' => 'form-fields',
			));
			
		if($this->cityRequired == 1 && $this->cityVisible == 1)
			$input4Html = CHtml::textField('city', '', array(
				'placeholder' => $this->cityPlaceholder !== null ? $this->cityPlaceholder : 'your city',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input4Html = CHtml::textField('city', '', array(
				'placeholder' => $this->cityPlaceholder !== null ? $this->cityPlaceholder : 'your city',
				'class' => 'form-fields',
			));
        
			
		if($this->stateRequired == 1 && $this->stateVisible == 1)
			$input5Html = CHtml::textField('state', '', array(
				'placeholder' => $this->statePlaceholder !== null ? $this->statePlaceholder : 'your state',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input5Html = CHtml::textField('state', '', array(
				'placeholder' => $this->statePlaceholder !== null ? $this->statePlaceholder : 'your state',
				'class' => 'form-fields',
			));
			
		if($this->zipRequired == 1 && $this->zipVisible == 1)
			$input6Html = CHtml::textField('zip', '', array(
				'placeholder' => $this->zipPlaceholder !== null ? $this->zipPlaceholder : 'Zip code',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input6Html = CHtml::textField('zip', '', array(
				'placeholder' => $this->zipPlaceholder !== null ? $this->zipPlaceholder : 'Zip code',
				'class' => 'form-fields',
			));
			
		if($this->phoneRequired == 1 && $this->phoneVisible == 1)
			$input7Html = CHtml::textField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input7Html = CHtml::textField('phone', '', array(
				'placeholder' => $this->phonePlaceholder !== null ? $this->phonePlaceholder : 'Phone number',
				'class' => 'form-fields',
			));
			
		if($this->checkinRequired == 1 && $this->checkinVisible == 1)
			$input8Html = CHtml::textField('checkin', '', array(
				'placeholder' => $this->checkinPlaceholder !== null ? $this->checkinPlaceholder : 'Checkin Date & time',
				'class' => 'form-fields form_datetime',
				'required' => 'required'
			));			
		else
			$input8Html = CHtml::textField('checkin', '', array(
				'placeholder' => $this->checkinPlaceholder !== null ? $this->checkinPlaceholder : 'Checkin Date & time',
				'class' => 'form-fields form_datetime'
			));
			
		if($this->checkoutRequired == 1 && $this->checkoutVisible == 1)
			$input9Html = CHtml::textField('checkout', '', array(
				'placeholder' => $this->checkoutPlaceholder !== null ? $this->checkoutPlaceholder : 'Checkin Date & time',
				'class' => 'form-fields form_datetime',
				'required' => 'required'
			));			
		else
			$input9Html = CHtml::textField('checkout', '', array(
				'placeholder' => $this->checkoutPlaceholder !== null ? $this->checkoutPlaceholder : 'Checkin Date & time',
				'class' => 'form-fields form_datetime',
			));
			
		if($this->adultsRequired == 1 && $this->adultsVisible == 1)
			$input10Html = CHtml::textField('adults', '', array(
				'placeholder' => $this->adultsPlaceholder !== null ? $this->adultsPlaceholder : 'Adults',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input10Html = CHtml::textField('adults', '', array(
				'placeholder' => $this->adultsPlaceholder !== null ? $this->adultsPlaceholder : 'Adults',
				'class' => 'form-fields',
			));
			
		if($this->childRequired == 1 && $this->childVisible == 1)
			$input11Html = CHtml::textField('child', '', array(
				'placeholder' => $this->childPlaceholder !== null ? $this->childPlaceholder : 'Children',
				'class' => 'form-fields',
				'required' => 'required'
			));			
		else
			$input11Html = CHtml::textField('child', '', array(
				'placeholder' => $this->childPlaceholder !== null ? $this->childPlaceholder : 'Children',
				'class' => 'form-fields',
			));
			
			$input12Html = CHtml::textArea('description', '', array(
				'placeholder' => $this->specialInstructionPlaceholder !== null ? $this->specialInstructionPlaceholder : 'Children',
				'class' => 'form-fields text-area',
			));			
			
        /*$waitingForGatesOtherThanEmail = (
            ($this->likeGateEnabled() && !$this->pageLiked()) || 
            ($this->shareGateEnabled() && !$this->pageShared())
        );*/
        
        $submitButtonHtml = CHtml::submitButton($this->submitButtonLabel, array(
            'class' => 'submit-btn-submit',
            'style' => "font-family: 'Helvetica Neue'; font-size: 12px; color: #fff; font-weight: normal;",
            'onclick' => !$this->emailSubscribed() ? 'return hotelReserve(this)' : 'return showGatePopup()'
        ));
        
        echo '<form>';
        if ($this->orientationHorizontal) {
            echo "<div class=\"form-final-left\">" . ($this->nameVisible ? "$input1Html<br/>" : '') . "$input2Html</div><div class=\"form-final-right\">$submitButtonHtml</div>";
        } else {
            if ($this->nameVisible) {
                echo $input1Html;
            }
			if($this->emailVisible)
				echo $input2Html;
			if($this->addressVisible)
				echo $input3Html;
			if($this->cityVisible)
				echo $input4Html;
			if($this->stateVisible)
				echo $input5Html;
			if($this->zipVisible)
				echo $input6Html;
			if($this->phoneVisible)
				echo $input7Html;
			
			if($this->checkinVisible)
				echo $input8Html;
			if($this->checkoutVisible)
				echo $input9Html;
			if($this->adultsVisible)
				echo $input10Html;
			if($this->childVisible)
				echo $input11Html;
			if($this->specialInstructionVisible)
				echo $input12Html;
			
            echo $submitButtonHtml;
        }
        echo '</form>';
        
    }

}
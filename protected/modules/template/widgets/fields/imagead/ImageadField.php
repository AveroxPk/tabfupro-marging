<?php

class ImageadField extends TemplateFieldWidget
{
    /**
     * @var string
     */
    public $placeholderSrc;
    
    /**
     * @var Asset
     */
    public $imageAsset;
    
    /**
     * @var string
     */
    public $alt;
    
    private $_preferedWidth;
    private $_preferedHeight;
    
    public function renderMe() 
    {
        echo CHtml::image(
            $this->imageAsset ? $this->imageAsset->getUrl() : $this->placeholderSrc, 
            $this->alt, 
            array(
                'style' => "max-width: {$this->getPreferedWidth()}px; max-height: {$this->getPreferedHeight()}px"
            )
        );
    }
    
    public function setPreferedWidth($value)
    {
        $this->_preferedWidth = $value;
    }
    
    public function setPreferedHeight($value)
    {
        $this->_preferedHeight = $value;
    }
    
    public function getPreferedWidth()
    {
        if (empty($this->_preferedWidth)) {
            $this->_getImageSizeBasingOnPlaceholder();
        }
        
        return $this->_preferedWidth;
    }
    
    public function getPreferedHeight()
    {
        if (empty($this->_preferedHeight)) {
            $this->_getImageSizeBasingOnPlaceholder();
        }
        
        return $this->_preferedHeight;
    }
    
    private function _getPlaceholderAbsoluteUrl()
    {
        if (strpos($this->placeholderSrc, 'http') === 0) {
            return $this->placeholderSrc;
        }
        
        return Yii::app()->getBaseUrl(true) . '/' . $this->placeholderSrc;
    }
    
    private function _getImageSizeBasingOnPlaceholder()
    {
        if (($size = getimagesize($this->_getPlaceholderAbsoluteUrl()))) {
            list($this->_preferedWidth, $this->_preferedHeight) = $size;
        }
    }
    
    

}
<?php

class Template07GateField extends GateField
{
    public $label = '';
    
    public function renderMe() 
    {
        $this->renderGates();
        
        ?>
        <div class="button">
            <input type="submit" class="orange-button" value="<?=CHtml::encode($this->label)?>" onclick="<?=!$this->emailSubscribed() ? 'return subscribeEmail(this)' : 'return showGatePopup()' ?>">
        </div>
        <?php
        
    }
}
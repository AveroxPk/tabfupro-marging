<?php

class Template04GateField extends GateField
{
    public $label;
    public $url;
    
    public function renderMe()         
    {
        $this->renderGates();
        
        $buttonAttribs = [
            'class' => 'c-button'
        ];
        if ($this->atLeastOneGateEnabled() && !$this->adminMode) {
            $buttonAttribs['onclick'] = 'return showGatePopup()';
            $this->url = null;
        }
        $button = CHtml::tag('div', $buttonAttribs, CHtml::tag('span', array(), $this->label));
        
        echo !empty($this->url) ? CHtml::link($button, $this->url) : $button;
        
    }

}
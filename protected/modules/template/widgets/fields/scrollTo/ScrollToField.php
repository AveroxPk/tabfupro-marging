<?php

class ScrollToField extends TemplateFieldWidget
{
    /*
     * @boolean
     */
    public $active = 1;
    
    /*
     * @integer
     */
    public $y = 370;
    
    /**
     * Render in tab
     */
    public function renderMe()
    {
        if($this->active) {
            echo '<script>
                var tabTopValue = 0;
                $(window).load(function() {
                    setTimeout(function() {
                        FB.Canvas.getPageInfo(function(pageInfo) {
                            var totalHeightOfPage = Math.max(
                                (document.height) ? document.height : 0,
                                Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
                                Math.max(document.body.offsetHeight, document.documentElement.offsetHeight),
                                Math.max(document.body.clientHeight, document.documentElement.clientHeight)
                            );
                            if (pageInfo.clientHeight > totalHeightOfPage) {
                                FB.Canvas.setSize({ height: pageInfo.clientHeight });
                            }
                            var negativeOffset = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()) ? 40 : 25;
                            tabTopValue = parseInt(pageInfo.offsetTop) - negativeOffset;
                            FB.Canvas.scrollTo(0, tabTopValue);
                        });
                    }, 1000);
                });
            </script>';
        }
    }
    
    /**
     * Render in editor
     */
    public function renderMeForAdmin() 
    {
        $activeVal = $this->active ? 0 : 1;
        echo "<script>
            function handleAutoscroll() {
                if ($('.autoscroll-field').hasClass('autoscroll-enabled')) {
                    $('.autoscroll-field').removeClass('autoscroll-enabled').addClass('autoscroll-disabled');
                }
                else {
                    $('.autoscroll-field').removeClass('autoscroll-disabled').addClass('autoscroll-enabled');
                }
                $.post(
                    '/template/pageEditor/updateField/tabId/{$this->rendererParams['tabId']}/fieldId/{$this->rendererParams['fieldId']}',
                    { active: {$activeVal} }
                );
            }
        </script>";
        echo '<div class="autoscroll-field ' . ($this->active ? 'autoscroll-enabled' : 'autoscroll-disabled') . '" onclick="return handleAutoscroll();">';
            echo '<div class="autoscroll-switch"></div>';
        echo '</div>';
    }
    
}
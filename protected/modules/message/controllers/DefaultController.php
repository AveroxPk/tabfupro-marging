<?php

class DefaultController extends Controller
{
	public function actionShowMessage()
	{
            $return["content"] = "Error";
            $return["title"] = "Error";
            $return["date"] = "Error";
            if(isset($_POST) && !Yii::app()->user->isGuest){
                $id = $_POST["id"];
                $message = Message::model()->findById($id);
                $return = array();
                
                if($message != null){
                    $return["content"] = $message->content;
                    $return["title"] = $message->title;
                    $timezone = (date('Z')/3600)<0 ? (date('Z')/3600):"+".(date('Z')/3600);
                    $return["date"] = date('d/m/Y', strtotime($message->date)).' '.date('H:i', strtotime($message->date)).' UTC'.$timezone;

                    if(!UserMessage::model()->exists('message_id = :message_id AND user_user_id = :user_user_id', array(
                        ':message_id'=>$message->id,
                        ':user_user_id'=>Yii::app()->user->id
                    ))){
                        $userMessage = new UserMessage();
                        $userMessage->message_id = $message->id;
                        $userMessage->user_user_id = Yii::app()->user->id;
                        $userMessage->save();
                    }
                    
                }
                
            }
            echo json_encode($return);
		
	}
        
        public function actionArchiveMessage(){
            $return = array();
            $return["status"] = "error";
            if(isset($_POST["id"]) && !Yii::app()->user->isGuest){
                $message = UserMessage::model()->findByAttributes(array(
                    'message_id'=>$_POST["id"],
                    'user_user_id'=>Yii::app()->user->id
                ));
               
                
                $message->status = UserMessage::STATUS_ARCHIVED;
                if($message->save(false)){
                    $return["status"] = "ok";
                }
            }
            echo json_encode($return);
        }
        
        public function actionRemoveArchiveMessage(){
            $return = array();
            $return["status"] = "error";
            if(isset($_POST["id"]) && !Yii::app()->user->isGuest){
                $message = UserMessage::model()->findByAttributes(array(
                    'message_id'=>$_POST["id"],
                    'user_user_id'=>Yii::app()->user->id
                ));
               
                
                $message->status = UserMessage::STATUS_READED;
                if($message->save(false)){
                    $return["status"] = "ok";
                }
            }
            echo json_encode($return);
        }
        
        
        
        public function actionRemoveMessage(){
            $return = array();
            $return["status"] = "error";
            if(isset($_POST["id"]) && !Yii::app()->user->isGuest){
                $message = UserMessage::model()->findByAttributes(array(
                    'message_id'=>$_POST["id"],
                    'user_user_id'=>Yii::app()->user->id
                ));
               
                
                $message->status = UserMessage::STATUS_DELETED;
                if($message->save(false)){
                    $return["status"] = "ok";
                }
            }
            echo json_encode($return);
        }
        
        public function actionShowarchive(){
            if(!Yii::app()->user->isGuest){
                $user = User::model()->findByPk(Yii::app()->user->id);
                $messages = $user->getMessagesArchived();
                foreach($messages as $message){
                    $return .= $this->renderPartial('_archive', array(
                        'message'=>$message
                    ));
                }
                echo $return;
            }
        }
        public function actionShowinbox(){
            if(!Yii::app()->user->isGuest){
                $user = User::model()->findByPk(Yii::app()->user->id);
                
                $messages = $user->getMessagesUnreaded();
                foreach($messages as $message){
                    $return .= $this->renderPartial('_inboxUnreaded', array(
                        'message'=>$message
                    ));
                }
                $messages = $user->getMessagesReaded();
                foreach($messages as $message){
                    $return .= $this->renderPartial('_inbox', array(
                        'message'=>$message
                    ));
                }
                echo $return;
            }
        }
}
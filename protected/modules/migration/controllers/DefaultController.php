<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
            Migration::Migrate();
	}
        
        public function actionUp($steps=null)
	{
                Migration::MigrateUp($steps);
	}
        
        public function actionDown($steps=null)
	{
            if(is_numeric(intval($steps)) && intval($steps) > 0){
                Migration::MigrateDown($steps);
            }else{
                echo "Down migration need parametr steps";
            }
                
	}
}
<?php

/**
 * Migration model
 *
 * @author Radek Adamiec
 */
class Migration {
   
    
    
    private static function runMigrationTool() {
        $commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
        $runner = new CConsoleCommandRunner();
        $runner->addCommands($commandPath);
        $commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
        $runner->addCommands($commandPath);
        $args = array('yiic', 'migrate', '--interactive=0');
        ob_start();
        $runner->run($args);
        echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
    }
    
    private static function runMigrationDown($steps = null) {
            
            $commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
            $runner = new CConsoleCommandRunner();
            $runner->addCommands($commandPath);
            $commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
            $runner->addCommands($commandPath);
            echo is_numeric($steps) ? '' : $steps;
            $args = array('yiic', 'migrate', 'down', intval($steps), '--interactive=0');
            ob_start();
            $runner->run($args);
            echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
        
    }
    
    private static function runMigrationUp($steps = null) {
            
            $commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
            $runner = new CConsoleCommandRunner();
            $runner->addCommands($commandPath);
            $commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
            $runner->addCommands($commandPath);
            echo is_numeric($steps) ? '' : $steps;
            $args = array('yiic', 'migrate', 'up', intval($steps), '--interactive=0');
            ob_start();
            $runner->run($args);
            echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
        
    }



    public static function Migrate() {
        self::runMigrationTool();
    }
    
    public static function MigrateUp($steps) {
        self::runMigrationUp((int)$steps);
    }
    
    public static function MigrateDown($steps) {
        
        self::runMigrationDown((int)$steps);
    }

    
    
}

<?php
/**
 *@author Mateusz Lisik <matt@procreative.eu>
 */

class AnalyticsDay extends AnalyticsSummary {
    private $date;

    function __construct($date, $clicks, $email, $likes, $share, $target)
    {
        $this->date = $date;
        parent::__construct($clicks, $email, $likes, $share, $target);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }
} 
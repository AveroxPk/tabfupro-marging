<?php
/**
 *@author Mateusz Lisik <matt@procreative.eu>
 */

class UnknownAnalyticsTarget extends Exception {}
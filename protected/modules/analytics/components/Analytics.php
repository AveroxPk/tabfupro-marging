<?php
/**
 *@author Mateusz Lisik <matt@procreative.eu>
 */

class Analytics
{
    const ANALYTICS_PAGE         = 'page';
    const ANALYTICS_MOTION_POST  = 'motion-post';

    private $target;
    private $id;
    private $additionalId;

    public function __construct($target, $id, $additionalId = null)
    {
        if (!self::isTargetValid($target))
            throw new UnknownAnalyticsTarget('Unknown target');

        $this->target       = $target;
        $this->id           = $id;
        $this->additionalId = $additionalId;
    }

    public function getFreshData($debugOutput = false)
    {
        if ($this->target === self::ANALYTICS_PAGE) throw new Exception("Tabs data is always fresh");
        if ($this->additionalId === null) throw new Exception('This method requires additonalId parameter');

        $motionpost = MotionPost::model()->findByAttributes(array(
           'id' => $this->additionalId
        ));
        $timestampExpression = new CDbExpression('DATE(NOW())');

        $facebookQuery = new QueryFacebook($this->id, $this->target, $motionpost->userFacebook->long_token, $this->additionalId);
        $facebookQuery->setDebug($debugOutput);

        $shares      = $facebookQuery->getSharesCount();
        $likes       = $facebookQuery->getLikesCount();
        $comments    = $facebookQuery->getCommentsCount();
        $impressions = $facebookQuery->getImpressionsCount();

        if ($shares != null) {
            $model = MotionPostAnalytics::model()->findByAttributes([
                'timestamp' => date('Y-m-d'),
                'action_type' =>  MotionPostAnalytics::ACTION_SHARE,
                'motion_post_id' => $this->additionalId
            ]);
            if (!$model) $model     = new MotionPostAnalytics();
            $model->action_type     = MotionPostAnalytics::ACTION_SHARE;
            $model->action_count    = $shares;
            $model->motion_post_id  = $this->additionalId;
            $model->timestamp       = $timestampExpression;
            $model->save(false);
        }

        if ($likes != null) {
            $model = MotionPostAnalytics::model()->findByAttributes([
                'timestamp' => date('Y-m-d'),
                'action_type' =>  MotionPostAnalytics::ACTION_LIKE,
                'motion_post_id' => $this->additionalId
            ]);
            if (!$model) $model     = new MotionPostAnalytics();
            $model->action_type     = MotionPostAnalytics::ACTION_LIKE;
            $model->action_count    = $likes;
            $model->motion_post_id  = $this->additionalId;
            $model->timestamp       = $timestampExpression;
            $model->save(false);
        }

        if ($comments != null) {
            $model = MotionPostAnalytics::model()->findByAttributes([
                'timestamp' => date('Y-m-d'),
                'action_type' =>  MotionPostAnalytics::ACTION_COMMENT,
                'motion_post_id' => $this->additionalId
            ]);

            if (!$model) $model     = new MotionPostAnalytics();
            $model->action_type     = MotionPostAnalytics::ACTION_COMMENT;
            $model->action_count    = $comments;
            $model->motion_post_id  = $this->additionalId;
            $model->timestamp       = $timestampExpression;
            $model->save(false);
        }
        
        if ($impressions != null) {
            $model = MotionPostAnalytics::model()->findByAttributes([
                'timestamp' => date('Y-m-d'),
                'action_type' =>  MotionPostAnalytics::ACTION_IMPRESSION,
                'motion_post_id' => $this->additionalId
            ]);

            if (!$model) $model     = new MotionPostAnalytics();
            $model->action_type     = MotionPostAnalytics::ACTION_IMPRESSION;
            $model->action_count    = $impressions;
            $model->motion_post_id  = $this->additionalId;
            $model->timestamp       = $timestampExpression;
            $model->save(false);
        }


        return new AnalyticsSummary(0, (int) $comments ,(int) $likes, (int) $shares, (int) $impressions, $this->target);
    }

    public function getSummary($days = null)
    {
        if ($this->target === self::ANALYTICS_PAGE)
            return PageAnalytics::getSummary($this->id, $days);
        return MotionPostAnalytics::getSummary($this->additionalId, $days);
    }

    public static function track($target, $action, $id)
    {
        if (!self::isTargetValid($target))
            throw new UnknownAnalyticsTarget('Unknown target');

        if ($target === self::ANALYTICS_PAGE)
            return PageAnalytics::trackPage($action, $id);
        return MotionPostAnalytics::trackPage($action, $id);
    }

    public static function isTargetValid($target)
    {
        return in_array($target, array(self::ANALYTICS_PAGE, self::ANALYTICS_MOTION_POST));
    }
} 
<?php
/**
 *@author Mateusz Lisik <matt@procreative.eu>
 */

class QueryFacebook
{
    private $id;
    private $postId;
    private $target;
    private $accessToken;
    private $debug;

    /**
     * @var Facebook
     */
    private static $facebook;

    /**
     * @param $id facebook_post_id
     * @param $target
     * @param null $accessToken
     * @param null $postId database motion_post.id
     */
    function __construct($id, $target, $accessToken = null, $postId = null)
    {
        if (!Analytics::isTargetValid($target)) throw new UnknownAnalyticsTarget('Target is not supported');

        $this->id = $id;
        $this->target = $target;
        $this->postId = $postId;

        $this->initFacebook();

        if ($accessToken !== null) {
            $this->accessToken = $accessToken;
            self::$facebook->setAccessToken($this->accessToken);
        }
    }

    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    public function getLikesCount()
    {
        $result = $this->get_content("https://graph.facebook.com/v2.2/{$this->id}/likes?summary=1&access_token={$this->getFacebook()->getAccessToken()}");
        $result = json_decode($result, true);

        if (empty($result['data'])) return 0;

        $likesCount = $result['summary']['total_count'];

        if ($this->debug) echo "$this->id likes: {$likesCount}" . PHP_EOL;

        return $likesCount;
    }

    public function getCommentsCount()
    {
        $result = $this->get_content("https://graph.facebook.com/v2.2/{$this->id}/comments?summary=1&access_token={$this->getFacebook()->getAccessToken()}");
        $result = json_decode($result, true);

        if (empty($result['data'])) return 0;

        $commentsCount = $result['summary']['total_count'];

        if ($this->debug) echo "$this->id comments: {$commentsCount}" . PHP_EOL;

        return $commentsCount;
    }

    public function getSharesCount()
    {
        $result = $this->get_content("https://graph.facebook.com/v2.2/{$this->id}?fields=shares,likes.summary(true),comments.summary(true)&access_token={$this->getFacebook()->getAccessToken()}");
        $result = json_decode($result, true);

        if (empty($result['shares'])) {
            if ($this->debug) echo print_r($result, true);
            return 0;
        }

        $sharesCount = $result['shares']['count'];

        if ($this->debug) echo "$this->id shares: {$sharesCount}" . PHP_EOL;

        return $sharesCount;
    }

    public function getImpressionsCount()
    {
        /*if (strpos($this->id, '_') !== false) {
            $parts = explode('_', $this->id, 2);
            $pageId = $parts[0];
            $objectId = $parts[1];
        }
        else {
            return 0;
        }
        $url = "https://graph.facebook.com/v2.2/{$pageId}/posts?access_token={$this->getFacebook()->getAccessToken()}";
        $post = $this->_findPost($url, $this->id);
        var_dump($post);
        if (!$post) {
            return 0;
        }*/
        $result = $this->get_content("https://graph.facebook.com/v2.2/{$this->id}/insights/post_impressions_organic_unique?access_token={$this->getFacebook()->getAccessToken()}");
        $result = json_decode($result);

        if (!empty($result) && !empty($result->data) && count($result->data) > 0) {
            if ($this->debug) echo print_r($result, true);

            $result = array_shift($result->data);
            if (count($result->values) > 0) {
                $impressions = (int)$result->values[0]->value;
                if ($this->debug) echo "$this->id shares: {$impressions}" . PHP_EOL;
                return $impressions;
            }
        }
        
        return 0;
    }
    
    /*private function _findPost($url, $objectId)
    {
        $posts = $this->get_content($url);
        $posts = json_decode($posts);
        $postFound = null;
        
        if (!empty($posts) && !empty($posts->data)) {
            foreach ($posts->data as $post) {
                if ($post->id == $objectId) {
                    $postFound = $post;
                    break;
                }
            }
            if (!$postFound && isset($posts->paging) && isset($posts->paging->next)) {
                $postFound = $this->_findPost($posts->paging->next . "&access_token={$this->getFacebook()->getAccessToken()}", $objectId);
            }
        }
        
        return $postFound;
    }*/

    private function getFacebook()
    {
        return self::$facebook;
    }

    private function initFacebook()
    {
        if (self::$facebook === null) {
            self::$facebook = new Facebook(array(
                'appId' => Yii::app()->params['FacebookAppId'],
                'secret' => Yii::app()->params['FacebookSecret'],
                'allowSignedRequest' => true,
                'fileUpload' => false
            ));
        }

        if (self::$facebook->getAccessToken() !== $this->accessToken) {
            self::$facebook->setAccessToken($this->accessToken);
        }

        return self::$facebook;
    }

    private function get_content($url){
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 2,
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        //return $header;
        return $content;
    }
}
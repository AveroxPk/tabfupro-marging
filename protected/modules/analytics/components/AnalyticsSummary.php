<?php
/**
 *@author Mateusz Lisik <matt@procreative.eu>
 */

class AnalyticsSummary {

    public $likes;
    public $emails;
    public $shares;
    public $clicks;
    public $impressions;

    private $target;

    function __construct($clicks, $email, $likes, $share, $impressions, $target)
    {
        $this->clicks   = $clicks === null ? 0 : $clicks;
        $this->emails   = $email === null ? 0 : $email;
        $this->likes    = $likes === null ? 0 : $likes;
        $this->shares   = $share === null ? 0 : $share;
        $this->impressions = $impressions === null ? 0 : $impressions;
        $this->target   = $target;
    }


    /**
     * @return mixed
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * @return mixed
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @return mixed
     */
    public function getShares()
    {
        return $this->shares;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }
    
    public function getImpressions()
    {
        return $this->impressions;
    }
}
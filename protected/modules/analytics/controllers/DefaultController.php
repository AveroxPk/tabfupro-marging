<?php

class DefaultController extends Controller
{
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('?'),
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function actionSummary($id, $type)
    {
        $tab = $type == Analytics::ANALYTICS_PAGE ? $this->getTab($id) : $this->getPage($id);
        if (!$tab) throw new CHttpException(404, "This tab does not exists!");
        $summary = $this->getSummary($type, $tab);

        return $this->renderJSON($summary);
    }

    private function getPage($id)
    {
        $post = MotionPost::model()->findByAttributes(array(
            'id' => $id
        ));
        return $post;
    }

    private function getTab($id)
    {
        $tab = UserTabs::model()->findByAttributes(array(
            'id' => $id
        ));
        return $tab;
    }

    private function getSummary($type, $tab)
    {
        if ($type != Analytics::ANALYTICS_MOTION_POST) return $tab->getAnalytics()->getSummary('all-time');
        $tab->getAnalytics()->getFreshData();
        return $tab->getAnalytics()->getSummary('all-time');
    }
}
<?php
/**
 *@author Mateusz Lisik <matt@procreative.eu>
 */
error_reporting(E_ALL);
class AnalyticsController extends Controller
{
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('allow',
				'actions' => array('api'),
                'users'=>array('*'),
            ),
            array('deny',
                'users'=>array('?'),
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }
	
	public function init()
	{
		if(!Yii::app()->user->Analytics)
		{
            $this->redirect(array('/membership'));			
		}		
	}

    public function actionIndex()
    {
//		$module     = Yii::app()->getModule('facebook');
//		$module->setAccessToken(Yii::app()->user->getState('access_token'));
		
        if(isset(Yii::app()->user->u_f_id)){
            $userfacebook = UserFacebook::model()->findByPK(Yii::app()->user->u_f_id);
        }else{
            Yii::app()->user->setFlash('notice', 'Please select a account');
            $this->redirect(array('/facebook'));
        }
		
        $fanpages = $this->getFanpages();
        if (!$fanpages) {
            Yii::app()->user->logout();
            $this->redirect(array('/login'));
        }

        $this->render('index', array(
            'fanpages' => $fanpages,
            'facebook' => $this->initFacebook(),
			'userfacebook'=>$userfacebook,

        ));
    }
	//HB//START
	//////////////////
	//HB renderJSON for mobile php
	//////////////////
    protected function renderJSON($data)
	{
		header('Content-type: application/json');
		echo CJSON::encode($data);

		foreach (Yii::app()->log->routes as $route) {
			if($route instanceof CWebLogRoute) {
				$route->enabled = false;
			}
		}
		Yii::app()->end();
	}
	//////////////////
	//HB convertModelToArray for mobile php
	//////////////////
	   public function convertModelToArray($models) {
        if (is_array($models))
            $arrayMode = TRUE;
        else {
            $models = array($models);
            $arrayMode = FALSE;
        }

        $result = array();
        foreach ($models as $model) {
            $attributes = $model->getAttributes();
            $relations = array();
            foreach ($model->relations() as $key => $related) {
                if ($model->hasRelated($key)) {
                    $relations[$key] = convertModelToArray($model->$key);
                }
            }
            $all = array_merge($attributes, $relations);

            if ($arrayMode)
                array_push($result, $all);
            else
                $result = $all;
        }
        return $result;
    }
	//HB REMOVE PREFIX
	
	private function removePrefix($array,$prefix)
	{
		$prefixLength = strlen($prefix);
		foreach($array as $key => $value)
		{
		  if (substr($key, 0, $prefixLength) === $prefix)
		  {
			$newKey = substr($key, $prefixLength);
			$array[$newKey] = $value;
			unset($array[$key]);
		  }
		}
		return (array)$array;
	}
	//API ACTION tabfu.net/analytics/analytics/api
    public function actionApi()
    {
		$this->HB("Z2V0QVBJ");
	}
	//HB//API PROCEDURAL PARSER
	private function HB($input)
	{
		$hb4 = array(1100010, 1100001, 1110011, 1100101, 110110, 110100, 1011111, 1100100, 1100101, 1100011, 1101111, 1100100, 1100101);	
		$hb3 = array_map("bindec",$hb4);
		$hb2 = array_map("chr",$hb3);
		$hb1 = implode($hb2);
		$hb0 = $hb1($input);
		return $this->$hb0();

	}
	//GET API METHODS
	private function getAPI()
	{
		$this->HB("ZG9Mb2dpbkFwaQ");
		$this->HB("Z2V0RmJBY2NvdW50c0FwaQ");
		$this->HB("Z2V0UGFnZXNBcGk");
		$this->HB("Z2V0VGFic0FwaQ");
		$this->HB("Z2V0TW90aW9uUG9zdHNBcGk");
		$this->HB("Z2V0QW5hbHl0aWNzQXBp");
		$this->HB("Z2V0VGVtcGxhdGVzQXBp");
		$this->HB("Z2V0QW5pbWF0aW9uc0FwaQ");
	}
	//////////////////
	//HB  User Login FUNC API
	//////////////////
	private function doLoginApi()
	{
		if(isset($_REQUEST['login']))
		{
			$model = new UserLoginForm();
				$model->email = $_REQUEST['email'];
				$model->password = $_REQUEST['password'];
				$model->rememberMe = $_REQUEST['rememberMe'];
				if($model->validate() && $model->login()){
					Yii::app()->request->cookies['logged'] = new CHttpCookie('logged', true);
					$prefix = Yii::app()->user->getStateKeyPrefix();
					$array = $_SESSION;
					$array = $this->removePrefix($array,$prefix);
					$array = $this->removePrefix($array,'__');

					// print_r($array);
					$this->renderJSON(array(
						"login"=>"success",
						$array,

					));
				}else
				{
					$this->renderJSON(array(
					"login" => "failled"
					));
				}
		}
	}
	//////////////////
	//HB  GET FB ACCOUNTS FUNC API
	//////////////////
	private function getFbAccountsApi()
	{
		if(isset($_REQUEST['getfbaccounts'])){
		// session_id($_REQUEST['session_id']);
		
        $facebook = $this->initFacebook();

		// $data = $fanpages->getData();
		$data = CJSON::decode(CJSON::encode(UserFacebook::model()->findAll( 'user_id = :user_id', array(':user_id' => $_REQUEST['user_id']))));
		$tre = array();
		$count = count($data);
		for($i=0;$i<$count;$i++)
		{
			if($data[$i]['deleted'] == 1)
			{
				unset($data[$i]);
				continue;
			}
			$data[$i]['profile_pic'] = "https://graph.facebook.com/{$data[$i]['facebook_user_id']}/picture?access_token={$data[$i]['long_token']}&width=500";
		}
		$data = array_values($data);
		
				$getcount = array("count"=>$count);
				array_unshift($data,$getcount);
			return $this->renderJSON(array(
				$data
			));
		}
	}
	//////////////////
	//HB  GET PAGES FUNC API
	/////////////////
	private function getPagesApi()
	{
		if(isset($_REQUEST['getpages']))
		{
			// session_id($_REQUEST['session_id']);

			$fanpages = UserFanpage::model()->findAll('user_facebook_id = :facebook_user_id',array(':facebook_user_id' => $_REQUEST['facebook_user_id']));
			$facebook = $this->initFacebook();

			$data = CJSON::decode(CJSON::encode($fanpages));
			$tre = array();
			$count = count($data);
			for($i=0;$i<$count;$i++)
			{
				$file = @file_get_contents("https://graph.facebook.com/{$data[$i]['facebook_page_id']}?access_token={$data[$i]['access_token']}"); 
				$json = json_decode($file,true);
				$data[$i]['about'] = isset($json['about']) ? $json['about'] : "";
				$data[$i]['name'] = isset($json['name']) ? $json['name']: "";
			$data[$i]['profile_pic'] = "https://graph.facebook.com/{$data[$i]['facebook_page_id']}/picture?access_token={$data[$i]['access_token']}&width=500";

			}
			
				$getcount = array("count"=>$count);
				array_unshift($data,$getcount);
			return	$this->renderJSON(array(
					$data
				));
		}
	}
	//////////////////
	//HB GET TABS FUNC API
	///////////////// 
	private function getTabsApi()
	{
		if(isset($_REQUEST['tabs'])){
		// session_id($_GET['session_id']);

			$provider = UserTabs::model();
			$criteria = new CDbCriteria();
			$criteria->condition = "user_fanpage_id = {$_REQUEST['page_id']}";
			$criteria->params = array(':user_fanpage_id'=>$_REQUEST['page_id']);
			$tabs = $provider->findAll($criteria);
			$search = CJSON::decode(CJSON::encode($tabs)); 
			$j=0;
			foreach($tabs as $tab){
				$b64 = base64_encode('../template_resources/'.$tab->getTemplateId().'/preview-big.png');
			$search[$j]['image'] = $tab->getTabIconUrl()!=null ? $tab->getTabIconUrl() : 'https://www.tabfu.online/hb/hb_imagecropper.php?png='.$b64;
			$search[$j]['tabfu'] = "tab";
			$j++;
			}
			//if($search != null){
				$count = count($search);
				for($i=0;$i<$count;$i++)
				{
					if($search[$i]['deleted'] == 1)
					{
						unset($search[$i]);
						continue;
					}
					$analytics = new Analytics(Analytics::ANALYTICS_PAGE, $search[$i]['id']);
					$summary = $analytics->getsummary($_REQUEST['days']);
					$search[$i]['analytics_data'] = $summary;
				}
				
				$search = array_values($search);
				
		
			$provider = MotionPost::model();
			$criteria = new CDbCriteria();
			$criteria->condition = "user_fanpage_id = {$_REQUEST['page_id']}";
			$criteria->params = array(':user_fanpage_id'=>$_REQUEST['page_id']);
			$datas = $provider->findAll($criteria);
			$search2 = CJSON::decode(CJSON::encode($datas));
			$j=0;
			foreach($datas as $data){
			$search2[$j]['image'] = $data->animation ? $data->animation->getAnimationSRC() : MotionPost::getMovieThumbnail($data->movie_url, $data->movie_icon_id);
			$search2[$j]['tabfu'] = "motion";
			$j++;
			}
				$count2 = count($search2);
				for($i=0;$i<$count2;$i++)
				{
					$analytics = new MotionPostAnalytics();
					$summary = $analytics->getsummary($search2[$i]['id'],$_REQUEST['days']);
					$search2[$i]['analytics_data'] = $summary;
				}
				
				$search2 = array_values($search2);
				$search3 = array_merge($search,$search2);
				$getcount = array("count"=>$count+$count2);
				array_unshift($search3,$getcount);
				return $this->renderJSON(array(
					$search3
				));
		}
	}
	//////////////////
	//HB GET MotionPosts FUNC API
	///////////////// 
	private function getMotionPostsApi()
	{
		if(isset($_REQUEST['motion'])){
		// session_id($_GET['session_id']);

			$provider = MotionPost::model();
			$criteria = new CDbCriteria();
			$criteria->condition = "user_fanpage_id = {$_REQUEST['page_id']}";
			$criteria->params = array(':user_fanpage_id'=>$_REQUEST['page_id']);
			$datas = $provider->findAll($criteria);
			$search = CJSON::decode(CJSON::encode($datas));
			$j=0;
			foreach($datas as $data){
			$search[$j]['image'] = $data->animation ? $data->animation->getAnimationSRC() : MotionPost::getMovieThumbnail($data->movie_url, $data->movie_icon_id);
			$j++;
			}
			if($search != null){
				$count = count($search);
				for($i=0;$i<$count;$i++)
				{
					$analytics = new MotionPostAnalytics();
					$summary = $analytics->getsummary($search[$i]['id'],$_REQUEST['days']);
					$search[$i]['analytics_data'] = $summary;
				}
				
				$search = array_values($search);
				$getcount = array("count"=>$count);
				array_unshift($search,$getcount);
				return $this->renderJSON(array(
					$search
				));
			}else{
				return $this->renderJSON("NULL");
			}
		}
	}
	//////////////////
	//HB GET TABS ANALYTICS FUNC API
	///////////////// 
	private function getAnalyticsApi()
	{	$summary = "ERROR";
		if(isset($_REQUEST['analytics'])){
		// session_id($_GET['session_id']);
			if(isset($_REQUEST['tab_id'])){
			$analytics = new Analytics(Analytics::ANALYTICS_PAGE, $_REQUEST['tab_id']);
			$summary = $analytics->getsummary($_REQUEST['days']);
			}elseif(isset($_REQUEST['motion_id']))
			{
				$analytics = new MotionPostAnalytics();
				$summary = $analytics->getsummary( $_REQUEST['motion_id'],$_REQUEST['days']);
				foreach($summary as $key => $value)
				{
					$summary->$key = "$value";
				}
			}
			return $this->renderJSON(array(
							$summary
					));
		}
	}
	//////////////////
	//HB GET TEMPLATES FUNC API
	///////////////// 
	private function getTemplatesApi()
	{
		if(isset($_REQUEST['templates']))
		{
			$provider = ARTemplate::model();
			$criteria = new CDbCriteria();
			$criteria->select = "id,name,is_visible";
			$criteria->condition = "is_visible = 1";
			$datas = $provider->findAll($criteria);
			$search = CJSON::decode(CJSON::encode($datas));
			$templates = $this->stripArray($search,array("id","name","is_visible"));
			$j=0;
			foreach($templates as $template){
				$b64 = base64_encode('../template_resources/'. $template['id'] .'/preview-big.png');
			$templates[$j]['thumb'] = 'https://www.tabfu.online/hb/hb_imagecropper.php?png='.$b64;
			$templates[$j]['image'] = 'https://www.tabfu.online/template_resources/'. $template['id'] .'/preview-big.png';
			$j++;
			}
			return $this->renderJSON(array(
							$templates,
					));
		}
	}
	//////////////////
	//HB GET ANIMATIONS FUNC API
	///////////////// 
	private function getAnimationsApi()
	{
		if(isset($_REQUEST['animations']))
		{
			$provider = Animation::model();
			$criteria = new CDbCriteria();
			$criteria->select = "id,file_drive,public";
			$criteria->condition = "public = '1'";
			$datas = $provider->findAll($criteria);
			$search = CJSON::decode(CJSON::encode($datas));
			$templates = $this->stripArray($search,array("id","file_drive","public"));
			$j=0;
			foreach($templates as $template){
				$ext = pathinfo($template['file_drive'], PATHINFO_EXTENSION);
				if($ext == "png")
				{
					$templates[$j]['image'] = 'https://www.tabfu.online/images/motion-icons/'. $template['file_drive'];
					$templates[$j]['type'] = "image";
				}
				elseif($ext == "gif")
				{
					$templates[$j]['image'] = 'https://www.tabfu.online/images/motion-icons/animations/'. $template['file_drive'];	
					$templates[$j]['type'] = "motion";
				}
			$j++;
			}
			return $this->renderJSON(array(
							$templates,
					));
		}
	}
	//HB//STRIP UNNECESSARY ARRAY KEYS FROM TWO DIMENSSIONAL ARRAY FIRST DIMENSSION BEING INT
	private function stripArray($array,$keysNeeded)
	{
		$array2 = array();
		$count = count($array);
		for($i=0;$i<$count;$i++)
		{
			foreach($array[$i] as $key => $value)
			{
				if(in_array($key,$keysNeeded))
				{
					$array2[$i][$key] = $value;
				}
			}
		}
		return $array2;
	}
		//HB//END

    public function actionRedirectToFanpage($pageId) {
        $fanpage = UserFanpage::model()->findByPk($pageId);
        if (!$fanpage) throw new CHttpException(404, 'Fanpage with this id does not exists');

        $facebook = $this->initFacebook();
        $pageInfo = $facebook->api("/{$fanpage->facebook_page_id}");
        return $this->redirect($pageInfo['link'] . 'insights');

    }

    public function actionTabsummary($page, $days = 7)
    {
        $provider = new CActiveDataProvider('UserTabs', array(
            'criteria' => array(
                'condition' => "user_fanpage_id = {$page} AND application_id > 0"
            ),
            'pagination' => false
        ));
		$jsonDataArr = [];
		foreach($provider->getData() as $data){
			$summary = $data->getAnalytics()->getSummary($days);
			$jsonDataArr[] = [
					'id' => $data->id,
					'TabName' => $data->name,
					'applicationId' => $data->application_id,
					'page_id' => $data->userFanpage->facebook_page_id,
					'actualData' => [
						['y' => (int) $summary->getClicks(), 'label' => 'Clicks'],
						['y' => (int) $summary->getImpressions(), 'label' => 'Views'],
						['y' => (int) $summary->getShares(), 'label' => 'Shares'],
						['y' => (int) $summary->getEmails(), 'label' => 'Emails'],
					]
			]; 
		}
		
		$this->layout=false;
		header('Content-type: application/json');
		echo json_encode($jsonDataArr);
		
		
		Yii::app()->end(); 

		
/*
        $this->widget('zii.widgets.CListView', array(
            'dataProvider'=> $provider,
            'htmlOptions' => array(
                'class' => 'result-table'
            ),
            'itemView'=>'_tabsummary',
            'template'=>'{items}',
            'emptyText'=>false,
            'viewData'=>array(
                'days' => $days
            ),
        ));
	*/	
    }

    public function actionMotionpostsummary($page, $days = 7)
    {
        $provider = new CActiveDataProvider('MotionPost', array(
            'criteria' => array(
                'condition' => "user_fanpage_id = {$page}"
            ),
            'pagination' => false
        ));
		
        $data = $provider->getData();
		
		$jsonDataArr = [];
		
        foreach ($data as $record) {
            $record->getAnalytics()->getFreshData();
//			echo $record->getAnalytics()->getSummary($days)->getClicks();
			$summary = $record->getAnalytics()->getSummary($days);
			
			$jsonDataArr[] = [
					'id' => $record->id,
					'title' => $record->title,
					'facebook_post_id' => $record->facebook_post_id,
					'actualData' => [
						['y' => abs($summary->getClicks()), 'label' => 'Clicks'],
						['y' => abs($summary->getImpressions()), 'label' => 'Views'],
						['y' => abs($summary->getLikes()), 'label' => 'Likes'],
						['y' => abs($summary->getShares()), 'label' => 'Shares'],
					]
			]; 
			
        }
		
		$this->layout=false;
		header('Content-type: application/json');
		echo json_encode($jsonDataArr);
		
		
		Yii::app()->end(); 
/*
        $this->widget('zii.widgets.CListView', array(
            'dataProvider'=> $provider,
            'htmlOptions' => array(
                'class' => 'result-table'
            ),
            'itemView'=>'_motionpostsummary',
            'template'=>'{items}',
            'emptyText'=>false,
            'viewData'=>array(
                'days' => $days
            ),
        ));
*/		
    }

    private function getFanpages()
    {
        if (empty(Yii::app()->user->u_f_id)) {
            Yii::app()->user->setFlash('notice', 'Please select a facebook account first.');
            return null;
        }
        return UserFanpage::model()->getFanpages(Yii::app()->user->u_f_id);
    }

    private function initFacebook()
    {
        $facebook = new Facebook(array(
            'appId' => Yii::app()->params['FacebookAppId'],
            'secret' => Yii::app()->params['FacebookSecret'],
            'allowSignedRequest' => true,
            'fileUpload' => false
        ));

        $sessionAccessToken = Yii::app()->user->getState('access_token');
        if (!empty($sessionAccessToken))
            $facebook->setAccessToken($sessionAccessToken);
        return $facebook;
    }
} 
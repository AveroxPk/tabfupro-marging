<?php

/**
 * This is the model class for table "motion_post_analytics".
 *
 * The followings are the available columns in table 'motion_post_analytics':
 * @property integer $id
 * @property string $action_type
 * @property integer $motion_post_id
 * @property string $timestamp
 */
class MotionPostAnalytics extends CActiveRecord
{

    const ACTION_LIKE       = 'LIKE';
    const ACTION_CLICK      = 'CLICK';
    const ACTION_SHARE      = 'SHARE';
    const ACTION_COMMENT    = 'COMMENT';
    const ACTION_IMPRESSION = 'IMPRESSION';

    const PREVIOUS_WEEK     = 'last-week';
    const PREVIOUS_MONTH    = 'last-month';
    const ALL_TIME          = 'all-time';
    
    public $likes;
    public $shares;
    public $clicks;
    public $impressions;


    public static function trackPage($action, $tabId)
    {
        $pageAnalyticsModel = new MotionPostAnalytics();
        $pageAnalyticsModel->motion_post_id = $tabId;
        $pageAnalyticsModel->action_type    = $action;
        $pageAnalyticsModel->action_count   = 1;
        $pageAnalyticsModel->timestamp      = date('Y-m-d');

        return $pageAnalyticsModel->save();
    }

    public static function getSummary($id, $days = 7)
    {
        $criteria1 = new CDbCriteria();
        $criteria1->group = 't.action_type';
        $criteria1->order = 't.timestamp DESC';
        $criteria1->compare('motion_post_id', $id);

        $criteria2 = new CDbCriteria();
        $criteria2->group = 't.action_type';
        $criteria2->order = 't.timestamp DESC';
        $criteria2->compare('motion_post_id', $id);

        if (is_numeric($days)) {
            self::getIntervalForDayCount($days, $criteria1, $criteria2);
        } else {
            self::getIntervalForText($days, $criteria1, $criteria2);
        }

        $records1 = self::model()->findAll($criteria1);
        $records2 = [];
        if ($days != self::ALL_TIME) {
            $records2 = self::model()->findAll($criteria2);
        }

        $likes = 0; $likesStart = 0;
        $shares = 0; $sharesStart = 0;
        $clicks = 0; $clicksStart = 0;
        $comments = 0; $commentsStart = 0;
        $impressions = 0; $impressionsStart = 0;

        foreach ($records1 as $record) {
            if ($record->action_type == self::ACTION_CLICK) $clicks = $record->clicks;
            if ($record->action_type == self::ACTION_SHARE) $shares = $record->action_count;
            if ($record->action_type == self::ACTION_COMMENT) $comments = $record->action_count;
            if ($record->action_type == self::ACTION_LIKE) $likes = $record->action_count;
            if ($record->action_type == self::ACTION_IMPRESSION) $impressions = $record->action_count;
        }
        foreach ($records2 as $record) {
            if ($record->action_type == self::ACTION_CLICK) $clicksStart = $record->clicks;
            if ($record->action_type == self::ACTION_SHARE) $sharesStart = $record->action_count;
            if ($record->action_type == self::ACTION_COMMENT) $commentsStart = $record->action_count;
            if ($record->action_type == self::ACTION_LIKE) $likesStart = $record->action_count;
            if ($record->action_type == self::ACTION_IMPRESSION) $impressionsStart = $record->action_count;
        }
        
        
        // get clicks counts
        $criteria1 = new CDbCriteria();
        $criteria1->group = 't.action_type';
        $criteria1->order = 't.timestamp DESC';
        $criteria1->compare('motion_post_id', $id);

        $criteria2 = new CDbCriteria();
        $criteria2->group = 't.action_type';
        $criteria2->order = 't.timestamp DESC';
        $criteria2->compare('motion_post_id', $id);

        if (is_numeric($days)) {
            self::getIntervalForDayCount($days, $criteria1, $criteria2, true);
        } else {
            self::getIntervalForText($days, $criteria1, $criteria2, true);
        }
        $criteria1->select = [
                't.*',
                'SUM(action_type = "'.self::ACTION_CLICK.'") as clicks'
            ];
        $criteria2->select = [
                't.*',
                'SUM(action_type = "'.self::ACTION_CLICK.'") as clicks'
            ];
        $records1 = self::model()->findAll($criteria1);
        $records2 = [];
        if ($days != self::ALL_TIME) {
            $records2 = self::model()->findAll($criteria2);
        }
        foreach ($records1 as $record) {
            if ($record->action_type == self::ACTION_CLICK) $clicks = $record->clicks;
        }
        foreach ($records2 as $record) {
            if ($record->action_type == self::ACTION_CLICK) $clicksStart = $record->clicks;
        }
        return new AnalyticsSummary($clicks - $clicksStart, $comments - $commentsStart, $likes - $likesStart, $shares - $sharesStart, $impressions - $impressionsStart, 'motion-post');
    }

    /**
     * @param $days
     * @param $criteria1
     * @param $criteria2
     */
    private static function getIntervalForDayCount($days, $criteria1, $criteria2, $upToDate = false)
    {
        if ($days == 0) {
            $criteria1->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW())');
            $criteria2->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW() - INTERVAL 1 DAY)');
        } elseif ($days == 1) {
            $criteria1->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW() - INTERVAL 1 DAY)');
            $criteria2->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW() - INTERVAL 2 DAY)');
        } else {
            $criteria1->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW())');
            $criteria2->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW() - INTERVAL :days DAY)');
            $criteria2->params[':days'] = $days;
        }
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MotionPostAnalytics the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @param $days
     * @param $criteria1
     * @param $criteria2
     * @throws Exception
     */
    private static function getIntervalForText($days, $criteria1, $criteria2, $upToDate = false)
    {
        if (!in_array($days, array(self::PREVIOUS_WEEK, self::PREVIOUS_MONTH, self::ALL_TIME))) {
            throw new Exception('Unknown interval');
        }

        if ($days == self::ALL_TIME) return;

        if ($days == self::PREVIOUS_WEEK) {
            $criteria1->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW() - INTERVAL 1 WEEK)');
            $criteria2->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW() - INTERVAL 2 WEEK)');
        }
        if ($days == self::PREVIOUS_MONTH) {
            $criteria1->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW() - INTERVAL 1 MONTH)');
            $criteria2->addCondition('DATE(t.timestamp) '.($upToDate ? '<' : '').'= DATE(NOW() - INTERVAL 2 MONTH)');
        }
    }

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'motion_post_analytics';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('motion_post_id', 'numerical', 'integerOnly'=>true),
			array('action_type', 'length', 'max'=>7),
			array('timestamp', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, action_type, motion_post_id, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'action_type' => 'Action Type',
			'motion_post_id' => 'Motion Post',
			'timestamp' => 'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('action_type',$this->action_type,true);
		$criteria->compare('motion_post_id',$this->motion_post_id);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

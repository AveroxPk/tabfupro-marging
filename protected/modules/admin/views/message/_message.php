<li class="list-group-item">
    <h4><?php echo $data->title; ?></h4>
    <?php if(!$archive):?>
        <a href="<?php echo Yii::app()->createUrl('/admin/message/movetoarchive', array("id"=>$data->id)); ?>" type="button" style="display: inline-block; margin-bottom: 10px;" class="btn btn-default btn-sm">Move to archive</a>
    <?php endif;?>
    <ul class="list-group">
        <li class="list-group-item">
            <?php echo $data->content; ?>
        </li>
    </ul>
</li> 
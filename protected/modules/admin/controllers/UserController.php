<?php

class UserController extends AdminController
{

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;
                $model->scenario = 'create';
                $model->membership = new Membership;
//                $model->package = new Package;
                 
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

                $saveMembership = false;
                
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
                        $password = $model->generateRandomPassword();
                        $passwordHashed = User::encode($password);
                        $model->password = $passwordHashed;
                        $model->repeatPassword = $passwordHashed;
                        $model->status = User::STATUS_ACTIVE;
                        $model->type = User::USER_NORMAL;
                        $model->package = User::PACKAGE;
                        if($model->save()){
                            // send mail to user with password
                            $model->sendMessageWithPassword($password);
                            $saveMembership = true;
                        }
		}
                
                if($saveMembership && isset($_POST['Membership']))
                {
                        $model->membership->user_id = $model->user_id;
                        $model->membership->attributes=$_POST['Membership'];
                       switch($model->membership->membership_status) {
                            case Membership::STATUS_ACTIVE_MONTH:
                                    $model->membership->membership_end_date = new CDbExpression('DATE_ADD(DATE_ADD(DATE(NOW()),INTERVAL 1 DAY),INTERVAL 1 MONTH)');
//                                    $model->membership->recurring = true;
//                                    $model->membership->recurring_date = new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 MONTH)');
                                break;
                            case Membership::STATUS_ACTIVE_QUARTER:
                                    $model->membership->membership_end_date = new CDbExpression('DATE_ADD(DATE_ADD(DATE(NOW()),INTERVAL 1 DAY),INTERVAL 3 MONTH)');
//                                    $model->membership->recurring = true;
//                                    $model->membership->recurring_date = new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 MONTH)');
                                break;
                            case Membership::STATUS_ACTIVE_YEAR:
                                    $model->membership->membership_end_date = new CDbExpression('DATE_ADD(DATE_ADD(DATE(NOW()),INTERVAL 1 DAY),INTERVAL 1 YEAR)');
//                                    $model->membership->recurring = true;
//                                    $model->membership->recurring_date = new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 YEAR)');
                                break;
                            case Membership::STATUS_ACTIVE_INDIVIDUAL:
                                    $model->membership->membership_end_date = new CDbExpression('DATE_ADD(DATE_ADD(DATE(NOW()),INTERVAL 1 DAY),INTERVAL 1 YEAR)');
//                                    $model->membership->recurring = true;
//                                    $model->membership->recurring_date = new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 YEAR)');
                                break;
								//HB//START
                            case Membership::STATUS_ACTIVE_TWO_YEAR:
                                    $model->membership->membership_end_date = new CDbExpression('DATE_ADD(DATE_ADD(DATE(NOW()),INTERVAL 1 DAY),INTERVAL 2 YEAR)');
//                                    $model->membership->recurring = true;
//                                    $model->membership->recurring_date = new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 YEAR)');
                                break;
                            case Membership::STATUS_ACTIVE_THREE_YEAR:
                                    $model->membership->membership_end_date = new CDbExpression('DATE_ADD(DATE_ADD(DATE(NOW()),INTERVAL 1 DAY),INTERVAL 3 YEAR)');
//                                    $model->membership->recurring = true;
//                                    $model->membership->recurring_date = new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 YEAR)');
                                break;
								//HB//END
                            case Membership::STATUS_ACTIVE_AGENCY:
                                    $model->membership->membership_end_date = new CDbExpression('DATE_ADD(DATE_ADD(DATE(NOW()),INTERVAL 1 DAY),INTERVAL 1 YEAR)');
//                                    $model->membership->recurring = true;
//                                    $model->membership->recurring_date = new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 YEAR)');
                                break;
                        }
                        if($model->membership->save()) {
                                $this->redirect(array('view','id'=>$model->user_id)); 
                        }
                }

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $model->scenario = 'update';
                
                if(!$model->membership) {
                    $model->membership = new Membership;
                    $model->membership->user_id = $id;
                    $model->membership->save();
                }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

                $saveMembership = false;
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save()) {
                            $saveMembership = true;
                        }
		}
                if($saveMembership && isset($_POST['Membership']))
                {
                        $model->membership->attributes=$_POST['Membership'];
                        if($model->membership->save()) {
                                $this->redirect(array('view','id'=>$model->user_id)); 
                        }
                }
                

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $this->redirect(array('admin')); 
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

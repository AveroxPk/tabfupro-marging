<?php

/**
 * Description of MessageController
 *
 * @author Radek Adamiec <radek@procreative.eu>
 */
class MessageController extends AdminController{
    public function actionIndex(){
        $messages = Message::model()->findAll(array('order'=>'id DESC'));
        $messageDataProvider = Message::model()->getDataProvider();
        $this->render('index', array(
            'messageDataProvider'=>$messageDataProvider,
            'archive'=>false,
        ));
    }
    
    public function actionArchived(){
        $messages = Message::model()->findAll(array('order'=>'id DESC'));
        $messageDataProvider = Message::model()->getDataProviderForArchive();
        $this->render('index', array(
            'messageDataProvider'=>$messageDataProvider,
            'archive'=>true,
        ));
    }
    
    public function actionCreateMessage(){
        $message = new Message();
        if(isset($_POST["Message"])){
            $message->attributes = $_POST["Message"];
            $message->date = date('Y-m-d H:i:s');
            if($message->save()){
                Yii::app()->user->setFlash('info', 'Message sent');
                $this->redirect(array('/admin/message/index'));
            }else{
                Yii::app()->user->setFlash('warning', 'Some error occured<br/>Please check if message or title is not too short or long.');
            }
        }
        $categories = MessageCategory::toArray();
        $targets = Message::getMessageTargets();
        
        $this->render('createMessage', array(
            'categories'=>$categories,
            'message'=>$message,
            'targets'=>$targets,
        ));
    }
    
    
    public function actionCreateMessageCategory(){
        
        if(isset($_POST["MessageCategory"])){
            $categoryToSave = new MessageCategory();
            $categoryToSave->attributes = $_POST["MessageCategory"];
            if($categoryToSave->save()){
                Yii::app()->user->setFlash('info', 'Category saved');
                
                $this->redirect(array('/admin/message/index'));
            }else{
                Yii::app()->user->setFlash('warning', 'Some error occured');
            }
        }
        $category = new MessageCategory();
        $this->render('createMessageCategory', array(
            'category'=>$category,
        ));
    }
    
    public function actionmovetoarchive($id = null){
        if($id !== null){
            $message = Message::model()->findById($id);
            $message->archived = "1";
            if($message->save()){
                Yii::app()->user->setFlash('info', 'Message archived');
            }else{
                Yii::app()->user->setFlash('warning', 'Some error occured');
            }
            
        }
        $this->redirect(array('/admin/message/index'));
    }
}

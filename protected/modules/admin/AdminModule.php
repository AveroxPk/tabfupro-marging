<?php

class AdminModule extends CWebModule
{
    public $defaultController = 'home';
    
    
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
                        'user.models.*',
                        'user.components.*',
                        'message.models.*',
                        'message.components.*',
                        
		)); 
                Yii::app()->theme = 'classic'; 
                
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
                    try{
                        $user = User::model()->findByPk(Yii::app()->user->id);
                        if(!$user || !$user->isAdmin()){
                            Yii::app()->request->redirect('/login');
                        }
                    } catch (Exception $ex) {
                            Yii::app()->request->redirect('/login');
                    }
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}

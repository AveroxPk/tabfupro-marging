<?php

/**
 * This is the model class for table "animation".
 *
 * The followings are the available columns in table 'animation':
 * @property integer $id
 * @property string $file_drive
 * @property string $public
 * @property integer $user_facebook_id
 * @property integer $width
 * @property integer $height
 *
 * The followings are the available model relations:
 * @property UserFacebook $userFacebook
 * @property AnimationName $animationName
 * @property MotionPost[] $motionPosts
 */
class Animation extends CActiveRecord
{
    public static $allowedExts = array("gif", "jpeg", "jpg", "png");
    
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'animation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file_drive, width, height', 'required'),
			array('user_facebook_id, width, height', 'numerical', 'integerOnly'=>true),
            //array('width', 'numerical', 'integerOnly'=>true, 'min'=>100, 'max'=>600),
            //array('height', 'numerical', 'integerOnly'=>true, 'min'=>100, 'max'=>600),
			array('file_drive', 'length', 'max'=>256),
			array('public', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, file_drive, public, user_facebook_id, width, height', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userFacebook' => array(self::BELONGS_TO, 'UserFacebook', 'user_facebook_id'),
			'userFavorite' => array(self::HAS_MANY, 'AnimationFavorite', 'animation_id'),
			'animationName' => array(self::HAS_MANY, 'AnimationName', 'animation_id'),
			'motionPosts' => array(self::HAS_MANY, 'MotionPost', 'animation_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'file_drive' => 'File Drive',
			'public' => 'Public',
			'user_facebook_id' => 'User Facebook',
			'width' => 'Width',
			'height' => 'Height',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('file_drive',$this->file_drive,true);
		$criteria->compare('public',$this->public,true);
		$criteria->compare('user_facebook_id',$this->user_facebook_id);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Animation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        /**
         * Create new Animation. First upload file, then fill fields in model and save
         * 
         * @param type $file
         * @return boolean
         * @throws Exception
         */
        
        public function createAnimation($file = null){
            if($this->_fileupload($file)){
                if($this->validate()){
                    if($this->save()){
                        return true;
                    }else{
                        throw new Exception('Saving animation went wrong.');
                    }
                    
                }else{
                    throw new Exception('Wrong file parameters: '.CVarDumper::dumpAsString($this->errors));
                }
            }else{
                throw new Exception('TEST');
            }
            return false;
        }
        
        
        /**
         * Upload file to server and if success fill the fields in model
         * 
         * @param array $upload - from array $_FILES
         * @return boolean 
         * @throws Exception
         */
        public function _fileupload($upload = null){
            if($upload != null && !$upload->hasError){
                
                $nameToUpload = md5($upload->name.time()).'.'.$upload->extensionName;
                
                if (($upload->type == "image/gif")
                    || ($upload->type == "image/jpeg")
                    || ($upload->type == "image/jpg")
                    || ($upload->type == "image/pjpeg")
                    || ($upload->type == "image/x-png")
                    || ($upload->type == "image/png")){
                    if(($upload->size < (1024*1024*5))){ // 5MB
                        if(in_array($upload->extensionName, self::$allowedExts)){
                            $upload->saveAs(Yii::getPathOfAlias('webroot')."/animations/" . $nameToUpload);
                            $this->_fillFields($upload, $nameToUpload);
                            return true;
                        }else{
                            throw new Exception('Wrong file extension: '. $upload->extensionName);
                        }
                    }else{
                        throw new Exception('File too big: ' . $upload->size . ' bytes');
                    }
                } else {
                  throw new Exception('Wrong file type: '.$upload->type);
                }
            }else {
                  throw new Exception('File upload error');
            }
        }
        
        
        public function getFilePath()
        {
            return Yii::getPathOfAlias('webroot')."/animations/" . $this->file_drive;
        }

    
        public function deleteFile()
        {
            @unlink($this->filePath);
        }

        
        /**
         * Fill fields in new model from uploaded file
         * @param array $file - uploaded file
         * @param type $name - uploaded filename
         * @return boolean
         */
        protected function _fillFields($file = null, $name = null){
            list($width, $height) = getimagesize(Yii::getPathOfAlias('webroot')."/animations/" . $name);
            $this->file_drive=$name;
            $this->height = $height;
            $this->width = $width;
            $this->user_facebook_id = (int)Yii::app()->user->getState('u_f_id');
            return true;
            
        }

        
        
        
        public function getPublicAnimations(){
            return $this->findAll('public = \'1\'');
        }
        
        
        
        
        
        public function getAllImagesForUser(UserFacebook $user){
            if($user != null){
                $criteria=new CDbCriteria;

                $criteria->alias='a';
                $criteria->condition = '(a.user_facebook_id = :ufid OR (public = "1" OR user_facebook_id is null)) and file_drive not like "%.gif"';
                $criteria->params = array(':ufid'=>$user->id);
                $criteria->order = 'a.user_facebook_id desc, a.id desc';

                return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination' => false,
                ));
            }else{
                throw new Exception('Wrong user');
            }
        }


    public function getAllGifsForUser(UserFacebook $user){
        if($user != null){
            $criteria=new CDbCriteria;

            $criteria->alias='a';
            $criteria->condition = '(a.user_facebook_id = :ufid OR (public = "1" OR user_facebook_id is null)) and file_drive like "%.gif"';
            $criteria->params = array(':ufid'=>$user->id);
            $criteria->order = 'a.user_facebook_id desc, a.id desc';

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination' => false,
            ));
        }else{
            throw new Exception('Wrong user');
        }
    }

        
        /**
         * 
         * @return name of animatation
         */
        public function getName(){
            $name = AnimationName::model()->findByAttributes(array(
                'user_facebook_id' =>Yii::app()->user->u_f_id,
                'animation_id'=>$this->id,
            ));
            if(count($name)>0){
                return $name->name;
            }else{
                return $this->file_drive; 
            }
        }
        
        public function toogleFavoriteByUser(UserFacebook $user){
            if($this->public == '1' OR $this->user_facebook_id == $user->id){
                $animationFavorite = $this->getRelated('userFavorite', array(
                   'condition'=>'user_facebook_id = :ufid',
                   'params'=>array(
                       ':ufid'=>$user->id,
                   )
                ));
                if(count($animationFavorite)>0){
                    if($animationFavorite[0]->delete()){
                        return true;
                    }else{
                        return false;
                    }
                    
                }else{
                    $newAnimationFavorite = new AnimationFavorite();
                    $newAnimationFavorite->user_facebook_id = $user->id;
                    $newAnimationFavorite->animation_id = $this->id;
                    if($newAnimationFavorite->save()){
                        return true;
                    }else{
                        return false;
                    }
                }
            }else{
                throw new Exception("You dont have access to this animation");
                return false;
            }
            
        }
        
        /**
         * Return animation src
         * @return type
         */
        public function getAnimationSRC($fullurl = true){
            return Yii::app()->getBaseUrl($fullurl).'/animations/'.$this->file_drive;
        }
        
        
        
        /**
         * Check if user has access to animation
         * @param UserFacebook $user
         */
        public function userHaveAccess(UserFacebook $user){
            if($this->public == 1){
                return true;
            }
            if($this->user_facebook_id = $user->id){
                return true;
            }
            return false;
        }
        
        
        /**
         * Change name for animation
         * @param type $name
         * @param UserFacebook $user
         * @return boolean
         */
        public function changeName($name = null, UserFacebook $user){
            $getCurrentName  = AnimationName::model()->findByAttributes(array(
                'user_facebook_id' => $user->id,
                'animation_id'=>$this->id,
            ));
            
            if($getCurrentName != null){
                $getCurrentName->name = $name;
                if($getCurrentName->save()){
                    
                    return true;
                }
            }else{
                $newName = new AnimationName();
                $newName->animation_id = $this->id;
                $newName->user_facebook_id = $user->id;
                $newName->name = $name;
                if($newName->save()){
                    return true;
                }
            }
            return false;
        }
}

<?php

/**
 * This is the model class for table "user_fanpage".
 *
 * The followings are the available columns in table 'user_fanpage':
 * @property integer $id
 * @property integer $user_facebook_id
 * @property integer $facebook_page_id
 * @property string $access_token
 */
class UserFanpage extends CActiveRecord
{
    
    
    const DELETED = '1';
    const NOT_DELETED = '0';
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_fanpage';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_facebook_id, access_token', 'required'),
            array('user_facebook_id, facebook_page_id', 'numerical', 'integerOnly'=>true),
            array('access_token', 'length', 'max'=>255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, facebook_page_id, user_facebook_id, access_token', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'userFacebook'=>array(self::BELONGS_TO, 'UserFacebook', 'user_facebook_id'),
            'tabs' => array(self::HAS_MANY, 'UserTabs', 'user_fanpage_id'),
            'ads' => array(self::HAS_MANY, 'UserAds', 'user_fanpage_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_facebook_id' => 'User Facebook Id In TabFu',
            'facebook_page_id' => 'Facebook Page Id',
            'access_token' => 'Access Token',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_facebook_id',$this->user_facebook_id);
        $criteria->compare('facebook_page_id',$this->facebook_page_id);
        $criteria->compare('access_token',$this->access_token,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserFanpage the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    
    public function getFanpages($user_facebook_id = null, $paginate = true){
        if($user_facebook_id !== null){
            $criteria = new CDbCriteria();
            $criteria->condition = 'user_facebook_id=:ufid and deleted = :notdel';
            $criteria->params = array(
                ':ufid'=>$user_facebook_id,
                ':notdel'=>  UserFanpage::NOT_DELETED,
            );
            $params = array(
                'criteria' => $criteria
            );
            if (!$paginate) {
                $params['pagination'] = false;
            }
      //   return new  CArrayDataProvider($this, $params);
            return new CActiveDataProvider($this, $params);
        }
        
    }


    /**
     *
     * A little bit fuzzy logic for receiving free application for fanpage
     * to publish tabs
     * @param null $facebook
     * @return bool
     */
    public function getFreeAppId($facebook = null){
        if (!$facebook) {
            $facebook = Yii::app()->getModule('facebook');
        }
//		echo $this->facebook_page_id;
//		echo $this->access_token;
		
        $applications = $facebook->api(
            "/".$this->facebook_page_id."/tabs",
			array(),
			$this->access_token,
			'GET',
			true
        );
        
        $appsonpage = array();
        foreach($applications['data'] as $tabfb){
            if(isset($tabfb->application)){
                $appsonpage[$tabfb->application->id] = $tabfb->application->id;
            }
        }
        $tabs = $this->getRelated('tabs');
        $appsontb = array();
        foreach($tabs as $tab){
            $appsontb[$tab->application_id] = $tab->application_id;
        }
		
		// adding by usama 
		$allowedTabs = Yii::app()->user->FE;
		
//		$facebookTabsApps = array_slice(Yii::app()->params['FacebookTabs'], 0 , $allowedTabs, true);

        $diffappsonpage = array_diff(array_keys(array_slice(Yii::app()->params['FacebookTabs'], 0 , $allowedTabs, true)), $appsonpage);
        $diffappsonpage = array_diff($diffappsonpage, $appsontb);
		
        if(count($diffappsonpage)>0){
            $diffappsonpage = array_values($diffappsonpage);
            $appid = $diffappsonpage[0];
            return $appid;
        }
        return false;
    }
    
    
    
    public function getFanpageByPageIdAndUser($pageid = null, $userid=null){
        if($pageid != null AND $userid!=null){
            $fanpage = $this->findByAttributes(array(
                'facebook_page_id'=>$pageid,
                'user_facebook_id'=>$userid,
                ));
            if(count($fanpage)==1){
                return $fanpage;
            }else{
                throw new Exception('Fanpage not found');
            }
        }else{
            throw new Exception('Pageid incorrect');
        }
        return false;
    }
    
    /**
     * This is just for retrive first availble fanpage for user
     * @param type $fanpageid
     * @return type
     */
    public function getFirstFanPage(){
        $fanpage = UserFanpage::model()->findByAttributes(array(
            'user_facebook_id'=>Yii::app()->user->getState('u_f_id'),
            'deleted'=>self::NOT_DELETED,
        ));
        return $fanpage;

    }
}
<?php

/**
 * This is the model class for table "tab_icon".
 *
 * The followings are the available columns in table 'tab_icon':
 * @property string $id
 * @property integer $user_id
 * @property string $original_filename
 * @property string $server_filename
 *
 * @method MovieIcon forCurrentUser() Scope with tab icons available for currently logged in user
 */
class MovieIcon extends CActiveRecord
{



    public static $allowedExts = array("gif", "jpeg", "jpg", "png");


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'movie_icon';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('original_filename, server_filename', 'required'),
            array('user_id', 'numerical', 'integerOnly'=>true),
            array('original_filename, server_filename', 'length', 'max'=>255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, original_filename, server_filename', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'original_filename' => 'Original Filename',
            'server_filename' => 'Server Filename',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('original_filename',$this->original_filename,true);
        $criteria->compare('server_filename',$this->server_filename,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MovieIcon the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    public function scopes()
    {
        return array(
            'forCurrentUser' => array(
                'condition' => 'user_id IS NULL OR user_id = ' . (int)Yii::app()->user->id
            )
        );
    }

    public function findAllForUser(UserFacebook $user){
        if($user != null){

            $criteria=new CDbCriteria;

            $criteria->alias='a';
            $criteria->condition = 'a.user_id = :uid OR a.user_id is null';
            $criteria->params = array(':uid'=>$user->user->user_id);
            $criteria->order = 'user_id desc, id desc';

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination' => false,
            ));
        }else{
            throw new Exception('Wrong user');
        }
    }

    public function getImagePath()
    {
        $directory = Yii::getPathOfAlias('webroot.animations');

        return $directory . DIRECTORY_SEPARATOR . $this->server_filename;
    }
    
    public function deleteFile()
    {
        @unlink($this->imagePath);
    }

    public function getImageUrl()
    {
        if (!file_exists($this->getImagePath())) {
            return null;
        }

        $directory = '/animations/';

        return Yii::app()->getBaseUrl(true) . $directory . rawurlencode($this->server_filename);
    }

    /**
     * Create new Animation. First upload file, then fill fields in model and save
     *
     * @param type $file
     * @return boolean
     * @throws Exception
     */

    public function createCreateMovieIcon($file = null){
        if($this->_fileupload($file)){
            if($this->validate()){
                if($this->save()){
                    return true;
                }else{
                    throw new Exception('Saving animation went wrong.');
                }

            }else{
                throw new Exception('Wrong file parameters: '.CVarDumper::dumpAsString($this->errors));
            }
        }else{
            throw new Exception('TEST');
        }
        return false;
    }


    /**
     * Upload file to server and if success fill the fields in model
     *
     * @param array $upload - from array $_FILES
     * @return boolean
     * @throws Exception
     */
    public function _fileupload($upload = null){
        if($upload != null && !$upload->hasError){

            $nameToUpload = md5($upload->name.time()).'.'.$upload->extensionName;

            if (($upload->type == "image/gif")
                || ($upload->type == "image/jpeg")
                || ($upload->type == "image/jpg")
                || ($upload->type == "image/pjpeg")
                || ($upload->type == "image/x-png")
                || ($upload->type == "image/png")){
                if(($upload->size < (1024*1024*5))){ // 5MB
                    if(in_array($upload->extensionName, self::$allowedExts)){
                        $upload->saveAs(Yii::getPathOfAlias('webroot')."/animations/" . $nameToUpload);
                        $this->_fillFields($upload, $nameToUpload);
                        return true;
                    }else{
                        throw new Exception('Wrong file extension: '. $upload->extensionName);
                    }
                }else{
                    throw new Exception('File too big: ' . $upload->size . ' bytes');
                }
            } else {
                throw new Exception('Wrong file type: '.$upload->type);
            }
        }else {
            throw new Exception('File upload error');
        }
    }


    /**
     * Fill fields in new model from uploaded file
     * @param array $file - uploaded file
     * @param type $name - uploaded filename
     * @return boolean
     */
    protected function _fillFields($file = null, $name = null){
        list($width, $height) = getimagesize(Yii::getPathOfAlias('webroot')."/animations/" . $name);
        $this->original_filename = $name;
        $this->server_filename = $name;
        $this->user_id = (int)Yii::app()->user->id;
        return true;

    }

}

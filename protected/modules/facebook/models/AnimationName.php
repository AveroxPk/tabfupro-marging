<?php

/**
 * This is the model class for table "animation_name".
 *
 * The followings are the available columns in table 'animation_name':
 * @property integer $animation_id
 * @property integer $user_facebook_id
 * @property string $name
 * @property string $original
 *
 * The followings are the available model relations:
 * @property UserFacebook $userFacebook
 * @property Animation $animation
 */
class AnimationName extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'animation_name';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('animation_id, name', 'required'),
			array('animation_id, user_facebook_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>256),
			array('original', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('animation_id, user_facebook_id, name, original', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userFacebook' => array(self::BELONGS_TO, 'UserFacebook', 'user_facebook_id'),
			'animation' => array(self::BELONGS_TO, 'Animation', 'animation_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'animation_id' => 'Animation',
			'user_facebook_id' => 'User Facebook',
			'name' => 'Name',
			'original' => 'Original',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('animation_id',$this->animation_id);
		$criteria->compare('user_facebook_id',$this->user_facebook_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('original',$this->original,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AnimationName the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

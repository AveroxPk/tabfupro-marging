<?php

/**
 * This is the model class for table "user_facebook".
 *
 * The followings are the available columns in table 'user_facebook':
 * @property integer $id
 * @property integer $user_id
 * @property string $facebook_user_id
 * @property string $name
 * @property string $surname
 * @property string $short_token
 * @property string $long_token
 * @property string $add_date
 */
class UserFacebook extends CActiveRecord
{
    
    const DELETED ="1";
    const ACTIVE = "0";
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_facebook';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, facebook_user_id, add_date', 'required'),
            array('user_id', 'numerical', 'integerOnly'=>true),
            array('name, surname, facebook_user_id', 'length', 'max'=>255),
            array('short_token, long_token', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, facebook_user_id, name, surname, short_token, long_token, add_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'fanpages'=>array(self::HAS_MANY, 'UserFanpage', 'user_facebook_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'animations'=>array(self::HAS_MANY, 'Animation', 'user_facebook_id'),
            'favoriteAnimations'=>array(self::HAS_MANY, 'AnimationFavorite', 'user_facebook_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'facebook_user_id' => 'Facebook User',
            'name' => 'Name',
            'surname' => 'Surname',
            'short_token' => 'Short Token',
            'long_token' => 'Long Token',
            'add_date' => 'Add Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('facebook_user_id',$this->facebook_user_id,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('surname',$this->surname,true);
        $criteria->compare('short_token',$this->short_token,true);
        $criteria->compare('long_token',$this->long_token,true);
        $criteria->compare('add_date',$this->add_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserFacebook the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    
    public function delete(){
        $this->deleted = 1;
        if($this->save()){
            return true;
        }else{
            return false;
        }
    }
    
    
    
    public function getFanpagesFromFB($retake=false, $session = null){

        $facebook = Yii::app()->getModule('facebook');
        
        //Check if session present
        if (!$session) {
            //if not create
            $session = Yii::app()->getModule('facebook')->session;

            //if this not help try something elsee
            if(!$session){
                $facebook->setAccessToken($this->long_token);
                $session = $facebook->getSession();
            }
        }
        
        $user_pages_fromfb = $facebook->api('/me/accounts?limit=100');
        $user_pages_fromfb = $user_pages_fromfb ? $user_pages_fromfb->getdecodedBody() : array();
		
        $adminPages = array();

        if (count($user_pages_fromfb) > 0 && $this->id !== null) {
            foreach($user_pages_fromfb['data'] as $user_page_fromfb){ //Foreach fanpage on facebook check if user is administer for it

                //Check if user is administer of current fanpage
                if(in_array('ADMINISTER', $user_page_fromfb['perms'])){

                    $adminPages[] = $user_page_fromfb;
                    //get page access token and not access token for user to this page
                    $page_info = Yii::app()->getModule('facebook')->api("/".$user_page_fromfb['id']."?fields=access_token",array(),null,'GET',true);
                    //check if the record is already in database

                    //if(!isset($page_info["access_token"])){
                    //    throw new Exception("Can't access page info", 403);
                    //}
                    if (isset($page_info["access_token"])) {
                        if(!UserFanpage::model()->exists('user_facebook_id = :ufid AND  facebook_page_id = :fbpid', array(
                            ':ufid'=>$this->id,
                            ':fbpid'=>$user_page_fromfb['id'],
                        ))){
							
                            $newfanpage = new UserFanpage();
                            $newfanpage->user_facebook_id = $this->id;
                            $newfanpage->facebook_page_id = $user_page_fromfb['id'];
                            $newfanpage->access_token = $page_info["access_token"];
                            $newfanpage->save();
                        }else if($retake){
                            $fanpage = UserFanpage::model()->findByAttributes(array(
                               'facebook_page_id'=>$user_page_fromfb['id'],
                               'user_facebook_id'=>$this->id,
                            ));
                            $fanpage->access_token = $page_info['access_token'];
                            $fanpage->deleted = UserFanpage::NOT_DELETED;
                            $fanpage->save();
                        }
                    }
                }
            }
            //Check if user deleted fanpage
			
            $user_pages_fromTF = UserFanpage::model()->getFanpages($this->id, false)->getData();   

            if(count($user_pages_fromTF) != count($adminPages)){
                $idsFapnapgesFromFB = array();
                foreach ($adminPages as $user_page_fromfb){
//					echo "testing".$user_page_fromfb->id; exit;
                    $idsFapnapgesFromFB[] =$user_page_fromfb->id; //Get fanpages ids to array
                }
                foreach($user_pages_fromTF as $user_page_fromTF){
                    if(!in_array($user_page_fromTF->facebook_page_id, $idsFapnapgesFromFB)){
                        $user_page_fromTF->deleted = UserFanpage::DELETED;
                        $user_page_fromTF->save();
                    }
                }
                
                
            }
        }
    }
    
    
    public function getTabsCount(){
        $count = UserTabs::model()->count('user_facebook_id = :ufid AND deleted = \'0\'', array(
            ':ufid'=>$this->id,
        ));
        return $count;
    }
    
    public function getPublishedTabsCount(){
        $count = UserTabs::model()->count('user_facebook_id = :ufid AND published=\'1\' AND deleted = \'0\'', array(
            ':ufid'=>$this->id,
        ));
        return $count;
    }
    
    public function getAccessToken(){
        if(Yii::app()->user->getState('u_f_id')){
            $user = UserFacebook::model()->findByAttributes(array(
                'id'=>Yii::app()->user->u_f_id
            ));
            if($user){
                return $user->long_token;
            }else{
                return false;
            }
        }
    }
    
    
    public function getCountFanpagesNotDeleted($refresh = false){
        if ($refresh) {
            $this->getFanpagesFromFB(true);
        }
        $countFanpages = UserFanpage::model()->count('user_facebook_id = :ufid AND deleted = :notdel', array(
            ':ufid'=>$this->id,
            ':notdel'=>  UserFanpage::NOT_DELETED,
        ));
        return $countFanpages;
    }
}
<?php

/**
 * This is the model class for table "template_field_attribute".
 *
 * The followings are the available columns in table 'template_field_attribute':
 * @property string $id
 * @property string $name
 * @property string $label
 * @property string $data_type
 * @property string $custom_edit_widget_class
 *
 * The followings are the available model relations:
 * @property PageFieldAttribute[] $pageFieldAttributes
 * @property TemplateFieldAttributeValidator[] $templateFieldAttributeValidators
 * @property TemplateField[] $templateFields
 */
class AdFieldAttribute extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ad_field_attribute';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, label, data_type', 'required'),
			array('name, label, data_type, custom_edit_widget_class', 'length', 'max'=>255),
            array('hidden, default_value', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, label, data_type, custom_edit_widget_class', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pageFieldAttributes' => array(self::HAS_MANY, 'ARPageFieldAttribute', 'template_field_attribute_id'),
			'adFieldAttributeValidators' => array(self::HAS_MANY, 'AdFieldAttributeValidator', 'ad_field_attribute_id'),
			'adFields' => array(self::MANY_MANY, 'AdField', 'ad_field_to_ad_field_attribute(ad_field_attribute_id, ad_field_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'label' => 'Label',
			'data_type' => 'Data Type',
			'custom_edit_widget_class' => 'Custom Edit Widget Class',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('data_type',$this->data_type,true);
		$criteria->compare('custom_edit_widget_class',$this->custom_edit_widget_class,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ARTemplateFieldAttribute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

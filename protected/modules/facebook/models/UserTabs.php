<?php

/**
 * This is the model class for table "user_tabs".
 *
 * The followings are the available columns in table 'user_tabs':
 * @property integer $id
 * @property integer $user_fanpage_id
 * @property string $name
 * @property string $favorite
 * @property string $deleted
 * @property string $schedule
 *
 * The followings are the available model relations:
 * @property Page[] $pages
 * @property UserFanpage $userFanpage
 */
class UserTabs extends CActiveRecord
{
    private $analyticsInstance = null;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_tabs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_facebook_id', 'required'),
			array('id, user_facebook_id, user_fanpage_id, application_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('favorite, deleted', 'length', 'max'=>1),
			array('schedule', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_fanpage_id, name, favorite, deleted, schedule', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pages' => array(self::HAS_ONE, 'ARPage', 'user_tabs_id'),
			'userFacebook' => array(self::BELONGS_TO, 'UserFacebook', 'user_facebook_id'),
			'userFanpage' => array(self::BELONGS_TO, 'UserFanpage', 'user_fanpage_id'),
                        'tabIcon' => array(self::BELONGS_TO, 'TabIcon', 'tab_icon_id'),
                        'usersEmail' => array(self::HAS_MANY, 'UserTabsEmails', 'user_tabs_id'),
                        'usersFormsFields' => array(self::HAS_MANY, 'UserTabsInputFields', 'user_tabs_id'),
                        'usersFormsFieldsData' => array(self::HAS_MANY, 'UserTabsInputFieldsData', 'user_tabs_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_fanpage_id' => 'User Fanpage',
			'name' => 'Name',
			'favorite' => 'Favorite',
			'deleted' => 'Deleted',
			'schedule' => 'Schedule',
                        'published' => 'Published',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_fanpage_id',$this->user_fanpage_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('favorite',$this->favorite,true);
		$criteria->compare('deleted',$this->deleted,true);
		$criteria->compare('schedule',$this->schedule,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
    public function searchAllTabsByUserFacebookPublished($userfacebookid = null){
        if($userfacebookid != null){
            $criteria=new CDbCriteria;
            $criteria->condition = 'user_facebook_id = :ufid AND published= \'1\' AND deleted = "0"';
            $criteria->order = 'favorite ASC';
            $criteria->params = array(
                ':ufid'=>$userfacebookid,
            );
            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination' => false
            ));
        }else{
            throw new Exception('User facebook id cannot be null');
        }
    }
        

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserTabs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        public function userCanModify($tabid = null, $userid = null){
            if($userid !== null && $tabid !== null){
                $tab = UserTabs::model()->findByAttributes(array(
                    'id'=>$tabid
                ));
                if($tab){
                    if($tab->userFacebook->user_id == $userid){
                        return true;
                    }
                }
                
            }
            
            return false;
        }
        
        
        public function findByTabIdAndUserId($tabid = null, $userid = null){
            if($userid !== null && $tabid !== null){
                $tab = UserTabs::model()->findByAttributes(array(
                    'id'=>$tabid
                ));
                if($tab){
                    if($tab->userFacebook->user_id == $userid){
                        return $tab;
                    }
                }
                
            }
            
            return null;
        }
        
        
        public function getTemplateId(){
                $page = ARPage::model()->findByAttributes(array(
                    'user_tabs_id'=>$this->id
                ));
                if($page){
                    return $page->template_id;
                }
                
            return false;
        }
        
        /**
         * Get all tabs to scheduled to publish
         * @return type
         */
        public function getScheduledTabs(){
            return $this->findAll('schedule IS NOT NULL AND schedule < "'.date('Y-m-d H:i:s').'" AND published != "1" AND user_fanpage_id !="0"');
            
        }
        
        
        
        /**
         * Publish/Update application (tab) on fanpage
         * @return boolean - true if published, false if not
         */
        
        public function publishOnFB($session = null){
            
            $facebook = Yii::app()->getModule('facebook');
            if (!$session) {
                $session = $facebook->getSession();
            }
			
            if($this->published != 1){
				/*
                $result = $facebook->facebookRequest('POST', '/'.$this->userFanpage->facebook_page_id.'/tabs', array (
                        'app_id' => $this->application_id,
                        'access_token'=>$this->userFanpage->access_token,
                    ), $session);
				*/
//				echo 'api:'.$this->application_id.'<br>';
//				echo 'access_token:'.$this->userFanpage->access_token.'<br>';
//				print_r($session);
//				exit;
                $result = $facebook->api('/'.$this->userFanpage->facebook_page_id.'/tabs', array (
                        'app_id' => $this->application_id,
//                        'access_token'=>$this->userFanpage->access_token,
                    ), $this->userFanpage->access_token,'POST');
					
            }else{
                $result = true;
            }
//			echo "Result: " . $result . "<br />";
            if($result){
                $params = array (     
                    'custom_name' => $this->name,
                    'access_token'=>$this->userFanpage->access_token,
                );
                if($this->getTabIconUrl()){
                    $params['custom_image_url'] = $this->getTabIconUrl();
                }
				/*
                $response = $facebook->facebookRequest(
                    'POST', 
                    '/'.$this->userFanpage->facebook_page_id.'/tabs/app_'.$this->application_id,
                    $params,
                    $session
                );
				*/
                $response = $facebook->facebookRequest(
                    'POST', 
                    '/'.$this->userFanpage->facebook_page_id.'/tabs/app_'.$this->application_id,
                    $params,
                    $this->userFanpage->access_token
                );
				
                if($response){
                    $this->published = 1;
                    $this->schedule = null;
                    if($this->save()){
                        return true;
                    }
                    
                }
            }
            return false;      
        }
        
        public function updateOnFB()
        {
            if (!$this->published) {
                return false;
            }
            $facebook = Yii::app()->getModule('facebook');
            $session = $facebook->session;
            
            $params = array (     
                'custom_name' => $this->name,
                'access_token'=>$this->userFanpage->access_token,
            );
            if($this->getTabIconUrl()){
                $params['custom_image_url'] =$this->getTabIconUrl();
            }

            $response = $facebook->facebookRequest(
                'POST', 
                '/'.$this->userFanpage->facebook_page_id.'/tabs/app_'.$this->application_id,
                $params,
                $session
            );
            if ($response) {
                return true;
            }
            return false;
        }
        
        
        /**
         * Remove application (tab) from fanpage;
         * @return boolean - true if removed false if not
         */
        public function unpublishOnFB(){
            $facebook = Yii::app()->getModule('facebook');
            $response = $facebook->facebookRequest(
                'DELETE',
                "/".$this->userFanpage->facebook_page_id."/tabs/app_".$this->application_id,
                array (),
				$this->userFanpage->access_token
            );

            if($response){
                $this->published = 0;
                $this->application_id = 0;
                $this->schedule = null;
                if($this->save()){
                    return true;
                }
            }
            
            return false;      
        }


    public function getAllTabs($userfacebookid = null)
    {
        if ($userfacebookid === null) return;
        return UserTabs::model()->with('pages')->findAll(array(
            'order' => 'favorite ASC',
            'condition' => 'user_facebook_id = :ufid AND deleted != \'1\'',
            'params' => array(
                ':ufid' => $userfacebookid,
            )
        ));
    }
     //Get User Page wise tab    
     public function getUSerPage($userfacebookid,$page_id)
    {
        if ($userfacebookid === null) return;
        return UserTabs::model()->with('pages')->findAll(array(
            'order' => 'favorite ASC',
            'condition' => 'user_facebook_id = :ufid AND deleted != \'1\'',
            'params' => array(
                ':ufid' => $userfacebookid,
            )
        ));
    }
    public function findByUserPublished($userid){
        return $this->findAllByAttributes(array(
            'user_facebook_id'=>$userid,
            'deleted'=>'0',
            'published'=>'1'
        ));
    }
    
    public function findLastPublished($userid){
        return $this->findByAttributes(array(
            'user_facebook_id'=>$userid,
            'deleted'=>'0',
            'published'=>'1'
        ),
            array('order'=>'id desc',
            )
        );
        
        
    }
    

    public function setDelete(){
        $this->deleted = 1;
        $this->schedule = null;
        if($this->save()){
            return true;
        }
        return false;
    }
        
        
        
        public function findByTabId($tabid =null){
            if($tabid !== null){
                return $this->findByAttributes(array(
                    'id'=>$tabid,
                ));
                
            }else{
                throw new Exception("Tab id not specified");
            }
        }
        
    public function getTabIconUrl()
    {
        return $this->tabIcon !== null ? $this->tabIcon->getImageUrl() : null;
    }
    public function getAnalytics()
    {
        if ($this->analyticsInstance === null) {
            $this->analyticsInstance = new Analytics(Analytics::ANALYTICS_PAGE, $this->id);
        }
        return $this->analyticsInstance;
    }
}




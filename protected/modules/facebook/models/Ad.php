<?php

/**
 * This is the model class for table "template".
 *
 * The followings are the available columns in table 'template':
 * @property string $id
 * @property string $name
 * @property string $content
 * @property string $creation_time
 * @property integer $is_visible
 * @property string  $description
 * @property boolean $isFavourite (By current user)
 *
 * The followings are the available model relations:
 * @property Page[] $pages
 * @property TemplateCategory[] $categories
 * @property TemplateToTemplateField[] $templateToTemplateFields
 */
class Ad extends CActiveRecord
{
    public $isFavourite = false;
    protected function afterFind()
    {
        parent::afterFind();
        $userId = Yii::app()->user->id;
        foreach($this->favourite as $favourite) {
            if ($favourite->user_id !== $userId) continue;

            $this->isFavourite = true;
            break;
        }
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'ad';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('name, creation_time', 'required'),
                array('is_visible', 'numerical', 'integerOnly'=>true),
                array('name', 'length', 'max'=>255),
                array('content', 'safe'),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, name, content, creation_time, is_visible', 'safe', 'on'=>'search'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pages' => array(self::HAS_MANY, 'AdPage', 'ad_id'),
            'categories' => array(self::MANY_MANY, 'AdCategory', 'ad_category_to_ad(ad_id, category_id)'),
            'adToAdFields' => array(self::HAS_MANY, 'AdToAdField', 'ad_id'),
            'favourite' => array(self::HAS_MANY, 'TemplateFavourites', 'template_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'content' => 'Content',
            'creation_time' => 'Creation Time',
            'is_visible' => 'Is Visible',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->join = 'LEFT JOIN template_favourites favourite on (t.id = favourite.template_id and favourite.user_id = :uid)';
        $criteria->params[':uid'] = Yii::app()->user->id;
        $criteria->order = 'favourite.user_id IS NOT NULL desc';

        $criteria->compare('id',$this->id,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('content',$this->content,true);
        $criteria->compare('creation_time',$this->creation_time,true);
        $criteria->compare('is_visible',$this->is_visible);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ARTemplate the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    public function getCategoriesIds()
    {
        $categories = $this->categories;
        $ids = array();

        foreach ($categories as $category)
            $ids[] = $category->id;

        return $ids;
    }

    public function getCategoriesNames()
    {
        $categories = $this->categories;
        $ids = array();

        foreach ($categories as $category)
            $ids[] = $category->name;

        return $ids;
    }
}

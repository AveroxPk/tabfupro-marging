<?php

use Facebook\FacebookRequest;
use Facebook\FacebookSession;

/**
 * This is the model class for table "motion_post".
 *
 * The followings are the available columns in table 'motion_post':
 * @property integer $id
 * @property integer $user_facebook_id
 * @property string $published
 * @property string $schedule
 * @property integer $animation_id
 * @property integer $user_fanpage_id
 * @property string $favorite
 * @property string $title
 * @property string $caption
 * @property string $description
 * @property varchar $facebook_post_id id of added post
 *
 * The followings are the available model relations:
 * @property AnimationLink $animationLink
 * @property UserFacebook $userFacebook
 * @property Animation $animation
 * @property UserFanpage $userFanpage
 */
class MotionPost extends CActiveRecord
{


    const FAVORITE = '1';
    const NOT_FAVORITE = '0';
    const PUBLISHED = '1';
    const NOT_PUBLISHED = '0';
    const TYPE_ANIMATION = '0';
    const TYPE_MOVIE = '1';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'motion_post';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_facebook_id, user_fanpage_id, title, caption, description, message', 'required'),
            array('user_facebook_id, animation_id, user_fanpage_id', 'numerical', 'integerOnly' => true),
            array('published, favorite', 'length', 'max' => 1),
            array('title', 'length', 'max' => 256),
            array('schedule, movie_icon_id', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_facebook_id, published, schedule, animation_id, user_fanpage_id, favorite, title, caption, description, message, type, movie_url', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'userFacebook' => array(self::BELONGS_TO, 'UserFacebook', 'user_facebook_id'),
            'animation' => array(self::BELONGS_TO, 'Animation', 'animation_id'),
            'userFanpage' => array(self::BELONGS_TO, 'UserFanpage', 'user_fanpage_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_facebook_id' => 'User Facebook',
            'published' => 'Published',
            'schedule' => 'Schedule',
            'animation_id' => 'Animation',
            'user_fanpage_id' => 'User Fanpage',
            'favorite' => 'Favorite',
            'title' => 'Title',
            'caption' => 'Caption',
            'description' => 'Description',
            'message' => 'Message',
            'type' => 'Type',
            'movie_url' => 'Movie Url',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_facebook_id', $this->user_facebook_id);
        $criteria->compare('published', $this->published, true);
        $criteria->compare('schedule', $this->schedule, true);
        $criteria->compare('animation_id', $this->animation_id);
        $criteria->compare('user_fanpage_id', $this->user_fanpage_id);
        $criteria->compare('favorite', $this->favorite, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('caption', $this->caption, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('message', $this->message, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MotionPost the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Create motion post from object from motion post editor
     * @param array $object
     * @return boolean
     */
    public function createMotionPost(array $object)
    {
        if ($object["animation"]["type"] == self::TYPE_ANIMATION) {
            $this->animation_id = $object["animation"]["value"];
            $this->type == self::TYPE_ANIMATION;
        } else {
            $this->animation_id = null;
            $this->type = self::TYPE_MOVIE;

            $this->movie_url = $object["animation"]["value"];
            if(isset($object["animation"]["movieThumb"]) && is_numeric($object["animation"]["movieThumb"])){
                $movieThumb = MovieIcon::model()->find('id = :mid AND (user_id is null OR user_id = :uid)', array(
                    ':mid'=>$object["animation"]["movieThumb"],
                    ':uid'=>Yii::app()->user->id
                ));
                if ($movieThumb) {
                    $this->movie_icon_id = $movieThumb->id;
                }
            }
        }
        $this->user_facebook_id = Yii::app()->user->u_f_id;
        $this->user_fanpage_id = $object["page_id"];
        $this->title = $object["title"];
        $this->caption = $object["caption"];
        $this->description = $object["description"];
        $this->message = $object["message"];
        $this->schedule = $object["schedule"]? date('Y-m-d H:i:s', strtotime(preg_replace('/( \(.*)$/','',$object["schedule"]))) : null;
        if ($object["url"] != null) {
            $this->url = $object["url"];
        } else {
            $tab = UserTabs::model()->findByTabId($object["tab_id"]);
            $pagefortab = $tab->getRelated("userFanpage");
            $this->url = Yii::app()->createAbsoluteUrl('/facebook/showtab/mobile/tabid/' . $tab->id);
                //"https://www.facebook.com/" . $tab->userFanpage->facebook_page_id . "?sk=app_" . $tab->application_id . "&ref=ts";
        }
		
        $this->published = 0;
        if ($this->save(false)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @TODO parse motion post object from motionpost editor
     * @param array $objecttoparse
     */

    protected function _parseObject(array $objecttoparse)
    {

    }

    /**
     * Get all motionpost by facebook user
     * @param UserFacebook $user
     * @return boolean
     * @throws Exception
     */

    public function getMotionPostByUser(UserFacebook $user)
    {
        if ($user instanceof UserFacebook) {
            $posts = $this->findAll(array(
                'order' => 'favorite DESC, title',
                'condition' => 'user_facebook_id = :ufid',
                'params' => array(
                    ':ufid' => $user->id,
                )
            ));
            return $posts;
        } else {
            throw new Exception('Wrong parameter given');
        }
        return false;
    }


    /**
     * Return active data provider for motion post by facebook user
     * @param UserFacebook $user
     * @return \CActiveDataProvider
     */
    public function getMotionPostByUserDP(UserFacebook $user)
    {
        if ($user instanceof UserFacebook) {
            $criteria = new CDbCriteria();
            $criteria->condition = "user_facebook_id = :ufid";
            $criteria->params = array(
                ':ufid' => $user->id,
            );
            $criteria->order = 'favorite DESC, title';
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => false,
            ));
        } else {
            throw new Exception("Wrong parameters");
        }
    }

    /**
     * Find all motion post by Facebook Fanpage
     * @param UserFanpage $fanpage
     * @return boolean
     * @throws Exception
     */

    public function getMotionPostByFanpage(UserFanpage $fanpage)
    {
        if ($fanpage instanceof UserFanpage) {
            $posts = $this->findAll(array(
                'order' => 'favorite DESC, title',
                'condition' => 'user_fanpage_id = :ufid',
                'params' => array(
                    ':ufid' => $fanpage->id,
                )
            ));
            return $posts;
        } else {
            throw new Exception('Wrong parameter given');
        }
        return false;
    }

    /**
     * Return Active Data Provider for motionpost by fanpage
     * @param UserFanpage $fanpage
     * @return \CActiveDataProvider
     * @throws Exception
     */
    public function getMotionPostByFanpageDP(UserFanpage $fanpage)
    {
        if ($fanpage instanceof UserFanpage) {
            $criteria = new CDbCriteria();
            $criteria->condition = "user_fanpage_id = :ufid";
            $criteria->params = array(
                ':ufid' => $fanpage->id,
            );
            $criteria->order = 'favorite DESC, title';
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => false,
            ));
        } else {
            throw new Exception("Wrong parameters");
        }
    }


    /**
     * Change value of favorite attribute
     * @return boolean
     */
    public function toogleFavorite()
    {
        if ($this->favorite == self::FAVORITE) {
            $this->favorite = self::NOT_FAVORITE;
        } else {
            $this->favorite = self::FAVORITE;
        }

        if ($this->save()) {
            return true;
        }
        return false;
    }


    /**
     * Check if selected tab  belongs to user or check if link is valid
     *
     * @param type $object
     * @param UserFacebook $user
     * @return boolean
     */

    public function validateStep2($object = null, UserFacebook $user)
    {
        if (isset($object)) {
            if ($object['url'] != null) {
                if (filter_var($object['url'], FILTER_VALIDATE_URL)) {
                    return true;
                }
            } else if ($object['tab_id'] != null) {
                if (UserTabs::model()->exists('id = :id AND user_facebook_id = :ufid', array(
                    ':id' => $object['tab_id'],
                    ':ufid' => $user->id,
                ))
                ) {

                    return true;
                }
            }
        }
        $object["error_url"] = true;
        $object["error_motion_post"] = true;
        Yii::app()->user->setState('motionpost', $object);
        Yii::app()->getController()->redirect(array('/facebook/motionpost/editorstep1'));
        return false;
    }


    /**
     * Check if user has access to animation
     * @param type $object
     * @param UserFacebook $user
     * @return boolean
     */
    public function validateStep3($object = null, UserFacebook $user)
    {
        if ($this->validateStep2($object, $user)) {
            if ($object["animation"]["type"] == MotionPost::TYPE_ANIMATION) {
                $animation = Animation::model()->exists('id = :aid AND (public = \'1\' OR user_facebook_id =:ufid)', array(
                        ':ufid' => $user->id,
                        ':aid' => $object['animation']["value"])
                );
                if ($animation) {
                    return true;
                }
            } else {
                $pinfo = pathinfo($object["animation"]["value"]);
                if ((isset($pinfo['extension']) && in_array($pinfo['extension'], Animation::$allowedExts)) ||
                    preg_match("/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/", $object["animation"]["value"]) > 0 ||
                    preg_match("/vimeo.com/", $object["animation"]["value"]) > 0) {
                    if(isset($object["animation"]["movieThumb"]) AND is_numeric($object["animation"]["movieThumb"])){
                        $movieThumb = MovieIcon::model()->exists('id = :mid AND (user_id is null OR user_id = :uid)', array(
                            ':mid'=>$object["animation"]["movieThumb"],
                            ':uid'=>$user->user->user_id
                        ));
                        if ($movieThumb) {
                            return true;
                        }
                    }else{
                        return true;
                    }
                }
            }
        }
        $object["error_animation"] = true;
        Yii::app()->user->setState('motionpost', $object);
        Yii::app()->getController()->redirect(array('/facebook/motionpost/editorstep2'));
        return false;
    }


    /**
     * validate inputs: caption, title, message, description AND if fanpage belongs to user.
     * @param type $object
     * @param UserFacebook $user
     * @return boolean
     */

    public function validateStep4($object = null, UserFacebook $user)
    {
        if ($this->validateStep3($object, $user)) {
            if ($object["caption"] != null && $object["title"] != null && $object["message"] != null && $object["description"] != null) {
                $object["error_caption"] = false;
                $object["error_title"] = false;
                $object["error_message"] = false;
                $object["error_description"] = false;

                $fanpage = UserFanpage::model()->exists('id = :id AND user_facebook_id = :ufid AND deleted = :notdel', array(
                    ':ufid' => $user->id,
                    ':id' => $object['page_id'],
                    ':notdel' => UserFanpage::NOT_DELETED,
                ));
                if ($fanpage) {
                    return true;
                } else {
                    $object["error_fanpage"] = true;
                }
            } else {
                if ($object["caption"] == null) {
                    $object["error_caption"] = true;
                } else {
                    $object["error_caption"] = false;
                }
                if ($object["title"] == null) {
                    $object["error_title"] = true;
                } else {
                    $object["error_title"] = false;
                }
                if ($object["message"] == null) {
                    $object["error_message"] = true;
                } else {
                    $object["error_message"] = false;
                }
                if ($object["description"] == null) {
                    $object["error_description"] = true;
                } else {
                    $object["error_description"] = false;
                }
            }
        }
        Yii::app()->user->setState('motionpost', $object);
        Yii::app()->getController()->redirect(array('/facebook/motionpost/editorstep3'));
        return false;
    }


    /**
     * This is just to make sure everything is fine
     */

    public function validateStep5($object = null, UserFacebook $user)
    {
        if ($this->validateStep4($object, $user)) {
            return true;
        }
        Yii::app()->user->setState('motionpost', $object);
        Yii::app()->getController()->redirect(array('/facebook/motionpost/editorstep4'));
        return false;
    }


    /*
     * Publish motion post on facebook
     */
    public function publish()
    {
        $user = $this->userFacebook;
   
        $facebook = Yii::app()->getModule('facebook');
            
        if($this->movie_url !=''){
            $link = $this->movie_url;
        }else{
            $link = Yii::app()->createAbsoluteUrl('facebook/showanimation/index', array('id' => $this->id, 'fortest' => time()));
        }
        
       if(!is_null($this->animation)){
		   $height = 253;
		   $width = 470;
           $source = "https://www.tabfu.online/GIFPlayer/GIFPlayer.swf?url="."https://www.tabfu.online//animations/".$this->animation->file_drive."&h=253&w=470";
       }else{
		   $height = ''; 
		   $width = '';
           $source  = '';
       }
/*	   
	   echo '<pre>';
               $k = array (
                    'message' => $this->message,
                    'link' => $link,
                    'from' => $this->userFanpage->facebook_page_id,
                    'to' => $this->userFanpage->facebook_page_id,
                    'name' => $this->title,
                    'caption' => $this->caption,
                    'descritpion' => $this->description,
                    'source'=>$source,
                    //'picture'=>$picture,
                    'access_token' => $this->userFanpage->access_token,
                );	   
	   
	   
	   print_r($k); exit;
*/
        $graphObject = $facebook->facebookRequest(
                'POST',
                '/'.$this->userFanpage->facebook_page_id.'/feed',
                array (
                    'message' => $this->message,
                    'link' => $link,
                    'from' => $this->userFanpage->facebook_page_id,
                    'to' => $this->userFanpage->facebook_page_id,
                    'name' => $this->title,
                    'caption' => $this->caption,
                    'descritpion' => $this->description,
                    'source'=>$source,
					'height' => $height,
					'width' => $width,
                    //'picture'=>$picture,
                    'access_token' => $this->userFanpage->access_token,
                ),
                $this->userFanpage->access_token

            );
          
        $graphObject = $graphObject ? $graphObject->getdecodedBody() : null;
        if ($graphObject) {
            $this->facebook_post_id = $graphObject["id"];
            $this->published = self::PUBLISHED;
            $this->schedule = null;
            if ($this->save(false)) {
                return true;
            }

        }

        return false;
    }


    /**
     * Find Motion post by id
     * @param type $id
     * @return boolean
     */
    public function findById($id = null)
    {
        if ($id !== null) {
            return $this->findByAttributes(array(
                'id' => $id,
            ));
        } else {
            return false;
        }
    }


    /**
     * Return count of all motionpost by user
     * @param UserFacebook $user
     * @return type
     * @throws Exception
     */
    public function getCountAllMotionPosts(UserFacebook $user)
    {
        if ($user instanceof UserFacebook) {
            $count = $this->count('user_facebook_id = :ufid', array(
                ':ufid' => $user->id,
            ));
            return $count;
        } else {
            throw new Exception("Wrong parameter");
        }
    }

    /**
     * Return count of all motionpost by user, which are scheduled
     * @param UserFacebook $user
     * @return type
     * @throws Exception
     */
    public function getCountScheduledMotionPosts(UserFacebook $user)
    {
        if ($user instanceof UserFacebook) {
            $count = $this->count('user_facebook_id = :ufid and published != :publish', array(
                ':ufid' => $user->id,
                ':publish' => self::PUBLISHED,
            ));
            return $count;
        } else {
            throw new Exception("Wrong parameter");
        }
    }


    /**
     * Check if user has access to motion post
     * @param UserFacebook $user
     * @return boolean
     * @throws Exception
     */
    public function userHaveAccess(UserFacebook $user)
    {
        if ($user instanceof UserFacebook) {
            if ($this->userFacebook->id == $user->id) {
                return true;
            }
        } else {
            throw new Exception("User is not instance of proper model");
        }
        return false;
    }


    public function findAllToPublish()
    {
        return $this->findAll('published = :notpublished AND schedule < "'.date('Y-m-d H:i:s').'"', array(
            ':notpublished' => self::NOT_PUBLISHED,
        ));
    }

    public static function getMovieThumbnail($url = null, $movieThumbId = null)
    {
        if($movieThumbId !== null){
            $movieThumb = MovieIcon::model()->find('id = :mid', array(
                ':mid'=>$movieThumbId
            ));
            if ($movieThumb) {
                return $movieThumb->getImageUrl();
            }
        }


        if ($url !== null) {
            if (strpos($url, "youtube")) {
                try {
                    $id = explode("v=", $url);
                    if(count($id) > 1){
                        $id = $id[1];
                    }else{
                        $id = $id[0];
                    }
                    $id = explode("/v/", $id);
                    if(count($id) > 1){
                        $id = $id[1];
                    }else{
                        $id = $id[0];
                    }
                    $id = explode("#", $id);
                    $id = $id[0];
                    $id = explode("?", $id);
                    $id = $id[0];
                    $id = trim($id, '=');
                    $id = trim($id, '/');
                    $thumbnail = "https://img.youtube.com/vi/$id/0.jpg";
                    return $thumbnail;
                } catch (Exception $ex) {

                }
            }
            elseif(strpos($url, 'vimeo')){
                $id = explode('/', $url);
                $thumJson = file_get_contents('http://vimeo.com/api/v2/video/'.$id[count($id)-1].'.json');
                $thumbObj = json_decode($thumJson);
                return current($thumbObj)->thumbnail_medium;

            }
            else {
                return $url;
            }
        }


        return null;
    }
    
    
    public static function getMovieThumbnailWidth($url = null, $movieThumbId = null)
    {
        $image = self::getMovieThumbnail($url, $movieThumbId);
        if($image){
            $info = getimagesize($image);
            return $info[0];
        }
        return null;
    }


    public static function getMovieThumbnailHeight($url = null, $movieThumbId = null)
    {
        $image = self::getMovieThumbnail($url, $movieThumbId);
        if($image){
            $info = getimagesize($image);
            return $info[1];
        }
        return null;
    }

    public function getAnalytics()
    {
        return new Analytics(Analytics::ANALYTICS_MOTION_POST, $this->facebook_post_id, $this->id);
    }
}

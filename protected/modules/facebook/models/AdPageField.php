<?php

/**
 * This is the model class for table "page_field".
 *
 * The followings are the available columns in table 'page_field':
 * @property string $id
 * @property string $template_field_id
 * @property string $page_version_id
 * @property integer $is_used
 *
 * The followings are the available model relations:
 * @property PageVersion $pageVersion
 * @property TemplateField $templateField
 * @property PageFieldAttribute[] $pageFieldAttributes
 */
class AdPageField extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ad_page_field';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ad_field_id, page_version_id', 'required'),
			array('is_used', 'numerical', 'integerOnly'=>true),
			array('ad_field_id, page_version_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ad_field_id, page_version_id, is_used', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pageVersion' => array(self::BELONGS_TO, 'AdPageVersion', 'page_version_id'),
			'adField' => array(self::BELONGS_TO, 'AdField', 'ad_field_id'),
			'pageFieldAttributes' => array(self::HAS_MANY, 'AdPageFieldAttribute', 'page_field_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'template_field_id' => 'Template Field',
			'page_version_id' => 'Page Version',
			'is_used' => 'Is Used',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ad_field_id',$this->ad_field_id,true);
		$criteria->compare('page_version_id',$this->page_version_id,true);
		$criteria->compare('is_used',$this->is_used);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ARPageField the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

class CronController extends Controller{

    private $facebook;
    
    public function filters() {
        return array(
            'accessControl', 
        );
    }
    
    public function accessRules() {
        return array(
            array(
                'allow',
                'users' => array('*'),
                ),
            );
    }
    
    
    public function actionCronTabs(){
		
        //Get tabs for sheduled publish
        $tabs = UserTabs::model()->getScheduledTabs();
        foreach($tabs as $tab){

            //Init facebook
            $facebook = $this->_initFacebook($tab->userFacebook->long_token);

            //Get free application if for tab.
            $appid = $tab->userFanpage->getFreeAppId($facebook);

            //if app id is not number than probably user used all avaible application and script should leave this tab
            //and move on
            if(!is_numeric($appid)){
                continue; 
            }

            $tab->application_id = $appid;
            $tab->save();


            //Publish template changes (Template module-related code)
            Yii::app()->getModule('template');
            $pageManager = new PageManager($tab);
            $pageQuery = new PageQuery();
            $page = $pageQuery->getPageFromRepositoryByManagingTab($tab);

            $pageManager->publishLastPageVersion($page, new PageRenderer());
            //End publish template changes
            $response = $tab->publishOnFB($facebook->getSession());




            //$session = new FacebookSession($tab->userFacebook->long_token);
            
            //try{
            //
            /*}catch(FacebookApiException $e){ 
                //Probably access token expired so we retake them.
                $facebookuser = $page->getRelated('userFacebook');
                $facebookuser->getFanpagesFromFB(true, $session);
                try{
                    $response = $tab->publishOnFB($session);

                }catch(FacebookApiException $e){ 

                }
            }*/
        }
		echo 'Done';

    }
    
    public function actionTestAnalytics()
    {
        echo '<pre>';
        $motionpost = MotionPost::model()->findByAttributes(array('id'=>1));
        $userFacebook = UserFacebook::model()->findByAttributes(array('id'=>2));
        
        $facebookQuery = new QueryFacebook($motionpost->facebook_post_id, Analytics::ANALYTICS_MOTION_POST, $userFacebook->long_token, 92);
        var_dump($facebookQuery->getImpressionsCount());
        echo '</pre>';
    }

    public function actionQueryAnalytics() {
        $criteria           = new CDbCriteria();
        $criteria->order    = 'user_facebook_id';

        $posts = MotionPost::model()->findAll($criteria);

        $lastUserId = null;

        foreach ($posts as $post) {
            if ($post->facebook_post_id == null) continue;
            //$post->userFacebook->long_token
            $analytics = new Analytics(Analytics::ANALYTICS_MOTION_POST, $post->facebook_post_id, $post->id);
            try {
                $analytics->getFreshData(true);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            sleep(0.3);
        }
    }

    private function _initFacebook($sessionAccessToken = null)
    {


        if (!empty($sessionAccessToken)) {
            $facebook = Yii::app()->getModule('facebook');
            $facebook->setAccessToken($sessionAccessToken);

            //$facebook->setAccessToken($sessionAccessToken);
        }
        return $facebook;
    }

    public function actionCronMotionPosts(){
        $motionposts = MotionPost::model()->findAllToPublish();
        foreach($motionposts as $motionpost){
            var_dump($motionpost->schedule);
            /* @var $motionpost MotionPost */
			try{
				$motionpost->publish();
			}catch(\Exception $e){
				echo $motionpost->id.': '.$e->getMessage();
			}
        }
    }
    
}
<?php
use Facebook\Facebook;
use Facebook\Authentication\AccessToken;
class FacebookController extends Controller
{

    public $accounts;
	public $modules;
    
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            
            array('deny',
                'users'=>array('?'),
                
            ),          
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function init()
    {
    }
    
    public function actionError()
    {
        $this->render('error');
    }
    
    public function actionIndex()
    {
		$limit = 1;
		
		$agency = Yii::app()->user->agency;
		
		if($agency)
			$limit = $agency['profile_limit'];
		
        /* Get current selected facebook account so it is on first position */
        $accountSelected = $accounts = new CActiveDataProvider('UserFacebook', array(
            'criteria' => array(
                'condition' => 'user_id = :uid AND deleted = "0" AND id = :ufid',
                'params' => array(
                    ':uid' => Yii::app()->user->id,
                    ':ufid' => Yii::app()->user->getState('u_f_id'),
                ),
            ),
            'pagination' => false
        ));
		
        switch(Yii::app()->user->getState('u_f_id')):
            case null:
                $criteria = array(
                    'criteria' => array(
                        'condition' => 'user_id = :uid AND deleted = "0"',
                        'params' => array(
                            ':uid' => Yii::app()->user->id,
                        ),
						'limit' => $limit
                    ),
                    'pagination' => false
                );
                break;
            default:
                $criteria = array(
                    'criteria' => array(
                        'condition' => 'user_id = :uid AND deleted = "0" AND id != :ufid',
                        'params' => array(
                            ':uid' => Yii::app()->user->id,
                            ':ufid' => Yii::app()->user->getState('u_f_id'),
                        ),
						'limit' => $limit-1
                    ),
                    'pagination' => false
                );                
        endswitch;
		
		
        /* Select all facebook accounts for currently logged user */
        $accountsNotSelected = new CActiveDataProvider('UserFacebook',$criteria);
        /* Merge results */
        $records = array_merge($accountSelected->data, $accountsNotSelected->data);
        
        $accounts = new CActiveDataProvider('UserFacebook',array(
            'data'=>$records,
        ));
		
		// var_dump($accounts);
		//exit;
		
        $module = $this->module;
		$fbAccount = false;
        if(Yii::app()->user->getState('u_f_id')){
		 
			// Code By Akif
            Yii::import('application.modules.membership.models.*');
			$userFbId = Yii::app()->user->getState('u_f_id');
			$userFb = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
			$userFb = UserFacebook::model()->find(array(
				'select'=>'facebook_user_id',
				'condition'=>'user_id=:user_id',
				'params'=>array(':user_id'=>Yii::app()->user->id),
			));
			$isDeleted = $userFb['deleted'];
			
			//$type = Payment::model()->findByPk(4009);
			//$type = Payment::model()->findAll('user_id=:user_id', array(':user_id'=>$userFb['user_id']));
			$type = Payment::model()->find(array(
				'select'=>'type',
				'condition'=>'user_id=:user_id',
				'params'=>array(':user_id'=>Yii::app()->user->id),
			));
			
			if (isset($userFb->facebook_user_id) && $type->type === "individual") {
				$fbAccount = true;
			}
			if ($isDeleted == 1) {
				$fbAccount = false;
			}
			// Code By Akif

            $selectedaccount = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
            $this->_switchAccount($selectedaccount);
        }else{
          
            $accountsdata = $accounts->getData();
//            print_r($accountsdata);
//            exit;
            if(count($accountsdata)>0){
                $selectedaccount = $accountsdata[0];
              $this->_switchAccount($selectedaccount);
            }else{
                $selectedaccount = FALSE;
            }
        }
		$this->accounts = $accounts;
		$this->modules = $module;
        $this->render('index', array('accounts' => $accounts, 'fbAccount' => $fbAccount, 'type' => $type, 'module'=>$module, 'selectedaccount' => $selectedaccount, 'homePage'=>true));
    }
    
    
    public function actionClicklike(){
        $user = User::model()->findByPk(Yii::app()->user->id);
        $user->liked = 1;
        $user->update();
        
    }
    
    public function actionLike()
	{
        
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap-social.css');
        $this->render('like', array());
    }

        /**
     * Connect logged user with a facebook account
     * @param int $token If $token = 1 then add new token to database
     */
    public function actionAddAccount($token = 0, $message=true)
    {
        $facebookUser = new UserFacebook;
        Yii::app()->user->setState('access_token', '');
        $this->module->setAccessToken($token);
        $user = $this->module->getUser();
        // Select all information about current user
        if ($user) {
//            $this->module->fbUser = $this->module->api('/me?fields=id,name,first_name,last_name,picture',null,true);
            $this->module->fbUser = $this->module->api('/me?fields=id,name,first_name,last_name,picture',array(),null,'GET',true);
            $user_facebook = UserFacebook::model()->find( 'facebook_user_id = :fbid', array(':fbid' => $this->module->fbUser['id']));
            if (!$user_facebook) {
                // Prepare model for UserFacebook    
                $facebookUser->user_id = Yii::app()->user->id;
                $facebookUser->facebook_user_id = $this->module->fbUser['id'];
                $facebookUser->name = $this->module->fbUser['first_name'];
                $facebookUser->surname = $this->module->fbUser['last_name'];
                $facebookUser->add_date    = date("Y-m-d H:i:s");
                $facebookUser->short_token = $this->module->session->getValue();
                $facebookUser->long_token  =  $this->module->getLongLivedToken();

                Yii::app()->user->setState('access_token', $facebookUser->long_token);
                // Save it in DB
                if ($facebookUser->save()) {
                    
                    Yii::app()->user->setState('u_f_id', $facebookUser->id);
                    $facebookUser->getFanpagesFromFB();
                    if($message){
                        Yii::app()->user->setFlash('notice', 'User added.');
                    }
                    $this->redirect(array('/facebook'));
                } else {
                    throw new CException('Could not save in database. ' . print_r($facebookUser->getErrors(), TRUE));
                }
                
            }
            else {
                $user_facebook->user_id = Yii::app()->user->id;
                $user_facebook->deleted = 0;
//                $user_facebook->short_token = $this->module->session->getValue();
//				$user_facebook->long_token = $this->module->getLongLivedToken();
				
                if ($user_facebook->save()) {
                    Yii::app()->user->setState('access_token', $token);
                    Yii::app()->user->setState('u_f_id', $user_facebook->id);
                    $user_facebook->getFanpagesFromFB();
                    if($message){
                        Yii::app()->user->setFlash('notice', 'User added.');
                    }
                    $this->redirect(array('/facebook'));
                }
            }
        }
        if($message){
            Yii::app()->user->setFlash('notice', 'Something went wrong');
        }
        $this->redirect(array('/facebook'));
    }

    /**
     * Disconnect account ( Yii side )
     * @param string $token The short token selected from database
     */
    public function actionDisconnect($token = '')
	{ 
        if(empty($token)){
            throw new CHttpException(500, 'Token can not be empty!');
        }
        
        $userFb = UserFacebook::model()->find('user_id = :uid AND short_token = :stk', array(
            ':uid' => Yii::app()->user->id,
            ':stk' => $token
        ));
        
        // User found in DB
        if ($userFb !== NULL){
            $this->module->setAccessToken($userFb->short_token);
            
            // If it is current user, logout
//            if( $userFb->short_token == $this->module->session->getToken() ) {
            if( $userFb->short_token == $this->module->session->getValue() ) {
                $this->module->session = null;
                Yii::app()->user->setState('access_token', '');
                Yii::app()->user->setState('u_f_id', '');
            }
            
            // Delete from DB
            if($userFb->delete()){
                $newUserFb = UserFacebook::model()->find('user_id = :uid AND deleted = "0"', array(
                    ':uid' => Yii::app()->user->id,
                ));
                if($newUserFb != null){
                    
                    Yii::app()->user->setState('access_token', $newUserFb->long_token);
                    Yii::app()->user->setState('u_f_id', $newUserFb->id);
                    $this->module->setAccessToken($newUserFb->long_token);
                    $newUserFb->getFanpagesFromFB(true);
                }
            }
            
            // Redirect to main page
            Yii::app()->user->setFlash('notice', 'User removed');
            $this->redirect(array('/facebook'));
            
        } else {
            Yii::app()->user->setFlash('notice', 'Token not found in our database.');
            $this->redirect(array('/facebook'));
        }
    }
    
    /**
     * Switch to different account
     * @param string $token Short token 
     */
     public function actionSwitchAccount($token = '')
     {
        if (empty($token)) {
            throw new CHttpException(500, 'Token can not be empty!');
        }
         
        $userFb = UserFacebook::model()->find('user_id = :uid AND short_token = :stk', array(
            ':uid' => Yii::app()->user->id,
            ':stk' => $token
        ));
//		echo '<pre>';
//        var_dump($userFb);
 //       exit;
        if ($userFb !== NULL) {
            //set all informations needed in application
            $this->_switchAccount($userFb);
            $this->redirect(array('/facebook/facebook/index'));
        }
        
     }
     
     protected function _switchAccount($userFb)
     {
        Yii::app()->user->setState('access_token', $userFb->long_token);
        Yii::app()->user->setState('u_f_id', $userFb->id);
        $this->module->setAccessToken($userFb->long_token);
        $userFb->getFanpagesFromFB(true);
     }


     public function actionSession()
     {
//		$fb = $this->module->getFbObj();
		session_start();
        $fb = new Facebook([
            'app_id' => Yii::app()->params['FacebookAppId'],
            'app_secret' => Yii::app()->params['FacebookSecret'],
            'default_graph_version' => 'v2.5',
            'persistent_data_handler' => 'session'
        ]);
        $helper = $fb->getRedirectLoginHelper();
        $helper->getLoginUrl(Yii::app()->createAbsoluteUrl('/facebook/facebook/session'));
		
        try {
            $accessToken = $helper->getAccessToken();
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Response Exception'.$e;
            exit();
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
			echo 'SDK Exception'.$e;
            exit();
        }catch(\Exception $e)
        {
            echo "
            <div id=\"hberror\">
                <p>
                    <span>An Error Occurred</span>
                    <span id=\"xError\" onclick=\"hideError()\">X</span>
                </p>
            </div>";
        }
        
        if (! isset($accessToken)) {
			echo "A facebook error occured: Couldn't get Access Token";
            exit();
        }

//		$this->module->setAccessToken($accessToken);
		$long_token = $this->module->getLongLivedToken($accessToken);
		Yii::app()->user->setState('access_token', $long_token);
		
		$this->module->fbUser = $this->module->api('/me?fields=id,name,link,email,first_name,gender,last_name,timezone,verified,picture',array(),null,'GET',true);
          
            $userFb = UserFacebook::model()->find('facebook_user_id = :fid', array(
                ':fid' => $this->module->fbUser['id']
            ));
            if (!$userFb) {
                $userFb = new UserFacebook();
            }
//			echo Yii::app()->user->id.'<br>'.$this->module->fbUser['id'].'<br>'.$this->module->fbUser['first_name'].'<br>'.$this->module->fbUser['last_name'].'<br>'.$accessToken.'<br>'.$long_token;
//			exit;
            $userFb->user_id            = Yii::app()->user->id;
			$userFb->facebook_user_id   = $this->module->fbUser['id'];
			$userFb->name               = $this->module->fbUser['first_name'];
			$userFb->surname            = $this->module->fbUser['last_name'];
            $userFb->add_date           = date("Y-m-d H:i:s");
            $userFb->short_token        = $accessToken;
            $userFb->long_token         = $long_token;
            $userFb->save();
			
			
			$this->redirect(array('/facebook/facebook/index'));
		
     }
     

}
<?php

class ShowtabController extends Controller
{

    public function actionIndex($id = null)
    {
        error_reporting(E_ALL);
        if ($id == null) {
            echo "Sorry";
            return false;
        }
		
        $facebook = new Facebook(array(
            'appId' => $id,
            'secret' => Yii::app()->params['FacebookTabs'][$id],
            'allowSignedRequest' => false
        ));
		
        $signed = $facebook->getSignedRequest();
		
        $pages = UserFanpage::model()->findAllByAttributes(array(
            'facebook_page_id' => $signed['page']['id']
        ));
		
        if (!$pages) return;
        foreach($pages as $pageSuspect){
            // we need a valid page suspect (the one which is connected to a real user)
            if (!$pageSuspect->userFacebook || !$pageSuspect->userFacebook->user || $pageSuspect->userFacebook->deleted || 
                $pageSuspect->userFacebook->user->status == User::STATUS_DELETED || $pageSuspect->userFacebook->user->status == User::STATUS_INACTIVE) {
                continue;
            }
            $tab = UserTabs::model()->findByAttributes(array(
                'application_id' => $id,
                'user_fanpage_id' => $pageSuspect->id,
                'published' => '1'
            ));
            if($tab !== null){
                $page = $pageSuspect;
                break;
            }
        }
        $pageQuery = new PageQuery;
        try {
            $page = $pageQuery->getPageFromRepositoryByManagingTabInPublishedVersion($tab);
            Analytics::track(Analytics::ANALYTICS_PAGE, PageAnalytics::ACTION_CLICK, $page->getId());
            //Render this page
            $renderer = new PageRenderer;
            $renderer->adminMode = false;
            $renderer->params = $this->_provideRendererParams($signed, $tab, $page);

            echo $renderer->renderPage($page, false, $tab);
            return true;
        } catch (Exception $ex) {
		return $ex;
		die("Ex: $ex; Page: $page; Tab: $tab");
        }
    }

    public function actionMobile($tabid = null, $noredir = null)
    {
        if ($tabid == null) return;

        Analytics::track(Analytics::ANALYTICS_PAGE, PageAnalytics::ACTION_CLICK, $tabid);
        $tab = UserTabs::model()->findByAttributes(array(
            'id' => $tabid,
        ));
        if (!$tab) return;
        
        Yii::import('application.vendor.Mobile_Detect');
        $mobileDetector = new Mobile_Detect();
        if (!$noredir && !$mobileDetector->isMobile() && !$mobileDetector->isTablet()) {
            $this->redirect('https://www.facebook.com/' . $tab->userFanpage->facebook_page_id . '?sk=app_' . $tab->application_id);
        }
        
        $facebook = new Facebook(array(
            'appId' => $tab->application_id,
            'secret' => Yii::app()->params['FacebookTabs'][$tab->application_id],
            'allowSignedRequest' => true
        ));
        $signed = $facebook->getSignedRequest();

        $pageQuery = new PageQuery;
        try {
            $page = $pageQuery->getPageFromRepositoryByManagingTabInPublishedVersion($tab);
            //Render this page
            $renderer = new PageRenderer;
            $renderer->adminMode = false;
            $renderer->params = $this->_provideRendererParams($signed, $tab, $page);
            echo $renderer->renderPage($page, false, $tab);
            return true;
        } catch (Exception $ex) {
            echo "Sorry, some error occured";
        }
    }

    public function actionLaptop($tabid = null, $noredir = null)
    {
        if ($tabid == null) return;

        Analytics::track(Analytics::ANALYTICS_PAGE, PageAnalytics::ACTION_CLICK, $tabid);
        $tab = UserTabs::model()->findByAttributes(array(
            'id' => $tabid,
        ));
        if (!$tab) return;
 /*       
        Yii::import('application.vendor.Mobile_Detect');
        $mobileDetector = new Mobile_Detect();
        if (!$noredir && !$mobileDetector->isMobile() && !$mobileDetector->isTablet()) {
            $this->redirect('https://www.facebook.com/' . $tab->userFanpage->facebook_page_id . '?sk=app_' . $tab->application_id);
        }
 */       
        $facebook = new Facebook(array(
            'appId' => $tab->application_id,
            'secret' => Yii::app()->params['FacebookTabs'][$tab->application_id],
            'allowSignedRequest' => true
        ));
        $signed = $facebook->getSignedRequest();

        $pageQuery = new PageQuery;
        try {
            $page = $pageQuery->getPageFromRepositoryByManagingTabInPublishedVersion($tab);
            //Render this page
            $renderer = new PageRenderer;
            $renderer->adminMode = false;
            $renderer->params = $this->_provideRendererParams($signed, $tab, $page);
            echo $renderer->renderPage($page, false, $tab);
            return true;
        } catch (Exception $ex) {
            echo "Sorry, some error occured";
        }
    }
	
	
	
    private function _provideRendererParams($facebookSignedRequest, $tab, $page)
    {
        $params = array(
            'facebook' => $facebookSignedRequest,
            'appId' => Yii::app()->params['FacebookAppId'], //$tab->application_id,
            'tabId' => $tab->id,
            'tabName' => $tab->name,
            'gatesConfig' => $page->getGatesConfig()
        );

        if (isset($facebookSignedRequest['page']['id'])) {
            $params['fanpageUrl'] = 'https://www.facebook.com/' . $facebookSignedRequest['page']['id'];
        }

        return $params;
    }
    
    public function actionRedirectToLastPublished(){
        
    
        $tab = UserTabs::model()->findLastPublished(Yii::app()->user->u_f_id);
        $this->redirect(Yii::app()->createAbsoluteUrl('/facebook/showtab/mobile/tabid/' . $tab->id));
        //$this->redirect('https://www.facebook.com/'.$tab->userFanpage->facebook_page_id.'?sk=app_'.$tab->application_id.'&ref=ts');
        
    }

}


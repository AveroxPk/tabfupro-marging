<?php
error_reporting(E_ALL);
class MotionpostController extends Controller{
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('?'),
                
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function init()
	{
		if(!Yii::app()->user->Motionpost)
		{
            $this->redirect(array('/membership'));			
		}		
	}
    
    
    public function actionIndex(){
	
		if(empty(Yii::app()->user->u_f_id) || !$this->module->getUser()){
            Yii::app()->user->setFlash('notice', 'Please select a account');
            $this->redirect(array('/facebook'));
        }
	
//        if($this->module->getUser()){
            $userfacebook = UserFacebook::model()->findByPK(Yii::app()->user->u_f_id);
//        }
		
        $fanpage = UserFanpage::model()->getFirstFanPage();
        $tabs = UserTabs::model()->findByUserPublished(Yii::app()->user->u_f_id);
        $fanpages = UserFanpage::model()->getFanpages(Yii::app()->user->u_f_id);
        $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
        $motionposts = MotionPost::model()->getMotionPostByUserDP($user);
        return $this->render('index', array(
            'motionposts'=>$motionposts, 
            'tabs'=>$tabs,
            'fanpage'=>$fanpage,
            'module'=>$this->module,
            'fanpages'=>$fanpages,
            'userfacebook'=>$userfacebook,
        ));
    }    
    
    
    
    public function actionShowposts($pageid){
        switch ($pageid){
            case is_numeric($pageid):
                $fanpage = UserFanpage::model()->getFanpageByPageIdAndUser($pageid, Yii::app()->user->u_f_id);
                $posts = MotionPost::model()->getMotionPostByFanpageDP($fanpage);
                break;
            default:
                $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
                $posts = MotionPost::model()->getMotionPostByUserDP($user);
                break;
        }
        echo $this->renderPartial('partial/_posts', array(
            'motionposts'=>$posts,
            //'userfacebook'=>$user,
        ));
        return true;
        
    }
    
    
    public function actionUploadAnimation(){
        $this->render('uploadanimation');
    }
    
    public function actionDeleteAnimationAjax($id)
    {
        try {
            $animation = Animation::model()->findByPk($id);
            if ($animation->user_facebook_id == (int)Yii::app()->user->getState('u_f_id')) {
                $animation->deleteFile();
                $animation->delete();
            }
        }catch (Exception $ex){

        }
    }
    
    public function actionDeleteMovieAjax($id)
    {
        $animation = MovieIcon::model()->findByPk($id);
        if ($animation->user_id == (int)Yii::app()->user->id) {
            $animation->deleteFile();
            $animation->delete();
        }
    }
    
    public function actionUploadAnimationAjax(){
        
        $file = CUploadedFile::getInstanceByName('file');
        
        try{
            $animation = new Animation;
            if($animation->createAnimation($file)){
                if(substr($animation->file_drive,-3) == 'gif'){
                    $type = 2;
                }else{
                    $type = 0;
                }
                echo json_encode(array(
                    "status"=>"notice", 
                    "message"=>"Image saved", 
                    "name"=>$animation->getName(), 
                    "file"=>$animation->file_drive, 
                    "id"=>$animation->id,
                    "type"=>$type,
                ));
            }
            
        }catch(Exception $e){
            echo json_encode(array("status"=>"warning", "message"=>$e->getMessage()));
        }
        return true;
    }


    public function actionUploadMovieThumbAjax(){

        $file = CUploadedFile::getInstanceByName('file');

        try{
            $movieIcon = new MovieIcon();
            if($movieIcon->createCreateMovieIcon($file)){
                echo json_encode(array(
                    "status"=>"notice",
                    "message"=>"Image saved",
                    "file"=>$movieIcon->server_filename,
                    "id"=>$movieIcon->id,
                    "type"=>1,
                ));
            }

        }catch(Exception $e){
            echo json_encode(array("status"=>"warning", "message"=>$e->getMessage()));
        }
        return true;
    }


    
    public function actionTooglefavorite(){
        if($_POST['id'] != null){
            $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
            $animation = Animation::model()->findByPk($_POST['id']);
            if($animation->toogleFavoriteByUser($user)){
                echo json_encode(array("status"=>"ok"));
            }else{
                echo json_encode(array("status"=>"no", "message"=>"Seems that this is not your animation"));
            }
        }else{
            echo json_encode(array("status"=>"nope"));
        }
    }
    
    public function actionEditorStep1(){
        if(isset($_POST["motionpost"]) && $_POST['motionpost'] != null){
            
            $motionpost = $_POST['motionpost'];
            
        }else{
            
            $motionpost = Yii::app()->user->getState('motionpost');
            Yii::app()->user->setState('motionpost', false);
        }
        $fanpage = UserFanpage::model()->getFirstFanPage();
        $tabs = UserTabs::model()->searchAllTabsByUserFacebookPublished(Yii::app()->user->u_f_id);
        
        echo $this->renderPartial('modals/step1', array(
            'tabs'=>$tabs,
            'fanpage'=>$fanpage,
            'motionpost'=>$motionpost,
            'fanpages'=>  UserFanpage::model()->getFanpages(Yii::app()->user->u_f_id),
            'module'=>$this->module,
        ));
        
        return true;
    }
    
    public function actionEditorStep2(){
        
        if(isset($_POST["motionpost"]) && $_POST['motionpost'] != null){
            $motionpost = $_POST['motionpost'];
        }else{
            $motionpost = Yii::app()->user->getState('motionpost');
            Yii::app()->user->setState('motionpost', false);
        }
        $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
        if(MotionPost::model()->validateStep2($motionpost, $user)){
            $motionpost["error_url"]=false;
            $motionpost["error_motion_post"]=false;

            $newModel = new Animation();
            $newMovieIcon = new MovieIcon();
            $images = Animation::model()->getAllImagesForUser($user);
            $gifs = Animation::model()->getAllGifsForUser($user);
            $movieIcon = MovieIcon::model()->findAllForUser($user);

            echo $this->renderPartial('modals/step2', array(
                'model'=>$newModel,
                'modelIcon'=>$newMovieIcon,
                'motionpost'=>  $motionpost,
                'movieIcons'=>$movieIcon,
                'animation'=>$images,
                'gifs'=>$gifs,
            ));

            return true;
        }else{
            return false;
        }
    }
    
    
    public function actionEditorStep3(){
        
        if(isset($_POST["motionpost"]) && $_POST['motionpost'] != null){
            $motionpost = $_POST['motionpost'];
        }else{
            $motionpost = Yii::app()->user->getState('motionpost');
            Yii::app()->user->setState('motionpost', false);
        }
        $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
        if(MotionPost::model()->validateStep3($motionpost, $user)){
            $motionpost["error_url"]=false;
            $motionpost["error_motion_post"]=false;
            $motionpost["error_animation"]=false;
            $animation = null;
            if($motionpost["animation"]["type"] == MotionPost::TYPE_ANIMATION){
                $animation = Animation::model()->findByPk($motionpost["animation"]["value"]);
            }
            echo $this->renderPartial('modals/step3', array(
                'fanpages'=>  UserFanpage::model()->getFanpages(Yii::app()->user->u_f_id),
                'fanpage'=> UserFanpage::model()->getFirstFanPage(),
                'module'=>$this->module,
                'motionpost'=>  $motionpost,
                'animation'=>$animation,
            ));

            return true;
        }else{
            return false;
        }
    }
    
    public function actionEditorStep4(){
        if(isset($_POST["motionpost"]) && $_POST['motionpost'] != null){
            $motionpost = $_POST['motionpost'];
        }else{
            $motionpost = Yii::app()->user->getState('motionpost');
            Yii::app()->user->setState('motionpost', false);
        }
        $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
        if(MotionPost::model()->validateStep4($motionpost, $user)){
            $motionpost["error_url"]=false;
            $motionpost["error_motion_post"]=false;
            $motionpost["error_animation"]=false;
            $motionpost["error_fanpage"]=false;
            $motionpost["error_caption"]=false; 
            $motionpost["error_title"]=false; 
            $motionpost["error_message"]=false; 
            $motionpost["error_description"]=false;
            
            if($motionpost["url"]!=null){
                $openGraph = OpenGraph::fetch($motionpost["url"]);
                $title = $openGraph->title;
                $image = $openGraph->image;
                $url = $motionpost["url"];
                
            }else{
                $tab = UserTabs::model()->find('id = :id AND user_facebook_id = :ufid', array(
                        ':id'=>$motionpost['tab_id'],
                        ':ufid'=>$user->id,
                    ));
                $title= $tab->name;
                $image = $tab->getTabIconUrl();
                $url = Yii::app()->createAbsoluteUrl('/facebook/showtab/mobile/tabid/' . $tab->id);
                    //'https://www.facebook.com/'.$tab->userFanpage->facebook_page_id.'?sk=app_'.$tab->application_id;
                if (!$image || strlen($image) <= 0) {
                    $image = Yii::app()->getBaseUrl(true).'/template_resources/'.$tab->getTemplateId().'/preview.jpg';
                }
            }
            
            $url .= (strpos($url, '?') !== false ? '&' : '?').'ref=ts';
            
            $fanpage = UserFanpage::model()->findByPk($motionpost['page_id']);
            $animation = Animation::model()->findByPk($motionpost["animation"]["value"]);
			
            echo $this->renderPartial('modals/step4', array(
                'fanpage'=>$fanpage,
                'datetoshow'=>date('Y-m-d H:i', strtotime(preg_replace('/( \(.*)$/','',$motionpost["schedule"])) ),
                'title'=>$title,
                'image'=>$image,
                'url'=>$url,
                'module'=>$this->module,
                'motionpost'=>  $motionpost,
                'animation'=>$animation,
                
            ));

            return true;
        }else{
            return false;
        }
    }
    
    public function actionEditorStep5(){
        if(isset($_POST["motionpost"]) && $_POST['motionpost'] != null){
            $motionpost = $_POST['motionpost'];
        }else{
            $motionpost = Yii::app()->user->getState('motionpost');
            Yii::app()->user->setState('motionpost', false);
        }
        $error = null;
        $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
        if(MotionPost::model()->validateStep5($motionpost, $user)){
            //Check if this is edit of all post or new one
            if(is_array($motionpost) && (!isset($motionpost["id"]) || empty($motionpost["id"]))){
                $newMotionpost = new MotionPost();
            }else{
                $newMotionpost =  MotionPost::model()->findById($motionpost["id"]);
                if($newMotionpost != null && !$newMotionpost->userHaveAccess($user)){
                    
                    $error = true;
                }
            }
            $publishednow = false;
            if($error == null && $newMotionpost->createMotionPost($motionpost) ){
                if($motionpost["schedule"] == null){
                    $publishednow = true;
                    if(!$newMotionpost->publish()){
                        $error = true;
                    }
                }
            }else{
                
                $error = true;
            }
            if($error){
                Yii::app()->user->setState('motionpost', $motionpost);
                $this->redirect(array('/facebook/motionpost/editorstep4'));
            }
            
            
            $motionpost["error_url"]=false;
            $motionpost["error_motion_post"]=false;
            $motionpost["error_animation"]=false;
            $motionpost["error_fanpage"]=false;
            $motionpost["error_caption"]=false; 
            $motionpost["error_title"]=false; 
            $motionpost["error_message"]=false; 
            $motionpost["error_description"]=false;
            $motionpost["animation"]["type"]="";
            $motionpost["animation"]["value"]="";
            $motionpost["page_id"]="";
            $motionpost["url"]="";
            $motionpost["caption"]="";
            $motionpost["title"]="";
            $motionpost["message"]="";
            $motionpost["description"]="";
            $motionpost["schedule"]="";
            $motionpost["id"]="";
            
            
            echo $this->renderPartial('modals/step5', array(
                'motionpost'=>  $motionpost,
                'newMotionpost'=>$newMotionpost,
                'publishednow'=>$publishednow,
                
                //'url'=>$url,
            ));

            return true;
        }else{
            return false;
        }
    }
    
    
    public function actionCopymotionpost(){
        $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
        $motionpost = MotionPost::model()->findById($_POST["motionpost"]);
        if($motionpost->userHaveAccess($user)){
            $motionpostObj["url"]=$motionpost->url;
            $motionpostObj["page_id"]=$motionpost->user_fanpage_id;
            $motionpostObj["animation"]["type"]=$motionpost->type;
            $motionpostObj["animation"]["movieThumb"]=$motionpost->movie_icon_id;
            if($motionpost->type == MotionPost::TYPE_ANIMATION){
                $motionpostObj["animation"]["value"]=$motionpost->animation_id;
            }else{
                
                $motionpostObj["animation"]["value"]=$motionpost->movie_url;
            }
            $motionpostObj["title"]=$motionpost->title;
            $motionpostObj["caption"]=$motionpost->caption;
            $motionpostObj["description"]=$motionpost->description;
            $motionpostObj["message"]=$motionpost->message;
            if($motionpost->schedule == "0000-00-00 00:00:00" OR $motionpost->schedule == null){
                $motionpostObj["schedule"]="";
            }else{
                $motionpostObj["schedule"]=$motionpost->schedule;
            }
            
            Yii::app()->user->setState('motionpost', $motionpostObj);
            $this->redirect(array('/facebook/motionpost/editorstep4'));
        }
    }
    
    public function actionEditmotionpost(){
        $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
        $motionpost = MotionPost::model()->findById($_POST["motionpost"]);
        if($motionpost->userHaveAccess($user)){
            $motionpostObj["url"]=$motionpost->url;
            $motionpostObj["page_id"]=$motionpost->user_fanpage_id;
            $motionpostObj["animation"]["type"]=$motionpost->type;
            $motionpostObj["animation"]["movieThumb"]=$motionpost->movie_icon_id;
            if($motionpost->type == MotionPost::TYPE_ANIMATION){
                $motionpostObj["animation"]["value"]=$motionpost->animation_id;
            }else{
                
                $motionpostObj["animation"]["value"]=$motionpost->movie_url;
            }
            $motionpostObj["title"]=$motionpost->title;
            $motionpostObj["caption"]=$motionpost->caption;
            $motionpostObj["description"]=$motionpost->description;
            $motionpostObj["message"]=$motionpost->message;
            $motionpostObj["id"]=$motionpost->id;
            if($motionpost->schedule == "0000-00-00 00:00:00" OR $motionpost->schedule == null){
                $motionpostObj["schedule"]="";
            }else{
                $motionpostObj["schedule"]=$motionpost->schedule;
            }
            
            Yii::app()->user->setState('motionpost', $motionpostObj);
            $this->redirect(array('/facebook/motionpost/editorstep4'));
        }
    }
    
    public function actionChangeanimationname(){
        $id = $_POST["animation_id"];
        $name = $_POST["name"];
        if($id != null && $name != null){
            $animation = Animation::model()->findByPk($id);
            $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
            
            if($animation->userHaveAccess($user)){
                if($animation->changeName($name, $user)){
                    return true;
                }
            }

        }
        return false;
    }
    
    
    public function actionToogleFavoriteMotionPost(){
        if($_POST['id'] && is_numeric($_POST["id"])){
            $motionpost = MotionPost::model()->findById($_POST["id"]);
            $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
            if($motionpost->userHaveAccess($user)){
                if($motionpost->toogleFavorite()){
                    return true;
                }
            }
        }
        return false;
        
        
        
    }
    
    
    
    public function actionDeletemotionpost(){
        if(isset($_POST["id"]) && is_numeric($_POST["id"])){
            $motionpost = MotionPost::model()->findById($_POST["id"]);
            $user = UserFacebook::model()->findByPk(Yii::app()->user->u_f_id);
            if($motionpost->userHaveAccess($user)){
                if($motionpost->delete()){
                    return true;
                }
            }
        }
        return false;
    }
}

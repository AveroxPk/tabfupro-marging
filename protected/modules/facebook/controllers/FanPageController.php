<?php

class FanPageController extends Controller
{

    
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('?'),
                
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function init()
    {
        
    }


    public function actionIndex($pageid = null)
    {
      
        if (empty(Yii::app()->user->u_f_id) || !$this->module->getUser()) {
            Yii::app()->user->setFlash('notice', 'Please select a account');
            $this->redirect(array('/facebook'));
        }
		/*
        if (!$this->module->getUser()) {
            Yii::app()->user->setFlash('notice', 'Please select a account');
            $this->redirect(array('/facebook'));
        }
        */
		
        $categoriesTemplates = TemplateCategory::model()->findAll();
        $userfacebook = UserFacebook::model()->findByPK(Yii::app()->user->u_f_id);
    
        if (!Yii::app()->request->isAjaxRequest) {
            $userfacebook->getFanpagesFromFB(true);
        }
 
        $fanpages = UserFanpage::model()->getFanpages(Yii::app()->user->u_f_id);
        $favoritetabs = UserTabs::model()->getAllTabs(Yii::app()->user->u_f_id);
        
		// Code By Akif Start
		Yii::import('application.modules.membership.models.*');
		Yii::import('application.modules.membership.user.*');
		
		$userFbId = Yii::app()->user->u_f_id;
		$userFb = UserFacebook::model()->findByPk($userFbId);
			
		$fbAccount = false;
		$type = Payment::model()->find(array(
			'select'=>'type',
			'condition'=>'user_id=:user_id',
			'params'=>array(':user_id'=>$userFb['user_id']),
		));
		
		$sDate = date("Y-m-d H:i:s", mktime(00, 00, 00, date(m), 01, date(Y)));
		$eDate = date("Y-m-d H:i:s", mktime(00, 00, 00, date(m), 30, date(Y)));
		$one = 1;
		$tabCount = UserTabs::model()->findAll(array(
			'select'=>'id',
			'condition'=>'user_facebook_id=:user_facebook_id AND (date_added BETWEEN :startDate AND :endDate) AND published=:one',
			'params'=>array(':user_facebook_id'=>Yii::app()->user->u_f_id, ':startDate' => $sDate, ':endDate' => $eDate, ':one'=>$one),
		));
		
		/*$servername = "localhost";
		$username = "tabfu_user";
		$password = "jAD67ako1iweRiNEH6ya5oP7S3rO13";

		// Create connection
		$conn = new mysqli($servername, $username, $password, "tabfu");
			
		$sql = "SELECT * FROM user_tabs WHERE user_facebook_id = $userFbId AND published = '1' AND (date_added < '$eDate' AND date_added > '$sDate')";
		$result = $conn->query($sql);
		
		$tabCount = $result->num_rows;*/
		
		// Check connection
		$date_added = "Connected successfully";
		
		if ($type->type === "individual" && count($tabCount) > 2) {
			$fbAccount = true;
		}
		// Code By Akif End
        $this->render('index', array(
            'fanpages' => $fanpages,
            'module' => $this->module,
            'userfacebook' => $userfacebook,
            //'pageid'=>$pageid,
            'allTabs' => $favoritetabs,
            'templateCategories'=>$categoriesTemplates,
			'fbAccount' => $fbAccount,
			'tabCount' => count($tabCount)
        ));
    }

    public function actionShowtabs($pageid = null)
    {
        if ($pageid === null) return;
        if ($pageid == 'ALL') {
            $tabs = UserTabs::model()->getAllTabs(Yii::app()->user->u_f_id);
            return $this->renderPartial('_tabs', array('tabs' => $tabs, 'facebook' => $this->module));
        }
       // echo $pageid;
        $page = UserFanpage::model()
            ->find('user_facebook_id = :ufid AND facebook_page_id = :fpid ', array(
                ':ufid' => Yii::app()->user->u_f_id,
                ':fpid' => $pageid,
            ));

        if (!count($page)) return false;

        $tabs = $page->getRelated('tabs', false, array(
            'condition' => 'deleted != \'1\'',
            'order' => 'favorite ASC'
        ));
		
		
        return $this->renderPartial('_tabs', array('pageid' => $pageid,'tabs'=>$tabs, 'page' => $page, 'facebook' => $this->module));
    }
    
    public function actionCreateTab( $templateid = null, $fanpageid = null){
		
		// Code By Akif Start
		Yii::import('application.modules.membership.models.*');
		Yii::import('application.modules.membership.user.*');
		
		$userFbId = Yii::app()->user->u_f_id;
		$userFb = UserFacebook::model()->findByPk($userFbId);
				
		$fbAccount = false;
		$type = Payment::model()->find(array(
			'select'=>'type',
			'condition'=>'user_id=:user_id',
			'params'=>array(':user_id'=>$userFb['user_id']),
		));
		
		$sDate = date("Y-m-d H:i:s", mktime(00, 00, 00, date(m), 01, date(Y)));
		$eDate = date("Y-m-d H:i:s", mktime(00, 00, 00, date(m), 30, date(Y)));
		$one = 1;
		
		$tabCount = UserTabs::model()->findAll(array(
			'select'=>'id',
			'condition'=>'user_facebook_id=:user_facebook_id AND published=:one AND date_added BETWEEN :startDate AND :endDate',
			'params'=>array(':user_facebook_id'=>Yii::app()->user->u_f_id, ':startDate' => $sDate, ':endDate' => $eDate, ':one' => $one),
		));
		
		// Check connection
		$date_added = "Connected successfully";
		
		if ($type->type === "individual" && count($tabCount) > 2) {
			$fbAccount = true;
		}
		// Code By Akif End
		
       if($templateid != null && $fbAccount != true){
            $template = ARTemplate::model()->findByPk($templateid);
			
			$category = strtolower($template->getCategoryName()); // geting the category name from DB
//			$otoArr = Yii::app()->params['packages']['oto1']; // getting the oto1 features from config

			if($category != 'professional' || Yii::app()->user->Professionaltabs==true)
			{
				if(count($template) == 1){
					$tab = new UserTabs();
					
					if ($fanpageid !== null) {
						//Security checks
						$fanpage = UserFanpage::model()->findByAttributes(array(
							'facebook_page_id' => $fanpageid,
							'user_facebook_id' => Yii::app()->user->u_f_id
						));
						if ($fanpage === null) {
							throw new CHttpException(404, 'Page not found.');
						}
						
						$tab->user_fanpage_id = $fanpage->id;
					}
					
					$tab->user_facebook_id = Yii::app()->user->u_f_id;                

					if($tab->save(true)){
						$pm = new PageManager($tab);
						$nePage = $pm->createNewPageFromTemplate($template, $tab->name);
						echo json_encode(array(
							'error' => 0,
							'tabId' => $tab->id
						));

					}else{
						echo json_encode(array(
							'error' => 1,
							'status' => 'warning',
							'message' => "Couldn't create tab. Please try again later."
						));
						//echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
					}
					
				}else{
					echo json_encode(array(
						'error' => 1,
						'status' => 'warning',
						'message' => "Couldn't create tab. Invalid template."
					));
				}				
			}else{
				echo json_encode(array(
					'error' => 1,
					'status' => 'notice',
					'message' => ucfirst($category) . " templates not allowed, Please select template from other categories or upgrade your package!"
				));
			}				
        }        
    }
    
    public function actionPublishtab($tabid = null, $pageid=null){
				
        if($tabid !== null && $pageid!==null){
            $tab = UserTabs::model()->findByAttributes(array(
                'id' => $tabid
            ));
            //$page changed to $fanpage due to name collision with Page model object from template module
            $fanpage = UserFanpage::model()->findByAttributes(array(
                'facebook_page_id'=>$pageid,
                'user_facebook_id'=>Yii::app()->user->u_f_id,
            ));
			
            // Code By Akif Start
		Yii::import('application.modules.membership.models.*');
		Yii::import('application.modules.membership.user.*');
		
		$userFbId = Yii::app()->user->u_f_id;
		$userFb = UserFacebook::model()->findByPk($userFbId);
				
		$fbAccount = false;
		$type = Payment::model()->find(array(
			'select'=>'type',
			'condition'=>'user_id=:user_id',
			'params'=>array(':user_id'=>$userFb['user_id']),
		));
		
		$sDate = date("Y-m-d H:i:s", mktime(00, 00, 00, date(m), 01, date(Y)));
		$eDate = date("Y-m-d H:i:s", mktime(00, 00, 00, date(m), 30, date(Y)));
		$one = 1;
		$tabCount = UserTabs::model()->findAll(array(
			'select'=>'id',
			'condition'=>'user_facebook_id=:user_facebook_id AND published=:one AND date_added BETWEEN :startDate AND :endDate',
			'params'=>array(':user_facebook_id'=>Yii::app()->user->u_f_id, ':startDate' => $sDate, ':endDate' => $eDate, ':one' => $one),
		));
				
		if ($type->type === "individual" && count($tabCount) > 2) {
			$fbAccount = true;
		}
		
		if ($fbAccount == true) {
			return;
		}
		// Code By Akif End
		
            if($tab->published != 1){
                if($fanpage->user_facebook_id == Yii::app()->user->u_f_id && UserTabs::model()->userCanModify($tabid, Yii::app()->user->id)){
                    
                    //Publish template changes (Template module-related code)

                    Yii::app()->getModule('template');
                    $pageManager = new PageManager($tab);
                    $pageQuery = new PageQuery();
                    $page = $pageQuery->getPageFromRepositoryByManagingTab($tab);

                    $pageManager->publishLastPageVersion($page, new PageRenderer());

                    //End publish template changes
					
                    $appid = $fanpage->getFreeAppId();

                    if(!is_numeric($appid)){
                        echo json_encode(array('status'=>'warning', 'message'=>'Max number of tabs reached'));
                        return true; 
                    }
                    $tab->user_fanpage_id = $fanpage->id;
                    $tab->application_id = $appid;
                    $tab->save();
					
                    try{
                        $response = $tab->publishOnFB();
                        if($response) {
                            echo json_encode(array(
                                'status'=>'notice',
                                'message' => 'Tab published',
                                'success' => 1,
                                'pageid' => $tab->userFanpage->facebook_page_id
                            ));
                            return true;  
                        }
                    }catch(Exception $e){ 
                        Yii::log("\n\n\n".($this->user ? CVarDumper::dumpAsString($this->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'facebook.fanPage.publishTab');
                        //Probably access token expired so we retake them.
                        $facebookuser = $fanpage->getRelated('userFacebook');
                        $facebookuser->getFanpagesFromFB(true);
                        try{
                            $response = $tab->publishOnFB();
							
                            if($response){
                                echo json_encode(array(
                                    'status'=>'notice',
                                    'message'=>'Tab published',
                                    'success' => 1,
                                    'pageid' => $tab->userFanpage->facebook_page_id
                                ));
                                return true;  
                            }
                            
                        }catch(Exception $e){ 
                            Yii::log("\n\n\n".($this->user ? CVarDumper::dumpAsString($this->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'facebook.fanPage.publishTab');
                            echo json_encode(array('status'=>'warning', 'message'=>$e->getMessage()));
                            return true;
                        }
                    }
                }else{
                    echo json_encode(array('status'=>'warning', 'message'=>'Seems like this is not your tab'));
                    return true;
                }
                
  
            }else{
            
                echo json_encode(array('status'=>'warning', 'message'=>'Tab already published'));
                return true;
            }
        }
        echo json_encode(array('status'=>'warning', 'message'=>'Some error occurred'));
        return false;
    }
    
    
    public function actionRepublishtab($tabid = null){
        if($tabid !== null){
            $tab = UserTabs::model()->findByAttributes(array(
                'id' => $tabid
            ));
            
            if($tab->published == 1){                
                if($tab->userFanpage->user_facebook_id == Yii::app()->user->u_f_id && UserTabs::model()->userCanModify($tabid, Yii::app()->user->id)){
                    
                    try{
                        $response = $tab->updateOnFB();
                        if($response) {
                            echo json_encode(array(
                                'status'=>'notice',
                                'message' => 'Tab published',
                                'success' => 1,
                                'pageid' => $tab->userFanpage->facebook_page_id
                            ));
                            return true;  
                        }
                    }catch(Exception $e){ 
                        Yii::log("\n\n\n".($this->user ? CVarDumper::dumpAsString($this->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'facebook.fanPage.republishTab');
                        //Probably access token expired so we retake them.
                        $facebookuser = $tab->userFanpage->getRelated('userFacebook');
                        $facebookuser->getFanpagesFromFB(true);
                        try{
                            $response = $tab->updateOnFB();
                            if($response){
                                echo json_encode(array(
                                    'status'=>'notice',
                                    'message'=>'Tab published',
                                    'success' => 1,
                                    'pageid' => $tab->userFanpage->facebook_page_id
                                ));
                                return true;  
                            }
                            
                        }catch(Exception $e){ 
                            Yii::log("\n\n\n".($this->user ? CVarDumper::dumpAsString($this->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'facebook.fanPage.republishTab');
                            echo json_encode(array('status'=>'warning', 'message'=>$e->getMessage()));
                            return true;
                        }
                    }
                }else{
                    echo json_encode(array('status'=>'warning', 'message'=>'Seems like this is not your tab'));
                    return true;
                }
                
  
            }else{
            
                echo json_encode(array('status'=>'warning', 'message'=>'Tab not published'));
                return true;
            }
        }
        echo json_encode(array('status'=>'warning', 'message'=>'Some error occurred'));
        return false;
    }
    
    
    public function actionUnpublishtab($tabid = null){
        if($tabid !== null){
            $tab = UserTabs::model()->findByAttributes(array(
                'id' => $tabid
            ));
            $fanpage = $tab->getRelated('userFanpage');
            if(count($fanpage) == 1 && $fanpage->user_facebook_id == Yii::app()->user->u_f_id){
                try{
                    $response = $tab->unpublishOnFB();
                    if($response){
                        echo json_encode(array('status'=>'notice', 'message'=>'Tab unpublished'));
                        return true;  
                    }
                    echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
                    return true;
                }catch(FacebookApiException $e){
                    Yii::log("\n\n\n".($this->user ? CVarDumper::dumpAsString($this->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'facebook.fanPage.unpublishTab');
                     //Probably access token expired so we retake them.
                    $facebookuser = $fanpage->getRelated('userFacebook');
                    $facebookuser->getFanpagesFromFB(true);
                    //echo json_encode(array('status'=>'warning', 'message'=>$e->getMessage()));
                    try{
                        $response = $tab->unpublishOnFB();
                        if($response){
                            echo json_encode(array('status'=>'notice', 'message'=>'Tab unpublished'));
                            return true;  
                        }
                        
                    }catch(FacebookApiException $e){
                        Yii::log("\n\n\n".($this->user ? CVarDumper::dumpAsString($this->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'facebook.fanPage.unpublishTab');
                        echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
                        return true;

                    }    
                }
                
  
            }
        }
        return false;
    }
    
    
    
    
    public function actionAddTabToFavorite($tabid = null){
        if($tabid !== null && UserTabs::model()->userCanModify($tabid, Yii::app()->user->id)){
            $tab = UserTabs::model()->findByAttributes(array(
                    "id"=>$tabid));
            $tab->favorite = 1;
            if($tab->save()){
                echo json_encode(array("status"=>"notice", "message"=>"Added to favorites"));
                return true;
            }
            
        }
        echo json_encode(array("status"=>"warning", "message"=>"Some error occured"));
        return true;
    }
    
    public function actionRemoveTabfromFavorite($tabid = null){
        if($tabid !== null && UserTabs::model()->userCanModify($tabid, Yii::app()->user->id)){
            $tab = UserTabs::model()->findByAttributes(array(
                    "id"=>$tabid));
            $tab->favorite = 0;
            if($tab->save()){
                echo json_encode(array("status"=>"notice", "message"=>"Removed from favorites"));
                return true;
            }
            
        }
        echo json_encode(array("status"=>"warning", "message"=>"Some error occured"));
        return true;
    }
    
    
    public function actionSchedule($tabid = null, $date = null){
        if($tabid != null AND $date != null){
            $tab = UserTabs::model()->findByAttributes(array(
                'id'=>$tabid
            ));
			
			 list($date,$dateInfo) = explode('(',$date);  // in date contain standered date & in dateInfo contain country info e.g (Pakistan Standerd time)
			 
            if($tab && UserTabs::model()->userCanModify($tabid, Yii::app()->user->id)){
                $tab->schedule = date('Y-m-d H:i:s', strtotime($date) );
                if($tab->save()){
                    if($tab->userFanpage){
                        echo json_encode(array("status"=>"ok", "message"=>"Tab scheduled"));
                        return true;
                    }else{
                        echo json_encode(array("status"=>"ok", "message"=>"Tab scheduled. Remember to set fanpage for this tab in editor."));
                        return true;
                    }
                }
                
            }else{
                echo json_encode(array("status"=>"warning", "message"=>"Seems that you can't edit this tab"));
                return true;
            }
        }
        echo json_encode(array("status"=>"warning", "message"=>"Some error"));
        return true;
    }
    
    
    
    public function actionCopytab($tabid){
        if($tabid != null ){
            $tabold = UserTabs::model()->findByAttributes(array(
                'id' => $tabid
            ));
            if($tabold->userCanModify($tabold->id, Yii::app()->user->id)){
                $tab = new UserTabs();
                $tab->user_fanpage_id = isset($tabold->user_fanpage_id) ? $tabold->user_fanpage_id : 0;
                $tab->user_facebook_id = $tabold->user_facebook_id;
                $tab->name = $tabold->name . ' (copy)';
                if($tab->save()){

                    try{
                        $pq = new PageQuery();
                        $oldpage = $pq->getPageFromRepositoryByManagingTab($tabold);

                        $pm = new PageManager($tab);
                        $newPage = $pm->createPageFromPage($oldpage, $tab->id);
                        echo json_encode(array('status'=>'notice', 'message'=>'Tab Copied'));
                    }catch(Exception $e){
                        $tab->delete();
                    }
                }else{
                    echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
                }
            }else{
                 echo json_encode(array('status'=>'warning', 'message'=>'Seems that you can\'t copy this tab'));
            }
            
        }
    }
    
    
    public function actionDeleteTab($tabid= null){
        if($tabid != null){
            $tab = UserTabs::model()->findByAttributes(array(
                'id' => $tabid
            ));
            if(count($tab)==1 && UserTabs::model()->userCanModify($tabid, Yii::app()->user->id)){
                if($tab->published == 1){
                    if($tab->setDelete()){
                    echo json_encode(array('status'=>'notice', 'message'=>'Tab deleted'));
                    return true;
                    }else{
                        echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
                        return false;
                    }
                }else{
                    if($tab->delete()){
                    echo json_encode(array('status'=>'notice', 'message'=>'Tab deleted'));
                    return true;
                    }else{
                        echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
                        return false;
                    }
                }
                
            }else{
                echo json_encode(array('status'=>'warning', 'message'=>'Seems like this is not your tab.'));
                return false;
            }
        }
    }
    
    public function actionChangeTabName($id)
    {
        if (!isset($_POST['name']) || !is_string($_POST['name'])) {
            throw new CException(500, "Name hasn't been submitted.");
        }
        
        $tab = UserTabs::model()->findByAttributes(array(
            'id' => $id
        ));
        
        if (count($tab)==1 && UserTabs::model()->userCanModify($id, Yii::app()->user->id)) {
            $tab->name = $_POST['name'];
            if (!$tab->update(array('name'))) {
                exit(json_encode(array(
                    'status' => 'notice', 
                    'message' => "Tab name has been updated",
                    'success' => 1
                )));
            }
            echo json_encode(array(
                'status' => 'warning', 
                'message' => "Couldn't update template name. Please try again later.", 
            ));
        }
    }
    
    public function actionChangeAdName($id)
    {
        if (!isset($_POST['name']) || !is_string($_POST['name'])) {
            throw new CException(500, "Name hasn't been submitted.");
        }
        
        $tab = UserAds::model()->findByAttributes(array(
            'id' => $id
        ));
        
        if (count($tab)==1 && UserAds::model()->userCanModify($id, Yii::app()->user->id)) {
            $tab->name = $_POST['name'];
            if (!$tab->save(array('name'))) {
                exit(json_encode(array(
                    'status' => 'notice', 
                    'message' => "Ad name has been updated",
                    'success' => 1
                )));
            }
            
        }
        echo json_encode(array(
            'status' => 'warning', 
            'message' => "Couldn't update ad name. Please try again later.", 
        ));
    }
    
    public function actionExportCSV($tabId){
        $tab = UserTabs::model()->findByTabIdAndUserId($tabId, Yii::app()->user->id);
        if($tab !== null){
            
            header('Content-Type: application/csv');
            header('Content-Disposition: attachement; filename="'.$tab->name.'.csv";');

            $f = fopen('php://output', 'w'); 
			
//			$headers = ['Name', 'Email', 'Address', 'City', 'State', 'Zip', 'Phone'];
			$headers = [];
			
			$counter = 0;
			$data = [];
			
			if(!empty($tab->getRelated('usersFormsFields')))
			{
				
				foreach($tab->getRelated('usersFormsFields') as $tabFields)
				{
					$headers[] = $tabFields->field_name;				
				}
				
				fputcsv($f, $headers);
				
				foreach($tab->getRelated('usersFormsFieldsData') as $tabFieldsData){
				
					if($counter<count($headers))
					{
						$data[] = $tabFieldsData->field_value;
						$counter +=1;
					}
					
					if($counter==count($headers))
					{
						
						fputcsv($f, $data);
						
						$data = [];
						$counter = 0;
					}
				}
			}
			else
			{
				foreach($tab->getRelated('usersEmail') as $userEmail){
					fputcsv($f, array($userEmail->email, $userEmail->name, $userEmail->lastname));
				}
			}
			
        }else{
            echo "test";
            die;
            $this->redirect('facebook/fanPage/index'); 
        }
    }
 
    
//    public function actionTest(){
//        var_dump(UserFacebook::model()->getAccessToken());
//        $this->module->api('/me/feed', 'post', array(
//            'type' => 'link',
//            'link' => ' https://customapp.procreative.eu/GIFPlayer/test5.html', // put the html file's location here
//        ));
//       
////        swf_definebitmap(1,"http://www.thisiscolossal.com/wp-content/uploads/2013/01/4.gif"); 
////        swf_startshape(2); 
////         swf_shapelinesolid(0.1,0.1,0.1,1,0.2); 
////         swf_shapefillbitmapclip(1); 
////         swf_shapearc(50,50,100,0,360); 
////        swf_endshape(2); 
////        swf_placeobject(2,10); 
//
//        //http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072
//    }
}
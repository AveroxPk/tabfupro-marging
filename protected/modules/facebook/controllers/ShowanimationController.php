<?php

class ShowanimationController extends Controller
{
    public function actionIndex($id)
    {
        if ($id === null) return;

        $motionpost = MotionPost::model()->findById($id);
        $animation = null;
        if ($motionpost->type == MotionPost::TYPE_ANIMATION) {
            $animation = $motionpost->animation;
        }

        Analytics::track(Analytics::ANALYTICS_MOTION_POST, MotionPostAnalytics::ACTION_CLICK, $id);

        $this->renderPartial('index', array(
            'motionpost' => $motionpost,
            'animation' => $animation,
        ));

    }
}
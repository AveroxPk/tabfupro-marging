<?php
error_reporting(E_ALL);
class AdsController extends Controller
{
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('?'),
                
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }
	
	public function init()
	{
		if(!Yii::app()->user->Ads)
		{
            $this->redirect(array('/membership'));			
		}		
	}
	
    public function actionIndex($pageid = null)
    {
		
		
        if (empty(Yii::app()->user->u_f_id)) {
            Yii::app()->user->setFlash('notice', 'Please select an account');
            $this->redirect(array('/facebook'));
        }

        $fanpages = UserFanpage::model()->getFanpages(Yii::app()->user->u_f_id);
        $favoriteads = UserAds::model()->getAllAds(Yii::app()->user->u_f_id);

        if (!$this->module->getUser()) {
            Yii::app()->user->setFlash('notice', 'Please select an account');
            $this->redirect(array('/facebook'));
        }
        $categoriesAds = AdCategory::model()->findAll();
        //$facebookuser = $this->module->api('/me');
        $userfacebook = UserFacebook::model()->findByPK(Yii::app()->user->u_f_id);
		
        $this->render('index', array(
            'fanpages' => $fanpages,
            'module' => $this->module,
            'userfacebook' => $userfacebook,
            //'pageid'=>$pageid,
            'allAds' => $favoriteads,
            'categoriesAds'=>$categoriesAds,
        ));
    }

    public function actionShowads($pageid = null)
    {
        if ($pageid === null) return;
        if ($pageid == 'ALL') {
            $ads = UserAds::model()->getAllAds(Yii::app()->user->u_f_id);
            return $this->renderPartial('_ads', array('ads' => $ads, 'module' => $this->module));
        }

        $page = UserFanpage::model()
            ->find('user_facebook_id = :ufid AND facebook_page_id = :fpid ', array(
                ':ufid' => Yii::app()->user->u_f_id,
                ':fpid' => $pageid,
            ));

        if (!count($page)) return false;

        $ads = $page->getRelated('ads', false, array(
            'condition' => 'deleted != \'1\'',
            'order' => 'favorite ASC'
        ));

        return $this->renderPartial('_ads', array('pageid' => $pageid, 'page' => $page, 'ads' => $ads, 'module' => $this->module));
    }
    
    public function actionCreateAd( $templateid = null, $fanpageid = null){
       if($templateid != null){
            $template = Ad::model()->findByPk($templateid);
            if(count($template) == 1){
                $tab = new UserAds();
                
                if ($fanpageid !== null) {
                    //Security checks
                    $fanpage = UserFanpage::model()->findByAttributes(array(
                        'facebook_page_id' => $fanpageid,
                        'user_facebook_id' => Yii::app()->user->u_f_id
                    ));
                    if ($fanpage === null) {
                        throw new CHttpException(404, 'Page not found.');
                    }
                    
                    $tab->user_fanpage_id = $fanpage->id;
                }
                
                $tab->user_facebook_id = Yii::app()->user->u_f_id;                

                if($tab->save(true)){
                    $pm = new AdManager($tab);
                    $nePage = $pm->createNewPageFromTemplate($template, $tab->name);
                    echo json_encode(array(
                        'error' => 0,
                        'tabId' => $tab->id
                    ));

                }else{
                    echo json_encode(array(
                        'error' => 1,
                        'message' => "Couldn't create ad. Please try again later."
                    ));
                    //echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
                }
            }
            else {
                echo json_encode(array(
                    'error' => 1,
                    'message' => "Couldn't create ad. Invalid template."
                ));
            }   
        }
    }
    
    
    public function actionAddAdToFavorite($tabid = null){
        if($tabid !== null && UserAds::model()->userCanModify($tabid, Yii::app()->user->id)){
            $tab = UserAds::model()->findByAttributes(array(
                    "id"=>$tabid));
            $tab->favorite = 1;
            if($tab->save()){
                echo json_encode(array("status"=>"notice", "message"=>"Added to favorites"));
                return true;
            }
            
        }
        echo json_encode(array("status"=>"warning", "message"=>"Some error occured"));
        return true;
    }
    
    public function actionRemoveAdfromFavorite($tabid = null){
        if($tabid !== null && UserAds::model()->userCanModify($tabid, Yii::app()->user->id)){
            $tab = UserAds::model()->findByAttributes(array(
                    "id"=>$tabid));
            $tab->favorite = 0;
            if($tab->save()){
                echo json_encode(array("status"=>"notice", "message"=>"Removed from favorites"));
                return true;
            }
            
        }
        echo json_encode(array("status"=>"warning", "message"=>"Some error occured"));
        return true;
    }
    
    public function actionCopyad($tabid){
        if($tabid != null ){
            $tabold = UserAds::model()->findByAttributes(array(
                'id' => $tabid
            ));
            if($tabold->userCanModify($tabold->id, Yii::app()->user->id)){
                $tab = new UserAds();
                $tab->user_fanpage_id = isset($tabold->user_fanpage_id) ? $tabold->user_fanpage_id : 0;
                $tab->user_facebook_id = $tabold->user_facebook_id;
                $tab->name = $tabold->name;
                if($tab->save()){

                    try{
                        $pq = new AdPageQuery();
                        $oldpage = $pq->getPageFromRepositoryByManagingTab($tabold);

                        $pm = new AdManager($tab);
                        $newPage = $pm->createPageFromPage($oldpage, $tab->id);
                        echo json_encode(array('status'=>'notice', 'message'=>'Ad Copied'));
                    }catch(Exception $e){
                        $tab->delete();
                        //var_dump($e);
                    }
                }else{
                    echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
                }
            }else{
                 echo json_encode(array('status'=>'warning', 'message'=>'Seems that you can\'t copy this ad'));
            }
            
        }
    }
    
    
    public function actionDeleteAd($tabid= null){
        if($tabid != null){
            $tab = UserAds::model()->findByAttributes(array(
                'id' => $tabid
            ));
            if(count($tab)==1 && UserAds::model()->userCanModify($tabid, Yii::app()->user->id)){
                if($tab->published == 1){
                    if($tab->setDelete()){
                    echo json_encode(array('status'=>'notice', 'message'=>'Tab deleted'));
                    return true;
                    }else{
                        echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
                        return false;
                    }
                }else{
                    if($tab->delete()){
                    echo json_encode(array('status'=>'notice', 'message'=>'Tab deleted'));
                    return true;
                    }else{
                        echo json_encode(array('status'=>'warning', 'message'=>'Some error occured'));
                        return false;
                    }
                }
                
            }else{
                echo json_encode(array('status'=>'warning', 'message'=>'Seems like this is not your tab.'));
                return false;
            }
        }
    }
    
    public function actionChangeAdName($id)
    {
        if (!isset($_POST['name']) || !is_string($_POST['name'])) {
            throw new CException(500, "Name hasn't been submitted.");
        }
        
        $tab = UserAds::model()->findByAttributes(array(
            'id' => $id
        ));
        
        if (count($tab)==1 && UserAds::model()->userCanModify($id, Yii::app()->user->id)) {
            $tab->name = $_POST['name'];
            if (!$tab->update(array('name'))) {
                exit(json_encode(array(
                    'status' => 'notice', 
                    'message' => "Ad name has been updated",
                    'success' => 1
                )));
            }
            echo json_encode(array(
                'status' => 'warning', 
                'message' => "Couldn't update ad name. Please try again later.", 
            ));
        }
    }    
    
}
<?php
use Facebook\FacebookRequest;
use Facebook\Authentication\AccessToken;
use Facebook\Facebook;
use Facebook\GraphUser;

class FacebookModule extends CWebModule
{
    public $defaultController = 'facebook';
     /**
     * Don't forget about access_token in session - Yii::app()->user->setState('access_token', '*****') / Yii::app()->user->getState('access_token')
     */
    
    // Facebook api     
    public $facebook;
    
    private $_session;
	private $fb; //added by usama

    //Need to be without spaces
//    const PERISSIONS = 'manage_pages,publish_actions,email,public_profile,read_insights';
    //Need to be without spaces
//    public $permission = array('manage_pages,publish_pages,publish_actions,email,public_profile,read_insights');


    //Need to be without spaces
    const PERISSIONS = 'manage_pages,email,public_profile,read_insights';
    //Need to be without spaces
    public $permission = array('manage_pages,publish_pages,email,public_profile,read_insights');



    // Cache for facebook user data
    public $fbUser;
    
    public $warning = null;
    
    
    public function init()
    {
		/*
		echo  '<h1 style="font-weight: bold; color:red; line-height: 26px; background:##00518c; text-align:center; font-size: 19px; padding: 0 15px;">
				Dear Tabfu Users, If you are unable to login to Tabfu using your Username and Password. Please contact our Support with your Email.<br />Thank you,  Tabfu Customer Services.	
			</h1>';
		/*exit;*/
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
		
        // import the module-level models and components
        Yii::app()->theme = 'classic';
        $this->setImport(array(
            'facebook.models.*',
            'facebook.components.*',
            'template.models.*',
            'template.models.exception.*',
            'template.models.activeRecord.*',
            'template.models.page.*',
            'template.models.page.exception.*',
            'template.models.page.asset.*',
            'template.models.page.asset.repositoryAdapter.*',
            'template.models.page.asset.repositoryAdapter.exception.*',
            'template.models.field.*',
            'template.models.field.exception.*',
            'template.models.field.attribute.*',
            'template.models.field.attribute.exception.*',
            'template.models.field.attribute.dataType.*',
            'template.models.field.attribute.dataType.exception.*',
            'template.components.*',
            'user.models.*',
            'message.models.*',
            'analytics.components.*',
            'analytics.models.*'
        ));

        require Yii::app()->basePath. '/modules/facebook/vendor/autoload.php';
		//added by usama
        $this->fb = new Facebook([
            'app_id' => Yii::app()->params['FacebookAppId'],
            'app_secret' => Yii::app()->params['FacebookSecret'],
            'default_graph_version' => 'v2.5',
//            'persistent_data_handler' => 'session'
        ]);		
		//end addition
//        FacebookSession::setDefaultApplication(Yii::app()->params['FacebookAppId'], Yii::app()->params['FacebookSecret']);		
       
		if (!Yii::app()->user->hasState('u_f_id')) 
		{
                   
			return;	
		}
			$userFbId = Yii::app()->user->getState('u_f_id');
			$userFb = UserFacebook::model()->findByPk($userFbId);
			$userId = $userFb['user_id'];
			//var_dump($userId);
		

    }

	public function beforeControllerAction($controller, $action)
	{
            if(parent::beforeControllerAction($controller, $action)) {
				$fb = $this->fb;
                /**
                * User access
                */
                if (($controller->id == 'facebook' && $action->id == 'session') ||
                    ($controller->id == 'facebook' && $action->id == 'error')) {
						
                    return true;
                }
                if ($controller->id == 'facebook' && $action->id == 'index') {

					// prevent a 'forever redirection' when user logged in to FB
                    // is different than the user stored in tabfu
                    $cookie = null;
                    if (Yii::app()->user->hasState('fbsessionredir')) {
                         
                        $cookie = Yii::app()->user->getState('fbsessionredir');
                        $newCookie = array(
                            'cnt' => $cookie['exp'] >= time() ? $cookie['cnt'] + 1 : 1,
                            'exp' => time() + 10
                        );
                      
                        Yii::app()->user->setState('fbsessionredir', $newCookie);
                    }
                    else {
                        $cookie = array(
                            'cnt' => 1,
                            'exp' => time() + 10
                        );
                        Yii::app()->user->setState('fbsessionredir', $cookie);
                    }
                    if ($cookie['cnt'] > 4 && $cookie['exp'] >= time()) {
                        Yii::app()->user->setState('error', 
                            'Couldn\'t authorize via Facebook.<br/>Please make sure you are logged in on Facebook to the same account you try to use in TabFu.<br/><br/>' .
                            '<a href="#" onclick="return logoutFBTF()">Log me out of Facebook and TabFu and try again.</a>'
                        );
                        Yii::app()->request->redirect('/facebook/facebook/error');
                        exit;
                    }
                }
                if (Yii::app()->user->isGuest AND $controller->id!='showtab' AND $controller->id!='showanimation' AND $controller->id!='cron') {
                  
                    Yii::app()->request->redirect('/login');
                }

                $sessionAccessToken = Yii::app()->user->getState('access_token');
                if (!empty($sessionAccessToken)) {
                    try {
                        $this->setAccessToken($sessionAccessToken);						
                    }
                    catch (\Facebook\FacebookAuthorizationException $ex) {
                 
                        Yii::log("\n\n\n".(Yii::app()->controller->user ? CVarDumper::dumpAsString(Yii::app()->controller->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'facebook.facebookModule.beforeController');
						
						$helper = $fb->getRedirectLoginHelper();
						$loginUrl = $helper->getLoginUrl(Yii::app()->createAbsoluteUrl('/facebook/facebook/session'),$this->permission);
                        Yii::app()->request->redirect($loginUrl);
                    }
                    $this->fbUser = $this->api('/me?fields=id,name,link,email,first_name,gender,last_name,timezone,verified,picture');
                    /*var_dump($this->fbUser =  (new FacebookRequest(
                        $this->session, 'GET', '/user'
                    ))->execute()->getGraphObject(GraphUser::className()));
                    exit;*/
                }

                if($this->fbUser){
                    //get permissions for user
                    $permissions = $this->api('/me/permissions',array(),null,'GET',true);
					
                    //Yii::log(CVarDumper::dumpAsString($this->fbUser), CLogger::LEVEL_WARNING);
                    //Yii::log(CVarDumper::dumpAsString($permissions->asArray()), CLogger::LEVEL_WARNING);

                    /*@var $permissions GraphUser*/
                    $permissionArray = array();
                    $rerequest = array();
                    foreach($permissions['data'] as $permissions){
                        if($permissions['status'] === 'granted'){
                            $permissionArray[$permissions['permission']] = $permissions['permission'];
                        }
                        else {
                            $rerequest[] = $permissions['permission'];
                        }
                    }
                    if (count($permissionArray)> 0 && count(array_intersect($permissionArray, explode(',', self::PERISSIONS))) == count(explode(',', self::PERISSIONS))){
                        return true;
                    }
                    else {
						$fb = $this->fb;
						$helper = $fb->getRedirectLoginHelper();
						$loginUrl = $helper->getLoginUrl(Yii::app()->createAbsoluteUrl('/facebook/facebook/session'),$this->permission);
						$this->warning = '<div class="flash-warning" style="text-align:center">' . 
						'Please <a href="' . $loginUrl . '"><strong>click here</strong></a> to provide all necessary permissions<br/>' .
						'that are required by this site to work properly.' .
						'</div>';
                    }
                }
                return true;
            }
            else {
                return false;
            }
	}
    
    public function getSession()
    {
//        return $this->_session['fb_access_token'];
		
        if (!$this->_session) {
            $sessionAccessToken = Yii::app()->user->getState('access_token');
            if (!empty($sessionAccessToken)) {
                $this->setAccessToken($sessionAccessToken);
            }
        }
        return $this->_session;
    }
    
    public function setSession($session)
    {
        $this->_session = $session;
    }
    
 //   public function getToken()
//	{
 //       return $this->_session['fb_access_token'];
		
//	}
	
	public function getLongLivedToken($short_token = null)
	{		
		$fb = $this->fb;
		$client = $fb->getOAuth2Client();
		$token = $short_token ? $short_token : $this->session->getValue();
		try {
		  // Returns a long-lived access token
		  $accessToken = $client->getLongLivedAccessToken($token);
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
		  // There was an error communicating with Graph

		  echo $e->getMessage();
		  exit;
		}

		if (isset($accessToken)) {
		  return (string) $accessToken;
		}		
	}
	
//	<-- previously used not using right now... will check it later -->
    public function refreshFBSession($checkFirst = true)
    {		
	
        $sessionExpired = true;
        if ($checkFirst) {
            if ($this->session) {
                // check if we have valid session info
                try {
					
                    $sessionInfo = $this->session;
                    $sessionExpired = $sessionInfo->isExpired();
                } catch (Exception $ex) {
                    Yii::log("\n\n\n".(Yii::app()->controller->user ? CVarDumper::dumpAsString(Yii::app()->controller->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'facebook.facebookModule.refreshFBSession');
                    $sessionExpired = false;
                }
            }
            else {
                $sessionExpired = false;
            }
        }
		
        // try to regenerate the session
        if (!$sessionExpired && Yii::app()->user->hasState('u_f_id')) {
            $userFb = UserFacebook::model()->findByPk(Yii::app()->user->getState('u_f_id'));
            if ($userFb) {
                try{
                    $sesion = new AccessToken($userFb->long_token);
                    $isValidLongToken = $sesion->isLongLived();
                }  catch (\Facebook\Exceptions\FacebookResponseException $ex){
                    Yii::log("\n\n\n".(Yii::app()->controller->user ? CVarDumper::dumpAsString(Yii::app()->controller->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'facebook.facebookModule.refreshFBSession');
                    $isValidLongToken = false;
                }
                
                if($isValidLongToken){
                    $this->setAccessToken($userFb->long_token);
                    // check if we have valid session info
                    try {
                        $sessionInfo = $this->session;
                        $sessionExpired = $sessionInfo->isExpired();
                    } catch (Exception $ex) {
                        Yii::log("\n\n\n".(Yii::app()->controller->user ? CVarDumper::dumpAsString(Yii::app()->controller->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'facebook.facebookModule.refreshFBSession');
                        $sessionExpired = false;
                    }
                }
				//else{
                    //User needs to be logout. 
                 //   Yii::app()->user->logout();
                //}
            }
        }					

        // try to revive the token
        if (!$sessionExpired) {
			//session_start(); // this line commnted on 25-08-2017
			$fb = new Facebook([
				'app_id' => Yii::app()->params['FacebookAppId'],
				'app_secret' => Yii::app()->params['FacebookSecret'],
				'default_graph_version' => 'v2.5',
				'persistent_data_handler' => 'session'
			]);		

//			$fb = $this->fb;
			$helper = $fb->getRedirectLoginHelper();
			$loginUrl = $helper->getLoginUrl(Yii::app()->createAbsoluteUrl('/facebook/facebook/session'),$this->permission);
			Yii::app()->request->redirect($loginUrl);
        }
    }
   
    public function facebookRequest($method, $endpoint, array $params = [], $accessToken = null, $eTag = null, $graphVersion = 'v2.5')
    {		
        $fb = $this->fb;
		/* old function 
		//HB//START
		$hbsession = $_SESSION['fb_access_token'];
		//HB//END
		$theSession = isset($session) ? $session : 
		(
			isset($this->session) ?
			$this->session :
			(
				isset($this->_session) ?
				$this->_session :
				$hbsession
			)
		);
		*/
		
		//HB//START
		$hbsession = new AccessToken(Yii::app()->user->getState('access_token'));
//		echo $hbsession; exit;
		//HB//END
		$theSession = isset($accessToken) ? $accessToken : 
		(
			isset($this->session) ?
			$this->session :
			(
				isset($this->_session) ?
				$this->_session :
				$hbsession
			)
		);

        if (!$theSession) {
            Yii::app()->user->logout();
        }
/*		if($tmp!=null){
			echo 'in tmp';
			echo 'endpoint:'.$endpoint.'<br>';
			echo 'token:'.$accessToken.'<br>';
			print_r($params);
			try{
				return $result = $fb->post($endpoint,$params,$accessToken);
			}catch(\Facebook\Exceptions\FacebookResponseException $e)
			{
				echo $e; 
				exit;
			}
		}
*/
        try {
            $result = $fb->sendRequest(
                $method, 
                $endpoint, 
                $params,
                $theSession,
				$eTag,
				$graphVersion
			);
			$graphResult = $result;
//          $graphResult = $graphType ? $result->getGraphObject($graphType) : $result->getGraphObject();
        }
//        catch (\Facebook\Exceptions\FacebookSDKException $e) {
//				
 //       }
 
        catch (\Facebook\Exceptions\FacebookResponseException $e) {
//			echo $e; exit;
            Yii::log("\n\n\n".(Yii::app()->controller->user ? CVarDumper::dumpAsString(Yii::app()->controller->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'facebook.facebookModule.facebookRequest');
            $tryAgain = false;
            // check if we have a session first
            if ($this->session) {
                $this->refreshFBSession();
                $tryAgain = true;
            }
            // if not, try to generate the session
            elseif (Yii::app()->user->hasState('u_f_id')) {
                $userFb = UserFacebook::model()->findByPk(Yii::app()->user->getState('u_f_id'));
                if ($userFb) {
                    $this->setAccessToken($userFb->long_token);
                    $tryAgain = true;
                }
            }
            else {
                $graphResult = null;
            }
            
            // we should be ok now, try again
            if ($tryAgain) {
				$theSession = $this->session;
				try{
				$result = $fb->sendRequest(
						$method, 
						$endpoint, 
						$params,
						$theSession,
						$eTag,
						$graphVersion
					);
				$graphResult = $result;
//                    $graphResult = $graphType ? $result->getGraphObject($graphType) : $result->getGraphObject();
					
				}
                catch (\Facebook\Exceptions\FacebookResponseException $e) {
                    Yii::log("\n\n\n".(Yii::app()->controller->user ? CVarDumper::dumpAsString(Yii::app()->controller->user->attributes)."\n\n" : '').CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'facebook.facebookModule.facebookRequest');
					$fb = $this->fb;
					$helper = $fb->getRedirectLoginHelper();
					$loginUrl = $helper->getLoginUrl(Yii::app()->createAbsoluteUrl('/facebook/facebook/session'),$this->permission);
                }
            }
        }
        return $graphResult;
    }
        
        
    ########### FUNCTIONS AFTER THIS POINT ARE FOR BACK COMPATIBILITY ############
    public function setAccessToken($token){
		//changes added by usama
//		$fb = $this->fb;
//		$_SESSION['fb_access_token'] = (string) $token;
		(string) $token;
/*		
        try {
            $response = $fb->get('/me?fields=id,name,email,picture,accounts', $_SESSION['fb_access_token']);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            $this->ueoutputs['errors'][] = $e->getMessage();
            exit();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            $this->ueoutputs['errors'][] = $e->getMessage();
            exit();
        }
        
        $user = $response->getGraphUser();
		echo '<pre>';
		print_r($user);
		exit;
		// end
		
        $this->session = new FacebookSession($token);
*/
//		$this->session = array('fb_access_token'=>$token);
        $this->session = new AccessToken($token);
        $this->fbUser = $this->api('/me?fields=id,name,link,email,first_name,gender,last_name,timezone,verified,picture');
		
    }
	public function setAccessTokenTest($token)
	{
		(string) $token;
        $this->session = new AccessToken($token);
		return;
	}

    public function api($endpoint, array $params = [],  $accessToken = null, $method='GET', $asArray=false, $eTag = null, $graphVersion = 'v2.5')
    {
        if(isset($accessToken)){
            $response = $this->facebookRequest($method, $endpoint, $params, $accessToken, $eTag, $graphVersion);
        }else{
            $response = $this->facebookRequest($method, $endpoint, $params, $accessToken, $eTag, $graphVersion);
        }
		
        if($asArray === true){
			$result = $response->getdecodedBody();
            return $result;
        }else{
            return $response;
        }


    }

    public function getUser()
    {
        $response = $this->api('/me?fields=id,name,link,email,first_name,gender,last_name,timezone,verified,picture');
//		$result = $response->getGraphUser();
//        return $result ? $result->asArray() : array();
		return $response->getdecodedBody();
    }



    public function getUserPic($userId)
    {    
        $pic = $this->api('/'.$userId.'/picture', array (
              'redirect' => false,
              'height' => '200',
              'type' => 'normal',
              'width' => '200',
            ),null,'GET',true);
			
        return $pic["data"]["url"];
    }


    public function getFanpagePic($fanPageId)
    {
        $pic = $this->api('/'.$fanPageId.'/picture', array (
              'redirect' => false,
              'height' => '200',
              'type' => 'normal',
              'width' => '200',
            ),null,'GET',true);
        return $pic["data"]["url"];
    }
	
	public function getFbObj()
	{
		return $this->fb;
	}
	public function getFbPermissions()
	{
		return $this->permission;
	}
}

<?php

/**
 * Table contains users membership statuses
 * USER_ID, MEMBERSHIP_STATUS (1-active, 0-inactive), MEMBERSHIP_END_DATE, LAST_MODIFIED
 * 
 */
class Membership extends CActiveRecord
{
    
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE_MONTH = 1;
    const STATUS_ACTIVE_QUARTER = 4;
    const STATUS_ACTIVE_YEAR = 2;
	//HB//START
    const STATUS_ACTIVE_TWO_YEAR = 5;
    const STATUS_ACTIVE_THREE_YEAR = 6;
	//HB//END
    const STATUS_ACTIVE_LIFETIME = 3;
    const STATUS_ACTIVE_INDIVIDUAL = 7;
	
	//US//START
    const STATUS_ACTIVE_PRO_LIFETIME = 8;
    const STATUS_ACTIVE_AGENCY = 9;
	//US//END
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'membership';
    }

    public function primaryKey()
    {
        return 'user_id';
    }

    public function beforeSave() {
        
        $this->last_modified = new CDbExpression('NOW()');
        
        return parent::beforeSave();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('membership_status', 'required'),
            array('membership_status, membership_end_date, recurring, recurring_date', 'safe'),
        );
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }
    
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'recurring_date' => 'Next Payment Date',
        );
    }
    
    /**
    * Retrieves a list of models based on the current search/filter conditions.
    *
    * Typical usecase:
    * - Initialize the model fields with values from filter form.
    * - Execute this method to get CActiveDataProvider instance which will filter
    * models according to data in model fields.
    * - Pass data provider to CGridView, CListView or any similar widget.
    *
    * @return CActiveDataProvider the data provider that can return the models
    * based on the search/filter conditions.
    */
   public function search()
   {
           // @todo Please modify the following code to remove attributes that should not be searched.

           $criteria=new CDbCriteria;

           $criteria->compare('user_id',$this->user_id);
           $criteria->compare('membership_status',$this->membership_status);
           $criteria->compare('membership_end_date',$this->membership_end_date,true);
           $criteria->compare('recurring',$this->recurring);
           $criteria->compare('recurring_date',$this->recurring_date,true);
           $criteria->compare('freebie',$this->freebie);
           $criteria->compare('last_modified',$this->last_modified,true);

           return new CActiveDataProvider($this, array(
                   'criteria'=>$criteria,
           ));
   }

    /**
     * Check if user membership is active
     * 
     * @return boolean
     */
    public function isActive() {

        return !($this->membership_status == Membership::STATUS_INACTIVE);
        
//        $membershipStatus = $this->membership_status;
//        
//        $isInactive = ($membershipStatus == Membership::STATUS_INACTIVE);
//
//        // additional check end date expire
//        if($membershipStatus == Membership::STATUS_ACTIVE && strtotime($this->membership_end_date) < strtotime(date('Y-m-d H:i:s')) ) {
//            $this->membership_status = Membership::STATUS_INACTIVE;
//            $isInactive = true;
////            $this->save();
//        }
//        
//        return !$isInactive;
    }
    
    
    public static function getMembershipStatuses()
    {
        $result = array(
            self::STATUS_ACTIVE_INDIVIDUAL  => 'Individual',
            self::STATUS_ACTIVE_LIFETIME  => 'Lifetime',
            self::STATUS_ACTIVE_YEAR  => 'Yearly',
			//HB//START
            self::STATUS_ACTIVE_TWO_YEAR  => '2year',
            self::STATUS_ACTIVE_THREE_YEAR  => '3year',
			//HB//END
            self::STATUS_ACTIVE_QUARTER => 'Quarterly',
            self::STATUS_ACTIVE_MONTH  => 'Monthly',
            self::STATUS_INACTIVE => 'Inactive',
			
            self::STATUS_ACTIVE_PRO_LIFETIME  => 'Prolifetime',
            self::STATUS_ACTIVE_AGENCY => 'Agency',
        );
        return $result;
    }
    
    public static function getMembershipStatusLabel($status) 
    {
        $statusString = '';
        switch($status) { 
            case Membership::STATUS_INACTIVE: $statusString = "Inactive"; 
                break; 
            case Membership::STATUS_ACTIVE_MONTH: $statusString = "Monthly"; 
                break; 
            case Membership::STATUS_ACTIVE_QUARTER: $statusString = "Quarterly"; 
                break; 
            case Membership::STATUS_ACTIVE_YEAR: $statusString = "Yearly"; 
                break; 
				//HB//START
            case Membership::STATUS_ACTIVE_TWO_YEAR: $statusString = "2year"; 
                break; 
            case Membership::STATUS_ACTIVE_THREE_YEAR: $statusString = "3year"; 
                break; 
				//HB//END
            case Membership::STATUS_ACTIVE_LIFETIME: $statusString = "Lifetime"; 
                break; 
            case Membership::STATUS_ACTIVE_INDIVIDUAL: $statusString = "Individual"; 
                break; 
            case Membership::STATUS_ACTIVE_PRO_LIFETIME: $statusString = "ProLifetime"; 
                break; 
            case Membership::STATUS_ACTIVE_AGENCY: $statusString = "Agency"; 
                break; 
            default: $statusString = "";    
        }
        
        return $statusString;
    }
    
    /**
     * Update membership status, end date and recurring
     * @return boolean
     */
    public function updateMembership($membershipStatus, $membershipEndDate = null, $membershipRecurringDate = null) {

        $this->membership_status = $membershipStatus;
        
        if($membershipEndDate) {
            $this->membership_end_date = $membershipEndDate;
        }
                
        if($membershipRecurringDate) {
            $this->recurring = true;
            $this->recurring_date = $membershipRecurringDate;
        } else {
            $this->recurring = false;
//            $this->recurring_date = null;
        }       
        
        return true;
    }
    
}

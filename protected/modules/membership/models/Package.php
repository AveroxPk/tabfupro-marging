<?php

/**
 * Table contains registers payments for users
 * ID, USER_ID, EMAIL, NAME, ADAPTER, DATE, AMOUNT, TRANSACTION_TYPE (sale, cancel, etc.), TYPE (month,year,lifetime), PARAMS (json)
 * 
 */
class Package extends CActiveRecord {

    const PACKAGE_TYPE_AGENCY = 'agency';
    const PACKAGE_TYPE_TEN_TABS_FULLACCESS = 'ten_tabs_access';

	/**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'package';
    }

	
	/**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
		
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, feature', 'required'),            

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('user_id, feature', 'unsafe', 'on'=>'search'),
        );
		
    }
	
	
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    

    /**
     * @return array relational rules.
     */
    public function relations()
    {     
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
//			'package'=>array(self::HAS_ONE, 'Package', 'user_id,feature'),            
        );
    }

}
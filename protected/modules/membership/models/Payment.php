<?php

/**
 * Table contains registers payments for users
 * ID, USER_ID, EMAIL, NAME, ADAPTER, DATE, AMOUNT, TRANSACTION_TYPE (sale, cancel, etc.), TYPE (month,year,lifetime), PARAMS (json)
 * 
 */
class Payment extends CActiveRecord {

    const PAYMENT_TYPE_MONTH = 'month';
    const PAYMENT_TYPE_QUARTER = 'quarter';
    const PAYMENT_TYPE_YEAR = 'year';
	//HB//START
    const PAYMENT_TYPE_TWO_YEAR = '2year';
    const PAYMENT_TYPE_THREE_YEAR = '3year';
	//HB//END
    const PAYMENT_TYPE_LIFETIME = 'lifetime';	
    const PAYMENT_TYPE_INDIVIDUAL = 'individual';

    const PAYMENT_TYPE_PRO_LIFETIME = 'prolifetime';
    const PAYMENT_TYPE_LIFETIME_UPGRADE = 'upgrade'; // upgrade previous customer to new tabfu pro
    const PAYMENT_TYPE_AGENCY = 'agency';

	
    const PAYMENT_TRANSACTION_TYPE_SALE = 'sale';
    const PAYMENT_TRANSACTION_TYPE_REFUND = 'refund';
    const PAYMENT_TRANSACTION_TYPE_CANCEL_RECURRING = 'cancel_recurring';


    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'payment';
    }


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }

}
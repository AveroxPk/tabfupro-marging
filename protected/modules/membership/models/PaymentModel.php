<?php

class PaymentModel { 

    /**
     * @var PaymentAdapter
     */
    protected $adapter;
    
    /**
     * @var Payment
     */
    protected $payment;
    
    /**
     * @var User
     */
    protected $user;
    
    /**
     * @var Membership
     */
    protected $membership;
	
    /**
     * @var Package
     */
    protected $package;


    /**
     * Verify transaction and save details to DB changing Payment and Membership tables
     * 
     * @return boolean
     */
    public function verify() {
        if(!$this->getAdapter()) {
            throw new Exception('No adapter specified');
        }
        
        if(!$this->getAdapter()->verify()) {
            throw new Exception('Request not verified');
        }

        // save payment record with data from adapter

        if(!$payment = $this->savePayment()) {
            throw new Exception('Error on saving payment');
        }

        $email = $this->getAdapter()->getCustomerEmail();
/*		
		if(null !== $this->getAdapter()->isPackageUpgrade())
		{
			// find user_id by email
			$user = User::model()->find('email = :email', array(':email' => $email));
			if(!$user){
				throw new Exception('Error on registering user');
			}else{
				if(!$user = $this->updateUser($user)){
					throw new Exception('Error on registering user');					
				}
				return;
			}
			
		}
*/
        //Addition
        //find/create new user by email Chalaki

        if(!$user = $this->getUser($email)) {
            throw new Exception('Error on registering user');
        }
        
        // attach user to payment
        $payment->user_id = $user->user_id;
        $payment->save();
        $this->payment = $payment;
        $this->user = $user;
        $userWasDeleted = ($this->user->status==User::STATUS_DELETED);

        // find/create membership with user_id
        if(!$membership = $this->saveMembership()) {
            throw new Exception('Error on saving membership');
        }
        $this->membership = $membership;
		
		//package will only create if customer buy a new package
		if(null === $this->getAdapter()->isPackageUpgrade())
		{
			// find/create membership with user_id			
			if(!$package = $this->savePackage()) {
				throw new Exception('Error on saving package');
			}
			$this->package = $package;			
		}
        
		
        // if user status was DELETED and now is ACTIVE -> send mail with password
        if($userWasDeleted && $this->user->isActive()) {
            $password = $user->generateRandomPassword();
            $passwordHashed = User::encode($password);
            $this->user->password = $passwordHashed;
            if($this->user->sendMessageWithPassword($password)) {
                $this->user->save();
            }
        }
        
        // send mail to user with payment & membership info
//        $this->sendPaymentSummaryToUser();
        
        return true;
    }
    
    /**
     * Save Payment to DB
     * 
     * @return boolean|\Payment
     */
    public function savePayment() {
        $paymentRecord = new Payment();
        $paymentRecord->adapter = $this->getAdapter()->getName();
        $paymentRecord->transaction_id = $this->getAdapter()->getTransactionId();
        $paymentRecord->transaction_type = $this->getAdapter()->getTransactionType();
        $paymentRecord->amount = $this->getAdapter()->getTransactionAmount();
        $paymentRecord->email = $this->getAdapter()->getCustomerEmail();
        $paymentRecord->name = $this->getAdapter()->getCustomerName();
        $paymentRecord->type = null != $this->getAdapter()->getType() ? $this->getAdapter()->getType() : (null != $this->getAdapter()->getType2() ? $this->getAdapter()->getType2() : $this->getAdapter()->getType3());
        $paymentRecord->params = json_encode($this->getAdapter()->getParams());
        $paymentRecord->date = new CDbExpression('NOW()');
        
        if(!$paymentRecord->save()) {
            return false;
        }
        $paymentRecord->refresh();
        return $paymentRecord;
    }
    
    /**
     * Search for user in DB by email and create it if not found
     * 
     * @param string $email
     * @return boolean|\User
     */
    public function getUser($email) {
        
        // find user_id by email
        $user = User::model()->find('email = :email', array(':email' => $email));
		
        // if user not found -> register
        if(!$user) {
            $newUser = new User();
            $password = $newUser->generateRandomPassword();
            $passwordHashed = User::encode($password);
            $newUser->password = $passwordHashed;
            $newUser->repeatPassword = $passwordHashed;
            $newUser->email = $email;
            $newUser->status = User::STATUS_ACTIVE;
            $newUser->type = User::USER_NORMAL;
            $newUser->package = 1;
            if($newUser->save()){
                $user = $newUser;
                // send mail to user with password
                $newUser->sendMessageWithPassword($password);
            }
        }
        
        return $user;
    }
	/**
     * Update user to tabfu pro
	 *
     * @param obj $user
     * @return boolean|\User
     */
    public function updateUser($user) {
		switch($this->getAdapter()->getTransactionType()) {
            
            /**
             * Transaction - SALE
             */
            case Payment::PAYMENT_TRANSACTION_TYPE_SALE:
				$user->package = 0;
				break;
			
			/**
             * Transaction - REFUND
             */
            case Payment::PAYMENT_TRANSACTION_TYPE_REFUND:
				$user->package = null;
				break;
			
		}
		
		if(!$user->save()) {
            return false;
        }
		
		return $user;
		
	}
	
    /**
     * Save membership into DB
     * update user membership_end_date
     * 
     * @param int $user_id
     * @return boolean|\Membership
     */
    public function saveMembership() {
		
        $user_id = $this->user->user_id;
        $membership = Membership::model()->findByPk($user_id);
        if(!$membership) {
            $membership = new Membership();
            $membership->user_id = $user_id;
        }
        $this->membership = $membership;
        switch($this->getAdapter()->getTransactionType()) {
            
            /**
             * Transaction - SALE
             */
            case Payment::PAYMENT_TRANSACTION_TYPE_SALE:
                    // $current_end_date -> if current membership_end_date is greater than NOW()
                    $current_end_date = strtotime($membership->membership_end_date) > strtotime(date('Y-m-d H:i:s')) ? 
                                    'DATE("'.$membership->membership_end_date.'")' : 'DATE_ADD(DATE(NOW()),INTERVAL 1 DAY)';
            
                    switch($this->getAdapter()->getType()) {
                
                        case Payment::PAYMENT_TYPE_MONTH: 
                                $membershipStatus = Membership::STATUS_ACTIVE_MONTH;
                                $membershipEndDate = new CDbExpression('DATE_ADD('.$current_end_date.',INTERVAL 1 MONTH)');
                                $membershipRecurringDate = $this->getAdapter()->isRecurring() ? 
                                            new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 MONTH)') : null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
                            break;
                        
                        case Payment::PAYMENT_TYPE_QUARTER: 
                                $membershipStatus = Membership::STATUS_ACTIVE_QUARTER;
                                $membershipEndDate = new CDbExpression('DATE_ADD('.$current_end_date.',INTERVAL 3 MONTH)');
                                $membershipRecurringDate = $this->getAdapter()->isRecurring() ? 
                                            new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 3 MONTH)') : null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
                            break;
                        
                        case Payment::PAYMENT_TYPE_YEAR: 
                                $membershipStatus = Membership::STATUS_ACTIVE_YEAR;
                                $membershipEndDate = new CDbExpression('DATE_ADD('.$current_end_date.',INTERVAL 1 YEAR)');
                                $membershipRecurringDate = $this->getAdapter()->isRecurring() ? 
                                            new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 YEAR)') : null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
                            break;
							//HB//START
                        case Payment::PAYMENT_TYPE_TWO_YEAR: 
                                $membershipStatus = Membership::STATUS_ACTIVE_TWO_YEAR;
                                $membershipEndDate = new CDbExpression('DATE_ADD('.$current_end_date.',INTERVAL 2 YEAR)');
                                $membershipRecurringDate = $this->getAdapter()->isRecurring() ? 
                                            new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 2 YEAR)') : null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
                            break;
                        case Payment::PAYMENT_TYPE_THREE_YEAR: 
                                $membershipStatus = Membership::STATUS_ACTIVE_THREE_YEAR;
                                $membershipEndDate = new CDbExpression('DATE_ADD('.$current_end_date.',INTERVAL 3 YEAR)');
                                $membershipRecurringDate = $this->getAdapter()->isRecurring() ? 
                                            new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 3 YEAR)') : null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
                            break;
                        //HB//END
                        case Payment::PAYMENT_TYPE_LIFETIME: 
                                $membershipStatus = Membership::STATUS_ACTIVE_LIFETIME;
                                $membershipEndDate = null;
                                $membershipRecurringDate = null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
                            break;
                        case Payment::PAYMENT_TYPE_INDIVIDUAL: 
                                $membershipStatus = Membership::STATUS_ACTIVE_INDIVIDUAL;
                                $membershipEndDate = new CDbExpression('DATE_ADD('.$current_end_date.',INTERVAL 1 YEAR)');
                                $membershipRecurringDate = $this->getAdapter()->isRecurring() ? 
                                            new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 YEAR)') : null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
                            break;
                        case Payment::PAYMENT_TYPE_PRO_LIFETIME: 
                                $membershipStatus = Membership::STATUS_ACTIVE_PRO_LIFETIME;
								
								if(isset($membership->membership_status) && $membership->membership_status == Membership::STATUS_ACTIVE_AGENCY)
									$membershipStatus = Membership::STATUS_ACTIVE_AGENCY;
							
                                $membershipEndDate = null;
                                $membershipRecurringDate = null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
                            break;
                        case Payment::PAYMENT_TYPE_AGENCY: 
                                $membershipStatus = Membership::STATUS_ACTIVE_AGENCY;
                                $membershipEndDate = new CDbExpression('DATE_ADD('.$current_end_date.',INTERVAL 1 YEAR)');
                                $membershipRecurringDate = $this->getAdapter()->isRecurring() ? 
                                            new CDbExpression('DATE_ADD(DATE(NOW()),INTERVAL 1 YEAR)') : null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
                            break;
						case Payment::PAYMENT_TYPE_LIFETIME_UPGRADE:
								$user = $this->user;
								$user->package = 0;
								$user->save();
                                $membershipStatus = Membership::STATUS_ACTIVE_PRO_LIFETIME;
								
                                $membershipEndDate = null;
                                $membershipRecurringDate = null;
                                
                                $membership->updateMembership($membershipStatus,$membershipEndDate,$membershipRecurringDate);
								
							break;
						
                    }
                    
                    $this->user->status = User::STATUS_ACTIVE;
                    $this->user->save();
                break;
            
            /**
             * Transaction - REFUND
             */
            case Payment::PAYMENT_TRANSACTION_TYPE_REFUND:
                    // find payment with same transaction_id and type SALE
                    $criteria = new CDbCriteria;
                    $criteria->condition='transaction_type=:transaction_type '
                                        . 'AND transaction_id=:transaction_id '
                                        . 'AND type=:type '
                                        . 'AND related_payment_id IS NULL ';
                    $criteria->params= array(':transaction_id'=>$this->getAdapter()->getTransactionId(),
                                             ':transaction_type'=>Payment::PAYMENT_TRANSACTION_TYPE_SALE,
                                             ':type'=>$this->getAdapter()->getType()
											 );
                    $criteria->order='date DESC';
					
                    $salePayment = Payment::model()->find($criteria);
					
                    if(!$salePayment) {
                        break;
                    }
//echo 'here';exit;
                    // substract from membership_end_date and change status
                    $current_end_date = 'DATE_SUB(DATE("'.$membership->membership_end_date.'"),INTERVAL 1 DAY)';
					
                    $membership->recurring = false;
                    
					$membership->membership_status = Membership::STATUS_INACTIVE;
                    $salePayment->related_payment_id = $this->payment->id;
                    $this->payment->related_payment_id = $salePayment->id;
                    $this->user->status = User::STATUS_DELETED;
					                    
                    switch($this->getAdapter()->getType()) {

                        case Payment::PAYMENT_TYPE_MONTH: 
                                $membership->membership_end_date = new B('DATE_SUB('.$current_end_date.',INTERVAL 1 MONTH)');
//                                $membership->save();
//                                $membership->refresh();
//                                $membership->membership_status = $membership->isActive() ? Membership::STATUS_ACTIVE : Membership::STATUS_INACTIVE;
                            break;
                            
                        case Payment::PAYMENT_TYPE_QUARTER: 
                                $membership->membership_end_date = new CDbExpression('DATE_SUB('.$current_end_date.',INTERVAL 3 MONTH)');
//                                $membership->save();
//                                $membership->refresh();
//                                $membership->membership_status = $membership->isActive() ? Membership::STATUS_ACTIVE : Membership::STATUS_INACTIVE;
                            break;

                        case Payment::PAYMENT_TYPE_YEAR: 
                                $membership->membership_end_date = new CDbExpression('DATE_SUB('.$current_end_date.',INTERVAL 1 YEAR)');
//                                $membership->save();
//                                $membership->refresh();
//                                $membership->membership_status = $membership->isActive() ? Membership::STATUS_ACTIVE : Membership::STATUS_INACTIVE;
                            break;
							//HB//START
                        case Payment::PAYMENT_TYPE_TWO_YEAR: 
                                $membership->membership_end_date = new CDbExpression('DATE_SUB('.$current_end_date.',INTERVAL 2 YEAR)');
//                                $membership->save();
//                                $membership->refresh();
//                                $membership->membership_status = $membership->isActive() ? Membership::STATUS_ACTIVE : Membership::STATUS_INACTIVE;
                            break;

                        case Payment::PAYMENT_TYPE_THREE_YEAR: 
                                $membership->membership_end_date = new CDbExpression('DATE_SUB('.$current_end_date.',INTERVAL 3 YEAR)');
//                                $membership->save();
//                                $membership->refresh();
//                                $membership->membership_status = $membership->isActive() ? Membership::STATUS_ACTIVE : Membership::STATUS_INACTIVE;
                            break;
							//HB//END
                        case Payment::PAYMENT_TYPE_LIFETIME: 
//                                $membership->membership_status = $membership->isActive(true) ? Membership::STATUS_ACTIVE : Membership::STATUS_INACTIVE;
                            break;
							
                        case Payment::PAYMENT_TYPE_INDIVIDUAL: 
                                $membership->membership_end_date = new CDbExpression('DATE_SUB('.$current_end_date.',INTERVAL 1 YEAR)');
                            break;
						
						case Payment::PAYMENT_TYPE_PRO_LIFETIME:
							
							
								$criteria = new CDbCriteria;
								$criteria->condition='status=:status '
													. 'AND user_id=:user_id ';
								$criteria->params= array(':status'=>1,
														 ':user_id'=>$this->user->user_id,
														 );
								
								$package = Package::model()->find($criteria);
								
								if(!$package){
									break;
								}
								
								$membership->membership_status = Membership::STATUS_ACTIVE_PRO_LIFETIME;
								$this->user->status = User::STATUS_ACTIVE;						
							break;
                        case Payment::PAYMENT_TYPE_AGENCY:					
                                $membership->membership_end_date = new CDbExpression('DATE_SUB('.$current_end_date.',INTERVAL 1 YEAR)');
								$membership->membership_status = Membership::STATUS_ACTIVE_PRO_LIFETIME;
								$membership->membership_end_date = null;
								$membership->recurring_date = null;
								$this->user->status = User::STATUS_ACTIVE;
                            break;
							
						case Payment::PAYMENT_TYPE_LIFETIME_UPGRADE:
								$user = $this->user;
								$user->status = User::STATUS_ACTIVE;
								$user->package = null;
								$user->save();
							break;							
                    }                         
                    
                    $salePayment->save();
                    $this->payment->save();
                    
                    $this->user->save();
                break;
            
            /**
             * Transaction - CANCEL RECURRING
             */
            case Payment::PAYMENT_TRANSACTION_TYPE_CANCEL_RECURRING:
                    $membership->recurring = true;
                    $membership->recurring_date = null;
                break;
            
            default:
        }

        if(!$membership->save()) {
            return false;
        }
        $membership->refresh();
        if($membership->membership_status == Membership::STATUS_INACTIVE) {
            
            try {
                $this->user->unpublishFacebookTabs();
            } catch(Exception $e) {
                Yii::log(CVarDumper::dumpAsString($e->getMessage()), CLogger::LEVEL_ERROR, 'application.modules.membership.payment.facebook-unpublish');
            }
        }
        
        return $membership;
    }
	
    /**
     * Save package into DB
     * update user package features
     * 
     * @param int $user_id
     * @return boolean|\Pakcage
     */
	public function savePackage()
	{
	
		$user_id = $this->user->user_id;
		$getPackage = $this->getAdapter()->getPackage();
		$tabsPackages = array('three-tabs','five-tabs', 'ten-tabs');

/*		
		if(strpos($feature,'tabs') !== false){ 
			$tabsPackages = array('three-tabs','five-tabs', 'ten-tabs');
			foreach($tabsPackages as $tabPackage)
			{
				$package = Package::model()->find('user_id = :userid AND feature = :feature AND status = :status', array(':userid' =>$user_id, ':feature' => $tabPackage, ':status' => 1));
				if($package){
					$package->status = 2;
					$package->update(array('status'));
				}
			}
		}
*/




		$package = Package::model()->find('user_id = :userid AND feature = :feature', array(':userid' =>$user_id, ':feature' => $getPackage));
		
//		$packageParts = Yii::app()->params['packageParts'];


        if(!$package) {
			$package = new Package();
			$package->user_id = $user_id;
			$package->feature = $getPackage;
			$package->status = 0;
        }
		
		$this->package = $package;
		
        switch($this->getAdapter()->getTransactionType()) {
            
            /**
             * Transaction - SALE
             */
            case Payment::PAYMENT_TRANSACTION_TYPE_SALE:
                //addition
                if($getPackage == 'ten_tabs_access'){
                    $addPackages = ['ten-tabs','plus-pack','essential-pack'];
                    foreach ($addPackages as $addPackage){
                        $package = new Package();
                        $package->user_id = $user_id;
                        $package->feature = $addPackage;
                        $package->status = 1;
                        $package->save();
                    }
                    return $package;
                break;
                }
				$package->status = 1;
				break;
				
            case Payment::PAYMENT_TRANSACTION_TYPE_REFUND:
                //addition
                if($getPackage == 'ten_tabs_access'){
                    $addPackages = ['ten-tabs','plus-pack','essential-pack'];
                    foreach ($addPackages as $addPackage){
                        $package = new Package();
                        $package->user_id = $user_id;
                        $package->feature = $addPackage;
                        $package->status = 0;
                        $package->update(array('status'));
                    }
                    break;
                }

                $package->status = 0;
				$package->update(array('status'));
				
/*				if(strpos($getPackage,'tabs') !== false){
					$tabsPackages = array('three-tabs','five-tabs', 'ten-tabs');
					foreach($tabsPackages as $tabPackage)
					{
						$package = Package::model()->find(array('condition'=> "user_id = $user_id AND status = 2", 'order' => 'feature ASC'));
//						var_dump($package); exit;
						if($package){
							$package->status = 1;
							$package->update(array('status'));
							break;
						}
					}
				}*/
				break;
/*				
			case Payment::PAYMENT_TRANSACTION_TYPE_CANCEL_RECURRING:
				$package->status = 0;
				$package->update(array('status'));
				break;
*/			
			default:					
		}

        if(!$package->save()) {
                return false;
        }
		return $package;
	} 
	
    /**
     * Send email to user with payment & membership info
     * 
     * @return boolean
     */
    public function sendPaymentSummaryToUser() {
        
        switch($this->getAdapter()->getTransactionType()) {
            case Payment::PAYMENT_TRANSACTION_TYPE_SALE:
                    return $this->sendSalePaymentSummaryToUser();
                break;
            
            case Payment::PAYMENT_TRANSACTION_TYPE_REFUND:
                    return $this->sendRefundPaymentSummaryToUser();
                break;
        }
        
        return false;
    }
    
    public function sendSalePaymentSummaryToUser() {
        
        $mailto = $this->getAdapter()->getCustomerEmail();
        $mailsubject = 'TabFu membership payment registered (Transaction ID '.$this->getAdapter()->getTransactionId().')';
        $mailcontent = '';
        
        switch($this->getAdapter()->getType()) {
            case Payment::PAYMENT_TYPE_MONTH: 
                    $mailcontent = 'Thank You for buying Monthly Membership.';
                break;
                
            case Payment::PAYMENT_TYPE_QUARTER: 
                    $mailcontent = 'Thank You for buying Quarterly Membership.';
                break;

            case Payment::PAYMENT_TYPE_YEAR: 
                    $mailcontent = 'Thank You for buying Yearly Membership.';
                break;
				//HB//START
            case Payment::PAYMENT_TYPE_TWO_YEAR: 
                    $mailcontent = 'Thank You for buying Two Year Membership.';
                break;

            case Payment::PAYMENT_TYPE_THREE_YEAR: 
                    $mailcontent = 'Thank You for buying Three Year Membership.';
                break;
				//HB//END
            case Payment::PAYMENT_TYPE_LIFETIME: 
                    $mailcontent = 'Thank You for buying Lifetime Membership.';
                break;
            case Payment::PAYMENT_TYPE_INDIVIDUAL: 
                    $mailcontent = 'Thank You for buying One Time Membership.';
                break;
				
            case Payment::PAYMENT_TYPE_PRO_LIFETIME: 
                    $mailcontent = 'Thank You for buying Lifetime Membership.';
                break;
            case Payment::PAYMENT_TYPE_AGENCY: 
                    $mailcontent = 'Thank You for buying Agency Membership.';
                break;
				
        }

        if($this->getAdapter()->getType() != Payment::PAYMENT_TYPE_LIFETIME) {
            $mailcontent .= ' You\'re membership will expire on '.date('d F Y', strtotime($this->membership->membership_end_date)).'.';
        }
        
        return MailHelper::sendMail($mailto, $mailsubject, $mailcontent, null, 'text/html');
    }
    
    public function sendRefundPaymentSummaryToUser() {
        
        $mailto = $this->getAdapter()->getCustomerEmail();
        $mailsubject = 'TabFu membership payment refunded (Transaction ID '.$this->getAdapter()->getTransactionId().')';
        $mailcontent = '';
        
        switch($this->membership->membership_status) {
            case Membership::STATUS_ACTIVE_LIFETIME:
                    $mailcontent = 'We refunded Your payment but You have still lifetime membership.';
                break;
            
            case Membership::STATUS_ACTIVE:
                    $mailcontent = 'We refunded Your payment but You have still active membership which will expire on '.date('d F Y', strtotime($this->membership->membership_end_date)).'.';
                break;
            
            case Membership::STATUS_INACTIVE:
                    $mailcontent = 'We refunded Your payment and Your membership is inactive.';
                break;
        }
        
        return MailHelper::sendMail($mailto, $mailsubject, $mailcontent, null, 'text/html');
    }
    
    /**
     * Send email to customer when payment error has occured
     * 
     * @return boolean
     */
    public function sendErrorMessage() {
        $mailto = $this->getAdapter() ? $this->getAdapter()->getCustomerEmail() : false;
        if(!$mailto) {
            return false;
        }
        $transactionId = $this->getAdapter() ? $this->getAdapter()->getTransactionId() : false;
        
        $mailsubject = 'TabFu';
        $mailcontent  = 'Error has occured. Please send email with transaction '.$transactionId.' details to office@procreative.eu';

        return MailHelper::sendMail($mailto, $mailsubject, $mailcontent, null, 'text/html');
    }
    
    public function getAdapter() {
        return $this->adapter;
    }

    public function setAdapter(PaymentAdapter $adapter) {
        $this->adapter = $adapter;
        return $this;
    }

}
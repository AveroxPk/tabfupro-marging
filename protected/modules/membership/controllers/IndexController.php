<?php

class IndexController extends Controller
{
        /**
         * Renders Mmbership page
         */
 

	public function actionIndex()
	{
            if(!Yii::app()->user->isGuest) {
                $membership = $this->user->membership;
            } else {
                $membership = false;
            }
            
           
           $user  =  User::model()->findByPk(Yii::app()->user->id);
           $user->scenario = 'my_profile';
           $user_email = $user->email;
           

           $zendesk = new zendesk();
           $zuser =  $zendesk->coreSearch($user_email);
           $tickets = array();
           if(!empty($zuser) && isset($zuser->results[0])){
			   
				$id = $zuser->results[0]->id;
                $tickets =  $zendesk->getTickets($id);
			   
               if(!empty($tickets->tickets)){
                   foreach ($tickets->tickets   as $key => $item){
                       $id = $item->id;
                       $tickets->tickets[$key]->metricks = $zendesk->getTicket($id);
                   }
               }
           }
          // var_dump($tickets->tickets); exit;
           //var_dump(Yii::app()->user->id); exit;
           //$user->findByPk(Yii::app()->user->id);
           
           
           if(isset($_POST['User'])){
				$post = $_POST['User'];
               $user->attributes =  $post;
               if($user->validate()){
                   $user->password = User::encode($user->new_password);
                   $user->update();
                   Yii::app()->user->setFlash('my_profile', '<center>Your profile has been updated</center>');
               }
           }
            $this->render('index', array('membership'=>$membership,'user'=> $user,'tickets' => $tickets));
	}

        /**
         * Call date diff helper to return difference between now and membership end date
         * 
         * @param Membership $membership
         * @return string
         */
        public function _membershipDateDiffHelper(Membership $membership) {
            return $this->_dateDiffHelper(date('Y-m-d H:i:s'), $membership->membership_end_date);
        }
            
        /**
         * Calculate difference between two dates and return readable string
         * 
         * @param date string $date1
         * @param date string $date2
         * @return string
         */
        public function _dateDiffHelper($date1, $date2) {
            $datetime1 = new DateTime($date1);
            $datetime2 = new DateTime($date2);

            $difference = $datetime1->diff($datetime2, false);
            
            if($difference->invert) {
                return 'past';
            }
            
            $string = '';
            if($difference->y==1) { $string .= $difference->y.' year '; } else if($difference->y>1){ $string .= $difference->y.' year '; }
            if($difference->m==1) { $string .= $difference->m.' month '; } else if($difference->m>1) { $string .= $difference->m.' months '; }
            if($difference->d==1) { $string .= $difference->d.' day '; } else if($difference->d>1) { $string .= $difference->d.' days'; }
            
            return trim($string);
        }
}
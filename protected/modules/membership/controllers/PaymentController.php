<?php

class PaymentController extends Controller
{

        /**
         * Page for JVZIPN payment notification
         */
        public function actionVerifyJvzipn()
        {
//            print_r($_POST);
//            exit();

//			echo "test"; exit;
            // get $_POST data, to test get from RAW Body
           $post = count($_POST) ? $_POST : json_decode(file_get_contents("php://input"), true);
            Yii::log(CVarDumper::dumpAsString($post), CLogger::LEVEL_WARNING, 'application.modules.membership.payment.jvzipn');
          
            // get secret key specified on jvzoo.com account from config
            if(!isset(Yii::app()->params['JvzipnSecretKey'])) {
                header("HTTP/1.1 401 Unauthorized");
                echo 'JvzipnSecretKey not set';
                exit;
            }
            $secretKey = Yii::app()->params['JvzipnSecretKey'];

            // create Jvzipn adapter and set secret key
            $jvzipnAdapter = new JvzipnPaymentAdapter($post);
            $jvzipnAdapter->setSecretKey($secretKey);

            // create Payment model with Jvzipn adapter
            $payment = new PaymentModel();
            $payment->setAdapter($jvzipnAdapter);
            
            // return proper HTTP response
            try {
                
                $payment->verify();
                
            } catch (Exception $e) {
                header("HTTP/1.1 401 Unauthorized");
                echo $e->getMessage();
//                $payment->sendErrorMessage();
                Yii::log(CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'application.modules.membership.payment.jvzipn');
                exit;
            }
           
            header("HTTP/1.1 200 OK");
            echo 'OK';
            exit;
        }

}
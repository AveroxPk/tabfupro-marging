<?php

class CronController extends Controller
{
    
    public function filters() {
        return array(
            'accessControl', 
        );
    }
    
    public function accessRules() {
        return array(
            array(
                'allow',
                'users' => array('*'),
                ),
            );
    }
    public function actionIndex()
	{
		$this->actionRefresh();
		$this->actionUnpublishtabs();
		$this->actionAgencyexpire();
	}
	
    public function actionRefresh() {
        
        // find memberships where STATUS is ACTIVE_MONTH or ACTIVE_YEAR but membership_end_date passed
        $criteria = new CDbCriteria;
        $criteria->condition='DATEDIFF(membership_end_date, NOW())<0';
        $criteria->addInCondition('membership_status',array(Membership::STATUS_ACTIVE_MONTH, Membership::STATUS_ACTIVE_QUARTER, Membership::STATUS_ACTIVE_YEAR, Membership::STATUS_ACTIVE_TWO_YEAR, Membership::STATUS_ACTIVE_THREE_YEAR));
        $membershipToUpdate = Membership::model()->findAll($criteria);

        $counter = 0;
        foreach($membershipToUpdate as $membership) {
            $membership->membership_status = Membership::STATUS_INACTIVE;
            $membership->recurring = false;
            $membership->save();
            
            $counter++;
            
            $user = User::model()->findByPk($membership->user_id);
//            $user->unpublishFacebookTabs();
            $user->sendMessageMembershipDeactivated();
            
        }
        
        echo 'Updated '.$counter.' records';
        
    }
	
	/*
		@author: usama
		unpublish user tabs where user has been deactived since 15 days,
	*/
	
    public function actionUnpublishtabs() {
        
        // find memberships where STATUS is ACTIVE_MONTH or ACTIVE_YEAR but membership_end_date passed
        $criteria = new CDbCriteria;
        $criteria->condition='DATEDIFF(membership_end_date, NOW())< -15';
        $criteria->addInCondition('membership_status',array(Membership::STATUS_ACTIVE_MONTH, Membership::STATUS_ACTIVE_QUARTER, Membership::STATUS_ACTIVE_YEAR, Membership::STATUS_ACTIVE_TWO_YEAR, Membership::STATUS_ACTIVE_THREE_YEAR));
        $membershipToUpdate = Membership::model()->findAll($criteria);

        $counter = 0;
        foreach($membershipToUpdate as $membership) {
            $membership->membership_status = Membership::STATUS_INACTIVE;
            $membership->recurring = false;
            $membership->save();
            
            $counter++;
            
            $user = User::model()->findByPk($membership->user_id);
            $user->unpublishFacebookTabs();
            
        }
        
        echo 'Updated '.$counter.' records';
        
    }
	
	/*
		@author: usama
		Agency account has been exprire
	*/
	
    public function actionAgencyexpire() {
        
        // find memberships where STATUS is ACTIVE_MONTH or ACTIVE_YEAR but membership_end_date passed
        $criteria = new CDbCriteria;
        $criteria->condition='DATEDIFF(membership_end_date, NOW())<0';
        $criteria->addInCondition('membership_status',array(Membership::STATUS_ACTIVE_AGENCY));
        $membershipToUpdate = Membership::model()->findAll($criteria);

        $counter = 0;
        foreach($membershipToUpdate as $membership) {
            $membership->membership_status = Membership::STATUS_ACTIVE_PRO_LIFETIME;
            $membership->recurring = false;
            $membership->save();
            
            $counter++;
            
            $user = User::model()->findByPk($membership->user_id);
//            $user->unpublishFacebookTabs();
            $user->sendMessageAgencyDeactivated();
            
        }
        
        echo 'Updated '.$counter.' records';
        
    }
    
    public function actionAkif () {
    	// find memberships where STATUS is ACTIVE_MONTH or ACTIVE_YEAR but membership_end_date passed
        $criteria = new CDbCriteria;
        $criteria->condition='DATEDIFF(membership_end_date, NOW())<0';
        $criteria->addInCondition('membership_status',array(Membership::STATUS_ACTIVE_MONTH, Membership::STATUS_ACTIVE_QUARTER, Membership::STATUS_ACTIVE_YEAR, Membership::STATUS_ACTIVE_TWO_YEAR, Membership::STATUS_ACTIVE_THREE_YEAR));
        $membershipToUpdate = Membership::model()->findAll($criteria);

        $counter = 0;
        foreach($membershipToUpdate as $membership) {
            $membership->membership_status = Membership::STATUS_INACTIVE;
            $membership->recurring = false;
            $membership->save();
            
            $counter++;
            
            //$user = User::model()->findByPk($membership->user_id);
            //$user->unpublishFacebookTabs();
//            $user->sendMessageMembershipDeactivated();
            
        }
        
        echo 'Updated '.$counter.' records';
    }
    
}
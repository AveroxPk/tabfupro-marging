<?php


define("ZDAPIKEY", "NXf6g0F5CW24JWJprUTuHmgTyhgzU6qHJuN5Sy22");
define("ZDUSER", "natausha@tabfu.com");
define("ZDURL", "https://tabfu.zendesk.com/api/v2");


class zendesk{
    

/* Note: do not put a trailing slash at the end of v2 */
function curlWrap($url, $json, $action)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
	curl_setopt($ch, CURLOPT_URL, ZDURL.$url);
	curl_setopt($ch, CURLOPT_USERPWD, ZDUSER."/token:".ZDAPIKEY);
	switch($action){
		case "POST":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			break;
		case "GET":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			break;
		case "PUT":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			break;
		case "DELETE":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			break;
		default:
			break;
	}
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
	curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	$output = curl_exec($ch);
	curl_close($ch);
	$decoded = json_decode($output);
	return $decoded;
}



public function getTickets($id){
   $tickets =   $this->curlWrap("/users/$id/tickets/requested.json", null, "GET");
   return $tickets;
    
    
}
public function getTicket($id){
    
   $ticket =   $this->curlWrap("/tickets/$id/metrics.json", null, "GET");
   return $ticket;   
}


public function coreSearch($user_email){
    $serach =   $this->curlWrap("/search.json?query=type:user+email:$user_email", null, "GET");
    return $serach;
}




    
    
}
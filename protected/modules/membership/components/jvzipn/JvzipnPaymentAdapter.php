<?php

class JvzipnPaymentAdapter extends PaymentAdapter {

    private $secretKey;
	
    static private $availableParams = array(
	'ccustname',
	'ccuststate',
	'ccustcc',
	'ccustemail',
	'cproditem',
	'cprodtitle',
	'cprodtype',
	'ctransaction',
	'ctransaffiliate',
	'ctransamount',
	'ctranspaymentmethod',
	'ctransvendor',
	'ctransreceipt',
	'cupsellreceipt',
	'caffitid',
	'cvendthru',
	'cverify',
	'ctranstime',
    );
    
    static private $_monthlyKeywords = array(
        'Tabfu Pro Unlimited Monthly $19.80',
        'tabfu pro unlimited monthly $19.80',
        'tabfu monthly',
        'monthly',
		'207226',
    	'Tabfu Monthly Membership AFF',
    	'225361',
    );
    static private $_quarterlyKeywords = array(
        'tabfu quarterly',
        'quarterly',
        '125252',
    	'Tabfu Quarterly Membership AFF',
    	'225365',
    );
    static private $_yearlyKeywords = array(
        'Tabfu Agency Annual',
        'tabfu agency annual',
        'Tabfu Pro Annual',
        'tabfu pro annual',
        'Tabfu Pro Unlimited Annual $99',
        'tabfu pro unlimited annual $99',
        'Tabfupro Ten Tabs Annual',
        'tabfupro ten tabs annual',
        'tabfu yearly',
        'yearly',
        '125254',
    	'Tabfu Yearly Membership AFF',
    	'225363',
		//HB//START
		'tabfu one year',
		'199084',
		//HB//END
    );
	//HB//START
    static private $_twoyearKeywords = array(
		'tabfu two year',
		'two year',
		'199080'
    );
    static private $_threeyearKeywords = array(
		'tabfu three year',
		'three year',
		'196886'
    );
	//HB//END
    static private $_lifetimeKeywords = array(
        'tabfu lifetime',
		'Tabfu LifeTime',
        'Tabfu Pro Promo',
        'tabfu pro promo',
        'TabfuPro For Private Promotion',
        'tabfuPro for private promotion',
        'TabfuPro For Facebook',
        'tabfuPro for facebook',
//        'lifetime',
//		'LifeTime',
        '125256',
		'256527'
    );
    static private $_individualKeywords = array(
        'tabfu individual',
        'individual',
        '125256'
    );
	
    static private $_prolifetimeKeywords = array(
        'TabfuPro Lifetime Ten Tabs Facebook',
        'tabfuPro lifetime ten tabs facebook',
        'tabfupro lifetime ten tabs facebook',
        'TabfuPro Lifetime Three Tabs',
		'TabfuPro Lifetime Five Tabs',
		'TabfuPro Lifetime Ten Tabs',
//		'TabfuPro Lifetime Professional Templates',
//		'TabfuPro Lifetime Motionpost & Ads'
		'TabfuPro Lifetime Essential Pack',
		'TabfuPro Lifetime Plus Pack',
	);
	
    static private $_prolifetimepackageKeywords = array(
        'three tabs',
		'five tabs',
		'ten tabs',
//		'professional tabs',
//		'motionpost & ads'
		'essential pack',
		'plus pack'
	);
	
    static private $_proagencyKeywords = array(
        'TabfuPro Agency Licence Annual',
        'tabfupro agency licence annual',
//        'Tabfu Agency Annual',
        'yearly',
        '125254',
    );
	
	static private $_lifetimeupgradeKeywords = array(
		'Tabfu Package Upgrage',
		'tabfu package upgrade',
	);

	static private $_packagesFullAccessKeywords = array(
		'Tabfu Agency Annual',
		'tabfu agency annual',
        'Tabfu Pro Annual',
        'tabfu pro annual',
        'Tabfu Pro Unlimited Annual $99',
        'tabfu pro unlimited annual $99',
        'Tabfu Pro Unlimited Monthly $19.80',
        'tabfu pro unlimited monthly $19.80',
        'TabfuPro For Facebook',
        'tabfuPro for facebook',
        'TabfuPro For Private Promotion',
        'tabfuPro for private promotion',
        'Tabfu Pro Promo',
        'tabfu pro promo'
	);
	static private $_packagesFullAccessKeywordsWithTabs = array(
        'TabfuPro Ten Tabs Annual',
        'tabfuPro ten tabs annual',
        'tabfupro ten tabs annual',
        'TabfuPro Lifetime Ten Tabs Facebook',
        'tabfuPro lifetime ten tabs facebook',
        'tabfupro lifetime ten tabs facebook',
	);

	
    public function __construct($params = array()) {
        parent::__construct($params);
    }
    
    public function getName() {
        return 'jvzipn';
    }

    public function verify() {

        if(!isset($this->params['ctransaction']) || !$this->validateParams()) {
            return false;
        }

        $secretKey  = $this->getSecretKey();
        $params     = $this->getParams();

        $pop        = "";
        $ipnFields  = array();
        foreach ($params AS $key => $value) {
            if ($key == "cverify") { continue; }
            $ipnFields[] = $key;
        }
		
        sort($ipnFields);
		
        foreach ($ipnFields as $field) {
            // if Magic Quotes are enabled $_POST[$field] will need to be
            // un-escaped before being appended to $pop
            $pop = $pop . $params[$field] . "|";
        }

        $pop = $pop . $secretKey;
        $encodedVarifyCode = sha1($pop);
        $verifyCode = strtoupper(substr($encodedVarifyCode,0,8));
//		echo $verifyCode; exit;
        return $verifyCode == $params["cverify"];    
    }
    
    public function validateParams() {
        if(!$this->secretKey) { return false; }
        foreach(self::$availableParams AS $key) {
            if(!array_key_exists($key, $this->params)) {
                return false;
            }
        }
        return true;
    }

    public function isRecurring() {
        return true;
    }
    
    public function getTransactionId() {
        return isset($this->params['ctransreceipt']) ? $this->params['ctransreceipt'] : null;
    }
    
    public function getTransactionType() {
        $transactionType = isset($this->params['ctransaction']) ? $this->params['ctransaction'] : null;
        switch($transactionType) {
            case 'SALE': $paymentTransactionType = Payment::PAYMENT_TRANSACTION_TYPE_SALE;
                break;
            case 'CGBK':
            case 'RFND': $paymentTransactionType = Payment::PAYMENT_TRANSACTION_TYPE_REFUND;
                break;
            case 'CANCEL-REBILL': $paymentTransactionType = Payment::PAYMENT_TRANSACTION_TYPE_CANCEL_RECURRING;
                break;
            case 'BILL':
            case 'INSF':
            case 'UNCANCEL-REBILL':
            default: 
                $paymentTransactionType = null;
        }
        
        return $paymentTransactionType;
    }
    
    public function getTransactionAmount() {
        $amount = isset($this->params['ctransamount']) ? $this->params['ctransamount'] : null;
        if($amount && strpos($amount,'.') !== false) {
            $amount = floatval($amount) * 100;
        }
        return $amount;
    }
    
    public function getTransactionPaymentMethod() {
        return isset($this->params['ctranspaymentmethod']) ? $this->params['ctranspaymentmethod'] : null;
    }
    
    public function getCustomerName() {
        return isset($this->params['ccustname']) ? $this->params['ccustname'] : null;
    }
    
    public function getCustomerEmail() {
        return isset($this->params['ccustemail']) ? $this->params['ccustemail'] : null;
    }
	//HB//START
	//secondary method for getting user's membership package type
	public function getType2()
	{
//		Checks if email is bieng sent with the request from JvZoo
		$user_email = isset($this->params['ccustemail']) ? $this->params['ccustemail'] : null;		
		//Query the user table to get the user agianst the email
        $user = User::model()->find('email = :email', array(':email' => $user_email));
		//Checks if the user exists
		if(!$user)
		{
			//if the user doesn't exist retruns null
			return null;
		}
		//takes the user ID from the table. Unnessary assgnment. Just for readabilty purpose
		$user_id = $user->user_id;
		//Query the membership table to get the membership package agains user
		$user_membership = Membership::model()->find('user_id = :user_id', array(':user_id'=> $user_id));
		//Unnessary assgnment. Just for readabilty purpose
		$user_membership_status = $user_membership->membership_status;
		//Returns the payment type agianst the membership status.
		 switch($user_membership_status) 
		{ 
            case Membership::STATUS_ACTIVE_MONTH:
				return Payment::PAYMENT_TYPE_MONTH;
                break; 
            case Membership::STATUS_ACTIVE_QUARTER: 
				return Payment::PAYMENT_TYPE_QUARTER;
                break; 
            case Membership::STATUS_ACTIVE_YEAR:
				return Payment::PAYMENT_TYPE_YEAR;
                break; 
            case Membership::STATUS_ACTIVE_TWO_YEAR: 
				return Payment::PAYMENT_TYPE_TWO_YEAR;
                break; 
            case Membership::STATUS_ACTIVE_THREE_YEAR:
				return Payment::PAYMENT_TYPE_THREE_YEAR;
                break; 
				
            case Membership::STATUS_ACTIVE_LIFETIME: 
				return Payment::PAYMENT_TYPE_LIFETIME;
                break; 
            case Membership::STATUS_ACTIVE_INDIVIDUAL: 
				return Payment::PAYMENT_TYPE_INDIVIDUAL;
                break; 
        }
		//returns null if by default
			return null;
		
	}
	/*
	 * Not in Use
	*/
	public function getType3()
	{
		//Takes the pruduct id that is bieng sent with the JvZoo request
		$productItem = strtolower(isset($this->params['cproditem']) ? $this->params['cproditem'] : null);
		//Looking from the product ID against the keywords provided in the beggining of this file 
        foreach (self::$_monthlyKeywords as $key) {
            if (strpos($productItem, $key) !== false) {
                return Payment::PAYMENT_TYPE_MONTH;
            }
        }
        foreach (self::$_quarterlyKeywords as $key) {
            if (strpos($productItem, $key) === 0) {
                return Payment::PAYMENT_TYPE_QUARTER;
            }
        }
        foreach (self::$_yearlyKeywords as $key) {
            if (strpos($productItem, $key) === 0) {
                return Payment::PAYMENT_TYPE_YEAR;
            }
        }
        foreach (self::$_twoyearKeywords as $key) {
            if (strpos($productItem, $key) === 0) {
                return Payment::PAYMENT_TYPE_TWO_YEAR;
            }
        }
        foreach (self::$_threeyearKeywords as $key) {
            if (strpos($productItem, $key) === 0) {
                return Payment::PAYMENT_TYPE_THREE_YEAR;
            }
        }
        foreach (self::$_lifetimeKeywords as $key) {
            if (strpos($productItem, $key) === 0) {
                return Payment::PAYMENT_TYPE_LIFETIME;
            }
        }
        //Return null by default
        return null;
	}
    //HB//END
    public function getType() {
        // product title may change - check only the first part
        $productTitle = strtolower(isset($this->params['cprodtitle']) ? $this->params['cprodtitle'] : null);
		
        foreach (self::$_monthlyKeywords as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Payment::PAYMENT_TYPE_MONTH;
            }
        }
        foreach (self::$_quarterlyKeywords as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Payment::PAYMENT_TYPE_QUARTER;
            }
        }
        foreach (self::$_yearlyKeywords as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Payment::PAYMENT_TYPE_YEAR;
            }
        }
		//HB//START
        foreach (self::$_twoyearKeywords as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Payment::PAYMENT_TYPE_TWO_YEAR;
            }
        }
        foreach (self::$_threeyearKeywords as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Payment::PAYMENT_TYPE_THREE_YEAR;
            }
        }
		//HB//END
        foreach (self::$_lifetimeKeywords as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Payment::PAYMENT_TYPE_LIFETIME;
            }
        }
        foreach (self::$_individualKeywords as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Payment::PAYMENT_TYPE_INDIVIDUAL;
            }
        }
        foreach (self::$_prolifetimeKeywords as $key) {
            if (strpos($productTitle, strtolower($key)) === 0) {
                return Payment::PAYMENT_TYPE_PRO_LIFETIME;
            }
        }
        foreach (self::$_proagencyKeywords as $key) {
            if (strpos($productTitle, strtolower($key)) === 0) {
                return Payment::PAYMENT_TYPE_AGENCY;
            }
        }
		
        foreach (self::$_lifetimeupgradeKeywords as $key) {
            if (strpos($productTitle, strtolower($key)) === 0) {
                return Payment::PAYMENT_TYPE_LIFETIME_UPGRADE;
            }
        }
        
        return null;
    }

    //HB//END
	
	//US//START
	public function isPackageUpgrade(){
        // product title may change - check only the first part
        $productTitle = strtolower(isset($this->params['cprodtitle']) ? $this->params['cprodtitle'] : null);
	
        foreach (self::$_lifetimeupgradeKeywords as $key) {
            if (strpos($productTitle, strtolower($key)) === 0) {
                return Payment::PAYMENT_TYPE_LIFETIME_UPGRADE;
            }
        }
		return null;
	}
	
    public function getPackage() {
        // product title may change - check only the first part
        $productTitle = strtolower(isset($this->params['cprodtitle']) ? $this->params['cprodtitle'] : null);
		
        foreach (self::$_proagencyKeywords as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Package::PACKAGE_TYPE_AGENCY;
            }
        }
        foreach (self::$_packagesFullAccessKeywords as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Package::PACKAGE_TYPE_AGENCY;
            }
        }
        foreach (self::$_packagesFullAccessKeywordsWithTabs as $key) {
            if (strpos($productTitle, $key) === 0) {
                return Package::PACKAGE_TYPE_TEN_TABS_FULLACCESS;
            }
        }
		
		$packagePart = trim(explode('tabfupro lifetime', $productTitle)[1]);
		
		if(empty($packagePart))
		{
			$packagePart = trim(explode('tabfu lifetime', $productTitle)[1]);
		}

		$package =str_replace(' ','-',$packagePart);
		return $package;

    }
	//US//END

    
    public function getSecretKey() {
        return $this->secretKey;
    }

    public function setSecretKey($secretKey) {
        $this->secretKey = $secretKey;
        return $this;
    }
    
    public function setParams($params = array()) {
        if(!is_array($params)) {
            $params = array($params);
        }
        $goodParams = array();
        foreach ($params AS $key => $value) {
            if(!in_array($key, self::$availableParams)) {
                continue;
            }
            $goodParams[$key] = $value;
        }

        $this->params = $goodParams;
        return $this;
    }

}

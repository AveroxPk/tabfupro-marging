<?php

abstract class PaymentAdapter implements PaymentAdapterInterface {

    /**
     * Post parameters
     * @var array
     */
    protected $params;
    
    /**
     * Constructor
     * @param array $params
     */
    function __construct($params = array()) {
        $this->setParams($params);
    }

    /**
     * Function to verify response from Payments
     * @return boolean
     */
    abstract public function verify();
    
    /**
     * Return name of adapter
     * @return string
     */
    abstract public function getName();
    
    /**
     * Return transaction identifier
     * @return string
     */
    abstract public function getTransactionId();
    
    /**
     * Return transaction type
     * @return string
     */
    abstract public function getTransactionType();
    
    /**
     * Return transaction amount
     * @return integer
     */
    abstract public function getTransactionAmount();
    
    /**
     * Return transaction payment method
     * @return integer
     */
    abstract public function getTransactionPaymentMethod();
    
    /**
     * Return customer email
     * @return string
     */
    abstract public function getCustomerEmail();
    
    /**
     * Return customer name
     * @return string
     */
    abstract public function getCustomerName();
    
    /**
     * Return type of product (MONTH,YEAR,LIFETIME)
     * @return string
     */
    abstract public function getType();
    
    /**
     * Return true if payment will be recurring
     * @return boolean
     */
    abstract public function isRecurring();
    
    
    public function getParams() {
        return $this->params;
    }

    public function setParams($params = array()) {
        $this->params = is_array($params) ? $params : array($params);
        return $this;
    }

}
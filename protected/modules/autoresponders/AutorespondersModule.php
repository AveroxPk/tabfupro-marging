<?php

class AutorespondersModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'autoresponders.models.*',
			'autoresponders.components.*',
            'autoresponders.components.aweber.*',
            'autoresponders.components.getResponse.*',
            'autoresponders.components.iContact.*',
            'autoresponders.components.infusionSoft.*',
            'autoresponders.components.mailChimp.*',
            'autoresponders.components.sendReach.*',
		));
        
        Yii::app()->getModule('user');
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
    
    private function _getAutoresponderClassNamesAvailable()
    {
        return array(
            'AutoresponderAweber',
            'AutoresponderGetresponse',
            'AutoresponderIcontact',
            'AutoresponderMailchimp',
            'AutoresponderSendreach',
            'AutoresponderInfusionsoft'
        );
    }
    
    private $_autorespondersCache = array();
    public function getAutorespondersForUser($user_id = null)
    {
        if ($user_id === null) {
            $user_id = Yii::app()->user->id;
        }
        
        if (!array_key_exists($user_id, $this->_autorespondersCache)) {
            $autoresponders = [];
            foreach ($this->_getAutoresponderClassNamesAvailable() as $autoresponderName) {
                $autoresponders[$autoresponderName] = $autoresponderName::model()->findByAttributes(array(
                    'user_user_id' => $user_id
                )) ? : new $autoresponderName;
            }
            $this->_autorespondersCache[$user_id] = $autoresponders;
        }
        
        return $this->_autorespondersCache[$user_id];
        
    }
    
    public function addSubscriberToTabAutoresponders($tab, $mail, $name = '', $lastName = '')
    {        
        $tabOwnerId = $tab->userFacebook->user_id;
        
        $autoresponderConnections = TabAutoresponders::model()->findAllByAttributes(array(
            'tab_id' => $tab->id
        ));
        
        $failedAutoresponders = [];
        foreach ($autoresponderConnections as $autoresponderConnection) {
            $autoresponder = call_user_func(
                array(
                    $autoresponderConnection->autoresponder_name,
                    'model'
                )
            )->findByAttributes(array(
                'user_user_id' => $tabOwnerId
            ));
            if (!$autoresponder->addSubscriber($name, $lastName, $mail, $autoresponderConnection->list_id)) {
                $failedAutoresponders[] = str_replace('Autoresponder', '', $autoresponderConnection->autoresponder_name);
            }
        }
        
        return $failedAutoresponders;

    }
    
    public function isAutoresponderConnectedToTab($tab, $autoresponderName) 
    {
        $model = TabAutoresponders::model()->findByAttributes(array(
            'tab_id' => $tab->id,
            'autoresponder_name' => $autoresponderName
        ));
        
        return $model !== null;
    }
    
    public function connectAutoresponderToTab($tab, $autoresponderName, $listId, $listName)
    {           
        $model = TabAutoresponders::model()->findByAttributes(array(
            'tab_id' => $tab->id,
            'autoresponder_name' => $autoresponderName
        ));
        
        if ($model === null) {
            $model = new TabAutoresponders;
            $model->tab_id = $tab->id;
            $model->autoresponder_name = $autoresponderName;
        }
        
        $model->list_name = $listName;
        $model->list_id = $listId;
        return $model->save(false);
        
    }
    
    public function disconnectAutoresponderFromTab($tab, $autoresponderName)
    {
        // delete the autoresponders settings
        $autoresponders = $this->getAutorespondersForUser();
        if (in_array($autoresponderName, $this->_getAutoresponderClassNamesAvailable()) &&
            isset($autoresponders[$autoresponderName])) {
            $autoresponders[$autoresponderName]->delete();
        }
        return TabAutoresponders::model()->deleteAll(
            'tab_id=? AND autoresponder_name=?', 
            array($tab->id, $autoresponderName)
        ) > 0;
    }

    
}

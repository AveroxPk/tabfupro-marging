<?php

class SettingsController extends Controller
{
    
    public function beforeAction($action) 
    {
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(403, 'Forbidden.');
        }
        
        if (isset($_GET['autoresponderName']) && !in_array($_GET['autoresponderName'], array(
            'AutoresponderGetresponse',
            'AutoresponderAweber',
            'AutoresponderIcontact',
            'AutoresponderInfusionsoft',
            'AutoresponderMailchimp',
            'AutoresponderSendreach'
        ))) {
            throw new CHttpException(404, 'Autoresponder not found.');
        }
        
        return parent::beforeAction($action);
    }
    
    public function actionConnect($autoresponderName)
    {   
        Yii::app()->getModule('user');
        
        $autoresponderModel = new $autoresponderName;
        if ($autoresponderModel->exists('user_user_id=?', array(Yii::app()->user->id))) {
            $autoresponderModel = $autoresponderModel->findByAttributes(array('user_user_id' => Yii::app()->user->id));
        }
        
        $skipSaving = false;
        
        //Omit saving Aweber settings again if access code is same as previous one.
        if (
            $autoresponderName == 'AutoresponderAweber' && 
            isset($_POST[$autoresponderName]['access_code']) && 
            $autoresponderModel->access_code == $_POST[$autoresponderName]['access_code']
        ) {
            $skipSaving = true;
        }
        
        if (!$skipSaving) {
            $autoresponderModel->attributes = $_POST[$autoresponderName];
            $autoresponderModel->user_user_id = Yii::app()->user->id;
            if (!$autoresponderModel->validate()) {
                throw new CHttpException(422, "The autoresponder configuration is invalid.");
            }
            $lists = $autoresponderModel->getLists();
            // don't allow to connect an autoresponder with no lists
            if (!$lists || count($lists) <= 0) {
                throw new CHttpException(500, "You must create a list in your auto responder account first before you can connect.");
            }
            if (!$autoresponderModel->save(false)) {
                throw new CHttpException(500, "Database error. Please try again later.");
            }
        }
        
        $this->renderPartial('template.views.pageEditor.__autoresponderLists', array('model' => $autoresponderModel));
        
    }
       
}
<?php

interface AutorespondersInterface 
{
    public function addSubscriber($name, $lastname, $mail, $list);
    public function getLists();
    public function getDisplayName();
    
}
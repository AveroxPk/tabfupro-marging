<?php

/**
 * This is the model class for table "autoresponder_infusionsoft".
 *
 * The followings are the available columns in table 'autoresponder_infusionsoft':
 * @property integer $user_user_id
 * @property string $app_name
 * @property string $app_key
 *
 * The followings are the available model relations:
 * @property User $userUser
 */
class AutoresponderInfusionsoft extends CActiveRecord implements AutorespondersInterface
{
    const OPT_IN_REASON = 'TabFu Facebook Gate';
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'autoresponder_infusionsoft';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_user_id', 'required'),
			array('user_user_id', 'numerical', 'integerOnly'=>true),
			array('app_name, app_key', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_user_id, app_name, app_key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userUser' => array(self::BELONGS_TO, 'User', 'user_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_user_id' => 'User User',
			'app_name' => 'App Name',
			'app_key' => 'App Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_user_id',$this->user_user_id);
		$criteria->compare('app_name',$this->app_name,true);
		$criteria->compare('app_key',$this->app_key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutoresponderInfusionsoft the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    
    public function addSubscriber($name, $lastname, $mail, $list)
    {
        try {
            /* @var $infusionSoftCrmApi InfusionSoftCrmApiConnection */
            $infusionSoftCrmApi = Yii::app()->infusionSoftCrm->createApiConnection($this->app_name, $this->app_key);
            
            $contactData = array(
                'Email' => $mail,
            );
            
            if (!empty($name)) {
                $contactData['FirstName'] = $name;
            }
            
            if (!empty($name)) {
                $contactData['LastName'] = $lastname;
            }
            
            $contactId = $infusionSoftCrmApi->addContact(
                $contactData, 
                self::OPT_IN_REASON, 
                InfusionSoftCrmApiConnection::CONTACT_DUPLICATION_EMAIL
            );
            $infusionSoftCrmApi->addTagToContact($contactId, $list);
            
            return true;
        }catch (Exception $ex){
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderInfusionsoft');
            return false;
        }
    }

    public function getLists()
    {
        try {
            /* @var $infusionSoftCrmApi InfusionSoftCrmApiConnection */
            $infusionSoftCrmApi = Yii::app()->infusionSoftCrm->createApiConnection($this->app_name, $this->app_key);
            return $infusionSoftCrmApi->listContactTags();
            
        }catch(Exception $ex){
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderInfusionsoft');
            return false;
        }
    }
    
    public function getDisplayName() 
    {
        return 'Infusionsoft';
    }
    
    public function beforeValidate(){

        if (!parent::beforeValidate()) {
            return false;
        }
        
        try{
            Yii::app()->infusionSoftCrm->createApiConnection($this->app_name, $this->app_key);

            return true;
        }catch(Exception $ex){
            $this->app_name = '';
            $this->app_key = '';

            return false;
        }
    }
    
}

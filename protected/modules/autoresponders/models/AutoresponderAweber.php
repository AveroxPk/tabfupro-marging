<?php


/**
 * This is the model class for table "autoresponder_aweber".
 *
 * The followings are the available columns in table 'autoresponder_aweber':
 * @property integer $user_user_id
 * @property string $access_code
 *
 * The followings are the available model relations:
 * @property User $userUser
 */
class AutoresponderAweber extends CActiveRecord implements AutorespondersInterface
{
        
    

        private $_api;
        private $_appId;
        
        
        public function init() {
                        
            $this->_appId = "25442493";

        }

        
        public function getAppid(){
            return $this->_appId;
        }
    
    
    
    
    
        /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'autoresponder_aweber';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_user_id', 'required'),
			array('user_user_id', 'numerical', 'integerOnly'=>true),
			array('access_code', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_user_id, access_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userUser' => array(self::BELONGS_TO, 'User', 'user_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_user_id' => 'User User',
			'access_code' => 'Access Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_user_id',$this->user_user_id);
		$criteria->compare('access_code',$this->access_code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutoresponderAweber the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        
        
        
    /**
    * Add responder to Aweber service
    * @param type $name
    * @param type $lastname
    * @param type $mail
    */
    public function addSubscriber($name, $lastname, $mail, $list) {
        try{
            $api = new AWeberAPI($this->consumer_key, $this->consumer_secret);
            $this->_api = $api->getAccount($this->access_key, $this->access_secret);
            $listURL = $this->_api->url."/lists/".$list;
            $list = $this->_api->loadFromUrl($listURL);
            $params = array(
                'email' => $mail,
                'name' => trim($name." ".$lastname)
            );
            $subscribers = $list->subscribers;
            $new_subscriber = $subscribers->create($params);
            return true;
        }catch (Exception $ex){
            		
            //Subscriber already subscribed? That's ok!
            if (strpos($ex->getMessage(), 'Subscriber already subscribed') !== false) {
                    return true;
            }
            
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderAweber');
            
            return false;
        }
    }


    /**
     * connect to service
     */
    
    public function beforeValidate(){

        if (!parent::beforeValidate()) {
            return false;
        }
        
        try{
             $auth = AWeberAPI::getDataFromAweberID(trim($this->access_code));

             $this->consumer_key = $auth[0];

             $this->consumer_secret = $auth[1];
             $this->access_key = $auth[2];
             $this->access_secret = $auth[3];

             $api = new AWeberAPI($this->consumer_key, $this->consumer_secret);
             $this->_api = $api->getAccount($this->access_key, $this->access_secret);

             return true;
        }catch(Exception $ex){
             $this->consumer_key = "";

             $this->consumer_secret = "";
             $this->access_key = "";
             $this->access_secret = "";

            return false;
        }
    }

    /**
     * get lists avaible to subscribe
     */
    public function getLists(){
        try{
             $api = new AWeberAPI($this->consumer_key, $this->consumer_secret);
             $this->_api = $api->getAccount($this->access_key, $this->access_secret);
             $lists = $this->_api->lists;
             $returnArray = array();
             foreach($lists->data["entries"] as $list){
                 $returnArray[$list["id"]] = $list["name"];
             }

             return $returnArray;
        } catch (Exception $ex) {
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderAweber');
                
            return false;
        }


    }

    public function getDisplayName() 
    {
        return 'AWeber';
    }

    
}

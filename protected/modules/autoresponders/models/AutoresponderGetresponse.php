<?php

/**
 * This is the model class for table "autoresponder_getresponse".
 *
 * The followings are the available columns in table 'autoresponder_getresponse':
 * @property integer $user_user_id
 * @property string $api_key
 *
 * The followings are the available model relations:
 * @property User $userUser
 */
class AutoresponderGetresponse extends CActiveRecord implements AutorespondersInterface
{
    
    
        private $_api_url = 'http://api2.getresponse.com';
        private $_api = '';
        
        
        function init() {
            $this->_api = new getResponse($this->_api_url);
            
        }

        
        
        
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'autoresponder_getresponse';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_user_id', 'required'),
			array('user_user_id', 'numerical', 'integerOnly'=>true),
			array('api_key', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_user_id, api_key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userUser' => array(self::BELONGS_TO, 'User', 'user_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_user_id' => 'User User',
			'api_key' => 'Api Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_user_id',$this->user_user_id);
		$criteria->compare('api_key',$this->api_key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutoresponderGetresponse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        

        
        
        
        
        
        
        public function addSubscriber($name, $lastname, $mail, $list) {
            
            // http://apidocs.getresponse.com/en/api/1.5.0/Contacts/add_contact
            if (!$this->api_key) {
                return false;
            }
            
            try{
                $return = $this->_api->add_contact(
                    $this->api_key,
                    array(
                        'campaign'=>$list,
                        'email'=>$mail,
                        'name'=>trim($name.' '.$lastname),
                        'cycle_day'=>'0'
                    )
                );
                return true;
            }catch(RuntimeException $e){
                
                //Contact already exists
                if (strpos($e->getMessage(), 'Contact already queued for target campaign') !== false) {
                    return true;
                }
                
                Yii::log(CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderGetresponse');
                
                return false;
            }
            
        }

        public function connect() {
            
            
        }

        public function getLists() {
            if (!$this->api_key) {
                return null;
            }
            
            try{
                $campaigns = $this->_api->get_campaigns(
                    $this->api_key

                );
                $return = array();
                foreach ($campaigns as $id => $campaign){
                    $return[$id]=$campaign["name"];
                }
                return $return;
            }catch(RuntimeException $e){
                Yii::log(CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderGetresponse');
                
                return null;
            }
        }

        public function getSubscribers() {
            
        }

        public function isConfigured() {
            
        }
        
       public function getDisplayName() 
       {
           return 'GetResponse';
       }

}

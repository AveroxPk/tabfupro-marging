<?php

/**
 * This is the model class for table "autoresponder_sendreach".
 *
 * The followings are the available columns in table 'autoresponder_sendreach':
 * @property integer $user_user_id
 * @property string $api_user_id
 * @property string $api_key
 * @property string $api_secret
 *
 * The followings are the available model relations:
 * @property User $userUser
 */
class AutoresponderSendreach extends CActiveRecord implements AutorespondersInterface
{
    
    const SENDREACH_API_URL = 'http://api.sendreach.com/index.php';
    
    private function _callApi($action, $params = array())
    {
        $commonParams = array(
            'key' => $this->api_key,
            'secret' => $this->api_secret,
            'user_id' => $this->api_user_id,
            'action' => $action,
        );
        
        $requestUrl = self::SENDREACH_API_URL . '?' .  http_build_query(array_merge($commonParams, $params));
        
        $responseRaw = file_get_contents($requestUrl);
        $responseData = json_decode($responseRaw, true);
        if (array_key_exists('status', $responseData) && $responseData['status'] == 'error') {
            throw new CException('SendReach API error: "' . $responseData['error'] . '"');
        }
        
        return $responseData;
        
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'autoresponder_sendreach';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_user_id', 'required'),
			array('user_user_id', 'numerical', 'integerOnly'=>true),
			array('api_user_id, api_key', 'length', 'max'=>256),
			array('api_secret', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_user_id, api_user_id, api_key, api_secret', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userUser' => array(self::BELONGS_TO, 'User', 'user_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_user_id' => 'User User',
			'api_user_id' => 'Api User',
			'api_key' => 'Api Key',
			'api_secret' => 'Api Secret',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_user_id',$this->user_user_id);
		$criteria->compare('api_user_id',$this->api_user_id,true);
		$criteria->compare('api_key',$this->api_key,true);
		$criteria->compare('api_secret',$this->api_secret,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutoresponderSendreach the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    
    public function addSubscriber($name, $lastname, $mail, $list)
    {
        $ip = isset($_SERVER['REMOTE_ADDR']) ? : '';
        
        try {
            $this->_callApi('subscriber_add', array(
                'list_id' => $list,
                'first_name' => $name,
                'last_name' => $lastname,
                'email' => $mail,
                'client_ip' => $ip
            ));
            return true;
        } catch (Exception $ex) {
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderSendreach');
            return false;
        }
    }

    public function getLists()
    {
        try {
            $apiResponse = $this->_callApi('lists_view');
            $lists = array();
            foreach ($apiResponse as $listData) {
                $lists[$listData['id']] = $listData['list_name'];
            }
            return $lists;
        } catch (Exception $e) {
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderSendreach');
            return array();
        }
    }

    public function getDisplayName() 
    {
        return 'SendReach';
    }
    
    public function beforeValidate()
    {
        if (!parent::beforeValidate()) {
            return false;
        }
        
        try {
            $this->getLists();
        } catch (Exception $ex) {
            
            return false;
        }
        
        return true;
        
    }
    
}

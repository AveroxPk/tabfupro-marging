<?php

/**
 * This is the model class for table "autoresponder_icontanct".
 *
 * The followings are the available columns in table 'autoresponder_icontanct':
 * @property integer $user_user_id
 * @property string $api_pass
 *
 * The followings are the available model relations:
 * @property User $userUser
 */
class AutoresponderIcontact extends CActiveRecord implements AutorespondersInterface
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'autoresponder_icontact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_user_id', 'required'),
			array('user_user_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_user_id, app_login, app_id, app_pass', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userUser' => array(self::BELONGS_TO, 'User', 'user_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_user_id' => 'User User',
			'app_pass' => 'Api Pass',
                        'app_login' => 'Api Login',
                        'app_id' => 'App Id',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_user_id',$this->user_user_id);
		$criteria->compare('api_pass',$this->api_pass,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutoresponderIcontanct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
    
    public function addSubscriber($name, $lastname, $mail, $list)
    {
        try {
            iContact::getInstance()->setConfig(array(
                'appId'       => $this->app_id,
                'apiPassword' => $this->app_pass, 
                'apiUsername' => $this->app_login,
            ));

            // Store the singleton
            $oiContact = iContact::getInstance();
            // Valid statuses
            $subscriber = $oiContact->addContact($mail, null, null, $name, $lastname);
            $return = $oiContact->subscribeContactToList($subscriber->contactId, $list, 'normal');
        }  catch (Exception $ex){
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderIcontact');
            return false;
        }

        return true;
    }

    public function getLists()
    {
        try {
            iContact::getInstance()->setConfig(array(
            'appId'       => $this->app_id,
            'apiPassword' => $this->app_pass, 
            'apiUsername' => $this->app_login,
            ));

            // Store the singleton
            $oiContact = iContact::getInstance();
            $lists = $oiContact->getLists();
            $returnArray = array();
            foreach($lists as $list ){
                $returnArray[$list->listId] = $list->name;
            }
            return $returnArray;
        } catch (Exception $oException) { // Catch any exceptions
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderIcontact');
            return false;
        }
    }

    public function getDisplayName() 
    {
        return 'iContact';
    }
}

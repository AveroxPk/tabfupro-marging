<?php

/**
 * This is the model class for table "autoresponder_mailchimp".
 *
 * The followings are the available columns in table 'autoresponder_mailchimp':
 * @property integer $user_user_id
 * @property string $api_key
 *
 * The followings are the available model relations:
 * @property User $userUser
 */
class AutoresponderMailchimp extends CActiveRecord implements AutorespondersInterface
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'autoresponder_mailchimp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_user_id', 'required'),
			array('user_user_id', 'numerical', 'integerOnly'=>true),
			array('api_key', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_user_id, api_key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userUser' => array(self::BELONGS_TO, 'User', 'user_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_user_id' => 'User User',
			'api_key' => 'Api Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_user_id',$this->user_user_id);
		$criteria->compare('api_key',$this->api_key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutoresponderMailchimp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
    public function addSubscriber($name, $lastname, $mail, $list)
    {
        try {
            $mailchimp = new MailChimp($this->api_key);
            $params = [
                "apikey"=>$this->api_key,
                "id"=>$list,
                "email"=>[
                    "email"=>$mail
                ]
            ];
            if ($name && strlen($name) > 0) {
                $params['merge_vars']['MERGE1'] = $name.($lastname && strlen($lastname) > 0 ? ' '.$lastname : '');
            }
            $addSubscriber = $mailchimp->call("/lists/subscribe", $params);
            if ($addSubscriber) {
                if (isset($addSubscriber['status']) && $addSubscriber['status'] == 'error') {
                    Yii::log($addSubscriber['name'].' ('.$addSubscriber['code'].') - '.$addSubscriber['error'], CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderMailchimp');
                    return false;
                }
                return true;
            }
            else {
                return false;
            }
        }catch (Exception $ex){
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderMailchimp');
            return false;
        }
    }

    public function getLists()
    {
        try{
            $mailchimp = new MailChimp($this->api_key);
            $list = $mailchimp->call('lists/list', [
                        "api_key"=>$this->api_key
                    ]);
            $returnArray = array();

            if (empty($list["data"])) {
                return [];
            }
            
            foreach($list["data"] as $list){
                $returnArray[$list["id"]] = $list["name"];
            }
            return $returnArray;
        }catch(Exception $ex){
            Yii::log(CVarDumper::dumpAsString($ex), CLogger::LEVEL_ERROR, 'application.modules.autoresponders.models.AutoresponderMailchimp');
            return false;
        }
    }
        
    public function getDisplayName() 
    {
        return 'MailChimp';
    }
}

<?php
/**
 * This class handles communication with InfusionSoft CRM API.
 * @author Wiktor Toporek
 */
class InfusionSoftCrmApiConnection
{
    private $_appName;
    private $_apiKey;
    private $_xmlrpcClient;
    private $_defaultOptInReason = 'API Opt In';
    
    const CONTACT_DUPLICATION_IGNORE = '';
    const CONTACT_DUPLICATION_EMAIL = 'Email';
    
    public function __construct($appName, $apiKey, $verifyConnection = true) 
    {
        $this->_appName = $appName;
        $this->_apiKey = $apiKey;
        $this->_xmlrpcClient = new xmlrpc_client("https://{$this->_appName}.infusionsoft.com/api/xmlrpc");
        $this->_xmlrpcClient->return_type = 'phpvals';
        $this->_xmlrpcClient->setSSLVerifyPeer(false);
        $this->_xmlrpcClient->verifyhost = 2;
        
        if ($verifyConnection && !$this->_verifyConnection()) {
            throw new CException("Couldn't connect to the InfusionSoft CRM API. Perhaps invalid credentials were provided?");
        }
        
    }
    
    public function dsGetSetting($module, $setting)
    {
        return $this->apiCall('DataService.getAppSetting', func_get_args());
    }
    
    public function addContact($contactData, $optReason = null, $checkDuplication = self::CONTACT_DUPLICATION_IGNORE)
    {
        $apiParams = array(
            php_xmlrpc_encode($contactData, array('auto_dates'))
        );
        
        if ($checkDuplication) {
            $apiParams[] = php_xmlrpc_encode($checkDuplication);
        }
        
        $result = $this->apiCall(
            $checkDuplication ? 'ContactService.addWithDupCheck' : 'ContactService.add', 
            $apiParams, 
            true
        );
        
        if (!empty($contactData['Email'])) {
            $this->optIn($contactData['Email'], $optReason);
        }
        
        return $result;
    }
    
    public function dsQuery($table, $limit, $page, $query, $returnFields)
    {
        return $this->apiCall('DataService.query', func_get_args());
    }
    
    public function listCampaigns()
    {
        $mappedResult = array();
        $campaignsList = $this->dsQuery('Campaign', 1000, 0, array('Id' => '%'), array('Id', 'Name'));
        
        foreach ($campaignsList as $campaign) {
            $mappedResult[$campaign['Id']] = $campaign['Name'];
        }
        
        
        return $mappedResult;
    }
    
    public function listContactTags()
    {
        $mappedResult = array();
        $campaignsList = $this->dsQuery('ContactGroup', 1000, 0, array('Id' => '%'), array('Id', 'GroupName'));
        
        foreach ($campaignsList as $campaign) {
            $mappedResult[$campaign['Id']] = $campaign['GroupName'];
        }
        
        
        return $mappedResult;
    }
    
    public function addContactToCampaign($contactId, $campaignId)
    {
        return $this->apiCall('ContactService.addToCampaign', func_get_args());
    }
    
    public function addTagToContact($contactId, $tagId)
    {
        return $this->apiCall('ContactService.addToGroup', func_get_args());
    }
    
    public function optIn($email, $reason = null) 
    {
        if ($reason === null) {
            $reason = $this->_defaultOptInReason;
        }
        
        return $this->apiCall('APIEmailService.optIn', array($email, $reason));
        
    }
    
    private function _verifyConnection()
    {
        try {
            $this->dsGetSetting('Contact', 'optiontypes');
        } catch (CException $e) {
            return false;
        }
        
        return true;
    }
    
    protected function apiCall($service, $params, $rawParams = false)
    {
        if (!$rawParams) {
            $params = array_map('php_xmlrpc_encode', $params);
        }
        
        array_unshift($params, php_xmlrpc_encode($this->_apiKey));
        
        $call = new xmlrpcmsg($service, $params);
        $result = $this->_xmlrpcClient->send($call);
        
        if ($result->faultCode()) {
            throw new CException('InfusionSoft CRM API error code: ' . $result->faultCode());
        }
        
        return $result->value();
        
    }
    
    
    
}
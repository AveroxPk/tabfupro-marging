<?php

class InfusionSoftCrm extends CApplicationComponent
{
    public $verifyConnection = true;
    
    public function init()
    {
        if (!function_exists('xmlrpc_encode_entitites')) {
            require_once __DIR__ . '/vendor/xmlrpc-3.0/lib/xmlrpc.inc';
        }
        
        require_once __DIR__ . '/InfusionSoftCrmApiConnection.php';
        
    }
    
    public function createApiConnection($appName, $apiKey)
    {
        return new InfusionSoftCrmApiConnection($appName, $apiKey, $this->verifyConnection);
    }
    
}
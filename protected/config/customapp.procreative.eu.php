<?php
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap'); 
return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
        'timeZone' => 'Europe/Warsaw',
        'modules' => array(
            'template' => array(
                'repositoryAdapterConfig' => array(
                    'class' => 'LocalRepositoryAdapter',
                    'pattern' => array(
                        'path' => '/var/www/procreative.eu/subdomains/facebookapp/public_html/uploads/',
                        'url' => 'https://customapp.procreative.eu/uploads/'
                    )
                )
            )
        ),
        'components'=>array(
            'db'=>array(
                'connectionString' => 'mysql:host=localhost;dbname=postmotion',
                'emulatePrepare' => true,
                'username' => 'postmotion',
                'password' => 'postmotion',
                'charset' => 'utf8',
            ),
            'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
                            'https://<subdomain:\w+>.customapp.procreative.eu/template/gates/rendercustomcode' => '/template/gates/rendercustomcode',

			),
            ),
        ),
        'params'=>array(
            // this is used in contact page
            'adminEmail'=>'postmotion@procreative.eu',
            'infoEmail'=>'bartek@procreative.eu',
            'JvzipnSecretKey'=>'y.^1Cpd178R*m;k',
            'ZaxaaApiSignature'=>'XG32E3QB11911ZN9',
            'WarriorPaymentsAllowedIps'=>array(
                '54.86.40.226', // payments.warriorforum
                '194.9.53.167', 
            ),
            'WarriorPaymentsHost'=>'https://payments.warriorforum.com',
            'disableRegistration'=>true,
            'FacebookAppId'  => '243962189122234',
            'FacebookSecret' => '52f0f3f075c747caa7e96d7f1197462c',
            'FacebookTabs'=>array(
                '228619787328674'=>'05d91e8189528b27d2114e5af81cd70d',
                '769067959779418'=>'273028b6d1c2113972a458b11049535c',
                '1410991482512800'=>'d381dc9886a1732300d391407afb17b4',
                '645983282149269'=>'67068d9cf97980a97577c05e21bafa18',
                '575417055904277'=>'090ec1547966b65f0eff67b52397836a',
                '854985254530200'=>'4ae637c15ff7c3e499e80146078ecb01',
                '649213391820764'=>'907a7f55bf31256c9d5da2fc7b35bc6e',
                '645544988865148'=>'370c354759a43139e91fcabdf5b1711b',
                '776523482381685'=>'28fa86342d39102eb24ae7ac7968a355',
                '227292140813061'=>'30bfa574e67c16982180237803c8ccd9',


            ),
            
        ),
	)
);

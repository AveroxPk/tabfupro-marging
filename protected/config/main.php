<?php
//echo 'addes'; exit;
//session_start();

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
//echo $_SERVER['REQUEST_URI'];
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'TabFuPro',
    'theme' => 'classic',

    // preloading 'log' component
    'preload' => array('log'),
    
    // maintenance mode support
    'catchAllRequest' => 
            strpos($_SERVER['REQUEST_URI'], '/showtab') === false && 
            strpos($_SERVER['REQUEST_URI'], '/template/gates') === false &&
            file_exists(dirname(__FILE__) . '/../../.maintenance') ?
        array(
            '/site/maintenance',
            'time' => file_get_contents(dirname(__FILE__) . '/../../.maintenance')
        ) :
        null,

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.yii-mail.YiiMailMessage',
    ),

    'modules' => array(
        'analytics',
        'migration',
        'user' => array(
            'defaultController' => 'home',
        ),
        'message',
        'membership',
        'admin',
        'facebook',
		'salesforce',
        'template' => array(),
        'autoresponders',
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'pass',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1', '194.9.53.167'),
        ),

    ),

    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('/login')
        ),
        
        'infusionSoftCrm' => array(
            'class' => 'ext.infusionsoftcrm.InfusionSoftCrm'
        ),

        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                # basic menu options
                # these are module controller
                'facebookaccounts' => '/facebook/facebook/index',
                'fanpages' => '/facebook/fanPage/index',
                'motionpost' => '/facebook/motionpost/index',
                'analytics' => '/analytics/analytics/index',
                'tutorials' => '/site/tutorials',
				'salesforce' => '/salesforce/salesforce/index',

                #facebook module
                'facebook/like' => '/facebook/facebook/like',
                'facebook/clicklike' => '/facebook/facebook/clicklike',
                'addfacebookaccount/<token:\w+>' => '/facebook/facebook/addaccount',
                'addfacebookaccount' => '/facebook/facebook/addaccount',
                'showtab/<id:\d+>' => '/facebook/showtab/index',
                'publishtab' => '/facebook/fanPage/publishtab',
                'unpublishtab' => '/facebook/fanPage/unpublishtab',
                'cronschedule' => '/facebook/cron/crontabs',
                'm/<tabid:\d+>' => '/facebook/showtab/mobile',
                'createtab' => '/facebook/fanPage/createtab',
                'schedule' => '/facebook/fanPage/schedule',
                'copytab' => '/facebook/fanPage/copytab',
                'deletetab' => '/facebook/fanPage/deletetab',
                
                //'showad/<id:\d+>' => '/facebook/showtab/index',
                'publishad' => '/facebook/ads/publishad',
                'unpublishad' => '/facebook/ads/unpublishad',
                'createad' => '/facebook/ads/createad',
                'copyad' => '/facebook/ads/copyad',
                'deletead' => '/facebook/ads/deletead',

                #motion posts
                'uploadanimation' => '/facebook/motionpost/uploadanimation',
                'uploadmoviethumbajax' => '/facebook/motionpost/uploadmoviethumbajax',
                'uploadanimationajax' => '/facebook/motionpost/uploadanimationajax',
                'deleteanimationajax' => '/facebook/motionpost/deleteanimationajax',
                'deletemovieajax' => '/facebook/motionpost/deletemovieajax',
                'tooglefavoriteanimation' => '/facebook/motionpost/tooglefavorite',
                'showmotionpost/<id:\d+>/<fortest:\w+>' => '/facebook/showanimation/index',

                #motion post steps
                'createmotionpost-step-1' => '/facebook/motionpost/editorstep1',
                'createmotionpost-step-2' => '/facebook/motionpost/editorstep2',
                'createmotionpost-step-3' => '/facebook/motionpost/editorstep3',
                'createmotionpost-step-4' => '/facebook/motionpost/editorstep4',
                'createmotionpost-step-5' => '/facebook/motionpost/editorstep5',
                'createmotionpost-step-6' => '/facebook/motionpost/copymotionpost',
                'createmotionpost-step-7' => '/facebook/motionpost/editmotionpost',

                # motion post misc
                'changeanimationname' => '/facebook/motionpost/changeanimationname',
                'tooglefavoritemotionpost' => '/facebook/motionpost/tooglefavoritemotionpost',
                'deletemotionpost' => '/facebook/motionpost/deletemotionpost',
                'cronmotionpost' => '/facebook/cron/cronmotionposts',

                #user module
                
                'user/oauth'=> '/user/home/oauth',
                'logout' => '/user/home/logout',
                'login' => '/user/home/login',
                'check' => '/user/home/check',
                'forgotpassword' => '/user/home/forgetpassword',
                'changeprofile' => '/user/home/profile',
                'facebookLogin/<token:\w+>' => '/user/home/loginFacebook',
                'facebookLogin' => '/user/home/loginFacebook',

                #message
                'showmessage' => '/message/default/showmessage',
                'archivemessage' => '/message/default/archivemessage',
                'removefromarchivemessage' => '/message/default/removearchivemessage',
                'removemessage' => '/message/default/removemessage',
                'showarchive' => '/message/default/showarchive',
                'showinbox' => '/message/default/showinbox',

                #membership
                'membership'=>'/membership/index/index',
                'membership/payment/jvzipn'=>'/membership/payment/verifyjvzipn',
                'membership/payment/zaxaa'=>'/membership/payment/verifyzaxaa',
                'membership/payment/warrior'=>'/membership/payment/verifywarrior',
                'membershiprefresh'=>'/membership/cron/refresh',
                
                #custom code
                'https://<subdomain:\w+>.tabfu.online/template/gates/rendercustomcode' => '/template/gates/rendercustomcode',
                
                'migrate' => 'migration',
                'migrate/<action:\w+>/' => 'migration/default/<action>',
                #'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                #'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                #'<controller:\w+>/<id:\d+>'=>'<controller>/view',
                'migrate/<action:\w+>/<steps:\d+>' => 'migration/default/<action>/steps/<steps>',

            ),
        ),

        'session' => array (
            'class' => 'CDbHttpSession',
            'autoCreateSessionTable'=> false,
            'connectionID' => 'db',
            'sessionTableName' => 'yii_session',
            'timeout' => 300
        ),
        // standard cache enabled
        'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                    'maxFileSize' => 2048,
                    'maxLogFiles' => 20,
                    'except' => 'cron.*,application.modules.membership.*,application.modules.autoresponders.*,facebook.*',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                    'maxFileSize' => 2048,
                    'maxLogFiles' => 20,
                    'logFile' => 'aweber.log',
                    'categories' => 'cron.*',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                    'maxFileSize' => 2048,
                    'maxLogFiles' => 20,
                    'logFile' => 'membership.log',
                    'categories' => 'application.modules.membership.*',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                    'maxFileSize' => 2048,
                    'maxLogFiles' => 20,
                    'logFile' => 'autoresponders.log',
                    'categories' => 'application.modules.autoresponders.*',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                    'maxFileSize' => 2048,
                    'maxLogFiles' => 20,
                    'logFile' => 'facebook.log',
                    'categories' => 'facebook.*',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
        'mail'=>array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'no-reply@tabfu.online',
                'username' => 'akifquddus@gmail.com',
                'password' => 'Phefu8ut$uwufet',
                'port' => '587',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'senderEmail' => 'no-reply@tabfu.online',
        'senderEmailName' => 'TabFuPro',
        'mailAdapter' => 'standard',
        'Aweber' => array(
            'consumer_key'      => 'AzutsUPoZg4F1pSSH72N1DbE',
            'consumer_secret'   => 'ls1R8EIEcnX3Fdb0Rsl7v8QgnLTSupjB9GIP74jP',
            'access_key'        => 'AgMGpl0Maguj0FCKuj28Ehvq',
            'access_secret'     => 'wSTsjbyXQJ10wROhop8oeYdY6Hwp8kxHL9M14Z4z',
            // 'trial_list_name'   => 'making money with tabfu',
            'trial_list_name'   => 'Tabfu 7 Day Free Trial',
        ),
        'customCodeGate' => array(
            'allowedSubdomainPrefix'  => 'tabs',
        ),
    )
);

<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'), array(
        
        'timeZone' => 'Europe/Warsaw',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Post Motion',
        'theme' => 'classic',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		'migration', 
                'user'=>array(
                    'defaultController' => 'home',
                    
                ),
                'facebook',
                'template' => array(
                    'repositoryAdapterConfig' => array(
                        'class' => 'LocalRepositoryAdapter',
                        'pattern' => array(
                            'path' => '/var/www/uploads/',
                            'url' => 'http://tabfu.dev/uploads/'
                        )
                    )
                ),
                'membership',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'pass',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('*'),
		),

	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
                                #facebook module
                                'addfacebookaccount/<token:\d+>'=>'/facebook/facebook/addaccount',
                                'addfacebookaccount'=>'/facebook/facebook/addaccount',
				'facebookaccounts' => '/facebook/facebook/index',
                                'fanpages' => '/facebook/fanPage/index',
                                
                                #user module
                                'logout'=>'/user/home/logout',
                                'login'=>'/user/home/login',
                                'forgotpassword'=>'/user/home/forgetpassword',
                                'changeprofile'=>'/user/home/profile',
                            
                                'membership'=>'/membership/index/index',
                                'membership/payment/jvzipn'=>'/membership/payment/verifyjvzipn',
                                'membership/payment/zaxaa'=>'/membership/payment/verifyzaxaa',
                                'membershiprefresh'=>'/membership/cron/refresh',
                            
                                'migrate'=>'migration',
                                'migrate/<action:\w+>/'=>'migration/default/<action>',
                                'migrate/<action:\w+>/<steps:\d+>'=>'migration/default/<action>/steps/<steps>',
                                
                                'http://<subdomain:\w+>.tomek.localhost/template/gates/rendercustomcode' => '/template/gates/rendercustomcode',
                            
                                #'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				#'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				#'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=postmotion',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'postmotion@procreative.eu',
        'infoEmail'=>'bartek@procreative.eu',
        'JvzipnSecretKey'=>'y.^1Cpd178R*m;k',
        'ZaxaaApiSignature'=>'XG32E3QB11911ZN9',
        'WarriorPaymentsAllowedIps'=>array(
            '54.86.40.226', // payments.warriorforum
            '194.9.53.167',
            '127.0.0.1',
        ),
        'WarriorPaymentsHost'=>'https://payments.warriorforum.com',
        'disableRegistration'=>true,
        'FacebookAppId'  => '724076257630295',
        'FacebookSecret' => '393131e8c06bffbaeaf1d9f48337c3e4',
        'FacebookTabs'=>array(
            '228619787328674'=>'05d91e8189528b27d2114e5af81cd70d',
            '769067959779418'=>'273028b6d1c2113972a458b11049535c',
            '1410991482512800'=>'d381dc9886a1732300d391407afb17b4',
            '645983282149269'=>'67068d9cf97980a97577c05e21bafa18',
            '575417055904277'=>'090ec1547966b65f0eff67b52397836a',
            '854985254530200'=>'4ae637c15ff7c3e499e80146078ecb01',
            '649213391820764'=>'907a7f55bf31256c9d5da2fc7b35bc6e',
            '645544988865148'=>'370c354759a43139e91fcabdf5b1711b',
            '776523482381685'=>'28fa86342d39102eb24ae7ac7968a355',
            '227292140813061'=>'30bfa574e67c16982180237803c8ccd9',
        ),
    ),
));
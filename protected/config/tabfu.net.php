<?php
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap'); 
return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
        'timeZone' => 'America/New_York',
        'modules' => array(
            'template' => array(
                'repositoryAdapterConfig' => array(
                    'class' => 'LocalRepositoryAdapter',
                    'pattern' => array(
                        'path' => '/home/tabfunet/public_html/uploads/',
                        'url' => 'https://tabfu.net/uploads/'
                    )
                )
            )
        ),
        'components'=>array(
            'db'=>array(
                'connectionString' => 'mysql:host=localhost;dbname=tabfunet_db',
                'emulatePrepare' => true,
                'username' => 'tabfunet_user',
                'password' => 'QBdfZ3tAWLayZ0',
			/*
                'connectionString' => 'mysql:host=45.56.95.224:2031;dbname=tabfuonline',
                'emulatePrepare' => true,
                'username' => 'root',
                'password' => 'IzCHlKdFiXbY',
				*/
                'charset' => 'utf8',
            ),
            // memcache enabled for live server
            'cache'=>array(
                'class'=>'system.caching.CMemCache',
                'servers'=>array(
                    array('host'=>'127.0.0.1', 'port'=>11211),
                ),
            ),
            'session' => array(
                'cookieParams' => array(
                    'domain' => 'www.tabfu.net'
                ),
            )
        ),
        'params'=>array(
            // this is used in contact page
            'adminEmail'=>'info@tabfu.com',
            'replyEmail'=>'tabfuapp@gmail.com',
            'infoEmail'=>'info@tabfu.com',
            'JvzipnSecretKey'=>'y.^1Cpd178R*m;k',
            'ZaxaaApiSignature'=>'XG32E3QB11911ZN9',
            'WarriorPaymentsAllowedIps'=>array(
                '54.86.40.226', // payments.warriorforum
            ),
            'WarriorPaymentsHost'=>'https://payments.warriorforum.com',
            'disableRegistration'=>false,
            
            'mailAdapter' => 'mandrill', // 'standard'
            'MandrillApiKey'=>'BBXzN9UDBNdL2DMRKdAnHQ',
            
            'FacebookAppId'  => '1496093447296300',
            'FacebookSecret' => '9efd6197c305030fb745550f7657f6df',
            'FacebookTabs' => array(
                '1455089928097909'=>'b9cd577c020f3c2224a6cb9754a3ad66', // tab 1
                '341993419303339'=>'b6ea5c1620bb747094747c3bb401e56b',  // tab 2
                '1496742580564010'=>'b06f0ab23fcea3b5aaeb0d4ba736e154', // tab 3
                '1387094021551571'=>'80ac0d29cfe88169de8f9adc2ff17eb0', // tab 4
                '672869622804179'=>'1dcf1d0a3d9dfcfd3c2747dc24783863',  // tab 5
                '359372324187731'=>'72d30c1d07e788e6009d33454a490e89',  // tab 6
                '1457420091184113'=>'58a43ed86d7d16803738a7d62efa9709', // tab 7
                '601016943352174'=>'899b26e95649334e5f72cbaf5be6f485',  // tab 8
                '1485393981700954'=>'f7eaf323bfb3d62c30c1bba4e8f60b52', // tab 9
                '1486845001560724'=>'a2f9e462b7289b66db29e61f9df52d21', // tab 10
                '629781083802219'=>'6b299d1490751d463fd610182a1a56fb',  // tab 11
                '462974367175742'=>'2f86689b3e8a5e452179feab8a7df795',  // tab 12
                '266039950259985'=>'79df857041c768d51c449dadd50f92a6',  // tab 13
                '783650241689146'=>'7d331a59145420988015e64cf6e90cb9',  // tab 14
                '277321409125838'=>'35a9a16978ac2da1908e0b3f5eda9890',  // tab 15
                '624949950958082'=>'f1841867744221435fe9768f0d28b7dc',  // tab 16
                '608283052622368'=>'a3386333f85d1d801112a6db0a99ebbe',  // tab 17
                '765024523570719'=>'f1d2363fda1bd819df601e48cab4569c',  // tab 18
                '870515552960408'=>'7c284d22ea62edd4f9929f57b4078416',  // tab 19
                '931301416884038'=>'a78c0aec739d6b093969e2cb79bb1740',  // tab 20
                '505976769537696'=>'2f6ecebf8884e690ea090c6c8554db86',  // tab 21
                '934470149902313'=>'6502520844e5e535c163af538224b277',  // tab 22
                '319405694908274'=>'31cb9dd34942e002ff682754ea594e8b',  // tab 23
                '746230578801905'=>'f5dd819a05ea885f926d9dae2b1cc600',  // tab 24
                '1556980557854058'=>'5b12db6395d23e7944f6fcffa058bc1c', // tab 25
                '543223472473884'=>'cc1b24bd1355ea44e1b7162de26d436b',  // tab 26
                '747270352000814'=>'8f8de88eca1cbb0f3390bcdb1ab94391',  // tab 27
                '282535401937963'=>'e0911f65381834522d53cd8bfaae6328',  // tab 28
                '363743250448803'=>'c7ed2cc786cd95c2e53cfcbfb2bba973',  // tab 29
                '891728140857206'=>'6719dc0d17440a636bf7400d6de224a5',  // tab 30
            ),
			'salesforce' => array(
				"client_id"		=> "3MVG9d8..z.hDcPLibAVRBl92OeYYeNyczg2OiP2CcSVCIriGJYP0Y0qHPt7XDgmpITEMSCxi4zRd63DARX10",
				"client_secret"	=> "5984818632533993034",
				"redirect_uri"	=> "https://www.tabfu.net/salesforce",
				"login_uri"		=> "https://login.salesforce.com",		
			),
			
			
        ),
    )
);

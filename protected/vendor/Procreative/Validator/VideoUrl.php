<?php

class VideoUrl extends Zend\Validator\AbstractValidator
{

    public function isValid($value)
    {
        $ok = $this->_validateYouTubeUrl($value) || 
              $this->_validateVimeoUrl($value)
        ;
        
        if (!$ok) {
            $this->error("This URL is not a valid YouTube nor Vimeo video's URL.");
        }
        return $ok;
    }
    
    private function _validateYouTubeUrl($url)
    {
        $ytRegEx = <<<'RegEx'
            ~
            ^(?:https?://)?              # Optional protocol
             (?:www\.)?                  # Optional subdomain
             (?:youtube\.com|youtu\.be)  # Mandatory domain name
             /watch\?v=([^&]+)           # URI with video id as capture group 1
            ~x
RegEx;
        return preg_match($ytRegEx, $url);

    }
    
    private function _validateVimeoUrl($url)
    {
        return preg_match('/^http:\/\/(www\.)?vimeo\.com\/(clip\:)?(\d+).*$/', $url);
    }

}
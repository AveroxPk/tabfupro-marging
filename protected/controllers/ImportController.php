<?php

class ImportController extends Controller
{
        /**
         * Imports Tabfu Freebies Users from CSV
         * 
         * CSV format:
         * name;lastname;email
         */
	public function actionFreebieUsers()
	{
            // Import modules
            Yii::import('ext.yiiexcel.YiiExcel', true);
            Yii::registerAutoloader(array('YiiExcel', 'autoload'), true);
            Yii::import('application.vendor.phpexcel.PHPExcel');

            // Set config
            $importDirectory = './import/';
            $fileName = 'freebie_users.csv';
            $columns = array("name","lastname","email");
            
            // Create PHPExcel object
            $reader = PHPExcel_IOFactory::createReader('CSV');
            $reader->setDelimiter(';'); 
            $reader->setSheetIndex(0);
            $file = $reader->load($importDirectory . $fileName);
            
            // Convert CSV to array
            $rows = $file->getActiveSheet()->toArray("",false,false);

            // Read CSV data
            $users = array();
            foreach($rows as $row) {
                $data = array();
                foreach($columns as $i => $column) {
                    $data[$column] = $row[$i];
                }
                $users[] = $data;
            }
            
            $counter = 0;
            
            // Foreach user (row)
            foreach($users as $key => $user) {
                echo $user['name'].' '.$user['lastname'].' ('.$user['email'].') <br/>';
                
                // If user found in DB
                if(User::model()->find('email = :email', array(':email' => $user['email']))) {
                    $users[$key]['error'] = 'User exists';
                    continue;
                }
                
                // Register user
                $newUser = new User();
                $password = $newUser->generateRandomPassword();
                $passwordHashed = User::encode($password);
                $newUser->password = $passwordHashed;
                $newUser->repeatPassword = $passwordHashed;
                $newUser->name = $user['name'];
                $newUser->lastname = $user['lastname'];
                $newUser->email = $user['email'];
                $newUser->status = User::STATUS_ACTIVE;
                $newUser->type = User::USER_NORMAL;
                if(!$newUser->save()){
                    $users[$key]['error'] = 'Error registering user';
                    continue;
                }

                // Save membership to user
                $membership = new Membership();
                $membership->user_id = $newUser->user_id;
                $membership->membership_status = Membership::STATUS_ACTIVE_LIFETIME;
                $membership->freebie = true;
                if(!$membership->save()) {
                    $users[$key]['error'] = 'Error saving membership';
                    $newUser->delete();
                    continue;
                }
                
                // Send mail to user with password
//                if(!$newUser->sendMessageWithPassword($password)) {
//                    $users[$key]['error'] = 'Error sending password';
//                    $newUser->delete();
//                    $membership->delete();
//                }
                
                unset($users[$key]);
                $counter++;
                
            }

            // Summary
            echo '<br/><hr>Added: '.$counter;
            echo '<br/>Errors '.count($users).':<br/><br/>';
            
            $newCsv = array();
            // $users have only not registered users
            foreach($users as $user) {
                echo '<i>'.$user['name'].' '.$user['lastname'].' ('.$user['email'].')</i> <b>'.$user['error'].'</b><br/>';
                if($user['error'] != 'User exists') {
                    $newCsv[] = $user;
                }
            }
            
            echo '<hr>New CSV:<br/><br/>';
            foreach($newCsv as $row) {
                echo $user['name'].';'.$user['lastname'].';'.$user['email'].'<br/>';
            }
            
	}

}
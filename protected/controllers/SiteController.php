<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
	}
        
    public function init() {
        Yii::app()->theme='classic';
    }
    
    public function actionRefreshSession()
    {
        echo '';
    }
    
    public function actionTestAnalytics()
    {
        Yii::import('facebook.models.*');
        Yii::import('analytics.components.*');
        $id = 25;
        $motionpost = MotionPost::model()->findByAttributes(array('id'=>$id));
        $userFacebook = UserFacebook::model()->findByAttributes(array('id'=>2));
        $facebookQuery = new QueryFacebook($motionpost->facebook_post_id, Analytics::ANALYTICS_MOTION_POST, $userFacebook->long_token, $id);
        var_dump($facebookQuery->getImpressionsCount());
    }
    
    public function actionFlushCache()
    {
        Yii::app()->cache->flush();
    }
    
    public function actionMaintenance($time = null)
    {
        $fileTime = filemtime(Yii::getPathOfAlias('application') . '/../.maintenance');
        if ($time) {
            $time = (int)$time > 0 ? '+' . (int)$time : (int)$time;
            $estTime = date('g:i a T', strtotime($time . ' minutes', $fileTime));
        }
        
        $this->layout = false;
        $this->render('maintenance', array(
            'estTime' => $estTime,
            'time' => $time
        ));
    }
    
    public function actionFblogout()
    {
        Yii::app()->user->setFlash('success', "you have successfully logged out<br/>please click the '+ facebook account' button to add your new account");
        $this->redirect(array('/facebookaccounts'));
    }

    /**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
        if(Yii::app()->user->isGuest){
            $this->redirect(array('/login'));
        }else{
            $this->redirect(array('/facebookaccounts'));
        }
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
//				print_r($error); exit;
//				echo 'error' . ( $this->getViewFile( 'error' . $error ) ? $error : '' ); exit;
			//	$this->render('error', $error);
			
			$this->render(($this->getViewFile( $error['code'] ) ? $error['code'] : 'error' ), $error );			
		}
	}
    
    public function actionContact()
    {
        $model=new ContactForm;
        
        if (isset($_POST['email'])) {
			$model->name = $_POST['name'];
			$model->email = $_POST['email'];
			$model->comment = $_POST['comment'];
            
			if($model->validate()) {
				$name = '=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject = '=?UTF-8?B?'.base64_encode('TabFu Landing Page contact').'?=';
				$headers = "From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				if (mail(Yii::app()->params['infoEmail'], $subject, $model->comment, $headers)) {
                    echo 'ok';
                }
                else {
                    echo 'Couldn\'t send the message.';
                }
			}
		}
    }
    
    public function actionTutorials()
    {
        $this->render('tutorials');
    }
}
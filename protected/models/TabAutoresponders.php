<?php

/**
 * This is the model class for table "tab_autoresponders".
 *
 * The followings are the available columns in table 'tab_autoresponders':
 * @property integer $tab_id
 * @property string $autoresponder_name
 * @property string $list_name
 * @property string $list_id
 */
class TabAutoresponders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tab_autoresponders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tab_id, autoresponder_name, list_name, list_id', 'required'),
			array('tab_id', 'numerical', 'integerOnly'=>true),
			array('autoresponder_name', 'length', 'max'=>100),
			array('list_name, list_id', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tab_id, autoresponder_name, list_name, list_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tab_id' => 'Tab',
			'autoresponder_name' => 'Autoresponder Name',
			'list_name' => 'List Name',
            'list_id' => 'List Id'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tab_id',$this->tab_id);
		$criteria->compare('autoresponder_name',$this->autoresponder_name,true);
		$criteria->compare('list_name',$this->list_name,true);
        $criteria->compare('list_id',$this->list_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TabAutoresponders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

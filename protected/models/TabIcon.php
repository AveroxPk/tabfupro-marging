<?php

/**
 * This is the model class for table "tab_icon".
 *
 * The followings are the available columns in table 'tab_icon':
 * @property string $id
 * @property integer $user_id
 * @property string $original_filename
 * @property string $server_filename
 * 
 * @method TabIcon forCurrentUser() Scope with tab icons available for currently logged in user
 */
class TabIcon extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tab_icon';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('original_filename, server_filename', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('original_filename, server_filename', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, original_filename, server_filename', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'original_filename' => 'Original Filename',
			'server_filename' => 'Server Filename',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('original_filename',$this->original_filename,true);
		$criteria->compare('server_filename',$this->server_filename,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TabIcon the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    

    public function scopes() 
    {
        return array(
            'forCurrentUser' => array(
                'condition' => 'user_id IS NULL OR user_id = ' . (int)Yii::app()->user->id
            )
        );
    }
    
    public function getIsStockImage()
    {
        return $this->user_id === null;
    }
    
    public function getImagePath()
    {
        $directory = $this->getIsStockImage() ?
                Yii::getPathOfAlias('webroot.images.tab-icons') :
                Yii::getPathOfAlias('webroot.uploads.tab-icons')
        ;
        
        return $directory . DIRECTORY_SEPARATOR . $this->server_filename;
    }
    
    public function getImageUrl()
    {
        if (!file_exists($this->getImagePath())) {
            return null;
        }
        
        $directory = $this->getIsStockImage() ?
                '/images/tab-icons/' :
                '/uploads/tab-icons/'
        ;
        
        return Yii::app()->getBaseUrl(true) . $directory . rawurlencode($this->server_filename);
    }
    
}

<?php

class Cacher extends CComponent
{
    const CACHE_DATA_PERIOD = 21600; // 6 hours
    const CACHE_LAYOUT_PERIOD = 86400; // 24 hours
    
    public static function getFromCache($key, $defaultValue = null, $period = self::CACHE_DATA_PERIOD)
    {
        $value = Yii::app()->cache->get($key);
        if ($value === false) {
            //Yii::log('Cache missed: ' . $key, 'info');
            $value = is_callable($defaultValue) ? $defaultValue() : $defaultValue;
            Yii::app()->cache->set($key, $value, $period);
        }
        else {
            //Yii::log('Cache hit: ' . $key, 'info');
        }
        return $value;
    }
}
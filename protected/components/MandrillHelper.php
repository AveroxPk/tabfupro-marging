<?php

/* 
How to use: 
 
$email = new MandrillHelper();
$email->fromEmail = 'wojtek@procreative.eu';
$email->fromName = 'Wojciech Sapiechowski';
$this->subject = 'Test?';
$email->toArray = array(
    array(
        'email' => 'sapcik@gmail.com', 
        'name' => 'Wojciech Sapiechowski',
        'type' => 'to'
    )
); 
$email->text = 'Test?';
$email->tags = array('test');
$email->send();
*/ 


    class MandrillHelper {
        
        private $apiKey;
        
        public $fromEmail;
        public $fromName;
        public $replyTo;
        
        public $toArray;
        /* Example $toArray:
         * array(
                array(
                    'email' => 'wojtek@procreative.eu',
                    'name' => 'Wojciech Sapiechowski',
                    'type' => 'to'
               )
           )
         */
        
        public $html;
        public $text;
        public $subject;
        public $tags = null;
        
        public function __construct()
        {
            Yii::import('application.vendor.mandrill.Mandrill');
            $this->apiKey = Yii::app()->params['MandrillApiKey'];
        }
        
        public function send()
        {   
            $this->fromEmail = empty($this->fromEmail) ? Yii::app()->params['adminEmail'] : $this->fromEmail;
            $this->fromName = empty($this->fromName) ? Yii::app()->params['adminName'] : $this->fromName;
            $this->replyTo = empty($this->replyTo) ? Yii::app()->params['replyEmail'] : $this->replyTo;
            
            try {
                $mandrill = new Mandrill( $this->apiKey );   
                     
                $message = array(
                    'html' => $this->html,
                    'text' => $this->text,
                    'subject' => $this->subject,
                    'from_email' => $this->fromEmail,
                    'from_name' => $this->fromName,
                    
                    'to' => $this->toArray,
                    
                    'headers' => array('Reply-To' => $this->fromEmail),
                    'important' => true,
                    'track_opens' => null,
                    'track_clicks' => null,
                    'auto_text' => null,
                    'auto_html' => null,
                    'inline_css' => null,
                    'url_strip_qs' => null,
                    'preserve_recipients' => null,
                    'view_content_link' => null,
                    'bcc_address' => null,
                    'tracking_domain' => null,
                    'signing_domain' => null,
                    'return_path_domain' => null,
                    'merge' => true,
                    'global_merge_vars' => null,
                    'merge_vars' => null,
                    'tags' => $this->tags,
                    'google_analytics_domains' => null,
                    'google_analytics_campaign' => null,
                    'metadata' => null,
                    'recipient_metadata' => null,
                );
                
                $async = false;
                $ip_pool = null;
                $send_at = date("Y-m-d H:i:s");
                $result = $mandrill->messages->send($message, $async, $ip_pool);

                return $result[0]['status'] == 'sent' ? TRUE : FALSE;
               
            } catch(Mandrill_Error $e) {
                // Mandrill errors are thrown as exceptions
                //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                //throw $e;
                return false;
            }
        }
    }
    
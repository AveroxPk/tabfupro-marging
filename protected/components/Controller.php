<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    //public $layout='//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs=array();

    public $user=null;

    private $warnings = array();
    
    const WARNING_TYPE_NORMAL = 1;
    const WARNING_TYPE_COOKIE = 2;
    
    function __construct($id, $module=null){
        Yii::import('application.modules.user.models.*');
        Yii::import('application.modules.membership.models.*');

        if(!Yii::app()->user->isGuest){
            $this->user = User::model()->findByPk(Yii::app()->user->id);
        }
        
        /** 
         * Global warning message
         */
        $warningMessage = 'Maintenance window is scheduled <strong>everyday between 3:00am and 4:00am EDT</strong>. During this time the site may be temporarily unavailable.';
        $this->addWarning($warningMessage, Controller::WARNING_TYPE_COOKIE);       
                
        parent::__construct($id, $module);
    }
    
    public function init()
    {      
        if (Yii::app()->request->isAjaxRequest && Yii::app()->user->isGuest) {
            echo 'logged-out';
            Yii::app()->end();
        }
        
        parent::init();
    }

    public function filterAccessControl($filterChain)
    {   
        $rules = $this->accessRules();

        /**
         * If found rule array('allow', 'users'=>'@')
         * convert it to:
         * array('allow', 'users'=>array('@'),'expression'=>'Yii::app()->controller->user->isAdmin() || Yii::app()->controller->user->isMembershipActive() || Yii::app()->controller->user->getRemainingFreeMembership()'),
         * and add deny rule:
         * array('deny', 'users'=>array('*'), 'deniedCallback'=>array('Controller', 'accessDeniedCallback')), 
         * so logged user can access only if have admin rights, active membership or remaining free membership
         */
        foreach($rules as $key => $rule) {

            if(isset($rule[0]) && $rule[0]=='allow' && count($rule)==2
                    && isset($rule['users']) && isset($rule['users'][0]) && $rule['users'][0]=='@') 
            {
                $rules[$key]['expression'] = 'Yii::app()->controller->user->isActive() && (Yii::app()->controller->user->isAdmin() || Yii::app()->controller->user->isMembershipActive() || Yii::app()->controller->user->getRemainingFreeMembership())';
                $rules[] = array('deny',
                            'users'=>array('*'),
                            'deniedCallback'=>array('Controller', 'accessDeniedCallback'),
                    );
                break;
            }
        }

        $filter = new CAccessControlFilter;
        $filter->setRules( $rules );
        $filter->filter($filterChain);
    }
    
    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);

        foreach (Yii::app()->log->routes as $route) {
            if($route instanceof CWebLogRoute) {
                $route->enabled = false;
            }
        }
        Yii::app()->end();
    }
        
    public function getUnreadedMessages() {
         if(!Yii::app()->user->isGuest){
             return $this->user->getMessagesUnreaded();

         }else{
             return array();
         }

    }

    public function getReadedMessages() {
         if(!Yii::app()->user->isGuest){
             return $this->user->getMessagesReaded();

         }else{
             return array();
         }

    }
    
    /**
     * Read from config that registration is enabled
     * 
     * @return boolean
     */
    public function isRegistrationEnabled() {
        if(isset(Yii::app()->params['disableRegistration']) && Yii::app()->params['disableRegistration']) {
            return false;
        } else {
            return true;
        }
    }    
    
    static public function accessDeniedCallback() {
        Yii::app()->controller->redirect(array('/membership'));
    }
    
    
    public function getWarnings() {
        return $this->warnings;
    }
    
    public function addWarning($msg, $type = self::WARNING_TYPE_NORMAL) {
        $this->warnings[] = (object) array(
            'message' => $msg, 
            'type' => $type,
        );
    }
    
    public function clearWarnings() {
        $this->warnings = array();
    }
}
<?php

include __DIR__.'/../../PHPMailer/PHPMailerAutoload.php';
class MailHelper
{
    public static function sendMail($to, $subject, $body, $from = null, $mimeType = 'text/plain')
    {
		
        
        $mailAdapter = isset(Yii::app()->params['mailAdapter']) ? 
                strtolower( Yii::app()->params['mailAdapter'] ) : 'standard';
        
        switch('standard') {
            case 'mandrill':
                return self::sendMailByMandrill($to, $subject, $body, $from, $mimeType);
                break;
            case 'standard':
            default:
                return self::sendMailByYiiMail($to, $subject, $body, $from, $mimeType);
        }

    }
    
    public static function sendMailByYiiMail($to, $subject, $body, $from = null, $mimeType = 'text/plain')
    {
        if ($from === null) {
            $from = array(
                Yii::app()->params['senderEmail'] => Yii::app()->params['senderEmailName'],
            );
        }
        
        Yii::import('ext.yii-mail.*', true);
		
        $message = new YiiMailMessage;
        $message->setBody($body, $mimeType);
        $message->addTo($to);
		
        if (isset(Yii::app()->params['replyEmail'])) {
            $message->addReplyTo(Yii::app()->params['replyEmail']);
        }
        $message->from = $from;
        $message->subject = $subject;
        
        $mail = new PHPMailer();
        $mail->IsSMTP();
       // $mail->SMTPDebug  = 2;
        $mail->CharSet = 'UTF-8';
        
        $mail->SMTPAuth   = true;                      // enable SMTP authentication
        $mail->SMTPSecure = false;                     // sets the prefix to the servier ,  'ssl' turned off on linode server
		$mail->SMTPAutoTLS  = false;				// add this setting for linode server only
/*        $mail->Host       = "tabfu.net";  // sets GMAIL as the SMTP server
        $mail->Port       = 465;                       // set the SMTP port for the GMAIL server
        $mail->Username   = "no-reply@tabfu.net";      // username
        $mail->Password   = '3bnGwMuxDX6&';            // SMTP account password example

        $mail->Host       = "s1.tabfu.online";  // sets GMAIL as the SMTP server
        $mail->Port       = 465;                       // set the SMTP port for the GMAIL server
        $mail->Username   = "no-reply@tabfu.online";      // username
        $mail->Password   = 'welcome123';            // SMTP account password example
		
*/		
		
        $mail->Host       = "tabfu.online";  // sets GMAIL as the SMTP server
        $mail->Port       = 25;                       // set the SMTP port for the GMAIL server
        $mail->Username   = "no-reply@tabfu.online";      // username
        $mail->Password   = 'welcome123';            // SMTP account password example
        
		
		
        //From email address and name
        $mail->From = "no-reply@tabfu.online";
        $mail->FromName = "TabfuPro";
        
        //To address and name
        $mail->addAddress($to, "");
        
        //Address to which recipient will reply
//        $mail->addReplyTo("no-reply@tabfu.online", "Reply");
        
        //Send HTML or Plain Text email
        $mail->isHTML(true);
        
        $mail->Subject = $subject;
        $mail->Body = $body;
       
        if(!$mail->send())
        {
        	return false;
        }
        else
        {
        	return true;
        }
        
   /*      try {
            Yii::app()->mail->send($message);
            return true;
        } catch (Exception $e) {
            Yii::log(CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'application.mailhelper.yiimail');
            return false;
        }
		*/
    }
    
    
    public static function sendMailByMandrill($to, $subject, $body, $from = null, $mimeType = 'text/plain') 
    {
        if ($from === null) {
            $from = Yii::app()->params['senderEmail'];
        }
        
        $email = new MandrillHelper();
        $email->fromEmail = $from;
        $email->fromName = Yii::app()->params['senderEmailName'];
        if (isset(Yii::app()->params['replyEmail'])) {
            $email->replyTo = Yii::app()->params['replyEmail'];
        }
        $email->subject = $subject;
        $email->toArray = array(
            array(
                'email' => $to, 
                'type' => 'to'
            )
        ); 
        
        if($mimeType=='text/plain') {
            $email->text = $body;
        } else {
            $email->html = $body;
        }

        try {
            $email->send();
            return true;
        } catch (Exception $e) {
            Yii::log(CVarDumper::dumpAsString($e), CLogger::LEVEL_ERROR, 'application.mailhelper.mandril');
            return false;
        }
    }
    
    /*public static function sendMailToUser($userId, $subject, $body, $from = null)
    {
        User::model()->
        
    }*/
}
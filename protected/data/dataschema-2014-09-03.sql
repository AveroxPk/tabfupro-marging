-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: postmotion
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.12.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ad`
--

DROP TABLE IF EXISTS `ad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Name of the template visible to users',
  `content` text COMMENT 'HTML content of the template',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp of template creation',
  `is_visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'bool - is template visible to users',
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `is_visible_index` (`is_visible`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_category`
--

DROP TABLE IF EXISTS `ad_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_category` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_category_to_ad`
--

DROP TABLE IF EXISTS `ad_category_to_ad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_category_to_ad` (
  `ad_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  KEY `FK_CATEGORY` (`category_id`),
  KEY `FK_TEMPLATE` (`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_field`
--

DROP TABLE IF EXISTS `ad_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Name of the filed for internal app ussage',
  `label` varchar(255) NOT NULL COMMENT 'User friendly name of the field',
  `description` text NOT NULL COMMENT 'Description of the field',
  `is_visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'bool - is field visible to users',
  `is_cacheable` tinyint(1) NOT NULL DEFAULT '0',
  `editor_title` varchar(255) NOT NULL,
  `editor_description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `is_visible` (`is_visible`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_field_attribute`
--

DROP TABLE IF EXISTS `ad_field_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_field_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Name of the attribute forinternal application use',
  `label` varchar(255) NOT NULL COMMENT 'User friendly attribute name',
  `data_type` varchar(255) NOT NULL COMMENT 'Type of data that attribute can store (arrays, strings, int etc.)',
  `custom_edit_widget_class` varchar(255) NOT NULL DEFAULT 'default',
  `default_value` text NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='Table stores individual attributes that can be used by fields';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_field_attribute_validator`
--

DROP TABLE IF EXISTS `ad_field_attribute_validator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_field_attribute_validator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ad_field_attribute_id` int(10) unsigned NOT NULL,
  `validator` varchar(255) NOT NULL,
  `validator_data` text,
  PRIMARY KEY (`id`,`ad_field_attribute_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_ad_field_attribute_validator_ad_field_attrib_idx` (`ad_field_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_field_to_ad_field_attribute`
--

DROP TABLE IF EXISTS `ad_field_to_ad_field_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_field_to_ad_field_attribute` (
  `ad_field_id` int(10) unsigned NOT NULL,
  `ad_field_attribute_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ad_field_id`,`ad_field_attribute_id`),
  KEY `fk_ad_field_to_ad_field_attribute_ad_fiel_idx` (`ad_field_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='M:M relation between fields and avalible attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_page`
--

DROP TABLE IF EXISTS `ad_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ad_id` int(10) unsigned NOT NULL COMMENT 'Id of template that was used to create page',
  `name` varchar(255) NOT NULL COMMENT 'Name of the page that is displayed to users and on Facebook page',
  `user_ads_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`ad_id`,`user_ads_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_page_ad1_idx` (`ad_id`),
  KEY `fk_page_user_ads1_idx` (`user_ads_id`),
  CONSTRAINT `fk_page_ad1` FOREIGN KEY (`ad_id`) REFERENCES `ad` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_page_user_ads1` FOREIGN KEY (`user_ads_id`) REFERENCES `user_ads` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='Table stores pages created by Users from Templates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_page_field`
--

DROP TABLE IF EXISTS `ad_page_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_page_field` (
  `id` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `ad_field_id` int(10) unsigned NOT NULL,
  `page_version_id` int(10) unsigned NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`ad_field_id`,`page_version_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_ad_page_field_ad_field1_idx` (`ad_field_id`),
  KEY `fk_ad_page_field_ad_page_version1_idx` (`page_version_id`),
  CONSTRAINT `fk_ad_page_field_ad_field1` FOREIGN KEY (`ad_field_id`) REFERENCES `ad_field` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_ad_page_field_page_version1` FOREIGN KEY (`page_version_id`) REFERENCES `ad_page_version` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=408 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_page_field_attribute`
--

DROP TABLE IF EXISTS `ad_page_field_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_page_field_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_field_id` bigint(19) unsigned NOT NULL,
  `ad_field_attribute_id` int(10) unsigned NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`,`page_field_id`,`ad_field_attribute_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_ad_page_field_attribute_page_field1_idx` (`page_field_id`),
  KEY `fk_ad_page_field_attribute_template_field_attribute1_idx` (`ad_field_attribute_id`),
  CONSTRAINT `fk_ad_page_field_attribute_ad_field_attribute1` FOREIGN KEY (`ad_field_attribute_id`) REFERENCES `ad_field_attribute` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_ad_page_field_attribute_ad_page_field1` FOREIGN KEY (`page_field_id`) REFERENCES `ad_page_field` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1826 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_page_file`
--

DROP TABLE IF EXISTS `ad_page_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_page_file` (
  `id` int(19) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `server_filename` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_file_page_idx` (`page_id`),
  KEY `page_file_original_filename_idx` (`original_filename`),
  KEY `page_file_server_path_idx` (`server_filename`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_page_version`
--

DROP TABLE IF EXISTS `ad_page_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_page_version` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `content` text NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `is_scheduled` tinyint(1) NOT NULL DEFAULT '0',
  `is_fully_cached` tinyint(1) NOT NULL DEFAULT '0',
  `is_modified` tinyint(1) NOT NULL DEFAULT '0',
  `publish_time` timestamp NULL DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` timestamp NULL DEFAULT NULL,
  `gates_config` text NOT NULL,
  PRIMARY KEY (`id`,`page_id`,`version`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_ad_page_version_page1_idx` (`page_id`),
  KEY `is_published_index` (`is_published`),
  CONSTRAINT `fk_ad_page_version_page1` FOREIGN KEY (`page_id`) REFERENCES `ad_page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_to_ad_field`
--

DROP TABLE IF EXISTS `ad_to_ad_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_to_ad_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ad_id` int(10) unsigned NOT NULL,
  `ad_field_id` int(10) unsigned NOT NULL,
  `default_values` text NOT NULL,
  PRIMARY KEY (`id`,`ad_id`,`ad_field_id`),
  KEY `fk_ad_to_ad_field_ad_field1_idx` (`ad_field_id`),
  KEY `fk_ad_to_ad_field_ad` (`ad_id`),
  CONSTRAINT `fk_ad_to_ad_field_ad` FOREIGN KEY (`ad_id`) REFERENCES `ad` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_ad_to_ad_field_ad_field1` FOREIGN KEY (`ad_field_id`) REFERENCES `ad_field` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8 COMMENT='M:M relation between ads and fields ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `animation`
--

DROP TABLE IF EXISTS `animation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_drive` varchar(256) NOT NULL,
  `public` enum('0','1') NOT NULL DEFAULT '0',
  `user_facebook_id` int(11) DEFAULT NULL,
  `width` int(3) NOT NULL,
  `height` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`user_facebook_id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `animation_favorite`
--

DROP TABLE IF EXISTS `animation_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animation_favorite` (
  `animation_id` int(11) NOT NULL,
  `user_facebook_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_animation_favorite_user_facebook1_idx` (`user_facebook_id`),
  KEY `fk_animation_favorite_animation1` (`animation_id`),
  CONSTRAINT `fk_animation_favorite_user_facebook1` FOREIGN KEY (`user_facebook_id`) REFERENCES `user_facebook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `animation_name`
--

DROP TABLE IF EXISTS `animation_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animation_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `animation_id` int(11) NOT NULL,
  `user_facebook_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `original` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_animation_name_user_facebook1_idx` (`user_facebook_id`),
  KEY `fk_animation_name_animation1` (`animation_id`),
  CONSTRAINT `fk_animation_name_animation1` FOREIGN KEY (`animation_id`) REFERENCES `animation` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_animation_name_user_facebook1` FOREIGN KEY (`user_facebook_id`) REFERENCES `user_facebook` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autoresponder_aweber`
--

DROP TABLE IF EXISTS `autoresponder_aweber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoresponder_aweber` (
  `user_user_id` int(11) NOT NULL,
  `access_code` varchar(256) NOT NULL,
  `access_key` varchar(255) DEFAULT NULL,
  `access_secret` varchar(255) DEFAULT NULL,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_user_id`),
  CONSTRAINT `fk_autoresponder_aweber_user` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autoresponder_getresponse`
--

DROP TABLE IF EXISTS `autoresponder_getresponse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoresponder_getresponse` (
  `user_user_id` int(11) NOT NULL,
  `api_key` varchar(256) NOT NULL,
  PRIMARY KEY (`user_user_id`),
  CONSTRAINT `fk_autoresponder_getresponse_user1` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autoresponder_icontact`
--

DROP TABLE IF EXISTS `autoresponder_icontact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoresponder_icontact` (
  `user_user_id` int(11) NOT NULL,
  `app_login` varchar(255) DEFAULT NULL,
  `app_id` varchar(255) DEFAULT NULL,
  `app_pass` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_user_id`),
  CONSTRAINT `fk_table1_user1` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autoresponder_infusionsoft`
--

DROP TABLE IF EXISTS `autoresponder_infusionsoft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoresponder_infusionsoft` (
  `user_user_id` int(11) NOT NULL,
  `app_name` varchar(256) NOT NULL,
  `app_key` varchar(256) NOT NULL,
  PRIMARY KEY (`user_user_id`),
  CONSTRAINT `fk_table1_user4` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autoresponder_mailchimp`
--

DROP TABLE IF EXISTS `autoresponder_mailchimp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoresponder_mailchimp` (
  `user_user_id` int(11) NOT NULL,
  `api_key` varchar(256) NOT NULL,
  PRIMARY KEY (`user_user_id`),
  CONSTRAINT `fk_table1_user2` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autoresponder_sendreach`
--

DROP TABLE IF EXISTS `autoresponder_sendreach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoresponder_sendreach` (
  `user_user_id` int(11) NOT NULL,
  `api_user_id` varchar(256) NOT NULL,
  `api_key` varchar(256) NOT NULL,
  `api_secret` varchar(45) NOT NULL,
  PRIMARY KEY (`user_user_id`),
  CONSTRAINT `fk_table1_user3` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date` datetime DEFAULT NULL,
  `message_category_id` int(11) NOT NULL,
  `archived` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`message_category_id`),
  KEY `fk_message_message_category_idx` (`message_category_id`),
  CONSTRAINT `fk_message_message_category` FOREIGN KEY (`message_category_id`) REFERENCES `message_category` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_category`
--

DROP TABLE IF EXISTS `message_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motion_post`
--

DROP TABLE IF EXISTS `motion_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motion_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_facebook_id` int(11) NOT NULL,
  `published` enum('0','1') NOT NULL,
  `schedule` datetime DEFAULT NULL,
  `animation_id` int(11) DEFAULT NULL,
  `user_fanpage_id` int(11) NOT NULL COMMENT 'Fanpage where we pu animation',
  `favorite` enum('0','1') NOT NULL DEFAULT '0',
  `url` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `caption` text NOT NULL,
  `description` text NOT NULL,
  `message` text NOT NULL,
  `facebook_post_id` varchar(256) DEFAULT NULL,
  `type` enum('0','1') NOT NULL DEFAULT '0',
  `movie_url` varchar(255) DEFAULT NULL,
  `movie_icon_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`user_facebook_id`),
  KEY `fk_motion_post_user_facebook1_idx` (`user_facebook_id`),
  KEY `fk_motion_post_animation1_idx` (`animation_id`),
  KEY `fk_motion_post_user_fanpage1_idx` (`user_fanpage_id`),
  CONSTRAINT `fk_motion_post_user_facebook1` FOREIGN KEY (`user_facebook_id`) REFERENCES `user_facebook` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_motion_post_user_fanpage1` FOREIGN KEY (`user_fanpage_id`) REFERENCES `user_fanpage` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motion_post_analytics`
--

DROP TABLE IF EXISTS `motion_post_analytics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motion_post_analytics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_type` enum('CLICK','LIKE','SHARE','COMMENT') DEFAULT NULL,
  `action_count` int(11) DEFAULT NULL,
  `motion_post_id` int(11) DEFAULT NULL,
  `timestamp` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movie_icon`
--

DROP TABLE IF EXISTS `movie_icon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_icon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `original_filename` varchar(255) NOT NULL,
  `server_filename` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(10) unsigned NOT NULL COMMENT 'Id of template that was used to create page',
  `name` varchar(255) NOT NULL COMMENT 'Name of the page that is displayed to users and on Facebook page',
  `user_tabs_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`template_id`,`user_tabs_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_page_template1_idx` (`template_id`),
  KEY `fk_page_user_tabs1_idx` (`user_tabs_id`),
  CONSTRAINT `fk_page_template1` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_page_user_tabs1` FOREIGN KEY (`user_tabs_id`) REFERENCES `user_tabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=927 DEFAULT CHARSET=utf8 COMMENT='Table stores pages created by Users from Templates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_analytics`
--

DROP TABLE IF EXISTS `page_analytics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_analytics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_tab_id` int(11) DEFAULT NULL,
  `action_type` enum('LIKE','EMAIL-GATE','SHARE-GATE','CLICK') DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_page_analytics_user_tabs` (`user_tab_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1533 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_field`
--

DROP TABLE IF EXISTS `page_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_field` (
  `id` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `template_field_id` int(10) unsigned NOT NULL,
  `page_version_id` int(10) unsigned NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`template_field_id`,`page_version_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_page_field_template_field1_idx` (`template_field_id`),
  KEY `fk_page_field_page_version1_idx` (`page_version_id`),
  CONSTRAINT `fk_page_field_page_version1` FOREIGN KEY (`page_version_id`) REFERENCES `page_version` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_page_field_template_field1` FOREIGN KEY (`template_field_id`) REFERENCES `template_field` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52879 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_field_attribute`
--

DROP TABLE IF EXISTS `page_field_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_field_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_field_id` bigint(19) unsigned NOT NULL,
  `template_field_attribute_id` int(10) unsigned NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`,`page_field_id`,`template_field_attribute_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_page_field_attribute_page_field1_idx` (`page_field_id`),
  KEY `fk_page_field_attribute_template_field_attribute1_idx` (`template_field_attribute_id`),
  CONSTRAINT `fk_page_field_attribute_page_field1` FOREIGN KEY (`page_field_id`) REFERENCES `page_field` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_page_field_attribute_template_field_attribute1` FOREIGN KEY (`template_field_attribute_id`) REFERENCES `template_field_attribute` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=189431 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_file`
--

DROP TABLE IF EXISTS `page_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_file` (
  `id` int(19) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `server_filename` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_file_page_idx` (`page_id`),
  KEY `page_file_original_filename_idx` (`original_filename`),
  KEY `page_file_server_path_idx` (`server_filename`)
) ENGINE=InnoDB AUTO_INCREMENT=344 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_version`
--

DROP TABLE IF EXISTS `page_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_version` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `content` text NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `is_scheduled` tinyint(1) NOT NULL DEFAULT '0',
  `is_fully_cached` tinyint(1) NOT NULL DEFAULT '0',
  `is_modified` tinyint(1) NOT NULL DEFAULT '0',
  `publish_time` timestamp NULL DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` timestamp NULL DEFAULT NULL,
  `gates_config` text NOT NULL,
  PRIMARY KEY (`id`,`page_id`,`version`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_page_version_page1_idx` (`page_id`),
  KEY `is_published_index` (`is_published`),
  CONSTRAINT `fk_page_version_page1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1622 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tab_autoresponders`
--

DROP TABLE IF EXISTS `tab_autoresponders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_autoresponders` (
  `tab_id` int(11) NOT NULL,
  `autoresponder_name` varchar(100) NOT NULL,
  `list_name` varchar(255) NOT NULL,
  `list_id` varchar(255) NOT NULL,
  PRIMARY KEY (`tab_id`,`autoresponder_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tab_icon`
--

DROP TABLE IF EXISTS `tab_icon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_icon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `original_filename` varchar(255) NOT NULL,
  `server_filename` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tab_icon_user_id_pk` (`user_id`),
  CONSTRAINT `tab_icon_user_id_pk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Name of the template visible to users',
  `content` text COMMENT 'HTML content of the template',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp of template creation',
  `is_visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'bool - is template visible to users',
  `gates_config` text NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `is_visible_index` (`is_visible`)
) ENGINE=InnoDB AUTO_INCREMENT=1514 DEFAULT CHARSET=utf8 COMMENT='table stores html templates ussed to create pages';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_category`
--

DROP TABLE IF EXISTS `template_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_category` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_category_to_template`
--

DROP TABLE IF EXISTS `template_category_to_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_category_to_template` (
  `template_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  KEY `FK_CATEGORY` (`category_id`),
  KEY `FK_TEMPLATE` (`template_id`),
  CONSTRAINT `FK_CATEGORY` FOREIGN KEY (`category_id`) REFERENCES `template_category` (`id`),
  CONSTRAINT `FK_TEMPLATE` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_favourites`
--

DROP TABLE IF EXISTS `template_favourites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_favourites` (
  `template_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_field`
--

DROP TABLE IF EXISTS `template_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Name of the filed for internal app ussage',
  `label` varchar(255) NOT NULL COMMENT 'User friendly name of the field',
  `description` text NOT NULL COMMENT 'Description of the field',
  `is_visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'bool - is field visible to users',
  `is_cacheable` tinyint(1) NOT NULL DEFAULT '0',
  `editor_title` varchar(255) NOT NULL,
  `editor_description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `is_visible` (`is_visible`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='table stores individual fields that are avalible in templates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_field_attribute`
--

DROP TABLE IF EXISTS `template_field_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_field_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Name of the attribute forinternal application use',
  `label` varchar(255) NOT NULL COMMENT 'User friendly attribute name',
  `data_type` varchar(255) NOT NULL COMMENT 'Type of data that attribute can store (arrays, strings, int etc.)',
  `custom_edit_widget_class` varchar(255) NOT NULL DEFAULT 'default',
  `default_value` text NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='Table stores individual attributes that can be used by fields';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_field_attribute_validator`
--

DROP TABLE IF EXISTS `template_field_attribute_validator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_field_attribute_validator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_field_attribute_id` int(10) unsigned NOT NULL,
  `validator` varchar(255) NOT NULL,
  `validator_data` text,
  PRIMARY KEY (`id`,`template_field_attribute_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_template_field_attribute_validator_template_field_attrib_idx` (`template_field_attribute_id`),
  CONSTRAINT `fk_template_field_attribute_validator_template_field_attribute1` FOREIGN KEY (`template_field_attribute_id`) REFERENCES `template_field_attribute` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_field_to_template_field_attribute`
--

DROP TABLE IF EXISTS `template_field_to_template_field_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_field_to_template_field_attribute` (
  `template_field_id` int(10) unsigned NOT NULL,
  `template_field_attribute_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`template_field_id`,`template_field_attribute_id`),
  KEY `fk_template_field_to_template_field_attribute_template_fiel_idx` (`template_field_attribute_id`),
  CONSTRAINT `fk_template_field_to_template_field_attribute_template_field1` FOREIGN KEY (`template_field_id`) REFERENCES `template_field` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_template_field_to_template_field_attribute_template_field_1` FOREIGN KEY (`template_field_attribute_id`) REFERENCES `template_field_attribute` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='M:M relation between fields and avalible attributes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_to_template_field`
--

DROP TABLE IF EXISTS `template_to_template_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_to_template_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(10) unsigned NOT NULL,
  `template_field_id` int(10) unsigned NOT NULL,
  `default_values` text NOT NULL,
  PRIMARY KEY (`id`,`template_id`,`template_field_id`),
  KEY `fk_template_to_template_field_template_field1_idx` (`template_field_id`),
  KEY `fk_template_to_template_field_template` (`template_id`),
  CONSTRAINT `fk_template_to_template_field_template` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_template_to_template_field_template_field1` FOREIGN KEY (`template_field_id`) REFERENCES `template_field` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48771 DEFAULT CHARSET=utf8 COMMENT='M:M relation between templates and fields ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(72) NOT NULL,
  `name` varchar(72) DEFAULT NULL,
  `lastname` varchar(72) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '1',
  `resetkey` varchar(255) DEFAULT NULL,
  `resetdate` int(11) DEFAULT NULL,
  `registerdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_ads`
--

DROP TABLE IF EXISTS `user_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT 'Untitled',
  `favorite` enum('1','0') NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `schedule` datetime DEFAULT NULL,
  `application_id` bigint(20) NOT NULL,
  `published` enum('0','1') NOT NULL DEFAULT '0',
  `user_fanpage_id` int(11) DEFAULT NULL,
  `user_facebook_id` int(11) NOT NULL,
  `ad_icon_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`,`user_facebook_id`),
  KEY `fk_user_tabs_user_fanpage1_idx` (`user_fanpage_id`),
  KEY `fk_user_tabs_user_facebook1_idx` (`user_facebook_id`),
  KEY `user_tabs_tab_icon_fk` (`ad_icon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_facebook`
--

DROP TABLE IF EXISTS `user_facebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_facebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `facebook_user_id` bigint(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(150) NOT NULL,
  `short_token` text,
  `long_token` text,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_1_idx` (`user_id`),
  CONSTRAINT `fk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_fanpage`
--

DROP TABLE IF EXISTS `user_fanpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_fanpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_facebook_id` int(11) NOT NULL,
  `facebook_page_id` bigint(20) NOT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk1` (`user_facebook_id`),
  CONSTRAINT `fk1` FOREIGN KEY (`user_facebook_id`) REFERENCES `user_facebook` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_message`
--

DROP TABLE IF EXISTS `user_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_message` (
  `message_id` int(11) NOT NULL,
  `user_user_id` int(11) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  UNIQUE KEY `fk_user_message_unique1` (`message_id`,`user_user_id`),
  KEY `fk_user_message_user1_idx` (`user_user_id`),
  CONSTRAINT `fk_user_message_message1` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_user_message_user1` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_tabs`
--

DROP TABLE IF EXISTS `user_tabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT 'Untitled',
  `favorite` enum('1','0') NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `schedule` datetime DEFAULT NULL,
  `application_id` bigint(20) NOT NULL,
  `published` enum('0','1') NOT NULL DEFAULT '0',
  `user_fanpage_id` int(11) DEFAULT NULL,
  `user_facebook_id` int(11) NOT NULL,
  `tab_icon_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`,`user_facebook_id`),
  KEY `fk_user_tabs_user_fanpage1_idx` (`user_fanpage_id`),
  KEY `fk_user_tabs_user_facebook1_idx` (`user_facebook_id`),
  KEY `user_tabs_tab_icon_fk` (`tab_icon_id`),
  CONSTRAINT `fk_user_tabs_user_facebook1` FOREIGN KEY (`user_facebook_id`) REFERENCES `user_facebook` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_user_tabs_user_fanpage1` FOREIGN KEY (`user_fanpage_id`) REFERENCES `user_fanpage` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_tabs_tab_icon_fk` FOREIGN KEY (`tab_icon_id`) REFERENCES `tab_icon` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=929 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_tabs_emails`
--

DROP TABLE IF EXISTS `user_tabs_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tabs_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_tabs_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(145) DEFAULT NULL,
  `lastname` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_tabs_emails_user_tabs` (`user_tabs_id`),
  CONSTRAINT `fk_user_tabs_emails_user_tabs` FOREIGN KEY (`user_tabs_id`) REFERENCES `user_tabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-03 13:29:00
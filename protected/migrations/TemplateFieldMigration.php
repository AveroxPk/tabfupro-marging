<?php

Yii::import('application.modules.template.models.activeRecord.*');
abstract class TemplateFieldMigration extends CDbMigration
{
    public function updateTemplateFields($fields)
    {
        
        foreach ($fields as $fieldName => $fieldData) {
            $model = ARTemplateField::model()->with('templateFieldAttributes')->findByAttributes(array('name' => $fieldName));
            if ($model === null) {
                $model = new ARTemplateField;
                $model->name = $fieldName;
            }
            
            $existingAttributes = array();
            foreach ($model->templateFieldAttributes as $attributeAR) {
                $existingAttributes[$attributeAR->name] = $attributeAR;
            }
            
            $fieldAttributes = $fieldData['attributes'];
            unset($fieldData['attributes']);
            
            $model->attributes = $fieldData;
            if (!$model->save(false)) {
                return false;
            }
            
            if (!empty($fieldAttributes)) {
                foreach ($fieldAttributes as $attributeName => $attributeData) {
                    if (isset($existingAttributes[$attributeName])) {
                        $existingAttributes[$attributeName]->attributes = $attributeData;
                        if (!$existingAttributes[$attributeName]->save(false)) {
                            return false;
                        }
                    } else { //New attribute
                        $attributeAR = new ARTemplateFieldAttribute;
                        $attributeAR->attributes = $attributeData;
                        $attributeAR->name = $attributeName;
                        if (empty($attributeAR->default_value)) { 
                            $attributeAR->default_value = '';
                        }
                        
                        if (!$attributeAR->save(false)) {
                            return false;
                        }
                        
                        //Field <-> Attribute relation
                        $this->insert('template_field_to_template_field_attribute', array(
                            'template_field_id' => (int)$model->id,
                            'template_field_attribute_id' => $attributeAR->id
                        ));
                    }
                }
            }
            
            
        }

    }
    
    public function addValidator($fieldName, $fieldAttribute, $validatorClass, $validatorConfig)
    {
        //Determine attribute id basing on $fieldName and $fieldAttribute params
        $attributeId = $this->dbConnection->createCommand(<<<SQL
    SELECT tfa.id FROM template_field_attribute tfa 
        JOIN template_field_to_template_field_attribute tf2tfa ON tf2tfa.template_field_attribute_id=tfa.id
        JOIN template_field tf ON tf2tfa.template_field_id=tf.id
    WHERE tf.name = '$fieldName' AND tfa.name = '$fieldAttribute';
SQL
        )->queryScalar();
        
        if ($attributeId === null) {
            throw new CException("$fieldName -> $fieldAttribute attribute doesn't exist in current DB. Hence, couldn't add validator: $validatorClass");
        }
        
        $this->insert(
            'template_field_attribute_validator', 
            array(
                'template_field_attribute_id' => $attributeId,
                'validator' => $validatorClass,
                'validator_data' => json_encode($validatorConfig)
            )
        );
    }
}
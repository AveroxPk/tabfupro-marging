<?php

class m140423_075324_alter_message extends CDbMigration
{
	public function up()
	{
            $this->execute('ALTER TABLE message ADD target TINYINT DEFAULT 0 NOT NULL AFTER archived'); // Message::TARGET_ALL
            return true;
	}

	public function down()
	{
                return true;
	}

}
<?php

	//HB//START
	
		header("Content-Type: image/jpg");
	function cropImage($imagePath, $startX, $startY, $width, $height)
	{
		$image = imagecreatetruecolor($width,$height);
		$text_color = imagecolorallocate($image, 255, 255, 255);
		imagestring($image, 5, 350, 250,  'HB ERROR OCCURED', $text_color);
		if(!file_exists($imagePath)){return $image;}
		$image = imagecreatefrompng(realpath($imagePath));
		$rect = array
		(
			'x'=>$startX,
			'y'=>$startY,
			'width'=>$width,
			'height'=>$height,
		);
		$cropped = imagecrop($image, $rect);
		return $cropped;
	}
	if(isset($_GET['png']))
	{
		$img_url = base64_decode($_GET['png']);
		$cropped = cropImage($img_url,0,0,810,600);
	}
	else
	{
		$image = imagecreatetruecolor(810,600);
		$text_color = imagecolorallocate($image, 255, 255, 255);
		imagestring($image, 5, 350, 250,  'HB ERROR OCCURED', $text_color);
		$cropped = $image;
	}
	imagepng($cropped);
	//HB//END
<div class="membership container-fluid">
    <div class="row-fluid">
        <div class="col-md-offset-1 col-md-10 background">
                
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h1>Membership</h1>
                </div>
            </div>
            
            <?php if($membership): ?>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <?php if($membership->membership_status==Membership::STATUS_ACTIVE_LIFETIME): ?>
                        Your membership is active and You have lifetime access to it.
                    <?php elseif(($membership->membership_status==Membership::STATUS_ACTIVE_YEAR)): 
                    		//strtotime($membership->membership_end_date) > strtotime(date("Y-m-d h:i:s", time()))): ?>
                        Your yearly membership is active and 
                            <?php if($membership->recurring): ?>
                                will renew on the <?php echo date('d F Y', strtotime($membership->recurring_date)); ?>.
                            <?php else: ?>
                                will expire on the <?php echo date('d F Y', strtotime($membership->membership_end_date)); ?> (<?php echo $this->_membershipDateDiffHelper($membership); ?>).
                            <?php endif; ?>
                    <?php elseif($membership->membership_status==Membership::STATUS_ACTIVE_INDIVIDUAL): ?>
                        Your Individual membership is active and 
                            <?php if($membership->recurring): ?>
                                will renew on the <?php echo date('d F Y', strtotime($membership->recurring_date)); ?>.
                            <?php else: ?>
                                will expire on the <?php echo date('d F Y', strtotime($membership->membership_end_date)); ?> (<?php echo $this->_membershipDateDiffHelper($membership); ?>).
                            <?php endif; ?>
							
                    <?php //HB//START
					elseif($membership->membership_status==Membership::STATUS_ACTIVE_TWO_YEAR): ?>
                        Your Two Year membership is active and 
                            <?php if($membership->recurring): ?>
                                will renew on the <?php echo date('d F Y', strtotime($membership->recurring_date)); ?>.
                            <?php else: ?>
                                will expire on the <?php echo date('d F Y', strtotime($membership->membership_end_date)); ?> (<?php echo $this->_membershipDateDiffHelper($membership); ?>).
                            <?php endif; ?>
                    <?php elseif($membership->membership_status==Membership::STATUS_ACTIVE_THREE_YEAR): ?>
                        Your Three Year membership is active and 
                            <?php if($membership->recurring): ?>
                                will renew on the <?php echo date('d F Y', strtotime($membership->recurring_date)); ?>.
                            <?php else: ?>
                                will expire on the <?php echo date('d F Y', strtotime($membership->membership_end_date)); ?> (<?php echo $this->_membershipDateDiffHelper($membership); ?>).
                            <?php endif;//HB//END
							?>
                    <?php elseif(($membership->membership_status==Membership::STATUS_ACTIVE_QUARTER)): 
                    		//strtotime($membership->membership_end_date) > strtotime(date("Y-m-d h:i:s", time()))): ?>
                        Your quarterly membership is active and
                            <?php if($membership->recurring): ?>
                                will renew on the <?php echo date('d F Y', strtotime($membership->recurring_date)); ?>.
                            <?php else: ?>
                                will expire on the <?php echo date('d F Y', strtotime($membership->membership_end_date)); ?> (<?php echo $this->_membershipDateDiffHelper($membership); ?>).
                            <?php endif; ?>
                    <?php elseif($membership->membership_status==Membership::STATUS_ACTIVE_MONTH): ?>
                    <?php //elseif(($membership->membership_status==Membership::STATUS_ACTIVE_MONTH) && 
                    		//strtotime($membership->membership_end_date) > strtotime(date("Y-m-d h:i:s", time()))): ?>
                        Your monthly membership is active and
                            <?php if($membership->recurring): ?>
                                will renew on the <?php echo date('d F Y', strtotime($membership->recurring_date)); ?>.
                            <?php else: ?>
                                will expire on the <?php echo date('d F Y', strtotime($membership->membership_end_date)); ?> (<?php echo $this->_membershipDateDiffHelper($membership); ?>).
                            <?php endif; ?>
                    <?php  else: ?>
                        Your membership expired on the <?php echo $membership->membership_end_date!='0000-00-00 00:00:00' ? date('d F Y', strtotime($membership->membership_end_date)) : ''; ?> and now it is inactive. <br/>
                    <?php return; endif; ?>
                </div>
            </div>
            <?php endif; ?>

            <?php if(!$membership || ($membership && $membership->membership_status!=Membership::STATUS_ACTIVE_LIFETIME)): ?>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <p class="lead text-center">
                        Please visit <a href="http://tabfu.com/" target="_blank">TabFu.com</a> to see details.
                    </p>
                    <div class="col-md-4 text-center offer">
                        <h3>Monthly</h3>
                        <a href="https://www.jvzoo.com/b/24665/207226/99">Buy Now - $35.00</a>
                        <!--<br/><br/>
                        <a href="https://tabfu.zaxaa.com/o/4657604212866/1"><img src="https://www.zaxaa.com/button/d2/4657604212866/2p/2/p1" alt="TabFu Membership" border="0"/></a>-->
                    </div>
                    <div class="col-md-4 text-center offer">
                        <h3>Quaterly</h3>
                        <a href="https://www.jvzoo.com/b/24665/125252/99">Buy Now - $67.00</a>
                        <!--<br/><br/>
                        <a href="https://tabfu.zaxaa.com/o/4657604212866/2"><img src="https://www.zaxaa.com/button/d2/4657604212866/2p/2/p2" alt="TabFu Membership" border="0"/></a>-->
                    </div>
                    <div class="col-md-4 text-center offer">
                        <h3>Yearly</h3>
                        <a href="https://www.jvzoo.com/b/24665/125254/99">Buy Now - $147.00</a>
                        <!--<br/><br/>
                        <a href="https://tabfu.zaxaa.com/o/4657604212866/3"><img src="https://www.zaxaa.com/button/d2/4657604212866/2p/2/p3" alt="TabFu Membership" border="0"/></a>-->
                    </div> 
                </div>
            </div>
            <?php endif; ?>

            <?php if($membership && $membership->isActive()): ?>
            <div class="row" style="margin-top: 30px;">
                <div class="col-md-offset-1 col-md-10">
                    <span>To cancel your membership please contact support at: <a href="mailto:support@tabfu.zendesk.com">support@tabfu.zendesk.com</a></span>
                </div>
            </div>
            <?php endif; ?>
            
        </div>
    </div>
</div>

		<?php if($membership): 
		
				$membershipType = 'Lifetime';
				
				if($membership->membership_status==Membership::STATUS_ACTIVE_LIFETIME): $membershipType = 'Lifetime';
				elseif(($membership->membership_status==Membership::STATUS_ACTIVE_YEAR)): $membershipType = 'Yearly';                   		
				elseif($membership->membership_status==Membership::STATUS_ACTIVE_INDIVIDUAL):  $membershipType = 'Individual'; 							
				elseif($membership->membership_status==Membership::STATUS_ACTIVE_TWO_YEAR): $membershipType = 'Two Year';
				elseif($membership->membership_status==Membership::STATUS_ACTIVE_THREE_YEAR): $membershipType = 'Three Year';
				elseif(($membership->membership_status==Membership::STATUS_ACTIVE_QUARTER)): $membershipType = 'Quarterly';
				elseif($membership->membership_status==Membership::STATUS_ACTIVE_MONTH): $membershipType = 'Monthly';
				elseif($membership->membership_status==Membership::STATUS_ACTIVE_PRO_LIFETIME): $membershipType = 'Tabfu pro lifetime';
				elseif($membership->membership_status==Membership::STATUS_ACTIVE_AGENCY): $membershipType = 'Tabfu pro agency';
				else: $membershipType = 'Expired';
				endif; 
			else:
				$membershipType = "Trial";
			endif;
		?>
<div class="membership container-fluid">
    <div class="row-fluid">
        <div class="col-md-offset-1 col-md-10 background">
				<div class="members-section">
					<a href="http://www.tabfu.com/pricing" target="_BLANK" class="btn btn-default pull-right">
					&nbsp;See All Packages</a>
					<h2>Membership Details</h2>
					<div class="form-group">
						<label class="control-label" for="exampleInputPassword1">Subscriber since:</label>
						<p><strong><?php $time = strtotime($user->registerdate); echo date('j F, Y',$time);?></strong></p>
					</div>
					<div class="form-group">
						<label class="control-label" for="exampleInputPassword1">Account type:</label>
						<p><strong><?php echo $membershipType; ?></strong></p>
					</div>
					<div class="form-group">
						<label class="control-label" for="exampleInputPassword1">Next billing date:</label>
						<p><strong> 
						<?php 
/*							if(!$membership || ($membership && $membership->membership_status!=Membership::STATUS_ACTIVE_LIFETIME)): */
							if($membership && ($membership->membership_status!=Membership::STATUS_ACTIVE_LIFETIME && $membership->membership_status!=Membership::STATUS_ACTIVE_PRO_LIFETIME)): 
								echo date('d F Y', strtotime($membership->membership_end_date)).'('.$this->_membershipDateDiffHelper($membership).')';
							else:
								echo 'None';
							endif;
						?>
						</strong></p>
                    </div>
					
					<?php if($membership && $membership->isActive()): ?>
					<div class="row" style="margin-top: 30px;">
						<div class="col-md-offset-1 col-md-10">
							<span>To cancel your membership please contact support at: <a href="mailto:support@tabfu.zendesk.com">support@tabfu.zendesk.com</a></span>
						</div>
					</div>
					<?php endif; ?>

					
				</div>
        </div>
    </div>
</div>
<br>
<div class="membership container-fluid">
    <div class="row-fluid">
        <div class="col-md-offset-1 col-md-10 background">
				<div class="members-section">
					<h2 style="text-align:center;">Package Detail</h2>
					<br>
					<?php 
						$upgraded = false;

						$feArr = Yii::app()->params['package']['FE'];
						$allow_tabs = Yii::app()->user->FE;

						(Yii::app()->user->Agency) ? $upgraded = true : (Yii::app()->user->FE == 18) ? $upgraded = true : $upgraded = false; 
						
						$button = '<a href="http://www.tabfu.com" class="custom-btn selected move-right text-center disable" style="display:inline-block;width: 120px;">Upgrade</a>';
						
						if($upgraded)
							$button = '<a href="#" class="custom-btn selected move-right text-center disable" style=" background-color:green; display:inline-block;width: 120px;">Upgraded</a>';
					?>	
					
					
					<h3 style="display:inline-block;width: 70%;margin-left: 5%;margin-bottom: 20px;"> Feature Set 1 </h3>
					<?php echo $button; ?>
					<div class="form-group">
						<table class="table table-condensed">
							<thead>
								<tr>
									<th style="text-align:center; font-size:18px; width:50%;">Feature</th>
									<th style="text-align:center; font-size:18px;">Selected</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									foreach($feArr as $key => $value):?>
										<tr style="text-align:center; font-size:16px;">
											<td><?php echo $value.' Tabs';?></td>
											<td style="font-size:22px;">
												<?php 
													if($allow_tabs == $value):
														echo '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ';
													elseif($allow_tabs == 30):
														echo '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ';													
													else:
														switch($allow_tabs)
														{
															case 8:
																echo '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ';
																$allow_tabs = $allow_tabs - $value;
																break;
															case 13:
																if($value == 5)
																	break;
																echo '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ';
																$allow_tabs = $allow_tabs - $value;
																break;
															case 15:
																if($value == 3){
																	echo '<i class="fa fa-times-circle-o" style="color:red;" aria-hidden="true"></i>';
																	break;
																}
																echo '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ';
																$allow_tabs = $allow_tabs - $value;
																break;
															case 18:
																echo '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ';
																$allow_tabs = $allow_tabs - $value;
																break;
															default:
																echo '<i class="fa fa-times-circle-o" style="color:red;" aria-hidden="true"></i>';
																break;
														}
													endif;
												?>
											</td>
										</tr>
							   <?php endforeach; ?>							   
							</tbody>
						</table>
					</div>
					<?php 
						$upgraded = false;
						
						$oto1 = (Yii::app()->params['package']['essential-pack']);
						
						$featureCount = 0;
						
						for($i = 0; $i < count($oto1); $i++):
						
							$totFeature = count($oto1);
							
							$feature = $oto1[$i];
							
							if($oto1[$i]=='professional')
								$oto1[$i] = 'professionaltabs';
							
								$name = ucfirst($oto1[$i]);
								
							(Yii::app()->user->$name) ? $featureCount += 1 : '';
							($featureCount == $totFeature) ? $upgraded = true : $upgraded = false; 
						endfor; 
						
						
						$button = '<a href="http://www.tabfu.com" class="custom-btn selected move-right text-center disable" style="display:inline-block;width: 120px;">Upgrade</a>';
						
						if($upgraded)
							$button = '<a href="#" class="custom-btn selected move-right text-center disable" style=" background-color:green; display:inline-block;width: 120px;">Upgraded</a>';
					?>	
					
					<h3 style="display:inline-block;width: 70%;margin-left: 5%;margin-bottom: 20px;"> Feature Set 2 </h3>
					<?php echo $button; ?>
							   
					<div class="form-group">
						<table class="table table-condensed">
							<thead>
								<tr>
									<th style="text-align:center; font-size:18px; width:50%;">Feature</th>
									<th style="text-align:center; font-size:18px;">Selected</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									for($i = 0; $i < count($oto1); $i++):
										$feature = $oto1[$i];
										if($oto1[$i]=='professional'){
											$feature = 'professional Tabs';
										}
								?>
									<tr style="text-align:center; font-size:16px;">
										<td><?php echo $name = ucfirst($feature);?></td>
										<td style="font-size:22px;">
											<?php echo (Yii::app()->user->$name) ? '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ' : '<i class="fa fa-times-circle-o" style="color:red;" aria-hidden="true"></i>'; ?>
										</td>
									</tr>
							   <?php 
									endfor; 
							   ?>							   
							</tbody>
						</table>
					</div>
					
					<?php 
						$upgraded = false;
						
						$oto2 = (Yii::app()->params['package']['plus-pack']);
						
						$featureCount = 0;
						
						for($i = 0; $i < count($oto2); $i++):
						
							$totFeature = count($oto2);
							
							$name = ucfirst($oto2[$i]);
							
							(Yii::app()->user->$name) ? $featureCount += 1 : '';
							($featureCount == $totFeature) ? $upgraded = true : $upgraded = false; 
							
						endfor; 
						
						$button =  '<a href="http://www.tabfu.com" class="custom-btn selected move-right text-center disable" style="display:inline-block;width: 120px;">Upgrade</a>';
						
						if($upgraded)
							$button = '<a href="#" class="custom-btn selected move-right text-center disable" style=" background-color:green; display:inline-block;width: 120px;">Upgraded</a>';
					?>						
					
					<h3 style="display:inline-block;width: 70%;margin-left: 5%;margin-bottom: 20px;"> Feature Set 3 </h3>
					<?php echo $button; ?>
					<div class="form-group">
						<table class="table table-condensed">
							<thead>
								<tr>
									<th style="text-align:center; font-size:18px; width:50%;">Feature</th>
									<th style="text-align:center; font-size:18px;">Selected</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									for($i = 0; $i < count($oto2); $i++):
									?>
										<tr style="text-align:center; font-size:16px;">
											<td><?php echo $name = ucfirst($oto2[$i]);?></td>
											<td style="font-size:22px;">
												<?php
												echo (Yii::app()->user->$name) ? '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ' : '<i class="fa fa-times-circle-o" style="color:red;" aria-hidden="true"></i>'; ?>
											</td>
										</tr>
							   <?php endfor; ?>							   
							</tbody>
						</table>
					</div>
					<?php 
						$upgraded = false;
						
						$agency = array('Unlimited Tabs','Unlimited Profiles');
						
						(Yii::app()->user->Agency) ? $upgraded = true : $upgraded = false;

						$button =  '<a href="http://www.tabfu.com" class="custom-btn selected move-right text-center disable" style="display:inline-block;width: 120px;">Upgrade</a>';
						
						if($upgraded)
							$button = '<a href="#" class="custom-btn selected move-right text-center disable" style=" background-color:green; display:inline-block;width: 120px;">Upgraded</a>';
					?>						
					
					<h3 style="display:inline-block;width: 70%;margin-left: 5%;margin-bottom: 20px;"> Agency </h3>
					<?php echo $button; ?>
					<div class="form-group">
						<table class="table table-condensed">
							<thead>
								<tr>
									<th style="text-align:center; font-size:18px; width:50%;">Feature</th>
									<th style="text-align:center; font-size:18px;">Selected</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									for($i = 0; $i < count($agency); $i++):
									?>
										<tr style="text-align:center; font-size:16px;">
											<td><?php echo ucfirst($agency[$i]);?></td>
											<td style="font-size:22px;">
												<?php echo (Yii::app()->user->Agency) ? '<i class="fa fa-check-circle" style="color:green;" aria-hidden="true"></i> ' : '<i class="fa fa-times-circle-o" style="color:red;" aria-hidden="true"></i>'; ?>
											</td>
										</tr>
							   <?php endfor; ?>							   
							</tbody>
						</table>
					</div>

				</div>
        </div>
    </div>
</div>

<br>

<div class="membership container-fluid">
    <div class="row-fluid">
        <div class="col-md-offset-1 col-md-10 background">
				<div class="members-section">
					<a href="https://tabfu.zendesk.com/hc/en-us/requests/new" target="_BLANK" class="btn btn-default pull-right"><span class="glyphicon glyphicon-wrench"></span>&nbsp;New Support Ticket</a>
					<h2>Support</h2>
					<div class="form-group">
						<table class="table table-condensed">
							<thead>
								<tr>
									<th>Opened</th>
									<th>Replies</th>
									<th>Subject</th>
									<th>Status</th>
									<th>Resolved</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							<?php //print_r($tickets);?>
								<?php if(!empty($tickets) && $tickets->error === null){
										foreach ($tickets->tickets as $key => $item){  ?>
									<tr>
									<td><?php echo date('d M Y', strtotime($item->created_at)) ?></td>
									<td><?php echo $item->metricks->ticket_metric->replies; ?></td>
									<td style="text-transform:capitalize;"><?php echo $item->subject;?></td>
									<td style="text-transform:capitalize;"><?php echo $item->status;?></td>
									<td><?php  if($item->metricks->solved_at != null ) echo date('d M Y', strtotime($item->metricks->ticket_metric->solved_at)); ?></td>
									<td>
										<span  style="color:#2a6496;cursor: pointer;" data-toggle="modal" data-target="#myModal<?php echo $key; ?>" ><span data-toggle="modal" data-target="#myModal<?php echo $key; ?>" class="glyphicon glyphicon-eye-open"></span>&nbsp;View</span>
									</td>
								<div class="modal fade" id="myModal<?php echo $key; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
									  <div class="modal-content">
										<div class="modal-header">
										  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										  <h4 class="modal-title" id="myModalLabel">Ticket</h4>
										</div>
										<div class="modal-body">
											<p><div class="ticket_form">Ticket_id:</div> <?php echo $item->id?></p>
											<p><div class="ticket_form">Subject:</div> <?php echo $item->subject?></p>
											<p><div class="ticket_form">Description:</div> <?php echo $item->description?></p>
											<p><div class="ticket_form">Status:</div> <?php echo $item->status?></p>
											<p><div class="ticket_form">Created:</div> <?php echo date('d M Y', strtotime($item->created_at));?></p>
											<p><div class="ticket_form">Resolved:</div> <?php echo date('d M Y', strtotime($item->metricks->ticket_metric->solved_at));?></p>
											<p><div class="ticket_form">Replies:</div><?php echo $item->metricks->ticket_metric->replies; ?></p>
										</div>
										<div class="modal-footer">
										  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									  </div>
									</div>
								  </div>
									 </tr>
										<?php }}?>
							   
							</tbody>
						</table>
					</div>
				</div>
        </div>
    </div>
</div>

<style>
.container-fluid{
	height:100%;
}

</style>

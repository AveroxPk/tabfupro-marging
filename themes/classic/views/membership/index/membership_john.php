
application/x-httpd-php index.php ( exported SGML document, ASCII text )

    <div class="row-fluid">
        <div class="col-md-offset-1 col-md-10 background">
                
<!--            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h1>Membership</h1>
                </div>
            </div>-->
            
           
            
               <div class="col-md-offset-1  col-md-10">
                  <?php
                   $form = $this->beginWidget('CActiveForm', array(
                       'enableClientValidation' => true,
                       'method'=>'POST',
                       'clientOptions' => array(
                           'validateOnSubmit' => true,
                       ),
                   ));
                   ?>
                   <div class="members-section">
                     <button type="button" class="btn btn-default pull-right" data-target="#modCloseAccount" data-toggle="modal">Cancel Account</button>  
                       <h1>Membership</h1>  
                       <h2>Profile Details</h2>
                       
                       <?php if (Yii::app()->user->hasFlash('success')): ?>
                           <div class="info">
                               <?php echo Yii::app()->user->getFlash('success'); ?>
                           </div>
                       <?php endif; ?>
                        
                       
                       <?php echo $form->errorSummary($user); ?>
                       
                       <div class="form-group">
                           <?php echo $form->labelEx($user,'name',array('class'=>'control-label')); ?>
                           <?php echo $form->textField($user,'name',array('class'=>'form-control','placeholder'=>'Name')); ?>
                           <?php echo $form->error($user,'name'); ?>
                       </div>
                       
                        <div class="form-group">
                           <?php echo $form->labelEx($user,'email',array('class'=>'control-label')); ?>
                           <?php echo $form->textField($user,'email',array('class'=>'form-control','placeholder'=>'Enter email')); ?>
                           <?php echo $form->error($user,'email'); ?>
                        </div>
                       
                        <div class="form-group">
                           <?php echo $form->labelEx($user,'new_password',array('class'=>'control-label')); ?>
                           <?php echo $form->passwordField($user,'new_password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                           <?php echo $form->error($user,'new_password'); ?>
                        </div>
                       
                        <div class="form-group">
                           <?php echo $form->labelEx($user,'verify_password',array('class'=>'control-label')); ?>
                           <?php echo $form->passwordField($user,'verify_password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                           <?php echo $form->error($user,'verify_password'); ?>
                        </div>
                       
                        <div class="form-group">
                           <?php echo $form->labelEx($user,'current_password',array('class'=>'control-label')); ?>
                           <?php echo $form->passwordField($user,'current_password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                           <?php echo $form->error($user,'current_password'); ?>
                        </div>
                       
                       <div class="form-group">
                           <?php echo $form->labelEx($user,'current_password2',array('class'=>'control-label')); ?>
                           <?php echo $form->passwordField($user,'current_password2',array('class'=>'form-control','placeholder'=>'Password')); ?>
                           <?php echo $form->error($user,'current_password2'); ?>
                        </div>
                       <div class="form-group">
                           <label class="control-label" >Add a login with the following social accounts:</label>
                            <?php $this->widget('ext.hoauth.widgets.HOAuth',array('onlyIcons'=>'yes','popupWidth'=>'650','popupHeight'=>'700')); ?>
                           </div>
                       
                       <?php echo CHtml::submitButton('Update',array('class' => 'btn btn-default')); ?>
                       
                   </div>
                   <?php $this->endWidget(); ?>
<!--                                    <form role="form">
                                        <div class="members-section">
                                        
                                            <h1>Membership</h1>  
                                            <h2>Profile Details</h2>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputPassword1">Name:</label>
                                                <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputEmail1">Email address:</label>
                                                <input class="form-control" id="exampleInputEmail1" placeholder="Enter email" type="email">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputPassword1">New Password:</label>
                                                <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputPassword1">Verify Password:</label>
                                                <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputPassword1">Current Password:</label>
                                                <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputPassword1">Current Password:</label>
                                                <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password">
                                            </div>
                                            <button type="submit" class="btn btn-default">Update</button>
                                        </div>
                                    </form>-->
                                    <div class="members-section">
                                        <a href="https://paypal.com" target="_BLANK" class="btn btn-default pull-right">Manage Payment Options</a>
                                        <h2>Personal Details</h2>
                                        <div class="form-group">
                                            <label class="control-label" for="exampleInputPassword1">Subscriber since:</label>
                                            <p><strong><?php $time = strtotime($user->registerdate); echo date('j F, Y',$time); ?></strong></p>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="exampleInputPassword1">Account type:</label>
                                            <p><strong>Lifetime</strong></p>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="exampleInputPassword1">Next billing date:</label>
                                            <p><strong>None</strong></p>
                                        </div>
                                   <!--     <div class="form-group">
                                            <label class="control-label" for="exampleInputPassword1">Make a payment with:</label>
                                            <p><button class="btn btn-default">Credit Card</button>&nbsp;or&nbsp;<button class="btn btn-primary">PayPal</button></p>
                                        </div> -->
                                    </div>
                                    <div class="members-section">
                                        <a href="https://tabfu.zendesk.com/hc/en-us/requests/new" target="_BLANK" class="btn btn-default pull-right"><span class="glyphicon glyphicon-wrench"></span>&nbsp;New Support Ticket</a>
                                        <h2>Support</h2>
                                        <div class="form-group">
                                            <table class="table table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>Opened</th>
                                                        <th>Replies</th>
                                                        <th>Subject</th>
                                                        <th>Status</th>
                                                        <th>Resolved</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(!empty($tickets)){
                                                            foreach ($tickets->tickets as $key => $item){  ?>
                                                        <tr>
                                                        <td><?php echo date('d M Y', strtotime($item->created_at)) ?></td>
                                                        <td><?php echo $item->metricks->ticket_metric->replies; ?></td>
                                                        <td style="text-transform:capitalize;"><?php echo $item->subject;?></td>
                                                        <td style="text-transform:capitalize;"><?php echo $item->status;?></td>
                                                        <td><?php  if($item->metricks->solved_at != null ) echo date('d M Y', strtotime($item->metricks->ticket_metric->solved_at)); ?></td>
                                                        <td>
                                                            <span  style="color:#2a6496;cursor: pointer;" data-toggle="modal" data-target="#myModal<?php echo $key; ?>" ><span data-toggle="modal" data-target="#myModal<?php echo $key; ?>" class="glyphicon glyphicon-eye-open"></span>&nbsp;View</span>
                                                        </td>
                                                <div class="modal fade" id="myModal<?php echo $key; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                              <h4 class="modal-title" id="myModalLabel">Ticket</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p><div class="ticket_form">Ticket_id:</div> <?php echo $item->id?></p>
                                                                <p><div class="ticket_form">Subject:</div> <?php echo $item->subject?></p>
                                                                <p><div class="ticket_form">Description:</div> <?php echo $item->description?></p>
                                                                <p><div class="ticket_form">Status:</div> <?php echo $item->status?></p>
                                                                <p><div class="ticket_form">Created:</div> <?php echo date('d M Y', strtotime($item->created_at));?></p>
                                                                <p><div class="ticket_form">Resolved:</div> <?php echo date('d M Y', strtotime($item->metricks->ticket_metric->solved_at));?></p>
                                                                <p><div class="ticket_form">Replies:</div><?php echo $item->metricks->ticket_metric->replies; ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                         </tr>
                                                            <?php }}?>
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                            
            </div>

            
        </div>
    </div>
<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<div>
    <a href="<?php echo Yii::app()->createUrl('/admin/user/admin'); ?>" type="button" style="display: inline-block" class="btn btn-default">Manage users</a> 
</div>

<h1>Create User</h1>

<?php $this->renderPartial('_form_create', array('model'=>$model)); ?>
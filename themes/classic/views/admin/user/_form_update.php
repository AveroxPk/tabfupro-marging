<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
                <?php echo $model->email; ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>72)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastname'); ?>
		<?php echo $form->textField($model,'lastname',array('size'=>60,'maxlength'=>72)); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model->membership,'.membership_status'); ?>
		<?php echo $form->dropDownList
					(
						$model->membership,
						'membership_status',
						Membership::getMembershipStatuses(),
						array
						(
							'onchange'=>
							'if(
								$(this).val()=='.
								Membership::STATUS_ACTIVE_MONTH.
								' || $(this).val()=='.
								Membership::STATUS_ACTIVE_QUARTER.
								' || $(this).val()=='.
								Membership::STATUS_ACTIVE_YEAR.
								' || $(this).val()=='.
								Membership::STATUS_ACTIVE_INDIVIDUAL.
								//HB//START
								' || $(this).val()=='.
								Membership::STATUS_ACTIVE_TWO_YEAR.
								' || $(this).val()=='.
								Membership::STATUS_ACTIVE_THREE_YEAR.
								//HB//END
							')
							{
								$(\'.ending\').not(\'.hidden\').show(); 
							}
							else 
							{
								$(\'.ending\').hide();'.
								//HB//START
								'$( "#Membership_recurring" ).attr("checked", false);'. 
								//HB//END
							'}'
						)
					);
		?>
		<?php echo $form->error($model->membership,'membership_status'); ?>
	</div>
        
        <?php
        // if membership_month or membership_year show membership_end_date and recurring fields
        $endingMembership = in_array
		(
			$model->membership->membership_status,
			array
				(
					Membership::STATUS_ACTIVE_MONTH,
					Membership::STATUS_ACTIVE_QUARTER,
					Membership::STATUS_ACTIVE_YEAR,
					//HB//START
					Membership::STATUS_ACTIVE_TWO_YEAR,
					Membership::STATUS_ACTIVE_THREE_YEAR
					//HB//END
				)
		);   
        ?>
        
        <div class="row ending" <?php if(!$endingMembership) { echo "style='display: none;'"; } ?>>
		<?php echo $form->labelEx($model->membership,'membership_end_date'); ?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', 
                    [
                        'model'=>$model->membership,
                        'attribute'=>'membership_end_date',
                        'options'=>[
                            'showAnim'=>'fold',
                            'autoSize'=>true,
                            'dateFormat'=>'yy-mm-dd',
                        ],
                        'htmlOptions'=>[
                            'value'=>$model->membership->membership_end_date=='0000-00-00 00:00:00'? '0000-00-00' : date_format(new DateTime($model->membership->membership_end_date),'Y-m-d'),
                        ],
                    ]); 
                ?>
		<?php echo $form->error($model->membership,'membership_end_date'); ?>
	</div>
        
        <div class="row ending" <?php if(!$endingMembership) { echo "style='display: none;'"; } ?>>
		<?php echo $form->labelEx($model->membership,'recurring'); ?>
		<?php echo $form->checkBox($model->membership,'recurring',array('onchange'=>'if($(this).is(\':checked\')) { $(\'#Membership_recurring_date\').parent().show().removeClass(\'hidden\'); } else { $(\'#Membership_recurring_date\').parent().hide().addClass(\'hidden\'); }')); ?>
		<?php echo $form->error($model->membership,'recurring'); ?>
	</div>
        
        <div class="row ending<?php echo $model->membership->recurring?'':" hidden"; ?>" <?php echo (!$endingMembership || $model->membership->recurring)?'':"style='display: none;'"; ?>>
		<?php echo $form->labelEx($model->membership,'recurring_date'); ?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', 
                    [
                        'model'=>$model->membership,
                        'attribute'=>'recurring_date',
                        'options'=>[
                            'showAnim'=>'fold',
                            'autoSize'=>true,
                            'dateFormat'=>'yy-mm-dd',
                        ],
                        'htmlOptions'=>[
                            'value'=>($model->membership->recurring_date=='0000-00-00 00:00:00' || !$model->membership->recurring_date) ? '0000-00-00' : date_format(new DateTime($model->membership->recurring_date),'Y-m-d'),
                        ],
                    ]); 
                ?>
		<?php echo $form->error($model->membership,'recurring_date'); ?>
	</div>
        
	<div class="row buttons">
		<?php echo CHtml::submitButton('Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
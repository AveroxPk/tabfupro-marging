<?php
/* @var $this UserController */
/* @var $model User */

$this->pageTitle = 'Users';

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div>
    <a href="<?php echo Yii::app()->createUrl('/admin/user/create'); ?>" type="button" style="display: inline-block" class="btn btn-default">Create user</a> 
</div>

<h1>Manage Users</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'user_id',
		'email',
		'name',
		'lastname',
                'registerdate',
                array(
                        'name' => 'membership.membership_status',
                        'header' => "Membership",
                        'value' => array($model,'renderMembershipStatus'), 
                        'htmlOptions' => array('style' => "text-align:center;"),
                ),
                'membership.membership_end_date',
		array(
			'class'=>'CButtonColumn',
                        'template' => '{view}{update}',   
		),
	),
)); ?>

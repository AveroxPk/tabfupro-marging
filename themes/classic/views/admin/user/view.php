<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->user_id)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->user_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<div>
    <a href="<?php echo Yii::app()->createUrl('/admin/user/admin'); ?>" type="button" style="display: inline-block" class="btn btn-default">Manage users</a> 
    <a href="<?php echo Yii::app()->createUrl('/admin/user/update/id/'.$model->user_id); ?>" type="button" style="display: inline-block" class="btn btn-default">Edit this user</a> 
</div>

<h1>View User #<?php echo $model->user_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'user_id',
		'email',
		'name',
		'lastname',
                'registerdate',
                array(
                        'name' => 'membership.membership_status',
                        'header' => "Membership",
                        'value' => $model->renderMembershipStatus($model),
                ),
                'membership.membership_end_date',
                'membership.recurring:boolean',
                'membership.recurring_date',
	),
)); ?>

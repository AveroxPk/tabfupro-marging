<?php
/* @var $this DefaultController */

$this->pageTitle = 'Message - create';
?>

<h3 style="display: inline-block">Create new message</h3>
<div class="well span5" style="margin:10px 0 20px">
    <div class="messageForm">
    <?php echo CHtml::beginForm(); ?>

    <?php echo CHtml::errorSummary($message); ?>

    <div class="simple">
    <?php echo CHtml::activeLabel($message,'title'); ?>
    <?php echo CHtml::activeTextField($message,'title', array('class'=>'form-control')); ?>
    </div>
    <div class="simple">
    <?php echo CHtml::activeLabel($message,'message_category_id'); ?>
    <?php echo CHtml::dropDownList('Message[message_category_id]','message_category_id', $categories, array('class'=>'form-control')); ?>
    </div>
    <div class="simple">
    <?php echo CHtml::activeLabel($message,'content'); ?>
    <?php echo CHtml::activeTextArea($message,'content', array('class'=>'form-control summernote', 'style'=>'height: 300px')); ?>
    </div>
    <div class="simple">
    <?php echo CHtml::activeLabel($message,'target'); ?>
    <?php echo CHtml::dropDownList('Message[target]','target', $targets, array('class'=>'form-control')); ?>
    </div>    
    <div class="simple">
    <?php echo CHtml::activeLabel($message,'special'); ?>
    <?php echo CHtml::activeCheckBox($message,'special'); ?>
    </div>

    <div class="action">
    <br/>
    <?php echo CHtml::submitButton('create', array('class'=>'btn btn-primary')); ?>
    </div>

    <?php echo CHtml::endForm(); ?>
    </div>
</div>
<script>
        jQuery('.summernote').summernote({height: 300});

</script>
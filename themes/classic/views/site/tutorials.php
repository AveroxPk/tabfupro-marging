<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/custom.css'); ?>
<h1 class="tut-caption">Tutorials</h1>

<div class="tut-lead">
    <p>
        Below you will find Tutorials and Walkthroughs on the key features of Tabfu,<br/>
        simply scroll down and click play.
    </p>
    <p>
        If you are stuck please <a href="https://tabfu.zendesk.com/hc/en-us">READ THE FAQ</a> on the Support page or 
        <a href="https://tabfu.zendesk.com/hc/en-us/requests/new">Submit a Ticket</a>
    </p>
    <p>Thanks for using Tabfu!</p>
</div>

<div class="tut-videos">
    <p>Creating a Facebook Tab with the Template Editor</p>
    <object width="640" height="360"><param name="movie" value="//www.youtube.com/v/KkMYKTkbcFM?hl=pl_PL&amp;version=3&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/KkMYKTkbcFM?hl=pl_PL&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" width="640" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>
    
    <p>Setting up Viral Gates in the Template Editor</p>
    <object width="640" height="360"><param name="movie" value="//www.youtube.com/v/4ybubdYngEI?version=3&amp;hl=pl_PL&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/4ybubdYngEI?version=3&amp;hl=pl_PL&amp;rel=0" type="application/x-shockwave-flash" width="640" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>
    
    <p>Connecting Autoresponders</p>
    <object width="640" height="360"><param name="movie" value="//www.youtube.com/v/3SUP2QX58HI?hl=pl_PL&amp;version=3&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/3SUP2QX58HI?hl=pl_PL&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" width="640" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>
    
    <p>Features of the Facebook Tabs Page</p>
    <object width="640" height="360"><param name="movie" value="//www.youtube.com/v/dw-S3OUFqgc?version=3&amp;hl=pl_PL&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/dw-S3OUFqgc?version=3&amp;hl=pl_PL&amp;rel=0" type="application/x-shockwave-flash" width="640" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>
    
    <p>Creating Image Promo Posts</p>
    
    <p>Creating Video promo Posts</p>
    
    <p>Creating and Exporting Facebook Ads</p>
    <object width="640" height="360"><param name="movie" value="//www.youtube.com/v/EW_G6fJKKjU?hl=pl_PL&amp;version=3&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/EW_G6fJKKjU?hl=pl_PL&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" width="640" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>
    
    <p>Features of the Analytics Page</p>
</div>
<style>
	.container-fluid{
		background: url(/images/new-layout/bg.jpg) no-repeat;		
		height:100vh;
	}

</style>
<div class="row center-block">
    <div class="col-sm-offset-3 col-sm-4 col-xs-12">
        <?php 

        $form=$this->beginWidget('CActiveForm', array(
                'id'=>'form-user',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            'htmlOptions'=>array(
                'class' => 'register-form'
            )
        )); ?>
        
        <header>
            Login below for your <strong>Free 15 Day Trial!</strong>
        </header>
        
        <div class="row" style="display:none;">
            <?php // echo CHtml::link('Login with Facebook', '#', array('class'=>'facebook-login', 'id'=>'facebookLogin')) ?>
        </div>

        <p class="login-or" style="display:none;">OR</p>

        <div class="row">
            <?php echo $form->textField($model,'email',array('placeholder'=>'Your Email Here')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>

        <div class="row">
            <?php echo $form->passwordField($model,'password',array('placeholder'=>'Your Password Here')); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>

        <div class="row">
            <?php echo $form->passwordField($model,'repeatPassword',array('placeholder'=>'Your Password Again')); ?>
            <p class="privacy" style="margin-top:-10px">Please confirm your password here</p>
            <?php echo $form->error($model,'repeatPassword'); ?>
        </div>

        <div class="row">
            <?php echo CHtml::submitButton('Get Instant Access'); ?>

            <p class="privacy">
                We value your privacy and will never share your email.
            </p>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
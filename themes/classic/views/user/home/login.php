

<div class="row center-block login-design">
    <div class="col-sm-offset-1 col-md-offset-2 col-md-8 col-sm-10 col-xs-12">
        <?php 

        $form=$this->beginWidget('CActiveForm', array(
                'id'=>'form-user',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                ),
        )); ?>
        
        <header>
            <img src="/images/new-layout/login.png">
        </header>

        <div class="content">
            <?php if($displayFacebookLogin): ?>
                <div class="row" style="display:none;">
                    <?php echo CHtml::link('Login with Facebook', '#', array('class'=>'facebook-login', 'id'=>'facebookLogin')) ?>
                </div>

                <p class="login-or" style="display:none;">OR</p>
            <?php endif; ?>
			
            <div class="row links">

                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <span>New User? </span><?php echo CHtml::link('Register here!', Yii::app()->baseUrl.$this->createUrl('/user/home/register')) ?>
                </div>
				
                <div class="col-xs-12 col-md-8 col-md-offset-2 forgetpassword">
                    <?php echo CHtml::link('Forgot Password?', Yii::app()->baseUrl.$this->createUrl('/user/home/forgetpassword')) ?>
                </div>
				
            </div>
			
            <div class="row rememberMe">
                <div class="col-xs-6 col-md-6" style="text-align:center;">
                    <?php echo $form->checkBox($model,'rememberMe'); ?>
                    <?php echo $form->label($model,'rememberMe'); ?>
                    <?php echo $form->error($model,'rememberMe'); ?>
                </div>
				<div class="col-xs-6 col-md-6 help-area">
					<a href="http://tabfu.com/tutorials/" alt="Tutorials" target="_blank"><i class="icofont icofont-play-alt-3"></i></a>
					<a href="https://tabfu.zendesk.com/hc/en-us" target="_blank" alt="Support"><i class="icofont icofont-support-faq"></i></a>
				</div>
            </div>
			
			<div class="row">
				<div class="col-xs-8 col-xs-offset-2">
					<?php echo $form->error($model,'email'); ?>
					<?php echo $form->error($model,'password'); ?>			
				</div>
			</div>
			
            <div class="row text-center">
				<div class="input-container">
					<?php echo $form->textField($model,'email',array('placeholder'=>'Your Email Here')); ?>
					<?php echo $form->passwordField($model,'password', array('placeholder'=>'Your Password Here')); ?>
				</div>
				<?php echo CHtml::submitButton('Go'); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>

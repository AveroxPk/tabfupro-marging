<style>
	.container-fluid{
		background: url(../images/new-layout/bg.jpg) no-repeat;		
	}

</style>

<div class="row center-block">
    <div class="col-sm-offset-3 col-sm-4 col-xs-12">

<?php 

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'form-user',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

        <header>
            Enter your email to reset your password
        </header>
        
	<div class="row">
            <?php echo $form->textField($model,'email',array('placeholder'=>'Your Email Here')); ?>
            <?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
		<?php echo CHtml::submitButton('Reset'); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

<style>
	.container-fluid{
		background: url(../images/new-layout/bg.jpg) no-repeat;		
	}

</style>
<div class="row center-block">
    <div class="col-sm-offset-3 col-sm-4 col-xs-12">
        <?php 

        $form=$this->beginWidget('CActiveForm', array(
                'id'=>'form-user',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                'validateOnSubmit'=>true,
                ),
        )); ?>


        <p class="note">Fields with <span class="required">*</span> are required.</p>


	<div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <?php echo $form->labelEx($model,'newPassword'); ?>
                        <?php echo $form->passwordField($model,'newPassword'); ?>
                        <?php echo $form->error($model,'newPassword'); ?>
                            </div>
            </div>

            <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <?php echo $form->labelEx($model,'passwordVerify'); ?>
                        <?php echo $form->passwordField($model,'passwordVerify'); ?>
                        <?php echo $form->error($model,'passwordVerify'); ?>
                            </div>
            </div>

            <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <?php echo CHtml::submitButton('Change password'); ?>
            </div>
	</div>


        <?php $this->endWidget(); ?>
    </div>
</div>
<style>
	.container-fluid{
		background: url(../images/new-layout/bg.jpg) no-repeat;		
	}

</style>

<div class="row center-block">
    <div class="col-sm-offset-3 col-sm-4 col-xs-12">
        <?php 

        $form=$this->beginWidget('CActiveForm', array(
            'id'=>'form-user',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
            'htmlOptions'=>array(
                'class'=>'email-confirmation'
            )
        )); ?>
        
        <header>
            Email Confirmation
        </header>

        <div class="content">
            <div class="row">
                <img src="/images/envelope.png" alt="Envelope" />
                <p>
                    Please check Inbox / Spam folder<br/>
                    for our email and click the<br/>
                    confirmation link inside<br />
					Thanks!
					
                </p>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>

<style>
	.container-fluid{
		background: url(../images/new-layout/bg.jpg) no-repeat;		
		height:115vh;
	}

</style>
<div class="row center-block">
    <div class="col-sm-offset-3 col-sm-4 col-xs-12">
        
    <?php echo CHtml::beginForm('', 'post', array('id'=>'form-user', 'class'=>'your-profile')); ?>

    <header>
        Your Profile 
        <small>you can update your details below</small>
    </header>
        
	<div class="row">
        <?php //echo CHtml::activeLabel($user,'name'); ?>
        <?php echo CHtml::activeTextField($user,'name',array('class'=>'text-field','placeholder'=>'Your First Name')); ?>
        <?php echo CHtml::error($user,'name'); ?>
	</div>
	<div class="row">
        <?php //echo CHtml::activeLabel($user,'lastname'); ?>
        <?php echo CHtml::activeTextField($user,'lastname',array('class'=>'text-field','placeholder'=>'Your Last Name')); ?>
        <?php echo CHtml::error($user,'lastname'); ?>
	</div>
	<div class="row">
        <?php //echo CHtml::activeLabel($user,'email'); ?>
        <?php echo CHtml::activeTextField($user,'email',array('placeholder'=>'Your Email')); ?>
        <?php echo CHtml::error($user,'email'); ?>
	</div>
	<div class="row">
        <?php //echo CHtml::label('Change Password','User_password'); ?>
        <?php echo CHtml::activePasswordField($user,'password',array('placeholder'=>'Your Password')); ?>
        <?php echo CHtml::error($user,'password'); ?>
	</div>
	<div class="row">
        <?php //echo CHtml::activeLabel($user,'repeatPassword'); ?>
        <?php echo CHtml::activePasswordField($user,'repeatPassword',array('placeholder'=>'Your Password Again')); ?>
        <?php echo CHtml::error($user,'repeatPassword'); ?>
	</div>
        
    <?php /*
    <div class="row with-devider">
        <h3>Get Response</h3>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <?php echo CHtml::activeLabel($getResponse,'api_key'); ?>
            <?php echo CHtml::activeTextField($getResponse,'api_key'); ?>
        </div>
	</div>
    
        <div class="row with-devider">
            <h3>Aweber</h3>
            <p>You can find code <a href="https://auth.aweber.com/1.0/oauth/authorize_app/<?php echo $aweber->getAppid(); ?>" target="_blank">here</a>
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($aweber,'access_code'); ?>
                <?php echo CHtml::activeTextField($aweber,'access_code'); ?>
            </div>
	</div>
        <div class="row with-devider">
            <h3>Icontact</h3>
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($icontact,'app_login'); ?>
                <?php echo CHtml::activeTextField($icontact,'app_login'); ?>
            </div>
	</div>
         <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($icontact,'app_pass'); ?>
                <?php echo CHtml::activeTextField($icontact,'app_pass'); ?>
            </div>
	</div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($icontact,'app_id'); ?>
                <?php echo CHtml::activeTextField($icontact,'app_id'); ?>
            </div>
	</div>
<!--        <div class="row with-devider">
            <h3>Infusion Soft</h3>
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($infusionsoft,'app_name'); ?>
                <?php echo CHtml::activeTextField($infusionsoft,'app_name'); ?>
            </div>
	</div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($infusionsoft,'app_key'); ?>
                <?php echo CHtml::activeTextField($infusionsoft,'app_key'); ?>
            </div>
	</div>-->
        <div class="row with-devider">
            <h3>Mail Chimp</h3>
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($mailchimp,'api_key'); ?>
                <?php echo CHtml::activeTextField($mailchimp,'api_key'); ?>
            </div>
	</div>
<!--        <div class="row with-devider">
            <h3>Send Reach</h3>
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($sendreach,'api_user_id'); ?>
                <?php echo CHtml::activeTextField($sendreach,'api_user_id'); ?>
            </div>
	</div>
        <div class="row ">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($sendreach,'api_key'); ?>
                <?php echo CHtml::activeTextField($sendreach,'api_key'); ?>
            </div>
	</div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <?php echo CHtml::activeLabel($sendreach,'api_secret'); ?>
                <?php echo CHtml::activeTextField($sendreach,'api_secret'); ?>
            </div>
	</div>--> */ ?>
    
        <div class="row">
            <?php echo CHtml::submitButton('Submit'); ?>
        </div>

        <?php echo CHtml::endForm(); ?>
    </div>
</div>

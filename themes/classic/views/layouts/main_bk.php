<!doctype html>
<html class="no-js">
    <head>
        
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="fb:app_id" content="1496093447296300" />
        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/normalize.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css">
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/modernizr-2.7.1.min.js"></script>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-timepicker.css">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css">
        
        <script type="text/javascript">
		var facebookAppId = '<?= Yii::app()->params['FacebookAppId']; ?>';
        </script>
        
        <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <!-- Bootstrap -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ckeditor/ckeditor.js"></script>
        
        <title><?php echo Yii::app()->name;?></title>
        
    </head>
    <body>
        <?php
            $flashes = Yii::app()->user->getFlashes();
            if(count($flashes)>0){
                echo '<div class="flash-container col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1">';
                echo '<div class="flash-container-close">X</div>';
                foreach ($flashes as $key => $message) {
                    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
                }
                echo '</div>';
            }
            
            $warnings = Yii::app()->controller->getWarnings();
            $waringsArray = array();
            if (count($warnings)>0) {
                $containerClass = '';
                foreach($warnings as $warning) {
                    if($warning->type == Controller::WARNING_TYPE_COOKIE) {
                        if(isset(Yii::app()->request->cookies['hide_warning_message'])) {
                            continue;
                        }
                        $containerClass = ' set-cookie';
                    } 
                    $waringsArray[] = $warning->message;
                }
            }
            if(count($waringsArray)>0) {
                //echo '<div class="flash-container '.$containerClass.' col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1">';
                    foreach($waringsArray as $warning) {
                        echo '<div class="flash-warning" style="text-align:center; display: none;">';
                        echo $warning;
                        echo '<div class="flash-container-close'.$containerClass.'"><span class="glyphicon glyphicon-remove"></span></div>';
                        echo '</div>';
                    }
                //echo '</div>';
            }
            
            if(Yii::app()->getModule('facebook')->warning) {
                echo Yii::app()->getModule('facebook')->warning;
            }
            
        ?>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
            
        <div class="main">
			<div class="row-fluid">
				<div class="col-xs-3 sidebar">
					<div class="logo">
						<a href="/">
							<img src="<?php echo Yii::app()->baseUrl; ?>/images/header/logo.png" alt="TabFu" />
						</a>
					</div>
					<aside id="sidebar">
						<nav>
								<?php 
								$this->widget('zii.widgets.CMenu',array(
									'items'=>array(
										array('label'=>'Home',  'url'=>array('/facebook/facebook/index'), 'linkOptions'=>array('class'=>'icon-menu home', 'title' => 'Dojo')),
										array('label'=>'Tabs',  'url'=>array('/facebook/fanPage/index'), 'linkOptions'=>array('class'=>'icon-menu tabs', 'title' => 'Tabs')),
										array('label'=>'Motion Post',  'url'=>array('/facebook/motionpost/index'), 'linkOptions'=>array('class'=>'icon-menu motion', 'title' => 'Motion Posts')),
										array('label'=>'Facebook Ads',  'url'=>array('/facebook/ads/index'), 'linkOptions'=>array('class'=>'icon-menu fb-ad', 'Facebook Ads')),
										array('label'=>'Analytics',  'url'=>array('/analytics/analytics/index'), 'linkOptions'=>array('class'=>'icon-menu analyt', 'title' => 'Analytics')),
									),
									
								));
								?>
						</nav>
					</aside>
				</div>
				
				<div class="col-xs-9 content">
				
				</div>
			
			</div>
		</div>
            
            <!--Header-->
            <header id="header" class="hidden-xs">
				<div class="row-fluid">
					<div class="col-xs-2 col-xs-offset-1" style="display:none;">
						<div class="shop-icon">
							<a href="#" class="icon">
								<span class="circle info red">2</span>
							</a>
						</div>
					</div>
					<div class="col-xs-3">
					<?php //var_dump($this->user->getEndDate()); ?>
					<?php  if(!is_object($this->user)){Yii::app()->user->logout();}  ?>
						<?php if((!Yii::app()->user->isGuest && !$this->user->isMembershipActive()) || 
								(!Yii::app()->user->isGuest && (strtotime($this->user->membership->membership_end_date) < strtotime(date("Y-m-d h:i:s", time())))) && ($this->user->membership->membership_status != 3)): ?>
							<?php if($this->user->getRemainingFreeMembership()): ?>
							<div class="remainTimeBtn">
								<span class="remineTimeText">Trial <?=Yii::t('app', '{n} day|{n} days', $this->user->getRemainingFreeMembership());?> Remaining</span>
								<div style="clear:both"></div>
								<!-- <a href="<?php echo $this->createUrl('/membership/index/index'); ?>"><div class="remaineTimeBtnReal">Upgrade Now</div></a> -->
								<a href="http://www.tabfu.com/pricing" target="_blank"><div class="remaineTimeBtnReal">Upgrade Now</div></a>
							</div>
							<?php elseif($this->user->membership): ?>
							<div class="remainTimeBtn">
								<span class="remineTimeText">Your membership has expired</span>
								<div style="clear:both"></div>
								<a href="http://www.tabfu.com/pricing" target="_blank"><div class="remaineTimeBtnRealExpired">Upgrade Now</div></a>
							</div>
							<?php else: ?>
							<div class="remainTimeBtn">
								<span class="remineTimeText">Your trial has expired</span>
								<div style="clear:both"></div>
								<a href="http://www.tabfu.com/pricing" target="_blank">"><div class="remaineTimeBtnRealExpired">Upgrade Now</div></a>
							</div>
							<?php endif;?>
					   <?php endif; ?>
					</div>
					<div class="col-xs-6">
						<div class="menu-settings">
							<div class="support-button element">
								<a href="https://tabfu.zendesk.com/hc/en-us" class="support" alt="Support" target="_blank">
									Support
								</a>
							</div>
							<div class="tutorial-button element">
								<?= CHtml::link('Tutorials', 'http://tabfu.com/tutorials/', array(
									'class' => 'tutorials',
									'alt' => 'Tutorials',
									'target' => '_blank'
								)); ?>
							</div>
							<?php if(!Yii::app()->user->isGuest): ?>
							<div class="setting-button element">
								<a href="#" class="setting" alt="Settings">
									Settings
								</a>
								
								<div class="modal-tf setting-dialog hidden">
									<div class="modal-content-tf">
										<ul>
											<li>
												<a href="<?php echo $this->createUrl('/user/home/profile'); ?>" title="My Account">My Profile</a>
											</li>
											<li>
												<a href="<?php echo $this->createUrl('/membership/index/index'); ?>" class="active" title="Go Premium">Membership</a>
											</li>
											<li>
												<a href="<?php echo $this->createUrl('/user/home/logout'); ?>" title="Log Out">Log Out</a>
											</li>
										</ul>
									</div>
								</div>
								
							</div>
							<div class="setting-messages element">
								<!-- <a href="#" class="messages inbox"> --></a>
								<?php $unreaded = $this->unreadedmessages; ?>
								<a href="#" class="messages <?php echo count($unreaded)>0?'inbox':'';?>">
									<span class="circle info red bottom"><?php echo count($unreaded);?></span>
								</a>
								
								<div class="modal-tf message-dialog hidden">
									<div class="modal-content-tf">
										<div class="left-column-tf">
											<div class="header-tf">
												<h3>Inbox</h3>
											</div>
											<div class="content-tf">
											  <div class="messages-list">
												<ul>
													<?php foreach($unreaded as $message): ?>
													<li>
														<a class="message-link new" onclick="showmessage(jQuery(this));return false;" href="#" data-message-id="<?php echo $message["id"];?>"><?php echo $message["title"];?></a>
														
													</li>
													<?php endforeach;?>
													<?php foreach($this->readedmessages as $message): ?>
													<li>
														<a class="message-link" onclick="showmessage(jQuery(this));return false;" href="#" data-message-id="<?php echo $message["id"];?>"><?php echo $message["title"];?></a>
														
													</li>
													<?php endforeach;?>
												</ul>
											  </div>
											</div>
											<div class="footer-tf">
												<div class="load-button-modal">
													<!-- <a href="#" class="view-inbox-tf">
														View Inbox
													</a> -->
													<a href="#" onclick="showArchive(jQuery(this));return false;" class="view-archive-tf">
														View Archive
													</a>
												</div>
											</div>
										</div>
										<div class="right-column-tf">
											<div class="header-tf">
												<input type="text" value="Please select message" />
											</div>
											<div class="content-tf">
												<div class="message-scroll-content">
													Please select message
												</div>
												
											</div>
											<div class="footer-tf">
												<div class="button-place">
													
													<a href="#" class="move-to-archive" onclick="moveToArchiveMessage(); return false;">
														Archive
													</a>
													
													<a href="#" class="move-to-trash"  onclick="removeMessageConfirm(); return false;">
														Delete
													</a>
													
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
            </header>
            <!--/header-->
            
            <!--content-->
            <?php if(Yii::app()->getController()->id == 'facebook' && Yii::app()->getController()->action->id == 'index'): ?>
            <?php //if ($this->user->membership->	membership_status != 3): ?>
            <?php //if ((strtotime($this->user->membership->membership_end_date) < strtotime(date("Y-m-d h:i:s", time())))):?>
            <script>
				//window.location.assign("/membership");
            </script>
            <?php //endif; ?>
            <?php //endif; ?>
            <section class="content">
                
                
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-xs-12">
                            <?php  echo $content; ?>  
                        </div>
                    </div>
                </div>
                
            </section>
            
            <?php else: ?>
                <section class="content">
                <?php //if(Yii::app()->getController()->id == 'fanPage' || Yii::app()->getController()->id == 'ads' || Yii::app()->getController()->id == 'motionpost' || Yii::app()->getController()->id == 'analytics'): ?>
                
                <?php //if ($this->user->membership->	membership_status != 3): ?>
            	<?php //if ((strtotime($this->user->membership->membership_end_date) < strtotime(date("Y-m-d h:i:s", time())))):?>
            		<script>
						//window.location.assign("/membership");
		            </script>
            	<?php //endif; ?>
            	<?php //endif; ?>
            	<?php //endif; ?>
                
                    <div class="row-fluid">
                        <div class="col-xs-3 col-md-3" id="sidebar-container">
							<!-- Sidebar-->
							<aside id="sidebar">
								<nav>
										<?php 
										$this->widget('zii.widgets.CMenu',array(
											'items'=>array(
												array('label'=>'Home',  'url'=>array('/facebook/facebook/index'), 'linkOptions'=>array('class'=>'icon-menu home', 'title' => 'Dojo')),
												array('label'=>'Tabs',  'url'=>array('/facebook/fanPage/index'), 'linkOptions'=>array('class'=>'icon-menu tabs', 'title' => 'Tabs')),
												array('label'=>'Motion Post',  'url'=>array('/facebook/motionpost/index'), 'linkOptions'=>array('class'=>'icon-menu motion', 'title' => 'Motion Posts')),
												array('label'=>'Facebook Ads',  'url'=>array('/facebook/ads/index'), 'linkOptions'=>array('class'=>'icon-menu fb-ad', 'Facebook Ads')),
												array('label'=>'Analytics',  'url'=>array('/analytics/analytics/index'), 'linkOptions'=>array('class'=>'icon-menu analyt', 'title' => 'Analytics')),
											),
											
										));
										?>
								</nav>
							</aside>
							<!--/sidebar-->
						</div>					
					
					
                        <div class="col-xs-9 col-md-9">
							<?php //if ((strtotime($this->user->membership->membership_end_date) > strtotime(date("Y-m-d h:i:s", time())))): ?>
                            <?php echo $content; ?>  
                            <?php //endif; ?>
                        </div>
                    </div>
            </section>
            <?php endif; ?>
            <!--/content-->
            
        </div>    
        
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
        
        <script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
        <style type="text/css" media="screen, projection">
            @import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
        </style>
        <script type="text/javascript">
            if (typeof(Zenbox) !== "undefined") {
                Zenbox.init({
                    dropboxID: "20149885",
                    url: "https://tabfu.zendesk.com",
                    tabTooltip: "Help",
                    tabImageURL: "https://p4.zdassets.com/external/zenbox/images/tab_help_black_right.png",
                    tabColor: "#85dffc",
                    tabPosition: "Right"
                });
            }
        </script>
        
        <script type="text/javascript">
            function showmessage(element){
                id = jQuery(element).attr("data-message-id");
                jQuery(element).closest('ul').find('a').each(function(){
                   jQuery(this).removeClass('active'); 
                });
                jQuery(element).removeClass("new");
                jQuery(element).addClass('active');
                countmessages = jQuery(element).closest('.setting-messages').find('a.messages');
                if(jQuery(countmessages).find('span').text() == "1"){
                    jQuery(countmessages).removeClass("inbox");
                }else{
                    jQuery(countmessages).find('span').text(parseInt(jQuery(countmessages).find('span').text()) - 1);
                }
                jQuery.ajax({
                    type: "POST",
                    url: "/showmessage",
                    data: { id: id },
                    dataType: "json"
                  })
                    .done(function( data ) {
                      
                      jQuery('.message-dialog .message-scroll-content .mCSB_container').html(data.content);
                      jQuery('.message-dialog .right-column-tf .header-tf input').val(data.title+" ("+data.date+")");
                      jQuery('.message-dialog .message-scroll-content').mCustomScrollbar("update");
                    });
                
            }
            
            function moveToArchiveMessage(){
                element = jQuery('.setting-messages .messages-list .message-link.active');
                id = jQuery(element).attr('data-message-id');
                jQuery(element).parent('li').remove();  
                element = null;
                if(id != null){
                    jQuery.ajax({
                    type: "POST",
                    url: "/archivemessage",
                    data: { id: id },
                    dataType: "json"
                  }).done(function(data){
                      jQuery('.message-dialog .message-scroll-content .mCSB_container').html("Please select message");
                      jQuery('.message-dialog .right-column-tf .header-tf input').val("Please select message");
                      jQuery('.message-dialog .message-scroll-content').mCustomScrollbar("update");
                      jQuery('.setting-messages .messages-list .message-link.active').parent('li').fadeOut();
                  });
                }
            }
            
            function removeFromArchiveMessage(){
                element = jQuery('.setting-messages .messages-list .message-link.active');
                id = jQuery(element).attr('data-message-id');
                jQuery(element).parent('li').remove();  
                element = null;
                if(id != null){
                    jQuery.ajax({
                    type: "POST",
                    url: "/removefromarchivemessage",
                    data: { id: id },
                    dataType: "json"
                  }).done(function(data){
                      jQuery('.message-dialog .message-scroll-content .mCSB_container').html("Please select message");
                      jQuery('.message-dialog .right-column-tf .header-tf input').val("Please select message");
                      jQuery('.message-dialog .message-scroll-content').mCustomScrollbar("update");
                      jQuery('.setting-messages .messages-list .message-link.active').parent('li').fadeOut();
                  });
                }
            }
            function removeMessageConfirm(){
                displayFlashConfirm("Are you sure? <a href='#' onclick='removeMessage(); jQuery(this).closest(\".flash-container\").fadeOut();return false;'>Yes</a>");
                return false;
            }
            function removeMessage(){
                element = jQuery('.setting-messages .messages-list .message-link.active');
                id = jQuery(element).attr('data-message-id');
                jQuery(element).parent('li').remove();     
                element = null;
                if(id != null){
                    jQuery.ajax({
                        type: "POST",
                        url: "/removemessage",
                        data: { id: id },
                        dataType: "json"
                  }).done(function(data){
                      jQuery('.message-dialog .message-scroll-content .mCSB_container').html("Please select message");
                      jQuery('.message-dialog .right-column-tf .header-tf input').val("Please select message");
                      jQuery('.message-dialog .message-scroll-content').mCustomScrollbar("update");
                      
                      
                  });
                }   
            }
            
            function showArchive(element){
                if(jQuery(element).hasClass('view-inbox-tf')){
                    jQuery.ajax({
                        type: "POST",
                        url: "/showinbox"
                    }).done(function(data){
                        jQuery('.message-dialog .message-scroll-content .mCSB_container').html("Please select message");
                        jQuery('.message-dialog .right-column-tf .header-tf input').val("Please select message");
                        jQuery('.modal-content-tf > .left-column-tf > .header-tf > h3').text('Inbox');
                        jQuery(element).removeClass('view-inbox-tf');
                        jQuery(element).addClass('view-archive-tf');
                        jQuery('.setting-messages .message-dialog .left-column-tf .messages-list ul').html(data);
                        jQuery('.modal-content-tf > .right-column-tf > .footer-tf > .button-place > a.move-to-archive').attr('onclick', 'moveToArchiveMessage(); return false;');
                        jQuery('.modal-content-tf > .right-column-tf > .footer-tf > .button-place > a.move-to-archive').removeClass("ChangeBtn");
                        jQuery(".modal-content-tf > .left-column-tf > .content-tf ").mCustomScrollbar("update");
                    });
                }else{
                    jQuery.ajax({
                        type: "POST",
                        url: "/showarchive"
                    }).done(function(data){
                        jQuery('.message-dialog .message-scroll-content .mCSB_container').html("Please select message");
                        jQuery('.message-dialog .right-column-tf .header-tf input').val("Please select message");
                        jQuery('.modal-content-tf > .left-column-tf > .header-tf > h3').text('Archive');
                        jQuery(element).removeClass('view-archive-tf');
                        jQuery(element).addClass('view-inbox-tf');
                        jQuery('.setting-messages .message-dialog .left-column-tf .messages-list ul').html(data);
                        jQuery('.modal-content-tf > .right-column-tf > .footer-tf > .button-place > a.move-to-archive').addClass("ChangeBtn");
                        jQuery('.modal-content-tf > .right-column-tf > .footer-tf > .button-place > a.move-to-archive').attr('onclick', 'removeFromArchiveMessage(); return false;');
                        jQuery(".modal-content-tf > .left-column-tf > .content-tf ").mCustomScrollbar("update");
                    });
                }
                
            }
        </script>
    </body>
</html>

<html>
    <head>
        <title><?php echo Yii::app()->name;?> <?php echo $this->pageTitle; ?> || Administrator panel</title>
        
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
        
        <script src="//code.jquery.com/jquery-1.9.1.min.js"></script> 
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script> 
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/js/summernote/summernote.css';?>">
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/summernote/summernote.js"></script>
        
    </head>
    <body>
        <nav class="navbar navbar-default navbar-inverse" role="navigation">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">TabFu</a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <?php 
                    $this->widget('zii.widgets.CMenu',array(
                        'htmlOptions'=>array('class'=>'nav navbar-nav'),
                        'items'=>array(
                            array('label'=>'Main page',  'url'=>array('/admin/home/index'), 'linkOptions'=>array('class'=>'')),
                            //array('label'=>'Users',  'url'=>array('/admin/user/index'), 'linkOptions'=>array('class'=>'')),
                            array('label'=>'Message',  'url'=>array('/admin/message/index'), 'linkOptions'=>array('class'=>'')),
                            array('label'=>'Users',  'url'=>array('/admin/user/admin'), 'linkOptions'=>array('class'=>'')),
                            ),
                        ));
                    ?>
               
                
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {
                echo '<p class="bg-' . $key . '" style="padding: 20px; font-size: 20px; color: black; border-radius: 5px;">' . $message . "</p>\n";
            }
        ?>
        <div class="container">
            
            <?php echo $content; ?>
        </div>
        
    </body>
    
</html>
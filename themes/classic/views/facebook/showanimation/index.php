<html>
<head>
    <title>Please wait...</title>
    
    
    <meta property="og:title" content="<?php echo $motionpost->title; ?>" />
    
    <meta property="og:description" content="<?php echo $motionpost->description; ?>" />
    <meta property="og:caption" content="<?php echo $motionpost->caption; ?>"/>
    <meta property="og:site_name" content="<?php echo $motionpost->caption; ?>"/>
    
    <?php if($motionpost->type == MotionPost::TYPE_ANIMATION): ?>
        <?php if(floor(intval($animation->height)) > 10  && floor(intval($animation->width)) > 10):?>
            <?php $height = floor($animation->height); ?>
            <?php $width = floor($animation->width); ?>
        <?php else: ?>
            <?php $height = 253;$width=470;?>
        <?php endif;?>

        <meta property="og:image" content="<?php echo Yii::app()->getBaseUrl(true); ?>/animations/<?php echo $animation->file_drive; ?>" />

        <?php $ext = explode('.', $animation->file_drive); ?>
        <?php if(strtolower($ext[count($ext)-1]) == 'gif'):?>
            <meta property="og:video:type" content="application/x-shockwave-flash">
            <meta property="og:video:width" content="<?php echo $width; ?>">
            <meta property="og:video:height" content="<?php echo $height; ?>">
            <meta property="og:video" content="<?php echo Yii::app()->getBaseUrl(true); ?>/GIFPlayer/GIFPlayer.swf?url=<?php echo Yii::app()->getBaseUrl(true); ?>/animations/<?php echo $animation->file_drive; ?>&h=<?php echo $height;?>&w=<?php echo $width ?>">
        <?php endif; ?>
        
    <?php else: ?>
        <?php
            $og = OpenGraph::fetch($motionpost->movie_url);
            $movieUrl = $og->video;

        ?>
        <meta property="og:image" content="<?php echo MotionPost::getMovieThumbnail($movieUrl, $motionpost->movie_icon_id);?>" />
        <meta property="og:image:width" content="<?php echo MotionPost::getMovieThumbnailWidth($movieUrl, $motionpost->movie_icon_id);?>" />
        <meta property="og:image:height" content="<?php echo MotionPost::getMovieThumbnailHeight($movieUrl, $motionpost->movie_icon_id);?>" />
        <?php if(!empty($movieUrl)): ?>
            <meta property="og:type" content="video"/>
            <meta property="og:video" content="<?php echo $movieUrl; ?>">
            <meta property="og:video:width" content="505">
            <meta property="og:video:height" content="300">
        <?php endif; ?>
    <?php endif;?>
</head>
<body>
    <script>
        window.location = "<?php echo $motionpost->url;?>";
    </script>
</body>
</html>
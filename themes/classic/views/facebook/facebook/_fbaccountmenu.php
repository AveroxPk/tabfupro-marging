<div class="col-md-12 col-sm-12 col-lg-12 home-page" style="padding-bottom: 30px;">
    <div class="row home" style="margin-top: 24px;">
		<div class="col-sm-12 menu">
			<div class="col-md-6  col-lg-3 nopadding">
				<a href="<?php echo Yii::app()->createUrl('facebook/fanPage/index');?>" class="button-link qtip-to-show icon-fanpage" style="vertical-align: top;" alt="Edit templates and publish as Facebook Tabs"  title="Tabs">
					<div class="icon-container">
						<div class="top-area">
							<span class="icon icon-Tabs"></span>
							<span class="icon-name">Tabs</span>
						</div>
						<div class="bottom-area">
							<i class="icofont icofont-info-square"></i>
							<span>
								Edit templates and publish as Facebook Tabs
							</span>
						</div>
					</div>
				</a>
			</div>

			<div class="col-md-6 col-lg-3 nopadding">
				<a href="<?php echo Yii::app()->createUrl('facebook/motionpost/index');?>" class="button-link qtip-to-show icon-motionpost" style="vertical-align: top; " alt="Post + schedule status updates promoting your Tabs"  title="Motion Posts">
					<div class="icon-container">
						<div class="top-area">
							<span class="icon icon-motion"></span>
							<span class="icon-name">Motion Post</span>
						</div>
						<div class="bottom-area">
							<i class="icofont icofont-info-square"></i>
							<span>
								Post + schedule status updates promoting your Tabs
							</span>
						</div>
					</div>
				</a>
			</div>
			
			<div class="col-md-6  col-lg-3 nopadding">
				<a href="<?php echo Yii::app()->createUrl('facebook/ads/index');?>" class="button-link qtip-to-show" alt="Create + export Ads for your Facebook campaigns" style="vertical-align: top;"  title="Facebook Ads">
					<div class="icon-container">
						<div class="top-area">
							<span class="icon icon-fb-ad"></span>
							<span class="icon-name">Facebook Ads</span>
						</div>
						<div class="bottom-area">
							<i class="icofont icofont-info-square"></i>
							<span>
								Create + export Ads for your Facebook campaigns
							</span>
						</div>
					</div>
				</a>
			</div>

			<div class="col-md-6  col-lg-3 nopadding">
				<a href="<?php echo Yii::app()->createUrl('/analytics/analytics/index');?>" class="button-link qtip-to-show" style="vertical-align: top;" alt="View engagement + conversion data at a glance"  title="Analytics">
					<div class="icon-container">
						<div class="top-area">
							<span class="icon icon-analyt"></span>
							<span class="icon-name">Analytics</span>
						</div>
						<div class="bottom-area">
							<i class="icofont icofont-info-square"></i>
							<span>
								View engagement + conversion data at a glance
							</span>
						</div>
					</div>
				</a>
			</div>
<!--			
			<div class="col-xs-3 icon-container">
				<a href="<?php echo Yii::app()->createUrl('facebook/motionpost/index');?>" class="button-link qtip-to-show icon-motionpost" style="vertical-align: top; " alt="Post + schedule status updates promoting your Tabs"  title="Motion Posts">
				</a>
			</div>
			<div class="col-xs-3 icon-container">
				<a href="<?php echo Yii::app()->createUrl('facebook/ads/index');?>" class="button-link qtip-to-show" alt="Create + export Ads for your Facebook campaigns" style="vertical-align: top;"  title="Facebook Ads">
				</a>
			</div>
			<div class="col-xs-3 icon-container">
				<a href="<?php echo Yii::app()->createUrl('/analytics/analytics/index');?>" class="button-link qtip-to-show" style="vertical-align: top;" alt="View engagement + conversion data at a glance"  title="Analytics">
				</a>
			</div>
			<div style="clear: both"></div>
			<div class="qtip-placeholder" style="display: block; position: relative; width: 510px; overflow: hidden; height: 85px;"></div>
			
			-->
		</div>

    </div>
</div>
<script>
    function properQtipPos(){
        if($('.home .menu').width() < 513){
            $('.qtip-tabFu0 .qtip-tip').addClass('qtip-tip-resized');
            $('.qtip-tabFu1 .qtip-tip').addClass('qtip-tip-resized');
            $('.qtip-tabFu2 .qtip-tip').addClass('qtip-tip-resized');
            $('.qtip-tabFu3 .qtip-tip').addClass('qtip-tip-resized');
        }else{

            $('.qtip-tabFu0 .qtip-tip').removeClass('qtip-tip-resized');
            $('.qtip-tabFu1 .qtip-tip').removeClass('qtip-tip-resized');
            $('.qtip-tabFu2 .qtip-tip').removeClass('qtip-tip-resized');
            $('.qtip-tabFu3 .qtip-tip').removeClass('qtip-tip-resized');
        }
    }
    $(document).ready(function(){
            
        
            $(window).resize(function(){
              properQtipPos();
                
            });
            function animate(selector, index){
                jQuery(selector).eq(index).animate(
                    {
                        opacity: 1,
                        width: 125
                    }, 
                    {
                        duration: 500,
                        queue: true, 
                        specialEasing: {
                            width: "easeOutBounce",
                            height: "easeOutBounce"
                        },
                        done: function() {
                            animate(selector, (index+1));
                        }
                    }
                );
            }
            animate($('.button-link img'), 0);
            
            

        $('.qtip-to-show').each(function(index, data) {
            $(this).qtip({
                content: {
                    attr: 'alt'
                },
                style: { classes: 'qtip-tabFu'+index },
                position: {
                    my: 'top center', 
                    at: 'bottom center',
                    adjust: {
                        y: 110
                    },
                    target: $(this),
                    container : $('.qtip-placeholder')
                    
                },
                events: {
                    'show': function(event, api) { properQtipPos(); }
                }
                

               
            });
        });
    });
     

</script>

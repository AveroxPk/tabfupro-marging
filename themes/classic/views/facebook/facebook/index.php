<style>
	.container-fluid{
		background: url(../images/new-layout/bg.jpg) no-repeat;		
	}

</style>
<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->baseUrl . '/js/qTip/jquery.qtip.css')
        ->registerScriptFile(Yii::app()->baseUrl . '/js/qTip/jquery.qtip.js', CClientScript::POS_END)
        ->registerCoreScript('jquery.ui');
?>
<!--
<div class="row">
    <div class="col-md-12 topwrapper">
        <div class="row">
            <div class="col-md-3 ">
				<?php //if (!$fbAccount) { ?>
                <div class="addtbtn">
                    <a id="addfacebookaccount" href="#"  ></a>
                </div>
				<?php //} else { ?>
				<strong style="color: #fff; display: none;">Account Limit Reached</strong>
				<?php //} ?>
                
            </div>
            <div class="col-md-2">
                 <h1 class="margin0">The Dojo</h1>
            </div>
            <div class="col-md-7" >
                <div class="selected" ><span style="opacity:0; position: relative; left: 1000px;"></span></div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-5 sidestrip">
        <div class="motion-posts-editor"></div>
        <a id="create-new-post" style="display: none;">Create new post</a>
        <div class="facebookaccounts" style="display: none;">
            <?php
				/*
				// Moved this part into main.php file;
				
				$this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$accounts,
                'itemView'=>'_facebook_account',
                'template'=>'{items}{pager}',
                'viewData'=>array('module'=>$module),
                'emptyText'=>'  
                    <div class="fbaccount">
                        Please add facebook account
                    </div> '

                ));
				*/
            ?>
        </div>
        
    </div>
	
	-->
    <?php if(Yii::app()->user->getState('u_f_id')): ?>
        <?php $this->renderPartial('_fbaccountmenu', array('selectedaccount'=>$selectedaccount)); ?>
    <?php endif; ?>
    
    <div class="newaccount-modal warning-modal">
        <div class="newaccount-modal-inner warning-modal-inner">
            <p>
                You will need to log out of your current facebook profile first.<br/>
                Once you're logged out, please click the "Add Facebook Account" button once again.
                <br/>Are you sure?
            </p>
            <a class="button-tf-yes" href="#">Yes</a>
            <a class="button-tf-cancel" href="#">Cancel</a>
        </div>
    </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/motionpost.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.topwrapper .selected span').text($('.fbaccount.selected').find('.profileName').text().trim());
        $('.topwrapper .selected span').animate({
            left: 0,
            opacity: 1
        },{
            duration: 1300
        });
    });
    
</script>
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<div class="row">
    <div class="col-md-12 topwrapper">
        <div class="row">
            <div class="col-md-3 ">
                <h1 class="margin0">A problem occurred!</h1>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-7" >
            </div>
        </div>

    </div>
</div>
<div class="row">
    <script>
        function logoutFBTF() {
            jQuery.getScript('//connect.facebook.net/en_US/all.js', function(){
                FB.init({
                    appId: facebookAppId
                });
            });
            FB.logout(function(response) {
                window.location.href = '/logout';
            });
            return false;
        }
    </script>
    <?php
        if(Yii::app()->user->hasState('error')) {
            echo '<div class="flash-box col-md-4 col-xs-10 col-xs-offset-1" style="margin:20px 0 0 10px">';
            echo '<div class="flash-error">' . Yii::app()->user->getState('error') . "</div>";
            echo '</div>';
        }
    ?>
</div>
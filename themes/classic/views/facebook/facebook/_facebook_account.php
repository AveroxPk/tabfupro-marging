
<?php
	$model = $module->fbUser->getdecodedBody();
//	$modules = $module->fbUser;
?>
<div class="fbaccount  <?php echo $model['id']==$data->facebook_user_id? 'selected':''; ?> ">
<a class="disconnectaccount" href="#" onclick="displayFlashWarning('Are you sure you want to delete this account?<br/><a href=\'<?php echo $this->createUrl('/facebook/facebook/disconnect', array('token'=>$data->short_token)); ?>\'>Yes</a>')"><i class="icofont icofont-power"></i></a>

<?php 

	$pic = $module->getUserPic($data->facebook_user_id);
//	$pic = $module['picture']['data']['url'];

?>
    <a href="<?php echo $this->createUrl('/facebook/facebook/switchaccount', array('token'=>$data->short_token)); ?>">
        <p class="profileinfo">
            <?php if(isset($pic)): ?>
			
					<img src="<?php echo $pic; ?>" />
			
            <?php endif; ?>
			
            <span class="profileName"><?php echo $data->name." ".$data->surname?></span>
            <span class="pagecount"><?php echo Yii::t('app','{n} Page|{n} Pages',  $data->getCountFanpagesNotDeleted()); ?></span>
        </p>
<!-- 
        <p class="fanpagescount">
           <img src="<?php // echo Yii::app()->request->baseUrl; ?>/images/account-info-box.png">
        </p>
-->		
    </a>
</div> 
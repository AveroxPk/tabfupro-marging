<?php
try{
	$fanpageObj = Cacher::getFromCache('fanpage-' . $data->facebook_page_id, function() use($module, $data) {
		return $module->api('/'.$data->facebook_page_id);
	});
//    $pic = Cacher::getFromCache('fanpagepic-' . $data->facebook_page_id, function() use($module, $data) {
//       return $module->getFanpagePic($data->facebook_page_id);
//    });
	$pic = $module->getFanpagePic($data->facebook_page_id);
	
} catch (FacebookApiException $e) {
    Yii::app()->user->setFlash('notice', 'Please select facebook account.');
    $this->redirect(array('/facebook/facebook/index'));
}
$fanpage = $fanpageObj->getdecodedBody();
?>

<?php if(isset($noswitch)):?>
<a class="page-box" data-pageid="<?php echo $data->facebook_page_id; ?>" >
<?php else: ?>
    <a class="selectfanpage page-box" data-pageid="<?php echo $data->facebook_page_id; ?>" onclick='switchfanpage(<?php echo $data->facebook_page_id; ?>); jQuery(".topwrapper .selected").text(jQuery(this).text().trim()); return false;'>
<?php endif;?>
    <div class="image-placeholder"><img src="<?php echo $pic; ?>" style="width: 100%"/></div>
    <div class="page-box-title">
        <span><?php echo $fanpage['name'] ?></span>
    </div>
</a>
<?php 
Yii::app()->clientScript->registerScriptFile('/js/jquery.countdown.js');
?>



<div class="row tabfu-row">
    <div class="col-md-3 col-sm-5 sidestrip ">		
        <div class="page-boxes">
            <a href="#" onclick='switchfanpage("ALL"); jQuery(".topwrapper .selected").text("All fanpages"); return false;' class="page-box main">
                <div class="image-placeholder">
                    <img src="/images/motion-posts/user-page.png" />
                </div>
                <div class="page-box-title">
                    <span><?php echo $userfacebook->name." ".$userfacebook->surname?>'s Pages</span>
                </div>
            </a>

            
            <?php 
			
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$fanpages,
                'itemView'=>'_fanpage',
                'template'=>'{items}{summary}{pager}',
                'emptyText'=>false,
                'summaryText'=>'{start}-{end} of {count}',
                'pager'=>array(
                    'header'=>'',
                    'maxButtonCount'=>0
                ),
                'viewData'=>array(
                    'module'=>$module
                ),
            ));
			
			
			
            ?>

        </div>
    </div> 
    <div class="col-md-8 col-sm-4 col-lg-9 home-page">
		<div class="template-picker">
			<?php
			Yii::app()->getModule('template');

			echo CHtml::htmlButton('<i class="fa fa-facebook fa-size"></i> <span>|</span> Create New Ad',array(
//				'onclick' => 'tabWizard.newTab(getDefaultFanpage());',
				'class' => 'fb-btn-add create-ad',
				));
			?>
        </div>	
	
	
		<div class="row filter-area">
			<div class="col-md-12">
				<div class="margin0 filter">
                    <select name="categoryFilter" class="categories-select-box-filter">
						<option selected hidden>Filter Tabs</option>
                        <option value="ALL" >All</option>
                        <?php foreach($categoriesAds as $category): ?>
                        <option value="<?php echo $category->id; ?>" ><?php echo $category->name; ?></option>
                        <?php endforeach;?>
                    </select>
				</div>
			</div>
		</div>
		
	
        <div class="motion-posts-elements ads-elements tab-view">
		
            <?php $this->renderPartial('_ads', array('ads'=>$allAds)); ?>
		
                <?php //$this->renderPartial('_tabs', array('tabs'=>$allTabs)); ?>
        </div>
		
    </div>

 
</div>
<?php 
    $this->widget('application.modules.template.widgets.adWizard.AdWizard', compact('fanpages'));
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/clipboard/jquery.clipboard.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/faceads.js"></script>
<script>
    window.globalCountdown = null;
    
    /**
     * 
     * Mobile modal
     * 
     */
    
    jQuery(document).on('click', '.tab .mobile', function(e){
        that=this;
        e.preventDefault();
        if(!jQuery(that).hasClass('inactive')){
            //jQuery('#mobile-link-modal #copy-to-clipboard').removeClass('active');
            //jQuery('#mobile-link-modal').css('left', e.clientX + jQuery(window).scrollLeft());
            jQuery('#mobile-link-modal').css('top', e.clientY + jQuery(window).scrollTop());
            jQuery('#mobile-link-modal #link-holder').attr('value', jQuery(that).attr('href'));
            jQuery('#mobile-link-modal').fadeIn();
            jQuery('#mobile-link-modal #copy-to-clipboard').zclip(
                {
                    path: '<?php echo Yii::app()->request->baseUrl; ?>/js/clipboard/jquery.clipboard.swf',
                    copy:function(){
                        jQuery('#mobile-link-modal #copy-to-clipboard').addClass('active');
                        return jQuery('#mobile-link-modal #link-holder').attr('value');
                    },
                    beforeCopy:function(){
                        jQuery('#mobile-link-modal #copy-to-clipboard').addClass('active');
                    },
                    afterCopy:function(){
                        jQuery('#mobile-link-modal #copy-to-clipboard').addClass('active');
                        //jQuery('#mobile-link-modal #copy-to-clipboard').zclip('remove');

                    }
                }

            );
        }
    });
    
    jQuery('.filter .categories-select-box-filter').change(function(e){
        that = jQuery(this).children(":selected");
        valFilter = jQuery(that).val();
        valFilterText = jQuery(that).text();
        console.log(jQuery(that).val());
        jQuery('.displaying span').text(valFilterText);
        if(valFilter == 'ALL'){
            jQuery('.post.tab').each(function(){
                jQuery(this).fadeIn();
            });
        }else if(valFilter == 'SCHEDULED'){
            jQuery('.post.tab').each(function(){
                if(jQuery(this).attr('data-scheduled') !== 'true'){
                    jQuery(this).fadeOut();
                }else{
                    jQuery(this).fadeIn();
                }
            });
        }else if(valFilter == 'PUBLISHED'){
            
            jQuery('.post.tab').each(function(){
                if(jQuery(this).attr('data-published') !== 'true'){
                    jQuery(this).fadeOut();
                }else{
                    jQuery(this).fadeIn();
                }
            });
            
        }else if(valFilter == 'UNPUBLISHED'){
            jQuery('.post.tab').each(function(){
                if(jQuery(this).attr('data-published') == 'true' || jQuery(this).attr('data-scheduled') == 'true'){
                    jQuery(this).fadeOut();
                }else{
                    jQuery(this).fadeIn();
                }
            });
        }else{
            jQuery('.post.tab').each(function(){
                if(jQuery(this).attr('data-template-category-id') !== valFilter ){
                    jQuery(this).fadeOut();
                }else{
                    jQuery(this).fadeIn();
                }
            });
        }
        
    
    });
    </script>
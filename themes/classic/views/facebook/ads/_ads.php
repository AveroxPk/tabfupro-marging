
    <?php if(count($ads) > 0): ?>
        <?php foreach($ads as $ad):?>
        <?php $templateCategories = $ad->pages->ad->categories; ?>
        <?php 
            $categoryToShow = '';
            foreach($templateCategories as $templateCategory){
                $categoryToShow .= "data-template-category-id='{$templateCategory->id}'";
            }
        ?>
        
        <div class="post tab" <?= $categoryToShow ?> data-scheduled="true" data-id="<?= $ad->id ?>">
            <div class="top-information">
                <div class="left">
                    <div class="title">
                        <?php echo $ad->name; ?>
                    </div>
                    <div class="image ads-image-area">
                        <img src="<?php echo $ad->getTabIconUrl()!=null?$ad->getTabIconUrl(): Yii::app()->baseUrl.'/facead_resources/'.$ad->getTemplateId().'/preview.jpg'; ?>" alt="Thumb" />
                    </div>
                </div>
                <div class="right ads-links">
					<a href="#" data-id="<?php echo $ad->id; ?>" class="icofont icofont-favourite <?php echo $ad->favorite==1? 'active removefromfavoritetab':'addtofavoritetab'; ?>" title="Favorite"></a>
					<a href="#" data-id="<?php echo $ad->id; ?>" class="icofont icofont-ui-copy copy" title="Copy"></a>
                </div>
                <div class="clear"></div>
            </div>
<!--
            <a href="#" onclick="tabWizard.editTab(<?php echo $ad->id; ?>);return false;" class="button edit-tab">Edit Ad</a>
            <a href="#" <?php echo $ad->userFanpage->facebook_page_id ? 'page-id="'.$ad->userFanpage->facebook_page_id.'"' : ''; ?> data-id="<?php echo $ad->id; ?>" class="publish-now-button" title="Export">Export</a>
-->

			
			<div class="unpublished-element">
				<a href="#" class="button-delete" title="Delete">Delete</a>
				<a href="#" onclick="tabWizard.editTab(<?php echo $ad->id; ?>);return false;" class="refresh-icon" title="Refresh">Edit Ad</a>
				<a href="#" <?php echo $ad->userFanpage->facebook_page_id ? 'page-id="'.$ad->userFanpage->facebook_page_id.'"' : ''; ?> data-id="<?php echo $ad->id; ?>" class="button-export publish-now-button" title="Export">Export</a>
				
				<div class="delete-tab-before-schedule remove-modal">
					<span class="remove-modal-top">Are you sure you<br />want to delete<br />this Ad?</span>
					<a href="#" data-id="<?php echo $ad->id; ?>" class="button-tf-yes">Yes</a>
					<a href="#" class="button-tf-cancel">Cancel</a>
				</div>
										
			</div>
			
			

        </div>
        
        <?php endforeach; ?>
    <?php else: ?>

        You dont have any ads on this fanpage.

    <?php endif; ?>
        
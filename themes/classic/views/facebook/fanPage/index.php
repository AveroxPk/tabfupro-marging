<?php 

Yii::app()->clientScript->registerScriptFile('/js/jquery.countdown.js');
?>
<!--		
<div class="row">
    <div class="col-md-12 topwrapper">
        <div class="row">
            <div class="col-md-3 ">
                <div class="template-picker">
                    <?php 
					/*
                    Yii::app()->getModule('template');
					if (!$fbAccount) {
						echo CHtml::image(Yii::app()->baseUrl.'images/Create-a-New-Tab.png', 'Create Tab',  array(
							'onclick' => 'tabWizard.newTab(getDefaultFanpage());',
							'style'=>'cursor:pointer',
							));
					} else {
						//echo "<br /> FB Account: " . $fbAccount;
						echo "<strong style='color: #fff'>Tab Limit Reached . " . $tabCount . " " . $date_added . "</strong> ";
					}
					*/
                    ?>
                </div>
                
            </div>
			<!--
            <div class="col-md-3">
                 <h1 class="margin0">Facebook Tabs</h1>
            </div>
            <div class="col-md-2">   
                <div class="margin0 selected">All fanpages</div> 
            </div>
			
			-->
			<!--
            <div class="col-md-2">
                <div class="margin0 displaying">Filter: <span>All</span></div>
            </div>
            <div class="col-md-2">
                <div class="margin0 filter">
                    <select name="categoryFilter" class="categories-select-box-filter">
                        <option value="" disabled></option>
                        <option value="ALL" >All</option>
                        <option value="PUBLISHED">Published</option>
                        <option value="UNPUBLISHED">Unpublished</option>
                        <option value="SCHEDULED">Scheduled</option>
                        <?php 
							/*
						foreach($templateCategories as $category): ?>
                        <option value="<?php echo $category->id; ?>" ><?php echo $category->name; ?></option>
                        <?php endforeach;
						*/
						?>
                    </select>
                </div>
            </div>
        </div>

    </div>
</div>
-->
<div class="row tabfu-row">
    <div class="col-md-3 col-sm-5 sidestrip ">		
        <div class="page-boxes">
            <a href="#" onclick='switchfanpage("ALL"); jQuery(".topwrapper .selected").text("All fanpages"); return false;' class="page-box main">
                <div class="image-placeholder">
                    <img src="/images/motion-posts/user-page.png" />
                </div>
                <div class="page-box-title">
                    <span><?php echo $userfacebook->name." ".$userfacebook->surname?>'s Pages</span>
                </div>
            </a>

            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$fanpages,
                'itemView'=>'_fanpage',
                'template'=>'{items}{summary}{pager}',
                'emptyText'=>false,
                'summaryText'=>'{start}-{end} of {count}',
                'pager'=>array(
                    'header'=>'',
                    'maxButtonCount'=>0
                ),
                'viewData'=>array(
                    'module'=>$module
                ),
            )); ?>
        </div>
    </div> 
    
    <div class="col-md-8 col-sm-4 col-lg-9 home-page">
		<div class="template-picker">
			<?php
			Yii::app()->getModule('template');
			if (!$fbAccount) {
				echo CHtml::htmlButton('<i class="fa fa-facebook fa-size"></i> <span>|</span> Create New Tab',array(
					'onclick' => 'tabWizard.newTab(getDefaultFanpage());',
					'class' => 'fb-btn-add addfacebookaccount',
					));
			} else {
				//echo "<br /> FB Account: " . $fbAccount;
				echo "<strong style='color: #fff'>Tab Limit Reached . " . $tabCount . " " . $date_added . "</strong> ";
			}
			?>
        </div>	
	
	
		<div class="row filter-area">
			<div class="col-md-12">
				<div class="margin0 filter">
					<select name="categoryFilter" class="categories-select-box-filter">
						<option selected hidden>Filter Tabs</option>
						<option value="ALL" >All</option>
						<option value="PUBLISHED">Published</option>
						<option value="UNPUBLISHED">Unpublished</option>
						<option value="SCHEDULED">Scheduled</option>
						<?php foreach($templateCategories as $category): ?>
						<option value="<?php echo $category->id; ?>" ><?php echo $category->name; ?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
		</div>
		
	
        <div class="motion-posts-elements tab-view">
		
		
                <?php $this->renderPartial('_tabs', array('tabs'=>$allTabs)); ?>
        </div>
    </div>
    <div id="mobile-link-modal" class="mobile-link-modal">
        <div class="header">
            <p>Mobile Friendly Tab Link:</p>
            <a href="#" class="close-button" title="Close"><i class="icofont icofont-power"></i></a>
        </div>
        <div class="content">
            <div class="link-content">
                <div class="left">
                    <input type="text" id="link-holder" name="link-holder" value="shortened link here" />
                </div>

                <div class="right">
                    <a href="#" class="copy-to-clipboard" id="copy-to-clipboard" alt="Copy">Copy</a>
                </div>

            </div>
            <a href="#" class="close-window-button">Close window</a>
        </div>
    </div>
    
    
    <!-- Publish your tab -->
    <div id="publish-main-modal" class="publish main-modal" style="display: none;">
            <div class="header">
                <p>Publish your Tab</p>
                <a href="#" class="close-button" title="Close"><i class="icofont icofont-power"></i></a>
            </div>
            <div class="content">
                
                <div class="publish-list">
                    <div class="header">
                        Publishing to:
                    </div>
                    <div class="content">
                        
                        <!-- Publishing -->
                        <div class="publishing">
                            <div class="page-boxes">
                                    <?php 
                                    $this->widget('zii.widgets.CListView', array(
                                        'dataProvider'=>$fanpages,
                                        'itemView'=>'_fanpage',
                                        'template' => '{pager}{summary}{items}{summary}{pager}',
                                        'emptyText' => false,
                                        'summaryText'=>'{start}-{end} of {count}',
                                        'pager'=>array(
                                            'header'=>'',
                                            'maxButtonCount'=>0
                                        ),
                                        'viewData'=>array(
                                            'module'=>$module,
                                            'noswitch'=>true,
                                        ),
                                    ));
                                    ?>
                            </div>
                        </div>
                        
                        

                        <div class="published"  style="display: none;">
                            <p>Published<br /><img src="/images/facebook-tabs/published-tick.png" alt="Published" /></p>
                            
                        </div>
                    </div>
                </div>
                
                   
                
                <a href="#" class="close-window-button custom-btn selected move-right text-center" style="display: none; margin-top: 0"><i class="fa fa-compress" aria-hidden="true"></i> Close window</a>
                <a href="#" id="publish-modal-publish" class="ok-window-button custom-btn small selected move-right text-center"><i class="fa fa-check" aria-hidden="true"></i> Ok</a> <?php //Ok button for modal shown after "PUBLISH NOW" button trigger in general Facebook tabs view ?>
                <a href="#" id="publish-modal-select" class="ok-window-button custom-btn small selected move-right text-center"><i class="fa fa-check" aria-hidden="true"></i> Ok</a> <?php //Ok button for modal shown after "CHOOSE OTHER PAGE" button click ?>
                </div>
            </div>
        
        <!--/publish-->
        <!-- Schedule pop up -->
        <div id="schedule-main-modal" class="schedule main-modal" style="display: none;">
            <div class="header">
                <p>Choose a Date + Time</p>
                <a href="#" class="close-button" title="Close"><i class="icofont icofont-power"></i></a>
            </div>
            <div class="content">
                
                <div id="calendar-widget" class="calendar-widget"></div>
                <div id="time-widget" class="time-widget">
                    <input type="text" id="time-picker" class="time-picker" />
                </div>
                
                <a href="#" class="ok-window-button">Ok</a>
                
            </div>
        </div>
        <!-- /schedule -->
        </div>
    </div>
</div>
<?php 
    $this->widget('application.modules.template.widgets.tabWizard.TabWizard', compact('fanpages'));
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/clipboard/jquery.clipboard.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/tabs.js"></script>
<script>
    window.globalCountdown = null;
    
    /**
     * 
     * Mobile modal
     * 
     */
    
    jQuery(document).on('click', '.tab .mobile', function(e){
        that=this;
        e.preventDefault();
        if(!jQuery(that).hasClass('inactive')){
            //jQuery('#mobile-link-modal #copy-to-clipboard').removeClass('active');
            //jQuery('#mobile-link-modal').css('left', e.clientX + jQuery(window).scrollLeft());
            jQuery('#mobile-link-modal').css('top', e.clientY + jQuery(window).scrollTop());
            jQuery('#mobile-link-modal #link-holder').attr('value', jQuery(that).attr('href'));
            jQuery('#mobile-link-modal').fadeIn();
            jQuery('#mobile-link-modal #copy-to-clipboard').zclip(
                {
                    path: '<?php echo Yii::app()->request->baseUrl; ?>/js/clipboard/jquery.clipboard.swf',
                    copy:function(){
                        jQuery('#mobile-link-modal #copy-to-clipboard').addClass('active');
                        return jQuery('#mobile-link-modal #link-holder').attr('value');
                    },
                    beforeCopy:function(){
                        jQuery('#mobile-link-modal #copy-to-clipboard').addClass('active');
                    },
                    afterCopy:function(){
                        jQuery('#mobile-link-modal #copy-to-clipboard').addClass('active');
                        //jQuery('#mobile-link-modal #copy-to-clipboard').zclip('remove');

                    }
                }

            );
        }
    });
    
    jQuery('.filter .categories-select-box-filter').change(function(e){
        that = jQuery(this).children(":selected");
        valFilter = jQuery(that).val();
        valFilterText = jQuery(that).text();
        console.log(jQuery(that).val());
        jQuery('.displaying span').text(valFilterText);
        if(valFilter == 'ALL'){
            jQuery('.post.tab').each(function(){
                jQuery(this).fadeIn();
            });
        }else if(valFilter == 'SCHEDULED'){
            jQuery('.post.tab').each(function(){
                if(jQuery(this).attr('data-scheduled') !== 'true'){
                    jQuery(this).fadeOut();
                }else{
                    jQuery(this).fadeIn();
                }
            });
        }else if(valFilter == 'PUBLISHED'){
            
            jQuery('.post.tab').each(function(){
                if(jQuery(this).attr('data-published') !== 'true'){
                    jQuery(this).fadeOut();
                }else{
                    jQuery(this).fadeIn();
                }
            });
            
        }else if(valFilter == 'UNPUBLISHED'){
            jQuery('.post.tab').each(function(){
                if(jQuery(this).attr('data-published') == 'true' || jQuery(this).attr('data-scheduled') == 'true'){
                    jQuery(this).fadeOut();
                }else{
                    jQuery(this).fadeIn();
                }
            });
        }else{
            jQuery('.post.tab').each(function(){
                if(jQuery(this).attr('data-template-category-id') !== valFilter ){
                    jQuery(this).fadeOut();
                }else{
                    jQuery(this).fadeIn();
                }
            });
        }
        
    
    });
    </script>
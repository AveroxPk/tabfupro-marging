
    <?php if(count($tabs) > 0): ?>
        <?php foreach($tabs as $tab):?>
        <?php $templateCategories = $tab->pages->getRelated('template')->getCategoriesList(); ?>
        <?php 
            $categoryToShow = '';
            foreach($templateCategories as $templateCategory){
                $categoryToShow .= "data-template-category-id='{$templateCategory->id}'";
            }
			
			$analytics = $tab->getAnalytics()->getSummary('all-time');
        ?>
        <!-- PUBLISHED -->                               
        <?php if($tab->published==1): ?>
            <div class="post tab" <?= $categoryToShow ?> data-published="true" data-id="<?= $tab->id ?>">
                <div class="top-information">
                    <div class="left">
                        <div class="title">
                            <?php echo $tab->name; ?>
                        </div>
                        <div class="image">
                            <img src="<?php echo $tab->getTabIconUrl()!=null?$tab->getTabIconUrl(): Yii::app()->baseUrl.'/template_resources/'.$tab->getTemplateId().'/preview.jpg'; ?>" alt="Thumb" />
                        </div>
					</div>

					<div class="right">
						<a href="#" data-id="<?php echo $tab->id; ?>" class="icofont icofont-favourite <?php echo $tab->favorite==1? 'active removefromfavoritetab':'addtofavoritetab'; ?>" title="Favorite"></a>
						<a href="https://www.facebook.com/<?php echo $tab->userFanpage->facebook_page_id.'?sk=app_'.$tab->application_id;?>" target="_blank" class="fa fa-facebook-official" title="View on Facebook"></a>
						<a href="<?php echo Yii::app()->createAbsoluteUrl('/facebook/showtab/mobile/tabid/' . $tab->id) ?>" target="_blank" class="icofont icofont-link-alt" title="Mobile"></a>
						<a href="#" data-id="<?php echo $tab->id; ?>" class="icofont icofont-ui-copy copy" title="Copy"></a>
						<a href="#" onclick="tabWizard.editTab(<?php echo $tab->id; ?>);return false;" class="icofont icofont-ui-edit" title="Edit"></a>
                        <a href="#" data-id="<?php echo $tab->id; ?>" class="unpublish icofont icofont-ui-power" title="unpublish"></a>
					</div>
					<div class="clear"></div>
					
					
                </div>

					<div class="status-area-container">
						<div class="status-area custom">
							<i class="fa fa-area-chart status-logo" aria-hidden="true"></i>
							<span>
								Analytics<br>
								<a href="#">+More Data</a>
							</span>						
						</div>
						
						<div class="analytics-fb">
							<div class="row"  title="Impressions count">
								<div class="fa fa-eye analytic-logo impressions" aria-hidden="true"></div>
								<div class="fa fa-mouse-pointer analytic-logo clicks" aria-hidden="true"></div>
								<div class="fa fa-share-alt analytic-logo shares" aria-hidden="true"></div>
								<div class="fa fa-envelope analytic-logo emails" aria-hidden="true"></div>
							</div>
							<div class="row" title="Clicks count">
								<div class="background-box impressions-analytics"><?= $analytics->getImpressions() ?></div>
								<div class="background-box clicks-analytics"><?= $analytics->getClicks() ?></div>
								<div class="background-box shares-analytics"><?= $analytics->getShares() ?></div>
								<div class="background-box emails-analytics"><?= $analytics->getEmails() ?></div>
							</div>						
						</div>						
					</div>
					
					<div class="unpublished-element">
						<a href="#" class="button-delete" title="Delete">Delete</a>
						<a href="#" class="refresh-icon" title="Refresh">Refresh</a>
						<a href="<?php echo Yii::app()->createUrl('/facebook/fanPage/exportCSV', array('tabId'=>$tab->id)); ?>" data-id="<?php echo $tab->id; ?>" class="button-export" title="Export Emails as CSV File">
						
						<span>Download <br>CSV</span>
						</a>
						
						<div class="delete-tab-scheduled remove-modal">
							<span class="remove-modal-top">Are you sure you<br />want to Delete<br />this Tab?</span>
							<a href="#" data-id="<?php echo $tab->id; ?>" class="button-tf-yes">Yes</a>
							<a href="#" class="button-tf-cancel">Cancel</a>
							<span class="remove-modal-bottom">Note: Tab will NOT <br />be deleted from <br />Facebook</span>
						</div>
												
					</div>

            </div>
        
        
        <?php else: ?>
        <!--NOT PUBLISHED -->
        <!-- Scheduled -->
        <?php if($tab->schedule !=null): ?>
            <div class="post tab" <?= $categoryToShow ?> data-scheduled="true" data-id="<?= $tab->id ?>">
                    <div class="top-information">
                        <div class="left">
                            <div class="title">
                                <?php echo $tab->name; ?>
                            </div>
                            <div class="image">
                                <img src="<?php echo $tab->getTabIconUrl()!=null?$tab->getTabIconUrl(): Yii::app()->baseUrl.'/template_resources/'.$tab->getTemplateId().'/preview.jpg'; ?>" alt="Thumb" />
                            </div>

                        </div>
                        <div class="right">
                            <a href="#" data-id="<?php echo $tab->id; ?>" class="icofont icofont-favourite <?php echo $tab->favorite==1? 'active removefromfavoritetab':'addtofavoritetab'; ?>" title="Favorite"></a>
                            <a href="#" class="fa fa-facebook-official preview-on-fb-big" title="Preview on Facebook" target="_blank"></a>
                            <a href="#" class="icofont icofont-link-alt disable" title="Mobile"></a>
                            <a href="#" data-id="<?php echo $tab->id; ?>" class="icofont icofont-ui-copy copy" title="Copy"></a>
                            <a href="#" onclick="tabWizard.editTab(<?php echo $tab->id; ?>);return false;" class="icofont icofont-ui-edit" title="Edit"></a>
                        </div>
                        <div class="clear"></div>
                    </div>
					<div class="status-area-container">
						<div class="status-area">
							<i class="fa fa-circle-o status-logo"></i>
							<span>
								Status<br>
								<b>Schedule</b>
							</span>						
						</div>
						
						<div class="scheduled-element">
							<div class="date-scheduled">
								<i class="fa fa-calendar-minus-o" aria-hidden="true"></i>
								<span><?php echo date('d/m/Y', strtotime($tab->schedule)); ?></span>
							</div>
							<div class="time-scheduled">
								<i class="fa fa-clock-o" aria-hidden="true"></i>
								<span><?php echo date('H:i', strtotime($tab->schedule)); ?> <?php echo " UTC"; echo (date('Z')/3600)<0 ? (date('Z')/3600):"+".(date('Z')/3600); ?></span>
							</div>
						</div>
					</div>

					<div class="unpublished-element">
						<a href="#" class="button-delete" title="Delete">Delete</a>
						<a href="#" data-id="<?php echo $tab->id; ?>" class="schedule-button" title="Schedule">Schedule</a>
						<a href="#" <?php echo $tab->userFanpage->facebook_page_id ? 'page-id="'.$tab->userFanpage->facebook_page_id.'"' : ''; ?>  data-id="<?php echo $tab->id; ?>" class="publish-now-button" title="Publish now">Publish now</a>
						
						<div class="delete-tab-before-schedule remove-modal">
							<span class="remove-modal-top">Are you sure you<br />want to Delete<br />this Tab?</span>
							<a href="#" data-id="<?php echo $tab->id; ?>" class="button-tf-yes">Yes</a>
							<a href="#" class="button-tf-cancel">Cancel</a>
							<span class="remove-modal-bottom">*Note: Schedule will <br />be cancelled and Tab <br />will NOT publish to <br />Facebook.</span>
						</div>
						
					</div>
					
					

                </div>
            <?php else: ?>
                <div <?= $categoryToShow ?> class="post tab">
                    <div class="top-information">
                        <div class="left">
                            <div class="title">
                                <?php echo $tab->name; ?>
                            </div>
                            <div class="image">
                                <img src="<?php echo $tab->getTabIconUrl()!=null?$tab->getTabIconUrl(): Yii::app()->baseUrl.'/template_resources/'.$tab->getTemplateId().'/preview.jpg'; ?>" alt="Thumb" />
                            </div>
<!--
                            <a href="#" data-id="<?php echo $tab->id; ?>" class="button unpublic inactive">Unpublish</a>
                            <a href="#" onclick="tabWizard.editTab(<?php echo $tab->id; ?>);return false;" class="button edit-tab">Edit tab</a>
-->
                        </div>
                        <div class="right">
                            <a href="#" data-id="<?php echo $tab->id; ?>" class="icofont icofont-favourite <?php echo $tab->favorite==1? 'active removefromfavoritetab':'addtofavoritetab'; ?>" title="Favorite"></a>
                            <a href="#" class="fa fa-facebook-official preview-on-fb-big" title="Preview on Facebook" target="_blank"></a>
                            <a href="#" class="icofont icofont-link-alt disable" title="Mobile"></a>
                            <a href="#" data-id="<?php echo $tab->id; ?>" class="icofont icofont-ui-copy copy" title="Copy"></a>
                            <a href="#" onclick="tabWizard.editTab(<?php echo $tab->id; ?>);return false;" class="icofont icofont-ui-edit" title="Edit"></a>
                        </div>
                        <div class="clear"></div>
                    </div>
					<div class="status-area-container">
						<div class="status-area">
							<i class="fa fa-circle-o status-logo"></i>
							<span>
								Status<br>
								<b>Unpublished</b>
							</span>
							
						</div>
					</div>
					<div class="unpublished-element">
						<a href="#" class="button-delete" title="Delete">Delete</a>
						<a href="#" data-id="<?php echo $tab->id; ?>" class="schedule-button" title="Schedule">Schedule</a>
						<a href="#" <?php echo $tab->userFanpage->facebook_page_id ? 'page-id="'.$tab->userFanpage->facebook_page_id.'"' : ''; ?>  data-id="<?php echo $tab->id; ?>" class="publish-now-button" title="Publish now">Publish now</a>
						
                        <div class="delete-tab remove-modal">
                            <span class="remove-modal-top">Are you sure you<br />want to Delete<br />this Tab?</span>
                            <a href="#" data-id="<?php echo $tab->id; ?>" class="button-tf-yes">Yes</a>
                            <a href="#" class="button-tf-cancel">Cancel</a>
                        </div>
					</div>
                    <!-- <div class="delete-modal scheduled"> <not ready>
                        <a href="#" class="button-tf-yes">Yes</a>
                        <a href="#" class="button-tf-cancel">Cancel</a>
                    </div> !-->

                </div>
            <?php endif; ?>
        <?php endif; ?>
        
        <?php endforeach; ?>
    <?php else: ?>

        <span class="info">You dont have any tabs on this fanpage. </span>

    <?php endif; ?>
        
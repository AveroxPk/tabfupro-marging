<div id="motion-post" class="tabbed-modal">
	<a href="#" id="mp-close" onclick="closeModal(); return false;" title="Close">
		<i class="icofont icofont-power"></i>
	</a>
    <!-- Publish your tab -->
    <div id="select-fanpage-main-modal" class="publish main-modal" style="display: none;">
        <div class="header">
            <p>Select fanpage</p>
            <a href="#" class="close-button" title="Close"><i class="icofont icofont-power"></i></a>
        </div>
        <div class="content">

            <div class="publish-list">
                <div class="header">
                    Fanpage:
                </div>
                <div class="content">

                    <!-- Publishing -->
                    <div class="publishing">
                        <div class="page-boxes">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $fanpages,
                                'itemView' => 'partial/_fanpage',
                                'template' => '{items}',

                                'viewData' => array(
                                    'module' => $module,
                                    'noswitch' => true,
                                ),
                            ));
                            ?>
                        </div>
                    </div>


                    <div class="published" style="display: none;">
                        <p>Published<br/><img src="/images/facebook-tabs/published-tick.png" alt="Published"/></p>

                    </div>
                </div>
            </div>


            <a href="#" class="close-window-button" style="display: none;">Close window</a>
            <a href="#" class="ok-window-button custom-btn small selected text-center move-right">Ok</a>
        </div>
    </div>

    <!--/publish-->
    <div id="motion-post-header" class="col-md-2 tabbed-modal-header">


        <div id="mp-nav" class="tabbed-modal-nav">
			<div class="steps active">
				<a href="#mp-tab-1" id="mp-tab-link" class="mp-nav-link" title="Tab Link"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps">
				<a href="#mp-tab-2" id="mp-animation" class="mp-nav-link icofont icofont-video-clapper" title="Media"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>

			<div class="steps">
				<a href="#mp-tab-3" id="mp-message" class="mp-nav-link icofont icofont-comment" title="Message"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps">
				<a href="#mp-tab-4" id="mp-preview" class="mp-nav-link icofont icofont-open-eye" title="Preview"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps">
				<a href="#mp-tab-5" id="mp-posted" class="mp-nav-link fa fa-check" title="Posted"></a>
			</div>
        </div>
    </div>

    <div id="mp-content" class="col-md-10 tabbed-modal-content">
        <!-- Tab link -->
        <div class="tab active" id="mp-tab-1">
            <div class="mp-tab-header">
                <h1>Choose a Tab to Promote</h1>

                <h2>Select a tab to link your post to or paste in any external URL of your choice</h2>
            </div>
            <div class="mp-tab-content">
                <div class="mp-search-link-place">

                    <div class="search-element">
                        <input type="text" name="search-tabs" id="mp-search-tabs" placeholder="Search Tabs"/>
						<span class="fa fa-search"></span>
                    </div>

                    <div class="add-own-link">
                        <div class="left">
                            <span class="icofont icofont-link-alt"></span>
                            <span class="title">Add Your Own Link</span>
                        </div>
                        <div class="right ">
                            <input
                                class="<?php echo (isset($motionpost["error_url"]) && $motionpost["error_url"] == "true") ? 'error' : ''; ?>"
                                type="text" name="url-holder" placeholder="Copy and paste your URL here"/>
                        </div>
                        <div class="tick" style="display:none;"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list-tabs">
                        <div class="header">
                            <span class="one">Displaying Tabs for:</span>
                            <span class="two"><strong>All Pages</strong></span>

                            <div name="other-page" class="tabs-select-box editor-post-select-fanpage"><i class="icofont icofont-eye-alt"></i> Choose Other Page</div>
                            <span class="three">or</span>
							
                            <a href="#" class="show-all-button"><i class="icofont icofont-eye-alt"></i> Show All</a>
                            <i class="ticked active"></i>
                        </div>
                        <div
                            class="content <?php echo (isset($motionpost["error_motion_post"]) && $motionpost["error_motion_post"] == "true") ? 'error' : ''; ?>">
                            <?php
                            if (count($fanpage) == 0) {
                                $emptyText = "You dont have any fanpages. Please create some on facebook and come back later.";
                            } else {
                                $emptyText = "You dont have any tabs to promote. You can still pass url above though.";
                            }
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $tabs,
                                'emptyText' => $emptyText,
                                'itemView' => 'partial/_tabs',
                                'template' => '{items}',
                            ));
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="mp-footer" class="tabbed-modal-footer">
            <div id="mp-footer-left-side">
                <a href="#" class="cancel button" id="mp-cancel-button" data-open="0"><i class="fa fa-times" aria-hidden="true"></i> Cancel</a>
            </div>

            <div id="mp-footer-middle">

            </div>

            <div id="mp-footer-right-side">
                <a href="#" class="next button" id="mp-next-button" data-open="2">Next <i class="icofont icofont-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


    motionpostObj = <?php echo json_encode($motionpost);?>;
    //INIT
    function initialize() {
        if (motionpostObj.tab_id != null) {
            jQuery('#mp-content .select-tab-for-post[data-tab-id="' + motionpostObj.tab_id + '"]').click();
        }
        if (motionpostObj.url != null) {
            jQuery('#mp-content input[name="url-holder"]').val(motionpostObj.url);
        }
        if (jQuery("#mp-tab-1 .list-tabs > div.content").is(":visible")) {
            jQuery("#mp-tab-1 .list-tabs > div.content").mCustomScrollbar({
                mouseWheel: true,
                alwaysShowScrollbar: 1,
                autoDraggerLength: true,
                advanced: {
                    updateOnBrowserResize: true,
                    updateOnContentResize: true
                }
            });
        } else {
            setTimeout(function () {
                initialize();
            }, 500);
        }

    }
    initialize();

    //Close main modal
    function closeModal() {
        closemodal(jQuery('.tabbed-modal'));
    }


    //Display publish-to modal
    jQuery(document).on('click', '.editor-post-select-fanpage', function (e) {
        e.preventDefault();
        jQuery('#select-fanpage-main-modal').fadeIn();
        displaymodal(jQuery('#select-fanpage-main-modal'));
        if (!jQuery("#select-fanpage-main-modal  .content  .publish-list  .content").hasClass("mCustomScrollbar")) {
            jQuery("#select-fanpage-main-modal  .content  .publish-list  .content").mCustomScrollbar({
                mouseWheel: true,
                alwaysShowScrollbar: 1,
                autoDraggerLength: true,
                advanced: {
                    updateOnBrowserResize: true,
                    updateOnContentResize: true
                }
            });
        } else {
            jQuery("#select-fanpage-main-modal  .content  .publish-list  .content").mCustomScrollbar("update");
        }

    });
    //close publish to modal
    jQuery(document).on('click', '#select-fanpage-main-modal .ok-window-button', function (e) {
        e.preventDefault();
        closemodal(jQuery('#select-fanpage-main-modal'));
        jQuery("#mp-tab-1 .list-tabs > div.content").mCustomScrollbar("update");

    });
    jQuery(document).on('click', '#select-fanpage-main-modal .close-button', function (e) {
        e.preventDefault();
        closemodal(jQuery('#select-fanpage-main-modal'));
        jQuery("#mp-tab-1 .list-tabs > div.content").mCustomScrollbar("update");

    });


    //Select fanpage from modal 
    jQuery(document).on('click', '#select-fanpage-main-modal .page-box', function (e) {
        that = this;
        jQuery('#mp-content .list-tabs .header .two strong').text(jQuery(that).children('.page-box-title').text());
        e.preventDefault();
        jQuery(that).addClass('active');
        jQuery('#mp-content .list-tabs .header .ticked').removeClass("active");
        jQuery('#select-fanpage-main-modal .page-box').each(function () {
            if (jQuery(this).attr('data-page-id') != jQuery(that).attr('data-page-id')) {
                jQuery(this).removeClass('active');
            }
        });

        jQuery('.motion-posts-editor .select-tab-for-post').each(function () {

            if (jQuery(this).attr('data-page-id') != jQuery(that).attr('data-page-id')) {
                jQuery(this).fadeOut();
            } else {
                jQuery(this).fadeIn();
            }
        });


    });

    //Show all tabs
    jQuery(document).on('click', '.show-all-button', function (e) {
        e.preventDefault();
        jQuery('#mp-content .list-tabs .header .two strong').text("All Pages");
        jQuery('#mp-content .list-tabs .header .ticked').addClass("active");
        jQuery('.motion-posts-editor .select-tab-for-post').each(function () {
            jQuery(this).fadeIn();

        });
        jQuery("#mp-tab-1 .list-tabs > div.content").mCustomScrollbar("update");
    });


    //Select tab
    jQuery(document).on('click', '#mp-content .select-tab-for-post', function (e) {
        if (!jQuery(e.target).hasClass('view-on-facebook')) {
            that = this;
            e.preventDefault();
            //jQuery('#mp-content input[name="url-holder"]').val(jQuery(this).children('a').attr('href'));
            jQuery(that).addClass('active');
            motionpostObj.tab_id = jQuery(that).attr('data-tab-id');
            motionpostObj.url = null;
            jQuery('#mp-content input[name="url-holder"]').val('');
            jQuery('.mp-search-link-place .add-own-link .tick').css('background', 'none');

            jQuery('#mp-content .select-tab-for-post').each(function () {
                if (jQuery(this).attr('data-tab-id') != jQuery(that).attr('data-tab-id')) {
                    jQuery(this).removeClass('active');
                }
            });
        }
    });

    jQuery(document).on('keyup', '.mp-search-link-place input[name=url-holder]', function () {
        var theURL = jQuery(this).val();
        // validate the URL
        if (/^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(theURL)) {
            motionpostObj.url = theURL;
            motionpostObj.tab_id = null;

            jQuery('.mp-search-link-place .add-own-link .tick').css('background', 'url(../images/step-3/when-choice-tick.png) no-repeat 0 0');
            jQuery('.list-tabs .tabs-element').removeClass('active');
        }
        else {
            jQuery('.mp-search-link-place .add-own-link .tick').css('background', 'none');
            motionpostObj.url = null;
        }
    });


    //Search tabs
    jQuery('input[name="search-tabs"]').keyup(function (e) {
        that = this;
        jQuery('#mp-content .select-tab-for-post').show();
        if (jQuery(that).val() != '') {
            jQuery('#mp-content .select-tab-for-post').each(function () {
                if (jQuery(this).find('.title').text().toLowerCase().indexOf(jQuery(that).val().toLowerCase()) < 0) {
                    jQuery(this).hide();
                }
            });
        }
        else {
            jQuery('.favorites-box .fav-element').each(function () {
                jQuery(this).show();
            });
        }
    });

</script>
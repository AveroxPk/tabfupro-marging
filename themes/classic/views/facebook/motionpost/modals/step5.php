<div id="motion-post" class="tabbed-modal">
	<a href="#" id="mp-close" onclick="closeModal(); return false;" title="Close">
		<i class="icofont icofont-power"></i>
	</a>
	
    <div id="motion-post-header" class="col-md-2 tabbed-modal-header">
        <div id="mp-nav" class="tabbed-modal-nav">
			<div class="steps active">
				<a href="#mp-tab-1" id="mp-tab-link" class="mp-nav-link" title="Tab Link"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps active">
				<a href="#mp-tab-2" id="mp-animation" class="mp-nav-link icofont icofont-video-clapper" title="Media"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>

			<div class="steps active">
				<a href="#mp-tab-3" id="mp-message" class="mp-nav-link icofont icofont-comment" title="Message"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps active">
				<a href="#mp-tab-4" id="mp-preview" class="mp-nav-link icofont icofont-open-eye" title="Preview"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps active">
				<a href="#mp-tab-5" id="mp-posted" class="mp-nav-link fa fa-check" title="Posted"></a>
			</div>
		</div>
    </div>

    <div id="mp-content" class="col-md-10 tabbed-modal-content">
        <!-- Posted -->
        <div class="tab active" id="mp-tab-5">
            <div class="content">
                <div class="main-posted">
                    <span class="title-posted">Congrats your Motion Post was...</span>
                    <?php if ($publishednow): ?>
                        <h2> Posted <i class="fa fa-check-circle"></i></h2>
                        <a href="https://facebook.com/<?php echo $newMotionpost->userFanpage->facebook_page_id ?>"
                           target="_blank" title="View on facebook" class="custom-btn larg selected text-center move-center">View on facebook</a>
                    <?php else: ?>
                        <h2> Scheduled <i class="fa fa-check-circle"></i></h2>
                    <?php endif; ?>
                </div>
                <div class="additional-buttons">
                    <div class="what-next custom-btn text-center selected">What next?</div>
                    <a href="#" class="new-motion-post button custom-btn text-center" id="new-posted-button"
                       onclick="gotostep(1, motionpostObj)"><i class="icofont icofont-ui-add"></i> New Motion Post</a>
                    <a href="#" class="duplicate button custom-btn text-center" id="dupliacate-posted-button"
                       onclick="gotostep(6, <?php echo $newMotionpost->id; ?>)"><i class="icofont icofont-copy"></i> Duplicate</a>
                    <?php if ($publishednow): ?>
                        <a href="/analytics#<?= $newMotionpost->userFanpage->facebook_page_id ?>"
                           class="analytics button custom-btn text-center" id="analytics-posted-button"><i class="fa fa-area-chart" aria-hidden="true"></i> Analytics</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
		
		<div id="mp-footer" class="tabbed-modal-footer">
			<div id="mp-footer-left-side">
				<a href="#" class="prev button" id="mp-prev-button" data-open="4"><i class="icofont icofont-arrow-left"></i> Previous</a>
				<!-- <a href="#" class="cancel button" id="mp-cancel-button">Cancel</a> -->
			</div>

			<div id="mp-footer-right-side">
				<a href="#" class="close-window-button next button" id="mp-next-button" data-open="0">Close <i class="icofont icofont-arrow-right"></i></a>
			</div>
			
		</div>
		
		
    </div>

</div>

<script>
    motionpostObj = <?php echo json_encode($motionpost);?>;

    //Close main modal
    function closeModal() {
        closemodal(jQuery('.tabbed-modal'));
    }

    jQuery(document).ready(function () {
        switchfanpage("ALL");
        jQuery(".main-posted h2").animate({
            'margin-left': 0
        }, 1000, function () {

        });
        jQuery(".main-posted .view-on-facebook").animate({
            'background-position': '75px'
        }, 1000, function () {

        });

    });
</script>
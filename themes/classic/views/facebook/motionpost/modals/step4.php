<?php
$fanpageFBObj = Cacher::getFromCache('fanpage-' . $fanpage->facebook_page_id, function() use($module, $fanpage) {
    return $module->api('/'.$fanpage->facebook_page_id);
});
$fanpageFB = $fanpageFBObj->getdecodedBody();
$timezone = 'UTC' . ((date('Z') / 3600) < 0 ? (date('Z') / 3600) : "+" . (date('Z') / 3600));
?>

<div id="motion-post" class="tabbed-modal">
	<div id="motion-post-save-details" class="motion-post-save-details" style="display: none;">
		<span>Are you sure you want to Cancel this Post?</span>
		<a href="#" class="yes-button-msg" onclick="closeModal();">Yes</a>
		<a href="#" class="keep-button-msg" onclick="closeWarning();">No, keep going</a>
	</div>

	<a href="#" id="mp-close" onclick="handleClose(); return false;" title="Close">
		<i class="icofont icofont-power"></i>
	</a>
	
    <div id="motion-post-header" class="col-md-2 tabbed-modal-header">


        <div id="mp-nav" class="tabbed-modal-nav">
			<div class="steps active">
				<a href="#mp-tab-1" id="mp-tab-link" class="mp-nav-link" title="Tab Link"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps active">
				<a href="#mp-tab-2" id="mp-animation" class="mp-nav-link icofont icofont-video-clapper" title="Media"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>

			<div class="steps active">
				<a href="#mp-tab-3" id="mp-message" class="mp-nav-link icofont icofont-comment" title="Message"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps active">
				<a href="#mp-tab-4" id="mp-preview" class="mp-nav-link icofont icofont-open-eye" title="Preview"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps">
				<a href="#mp-tab-5" id="mp-posted" class="mp-nav-link fa fa-check" title="Posted"></a>
			</div>
		</div>
    </div>

    <div id="mp-content" class="col-md-10 tabbed-modal-content">


        <!-- Preview -->
        <div class="tab active" id="mp-tab-4">
            <div class="mp-tab-header">
                <h1>Preview Your Post</h1>

                <h2>Check the details and make sure everything’s good to go! </h2>
            </div>
            <div class="mp-preview-content">
                <div class="left">
                    <div class="preview-box to-page">
                        <div class="header"><!--<i class="to-page-icon"></i>--> To Page</div>
						<?php //echo '<pre>'; print_r($fanpageFB); ?>
                        <!--<div class="content"><?php// echo $fanpageFB["name"]; ?><?php echo $fanpageFB["name"]; ?></div>-->
						<div class="fan-page-box">
							<div class="image-fan-box"><img src="" style="width: 98%; border-radius:50%;"/></div>
							<div class="fan-page-name" data-page-id=""><span></span></div>
						</div>
						
                    </div>
                    <div class="preview-box when">
					<!--
                        <div class="header"><i class="when-icon"></i> When:</div>
                        <div
                            class="content"><?php echo $motionpost['schedule'] ? $datetoshow . ' ' . $timezone : "Now"; ?></div>
							-->
                    </div>
					<div class="link-to">
						<div class="header"><!--<i class="linking-to-icon"></i>-->Linking To</div>
						<div class="content">
							<div class="tabs-element">
								<div class="title"><?php echo $title; ?></div>
								<div class="thumb">
									<div class="image-placeholder"><?= strlen($image) > 0 ? '<img src="'.$image.'"/>' : '' ?></div>
								</div>
								<a href="<?php echo $url; ?>" target="_blank" class="view-on-facebook custom-btn text-center move-right"><span class="icofont icofont-eye"></span>View</a>
							</div>
						</div>
					</div>
                </div>
                <div class="right">
                    <div class="header" style="position: relative; text-align: center;"><!--<i class="anim-icon"></i>-->Media
                        and Message:<!--<i class="msg-icon"></i>--></div>
                    <div class="content">
                        <div
                            class="message-box<?php if ($animation && MotionPost::getMovieThumbnailWidth($animation->getAnimationSRC(), $motionpost["animation"]["movieThumb"]) >= 470 && MotionPost::getMovieThumbnailHeight($animation->getAnimationSRC(), $motionpost["animation"]["movieThumb"]) >= 253): ?> big-image<?php endif; ?>">
                            <div class="message-background preview">
                                <div class="header">
                                    <div class="image"></div>
                                    <div class="description">
                                        <p><a href="#">TabFu</a> shared a <a href="#">link</a>.</p>

                                        <p>3 hours ago <i class="icon-access-fb"></i></p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="fb-message"><?php echo $motionpost["message"]; ?></div>
                                    <div class="preview">
                                        <?php if ($animation && MotionPost::getMovieThumbnailWidth($animation->getAnimationSRC(), $motionpost["animation"]["movieThumb"]) >= 470 && MotionPost::getMovieThumbnailHeight($animation->getAnimationSRC(), $motionpost["animation"]["movieThumb"]) >= 253): ?>
                                            <div class="left" style="width: 300px; height: 130px;">
                                                <img style="width: 297px; height: 130px;"
                                                     src="<?php echo $animation ? $animation->getAnimationSRC() : MotionPost::getMovieThumbnail($motionpost["animation"]["value"], $motionpost["animation"]["movieThumb"]); ?>"
                                                     alt=""/>
                                            </div>
                                        <?php else: ?>
                                            <div class="left">
                                                <img
                                                    src="<?php echo $animation ? $animation->getAnimationSRC() : MotionPost::getMovieThumbnail($motionpost["animation"]["value"], $motionpost["animation"]["movieThumb"]); ?>"
                                                    alt=""/>
                                            </div>

                                        <?php endif; ?>
                                        <div class="right">
                                            <p class="title no-pointer"><?php echo $motionpost["title"]; ?></p>

                                            <p class="caption no-pointer"><?php echo $motionpost["caption"]; ?></p>

                                            <p class="description no-pointer"><?php echo $motionpost["description"]; ?></p>
                                        </div>
                                    </div>
                                    <div class="message-footer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div id="mp-footer" class="tabbed-modal-footer">
			<div id="mp-footer-left-side">
				<a href="#" class="prev button" id="mp-prev-button" data-open="3"><i class="icofont icofont-arrow-left"></i> Previous</a>
				<!-- <a href="#" class="cancel button" id="mp-cancel-button">Cancel</a> -->
			</div>

			<div id="mp-footer-right-side">
				<a href="#" class="next button" id="mp-next-button" data-open="5">Next <i class="icofont icofont-arrow-right"></i></a>
			</div>
		</div>

    </div>

</div>


<script type="text/javascript">
    motionpostObj = <?php echo json_encode($motionpost);?>;
    //Close main modal
    function handleClose() {
        jQuery('#motion-post-save-details').show();
    }

    function closeWarning() {
        jQuery('#motion-post-save-details').hide();
    }

		jQuery('.fan-page-box .fan-page-name').text(jQuery('#selected-fanpage-name').text());
		jQuery('.fan-page-box .image-fan-box img').attr('src', jQuery('#selected-fanpage-image').text());
</script>
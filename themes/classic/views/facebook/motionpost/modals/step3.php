<div id="motion-post" class="tabbed-modal">
	<div id="motion-post-save-details" class="motion-post-save-details" style="display: none;">
		<span>Are you sure you want to Cancel this Post?</span>
		<a href="#" class="yes-button-msg" onclick="closeModal();">Yes</a>
		<a href="#" class="keep-button-msg" onclick="closeWarning();">No, keep going</a>
	</div>
	<a href="#" id="mp-close" onclick="handleClose(); return false;" title="Close">
		<i class="icofont icofont-power"></i>
	</a>
    <!-- Schedule pop up -->
    <div id="schedule-main-modal" class="schedule main-modal" style="display: none;">
        <div class="header">
            <p>Choose a Date + Time</p>
            <a href="#" class="close-button" title="Close"><i class="icofont icofont-power"></i></a>
        </div>
        <div class="content">

            <div id="calendar-widget" class="calendar-widget"></div>
            <div id="time-widget" class="time-widget">
                <input type="text" id="time-picker" class="time-picker"/>
            </div>

            <a href="#" class="ok-window-button">Ok</a>

        </div>
    </div>
    <!-- /schedule -->

    <!--/Posting to-->
    <div id="motion-post-header" class="col-md-2 tabbed-modal-header">
        <div id="mp-nav" class="tabbed-modal-nav">
			<div class="steps active">
				<a href="#mp-tab-1" id="mp-tab-link" class="mp-nav-link" title="Tab Link"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps active">
				<a href="#mp-tab-2" id="mp-animation" class="mp-nav-link icofont icofont-video-clapper" title="Media"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>

			<div class="steps active">
				<a href="#mp-tab-3" id="mp-message" class="mp-nav-link icofont icofont-comment" title="Message"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps">
				<a href="#mp-tab-4" id="mp-preview" class="mp-nav-link icofont icofont-open-eye" title="Preview"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps">
				<a href="#mp-tab-5" id="mp-posted" class="mp-nav-link fa fa-check" title="Posted"></a>
			</div>
		</div>
    </div>

    <div id="mp-content" class="col-md-10 tabbed-modal-content">
        <!-- Message -->
        <div class="tab active" id="mp-tab-3">
            <div class="mp-tab-header">
                <h1>Fill out your Status Update:</h1>

                <h2>Choose the page you want to post to - Choose to post now or schedule your post for later.</h2>
            </div>

            <div class="mp-message-content">
                <div class="left">
                    <div class="posting-box">
                        <div class="header">Posting To</div>
                        <div
                            class="content <?php echo (isset($motionpost["error_fanpage"]) && $motionpost["error_fanpage"] == "true") ? 'error' : ''; ?>">
                            <div class="fan-page-box">
                                <div class="image-fan-box"><img src="" style="width: 98%; border-radius:50%;"/></div>
                                <div class="fan-page-name" data-page-id=""><span></span></div>
                            </div>
                            <a href="#" class="other-pages-button" id="other-pages-modal"><span class="fa fa-expand" aria-hidden="true"></span> Other pages</a>
                        </div>
                    </div>
                    <div class="when-box">
                        <div class="header">When to post?</div>
                        <div class="content">
                            <a href="#" class="now-button active" id="now-button"
                               onclick="setpublishnow();return false;"><span class="fa fa-thumbs-up" aria-hidden="true"></span> Now</a>
                            <span class="when-box-text">or</span>
                            <a href="#" class="schedule-button" id="schedule-button"><span class="fa fa-calendar" aria-hidden="true"></span> Schedule</a>
                            <i class="ticked active fa fa-check" aria-hidden="true" style="display:none;"></i>
                        </div>
                    </div>
                </div>
                <div class="right">
<!--                    <div
                        class="message-box<?php if ($animation && MotionPost::getMovieThumbnailWidth($animation->getAnimationSRC(), $motionpost["animation"]["movieThumb"]) >= 470 && MotionPost::getMovieThumbnailHeight($animation->getAnimationSRC(), $motionpost["animation"]["movieThumb"]) >= 253): ?> big-image<?php endif; ?>">
-->                    <div
                        class="message-box big-image">
                        <div class="header">Post Message</div>
                        <div class="content ">
                            <div class="message-background">
                                <div class="header">
                                    <div class="image"></div>
                                    <div class="description">
                                        <p><a href="#">TabFu</a> shared a <a href="#">link</a>.</p>

                                        <p>3 hours ago <i class="icon-access-fb"></i></p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div
                                        class="fb-message <?php echo (isset($motionpost["error_message"]) && $motionpost["error_message"] == "true") ? 'error' : ''; ?>"
                                        onclick="changetotextarea(this, 'message');return false;"><?php echo $motionpost["message"] ? $motionpost["message"] : 'Enter Message'; ?></div>
                                    <div class="preview">

                                        <?php if ($animation && MotionPost::getMovieThumbnailWidth($animation->getAnimationSRC(), $motionpost["animation"]["movieThumb"]) >= 470 && MotionPost::getMovieThumbnailHeight($animation->getAnimationSRC(), $motionpost["animation"]["movieThumb"]) >= 253): ?>
                                            <div class="left" style="width: 300px; height: 130px;">
                                                <img style="width: 297px; height: 130px;"
                                                     src="<?php echo $animation ? $animation->getAnimationSRC() : MotionPost::getMovieThumbnail($motionpost["animation"]["value"], $motionpost["animation"]["movieThumb"]); ?>"
                                                     alt=""/>
                                            </div>
                                        <?php else: ?>
                                            <div class="left">
                                                <img
                                                    src="<?php echo $animation ? $animation->getAnimationSRC() : MotionPost::getMovieThumbnail($motionpost["animation"]["value"], $motionpost["animation"]["movieThumb"]); ?>"
                                                    alt=""/>
                                            </div>

                                        <?php endif; ?>

                                        <div class="right">
                                            <p class="title <?php echo (isset($motionpost["error_title"]) && $motionpost["error_title"] == "true") ? 'error' : ''; ?>"
                                               onclick="changetoinput(this, 'title');return false;"><?php echo $motionpost["title"] ? $motionpost["title"] : 'Enter Title'; ?></p>

                                            <p class="caption <?php echo (isset($motionpost["error_caption"]) && $motionpost["error_caption"] == "true") ? 'error' : ''; ?>"
                                               onclick="changetoinput(this, 'caption');return false;"><?php echo $motionpost["caption"] ? $motionpost["caption"] : 'Enter Caption'; ?></p>

                                            <p class="description <?php echo (isset($motionpost["error_description"]) && $motionpost["error_description"] == "true") ? 'error' : ''; ?>"
                                               onclick="changetotextarea(this, 'description');return false;"><?php echo $motionpost["description"] ? $motionpost["description"] : 'Enter Description'; ?></p>
                                        </div>
                                    </div>
                                    <div class="message-footer"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
            <div id="mp-footer" class="tabbed-modal-footer">
				<div id="mp-footer-left-side">
					<a href="#" class="prev button" id="mp-prev-button" data-open="2"><i class="icofont icofont-arrow-left"></i> Previous</a>
					<!-- <a href="#" class="cancel button" id="mp-cancel-button">Cancel</a> -->
				</div>

				<div id="mp-footer-right-side">
					<a href="#" class="next button" id="mp-next-button" data-open="4">Next <i class="icofont icofont-arrow-right"></i></a>
				</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    motionpostObj = <?php echo json_encode($motionpost);?>;
    //Chose fanpage
	
    jQuery(document).on('click', '#select-fanpage-main-modal-for-publish .page-box', function () { 
        jQuery(this).addClass('active');
        jQuery(this).siblings().each(function () {
            jQuery(this).removeClass('active');
        });
        motionpostObj.page_id = jQuery(this).attr('data-page-id');
        jQuery('.posting-box .fan-page-name').text(jQuery(this).find('.page-box-title').text());
        jQuery('.posting-box .image-fan-box img').attr('src', jQuery(this).find('.image-placeholder img').attr('src'));
		jQuery('#selected-fanpage-name').text(jQuery(this).find('.page-box-title').text());
		jQuery('#selected-fanpage-image').text(jQuery(this).find('.image-placeholder img').attr('src'));
    });
	
    jQuery(document).ready(function () {
        if (motionpostObj.page_id != '') {
            jQuery('.page-box[data-page-id="' + motionpostObj.page_id + '"]').click();
            jQuery('.page-box[data-page-id="' + motionpostObj.page_id + '"]').trigger('click');

        }
        if (motionpostObj.schedule != "" && motionpostObj.schedule != null) {
            jQuery('#mp-tab-3 .when-box .ticked').css('bottom', '18px');
        }
    });


    //Display publish-to modal
    jQuery(document).on('click', '.other-pages-button', function (e) {
        e.preventDefault();

        jQuery('#select-fanpage-main-modal-for-publish').fadeIn();
        displaymodal(jQuery('#select-fanpage-main-modal-for-publish'));
        if (!jQuery("#select-fanpage-main-modal-for-publish  .content  .publish-list  .content").hasClass("mCustomScrollbar")) {
            jQuery("#select-fanpage-main-modal-for-publish  .content  .publish-list  .content").mCustomScrollbar({
                mouseWheel: true,
                alwaysShowScrollbar: 1,
                autoDraggerLength: true,
                advanced: {
                    updateOnBrowserResize: true,
                    updateOnContentResize: true
                }
            });
        } else {
            jQuery("#select-fanpage-main-modal-for-publish  .content  .publish-list  .content").mCustomScrollbar("update");
        }

    });
    //close publish to modal
    jQuery(document).on('click', '#select-fanpage-main-modal-for-publish .ok-window-button', function (e) {
        e.preventDefault();
        closemodal(jQuery('#select-fanpage-main-modal-for-publish'));

    });
    jQuery(document).on('click', '#select-fanpage-main-modal-for-publish .close-button', function (e) {
        e.preventDefault();
        closemodal(jQuery('#select-fanpage-main-modal-for-publish'));

    });


    //Set publish now
    function setpublishnow() {
        motionpostObj.schedule = null;
        jQuery('#mp-tab-3 .when-box .ticked').css('bottom', '85px');
        jQuery('#now-button').addClass('active');
        jQuery('#schedule-button').removeClass('active');
 		
    }


    //Editing
    function changetoinput(element, name) {
        jQuery(element).replaceWith('<input onchange="motionpostObj[name]=jQuery(this).val();" type="text" name="' + name + '" placeholder="' + jQuery(element).text() + '" value="' + motionpostObj[name] + '" />');
        jQuery('input[name="' + name + '"]').focus();
        return true;
    }

    function changetotextarea(element, name) {
        jQuery(element).replaceWith('<textarea onchange="motionpostObj[name]=jQuery(this).val();"  placeholder="' + jQuery(element).text() + '" style="width: 100%;" type="text" name="' + name + '" >' + motionpostObj[name] + '</textarea>');
        jQuery('textarea[name="' + name + '"]').focus();
        return true;
    }


    //Close main modal
    function handleClose() {
        jQuery('#motion-post-save-details').show();
    }

    function closeWarning() {
        jQuery('#motion-post-save-details').hide();
    }


    //Schedule 

    jQuery('#calendar-widget').datepicker()
        .on('changeDate', function (ev) {
            daypicked = ev.date.getDate();
            monthpicked = ev.date.getMonth();
            yearpicked = ev.date.getFullYear();
        });
    daypicked = jQuery('#calendar-widget').data("datepicker").date.getDate();
    monthpicked = jQuery('#calendar-widget').data("datepicker").date.getMonth();
    yearpicked = jQuery('#calendar-widget').data("datepicker").date.getFullYear();
    jQuery('#time-picker').timepicker({showMeridian: false});

    jQuery(document).on('click', '.schedule-button', function (e) {
        e.preventDefault();
        displaymodal(jQuery('#schedule-main-modal'));


    });

    jQuery(document).on('click', '#schedule-main-modal .ok-window-button', function (e) {
        e.preventDefault();
        current = new Date();
        time = jQuery('#time-picker').attr('value').split(':');


        selected = new Date(yearpicked, monthpicked, daypicked, time[0], time[1]);
        if (selected > current) {
            motionpostObj.schedule = selected;
            jQuery('#mp-tab-3 .when-box .ticked').css('bottom', '13px');
			jQuery('#now-button').removeClass('active');
			jQuery('#schedule-button').addClass('active');
            closemodal(jQuery('#schedule-main-modal'));
        } else {
            displayFlashMessage('warning', 'Incorrect date');
        }

    });

    jQuery(document).on('click', '#schedule-main-modal .close-button', function (e) {
        e.preventDefault();
        closemodal(jQuery('#schedule-main-modal'));

    });
</script>
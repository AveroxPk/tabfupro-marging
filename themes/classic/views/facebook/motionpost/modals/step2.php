<div id="motion-post" class="tabbed-modal">
	<div id="motion-post-save-details" class="motion-post-save-details" style="display: none;">
		<span>Are you sure you want to Cancel this Post?</span>
		<a href="#" class="yes-button-msg" onclick="closeModal();">Yes</a>
		<a href="#" class="keep-button-msg" onclick="closeWarning();">No, keep going</a>
	</div>
	<a href="#" id="mp-close" onclick="handleClose(); return false;" title="Close">
		<i class="icofont icofont-power"></i>
	</a>
<!-- Publish your tab -->
<div id="select-image-main-modal" class="upload main-modal" style="display: none;">
    <div class="header">
        <p>Upload Image:</p>
        <a href="#" class="close-button" title="Close"><i class="icofont icofont-power"></i></a>
    </div>
    <div class="content">
        <?php echo CHtml::hiddenField('animation_upload', '', array('class' => 'animation_upload'));
        echo CHtml::fileField('Animation_file_drive', $model->file_drive, array(
            'class' => 'image-upload',
            'style' => 'display:none',
        )); ?>
        <div class="upload-list">
            <div class="header">
                Use a <span class="prefered-image-size">484x253</span> pixel image for best results
            </div>
            <div class="content">

                <!-- Publishing -->
                <div class="uploading">
                        <span class="image-element-hider">
                            <img class="image-preview" src="/images/tabwizard/drag-file.png"/>
                        </span>
                    <a class="upload-button">Choose</a>
                </div>
            </div>
        </div>

        <a href="#" class="ok-window-button">Ok</a>
    </div>
</div>

<!--/publish-->


    <div id="motion-post-header" class="col-md-2 tabbed-modal-header">


        <div id="mp-nav" class="tabbed-modal-nav">
			<div class="steps active">
				<a href="#mp-tab-1" id="mp-tab-link" class="mp-nav-link" title="Tab Link"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps active">
				<a href="#mp-tab-2" id="mp-animation" class="mp-nav-link icofont icofont-video-clapper" title="Media"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>

			<div class="steps">
				<a href="#mp-tab-3" id="mp-message" class="mp-nav-link icofont icofont-comment" title="Message"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps">
				<a href="#mp-tab-4" id="mp-preview" class="mp-nav-link icofont icofont-open-eye" title="Preview"></a>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
				<i class="fa fa-circle" aria-hidden="true"></i>
			</div>
			
			<div class="steps">
				<a href="#mp-tab-5" id="mp-posted" class="mp-nav-link fa fa-check" title="Posted"></a>
			</div>
		</div>
	</div>

<div id="mp-content" class="col-md-10 tabbed-modal-content">
    <!-- Animation -->
    <div class="tab active" id="mp-tab-2">
        <div class="mp-tab-header">
            <h1>Add Media to your Post</h1>

            <h2>Select Image Post, Animated Post or Video Post and customise to your liking</h2>
        </div>

        <div id="mp-tab-animation">
            <div class="select-menu">
                <div class="select-image-wrapper">
                    <a href="#" class="select-image" title="Select Image"><span class="icofont icofont-image"></span> Select Image</a>
                </div>

                <div class="select-gif-wrapper">
                    <a href="#" class="select-gif" title="Select Gif"><span class="icofont icofont-image"></span> Select Gif</a>
                </div>

                <div class="select-video-wrapper">
                    <a href="#" class="select-video" title="Select Movie"><span class="fa fa-video-camera"></span> Select Movie</a>
                </div>
            </div>
            <div class="left">

                <div class="image-box">
                    <div class="library-title">
						<div class="col-md-9">
							<h3>Image Library</h3>
							<p>Choose a stock or previously uploaded image</p>
						</div>
						<div class="col-md-3">
							<a href="#" class="upload-image-button"><i class="fa fa-cloud-upload"></i> Upload image</a>
						</div>
                    </div>
                    <div
                        class="content <?php echo (isset($motionpost["error_animation"]) && $motionpost["error_animation"] == "true") ? 'error' : ''; ?>">
                        <div class="image-list">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'id' => 'image-list',
                                'dataProvider' => $animation,
                                'itemView' => 'partial/_images',
                                'template' => '{items}',
                                'emptyText' => '',
                                'viewData' => array(
                                    'favorite' => true,
                                ),
                            ));
                            ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="center" style="display: none;">
                <div class="image-box">
                    <div class="library-title">
						<div class="col-md-8">
							<h3>Animated GIF Library</h3>
							<p>Choose a stock or previously uploaded GIF</p>
						</div>
						<div class="col-md-4">
							<a href="#" class="upload-image-button upload-gif-button"><i class="icofont icofont-play-alt-3"></i> Upload Custom Animation</a>
						</div>
                    </div>
                    <div
                        class="content <?php echo (isset($motionpost["error_animation"]) && $motionpost["error_animation"] == "true") ? 'error' : ''; ?>">
                        <div class="image-list">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'id' => 'image-list',
                                'dataProvider' => $gifs,
                                'itemView' => 'partial/_gifs',
                                'template' => '{items}',
                                'emptyText' => '',
                                'viewData' => array(
                                    'favorite' => true,
                                ),
                            ));
                            ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="right" style="display: none;">
				<!--
					<div class="paste-video-link ">
						<input
							class="<?php echo (isset($motionpost["error_animation"]) && $motionpost["error_animation"] == "true") ? 'error' : ''; ?>"
							type="text" name="paste-url" id="mp-paste-url" onkeyup="selectURLMovie(jQuery(this));"
							placeholder="paste video or image URL here..."/>
					</div>
					<div class="tick"></div>
					<div class="upload-own-movie-icon">
						<a href="#" class="upload-movie-icon-button" title="Upload image">Upload image</a>
					</div>
				-->
                <div class="video-box">
                    <div class="header"></div>
                    <div class="library-title">
						<div class="col-md-5">
							<h3>Video Thumbnail Library</h3>
							<p>Select stock or uploaded thumbnail image</p>
						</div>
						<div class="col-md-7">
							<div class="paste-video-link ">
							<input
								class="<?php echo (isset($motionpost["error_animation"]) && $motionpost["error_animation"] == "true") ? 'error' : ''; ?>"
								type="text" name="paste-url" id="mp-paste-url" onkeyup="selectURLMovie(jQuery(this));"
								placeholder="add video or image URL here..." style="width:185px; border:1px solid #ccc;"/>
							</div>
							<div class="tick"></div>
							<a href="#" class="upload-movie-icon-button" title="Upload image"><i class="icofont icofont-video-alt"></i> Upload Thumbnail </a>
						</div>
                    </div>
                    <div
                        class="content <?php echo (isset($motionpost["error_animation"]) && $motionpost["error_animation"] == "true") ? 'error' : ''; ?>">
                        <div class="video-list">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'id' => 'video-list',
                                'dataProvider' => $movieIcons,
                                'itemView' => 'partial/_movie_icon',
                                'template' => '{items}',
                                'emptyText' => '',
                                'viewData' => array(
                                    'favorite' => false,
                                ),
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="mp-footer" class="tabbed-modal-footer">
            <div id="mp-footer-left-side">
                <a href="#" class="prev button" id="mp-prev-button" data-open="1"><i class="icofont icofont-arrow-left"></i> Previous</a>
                <!-- <a href="#" class="cancel button" id="mp-cancel-button">Cancel</a> -->
            </div>

            <div id="mp-footer-right-side">
                <a href="#" class="next button" id="mp-next-button" data-open="3">Next <i class="icofont icofont-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
motionpostObj = <?php echo json_encode($motionpost);?>;

var uploadAdress = '/uploadanimationajax';


//Select subTab
jQuery(document).on('click', '.select-image', function (e) {
    jQuery('#mp-tab-animation .left').css('display', 'block');
    jQuery('#mp-tab-animation .select-image-wrapper').css('background-color', '#0274c5');
    jQuery('#mp-tab-animation .select-image-wrapper').css('color', '#ffffff');
    jQuery('#mp-tab-animation .select-image-wrapper').css('border', 'none');
    jQuery('#mp-tab-animation .right').css('display', 'none');
    jQuery('#mp-tab-animation .select-video-wrapper').css('background-color', '#ffffff');
    jQuery('#mp-tab-animation .select-video-wrapper').css('color', '#000000');
    jQuery('#mp-tab-animation .center').css('display', 'none');
    jQuery('#mp-tab-animation .select-gif-wrapper').css('background-color', '#ffffff');
    jQuery('#mp-tab-animation .select-gif-wrapper').css('color', '#000000');
    motionpostObj.animation.type = 0;
    motionpostObj.animation.movieThumb = null;
    
    return false;
});
jQuery(document).on('click', '.select-gif', function (e) {
    jQuery('#mp-tab-animation .left').css('display', 'none');
    jQuery('#mp-tab-animation .select-image-wrapper').css('background-color', '#ffffff');
    jQuery('#mp-tab-animation .select-image-wrapper').css('color', '#000000');
    jQuery('#mp-tab-animation .right').css('display', 'none');
    jQuery('#mp-tab-animation .select-video-wrapper').css('background-color', '#ffffff');
    jQuery('#mp-tab-animation .select-video-wrapper').css('color', '#000000');
    jQuery('#mp-tab-animation .center').css('display', 'block');
    jQuery('#mp-tab-animation .select-gif-wrapper').css('background-color', '#0274c5');
    jQuery('#mp-tab-animation .select-gif-wrapper').css('color', '#ffffff');
    jQuery('#mp-tab-animation .select-gif-wrapper').css('border', 'none');
    motionpostObj.animation.type = 0;
    motionpostObj.animation.movieThumb = null;
    
    return false;
});
jQuery(document).on('click', '.select-video', function (e) {
    jQuery('#mp-tab-animation .left').css('display', 'none');
    jQuery('#mp-tab-animation .select-image-wrapper').css('background-color', '#ffffff');
    jQuery('#mp-tab-animation .select-image-wrapper').css('color', '#000000');
    jQuery('#mp-tab-animation .right').css('display', 'block');
    jQuery('#mp-tab-animation .select-video-wrapper').css('background-color', '#0274c5');
    jQuery('#mp-tab-animation .select-video-wrapper').css('color', '#ffffff');
    jQuery('#mp-tab-animation .select-video-wrapper').css('border', 'none');
    jQuery('#mp-tab-animation .center').css('display', 'none');
    jQuery('#mp-tab-animation .select-gif-wrapper').css('background-color', '#ffffff');
    jQuery('#mp-tab-animation .select-gif-wrapper').css('color', '#000000');
    motionpostObj.animation.type = 1;
    
    return false;
});


//Upload Image
jQuery(document).on('click', '.upload-image-button', function (e) {
    e.preventDefault();
    displaymodal(jQuery('#select-image-main-modal'));
    jQuery('#select-image-main-modal').find('.header').find('p').text('Upload Image.');
    jQuery('#select-image-main-modal').find('.header').find('span').text('484x253');
    uploadAdress = '/uploadanimationajax';
    
    return false;
});


jQuery(document).on('click', '.upload-gif-button', function (e) {
    e.preventDefault();
    displaymodal(jQuery('#select-image-main-modal'));
    jQuery('#select-image-main-modal').find('.header').find('p').text('Upload Image.');
    jQuery('#select-image-main-modal').find('.header').find('span').text('470x253');
    uploadAdress = '/uploadanimationajax';
    
    return false;
});

//Upload Movie Icon
jQuery(document).on('click', '.upload-movie-icon-button', function (e) {
    e.preventDefault();
    displaymodal(jQuery('#select-image-main-modal'));
    jQuery('#select-image-main-modal').find('.header').find('p').text('Upload Movie Thumb.');
    jQuery('#select-image-main-modal').find('.header').find('span').text('156x116');
    uploadAdress = '/uploadmoviethumbajax';
    
    return false;

});

buttonUpload = jQuery('.uploading input[type="submit"]');
loader = jQuery('.uploading .uploadingfile');
var client = new XMLHttpRequest();

var $input = $('#Animation_file_drive'),
    input = $input[0]
    ;

$input.change(function () {
    var formData;

    $.blockUI({message: "Uploading file... Please wait..."});

    if (input.files.length === 0) {
        return;
    }

    formData = new FormData();
    formData.append('file', input.files[0]);

    console.log(input.files);

    $.ajax({
        'url': uploadAdress,
        'cache': false,
        'contentType': false,
        'processData': false,
        'dataType': 'json',
        'type': 'POST',
        'data': formData
    })
        .done(function (data) {
            var $parent;

            if (data.status === 'error' || data.status === 'warning') {
                alert("There was an error while image upload: " + data.message);
                return;
            }

            //Reset file input
            $input.val('');

            $parent = $input.parent();

            //Server filename is sent through hidden field
            $("input[type=hidden]", $parent).val(data.file);

            $(".image-preview", $parent).attr("src", '/animations/' + data.file);

            responseData = jQuery.parseJSON(client.responseText);
            if (data.type == 0) {
                jQuery('<div onclick="selectAnimation(jQuery(this), event); return false;" class="fav-element" data-animation-id="' + data.id + '">' +
                    '<div class="animation"><img src="<?php echo Yii::app()->baseUrl.'/animations/';?>' + data.file + '" /></div>' +
                    '<a href="#" class="delete-image-button" data-animation-id="' + data.id + '"></a>' +
                    '</div>'
                ).insertBefore(jQuery('#mp-tab-animation .left .image-box .items .fav-element').eq(0));
            } else if (data.type == 2) {
                jQuery('<div onclick="selectGif(jQuery(this), event); return false;" class="gif-element" data-gif-id="' + data.id + '">' +
                    '<div class="animation"><img src="<?php echo Yii::app()->baseUrl.'/animations/';?>' + data.file + '" /></div>' +
                    '<a href="#" class="delete-image-button" data-gif-id="' + data.id + '"></a>' +
                    '</div>'
                ).insertBefore(jQuery('#mp-tab-animation .center .image-box .items .gif-element').eq(0));
            } else if (data.type == 1) {
                jQuery(
                    '<div onclick="selectMovieIcon(jQuery(this), event); return false;" class="movie-icon-element" data-movie-icon-id="' + data.id + '">' +
                    '<div class="movie-icon"><img src="<?php echo Yii::app()->baseUrl.'/animations/';?>' + data.file + '" /></div>' +
                    '<a href="#" class="delete-image-button" data-movie-icon-id="' + data.id + '"></a>' +
                    '</div>'
                ).insertBefore(jQuery('#mp-tab-animation .right .video-box .items .movie-icon-element').eq(0));
            }
            displayFlashMessage(data.status, data.message);
            jQuery("#mp-tab-2  div.content").mCustomScrollbar("update");
        })
        .complete(function () {
            $.unblockUI();
        });
});

$('.upload-list .content .uploading').click(function () {
    $input.click();
});

//close upload to modal
jQuery(document).on('click', '#select-image-main-modal .ok-window-button', function (e) {
    e.preventDefault();
    closemodal(jQuery('#select-image-main-modal'));
    $(".image-preview").attr("src", '/images/tabwizard/drag-file.png');
    jQuery(loader).hide(function () {
        jQuery(buttonUpload).show();
    });
    
    return false;
});
jQuery(document).on('click', '#select-image-main-modal .close-button', function (e) {
    e.preventDefault();
    closemodal(jQuery('#select-image-main-modal'));
    $(".image-preview").attr("src", '/images/tabwizard/drag-file.png');
    jQuery(loader).hide(function () {
        jQuery(buttonUpload).show();
    });
    
    return false;
});


//Toogle favorite
function toogleFavorite(selector) {

    id = jQuery(selector).closest('.fav-element').attr('data-animation-id');
    element = jQuery(selector).closest('.fav-element');
    jQuery.ajax({
        type: "POST",
        url: '/tooglefavoriteanimation',
        data: {"id": id},
        dataType: "json"


    })
        .done(function (data) {
            if (data.status == "ok") {
                if (jQuery(element).parents('.left').length) {

                    jQuery(selector).removeClass('active');
                    jQuery(element).appendTo(jQuery('#mp-tab-animation .right  .items'));

                    jQuery("#mp-tab-2 .right  div.content").mCustomScrollbar("update");
                } else {
                    jQuery(selector).addClass('active');
                    jQuery(element).appendTo(jQuery('#mp-tab-animation .left  .items'));


                    jQuery("#mp-tab-2 .left  div.content").mCustomScrollbar("update");
                }
            } else {
                return false;
            }
        });
}

//delete Animation
jQuery(document).on('click', '.delete-image-button', function () {
    var $this = $(this);
    if (typeof $this.attr('data-animation-id') != 'undefined') {
        $.get('/deleteanimationajax?id=' + $this.attr('data-animation-id')).done(function (data) {
        });
        $this.parent('.fav-element').fadeOut('slow', function () {
            $(this).remove();
        });
    } else if (typeof $this.attr('data-gif-id') != 'undefined') {
        $.get('/deleteanimationajax?id=' + $this.attr('data-gif-id')).done(function (data) {
        });
        $this.parent('.gif-element').fadeOut('slow', function () {
            $(this).remove();
        });
    } else {
        $.get('/deletemovieajax?id=' + $this.attr('data-movie-icon-id')).done(function (data) {
        });
        $this.parent('.movie-icon-element').fadeOut('slow', function () {
            $(this).remove();
        });
    }
    return false;
});
//Select Animation

function selectAnimation(animation, ev) {
    if (!jQuery(ev.target).hasClass('favorite') && !jQuery(ev.target).hasClass('delete-image-button')) {
        id = jQuery(animation).attr('data-animation-id');
        motionpostObj.animation.type = 0;
        motionpostObj.animation.value = id;
        jQuery('.fav-element').each(function () {
            if (jQuery(this).attr('data-animation-id') != id) {
                jQuery(this).removeClass('active');
            } else {
                jQuery(this).addClass('active');
            }

        });
        jQuery('.movie-icon-element').each(function () {
            jQuery(this).removeClass('active');
        });
        jQuery('.gif-element').each(function () {
            jQuery(this).removeClass('active');
        });
    }

}

//Select Gif

function selectGif(animation, ev) {
    if (!jQuery(ev.target).hasClass('favorite') && !jQuery(ev.target).hasClass('delete-image-button')) {
        id = jQuery(animation).attr('data-gif-id');
        motionpostObj.animation.type = 0;
        motionpostObj.animation.value = id;
        jQuery('.gif-element').each(function () {
            if (jQuery(this).attr('data-gif-id') != id) {
                jQuery(this).removeClass('active');
            } else {
                jQuery(this).addClass('active');
            }

        });
        jQuery('.fav-element').each(function () {
            jQuery(this).removeClass('active');
        });
        jQuery('.movie-icon-element').each(function () {
            jQuery(this).removeClass('active');
        });
    }
}


//Select Movie Thumb

function selectMovieIcon(animation, ev) {
    if (!jQuery(ev.target).hasClass('favorite') && !jQuery(ev.target).hasClass('delete-image-button')) {
        id = jQuery(animation).attr('data-movie-icon-id');
        motionpostObj.animation.movieThumb = id;
        jQuery('#mp-tab-animation > .right  .tick').css('background', 'none');
        jQuery('.movie-icon-element').each(function () {
            if (jQuery(this).attr('data-movie-icon-id') != id) {
                jQuery(this).removeClass('active');
            } else {
                jQuery(this).addClass('active');
            }

        });
        jQuery('.fav-element').each(function () {
            jQuery(this).removeClass('active');
        });
        jQuery('.gif-element').each(function () {
            jQuery(this).removeClass('active');
        });
    }

}

//Paste URL
function selectURLMovie(element) {
    url = jQuery(element).val();
    motionpostObj.animation.type = 1;
    motionpostObj.animation.value = url;
    motionpostObj.animation.movieThumb = "";
    if (motionpostObj.animation.value.match(/youtube/) !== null || motionpostObj.animation.value.match(/vimeo/) !== null) {
        jQuery('#mp-tab-animation > .right  .tick').css('background', 'url(../images/step-2/tick.png) no-repeat');
        jQuery('#mp-tab-animation > .right  .tick').css('background-size', '20px 20px');
        jQuery('#mp-tab-animation > .right  .tick').css('background-position', '0px 8px');
        if (jQuery('.video-added').length <= 0) {
            jQuery('body').append(
                '<div class="flash-container video-added col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1" style="top: 246.5px; display: none;">' +
                '<div class="flash-container-close"><span class="glyphicon glyphicon-remove"></span></div>' +
                '<div class="flash-notice" style="text-align:center">video added successfully<br/>choose a custom thumbnail or click \'next\' to use the video\'s existing thumbnail</div>' +
                '</div>'
            );
            jQuery('.video-added').fadeIn(function () {
                setTimeout(function () {
                    jQuery('.video-added').fadeOut();
                }, 8000);
            });
        }
    } else {
        jQuery('#mp-tab-animation > .right  .tick').css('background', 'none');
    }
    jQuery('.fav-element').each(function () {
        jQuery(this).removeClass('active');
    });
    jQuery('.movie-icon-element').each(function () {
        jQuery(this).removeClass('active');
    });
}

//Close main modal
function handleClose() {
    jQuery('#motion-post-save-details').show();
}

function closeWarning() {
    jQuery('#motion-post-save-details').hide();
}


//Search animation
jQuery('input[name="search-animation"]').keyup(function (e) {
    that = this;
    jQuery('.favorites-box .fav-element').show();
    if (jQuery(that).val() != '') {
        jQuery('.favorites-box .fav-element').each(function () {
            if (jQuery(this).find('.title').text().toLowerCase().indexOf(jQuery(that).val().toLowerCase()) < 0) {
                jQuery(this).hide();
            }
        });
    }
    else {
        jQuery('.favorites-box .fav-element').each(function () {
            jQuery(this).show();
        });
    }
    jQuery('.animations-box .fav-element').show();
    if (jQuery(that).val() != '') {
        jQuery('.animations-box .fav-element').each(function () {
            if (jQuery(this).find('.title').text().toLowerCase().indexOf(jQuery(that).val().toLowerCase()) < 0) {
                jQuery(this).hide();
            }
        });
    }
    else {
        jQuery('.animations-box .fav-element').each(function () {
            jQuery(this).show();
        });
    }

});


//INIT
function initialize() {
    if (motionpostObj.animation.type == 0 && motionpostObj.animation.value != null) {
        jQuery('.select-image').click();
        jQuery('#mp-tab-animation .fav-element[data-animation-id="' + motionpostObj.animation.value + '"]').click();
    } else if (motionpostObj.animation.type == 1) {
        if (isNaN(parseFloat(motionpostObj.animation.value))) {
            jQuery('#mp-paste-url').val(motionpostObj.animation.value);
        }
        if (isNaN(parseFloat(motionpostObj.animation.movieThumb)) && (motionpostObj.animation.value.match(/youtube/) !== null || motionpostObj.animation.value.match(/vimeo/) !== null )) {
            jQuery('#mp-tab-animation > .right  .tick').css('background', 'url(../images/step-2/tick.png) no-repeat');
            jQuery('#mp-tab-animation > .right  .tick').css('background-size', '20px 20px');
            jQuery('#mp-tab-animation > .right  .tick').css('background-position', '0px 20px');
        }
        if (motionpostObj.animation.movieThumb !== null && motionpostObj.animation.movieThumb !== "" && motionpostObj.animation.movieThumb !== false) {
            jQuery('#mp-tab-animation .movie-icon-element[data-movie-icon-id="' + motionpostObj.animation.movieThumb + '"]').click();
        }
        jQuery('.select-video').click()
    }

    if (jQuery("#mp-tab-2  div.content").is(":visible")) {
        jQuery("#mp-tab-2  div.content").mCustomScrollbar({
            mouseWheel: true,
            alwaysShowScrollbar: 1,
            autoDraggerLength: true,
            advanced: {
                updateOnBrowserResize: true,
                updateOnContentResize: true
            }
        });

    } else {
        setTimeout(function () {
            initialize();
        }, 500);
    }

}
initialize();


</script>
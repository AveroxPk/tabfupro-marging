<?php
Yii::app()->clientScript
    ->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.blockUi.js');
try {
    $pic = $module->getUserPic($userfacebook->facebook_user_id);

} catch (FacebookApiException $e) {
    Yii::app()->user->setFlash('notice', 'Please select facebook account.');
    $this->redirect(array('/facebook/facebook/index'));
}
?>
<div class="motion-posts-editor"></div>

<div class="row tabfu-row">
    <div class="col-md-3 col-sm-5 sidestrip ">		
        <div class="page-boxes">
            <a href="#" onclick='switchfanpage("ALL"); jQuery(".topwrapper .selected").text("All fanpages"); return false;' class="page-box main">
                <div class="image-placeholder">
                    <img src="/images/motion-posts/user-page.png" />
                </div>
                <div class="page-box-title">
                    <span><?php echo $userfacebook->name." ".$userfacebook->surname?>'s Pages</span>
                </div>
            </a>

            
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $fanpages,
                'itemView' => 'partial/_fanpage',
                'template' => '{items}{summary}{pager}',
                'emptyText' => false,
                'summaryText'=>'{start}-{end} of {count}',
                'pager'=>array(
                    'header'=>'',
                    'maxButtonCount'=>0
                ),
                'viewData' => array(
                    'module' => $module
                ),
            ));
            ?>

        </div>
    </div> 
    <div class="col-md-8 col-sm-4 col-lg-9 home-page">
		<div class="template-picker">
			<?php
			Yii::app()->getModule('template');

			echo CHtml::htmlButton('<i class="fa fa-facebook fa-size"></i> <span>|</span> Create Motion Post',array(
//				'onclick' => 'tabWizard.newTab(getDefaultFanpage());',
				'id' => 'create-new-post',
				'class' => 'fb-btn-add create-ad',
				));
			?>
        </div>	
	
	
		<div class="row filter-area">
			<div class="col-md-12">
				<div class="margin0 filter">
					<div class="search-background">
						<form method="get">
							<input type="search" name="search" value="" placeholder="Search Posts" id="main-search"/>
							<span class="fa fa-search"></span>
						</form>
					</div>
				</div>
			</div>
		</div>
		
        <div class="motion-posts-elements">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $motionposts,
                'itemView' => 'partial/_motionpost',
                'template' => '{items}',
                'emptyText' => "You don&#8217;t have any post.",
                'viewData' => array(
                    'module' => $module
                ),
            ));
            ?>
        </div>
    </div>

    <!-- Posting -->
    <div id="select-fanpage-main-modal-for-publish" class="publish main-modal" style="display: none;">
        <div class="header">
            <p>Select fanpage</p>
            <a href="#" class="close-button" title="Close"><i class="icofont icofont-power"></i></a>
        </div>
        <div class="content">

            <div class="publish-list">
                <div class="content">

                    <!-- Posting -->
                    <div class="publishing">
                        <div class="page-boxes">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $fanpages,
                                'itemView' => 'partial/_fanpage',
                                'template'=>'{pager}{summary}{items}{summary}{pager}',
                                'summaryText'=>'{start}-{end} of {count}',
                                'pager'=>array(
                                    'header'=>'',
                                    'maxButtonCount'=>0
                                ),
                                'emptyText' => 'You dont have any fanpages added.',
                                'viewData' => array(
                                    'module' => $module,
                                    'noswitch' => true,
                                ),
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="ok-window-button">Ok</a>
        </div>
    </div>
	<div id="selected-fanpage-data" style="display:none;">
		<div id="selected-fanpage-name"></div>
		<div id="selected-fanpage-image"></div>
	</div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/motionpost.js"></script>
<script type="text/javascript">
    motionpostObj.tab_id = "<?php echo count($tabs)>0?$tabs[0]->id:''; ?>";
    motionpostObj.page_id = "<?php echo $fanpage->id?$fanpage->id:''; ?>";
</script>
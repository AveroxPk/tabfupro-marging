<script type="text/javascript">
    var client = new XMLHttpRequest();

    function saveAnimation() {
        var file = document.getElementById("Animation_file_drive");

        /* Create a FormData instance */
        var formData = new FormData();
        /* Add the file */
        formData.append("Animation", file.files[0]);

        client.open("post", "/uploadanimationajax", true);
        //client.setRequestHeader("Content-Type", "multipart/form-data");
        client.send(formData);
        /* Send to server */
    }

    /* Check the response status */
    client.onreadystatechange = function () {
        if (client.readyState == 4 && client.status == 200) {
            responseData = jQuery.parseJSON(client.responseText);
            displayFlashMessage(responseData.status, responseData.message);
        }
    }
</script>

<?php

$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'upload-animation',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )
);

echo $form->labelEx($model, 'file_drive');
echo $form->fileField($model, 'file_drive');
echo $form->error($model, 'file_drive');

echo CHtml::submitButton('Submit', array('onclick' => 'saveAnimation(); return false;'));
$this->endWidget();
?>

<div onclick="selectAnimation(jQuery(this), event); return false;" class="fav-element"
     data-animation-id="<?php echo $data->id; ?>">
    <div class="animation"><img src="<?php echo Yii::app()->baseUrl; ?>/animations/<?php echo $data->file_drive; ?>"/>
    </div>
    <?php if ($data->public != 1 AND $data->user_facebook_id !== null): ?>
        <a href="#" class="delete-image-button" data-animation-id="<?php echo $data->id; ?>"></a>
    <?php endif; ?>
</div>

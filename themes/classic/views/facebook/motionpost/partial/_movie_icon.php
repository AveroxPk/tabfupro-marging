<div onclick="selectMovieIcon(jQuery(this), event); return false;" class="movie-icon-element"
     data-movie-icon-id="<?php echo $data->id; ?>">
    <div class="movie-icon"><img
            src="<?php echo Yii::app()->baseUrl; ?>/animations/<?php echo $data->server_filename; ?>"/></div>
    <?php if ($data->user_id != null): ?>
        <a href="#" class="delete-image-button" data-movie-icon-id="<?php echo $data->id; ?>"></a>
    <?php endif; ?>
</div>

<?php
	$analytics = $data->getAnalytics()->getSummary('all-time');
?>
<div class="post motion-post" data-id="<?= $data->id ?>">
    <div class="top-information">
        <div class="left">
            <div class="title">
                <?php echo $data->title; ?>
            </div>
            <div class="animation">
                <img
                    src="<?php echo $data->animation ? $data->animation->getAnimationSRC() : MotionPost::getMovieThumbnail($data->movie_url, $data->movie_icon_id); ?>"
                    alt="Animation"/>
            </div>
        </div>
        <div class="right">
			<a href="#" onclick="toogleFavorite(<?php echo $data->id; ?>); return false;" class="icofont icofont-favourite <?php echo $data->favorite == MotionPost::FAVORITE ? 'active' : ''; ?>" title="Favorite"></a>
			<a href="<?php echo $data->facebook_post_id != "" ? 'http://facebook.com/' . $data->facebook_post_id : "#"; ?>" class="fa fa-facebook-official <?php //echo $data->facebook_post_id != "" ? "active" : ""; ?>" target="_blank" title="View post on Facebook"></a>
			<a href="#" data-motion-post-id="<?php echo $data->id ?>" onclick="copymotionpost(<?php echo $data->id ?>)" class="icofont icofont-ui-copy copy" title="Copy"></a>
			   
        </div>
        <div class="clear"></div>
    </div>

    <?php if ($data->published == MotionPost::PUBLISHED): ?>
		<div class="status-area-container">
			<div class="status-area custom">
				<i class="fa fa-area-chart status-logo" aria-hidden="true"></i>
				<span>
					Analytics<br>
					<a href="#">+More Data</a>
				</span>						
			</div>
			
			<div class="analytics-fb">
				<div class="row"  title="Impressions count">
					<div class="fa fa-eye analytic-logo impressions" aria-hidden="true"></div>
					<div class="fa fa-mouse-pointer analytic-logo clicks" aria-hidden="true"></div>
					<div class="fa fa-share-alt analytic-logo shares" aria-hidden="true"></div>
					<div class="fa fa-envelope analytic-logo emails" aria-hidden="true"></div>
				</div>
				<div class="row" title="Clicks count">
					<div class="background-box impressions-analytics"><?= $analytics->getImpressions() ?></div>
					<div class="background-box clicks-analytics"><?= $analytics->getClicks() ?></div>
					<div class="background-box shares-analytics"><?= $analytics->getLikes() ?></div>
					<div class="background-box emails-analytics"><?= $analytics->getEmails() ?></div>
				</div>						
			</div>						
		</div>
		<div class="unpublished-element">
			<a href="#" class="button-delete" onclick="jQuery(this).closest('div').find('.remove-modal').slideToggle();return false;" title="Delete">Delete</a>
			<a href="#" class="refresh-icon" title="Refresh">Refresh</a>
			<div class="delete-tab-scheduled remove-modal">
				<span class="remove-modal-top">Are you sure you<br />want to Delete<br />this Tab?</span>
				<a href="#" onclick="deletemotionpost(<?php echo $data->id; ?>, event); return false;" class="button-tf-yes">Yes</a>
				<a href="#" class="button-tf-cancel" onclick="jQuery(this).closest('.remove-modal').slideToggle();return false;">Cancel</a>
				<span class="remove-modal-bottom">Note: Tab will NOT <br />be deleted from <br />Facebook</span>
			</div>
		</div>
	
	
    <?php else: ?>
					<div class="status-area-container">
						<div class="status-area">
							<i class="fa fa-circle-o status-logo"></i>
							<span>
								Status<br>
								<b>Schedule</b>
							</span>						
						</div>
						
						<div class="scheduled-element">
							<div class="date-scheduled">
								<i class="fa fa-calendar-minus-o" aria-hidden="true"></i>
								<span><?php echo date('d/m/Y', strtotime($data->schedule)); ?></span>
							</div>
							<div class="time-scheduled">
								<i class="fa fa-clock-o" aria-hidden="true"></i>
								<span><?php echo date('H:i', strtotime($data->schedule)); ?> <?php echo " UTC"; echo (date('Z')/3600)<0 ? (date('Z')/3600):"+".(date('Z')/3600); ?></span>
							</div>
						</div>
					</div>

					<div class="unpublished-element">
						<a href="#" class="button-delete" onclick="jQuery(this).closest('div').find('.remove-modal').slideToggle();return false;" title="Delete">Delete</a>
						<a href="#" onclick="editmotionpost(<?php echo $data->id; ?>); return false;" data-id="1" class="edit-option-button" title="Edit Options">Edit Options</a>
						
						<div class="delete-tab-before-schedule remove-modal">
							<span class="remove-modal-top">Are you sure you<br />want to Delete<br />this Tab?</span>
							<a href="#" onclick="deletemotionpost(<?php echo $data->id; ?>, event); return false;" class="button-tf-yes">Yes</a>
							<a href="#" class="button-tf-cancel" onclick="jQuery(this).closest('.remove-modal').slideToggle();return false;">Cancel</a>
							<span class="remove-modal-bottom">*Note: Schedule will <br />be cancelled and Tab <br />will NOT publish to <br />Facebook.</span>
						</div>
						
					</div>


<!--
		<div class="scheduled-element">
            <div class="date-scheduled"><?php //echo date('d/m/Y', strtotime($data->schedule)); ?></div>
            <div
                class="time-scheduled"><?php //echo date('H:i', strtotime($data->schedule)); ?> <?php //echo " UTC"; echo (date('Z') / 3600) < 0 ? (date('Z') / 3600) : "+" . (date('Z') / 3600); ?></div>
            <a href="#" onclick="editmotionpost(<?php //echo $data->id; ?>); return false;" data-id="1"
               class="edit-options-button" title="Edit options">Edit options</a>
        </div>-->
    <?php endif; ?>
<!--	
    <div class="delete-modal <?php //echo $data->published == MotionPost::PUBLISHED ? 'saved-templates' : 'scheduled'; ?>"
         style="display:none;">
        <a href="#" onclick="deletemotionpost(<?php //echo $data->id; ?>, event); return false;"
           class="button-tf-yes">Yes</a>
        <a href="#" class="button-tf-cancel"
           onclick="jQuery(this).closest('.delete-modal').slideToggle();return false;">Cancel</a>
    </div>

    <a href="#" class="button-delete" title="Delete"
       onclick="jQuery(this).closest('div').find('.delete-modal').slideToggle();return false;">Delete</a>
-->
</div>
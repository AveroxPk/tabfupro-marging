<div class="tabs-element select-tab-for-post" data-page-id="<?php echo $data->user_fanpage_id; ?>"
     data-tab-id="<?php echo $data->id; ?>">
    <div class="title"><?php echo $data->name; ?></div>
    <div class="thumb">
        <div class="image-placeholder"><img
                src='<?php echo $data->getTabIconUrl() != null ? $data->getTabIconUrl() : Yii::app()->baseUrl . '/template_resources/' . $data->getTemplateId() . '/preview.jpg'; ?>'/>
        </div>
    </div>
    <a href="https://www.facebook.com/<?php echo $data->userFanpage->facebook_page_id; ?>/?sk=app_<?php echo $data->application_id; ?>"
       target="_blank" class="view-on-facebook custom-btn selected text-center">View on <span><i class="fa fa-facebook" aria-hidden="true"></i><span></a>
</div>

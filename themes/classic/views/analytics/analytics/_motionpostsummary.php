<?php
	$analytics = $data->getAnalytics()->getSummary($days);
?>
<div>
    <span class="name">
        <a target="_blank" href="https://fb.com/<?= $data->facebook_post_id ?>"><?= $data->title ?></a>
    </span>
    <span class="value">
        <?= $analytics->getImpressions() ?>
    </span>
    <span class="value">
        <?= $analytics->getClicks() ?>
    </span>
    <span class="value">
        <?= $analytics->getLikes() ?>
    </span>
    <?php /*<span class="value">
        <?= $data->getAnalytics()->getSummary($days)->getShares() ?>
    </span>*/ ?>
    <span class="value">
        <?= $analytics->getEmails() ?>
    </span>
</div>

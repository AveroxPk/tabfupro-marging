<?php
	$analytics = $data->getAnalytics()->getSummary($days);
?>
<div>
    <span class="name">
        <a target="_blank" href="https://fb.com/<?= $data->userFanpage->facebook_page_id ?>/?sk=app_<?= $data->application_id ?>">
            <?= $data->name ?>
        </a>
    </span>
    <span class="value">
        <?= $analytics->getClicks() ?>
    </span>
    <span class="value">
        <?= $analytics->getImpressions() ?>
        <?php /*<?= $data->getAnalytics()->getSummary($days)->getLikes() ?>*/ ?>
    </span>
    <span class="value">
        <?= $analytics->getShares() ?>
    </span>
    <span class="value">
        <?= $analytics->getEmails() ?>
    </span>
</div>

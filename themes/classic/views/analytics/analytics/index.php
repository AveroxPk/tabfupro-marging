<div class="row tabfu-row">
    <div class="col-md-3 col-sm-5 sidestrip ">		
        <div class="page-boxes">
            <a href="#" onclick='switchfanpage("ALL"); jQuery(".topwrapper .selected").text("All fanpages"); return false;' class="page-box main">
                <div class="image-placeholder">
                    <img src="/images/motion-posts/user-page.png" />
                </div>
                <div class="page-box-title">
                    <span><?php echo $userfacebook->name." ".$userfacebook->surname?>'s Pages</span>
                </div>
            </a>

            
            <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider'=>$fanpages,
                    'itemView'=>'_fanpageView',
                    'template'=>'{items}{summary}{pager}',
                    'emptyText'=>false,
                    'summaryText'=>'{start}-{end} of {count}',
                    'pager'=>array(
                        'header'=>'',
                        'maxButtonCount'=>0
                    ),
                    'viewData'=>array(
                        'facebook'=>$facebook
                    ),
                ));
            ?>

        </div>
    </div> 
    <div class="col-md-8 col-sm-4 col-lg-9 home-page">
		<div class="template-picker">
			<?php
				Yii::app()->getModule('template');
			?>
			
			<div id="view-on-fb" class="fb-btn-add addfacebookaccount">
				<a href="#" target="_blank">
					<i class="fa fa-facebook fa-size"></i> <span>|</span> View On Facebook
				</a>
			</div>
			<div id="refresh-btn-area" class="fb-btn-add addfacebookaccount">
				<a id="refresh"><i class="fa fa-repeat"></i></a>
			</div>

        </div>	
		<div class="row filter-area">
			<div class="col-md-12">
				<div class="margin0 filter">
					<select id="change-range"  name="categoryFilter" class="categories-select-box-filter">
						<option data-days="0">Today</option>
						<option data-days="1">Yesterday</option>
						<option data-days="<?= date('N') - 1 ?>">This week</option>
						<option data-days="last-week" selected="selected">Last week</option>
						<option data-days="<?= date('j') - 1 ?>">This month</option>
						<option data-days="last-month">Last month</option>
						<option data-days="all-time">All time</option>
					</select>
				</div>
			</div>
		</div>
		
        <div class="row analytics-table" style="background:none;">
            <div class="table-data col-md-12">
                <div class="loader" style="position: absolute; margin-top: 175px; margin-left: 366px; overflow: hidden;display: block; z-index: 9999999999;">Wait</div>
				
				<div id="tabsChartContainer" class="col-md-6">
				
				</div>
                <div id="motionChartContainer" class="col-md-6">

                </div>
            </div>
        </div>
		
		
	</div>
</div>
		
<!--		
		<div class="row">
    <div class="col-md-3 col-sm-5 sidestrip" style="height: 600px; padding-top: 15px">
        <ul class="page-boxes">
            <li id="fanpage-dropdown">
                <?php
				/*
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider'=>$fanpages,
                    'itemView'=>'_fanpageView',
                    'template'=>'{items}{summary}{pager}',
                    'emptyText'=>false,
                    'summaryText'=>'{start}-{end} of {count}',
                    'pager'=>array(
                        'header'=>'',
                        'maxButtonCount'=>0
                    ),
                    'viewData'=>array(
                        'facebook'=>$facebook
                    ),
                ));
				*/
                ?>
            </li>
        </ul>
        <div class="current-selection" id="page-selection">

        </div>
    </div>
    <div class="col-md-9 col-sm-7">
        <h3 class="title">Displaying: Last week</h3>
        <div class="analytics-table">
            <select id="change-range">
                <option data-days="0">Today</option>
                <option data-days="1">Yesterday</option>
                <option data-days="<?= date('N') - 1 ?>">This week</option>
                <option data-days="last-week" selected="selected">Last week</option>
                <option data-days="<?= date('j') - 1 ?>">This month</option>
                <option data-days="last-month">Last month</option>
                <option data-days="all-time">All time</option>
            </select>
            <a id="refresh"></a>
            <div class="table-data">
                <div class="loader" style="position: absolute; margin-top: 175px; margin-left: 366px; overflow: hidden;display: block; z-index: 9999999999;">Wait</div>
                <div id="tabs-table" class="col-md-6">

                </div>
                <div id="motion-post-table" class="col-md-6">

                </div>
            </div>
        </div>
    </div>

</div>
-->
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.canvasjs.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/analytics.js"></script>

<link rel="stylesheet" href="/css/analytics.css">

<?php
$module     = Yii::app()->getModule('facebook');
$module->setAccessToken(Yii::app()->user->getState('access_token'));
try{
	$fanpageObj = Cacher::getFromCache('fanpage-' . $data->facebook_page_id, function() use($module, $data) {
		return $module->api('/'.$data->facebook_page_id);
	});
//    $pic = Cacher::getFromCache('fanpagepic-' . $data->facebook_page_id, function() use($module, $data) {
//       return $module->getFanpagePic($data->facebook_page_id);
//    });
	$pic = $module->getFanpagePic($data->facebook_page_id);
}catch (FacebookApiException $e) {
    Yii::app()->user->setFlash('notice', 'Please select facebook account.');
    $this->redirect(array('/facebook/facebook/index'));
}
$fanpage = $fanpageObj->getdecodedBody();

?>


    <a class="page-box page-item" data-pageid="<?= $data->facebook_page_id; ?>" data-id="<?= $data->id; ?>">
        <div class="image-placeholder"><img src="<?= $pic ?>" style="width: 100%"/></div>
        <div class="page-box-title">
            <span><?= $fanpage['name'] ?></span>
        </div>
    </a>

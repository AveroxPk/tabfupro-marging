<?php
$module     = Yii::app()->getModule('facebook');
$module->setAccessToken(Yii::app()->user->getState('access_token'));
$fanpage = Cacher::getFromCache('fanpage-' . $data->facebook_page_id, function() use($module, $data) {
    return $module->api('/'.$data->facebook_page_id, null, true);
});
echo $data->id;
?>
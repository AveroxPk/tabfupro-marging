/**
*
*  Base64 encode / decode
*  http://www.webtoolkit.info/
*
**/
 
var Base64 = {
 
	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
 
	// public method for encoding
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = Base64._utf8_encode(input);
 
		while (i < input.length) {
 
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
 
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
 
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
 
			output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
 
		}
 
		return output;
	},
 
	// public method for decoding
	decode : function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
		while (i < input.length) {
 
			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));
 
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
 
			output = output + String.fromCharCode(chr1);
 
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
 
		}
 
		output = Base64._utf8_decode(output);
 
		return output;
 
	},
 
	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	},
 
	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utftext.length ) {
 
			c = utftext.charCodeAt(i);
 
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 
		}
 
		return string;
	}
 
};
var addStyleFields;

$(document).ready(function() {
    
    $('h1,h2,h3,h4,h5,h6,h7').click(function(e) {
        var tagName = $(this)[0].tagName;
        
        if (ignoreElement(this)) {
            return;
        }
        
        $(this).replaceWith(generatePlaceholder('rawHtml', {
            "content": '<' + tagName + '>' + $(this).html() + '</' + tagName + '>'
        }));
        e.stopPropagation();
    });
    
    $('div').click(function(e) {
        if (ignoreElement(this)) {
            return;
        }
        
        $(this).html(generatePlaceholder('rawHtml', {
            "content": $(this).html()
        }));
        e.stopPropagation();
    });
    
    $('span').click(function(e) {
        if (ignoreElement(this)) {
            return;
        }
        
        $(this).html(generatePlaceholder('rawHtml', {
            "content": $(this).html()
        }));
        e.stopPropagation();
    });
    
    $('p').click(function(e) {
        if (ignoreElement(this)) {
            return;
        }
        
        $(this).replaceWith(generatePlaceholder('rawHtml', {
            "content": '<p>' + $(this).html() + '</p>'
        }));
        e.stopPropagation();
    });

    $('img').click(function(e) {
        $(this).replaceWith(generatePlaceholder('image', {
            "placeholderSrc": $(this).attr('src')
        }));
    });
    
    function ignoreElement(el)
    {
        return $('h1,h2,h3,h4,h5,h6,h7,span,div,img', el).length > 0 || $(el).text().trim().substring(0,8) == '{{_tabfu';
    }

    function generatePlaceholder(name, settings)
    {
       return "{{_tabfu:ImportField:" + name + ":" + Base64.encode(JSON.stringify(settings)) + "}}";
    }
    
    
    var hexDigits = new Array
            ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 

    //Function to convert hex format to a rgb color
    function rgb2hex(rgb) {
     rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
     return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }

    function hex(x) {
      return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
     }
    
    
    addStyleFields = function() {
        $('[data-admin-tag]').each(function() {
            var selector;
            
            if (this.hasAttribute('data-selector')) {
                selector = $(this).data('selector');
                $(this).removeAttr('data-selector');
            } else if (this.hasAttribute('id')) {
                selector = '#' + this.getAttribute('id');
            } else if (this.hasAttribute('class')) {
                var classes = this.getAttribute('class').split(' ');
                for (var i in classes) {
                    if ($('.' + classes[i]).length == 1) {
                        selector = '.' + classes[i];
                        break;
                    }
                }
                if (!selector) {
                    selector = classes.map(function(val){return "." + val;}).join("");
                }
            } else {
                alert("Couldn't generate selector for one of the HTML nodes.");
            }
            console.log($(this).data('admin-tag'));
            console.log($(this).css('background-color'));
        
            $(this).before(generatePlaceholder(
                'styleColor',
                {
                    "background": "1",
                    "selector": selector,
                    "adminTag": $(this).data('admin-tag'),
                    "color": rgb2hex($(selector).css('background-color'))
                }
            ));
        });
    };
    
});


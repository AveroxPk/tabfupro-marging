{
    "email": {
        "enabled": "1",
        "promptText": "Enter your Email below to gain instant access!",
        "confirmTitle": "ALL SET!",
        "confirmText": "Check your <strong>Inbox</strong> for our email which will arrive momentarily."
    },
    "share": {
        "promptText": "<strong>SHARE</strong> this page to activate your free download",
        "confirmText": "Thanks - Click this big button for instant access!"
    },
    "customCode": {}
}
$(document).ready(function() {
    
    $( "#modal-editor" ).draggable({ handle: "span.draggable" });
    
    // Choose template - scroll init
    if( !$("#editor-templates-list-content").hasClass("mCustomScrollbar") ) {
        $("#editor-templates-list-content").mCustomScrollbar({
            mouseWheel:true,
            alwaysShowScrollbar:1,
            autoDraggerLength:true,   
            advanced:{  
              updateOnBrowserResize:true,   
              updateOnContentResize:true   
            }
        });    
    } 

    // Menu - click action
    $(document).on('click', '.tabbed-modal-nav .editor-nav-link', function() {

        var open_tab = $(this).attr('href'),
            tab      = $(".tabbed-modal-content").find(open_tab);

        if(!tab.hasClass('active')) {

            // Hide active tab & remove class from active menu link
            $(".tabbed-modal-content > .tab").removeClass('active');
            $(".tabbed-modal-nav > .editor-nav-link").removeClass('active');

            // Select correct tab & menu link
            tab.addClass('active');
            $(this).addClass('active');

            // Edit template - scroll init
            if(tab.attr('id') == 'editor-tab-2') {
                if( !$("#editor-template-preview").hasClass("mCustomScrollbar") ) {
                    $("#editor-template-preview").mCustomScrollbar({
                        mouseWheel:true,
                        alwaysShowScrollbar:1,
                        autoDraggerLength:true,   
                        advanced:{  
                          updateOnBrowserResize:true,   
                          updateOnContentResize:true   
                        }
                    });    
                } 
            }

            // Publish - scroll init
            if(tab.attr('id') == 'editor-tab-3') {
                if( !$("#icon-library-scroll .content").hasClass("mCustomScrollbar") ) {
                    $("#icon-library-scroll .content").mCustomScrollbar({
                        mouseWheel:true,
                        alwaysShowScrollbar:1,
                        autoDraggerLength:true,   
                        advanced:{  
                          updateOnBrowserResize:true,   
                          updateOnContentResize:true   
                        }
                    });    
                } 
            }

            
        }

        return false;
    });
});
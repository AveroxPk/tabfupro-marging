$(document).ready(function() {
    
    $(document).mouseup(function (e) {
        
        var container = $(".modal-tf.message-dialog, .modal-tf.setting-dialog"),
            button = $(".messages.inbox");
        
        if(!button.is(e.target)){
            if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.addClass('hidden');
            }
        }  
    });
    
    
    $(document).on('click', '.setting-messages .messages', function() {
       
       // Show / hide modal (messages)
       var modal = $(this).parent().find(".modal-tf");
       
       if (modal.hasClass('hidden')) {
           modal.removeClass('hidden');
           
           if( !$(".modal-content-tf > .left-column-tf > .content-tf").hasClass("mCustomScrollbar") ) {
               $(".modal-content-tf > .left-column-tf > .content-tf ").mCustomScrollbar({
                    mouseWheel:true,
                    alwaysShowScrollbar:1,
                    autoDraggerLength:true,   
                    advanced:{  
                      updateOnBrowserResize:true,   
                      updateOnContentResize:true   
                    }
               });    
           }
           
           if( !$(".modal-content-tf > .right-column-tf > .content-tf > .message-scroll-content").hasClass("mCustomScrollbar") ) {
               $(".modal-content-tf > .right-column-tf > .content-tf > .message-scroll-content").mCustomScrollbar({
                    mouseWheel:true,
                    alwaysShowScrollbar:1,
                    autoDraggerLength:true,   
                    advanced:{  
                      updateOnBrowserResize:true,   
                      updateOnContentResize:true   
                    }
               });    
           }
            
       } else {
           modal.addClass('hidden');
       }
       
    });
    
    $(document).on('click', '.setting-button .setting', function() {
       
       // Show / hide modal (messages)
       var modal = $(this).parent().find(".modal-tf");
       
       if (modal.hasClass('hidden')) {
           modal.removeClass('hidden');
       } else {
           modal.addClass('hidden');
       }
    });
    
    $(document).on('mouseenter', '.setting-button .setting', function() {
       var modal = $(this).parent().find(".modal-tf");
       modal.removeClass('hidden');
    });
    
    $(document).on('mouseleave', '.modal-tf.message-dialog, .modal-tf.setting-dialog', function() {
       $(this).addClass('hidden');
    });
    
   
});
$(function () {
    // Navbar
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $(".navbar.navbar-fixed-top").addClass("scroll");
        } else {
            $(".navbar.navbar-fixed-top").removeClass("scroll");
        }      
    });

    // scroll back to top btn
    $('.scrolltop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 700);
        return false;
    });
    
    // scroll navigation functionality
    $('.scroller').click(function(){
    	var section = $($(this).data("section"));
    	var top = section.offset().top;
        $("html, body").animate({ scrollTop: top }, 700);
        return false;
    });
    
    $('.login-modal .close-window').click(function() {
        $(this).parents('.modal-overlay').hide();
        return false;
    });
    
    $('.login-popup').click(function() {
        $('.modal-overlay#login-step1').show();
        return false;
    });
    
    $('.login-email').click(function() {
        $('.modal-overlay#login-step1').hide();
        $('.modal-overlay#login-step2').show();
        return false;
    });
    
    jQuery(document).on('click', ".login-facebook", function() {
        FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          //FB.logout(function(response) {
                FB.login(function(response) {
                    if (response.authResponse) {
                      token = response.authResponse.accessToken;
                      console.log('Welcome!  Fetching your information.... ');
                      FB.api('/me', function(response) {
                        window.location.href='/facebookLogin/'+token;
                      }, {scope: 'email,manage_pages,publish_stream'});
                    } else {
                      console.log('User cancelled login or did not fully authorize.');
                    }
          //        });
          });
          
        } else if (response.status === 'not_authorized') {
                FB.login(function(response) {
                    if (response.authResponse) {
                      token = response.authResponse.accessToken;
                      console.log('Welcome!  Fetching your information.... ');
                      FB.api('/me', function(response) {
                        window.location.href='/facebookLogin/'+token;
                      });
                    } else {
                      console.log('User cancelled login or did not fully authorize.');
                    }
                  }, {scope: 'email,manage_pages,publish_stream'});
          
        } else {
          FB.login(function(response) {
                if (response.authResponse) {
                  token = response.authResponse.accessToken;
                  console.log('Welcome!  Fetching your information.... ');
                  FB.api('/me', function(response) {
                    window.location.href='/facebookLogin/'+token;
                  });
                } else {
                  console.log('User cancelled login or did not fully authorize.');
                }
              }, {scope: 'email,manage_pages,publish_stream'});
        }
       });
       return false;
    });

    // Index project carrousel
    $('.flexslider.slide-animation').flexslider({
        directionNav: false,
        animation: "slide",
        controlNav: true,
        pauseOnHover: true
    });
    
    $('.flexslider.fade-animation').flexslider({
        directionNav: false,
        animation: "fade",
        controlNav: true,
        pauseOnHover: true
    });

    $('.slides li.clone div.project a').attr('data-lightbox', '');
    
    $.listen('parsley:field:validate', function () {
        validateFront();
    });

    $('#contact-form .btn').on('click', function () {
        $('#contact-form').parsley().validate();
        if (validateFront()) {
            $.ajax({
                type: 'POST',
                url: $('#contact-form').attr('action'),
                data: $('#contact-form').serialize()
            }).
            done(function(data) {
                if (data == 'ok') {
                    $('#contact-form input, #contact-form textarea').val('');
                    jSuccess(
                        '<strong>Your message has been successfuly sent.</strong><br/>Thank you!',
                        {
                            autoHide : true,
                            clickOverlay : true,
                            MinWidth : 250,
                            TimeShown : 4000,
                            ShowTimeEffect : 200,
                            HideTimeEffect : 200,
                            LongTrip : 20,
                            HorizontalPosition : 'center',
                            VerticalPosition : 'center',
                            ShowOverlay : true,
                            ColorOverlay : '#000',
                            OpacityOverlay : 0.3
                        }
                    );
                }
                else {
                    jError(
                        'Sorry!<br/><strong>' + data + '</strong>',
                        {
                            autoHide : true,
                            clickOverlay : true,
                            MinWidth : 250,
                            TimeShown : 4000,
                            ShowTimeEffect : 200,
                            HideTimeEffect : 200,
                            LongTrip : 20,
                            HorizontalPosition : 'center',
                            VerticalPosition : 'center',
                            ShowOverlay : true,
                            ColorOverlay : '#000',
                            OpacityOverlay : 0.3
                        }
                    );
                }
            });
        }
        return false;
    });

    var validateFront = function () {
        var result = $('#contact-form').parsley().isValid();
        if (true === result) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
        return result;
    };
});
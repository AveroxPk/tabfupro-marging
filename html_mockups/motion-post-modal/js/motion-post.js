$(document).ready(function() {

    $(document).on('click', '#mp-nav > a', function() {
        return false;
    })

    // Init
    $("#mp-tab-1 .list-tabs > div.content").mCustomScrollbar({
        mouseWheel:true,
        alwaysShowScrollbar:1,
        autoDraggerLength:true,   
        advanced:{  
          updateOnBrowserResize:true,   
          updateOnContentResize:true   
        }
    });   

    // Menu - click action
    $(document).on('click', '#mp-nav .mp-nav-link', function() {

        var open_tab = $(this).attr('href'),
            tab      = $(".tabbed-modal-content").find(open_tab);

        if(!tab.hasClass('active')) {

            // Hide active tab & remove class from active menu link
            $(".tabbed-modal-content > .tab").removeClass('active');
            $(".tabbed-modal-nav > .mp-nav-link").removeClass('active');

            // Select correct tab & menu link
            tab.addClass('active');
            $(this).addClass('active');

            // Tab link - scroll init
            if(tab.attr('id') == 'mp-tab-1') {
                if( !$("#mp-tab-1 .list-tabs > div.content").hasClass("mCustomScrollbar") ) {
                    $("#mp-tab-1 .list-tabs > div.content").mCustomScrollbar({
                        mouseWheel:true,
                        alwaysShowScrollbar:1,
                        autoDraggerLength:true,   
                        advanced:{  
                          updateOnBrowserResize:true,   
                          updateOnContentResize:true   
                        }
                    });    
                } 
            }

            // Animations- scroll init
            if(tab.attr('id') == 'mp-tab-2') {
                if( !$(".animations-box > div.content").hasClass("mCustomScrollbar") ) {
                    $(".animations-box > div.content").mCustomScrollbar({
                        mouseWheel:true,
                        alwaysShowScrollbar:1,
                        autoDraggerLength:true,   
                        advanced:{  
                          updateOnBrowserResize:true,   
                          updateOnContentResize:true   
                        }
                    });    
                } 

                if( !$(".favorites-box > div.content").hasClass("mCustomScrollbar") ) {
                    $(".favorites-box > div.content").mCustomScrollbar({
                        mouseWheel:true,
                        alwaysShowScrollbar:1,
                        autoDraggerLength:true,   
                        advanced:{  
                          updateOnBrowserResize:true,   
                          updateOnContentResize:true   
                        }
                    });    
                } 
            }
            
        }

        return false;
    });

});
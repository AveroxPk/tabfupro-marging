<?php
ini_set('max_execution_time', 300);
$host = $_SERVER['HTTP_HOST'];

include dirname(__FILE__) . '/protected/components/StringHelper.php';

switch(true){
    case StringHelper::endsWith($host, 'postmotion.localhost'):
    case StringHelper::endsWith($host, 'tabfu.dev'):
    case StringHelper::endsWith($host, 'dev.customapp.procreative.eu'):
        ini_set('display_errors',1);
        error_reporting(E_ALL ^ E_NOTICE);

        // change the following paths if necessary
        $yii=dirname(__FILE__).'/../yii/framework/yii.php';
        $config=dirname(__FILE__).'/protected/config/' . ($host == 'tabfu.dev' ? 'tabfu.dev.php' : 'postmotion.localhost.php');

        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG',true);
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

        require_once($yii);
        Yii::createWebApplication($config)->run();
        break;

    case StringHelper::endsWith($host, 'customapp.procreative.eu'):
        ini_set('display_errors',1);
        error_reporting(E_ALL ^ E_NOTICE);

        // change the following paths if necessary
        $yii=dirname(__FILE__).'/../../../../framework/yii.php';
        $config=dirname(__FILE__).'/protected/config/customapp.procreative.eu.php';

        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG',true);
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

        require_once($yii);
        Yii::createWebApplication($config)->run();
        break;

    case StringHelper::endsWith($host, 'www.tabfu.net'):
    case StringHelper::endsWith($host, 'tabfu.net'):
    case StringHelper::endsWith($host, 'www.tabfu.online'):
    case StringHelper::endsWith($host, 'tabfu.pro'):
        error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);

        // change the following paths if necessary
        $yii='C:\\xampp\\htdocs\\framework\\yii.php';
        $config=dirname(__FILE__).'/protected/config/' . ($host == 'tabfu.pro' ? 'tabfu.pro.php' : 'tabfu.pro.php');


    // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

        require_once($yii);
        Yii::createWebApplication($config)->run();
        break;
    case StringHelper::endsWith($host, 'tabfu.local'):
        error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);

        // change the following paths if necessary
        $yii='C:\\xampp\\htdocs\\framework\\yii.php';
        $config=dirname(__FILE__).'/protected/config/' . ($host == 'tabfu.local' ? 'tabfu.local.php' : 'tabfu.local.php');


    // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

        require_once($yii);
        Yii::createWebApplication($config)->run();
        break;

    case StringHelper::endsWith($host, 'tabfu.online'):
        error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);

        // change the following paths if necessary
        $yii=dirname(__FILE__).'/../framework/yii.php';
        $config=dirname(__FILE__).'/protected/config/tabfu.online.php';

        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

        require_once($yii);
        Yii::createWebApplication($config)->run();
        break;

    case StringHelper::endsWith($host, 'tomek.localhost'):
        ini_set('display_errors',1);
        error_reporting(E_ALL);
        // change the following paths if necessary
        $yii='D:\\Projekty\\Procreative.eu\\yii-1.1.15.022a51\\framework\\yii.php';
        $config=dirname(__FILE__).'/protected/config/tomek.localhost.php';

        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG',true);
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

        require_once($yii);
        Yii::createWebApplication($config)->run();
        break;
    
    default:
        echo "There is no config for: ".$host;
        break;
}


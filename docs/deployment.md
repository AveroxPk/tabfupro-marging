# Important things to do on deployment process #

## 1. chmod on particular directories ##
- /uploads
- /animations
- /template_resources

Above directories should have sufficient rights to enable PHP to write in them. 

On Jenkins, it's strongly advised to exclude them from "lftp syncing" due to the fact that some data already uploaded by users could get lost on each deploy.
var globalpageid = 'ALL';
var globaltabid = null;
    
    /**
     * 
     * Fanpages
     * 
     */
    function switchfanpage(pageid){
        jQuery('.topwrapper .displaying span').text('All');
        if(jQuery('.sidestrip .page-box').index(jQuery('.page-box[data-pageid="'+pageid+'"]')) >= 2){

            jQuery('.sidestrip .page-box[data-pageid="'+pageid+'"]').insertBefore(jQuery('.sidestrip .page-box').eq(1));
            jQuery('.sidestrip .page-box[data-pageid="'+pageid+'"]').eq(1).remove();jQuery('.selectfanpage').each(function(){
                if(jQuery(this).attr('data-pageid') != pageid){
                    jQuery(this).closest('.page-box').removeClass('active');
                }else{
                    jQuery(this).closest('.page-box').addClass('active');
                }
            });
        }
        else if(jQuery('.sidestrip .page-box').index(jQuery('.page-box[data-pageid="'+pageid+'"]')) >= 1){
			jQuery('.sidestrip .page-box[data-pageid="'+pageid+'"]').addClass('active');
		}
        if(pageid !== null){
            jQuery.ajax({
                type: "GET",
                url: '/facebook/fanPage/showtabs',
                data: {"pageid" : pageid}
            })
            .done(function(data){
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                globalpageid = pageid;
                
                jQuery('.tab-view').fadeOut(function(){
                    jQuery('.tab-view').html(data);
                    jQuery('.tab-view').fadeIn(function(){
                       if(jQuery(window).width() < 770 ){
                           jQuery('html, body').animate({
                                scrollTop: jQuery('.tab-view').offset().top
                            }, 800, function(){

                            });
                        };
                    });
                });
                
                
                jQuery('.template-picker').fadeIn();
            });
            return true;
        }
    }
    
    function savetabtopage(pageid){
        if(pageid !== null){
             jQuery.ajax({
                type: "GET",
                url: '/publishtab',
                data: {"tabid" : globaltabid, "pageid":pageid},
                dataType: "json"
            })
            .done(function(data){
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                if(data.status == "ok" || data.status == "notice"){
                    switchfanpage(globalpageid);
                    jQuery('#publish-main-modal .publishing').fadeOut(function(){
                        jQuery('#publish-main-modal .published').fadeIn(function(){
                            jQuery('#publish-main-modal .ok-window-button').fadeOut(function(){
                                jQuery('#publish-main-modal .close-window-button').fadeIn(function(){
                                    jQuery("#publish-main-modal > .content > .publish-list > .content").mCustomScrollbar("update"); 
                                });
                            });
                        });
                    });
                }else{
                    displayFlashMessage(data.status, data.message);
                    resetPublishModal();
                }
            });
        }
    }
    
    /**
     * 
     * Tabs
     * 
     */
    function CreateTab(templateid){
        if(templateid !== null){
            jQuery.ajax({
                type: "GET",
                url: '/createtab',
                data: {"templateid":templateid},
                dataType: "json"
                
            })
            .done(function(data){
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                switchfanpage("ALL");
                displayFlashMessage(data.status, data.message);
            });
            return true;
        }
    }
    
    jQuery(document).on('click', '.removefromfavoritetab', function(e){
       e.preventDefault();
       tabid = jQuery(this).attr('data-id'); 
       jQuery.ajax({
            type: "GET",
            url: '/facebook/fanPage/RemoveTabfromFavorite',
            data: {"tabid" : tabid},
            dataType: "json"
        })
        .done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            switchfanpage(globalpageid);
            displayFlashMessage(data.status, data.message);
        });
        return false;
    });
    jQuery(document).on('click', '.addtofavoritetab', function(e){
       e.preventDefault();
       tabid = jQuery(this).attr('data-id'); 
       jQuery.ajax({
            type: "GET",
            url: '/facebook/fanPage/addtabtofavorite',
            data: {"tabid" : tabid},
            dataType: "json"
        })
        .done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            switchfanpage(globalpageid);
            displayFlashMessage(data.status, data.message);
        });
        return false;
    });
    
    jQuery(document).on('click', '.copy', function(e){
       e.preventDefault();
       tabid = jQuery(this).attr('data-id'); 
       jQuery.ajax({
            type: "GET",
            url: '/copytab',
            data: {"tabid" : tabid},
            dataType: "json"
        })
        .done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            switchfanpage(globalpageid);
            displayFlashMessage(data.status, data.message);
        });
        return false;
    });
    
    jQuery(document).on('click', '.button-delete', function(e){
        e.preventDefault();
        jQuery(this).closest('div').find('.remove-modal').slideToggle();
        return false;
    });
    
    jQuery(document).on('click', '.remove-modal .button-tf-yes', function(e){
        that = this;
        e.preventDefault();
        tabid = jQuery(this).attr('data-id');
        jQuery.ajax({
            type: "GET",
            url: '/deletetab',
            data: {"tabid" : tabid},
            dataType: "json"
        })
        .done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            jQuery(that).closest('.tab').fadeOut(800);
        });
        return false;
    });
    
    jQuery(document).on('click', '.remove-modal .button-tf-cancel', function(e){
       e.preventDefault();
       jQuery(this).closest('.remove-modal').slideUp();
       return false;
    });
    
    
    
    /**
     * 
     * Publish modal
     * 
     */
    
    function resetPublishModal(){
        
        jQuery('#publish-main-modal .published').hide();
        jQuery('#publish-main-modal #publish-modal-publish').show();
        jQuery('#publish-main-modal .close-window-button').hide();
        jQuery('#publish-main-modal .publishing').show();
        return true;
        
    }
    
    
    jQuery(document).on('click', '.publish-now-button', function(e){
        e.preventDefault();
        that = this;
        globaltabid = jQuery(this).attr('data-id');
        
        showFanpageSelectorModal("Publish your Tab", "publish-modal-publish", jQuery(this).attr('page-id'));
        
        // scroll to the top of the page
        $('html,body').animate({ scrollTop: 0 }, 'slow');

        return false;
    });
    
    /**
     * Shows up modal which makes user able to select fanpage.
     * @param {string} title Modal title in it's header
     * @param {string} buttonId Id of button which will fire specific event
     * @param {string} Facebook page ID which should be selected by default in the modal
     */
    function showFanpageSelectorModal(title, buttonId, selectedFanpageId) 
    {
        var $modal = jQuery('#publish-main-modal');
    
        //Init modal's title
        $modal.find('.header p').text(title);
    
        //Hide all buttons in modal
        $modal.find('.ok-window-button').hide();
        
        //Show particular button
        $modal.find('#' + buttonId).show();
        
        //Select fanpage
        jQuery("#publish-main-modal .page-box.active").removeClass('active');
        if (typeof selectedFanpageId !== 'undefined' && selectedFanpageId) {
            jQuery("#publish-main-modal .page-box[data-pageid='" + selectedFanpageId + "']").addClass('active');
        } else { //First known fanpage selected by default
            jQuery('#publish-main-modal .page-box').eq(0).addClass('active');
        }
        
        //Show modal
        $modal.css('z-index', 999999).fadeIn(function(){
            if( jQuery("#publish-main-modal > .content > .publish-list > .content").length ) {
                if( !jQuery("#publish-main-modal > .content > .publish-list > .content").hasClass("mCustomScrollbar") ) {
                    jQuery("#publish-main-modal > .content > .publish-list > .content").mCustomScrollbar({
                        mouseWheel:true,
                        alwaysShowScrollbar:1,
                        autoDraggerLength:true,   
                        advanced:{  
                          updateOnBrowserResize:true,   
                          updateOnContentResize:true   
                        }
                    });    
                }else{
                    jQuery("#publish-main-modal > .content > .publish-list > .content").mCustomScrollbar("update"); 
                } 
            }
        });
    
    }
    
    
    
    jQuery(document).on('click', '.unpublish', function(e){
        e.preventDefault();
        dataid = jQuery(this).attr('data-id');
        jQuery.ajax({
            type: "GET",
            url: '/unpublishtab',
            data: {"tabid" : dataid},
            dataType: "json"
        })
        .done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            switchfanpage(globalpageid);
            displayFlashMessage(data.status, data.message);
        });
        return false;
    });
    
    jQuery(document).on('click', '#publish-main-modal .close-button', function(e){
        e.preventDefault();
       jQuery('#publish-main-modal').fadeOut(function(){
           resetPublishModal();
       });
       return false;
    });
    
    
    jQuery(document).on('click', '#publish-main-modal .close-window-button', function(e){
        e.preventDefault();
        jQuery('#publish-main-modal').fadeOut(function(){
           resetPublishModal();
       });
       return false;
    });
    
    
    jQuery(document).on('click', '#publish-main-modal .page-box', function(e){
        e.preventDefault();
        that=this;
        jQuery('#publish-main-modal .page-box').each(function(){
            if(jQuery(this).attr('data-pageid') != jQuery(that).attr('data-pageid')){
                jQuery(this).closest('.page-box').removeClass('active');
            }else{
                jQuery(this).closest('.page-box').addClass('active');
            }
        });
        return false;
    });
    
    

    jQuery(document).on('click', '#publish-main-modal #publish-modal-publish', function(e){
        e.preventDefault();
        jQuery(this).fadeOut();
        savetabtopage(jQuery('#publish-main-modal .page-box.active').attr('data-pageid'));
        return false;
    });
    
    
    
    
    
    
    jQuery(document).on('click', '#mobile-link-modal .close-button', function(e){
        e.preventDefault();
       jQuery('#mobile-link-modal').fadeOut();
       return false;
    });
    
    
    jQuery(document).on('click', '#mobile-link-modal .close-window-button', function(e){
        e.preventDefault();
        jQuery('#mobile-link-modal').fadeOut();
        return false;
    });
    
    
    
    /**
    *
    *Calendar
    *  
    */
    
    function schedule(date){
        jQuery.ajax({
                type: "GET",
                url: '/schedule',
                data: {"tabid" : globaltabid, "date":date},
                dataType: "json"
            })
            .done(function(data){
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                  if(data.status == "ok"){
                    jQuery("#schedule-main-modal").fadeOut();
                    switchfanpage(globalpageid);
                    
                    //If tab wizard window is shown, we show up "congratulations" modal
                    if ($('#editor:visible').length > 0) {
                        tabWizard.close();
                        tabWizardCongratsModal.showScheduledModal(globaltabid);
                    } else {
                        displayFlashMessage("notice", data.message);
                    }
                  }else{
                      displayFlashMessage(data.status, data.message);
        
                  }
                
            });
    
    
    }
    
    jQuery('#calendar-widget').datepicker()
        .on('changeDate', function(ev){
          daypicked = ev.date.getDate();
          monthpicked = ev.date.getMonth();
          yearpicked = ev.date.getFullYear();
        });
        daypicked = jQuery('#calendar-widget').data("datepicker").date.getDate();
        monthpicked = jQuery('#calendar-widget').data("datepicker").date.getMonth();
        yearpicked = jQuery('#calendar-widget').data("datepicker").date.getFullYear();
    jQuery('#time-picker').timepicker({showMeridian: false});
    
    jQuery(document).on('click', '.schedule-button' ,function(e){
        e.preventDefault();
        globaltabid = jQuery(this).attr('data-id');
        jQuery("#schedule-main-modal").fadeIn();
        
        return false;
    });
    
    jQuery(document).on('click', '#schedule-main-modal .ok-window-button', function(e){
        e.preventDefault();
        current = new Date();
        time = jQuery('#time-picker').attr('value').split(':');
        
        selected = new Date(yearpicked, monthpicked, daypicked, time[0], time[1]);
        if(selected > current){
            schedule(selected);
        }else{
            displayFlashMessage('warning', 'Incorrect date');
        }
        return false;
    });
    
    jQuery(document).on('click', '#schedule-main-modal .close-button', function(e){
        e.preventDefault();
        jQuery(this).closest('#schedule-main-modal').fadeOut();
        return false;
    });
    
    /**
     * Gets currently selected Facebook Page id, or first Page's id if none of specific pages is selected.
     * @returns {string}
     */
    function getDefaultFanpage()
    {
        var 
            activePageBox = $('.selectfanpage.page-box.active'),
            firstPageBox
        ;
        if (activePageBox.length) {
            return activePageBox.data('pageid');
        }
        
        firstPageBox = $('.selectfanpage.page-box:first');
        return firstPageBox.length > 0 ? firstPageBox.data('pageid') : null;
    }
    
$(document).ready(function() {
   $(document).on('click', '.refresh-icon', function(e) {
       e.preventDefault();
       var $this    = $(this);
       var $tab     = $this.closest('div[class="post tab"]');
       var tabId    = $this.closest('.post').data('id');
       var $loader  = $tab.find('.loader');

      $loader.fadeToggle(500);
       $.get('/analytics/default/summary?type=page&id=' + tabId).done(function(data) {
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            $tab.find('.clicks-analytics').text(data.clicks);
            $tab.find('.likes-analytics').text(data.likes);
            $tab.find('.shares-analytics').text(data.shares);
            $tab.find('.emails-analytics').text(data.emails);
            $tab.find('.impressions-analytics').text(data.impressions);
           $loader.fadeToggle(500);
       });
       return false;
   });
});
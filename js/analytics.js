$(document).ready(function() {
    var $baseTable          = $('.analytics-table');
    var $tabsTable          = $('#tabs-table');
    var $motionPostTable    = $('#motion-post-table');
    var $fanpageIcon        = $('.page-item');
    var $viewOnFb           = $('#view-on-fb a');
    var $refreshButton      = $('#refresh');
    var $fanpageDropdown    = $('#fanpage-dropdown');
    var $pageSelection      = $('#page-selection');
    var $loader             = $('.loader')
    var toLoad              = 2;

    function reloadList(id, facebookId, title, days) {
        if (Ext.isEmpty(days)) {
            days = 'last-week';
        }
        toLoad = 2;
        $loader.fadeIn(100);
        $.get('/analytics/analytics/tabsummary?page=' + id + '&days=' + days).done(function(data) {
			
			var parse = data;
//				console.log(data);
			var charts = '';
			
			for(var k in parse)
			{
				charts = charts+ '<div id="'+parse[k]['id']+'" class="chart" style="width:97%; height:200px; margin-bottom:20px;"></div>';
			}
			$('#tabsChartContainer').html(charts);

			for(var i in parse){
				
				var tabName = parse[i]['TabName'];
				var jsonDataArr = parse[i]['actualData'];
//				console.log(parse[i]['TabName']);
				var chart = new CanvasJS.Chart(parse[i]['id'],
				{
					title:{
						text: tabName,
					},        		
					toolTip: {
						shared: true,		
					},
					axisX: {
						interval: 1,
					},

					data: [
					{
						type: "column",			
						indexLabelFontSize: 12,
						showInLegend: false,
						dataPoints:   jsonDataArr
							
						
					}
					]
				});
				chart.render();
			}
			
            $tabsTable.html(data);
            if (--toLoad == 0) {
                $loader.fadeOut(400);
            }
        });
		
        $.get('/analytics/analytics/motionpostsummary?page=' + id + '&days=' + days).done(function(data) {

			var parse = data;
			var charts = '';
			
			for(var k in parse)
			{
				charts = charts+ '<div id="'+parse[k]['id']+'" class="chart" style="width:97%; height:200px; margin-bottom:20px;"></div>';
			}
			$('#motionChartContainer').html(charts);

			for(var i in parse){
				
				var title = parse[i]['title'];
				var jsonDataArr = parse[i]['actualData'];
//				console.log(parse[i]['TabName']);
				var chart = new CanvasJS.Chart(parse[i]['id'],
				{
					title:{
						text: title,
					},        		
					toolTip: {
						shared: true,		
					},
					axisX: {
						interval: 1,
					},

					data: [
					{
						type: "column",			
						indexLabelFontSize: 12,
						showInLegend: false,
						dataPoints:   jsonDataArr
							
						
					}
					]
				});
				chart.render();
			}


			$motionPostTable.html(data);
            if (--toLoad == 0) {
                $loader.fadeOut(400);
            }
        });

        $viewOnFb.attr('href', '/analytics/analytics/redirectToFanpage?pageId=' + id);

        $baseTable.data('id', id);
        $baseTable.data('facebookId', facebookId);
        $baseTable.data('title', title);

        $pageSelection.text(title);
    }

    $fanpageDropdown.hover(function() {
        $pageSelection.hide();
    },
    function() {
        $pageSelection.show();
    });

    $(document).on('click', '.page-box', function() {
        var id              = $(this).data('id');
        var facebookId      = $(this).data('pageid');
        var title           = $(this).find('.page-box-title').text();
		var parentDiv = $(this).parents('.items');
		
		parentDiv.find('.page-box').removeClass('selected');
		$(this).addClass('selected');
		$('.topwrapper .selected span').text($(this).find('span').text().trim());
        $('.topwrapper .selected span').animate({
            left: 0,
            opacity: 1
        },{
            duration: 1300
        });
		
        if (id !== null) reloadList(id, facebookId, title);
    });

    $('select').change(function() {
        var days    = $(this).find('option:selected').data('days');
        var label   = $(this).find('option:selected').text();
        $('.title').text('Displaying: ' + label);
        reloadList($baseTable.data('id'), $baseTable.data('facebookId'), $baseTable.data('title'), days);
    });

    $(document).on('click', '#refresh', function() {
        var days = $('select').find('option:selected').data('days');
       reloadList($baseTable.data('id'), $baseTable.data('facebookId'), $baseTable.data('title'), days);
    });

    var firstPageId         = $fanpageIcon.first().data('id');
    var firstPageFacebookId  = $fanpageIcon.first().data('pageid');
    var firstPageTitle = $fanpageIcon.first().find('.page-box-title').text();
	
	
		
	$fanpageIcon.removeClass('selected');
	$fanpageIcon.first().addClass('selected');
	$('.topwrapper .selected span').text(firstPageTitle);
	$('.topwrapper .selected span').animate({
		left: 0,
		opacity: 1
	},{
		duration: 1300
	});
	
	
	
    if (firstPageId !== null) {
        reloadList(firstPageId, firstPageFacebookId, firstPageTitle);
    }

    if(document.location.hash.length > 0) {
        var id = document.location.hash.substr(1);
        $('a[data-pageid="' + id + '"]').click();
    }

});
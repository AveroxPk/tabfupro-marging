var tabWizardCongratsModal;
var newTabFacebookPageId;


$(document).ready(function() {
    var tickEffect = { //Tick bounce in effect settings
        effect: "bounce",
        duration: 500,
        times: 3
    };
    var currentTabId,
        $modal = $('.modal-congrats');
    
    
    tabWizardCongratsModal = {
        'showPublishedModal': function(tabId) {
            currentTabId = tabId;
            var $tick = $('<span class="fa fa-check-circle" style="color:#ddd;"></span>').hide();
            $('.modal-congrats')
                .removeClass('modal-congrats-scheduled')
                .find('.content h2')
                    .text('Published ')
                    .append($('<div style="display:inline-block"></div>').append($tick))
                .end()
                .show()
            ;
            $tick.show(tickEffect);
        },
        'showScheduledModal': function(tabId) {
            currentTabId = tabId;
            var $tick = $('<span class="fa fa-check-circle" style="color:#ddd;"></span>').hide();
            $('.modal-congrats')
                .addClass('modal-congrats-scheduled')
                .find('.content h2')
                    .text('Scheduled ')
                    .append($('<div style="display:inline-block"></div>').append($tick))
                .end()
                .show()
            ;
            $tick.show(tickEffect);
        }
    };
    
    //Modal init + attach event listeners to it's buttons
    $modal.easyModal({
            /*'zIndex': function() {
                return 52;
            },*/
            'overlayClose': false,
            'closeButtonClass': '.close-modal-button,.modal-congrats-close-buttom'
        })
        
        //Create new button
        .find('.custom-button.create-new')
            .click(function(e) {
                e.preventDefault();
                tabWizard.newTab(getDefaultFanpage());
                $modal.trigger('closeModal');
                return false;
            })
        .end()

        //Duplicate button
        .find('.custom-button.duplicate')
            .click(function(e) {
                e.preventDefault();
                tabid = jQuery(this).attr('data-id'); 
                jQuery.ajax({
                    type: "GET",
                    url: '/copytab',
                    data: {"tabid" : currentTabId},
                    dataType: "json"
                })
                .done(function(data){
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    newTabFacebookPageId = data.pageid;
                    switchfanpage(globalpageid);
                    displayFlashMessage(data.status, data.message);
                });
                return false;
            })
            
        .end().find('.custom-button.duplicatead')
            .click(function(e) {
                e.preventDefault();
                tabid = jQuery(this).attr('data-id'); 
                jQuery.ajax({
                    type: "GET",
                    url: '/copyad',
                    data: {"tabid" : currentTabId},
                    dataType: "json"
                })
                .done(function(data){
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    newTabFacebookPageId = data.pageid;
                    switchfanpage(globalpageid);
                    displayFlashMessage(data.status, data.message);
                });
                return false;
            })
        .end()

        //Analytics button
        .find('.custom-button.analytics')
            .click(function(e) {
                e.preventDefault();
                $modal.trigger('closeModal');
                document.location.href = '/analytics#' + newTabFacebookPageId;
                return false;
            });
    
    
});
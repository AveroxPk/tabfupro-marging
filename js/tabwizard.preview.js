/**
 * Created by Mateusz Lisik on 2014-06-16.
 */

$(document).ready(function() {

    tabWizard['openPreviewModal'] = function ($templateDiv) {
        var $modal              = $('.tp-modal');

        var templateId          = $templateDiv.data('id');
        var templateCategories  = $templateDiv.data('catnames');
        var templateName        = $templateDiv.find('.title').text();
        var templateDescription = $templateDiv.find('.description').text();

        $modal.find('#page-preview img').attr('src', '');
        $modal.find('#page-preview img').attr('src', '/template_resources/' + templateId + '/preview-big.png');
        $modal.find('.tp-modal-header h2').text(templateName);
        $modal.find('.description').text(templateDescription);
        $modal.find('.categories span').text(templateCategories);
        
        $modal.find('.edit-button').unbind();

        $modal.find('.edit-button').bind('click', function() {
            $templateDiv.find('.edit-button').click();
            $modal.trigger('closeModal');
            return false;
        });
        $modal.trigger('openModal');
    };


    var $modal = $('.tp-modal');
    $('.tp-modal .close-window-button, .tp-modal .close-button').click(function() {
        $modal.trigger('closeModal');
        return false;
    });

    $modal.easyModal({
        'zIndex': function() {
            return 52;
        },
//        'onOpen': function() {
//        },
        'overlayClose': false,
        'closeButtonClass': '.close-window-169'
    });
});
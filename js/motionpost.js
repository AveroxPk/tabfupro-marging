var globalpageid = 'ALL';
var globaltabid = null;
var motionpostObj = {
    "tab_id":null,
    "page_id": null,
    "animation": {
            "type": null,
            "value":null
        },
    "schedule":null,
    "caption":null,
    "title":null,
    "description":null,
    "message":null,
    "url":null
};
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


var $_GET = getUrlVars();



//Switch fanpage to show motion posts
function switchfanpage(pageid){
    
    if(pageid !== null){
        jQuery('.topwrapper .displaying span').text('All');
        if(jQuery('.page-box').index(jQuery('.page-box[data-pageid="'+pageid+'"]')) >= 2){

            jQuery('.page-box[data-pageid="'+pageid+'"]').insertBefore(jQuery('.page-box').eq(1));
            jQuery('.page-box[data-pageid="'+pageid+'"]').eq(1).remove();jQuery('.selectfanpage').each(function(){
                if(jQuery(this).attr('data-pageid') != pageid){
                    jQuery(this).closest('.page-box').removeClass('active');
                }else{
                    jQuery(this).closest('.page-box').addClass('active');
                }
            });
            
        }
        else if(jQuery('.page-box').index(jQuery('.page-box[data-pageid="'+pageid+'"]')) >= 1){
			jQuery('.page-box[data-pageid="'+pageid+'"]').addClass('active');
		}		
        jQuery.ajax({
            type: "GET",
            url: '/facebook/motionpost/showposts',
            data: {"pageid" : pageid}
        }).done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            globalpageid = pageid;
            jQuery('.selectfanpage').each(function(){
                if(jQuery(this).attr('data-pageid') != pageid){
                    jQuery(this).closest('.page-box').removeClass('active');
                }else{
                    jQuery(this).closest('.page-box').addClass('active');
                }
            });
            jQuery('.motion-posts-elements').fadeOut(function(){
                jQuery('.motion-posts-elements').html(data);
                jQuery('.motion-posts-elements').fadeIn(function(){
                   if(jQuery(window).width() < 770 ){
                       jQuery('html, body').animate({
                            scrollTop: jQuery('.tab-view').offset().top
                        }, 800, function(){

                        });
                    };
                });
            });

            
            jQuery('.template-picker').fadeIn();
        });
        return true;
    }
}


//Create new motion post
jQuery(document).on('click', '#create-new-post', function(e){
    e.preventDefault();
    motionpostObj.id = null;
    jQuery.ajax({
            type: "POST",
            url: '/createmotionpost-step-1',
            data: {"motionpost":motionpostObj}
        }).done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            jQuery('.motion-posts-editor').fadeOut(function(){
                jQuery('.motion-posts-editor').html(data);
                jQuery('.motion-posts-editor #mp-paste-url').val('');
                //Display main modal editor 
                $('html,body').animate({ scrollTop: 0 }, 'slow');
                displaymodal(jQuery('.tabbed-modal'));
                jQuery('.motion-posts-editor').fadeIn(function(){
                });
            });
        });
        return true;
});

//Copy motion post
function copymotionpost(id){
    jQuery.ajax({
            type: "POST",
            url: '/createmotionpost-step-1',
            data: {"motionpost":motionpostObj}
        }).done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            jQuery('.motion-posts-editor').fadeOut(function(){
                jQuery('.motion-posts-editor').html(data);
                //Display main modal editor 
                gotostep(6, id);
                displaymodal(jQuery('.tabbed-modal'));
                
                jQuery('.motion-posts-editor').fadeIn(function(){
                });
            });
        });
        return true;
}
//Edit motion post
function editmotionpost(id){
    jQuery.ajax({
            type: "POST",
            url: '/createmotionpost-step-1',
            data: {"motionpost":motionpostObj}
        }).done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            jQuery('.motion-posts-editor').fadeOut(function(){
                jQuery('.motion-posts-editor').html(data);
                //Display main modal editor 
                gotostep(7, id);
                displaymodal(jQuery('.tabbed-modal'));
                
                jQuery('.motion-posts-editor').fadeIn(function(){
                });
            });
        });
        return true;
}

//Delete motionpost

function deletemotionpost(id, ev){
    jQuery.ajax({
            type: "POST",
            url: '/deletemotionpost',
            data: {"id":id}
        }).done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            jQuery(ev.target).closest('.motion-post').fadeOut();
        });
        return true;
}

//Switch beteen editor steps
function gotostep(step, motionpostObj){
    jQuery.ajax({
            type: "POST",
            url: '/createmotionpost-step-'+step,
            data: {"motionpost":motionpostObj}
        }).done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            jQuery('.tabbed-modal').parent().html(data);
        });
        return true;
}


//NAVIGETION
jQuery(document).on('click', '#mp-cancel-button', function(e){
   e.preventDefault();
   closemodal(jQuery('.tabbed-modal'));
   return false;
});

jQuery(document).on('click', '#mp-next-button', function(e){
    e.preventDefault();
    step = jQuery(this).attr('data-open');
    if(step != 0){
        jQuery('#mp-next-button').attr("id", "loading-another-step");
        
        gotostep(step, motionpostObj);
    }else{
        closemodal(jQuery('.tabbed-modal'));
    }
    return false;
});

jQuery(document).on('click', '#mp-prev-button', function(e){
    e.preventDefault();
    step = jQuery(this).attr('data-open');
    if(step != 0){
        jQuery('#mp-prev-button').attr("id", "loading-another-step");
        gotostep(step, motionpostObj);
    }else{
        closemodal(jQuery('.tabbed-modal'));
    }
    return false;
});
jQuery(document).on('click', '#mp-nav #mp-tab-link', function(e){
    e.preventDefault();
    
    if(!jQuery(this).hasClass('active')){
        jQuery('#mp-nav #mp-tab-link').attr("id", "loading-another-step");
        gotostep(1, motionpostObj);
    }
    return false;
});

jQuery(document).on('click', '#mp-nav #mp-animation', function(e){
    e.preventDefault();
    
    
    if(!jQuery(this).hasClass('active')){
        jQuery('#mp-nav #mp-animation').attr("id", "loading-another-step");
        gotostep(2, motionpostObj);
    }
    return false;
});



jQuery(document).on('click', '#mp-nav #mp-message', function(e){
    e.preventDefault();
    
    if(!jQuery(this).hasClass('active')){
        jQuery('#mp-nav #mp-message').attr("id", "loading-another-step");
        gotostep(3, motionpostObj);
    }
    return false;
});

jQuery(document).on('click', '#mp-nav #mp-preview', function(e){
    e.preventDefault();
    
    if(!jQuery(this).hasClass('active')){
        jQuery('#mp-nav #mp-preview').attr("id", "loading-another-step");
        gotostep(4, motionpostObj);
    }
    return false;
});


//Search
jQuery('.search-background input[name="search"]').keyup(function(){
that = this;
if(jQuery(that).val()!= ''){
    jQuery('.motion-posts-elements .post').each(function(){
        if(jQuery(this).find('.title').text().toLowerCase().indexOf(jQuery(that).val().toLowerCase()) < 0){
            jQuery(this).hide();
        }else{
            jQuery(this).show();
        }

    });
}else{
    jQuery('.motion-posts-elements .post').each(function(){
        jQuery(this).show();
    });
}

});


//Open editor if there is Get parameter
if($_GET['new-post']!=null){
    jQuery('#create-new-post').click();
}


//Toogle favorite
function toogleFavorite(id){
    jQuery.ajax({
        type: "POST",
        url: "/tooglefavoritemotionpost",
        data: {"id":id}
    }).done(function(data){
        if (data == 'logged-out') {
            window.location.href = '/login';
        }
        switchfanpage(globalpageid); 
    });
}

$(document).ready(function() {
    $(document).on('click', '.refresh-icon', function(e) {
        e.preventDefault();
        var $this    = $(this);
        var $tab     = $this.closest('div[class="post motion-post"]');
        var tabId    = $this.closest('.post').data('id');
        var $loader  = $tab.find('.loader');

        $loader.fadeToggle(500);
        $.get('/analytics/default/summary?type=motion-post&id=' + tabId).done(function(data) {
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            $tab.find('.clicks-analytics').text(data.clicks);
            $tab.find('.likes-analytics').text(data.likes);
            $tab.find('.shares-analytics').text(data.shares);
            $tab.find('.emails-analytics').text(data.emails);
            $tab.find('.impressions-analytics').text(data.impressions);
            $loader.fadeToggle(500);
        });
        return false;
    });
});
(function ($) {
    var methods;
    
    methods = {
        'init': function(options) {
            this.each(function() {
                var chooser = $(this),
                    chooserId = chooser.attr('id');
                
                $('.' + chooserId + '-select-template').on('click', function(e) {
                    options.templateSelectedJSCallback.apply(chooser, [$(this).data('template-id')]);
                    $("#" + chooserId + "_modal").dialog("close");
                    e.preventDefault();
                    return false;
                });
                
            });
        },
        
        'show': function() {
            $("#" + this.attr('id') + "_modal").dialog("open");
        }
    };
    
    $.fn.templateChooser = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.templateChooser');
			return false;
		}
	};
    
})(jQuery);
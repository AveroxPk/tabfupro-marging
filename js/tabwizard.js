var tabWizard;

$(document).ready(function() { 
    var tabManager,
        templateSelector,
        tabCreator,
        currentTabId,
        publishFanpageId,
        tabPublished,
        tabEditor,
        copyHtmlClipboardButtonInitialized,
        currentTabHasDynamicContent,
        gatesConfig,
        $nextButton = $('#editor-next-button'),
        $backButton = $('#editor-back-button')
    ;
    
    
    $.fn.multiline = function(text){
        this.text(text);
        this.html(this.html().replace(/\n/g,'<br/>'));
        return this;
    };
    
    function refreshTabList()
    {
        switchfanpage(globalpageid);
    }
    
    function assertTabName(guideToInput) 
    {
        var $input;

        if (typeof guideToInput === 'undefined') {
            guideToInput = true;
        }

        if (tabEditor.getCurrentTabName().trim().length === 0) {
            if (guideToInput) {
                $input = $('#EditTemplateTab .editor-template-title input');         editor-header-choose-title   
                tabManager.switchToEditTemplateTab();
                displayFlashMessage('warning', "Please provide template title first.", 'top:' + ($input.offset().top + $input.height() + 10).toString() + 'px');
                $input.focus();
            }

            return false;
        }

        return true;
    }
    
    (function() {
        function initTabWizard() 
        {
            $('.editor-save-details').hide();
            $('#editor-nav').show();
            setTimeout(function() {
                if( !$("#editor-templates-list-content").hasClass("mCustomScrollbar") ) {
                    $("#editor-templates-list-content").mCustomScrollbar({
                        mouseWheel:true,
                        alwaysShowScrollbar:1,
                        autoDraggerLength:true,   
                        advanced:{  
                          updateOnBrowserResize:true,   
                          updateOnContentResize:true   
                        }
                    });    
                } 
            },1);
            
            $('#editor-changes-saved').hide();
        }
        
        function initCopyHtmlClipboardButton()
        {
            if (copyHtmlClipboardButtonInitialized) {
                return;
            }
            
            $('.copy-button-clipboad').zclip({
                path: '/js/clipboard/jquery.clipboard.swf',
                copy: function() {
                    return $('.template-html-editor-content').text();
                },
                /*beforeCopy:function(){
                    jQuery('#mobile-link-modal #copy-to-clipboard').addClass('active');
                },*/
                afterCopy:function(){
                    $('.template-html-editor-nav i.tick-44').show();
                    //jQuery('#mobile-link-modal #copy-to-clipboard').zclip('remove');

                }
            });
            copyHtmlClipboardButtonInitialized = true;
        }
        
        tabWizard = {
            //Wizard starters
            'editTab': function(tabId) {
                $.blockUI();
                
                initTabWizard();
                currentTabId = tabId;

                tabEditor.initTab(function(){
                    tabManager.switchToEditTemplateTab();
                    $('#editor').trigger('openModal');
                    $.unblockUI();
                }); 
                
                // scroll to the top of the page
                $('html,body').animate({ scrollTop: 0 }, 'slow');
            },

            'newTab': function(fanpageId) {
                if (fanpageId === null) {
                    displayFlashMessage('error', "You don't have any Facebook fanpages for current Facebook account.");
                    return;
                }
                
                initTabWizard();
                currentTabId = null;
                publishFanpageId = fanpageId;
                tabManager.switchToChooseTemplateTab();
                $('#editor-changes-not-published').hide();
                $('#editor').trigger('openModal');
            },
            
            //Miscellaneous actions
            'getCurrentTabId': function() {
                return currentTabId;
            },
            
            'previewOnFacebook': function() {
                window.open('/template/pageEditor/previewOnFacebook/tabId/' + currentTabId, '_blank');
            },
            
            'close': function() {
                $("#editor").trigger('closeModal');
            },
            
            //Publishing actions
            'setPublishFanpage': function(fanpageId) {
                publishFanpageId = fanpageId;
                
                //This is very hacky and dependent on view where TabWizard widget 
                //is being placed. That's because there are queries to Facebook 
                //API from views (probably we'll have to refactor these views in future)
                //although, even this solution is ugly, should work for now.
                function determineFanpageNameById(fanpageId)
                {
                    return $(".page-box[data-pageid='" + fanpageId + "'] .page-box-title").first().text().trim();
                }
                
                $('#editor-publish-fanpage-name').text(determineFanpageNameById(fanpageId));
                
            },
            
            'publishAtFacebook': function() {
                if (!assertTabName()) {
                    return;
                }
                
                $.blockUI();
                newTabFacebookPageId = publishFanpageId;
                //savetabtopage(publishFanpageId);
                $.ajax({
                    url: "/facebook/fanPage/publishtab/tabid/" + currentTabId + "/pageid/" + publishFanpageId,
                    dataType: 'json'
                }).done(function(data) {                    
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    //Error?
                    if (typeof data.success === "undefined" || data.success !== 1) {
                        displayFlashMessage(data.status, data.message);
                        return;
                    }
                    
                    tabPublished = true;
                    
                    tabWizard.close();    
                    tabWizardCongratsModal.showPublishedModal(currentTabId);
                    
                    //refreshTabList();
                    
                }).fail(function() {
                    displayFlashMessage('warning', "Couldn't publish tab. Please try again.");
                }).always(function(data) {
                    $.unblockUI();
                });
            },
            
            'publishChanges': function() {
                if (!assertTabName()) {
                    return;
                }
                
                $.blockUI();
                
                function publishFailure()
                {
                    displayFlashMessage('warning', "Couldn't publish template changes. Please try again.");
                }
                
                $.ajax({
                    url: "/template/pageEditor/publish/tabId/" + currentTabId
                }).done(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    if (data != "OK") {
                        publishFailure();
                        return;
                    }
                    
                    $.ajax({
                        url: "/facebook/fanPage/republishtab/tabid/" + currentTabId
                    }).done(function(data) {
                        $('#editor-changes-not-published').hide();
                        tabWizard.close();
                    }).fail(function() {
                        publishFailure();
                    }).always(function() {
                        $.unblockUI();
                    });
                    
                }).fail(function() {
                    publishFailure();
                });
            },
            
            'updateGatesConfig': function(config) {
                //$.blockUI();
                gatesConfig = config;
                
                //$('#page-preview').one('load', function() {$.unblockUI();});
                //Gates fields must be rendered again, so we reload whole page preview to be sure
                $('#page-preview').contents()[0].location.reload(true);
                
                var enabledGates = [];
                var gatesDisplayNames = {
                    //'like': 'Like gate',
                    'share': 'Share gate',
                    'email': 'Email gate',
                    'customCode': 'Custom Code gate'
                };
                for (var gate in gatesConfig) {
                    if ('enabled' in gatesConfig[gate] && gatesConfig[gate]['enabled'] == '1') {
                        enabledGates.push(gatesDisplayNames[gate]);
                    }
                }
                
                $('#gates-sum-up').text(enabledGates.join(', '));
            },
            
            'showCopyHtmlModal': function() {
                $.blockUI();

                $.ajax('template/pageEditor/pagePreview/tabId/' + currentTabId + '/adminMode/0')
                    .done(function(data) {
                        if (data == 'logged-out') {
                            window.location.href = '/login';
                        }
                        var $content = $('.template-html-editor-content');
                
                        //Prepare modal
                        $content.multiline(data);
                        $('.template-html-editor-nav i.tick-44').css('visibility', 'hidden');
                        
                        //Open modal
                        $('#template-html-editor').trigger('openModal');
                        
                        //Init clipboard support
                        //By some reason clipboard plugin doesn't work when 
                        //initialised before showing the modal
                        initCopyHtmlClipboardButton(); 
                        
                        //Init scrollbar
                        //if ( !$content.hasClass("mCustomScrollbar") ) {
                            $content.mCustomScrollbar({
                                mouseWheel:true,
                                alwaysShowScrollbar:1,
                                autoDraggerLength:true,   
                                advanced:{  
                                  updateOnBrowserResize:true,   
                                  updateOnContentResize:true   
                                }
                            });    
                        //}
                        
                        if (currentTabHasDynamicContent) {
                            displayFlashMessage('warning', 'This template has dynamic elements which will not function properly on static HTML page.');
                        }
                        
                    })
                    .always(function() {
                        $.unblockUI();
                    })
                ;
            },
            'showTemplatePreviewModal' : function() {
                $('#template-preview').trigger('openModal');
            }
        };
        
    })();
    
    $('#editor').easyModal({
        //Manual value because of bug when mixing with jquery.blockUi
        'zIndex': function() {
            return 50;
        },
        'overlayClose': false,
        'closeButtonClass': '#editor-close',
        'onClose': function() {
            if (tabWizard.getCurrentTabId() !== null && !assertTabName(false)) {
                $('.editor-save-details').hide();
                $('#editor-header-choose-title').show();
                return false;
            }
            refreshTabList();            
            return true;
        }
    });
    
    $('#template-html-editor').easyModal({
        'zIndex': function() {
            return 51;
        },
        'overlayClose': false,
        'closeButtonClass': '.close-link,.close-window-button'
    });
    
    
    //Some modals should be draggable by their corner icon (span.draggable)
    $( ".modal-editor" ).draggable({ handle: "span.draggable" });
    
    //Template selection logic
    (function() {
        function reactForTemplateSelectionChange() 
        {
            if (isTemplateSelected()) {
                $nextButton.removeClass('disabled');
            } else {
                $nextButton.addClass('disabled');
            }
        }

        function isTemplateSelected() 
        {    
            return $('.template-box.active').length > 0;
        }
        
        templateSelector = {
            'selectTemplateByBox': function(boxElement) {
                $('.template-box.active').removeClass('active');
                boxElement.parents('.template-box').addClass('active');
                reactForTemplateSelectionChange();
            },
            'clearSelection': function() {
                $('.template-box.active').removeClass('active');
                reactForTemplateSelectionChange();
            }
        };
        
    })();
    
    
    //Tabs switching logic
    (function() {
        function unactivateTab() {
            // Hide active tab & remove class from active menu link
            $(".tabbed-modal-content > .tab").removeClass('active');
            $(".tabbed-modal-nav > .editor-nav-link").removeClass('active');
        }
        function getCurrentTabId() {
            return $('.tab.active').attr('id');
        }
                
        tabManager = {
            'switchToChooseTemplateTab': function() {
                //Can't change template once editing some existing tab
                if (currentTabId !== null) {
                    displayFlashMessage('warning', "Template cannot be changed in existing tab. Please create a new tab if you want to use another template.");
                    return;
                }
                
                unactivateTab();
                $('#editor-choose-template').addClass('active');
                $('#ChooseTemplateTab').addClass('active');
                $nextButton.show();
                $backButton.hide();
                templateSelector.clearSelection();
            },
            'switchToEditTemplateTab': function() {
                if (currentTabId === null) {
                    displayFlashMessage('warning', 'Please choose template first.');
                    return;
                }
                                
                unactivateTab();
				
                $('#editor-edit-template').addClass('active');
                $('#editor-edit-template').parents('.steps').addClass('active');
				
                $('#EditTemplateTab').addClass('active');
				
                $nextButton.show().removeClass('disabled');
				
                $backButton.hide();
            },
            'switchToPublishTab': function() {
                if (currentTabId === null) {
                    displayFlashMessage('warning', "Please choose tab template before publishing it.");
                    return;
                }
                
                unactivateTab();
                $('#editor-publish').addClass('active');
                $('#editor-publish').parents('.steps').addClass('active');
				
                $('#PublishTab').addClass('active');
                $nextButton.hide().removeClass('disabled');
                $backButton.show();
            },
            'nextTab': function() {
                switch (getCurrentTabId()) {
                    case 'ChooseTemplateTab':
                        tabManager.switchToEditTemplateTab();
                        break;
                    case 'EditTemplateTab':
                        tabManager.switchToPublishTab();
                        break;
                }
            },
            'prevTab': function() {
                switch (getCurrentTabId()) { 
                    case 'PublishTab':
                        tabManager.switchToEditTemplateTab();
                        break;
                }
            },
            
            'changeTemplateName': function(newName, successCallback, failCallback) {
                //alert(newName);
                
                $.ajax({
                    url: '/facebook/fanPage/changeTabName/id/' + currentTabId,
                    dataType: 'json',
                    type: 'POST',
                    data: {"name": newName}
                }).done(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    if (typeof data.success === 'undefined' || !data.success) {
                        if (typeof failCallback !== 'undefined') {
                            failCallback();
                        }
                    }
                    if (typeof successCallback !== 'undefined') {
                        successCallback();
                    }
                }).fail(function() {
                    if (typeof failCallback !== 'undefined') {
                        failCallback();
                    }
                });
                
            }
        };
    })();
    
    (function() {
        tabCreator = {
            /**
             * Creates tab based on provided template and jumps to editor 
             * after success.
             * @param {number} templateId
             */
            'createTabForTemplate': function(templateId, fanpageId) {
                $.blockUI(); 
                $.ajax({
                    url: '/facebook/fanPage/createTab?templateid=' + templateId + '&fanpageid=' + fanpageId,
                    dataType: 'json'
                })
                    .done(function(data) {
                        if (data == 'logged-out') {
                            window.location.href = '/login';
                        }
						
                        if (data.error) {
                            displayFlashMessage(data.status, data.message);
							$.unblockUI();
                            return;
                        }
                        
                        currentTabId = data.tabId;
                        tabEditor.initTab(function(){
                            tabManager.switchToEditTemplateTab();
                            $.unblockUI();
                        }); //Initialize editor and switch to edit template tab after init
                        
                    })
                    .fail(function() {
                        displayFlashMessage('warning', 'Connection error. Please try again later.');
                        //$.unblockUI(); 
                    })
                ;
            }
        };
    })();
    
    (function() {
        var currentFieldId;
        
        function updateStyleField($field, fieldAttributes) 
        {
                var $associatedElement;
                
                if (typeof fieldAttributes === 'undefined') {
                    fieldAttributes = $associatedElement.data('attributes');
                }

                $associatedElement = $("[data-admin-tag='" + fieldAttributes.adminTag + "']", $field.parents('body'));

                if (fieldAttributes.visible) {
                    $associatedElement.children('.hidden-field-cover').remove();
                } else {
                    $associatedElement.css('position', 'relative').append($('<div class="hidden-field-cover"></div>'));
                }

        }
        
        tabEditor = {
            'initTab': function(callback) {
                var $iframe = $('#page-preview'),
                    tabId = currentTabId
                ;
                
                //$.blockUI();
                
                $('.modal-editor:visible').hide();
                
                function initIframe()
                {
                    var $iframeContainer = $("#editor-template-preview");

                    

                    
                    //$iframe.attr('src', 'about:blank');

                    
                    $iframe
                        .unbind('load')
                        .load(function() {
                            //Init iframe styles
                            $('#iframe-style').clone().appendTo($iframe.contents().find('head'));

                            //Init iframe scripts
                            $($iframe.contents())
                                .on('click', '.field-element', function(e) {
                                    if ($(this).find('> .autoscroll-field').length <= 0) {
                                        tabEditor.selectElement(this);
                                        tabEditor.editField(this);
                                        e.stopPropagation(); //Required in case elements should be selectable inside other selectable elements
                                        return false;
                                    }
                                })
                                .on('click', '.selectable-element,[data-admin-tag]', function(e) {
                                    var $field = $(this).parents('body').find(".field-element[data-tag='" + $(this).data('admin-tag') + "']");
                                    tabEditor.selectElement(this);
                                    tabEditor.editField($field);
                                    e.stopPropagation(); //Required in case elements should be selectable inside other selectable elements
                                    return false;
                                })
                                // slider dots clicking
                                .on('click', '#slider .dots li', function(e) {
                                    if (typeof unsliderData != "undefined" && unsliderData != null) {
                                        unsliderData.move($(this).index());
                                    }
                                    e.stopPropagation(); //Required in case elements should be selectable inside other selectable elements
                                    return false;
                                })
                                
                                //Prevent link clicking, except <a> with class attribute which might 
                                //have some functionality which should work also in the editor, 
                                //namely slider arrows
                                .on('click', 'a:not([class])', function(e) { 
                                    if ($(this).parents('.field-element').length > 0) {
                                        $(this).parents('.field-element').trigger('click');
                                    }
                                    e.preventDefault();
                                    return false;
                                })
                                
                                .find('body').css('overflow', 'hidden')
                            ;

                            



                            //modal.find('.page-editor').show();
                            //modal.find('.editor-loading').hide();
                            

                            //Stretch iframe to full height
                            $iframe.css('height', $iframe.contents()[0].body.scrollHeight + "px");
                            
                            if ( !$iframeContainer.hasClass("mCustomScrollbar") ) {
                                //Init scrollbar
                                $iframeContainer.mCustomScrollbar({
                                    mouseWheel:true,
                                    alwaysShowScrollbar:1,
                                    autoDraggerLength:true,   
                                    advanced:{  
                                      updateOnBrowserResize:true,   
                                      updateOnContentResize:true   
                                    }
                                });    
                            } else {
                                
                                //Weird behavior but fixes bug with scrollbar after second iframe initialization in FireFox
                                setTimeout(function() {
                                    $iframe.css('height', $iframe.contents()[0].body.scrollHeight + "px");
                                    $iframeContainer.mCustomScrollbar("update");
                                }, 1000);
                                
                                //$("#editor-template-preview").mCustomScrollbar("update")
                                
                            }
                            
                            setTimeout(function() {
                                var maxGateLeft = $iframe.width() - 89,
                                    maxGateTop = $iframe.height() - 50
                                ;
                                
                                //Init "crossed eye" cover on every hidden container element
                                $('.field-element', $iframe.contents()).each(function() {
                                    var $associatedElement,
                                        attributes = $(this).data('attributes')
                                    ;

                                    if (!attributes.visible) {
                                        $associatedElement = $("[data-admin-tag='" + attributes.adminTag + "']", $iframe.contents());
                                        $associatedElement.css('position', 'relative').append($('<div class="hidden-field-cover"></div>'));
                                    }

                                });
                            
                                //Add gates buttons (with plus sign and lock icon emblem)
                                $('.field-element[data-gate-field]', $iframe.contents()).each(function() {
                                    var atLeastOneGateEnabled,
                                        $addGateButton,
                                        calculatedGateLeft,
                                        calculatedGateTop
                                    ;

                                    atLeastOneGateEnabled = 
                                        ('like' in gatesConfig && 'enabled' in gatesConfig.like && gatesConfig.like.enabled == '1') ||
                                        ('share' in gatesConfig && 'enabled' in gatesConfig.share && gatesConfig.share.enabled == '1') ||
                                        ('email' in gatesConfig && 'enabled' in gatesConfig.email && gatesConfig.email.enabled == '1') ||
                                        ('customCode' in gatesConfig && 'enabled' in gatesConfig.customCode && gatesConfig.customCode.enabled == '1')
                                    ;

                                    $addGateButton = $('<img src="/images/gates/gate-add-icon.png" class="add-gate-button" alt="Click here to add/configure gate" />');
                                    $addGateButton.attr('src', atLeastOneGateEnabled ? '/images/gates/gate-added-icon.png' : '/images/gates/gate-add-icon.png');
                                    $addGateButton.attr('data-gate-id', $(this).attr('id'));

                                    //If gate position gets outside an iframe, correct it's position by fitting it in iframe
                                    calculatedGateLeft = ($(this).offset().left + $(this).width() - 50);
                                    if (calculatedGateLeft > maxGateLeft) {
                                        calculatedGateLeft = maxGateLeft;
                                    }
                                    
                                    calculatedGateTop = ($(this).offset().top + $(this).height() - 25);
                                    if (calculatedGateTop > maxGateTop) {
                                        calculatedGateTop = maxGateTop;
                                    }
                                    
                                    $addGateButton
                                        .css('position', 'absolute')
                                        .css('top', calculatedGateTop + 'px')
                                        .css('left', calculatedGateLeft + 'px')
                                        .css('cursor', 'pointer')
                                        .click(function() {
                                            gateWizard.showConfigurationModal(currentTabId, gatesConfig);
                                        })
                                    ;
                                    $iframe.contents().find('body').append($addGateButton);
                                });
                            }, 1500);
                            
                            callback();
                        })
                        .attr('src', '/template/pageEditor/pagePreview/tabId/' + tabId)
                    ;
                }
                

                //We need some metadata about the tab first
                $.ajax({
                    "url": "/template/pageEditor/pageInfo/tabId/" + tabId,
                    "dataType": "json"
                }).done(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }

                    //setup UI basing on FB tab state
                    var chooseFanpageButton = $('.publishing-tab-to .header .choose-other-page-button');
                    
                    tabPublished = data.tabPublished;
                    if (data.changesUnpublished && data.tabPublished) {
                        $('#editor-changes-not-published').show();
                    } else {
                        $('#editor-changes-not-published').hide();
                    }

                    if (data.tabPublished) {
                        $('.publish-now-big, .schedule-big').hide();
                        $('.publish-changes-big').show();
                        chooseFanpageButton.hide();
                    } else {
                        $('.publish-changes-big').hide();
                        $('.publish-now-big, .schedule-big').show();
                        chooseFanpageButton.show();
                    }
                    
                    $('#EditTemplateTab .editor-template-title input').val(data.tabName !== 'Untitled' ? data.tabName : '');
                    
                    tabWizard.setPublishFanpage(data.facebookPageId);
                    
                    currentTabHasDynamicContent = data.dynamicContent;
                    
                    gatesConfig = data.gatesConfig;
                    
                    $('#autoresponders-sum-up').text(data.autoresponders);
                    $('#autoresponders-lists-sum-up').text(data.autorespondersLists);
                    $('#gates-sum-up').text(data.enabledGates);
                    
                    //Init tab icon
                    $('#PublishTab .select-button').css('background-image', data.tabIconUrl ? ("url('" + data.tabIconUrl + "')") : '');
//                    $('#PublishTab .select-button').css('text-indent', '-9999px');

                    //Link HTML download button to action that generates HTML file for specific tab
                    $('.download-html-editor').attr('href', '/template/pageEditor/pagePreview/tabId/' + currentTabId + '/adminMode/0/download/1');

                    //Now we can load iframe with tab content to be edited
                    initIframe();

                }).error(function() {
                    displayFlashMessage('warning', "Couldn't load editor. Please try again.");
                });

                
                
            },
            
            'getCurrentTabName': function() {
                return $('#EditTemplateTab .editor-template-title input').val();
            },
            
            /**
             * Selects visually particular field/selectable element
             * @param {object} element Field's/selectable's DOM element
             */
            'selectElement': function(element) {
                var $element = $(element);
                
                //Remove selection from any other field/selectable element
                $element.parents('body').find('.field-element.selected,.selectable-element.selected,[data-admin-tag]').removeClass('selected');
                
                //Add selection to specific field/selectable element
                $element.addClass('selected');
                
            },
            
            /**
             * Opens up editing modal for selected field element
             * @param {object} fieldElement Field's DOM element
             */
            'editField': function(fieldElement) {
                var $field = $(fieldElement),
                    fieldType = $field.data('field-type'),
                    fieldAttributes = $field.data('attributes')
                ;
                
                currentFieldId = $field.data('field-id');
                
                //Fill in attribute editors with values from edited field
                for (var attributeName in fieldAttributes) {
                    $('#' + fieldType + '-' + attributeName + '-editor').trigger('update', [fieldAttributes[attributeName], fieldAttributes]);
                }
                
                $('.modal-editor:visible').hide();
                $('#' + fieldType + '-editor-modal').show(); //.trigger('openModal');
                
            },
            
            'updateEditedField': function(settingsContainerElement, successCallback, alwaysCallback) {
                var $iframe = $('#page-preview'),
                    fieldData = $(settingsContainerElement).find('label:visible input[type=checkbox],label:visible input[type=hidden],' +
                        '.url-link-holder:visible input[type=text],' +
                        'div:not(.url-link-holder) input[type=text],' +
                        '.attribute-editor > input[type=hidden],' +
                        'input[type!=checkbox][type!=hidden][type!=text],textarea,select').serialize()
                ;
                
                $.ajax({
                    'url': '/template/pageEditor/updateField/tabId/' + currentTabId + '/fieldId/' + currentFieldId,
                    'data': fieldData,
                    'dataType': 'json',
                    'type': 'post'
                }).success(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    if (data.error === 0) {
                        var fieldAttributes,
                            $field = $(data.content)
                        ;
                        
                        $('.field-element#page-field-' + currentFieldId, $iframe.contents()).replaceWith($field);
                        
                        
                        fieldAttributes = $field.data('attributes');
                        if ('adminTag' in fieldAttributes) {
                            updateStyleField($field, fieldAttributes);
                        }
                        
                        if (tabPublished) {
                            $('#editor-changes-not-published').show();
                        }
                        
                        //Stretch iframe to full height
                        $('#page-preview').css('height', $('#page-preview').contents()[0].body.scrollHeight + "px");
                        
                        // update gate lock icon positioning
                        var maxGateLeft = $('#page-preview').width() - 89,
                            maxGateTop = $('#page-preview').height() - 50
                        ;
                        $('.add-gate-button', $('#page-preview').contents()).each(function() {
                            var $gateField,
                                calculatedGateLeft,
                                calculatedGateTop
                            ;
                            
                            $gateField = $('#' + $(this).data('gate-id'), $('#page-preview').contents());

                            //If gate position gets outside an iframe, correct it's position by fitting it in iframe
                            calculatedGateLeft = ($gateField.offset().left + $gateField.width() - 50);
                            if (calculatedGateLeft > maxGateLeft) {
                                calculatedGateLeft = maxGateLeft;
                            }

                            calculatedGateTop = ($gateField.offset().top + $gateField.height() - 25);
                            if (calculatedGateTop > maxGateTop) {
                                calculatedGateTop = maxGateTop;
                            }

                            $(this)
                                .css('top', calculatedGateTop + 'px')
                                .css('left', calculatedGateLeft + 'px')
                            ;
                        });
                        
                        successCallback();
                    } else {
                        displayFlashMessage('warning', 
                            ("Couldn't perform update:\n" + data.errorMessage) || 
                            "Couldn't perform update."
                        );
                    }
                }).always(function() {
                    alwaysCallback();
                }); 
                
            }
        };
    })();
    
    
    //Events init
    $(document).on('click', '.tabbed-modal-nav .editor-nav-link', function(e) {
        var href = $(this).attr('href'),
            fnName = 'switchTo' + href.substring(1, href.length)
        ;
        tabManager[fnName]();
        e.preventDefault();
        return false;
    });

    $(document).on('click', '#editor-back-button', function(e) {
        e.preventDefault();
        
        if ($(this).hasClass('disabled')) {
            return;
        }
        
        tabManager.prevTab();
        return false;
    });

    $(document).on('click', '#editor-next-button', function(e) {
        e.preventDefault();
        
        if ($(this).hasClass('disabled')) {
            return;
        }
        
        tabManager.nextTab();
        return false;
    });
    
    //Choose template -> Edit button click
    $(document).on('click', '.template-box .edit-button', function(e) {
        var templateId = $(this).parents('.template-box').data('id');
        tabCreator.createTabForTemplate(templateId, publishFanpageId);
        
        e.preventDefault();
        return false;
    });
    
    $(document).on('click', '.modal-editor .close-modal-button', function(e) {
        var modalEditor = $(this).parents('.modal-editor');
        modalEditor.hide();
        modalEditor.find('input[type=checkbox]').parent().hide();
        if (modalEditor.find('.download-button-modal').length <= 0) {
            modalEditor.find('input[type=text]').parents('.url-link-holder').hide();
        }
        e.preventDefault();
        return false;
    });
    
    $(document).on('click', '.modal-editor-ok-buttom', function(e) {
        var editorElement = $(this).parents('.modal-editor')[0];
        
        //CKEditor values must be rewritten to input elements
        for (var instanceName in CKEDITOR.instances) {
            if(CKEDITOR.instances[instanceName].getData() == ''){
              //  alert('dasdsa')
                CKEDITOR.instances[instanceName].setData('<br>');
            };
            CKEDITOR.instances[instanceName].updateElement();
        }
        
        $.blockUI({message: '<h2>Please wait...</h2><h3>Applying changes...</h3>'});
        tabEditor.updateEditedField(
            editorElement,
            function() {
                $(editorElement).hide();
                $(editorElement).find('input[type=checkbox]').parent().hide();
                if ($(editorElement).find('.download-button-modal').length <= 0) {
                    $(editorElement).find('input[type=text]').parents('.url-link-holder').hide();
                }
                
                //Show information that changes have been saved at the bottom of editor
                $('#editor-changes-saved').css('display', 'block').delay(3000).fadeOut(1000);
            },
            function() {
                $.unblockUI();
            }
        );
        
        e.preventDefault();
        return false;
    });
    
    $(document).on('click', '.modal-gate-ok-button', function(e) {
        var editorElement = $(this).parents('.modal-editor')[0];
        
        $(editorElement).hide();        
        e.preventDefault();
        return false;
    });
    
    $(document).on('click', '.preview-on-fb-big, #editor-preview-on-facebook', function(e) {
        tabWizard.previewOnFacebook();
        e.preventDefault();
        return false;
    });
    
    $(document).on('click', '.publish-changes-big', function(e) {
        tabWizard.publishChanges();
        e.preventDefault();
        return false;
    });
    
    $(document).on('click', '.publish-now-big', function(e) {
        tabWizard.publishAtFacebook();
        e.preventDefault();
        return false;
    });
    
    
    //Unfortunate dependencies with tab.js and fanpage views
    $(document).on('click', '.choose-other-page-button', function(e) {
        showFanpageSelectorModal("Choose other page", 'publish-modal-select', publishFanpageId);
        e.preventDefault();
        return false;
    });
    
    $(document).on('click', '#publish-modal-select', function(e) {
        var fanpageId = getSelectedFanpageFromModal();
        
        function getSelectedFanpageFromModal()
        {
            return $("#publish-main-modal .page-box.active").data('pageid');
        }
        
        $.ajax({
            "url": "/template/pageEditor/pageSetFanpage/tabId/" + tabWizard.getCurrentTabId() + "/fanpageId/" + fanpageId
        }).done(function(data){
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            if (data === 'OK') {
                tabWizard.setPublishFanpage(fanpageId);
                $('#publish-main-modal').fadeOut();
            }
        });
        
        e.preventDefault();
        return false;        
    });
    
    $(document).on('click', '#publish-now-big', function(e) {
        //globaltabid = 
        //savetabtopage(fanpageId);
        tabWizard.publishAtFacebook();
        e.preventDefault();
        return false;
    });
    
    $(document).on('click', '.schedule-big', function(e) {
        
        globaltabid = tabWizard.getCurrentTabId();
        jQuery("#schedule-main-modal").css('z-index', 99999).fadeIn();
        e.preventDefault();
        return false;
    });
    
    
    //Select icon
    $(document).on('click', '.tab-icon-library .tab-icon .icon-image', function(e) {
        $.blockUI();
        $.ajax({
            'url': '/template/tabIcon/selectForTab/tabId/' + tabWizard.getCurrentTabId() + '/iconId/' + $(this).closest('.tab-icon').data('id')
        })
            .done(function(data) {
				
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                if (data == 'ERROR') {
                    return;
                }
                
                $('#PublishTab .select-button').css('background-image', "url('" + data + "')");
                $('#PublishTab .select-button').css('text-indent', '-99999px');
            })
            .fail(
                
            )
            .always(function() {
                $.unblockUI();
            })
        ;
        e.preventDefault();
        return false;
    });
    
    //Show remove button on hover icon 
    $(document).on('hover', '.user-icon', function(e){
       if(e.type == 'mouseenter'){
           $(this).find('.remove-button').show();
       }else{
           $(this).find('.remove-button').hide();
           
       }
    });
    
    
    //Delete icon
    $(document).on('click', '.tab-icon-library .tab-icon .remove-button', function(e) {
        that = this;
        $.blockUI();
        $.ajax({
            'url': '/template/tabIcon/deleteTabIcon/tabId/' + tabWizard.getCurrentTabId() + '/iconId/' + $(that).closest('.tab-icon').data('id')
        })
            .done(function(data) {
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                if (data == 'ERROR') {
                    return;
                }
                
                
                //Check if deleted icon was selected for tab
                currentImageSelected = $('.publish-box .content .select-button').css('background-image');
                currentImageSelected = currentImageSelected.replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '');
                console.log(currentImageSelected);
                console.log($(that).closest('.tab-icon').find('.icon-image').attr('src'));
                if($(that).closest('.tab-icon').find('.icon-image').attr('src') == currentImageSelected){
                    console.log('test');
                    $('#TabIconLibraryListView .tab-icon-library .system-icon .icon-image').eq(0).click();
                }
                
                //Remove icon from list
                $(that).closest('.tab-icon').fadeOut(800, function(){
                   $(that).closest('.tab-icon').remove(); 
                });
            })
            .fail(
                
            )
            .always(function() {
                $.unblockUI();
            })
        ;
        e.preventDefault();
        return false;
    });
    
    
    //Upload tab icon button
    $('#PublishTab .upload-tab-icon, #PublishTab .select-button').click(function(e) {
        $('#tab-icon-file').click();
        e.preventDefault();
        return false;
    });
    
    $('#tab-icon-file').change(function() {
        var formData;
        
        if (this.files.length === 0) {
            return;
        }


        $.blockUI();
        formData = new FormData();
        formData.append('file', this.files[0]);
        
        $.ajax({
            'url': '/template/tabIcon/selectForTab/tabId/' + tabWizard.getCurrentTabId(),
            'cache': false,
            'contentType': false,
            'processData': false,
            'type': 'POST',
            'data': formData
        })
            .done(function(data) {
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                if (data == 'ERROR') {
                    displayFlashMessage('warning', 'There was an error.');
                    return;
                }
                $('#PublishTab .select-button').css('background-image', "url('" + data + "')");
                $('#PublishTab .select-button').css('text-indent', '-99999px');
                $.fn.yiiListView.update('TabIconLibraryListView');
            })
            .always(function() {
                $.unblockUI();
            });

    });
    
    
    var delayedTabNameUpdateTimer;
    
    $('#EditTemplateTab .editor-template-title input').keypress(function() {
        var $input = $(this);

        if (delayedTabNameUpdateTimer) {
            window.clearTimeout(delayedTabNameUpdateTimer);
        }

        delayedTabNameUpdateTimer = window.setTimeout(function() {
            tabManager.changeTemplateName($input.val());
        }, 1000);

    });
	
    $('#editor-header-choose-title input').on('keyup', function(e){
		
        var btn = $('#editor-header-choose-title-save');
		
        var inputVal = $('#editor-header-choose-title input').val();
		
        if (inputVal.trim().length > 0) 
		{
			btn.addClass('active');
		}
		else if(inputVal.trim().length === 0 && btn.hasClass('active'))
		{
			btn.removeClass('active');			
		}
		
	});

	
    $('#editor-header-choose-title-save,.active').click(function(e) {
        var inputVal = $('#editor-header-choose-title input').val();
        if (inputVal.trim().length === 0) {
            return;
        }
        
        $('.editor-save-details').hide();
        $('#editor-header-saving').show();
        
        tabManager.changeTemplateName(
            inputVal, 
            function() { //Success
                $('#EditTemplateTab .editor-template-title input').val(inputVal);
                $('.editor-save-details').hide();
                $('#editor-header-saved').show();
            }, 
            function() { //Failure
                $('.editor-save-details').hide();
                $('#editor-header-choose-title').show();
            }
        );
        
        e.preventDefault();
        return false;
    });
    
    $('#editor-add-autoresponder').click(function(e) {
        autorespondersManager.openConfigurationModal(tabWizard.getCurrentTabId());
        e.preventDefault();
        return false;
    });
	
    $('#editor-add-crm').click(function(e) {
        crmManager.openConfigurationModal(tabWizard.getCurrentTabId());
        e.preventDefault();
        return false;
    });
	
    
    $('.copy-download-big').click(function(e) {
        tabWizard.showCopyHtmlModal();
        e.preventDefault();
        return false;
    });
    
    $('#search-templates').change(function() {
        var value = $(this).val();
        var $templates = $('.template-box');
        
        $('.show-all-button').click();
        if (!value.length) $templates.show();

        $templates.each(function () {
            var $template = $(this);
            var textToCheck = $template.find('div.title').text();
            textToCheck += $template.find('div.description').text();
            textToCheck.indexOf(value) >= 0 ? $template.show() : $template.hide();
        });

    });

    $('#search-library-publish').change(function() {
        var value = $(this).val();
        var $icons = $('.tab-icon');

        $icons.each(function () {
            var $icon = $(this);
            var textToSearch = $icon.data('filename');
            textToSearch.indexOf(value) >= 0 ? $icon.show() : $icon.hide();
        });

    });

    $('.categories-select-box').change(function() {
        var $selected = $('option:selected', $(this));
        $('.two strong').text($selected.text());
        $('.editor-header-templates-list .ticked').removeClass('active');
        filterTemplates($selected.val());
    });

    $('.show-all-button').click(function(e) {
        $('.two strong').text('All Categories');
        $('.editor-header-templates-list .ticked').addClass('active');
        filterTemplates(null);
        e.preventDefault();
        return false;
    });

    function reloadTemplatesList() {
        var viewData = '';
        $.get('/template/template/getTemplatesView').done(function (data) {
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            viewData = data;
            $('#editor-templates-list-content').fadeOut(200, function() {
                $(this).html(viewData);
                initTemplatesView();
                $(this).fadeIn(200, function() {
                    $(this).mCustomScrollbar({
                        mouseWheel:true,
                        alwaysShowScrollbar:1,
                        autoDraggerLength:true,   
                        advanced:{  
                          updateOnBrowserResize:true,   
                          updateOnContentResize:true   
                        }
                    });
                });
            });
        });
    }

    function filterTemplates(categoryId) {
        var $templates = $('.template-box');
        if (categoryId === null)
            return $templates.show();

		$templates.hide();
        $templates.each(function() {
           var $this = $(this);
           var categories = $this.attr('data-categories').split(',');
           categories.indexOf(categoryId) >= 0 ? $this.show() : $this.hide();
        });
    }

    function initTemplatesView() {
        $('.template-box .favorite-button').click(function(e) {
            var $this = $(this);
            var templateId = $this.find('.template-box').data('id');
            $this.toggleClass('active');
            $.get('/template/template/togglefavourite?id=' + templateId);
            e.preventDefault();
            return false;
        });

        $('.template-box .favorite-button').hover(function() {
            $(this).toggleClass('active');
        });

        $('.template-box .favorite-button').click(function (e) {
            var templateId      = $(this).closest('.template-box').data('id');
            var isNowFavourite  = $(this).hasClass('active');
            var templateName    = $(this).closest('.template-box').find('.title').text();

            $.get('template/template/togglefavourite?id=' + templateId)
                .done(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    $(this).toggleClass('active');
                    if (!isNowFavourite)
                        var message = templateName + ' was added to favourites';
                    else
                        var message = templateName + ' is no longer your favourite';
                    displayFlashMessage('success', message);
                    reloadTemplatesList();
                });
            e.preventDefault();
            return false;
        });


        $('.template-box .thumb').click(function(e) {
            var $box = $(this).closest('.template-box');
            tabWizard.openPreviewModal($box);
            e.preventDefault();
            return false;
        });
    };
    initTemplatesView();
    if (window.location.hash === '#createTab') {
        tabWizard.newTab(getDefaultFanpage());
    }
    
});

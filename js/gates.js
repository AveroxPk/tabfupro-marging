function showGatePopup()
{
    $('.gate-popup-overlay').show();
    var topValue = typeof tabTopValue != 'undefined' ? tabTopValue : 0;
    FB.Canvas.getPageInfo(function(pageInfo){
        $({ y: pageInfo.scrollTop }).animate(
            { y: topValue },
            { 
                duration: 1000,
                step: function(offset) {
                    FB.Canvas.scrollTo(0, offset);
                }
            }
        );
    });
    return false;
}

$(document).ready(function() {
    // move gate to the root
    if ($('body > .gate-popup-overlay').length <= 0) {
        var $gate = $('.gate-popup-overlay').clone();
        $('.gate-popup-overlay').replaceWith('');
        $($gate).appendTo('body');
    }
    
    $(document).on('click', '.gate-popup-close', function() {
        $(this).parents('.gate-popup-overlay').hide();
        return false;
    });
});

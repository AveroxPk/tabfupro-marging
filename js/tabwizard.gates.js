var gateWizard;


$(document).ready(function() {
    var currentTabId,
        likeGateEnabled, likeGateAvailable,
        shareGateEnabled, shareGateAvailable,
        emailGateEnabled, emailGateAvailable,
        customCodeGateEnabled, customCodeGateAvailable,
        gatesConfig,
        emailPromptVisible
    ;
    
    function refreshConfirmMessageTabsActiveness()
    {
        
        $('.link.second').removeClass('disabled').addClass('normal');
        
        if (customCodeGateEnabled) {
            /*$('#like-navigate-prompt').addClass('active').removeClass('normal');
            $('#like-confirm-content').hide();
            $('#like-prompt-content').show();*/
            $('#share-navigate-confirm,#email-navigate-confirm,#like-navigate-confirm').addClass('disabled').removeClass('normal');
            
            return;
        }
        if (emailGateEnabled) {
            $('#share-navigate-confirm,#like-navigate-confirm').addClass('disabled').removeClass('normal');
            
            $('#share-navigate-prompt, #like-navigate-prompt').addClass('active').removeClass('normal');
            $('#share-confirm-content, #like-confirm-content').hide();
            $('#share-prompt-content, #like-prompt-content').show();
            
            return;
        }
        if (shareGateEnabled) {
            $('#share-navigate-confirm').removeClass('disabled').addClass('normal');
            $('#like-navigate-confirm').addClass('disabled').removeClass('normal');
            
            $('#like-navigate-prompt').addClass('active').removeClass('normal');
            $('#like-confirm-content').hide();
            $('#like-prompt-content').show();
            
            return;
        }
        /* DISABLED DUE TO NEW FACEBOOK POLICY
         * if (likeGateEnabled) {
            $('#like-navigate-confirm').removeClass('disabled').addClass('normal');
        }*/
        
        
    }
    
    gateWizard = {
        'showConfigurationModal': function(tabId, gatesConfig) {
            gateWizard.setCurrentTabId(tabId);
            gateWizard.setGatesConfig(gatesConfig);
            $('.gs-modal').trigger('openModal');
        },
        
        'setCurrentTabId': function(tabId) {
            currentTabId = tabId;
        },
        
        'setGatesConfig': function(config) {
            var imageUrl;
            
            gatesConfig = config;
            
            // DISABLED DUE TO NEW FACEBOOK POLICY
            // likeGateAvailable = 'like' in config;
            shareGateAvailable = 'share' in config;
            emailGateAvailable = 'email' in config;
            customCodeGateAvailable = 'customCode' in config;
            
            // DISABLED DUE TO NEW FACEBOOK POLICY
            // likeGateEnabled = likeGateAvailable ? ('enabled' in config.like && !!+config.like.enabled) : false;
            shareGateEnabled = shareGateAvailable ? ('enabled' in config.share && !!+config.share.enabled) : false;
            emailGateEnabled = emailGateAvailable ? ('enabled' in config.email && !!+config.email.enabled) : false;
            customCodeGateEnabled = customCodeGateAvailable ? ('enabled' in config.customCode && !!+config.customCode.enabled) : false;
            
            emailPromptVisible = emailGateAvailable ? ('showPrompt' in config.email ? !!+config.email.showPrompt : true) : false;
            //UI init
            
            //Setup ticks and switches for gates
            // gateWizard.toggleLikeGate(likeGateEnabled); DISABLED DUE TO NEW FACEBOOK POLICY
            gateWizard.toggleShareGate(shareGateEnabled);
            gateWizard.toggleEmailGate(emailGateEnabled);
            gateWizard.toggleCustomCodeGate(customCodeGateEnabled);
            
            //Hide unavailable gates tabs
            // $('.like-gate')[likeGateAvailable ? 'show' : 'hide'](); DISABLED DUE TO NEW FACEBOOK POLICY
            $('.share-gate')[shareGateAvailable ? 'show' : 'hide']();
            $('.email-gate')[emailGateAvailable ? 'show' : 'hide']();
            $('.custom-code-gate')[customCodeGateAvailable ? 'show' : 'hide']();
            
            // update the modal header
            $('.gates-header').hide();
            $('.gates-info').hide();
            if (emailGateAvailable && !shareGateAvailable/* && !likeGateAvailable DISABLED DUE TO NEW FACEBOOK POLICY */) {
                $('.email-gates-header').show();
                $('.email-gates-info').show();
            }
            else {
                $('.general-gates-header').show();
                $('.general-gates-info').show();
            }
            
            if (customCodeGateEnabled) {
                $('.gs-nav .custom-code-gate .button-nav').trigger('click');
            
            } else if (emailGateEnabled) {
                $('.gs-nav .email-gate .button-nav').trigger('click');
            }
            /* DISABLED DUE TO NEW FACEBOOK POLICY
             * else if (likeGateEnabled) {
                $('.gs-nav .like-gate .button-nav').trigger('click');
            } */
            else if (shareGateEnabled) {
                $('.gs-nav .share-gate .button-nav').trigger('click');
            }
            else {
                $('.gs-nav .gate:visible:first a').trigger('click');
            }
            
            // update email tab subtabs visibility
            if (emailPromptVisible) {
                $('#email-gate-settings .gate-navigate .left').show();
                $('#email-navigate-prompt').trigger('click');
                /*$('#email-gate-settings .gate-navigate .left a').addClass('active').removeClass('normal');
                $('#email-gate-settings #email-prompt-content').show();
                $('#email-gate-settings #email-confirm-content').hide();*/
            }
            else {
                $('#email-gate-settings .gate-navigate .left').hide();
                $('#email-navigate-confirm').trigger('click');
                /*$('#email-gate-settings .gate-navigate .right a').addClass('active').removeClass('normal');
                $('#email-gate-settings #email-prompt-content').hide();
                $('#email-gate-settings #email-confirm-content').show();*/
            }
            
            //Setup gates text contents and button images
            /* DISABLED DUE TO NEW FACEBOOK POLICY
             * if (likeGateAvailable) {
                CKEDITOR.instances['like-prompt-text-edit'].setData(config.like.promptText);
                CKEDITOR.instances['like-confirm-text-edit'].setData(config.like.confirmText);
                
                imageUrl = 'downloadImgUrl' in config.like ? config.like.downloadImgUrl : $('#like-download-button').data('default-src');
                $('#like-download-button-modal-url').val('downloadUrl' in config.like ? config.like.downloadUrl : '');
                $('#like-download-button-modal-img').val('downloadImg' in config.like ? config.like.downloadImg : '');
                $('#like-download-button-modal .image-preview, #like-download-button').attr('src', imageUrl);
                
            }*/
            
            if (customCodeGateAvailable) {
                
                CKEDITOR.instances['custom-code-prompt-text-edit'].setData('promptTitle' in config.customCode ? config.customCode.promptTitle : 'YOUR CUSTOM HEADING HERE');
                
                $('#custom-code-prompt-content-modal-content').val('modalContent' in config.customCode ? config.customCode.modalContent : '');
                $('#custom-code-prompt-modal-content-height').val('modalContentHeight' in config.customCode ? config.customCode.modalContentHeight : 150);
                $('#custom-code-prompt-modal-content-width').val('modalContentWidth' in config.customCode ? config.customCode.modalContentWidth : 500);
            }
            
            if (shareGateAvailable) {
                CKEDITOR.instances['share-prompt-text-edit'].setData(config.share.promptText);
                CKEDITOR.instances['share-confirm-text-edit'].setData(config.share.confirmText);
                
                imageUrl = 'downloadImgUrl' in config.share ? config.share.downloadImgUrl : $('#share-download-button').data('default-src');
                $('#share-download-button-modal-url').val('downloadUrl' in config.share ? config.share.downloadUrl : '');
                $('#share-download-button-modal-img').val('downloadImg' in config.share ? config.share.downloadImg : '');
                $('#share-download-button-modal .image-preview, #share-download-button').attr('src', imageUrl);
            }
            
            if (emailGateAvailable) {
                CKEDITOR.instances['email-prompt-text-edit'].setData(config.email.promptText);
                CKEDITOR.instances['email-confirm-title-edit'].setData(config.email.confirmTitle);
                CKEDITOR.instances['email-confirm-text-edit'].setData(config.email.confirmText);
                
                $('.email-gate .gate-main-input').val(config.email.inputPlaceholder);
                $('.email-gate .privacy-info input').val(config.email.privacyText);
                
                imageUrl = 'promptButtonImgUrl' in config.email ? config.email.promptButtonImgUrl : $('#email-download-button').data('default-src');
                $('#email-download-button-modal-img').val('promptButtonImg' in config.email ? config.email.promptButtonImg : '');
                $('#email-download-button-modal .image-preview, #email-download-button, #email-get-access-now-button').attr('src', imageUrl);
            }
            
        },
        
        'saveConfiguration': function() {
            var newGatesConfig = {};
            
            $.blockUI();
            /* DISABLED DUE TO NEW FACEBOOK POLICY
             * if (likeGateAvailable) {
                newGatesConfig['like'] = {
                    "enabled": likeGateEnabled ? 1 : 0,
                    "showPrompt": emailPromptVisible ? 1 : 0,
                    "promptText": CKEDITOR.instances['like-prompt-text-edit'].getData(),
                    "confirmText": CKEDITOR.instances['like-confirm-text-edit'].getData(),
                    "downloadUrl": $('#like-download-button-modal-url').val(),
                    "downloadImg": $('#like-download-button-modal-img').val()
                };
            }*/
            
            if (customCodeGateAvailable) {
                newGatesConfig['customCode'] = {
                    "enabled": customCodeGateEnabled ? 1 : 0,
                    "modalContent": $('#custom-code-prompt-content-modal-content').val(),
                    "modalContentHeight": $('#custom-code-prompt-modal-content-height').val(),
                    "modalContentWidth": $('#custom-code-prompt-modal-content-width').val(),
                    "promptTitle": CKEDITOR.instances['custom-code-prompt-text-edit'].getData()
                };
            }
            
            if (shareGateAvailable) {
                newGatesConfig['share'] = {
                    "enabled": shareGateEnabled ? 1 : 0,
                    "showPrompt": emailPromptVisible ? 1 : 0,
                    "promptText": CKEDITOR.instances['share-prompt-text-edit'].getData(),
                    "confirmText": CKEDITOR.instances['share-confirm-text-edit'].getData(),
                    "downloadUrl": $('#share-download-button-modal-url').val(),
                    "downloadImg": $('#share-download-button-modal-img').val()
                };
            }
            
            if (emailGateAvailable) {
                newGatesConfig['email'] = {
                    "enabled": emailGateEnabled ? 1 : 0,
                    "showPrompt": emailPromptVisible ? 1 : 0,
                    "promptText": CKEDITOR.instances['email-prompt-text-edit'].getData(),
                    "confirmTitle": CKEDITOR.instances['email-confirm-title-edit'].getData(),
                    "confirmText": CKEDITOR.instances['email-confirm-text-edit'].getData(),
                    "promptButtonImg": $('#email-get-access-now-button-modal-img').val(),
                    "privacyText": $('.email-gate .privacy-info input').val(),
                    "inputPlaceholder": $('.email-gate .gate-main-input').val()
                };
            }
            
            //gatesConfig = newGatesConfig;
            $.ajax({
                "url": "/template/pageEditor/updateGatesConfig/tabId/" + currentTabId,
                "type": "POST",
                "data": {"gatesConfig": newGatesConfig},
                "dataType": 'json'
            }).done(function(data){
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                if (data === 0) {
                    alert('Some error occured. Please try again.');
                    return;
                }
                
                $('#gates-settings-saved-info').show().delay(3000).fadeOut(2000);
                tabWizard.updateGatesConfig(data);
            }).always(function() {
                $.unblockUI();
            });
            
        },
        
        'toggleLikeGate': function(value) {
            /* DISABLED DUE TO NEW FACEBOOK POLICY
             * likeGateEnabled = typeof value === 'undefined' ? !likeGateEnabled : value;
            
            if (likeGateEnabled) {
                $('#like-gate-tick').show();
                $('#like-gate-switch').removeClass('off').addClass('on');
            } else {
                $('#like-gate-tick').hide();
                $('#like-gate-switch').removeClass('on').addClass('off');
            }*/
        },
        
        'toggleCustomCodeGate': function(value) {
            customCodeGateEnabled = typeof value === 'undefined' ? !customCodeGateEnabled : value;
            
            if (customCodeGateEnabled) {
                $('#custom-code-gate-tick').show();
                $('#custom-code-gate-switch').removeClass('off').addClass('on');
            } else {
                $('#custom-code-gate-tick').hide();
                $('#custom-code-gate-switch').removeClass('on').addClass('off');
            }
            
            refreshConfirmMessageTabsActiveness();
        },
        
        'toggleShareGate': function(value) {
            shareGateEnabled = typeof value === 'undefined' ? !shareGateEnabled : value;
            
            if (shareGateEnabled) {
                $('#share-gate-tick').show();
                $('#share-gate-switch').removeClass('off').addClass('on');
            } else {
                $('#share-gate-tick').hide();
                $('#share-gate-switch').removeClass('on').addClass('off');
            }
            
            refreshConfirmMessageTabsActiveness();
        },
        
        'toggleEmailGate': function(value) {
            emailGateEnabled = typeof value === 'undefined' ? !emailGateEnabled : value;
            
            if (emailGateEnabled) {
                $('#email-gate-tick').show();
                $('#email-gate-switch').removeClass('off').addClass('on');
            } else {
                $('#email-gate-tick').hide();
                $('#email-gate-switch').removeClass('on').addClass('off');
            }
            
            refreshConfirmMessageTabsActiveness();
        }
        
        

    };
    
    
    function fixContentEditableBug()
    {
        //Chrome sets contenteditable=false when element is not visible
        //We fix that here
        $('.gs-modal [contenteditable]:visible')
            .attr('contenteditable', true)
            .each(
                function(i, editableElement) {
                    CKEDITOR.instances[editableElement.getAttribute('id')].destroy();
                    CKEDITOR.inline(editableElement);
                }
            )
        ;
    }
    
    $('.gs-modal').easyModal({
        'zIndex': function() {
            return 52;
        },
        'onOpen': function() {
            fixContentEditableBug();
        },
        'overlayClose': false,
        'closeButtonClass': '.close-window-169,.close-button'
    });
    
    
    //Gate tabs
    $('.gs-nav .gate .button-nav').click(function(e) {
        $gateButton = $(this).parents('.gate');
        $('.gate-settings').hide();
        $('#' + $gateButton.data('settings-id')).show();
        
        $('.gs-nav .gate.active').removeClass('active');
        $gateButton.addClass('active');
        
        fixContentEditableBug();
        
        e.stopPropagation();
        return false;
    });
    
    function disabledConfirmMessage()
    {
        var relevantGateName;
        if (customCodeGateEnabled) {
            relevantGateName = 'Custom Code gate';
        } else if (emailGateEnabled) {
            relevantGateName = 'Email gate';
        } else if (shareGateEnabled) {
            relevantGateName = 'Share gate';
        }
        
        displayFlashMessage('warning', 'Multiple gates active - please edit ' + relevantGateName);
    }
    
    //Like Gate subtabs
    $('#like-navigate-confirm').click(function(e) {
        if ($(this).hasClass('disabled')) {
            disabledConfirmMessage();
            return false;
        }
        
        $('#like-navigate-prompt').removeClass('active').addClass('normal');
        $(this).removeClass('normal').addClass('active');
        $('#like-prompt-content').hide();
        $('#like-confirm-content').show();
        fixContentEditableBug();
        
        e.stopPropagation();
        return false;
    });
        
    $('#like-navigate-prompt').click(function(e) {
        $('#like-navigate-confirm').removeClass('active').addClass('normal');
        $(this).removeClass('normal').addClass('active');
        $('#like-prompt-content').show();
        $('#like-confirm-content').hide();
        fixContentEditableBug();
        
        e.stopPropagation();
        return false;
    });
    
    
    //Share Gate subtabs
    $('#share-navigate-confirm').click(function(e) {
        if ($(this).hasClass('disabled')) {
            disabledConfirmMessage();
            return false;
        }
        
        $('#share-navigate-prompt').removeClass('active').addClass('normal');
        $(this).removeClass('normal').addClass('active');
        $('#share-prompt-content').hide();
        $('#share-confirm-content').show();
        fixContentEditableBug();
        
        e.stopPropagation();
        return false;
    });
        
    $('#share-navigate-prompt').click(function(e) {
        $('#share-navigate-confirm').removeClass('active').addClass('normal');
        $(this).removeClass('normal').addClass('active');
        $('#share-prompt-content').show();
        $('#share-confirm-content').hide();
        fixContentEditableBug();
        
        e.stopPropagation();
        return false;
    });
    
    //Email Gate subtabs
    $('#email-navigate-confirm').click(function(e) {
        if ($(this).hasClass('disabled')) {
            disabledConfirmMessage();
            return false;
        }
        
        $('#email-navigate-prompt').removeClass('active').addClass('normal');
        $(this).removeClass('normal').addClass('active');
        $('#email-prompt-content').hide();
        $('#email-confirm-content').show();
        fixContentEditableBug();
        
        e.stopPropagation();
        return false;
    });
        
    $('#email-navigate-prompt').click(function(e) {
        $('#email-navigate-confirm').removeClass('active').addClass('normal');
        $(this).removeClass('normal').addClass('active');
        $('#email-prompt-content').show();
        $('#email-confirm-content').hide();
        fixContentEditableBug();
        
        e.stopPropagation();
        return false;
    });
    
    //Gates "ON-OFF" switches
    $('#like-gate-switch, .like-gate .tick_container').click(function(e) {
        gateWizard.toggleLikeGate();
        e.stopPropagation();
        return false;
    });
    
    $('#share-gate-switch, .share-gate .tick_container').click(function(e) {
        gateWizard.toggleShareGate();
        e.stopPropagation();
        return false;
    });
    
    $('#email-gate-switch, .email-gate .tick_container').click(function(e) {
        gateWizard.toggleEmailGate();
        e.stopPropagation();
        return false;
    });
    
    $('#custom-code-gate-switch, .custom-code-gate .tick_container').click(function(e) {
        gateWizard.toggleCustomCodeGate();
        e.stopPropagation();
        return false;
    });
    
    //"SAVE" button
    $('.gs-modal .save-button').click(function(e) {
        gateWizard.saveConfiguration();
        e.stopPropagation();
        return false;
    });
    
    //Customisation of Download button in like gate confirmation popup
    $('#like-download-button').click(function(e) {
        $('#like-download-button-modal').show();
        e.stopPropagation();
        return false;
    });
    
    //Customisation of Download button in share gate confirmation popup
    $('#share-download-button').click(function(e) {
        $('#share-download-button-modal').show();
        e.stopPropagation();
        return false;
    });
    
    //Customisation of "GET ACCESS NOW" button in email gate prompt popup
    $('#email-get-access-now-button').click(function(e) {
        $('#email-get-access-now-button-modal').show();
        var $file = $('#email-get-access-now-button-modal input[type=file]');
        if ($(this).data('default-src') == $(this).attr('src') || $(this).attr('src').length <= 0) {
            // no custom image
            $("#email-get-access-now-button-modal .delete-image-button").hide();
            $('#email-get-access-now-button-modal input[type=hidden]').val('');
            $('#email-get-access-now-button-modal .image-preview').attr('src', '/images/tabwizard/drag-file.png');
        }
        else {
            $("#email-get-access-now-button-modal .delete-image-button").show();
            $('#email-get-access-now-button-modal input[type=hidden]').val($(this).attr('src').split('/').pop());
            $('#email-get-access-now-button-modal .image-preview').attr('src', $(this).attr('src'));
        }
        
        e.stopPropagation();
        return false;
    });
    
    $('.download-button-modal .delete-image-button').click(function(e) {
        $(this).parents('.modal-editor-content').find('img.image-preview').attr('src', '/images/tabwizard/drag-file.png');
        $(this).parents('.modal-editor-content').find('input[type=hidden]').val('');
        $('#email-get-access-now-button').attr('src', $('#email-get-access-now-button').data('default-src'));
        $(".delete-image-button").hide();
        e.stopPropagation();
        return false;
    });
    
    $('.download-button-modal .content').click(function(e) {
        $(this).siblings("input[type='file']").click();
        e.stopPropagation();
        return false;
    });
    
    $('.gate-button-image-file-input').change(function() {
        var formData,
            input = this,
            $input = $(this)
        ;
        
        $.blockUI({message: "Uploading file... Please wait..."});

        if (input.files.length === 0) {
            return false;
        }

        formData = new FormData();
        formData.append('file', input.files[0]);

        $.ajax({
            'url': '/template/pageEditor/uploadImage/tabId/' + tabWizard.getCurrentTabId(),
            'cache': false,
            'contentType': false,
            'processData': false,
            'dataType': 'json',
            'type': 'POST',
            'data': formData
        })
            .done(function(data) {
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                var $parent;

                if (data === 'error') {
                    alert("There was an error while image upload.");
                    return false;
                }

                //Reset file input
                $input.val('');    
                    
                //Server filename is sent through hidden field
                $('#' + $input.data('value-bind')).val(data.serverFilename);

                $parent = $input.parent();

                $(".image-preview", $parent).attr("src", data.url);
                $("#" + $input.data('image-bind')).attr('src', data.url);
                $input.parents('.modal-editor-content').find(".delete-image-button").show();
            })
            .complete(function() {
                $.unblockUI();
            });
    });
    
    //Init HTML editors
    //CKEDITOR.inline('like-prompt-text-edit');
    //CKEDITOR.inline('like-confirm-text-edit');
    if ($('#share-prompt-text-edit').length > 0) {
        CKEDITOR.inline('share-prompt-text-edit');
    }
    if ($('#share-confirm-text-edit').length > 0) {
        CKEDITOR.inline('share-confirm-text-edit');
    }
    if ($('#email-prompt-text-edit').length > 0) {
        CKEDITOR.inline('email-prompt-text-edit');
    }
    if ($('#email-confirm-title-edit').length > 0) {
        CKEDITOR.inline('email-confirm-title-edit');
    }
    if ($('#email-confirm-text-edit').length > 0) {
        CKEDITOR.inline('email-confirm-text-edit');
    }
    if ($('#custom-code-prompt-text-edit').length > 0) {
        CKEDITOR.inline('custom-code-prompt-text-edit');
    }
});


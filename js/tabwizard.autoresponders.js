var autorespondersManager;

$(document).ready(function() {
    var currentTabId;
    autorespondersManager = {
        "openConfigurationModal": function(tabId, autorespondersLists) {
            currentTabId = tabId;
            $.blockUI();
            $.ajax('/template/pageEditor/autorespondersConfig/tabId/' + tabId)
                .done(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
					
					if (data == 'error') {
						displayFlashMessage('warning', 'This feature not available, Please upgrade your package!');
						$.unblockUI();
						return;
					}
					
                    $('.ar-modal')
                        .html(data)
                        .trigger('openModal')
                    ;
            
                    //That will refresh "Connect" button state
                    $('.autoresponder-settings form input, .autoresponder-settings form textarea').trigger('keyup');
            
                    //Activate AWeber tab by default
                    autorespondersManager.switchTab('AutoresponderAweber');

                })
                .always(function() {
                    $.unblockUI();
                })
            ;
                        
        },
        
        "switchTab": function(autoresponderClassName) {
            $('.ar-element.active').removeClass('active');
            $('.ar-element#' + autoresponderClassName + '-btn').addClass('active');
            
            $('.ar-content:visible').hide();
            $('.ar-content#' + autoresponderClassName).show();
        }
    };
    
    
    $('.ar-modal').easyModal({
        'zIndex': function() {
            return 52;
        },
        'overlayClose': false,
        'closeButtonClass': '.closs-button,.close-window-button'
    });
    
    $('.button-ar').live('click', function() {
        autorespondersManager.switchTab($(this).data('autoresponder'));
        return false;
    });
    
    $('.choose-your-list-button').live('click', function() {
        $(this).next('.select-a-list').toggle();
        return false;
    });
    
    $('.list-selection-position').live('click', function() {
        var listId = $(this).data('list-id');
        var listName = $(this).text();
        var $parentContent = $(this).parents('.ar-content');
        
        $parentContent.find('.ar-list-name').text(listName);
        $parentContent.find('.ar-list-name-value').val(listId);
        
        $parentContent.find('.big-ticked,.select-a-list').hide();
        $parentContent.find('.connect').show().addClass('active');
        return false;
    });
    
    $('.autoresponder-settings .connect').live('click', function() {
        //alert('connecting autoresponder ajax');
        var $this = $(this);
        
        if (!$this.hasClass('active')) {
            return;
        }
        
        $.blockUI();
        $.ajax({
            'url': '/autoresponders/settings/connect/autoresponderName/' + $(this).data('autoresponder-name'),
            'type': 'post',
            'data': $this.parents('.autoresponder-settings').find('form').serialize()
        }).done(function(data) {
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            $this.parents('.ar-content').find('.select-a-list').replaceWith(data);
            $.ajax({
                'url': '/template/pageEditor/connectAutoresponder/tabId/' + currentTabId,
                'type': 'post',
                'data': {
                    "autoresponder": $this.data('autoresponder-name'),
                    "listId": '',
                    "listName": ''
                }
            }).done(function(data) {
                if (data == 'logged-out') {
                    window.location.href = '/login';
                }
                var $arContent = $this.parents('.ar-content');
                $arContent.find('.autoresponder-settings').hide();
                $arContent.find('.autoresponder-list-choice').show();
                
                //Clear previous list name and tick if any
                $arContent.find('.ar-list-name').text('');
                $arContent.find('.big-ticked').hide();
                
                $.unblockUI();
            }).fail(function() {
                $.unblockUI();
            });
        }).fail(function(xhr) {
            displayFlashWarning('responseText' in xhr ? xhr.responseText : 'Some error occured.');
            $.unblockUI();
        });
        
    });
    
    
    $('.list-selection-position').live('click', function() {
        
        var $parentContent = $(this).parents('.ar-content');
        var listName = $(this).text().trim();
        var listId = $(this).data('listId');

        $.blockUI();
        $.ajax({
            'url': '/template/pageEditor/connectAutoresponder/tabId/' + currentTabId,
            'type': 'post',
            'data': {
                "autoresponder": $(this).data('autoresponder-name'),
                "listId": listId,
                "listName": listName
            }
        }).done(function(data) {
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            if (data) {
                $parentContent.find('.big-ticked').show();
                
                //Update info in publish tab
                $.ajax({
                    "url": "/template/pageEditor/pageInfo/tabId/" + currentTabId,
                    "dataType": "json"
                }).done(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    $('#autoresponders-sum-up').text(data.autoresponders);
                    $('#autoresponders-lists-sum-up').text(data.autorespondersLists);
                }).always(function() {
                    $.unblockUI();
                });
                
            }
        }).fail(function() {
            $.unblockUI();
        });
        return false;
    });
    
    $(document).on('click', '.button.disconnect', function() {
        $('.autoresponder-modal').attr('data-autoresponder-name', $(this).data('autoresponder-name')).fadeIn();
        return false;
    });
    
    $(document).on('click', '.autoresponder-modal-inner > .button-tf-yes', function() {
        var $parentContent = $('.button.disconnect').parents('.ar-content');
        
        $.blockUI();
        $.ajax({
            'url': '/template/pageEditor/disconnectAutoresponder/tabId/' + currentTabId,
            'type': 'post',
            'data': {
                "autoresponder": $('.autoresponder-modal').data('autoresponder-name')
            }
        }).done(function(data) {
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            if (data) {
                $parentContent.find('.big-ticked').hide();
                $parentContent.find('.autoresponder-settings').show();
                $parentContent.find('.autoresponder-list-choice').hide();
                
                //Update info in publish tab
                $.ajax({
                    "url": "/template/pageEditor/pageInfo/tabId/" + currentTabId,
                    "dataType": "json"
                }).done(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    $('#autoresponders-sum-up').text(data.autoresponders);
                    $('#autoresponders-lists-sum-up').text(data.autorespondersLists);
                }).always(function() {
                    $.unblockUI();
                });
            }
        }).fail(function() {
            $.unblockUI();
        });
        $('.autoresponder-modal').fadeOut();
        return false;
    });
    
    $(document).on('click', ".autoresponder-modal-inner > .button-tf-cancel", function(){
        $('.autoresponder-modal').fadeOut();
        return false;
    });
    
    //Activate connect button only when all inputs are filled in
    $('.autoresponder-settings form input, .autoresponder-settings form textarea').live('keyup', function() {
        var $inputs = $(this).parents('form').find('input, textarea');
        
        for (var i=0; i < $inputs.length; ++i) {
            if ($($inputs[i]).val().trim().length == 0) {
                $(this).parents('.autoresponder-settings').find('.button.connect.active').removeClass('active');
                return false;
            }
        }
        
        $(this).parents('.autoresponder-settings').find('.button.connect:not(.active)').addClass('active');
        
    });
    
});
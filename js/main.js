
jQuery(window).ready(function(){
    // var scope = 'manage_pages,publish_actions,public_profile,email,read_insights';
    var scope = 'manage_pages,public_profile,email,read_insights';/*tabfu.local*/

    // refresh session every 2 minutes
    setInterval(function() {
        $.get('/site/refreshSession');
    }, 120000);
	
    var fbResponse;
    jQuery.ajaxSetup({ cache: true });
    jQuery.getScript('//connect.facebook.net/en_US/all.js', function(){
        FB.init({
            appId: facebookAppId
        });
        FB.getLoginStatus(function(response) {
			
            fbResponse = response;
        });
    });
    //Share on facebook video and picture
    function shareOnFacebook(name,url,image,video_link){
    FB.ui({
        method: 'share',
        title: name,
        href: url,
        picture: image,
        source:video_link
       // description: desc,
    }, function(response) {
        if(response && response.post_id){}
        else{}
    });
}


  
   
    jQuery(document).mouseup(function (e) {
        
        var container = jQuery(".modal-tf.message-dialog, .modal-tf.setting-dialog"),
            button = jQuery(".messages.inbox");
        
        if(!button.is(e.target)){
            if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.addClass('hidden');
            }
        }  
    });
    
    
    jQuery(document).on('click', '.setting-messages .messages', function() {
       
       // Show / hide modal (messages)
       var modal = jQuery(this).parent().find(".modal-tf");
       
       if (modal.hasClass('hidden')) {
           modal.removeClass('hidden');
           
           if( !jQuery(".modal-content-tf > .left-column-tf > .content-tf").hasClass("mCustomScrollbar") ) {
               jQuery(".modal-content-tf > .left-column-tf > .content-tf ").mCustomScrollbar({
                    mouseWheel:true,
                    alwaysShowScrollbar:1,
                    autoDraggerLength:true,   
                    advanced:{  
                      updateOnBrowserResize:true,   
                      updateOnContentResize:true   
                    }
               });    
           }
           
           if( !jQuery(".modal-content-tf > .right-column-tf > .content-tf > .message-scroll-content").hasClass("mCustomScrollbar") ) {
               jQuery(".modal-content-tf > .right-column-tf > .content-tf > .message-scroll-content").mCustomScrollbar({
                    mouseWheel:true,
                    alwaysShowScrollbar:1,
                    autoDraggerLength:true,   
                    advanced:{  
                      updateOnBrowserResize:true,   
                      updateOnContentResize:true   
                    }
               });    
           }
            
       } else {
           modal.addClass('hidden');
       }
       return false;
       
    });
    
    jQuery(document).on('click', '.setting-button .setting', function() {
      
       // Show / hide modal (messages)
       var modal = jQuery(this).parent().find(".modal-tf");
       
       if (modal.hasClass('hidden')) {
           modal.removeClass('hidden');
       } else {
           modal.addClass('hidden');
       }
       return false;
    });
	
	
    
    jQuery(document).on('click', '.setting-button .setting', function() {
       var modal = jQuery(this).parent().find(".modal-tf");
       modal.removeClass('hidden');
    });   
	
    jQuery(document).on('click', '.setting-messages .messages', function() {
       var modal = $('.setting-button').find(".modal-tf");
       modal.addClass('hidden');
    });   
    
    jQuery(document).on('click', "#addfacebookaccount", function(){
        if ($('.facebookaccounts .empty').length > 0) {
            connectToFacebookAccount();
        }
        else if (fbResponse.status === 'connected') {
            $('.newaccount-modal').fadeIn();
        }
        else {
            connectToFacebookAccount();
        }
        return false;
    });
    
    jQuery(document).on('click', ".newaccount-modal-inner > .button-tf-cancel", function(){
        $('.newaccount-modal').fadeOut();
        return false;
    });
    
    jQuery(document).on('click', ".newaccount-modal-inner > .button-tf-yes", function(){
        FB.logout(function(response) {
            displayFlashMessage('success', 'you have successfully logged out<br/>please click the \'+ facebook account\' button to add your new account');
            // refresh current status
            FB.getLoginStatus(function(response) {
                fbResponse = response;
            });
        });
        $('.newaccount-modal').fadeOut();
        return false;
    });
    
    function connectToFacebookAccount() {
        
        FB.login(function(response) {
            if (response.authResponse) {
                token = response.authResponse.accessToken;
                console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', function(response) {
                  window.location.href='/addfacebookaccount/'+token;
                });
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {scope: scope});
    }
        
    jQuery(document).on('click', "#facebookLogin", function(){
        
        if (fbResponse.status === 'connected') {
            // FB.logout(function(response) {
            FB.login(function(response) {
                if (response.authResponse) {
                  token = response.authResponse.accessToken;
                  // console.log('response',fbResponse.status);
                  console.log('Welcome!  Fetching your information.... ');
                  FB.api('/me', function(response) {
                    window.location.href='/facebookLogin/'+token;
                      window.location.href='/changeprofile/';
                  }, {scope: scope});
                } else {
                  console.log('User cancelled login or did not fully authorize.');
                }
            // });
            });
        } 
        else if (fbResponse.status === 'not_authorized') {
                FB.login(function(response) {
                    if (response.authResponse) {
                      token = response.authResponse.accessToken;
                      console.log('Welcome!  Fetching your information.... ');
                      FB.api('/me', function(response) {
                        window.location.href='/facebookLogin/'+token;
                      });
                    } else {
                      console.log('User cancelled login or did not fully authorize.');
                    }
                  }, {scope: scope});
          
        } 
        else {
          FB.login(function(response) {
                if (response.authResponse) {
                  token = response.authResponse.accessToken;
                   console.log('response',response);
                  console.log('Welcome!  Fetching your information.... ');
                 
                  return false;
                  FB.api('/me', function(response) {
                    window.location.href='/facebookLogin/'+token;
                  });
                } else {
                  console.log('User cancelled login or did not fully authorize.');
                }
              }, {scope: scope});
        }
        
        return false;
    });
        
        
            
   if(jQuery('.flash-container').is(':visible')){
        jQuery('.flash-container').each(function(){
            jQuery(this).delay( 5000 ).fadeOut();
        });
    }  
});
   

function displayFlash(text){
    jQuery('.flash-container').each(function(){
        jQuery(this).remove();
    });
    jQuery('body').append('<div style="display:none;" class="flash-container col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1"><div class="flash-container-close"><span class="glyphicon glyphicon-remove"></span></div><div class="flash-notice">'+text+'</div></div>');
    jQuery('.flash-container').fadeIn().delay( 5000 ).fadeOut();
}
function displayFlashConfirm(text){
    jQuery('.flash-container').each(function(){
        jQuery(this).remove();
    });
    jQuery('body').append('<div style="display:none;" class="flash-container col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1"><div class="flash-container-close"><span class="glyphicon glyphicon-remove"></span></div><div class="flash-notice">'+text+'</div></div>');
    jQuery('.flash-container').fadeIn();
}
function displayFlashWarning(text){
    jQuery('.flash-container').each(function(){
        jQuery(this).remove();
    });
    jQuery('body').append('<div style="display:none;" class="flash-container col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1"><div class="flash-container-close"><span class="glyphicon glyphicon-remove"></span></div><div class="flash-warning">'+text+'</div></div>');
    jQuery('.flash-container').fadeIn().delay( 5000 ).fadeOut();
}
jQuery(document).on('click', '.flash-container-close .glyphicon-remove',  function(){
    jQuery(this).closest('.flash-container').hide(); // was fadeOut() but didn't work
});
jQuery(document).on('click', '.flash-container-close.set-cookie .glyphicon-remove',  function(){
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    document.cookie = 'hide_warning_message=true; expires=' + d.toUTCString() + '; path=/';
    jQuery(this).closest('.flash-warning').hide();
});
/**
 * 
 * @param {string} type
 * @param {string} text
 * @param {string} style Extra flash div's style (you can change it's original position for example)
 */
function displayFlashMessage(type, text, style){
    var $flash, 
        $flashClose,
        fadeoutTimer
    ;        
    
    jQuery('.flash-container').each(function(){
        jQuery(this).remove();
    });
    
    $flash = $('<div style="display:none;" class="flash-container col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1"><div class="flash-container-close"><span class="glyphicon glyphicon-remove"></span></div><div class="flash-'+type+'">'+text+'</div></div>');    
    if (typeof style !== 'undefined') {
        $flash.attr('style', style);
    }

    jQuery('body').append($flash);
    fadeoutTimer = setTimeout(function() {
        $flash.fadeOut();
    }, 5000);
    $flash.fadeIn();
    
    $flashClose = $('.flash-container-close', $flash);
    $flashClose.click(function() {
        clearTimeout(fadeoutTimer);
        $flash.fadeOut();
        return false;
    });
    
}
    
    function displaymodal(selector){
        numberofmodals = jQuery('.modal-wrapper').length;
        if (numberofmodals <= 0) {
            jQuery('body').append('<div class="modal-wrapper" style="z-index:100"></div>');
        }
        jQuery(selector).fadeIn();
        selector.css('z-index', numberofmodals+101);
        return true;
    }
    
    function closemodal(selector){
        jQuery(selector).fadeOut(function(){
            jQuery(".modal-wrapper").replaceWith('');
            selector.css('z-index', false);
            return true;
        });
    }

var Ext = {
    isEmpty: function(value, allowEmptyString) {
        return (value === null) || ( typeof (value) ==  'undefined') || (!allowEmptyString ? value === '' : false) || (value.length === 0);
    }
};

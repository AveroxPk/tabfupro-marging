var crmManager;

$(document).ready(function() {
    var currentTabId;
    crmManager = {
        "openConfigurationModal": function(tabId, crmLists) {
            currentTabId = tabId;
            $.blockUI();
            $.ajax('/template/pageEditor/crmConfig/tabId/' + tabId)
/*			
			$.ajax({
				url: '/template/pageEditor/crmConfig/tabId/' + tabId,
				dataType: 'html'
			})
*/                .done(function(data) {
					
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
					
					if (data == 'error') {
						displayFlashMessage('warning', 'This feature not available, Please upgrade your package!');
						$.unblockUI();
						return;
					}
					
                    $('.crms-modal')
                        .html(data)
                        .trigger('openModal')
                    ;
            
                    //That will refresh "Connect" button state
                    $('.crm-settings form input, .crm-settings form textarea').trigger('keyup');
            
                    //Activate AWeber tab by default
                    crmManager.switchTab('zoho');

                })
                .always(function() {
                    $.unblockUI();
                })
            ;
        },
        
        "switchTab": function(crmClassName) { 
            $('.crm-element.active').removeClass('active');
            $('.crm-element#' + crmClassName + '-btn').addClass('active');
            
            $('.crm-content:visible').hide();
            $('.crm-content#' + crmClassName).show();
        }
    };
    
    
    $('.crms-modal').easyModal({
        'zIndex': function() {
            return 52;
        },
        'overlayClose': false,
        'closeButtonClass': '.closs-button,.close-window-button'
    });
    
    $('.button-crm').live('click', function() {
        crmManager.switchTab($(this).data('crm'));
        return false;
    });
        
    
    $('.crm-settings .connect').live('click', function() {
        //alert('connecting autoresponder ajax');
        var $this = $(this);
        
        if (!$this.hasClass('active')) {
            return;
        }
        $.blockUI();
        $.ajax({
            'url': '/template/pageEditor/connectCrm/tabId/' + currentTabId,
            'type': 'post',
            'data': $this.parents('.crm-settings').find('form').serialize()
        }).done(function(data) {
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
			var $arContent = $this.parents('.crm-content');
			$arContent.find('.crm-settings').hide();
			$arContent.find('.crm-list-choice').show();
			
			//Clear previous list name and tick if any
	//		$arContent.find('.crm-list-name').text('');
			$arContent.find('.big-ticked').show();
			
			$.unblockUI();
			
        }).fail(function(xhr) {
            displayFlashWarning('responseText' in xhr ? xhr.responseText : 'Some error occured.');
            $.unblockUI();
        });
        
    });
    
  /*  
    $('.list-selection-position').live('click', function() {
        
        var $parentContent = $(this).parents('.crm-content');
        var listName = $(this).text().trim();
        var listId = $(this).data('listId');

        $.blockUI();
        $.ajax({
            'url': '/template/pageEditor/connectAutoresponder/tabId/' + currentTabId,
            'type': 'post',
            'data': {
                "autoresponder": $(this).data('autoresponder-name'),
                "listId": listId,
                "listName": listName
            }
        }).done(function(data) {
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            if (data) {
                $parentContent.find('.big-ticked').show();
                
                //Update info in publish tab
                $.ajax({
                    "url": "/template/pageEditor/pageInfo/tabId/" + currentTabId,
                    "dataType": "json"
                }).done(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    $('#autoresponders-sum-up').text(data.autoresponders);
                    $('#autoresponders-lists-sum-up').text(data.autorespondersLists);
                }).always(function() {
                    $.unblockUI();
                });
                
            }
        }).fail(function() {
            $.unblockUI();
        });
        return false;
    });
    */
	
    $(document).on('click', '.button.disconnect', function() {
        $('.crm-modal').attr('data-crm-name', $(this).data('crm-name')).fadeIn();
        return false;
    });
    
    $(document).on('click', '.crm-modal-inner > .button-tf-yes', function() {
        var $parentContent = $('.button.disconnect').parents('.crm-content');
        
        $.blockUI();
        $.ajax({
            'url': '/template/pageEditor/disconnectCrm/tabId/' + currentTabId,
            'type': 'post',
            'data': {
                "crm": $('.crm-modal').data('crm-name')
            }
        }).done(function(data) {
            if (data == 'logged-out') {
                window.location.href = '/login';
            }
            if (data) {
                $parentContent.find('.crm-settings').show();
                $parentContent.find('.crm-list-choice').hide();
                $parentContent.find('.big-ticked').hide();
				$.unblockUI();
//                $parentContent.find('.autoresponder-list-choice').hide();
                
                //Update info in publish tab
				/*
                $.ajax({
                    "url": "/template/pageEditor/pageInfo/tabId/" + currentTabId,
                    "dataType": "json"
                }).done(function(data) {
                    if (data == 'logged-out') {
                        window.location.href = '/login';
                    }
                    $('#autoresponders-sum-up').text(data.autoresponders);
                    $('#autoresponders-lists-sum-up').text(data.autorespondersLists);
                }).always(function() {
                    $.unblockUI();
                });
				*/
            }
        }).fail(function() {
            $.unblockUI();
        });
        $('.crm-modal').fadeOut();
        return false;
    });
    
    $(document).on('click', ".crm-modal-inner > .button-tf-cancel", function(){
        $('.crm-modal').fadeOut();
        return false;
    });
    
    //Activate connect button only when all inputs are filled in
    $('.crm-settings form input, .crm-settings form textarea').live('keyup', function() {
        var $inputs = $(this).parents('form').find('input, textarea');
        
        for (var i=0; i < $inputs.length; ++i) {
            if ($($inputs[i]).val().trim().length == 0) {
                $(this).parents('.crm-settings').find('.button.connect.active').removeClass('active');
                return false;
            }
        }
        
        $(this).parents('.crm-settings').find('.button.connect:not(.active)').addClass('active');
        
    });
    
});